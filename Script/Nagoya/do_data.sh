#!/bin/bash
#PBS -q one_week
#PBS -j oe
#PBS -l nodes=1:ppn=1
#PBS -o /tmp/joblog_${PBS_JOBID}.txt

#usage="Usage: $0 build_dir input_file middle_file output_file fitmode table_dir fill pileup glop_opt option log_file"
#if [ $# -ne 11 ]; then
#    echo $usage
#    exit -1
#fi

source ${SCRIPT_DIR}/setup_common.sh
if [[ "$HOSTNAME" =~ "crcpu"  ]]; then
	source ${SCRIPT_DIR}/setup_crcpu.sh
elif [[ "$HOSTNAME" =~ "lhcfs" ]] || [[ "$HOSTNAME" =~ "lhcfcl" ]]; then
	source ${SCRIPT_DIR}/setup_lhcfs2.sh
fi

build_dir=${BUILD_DIR}
input_file=$1
cal_file=$2
rec_file=$3
red_file=$4
table_dir=$5
fill=$6
subfill=$7
option_cal="$8"
option_rec="$9"
option_red="${10}"
log_file=${11}
skip_cal=0
skip_rec=0
skip_red=0

cd $build_dir
echo $PWD

exec 1>$log_file
exec 2>&1

echo "START"
date
echo "INPUT FILE = ${input_file}" 
echo "CAL FILE = ${cal_file}" 
echo "REC FILE = ${rec_file}"
echo "RED FILE = ${red_file}"
echo "OPTION_CAL : ${option_cal}"
echo "OPTION_REC : ${option_rec}"
echo "OPTION_RED : ${option_red}"

echo "$build_dir/bin/Calibrate -i $mctolevel0_file -o $cal_file -t $table_dir -F $fill -S $subfill $option_cal"
echo "$build_dir/bin/Reconstruct -i $cal_file -o $rec_file -t $table_dir -F $fill -S $subfill $option_rec"
echo "$build_dir/bin/Reduction -i $input_file -o $output_file  -F $fill -S $subfill $option_red"


if [[ $skip_cal -eq 0 ]]; then
   echo "== Calibrate =="
   time $build_dir/bin/Calibrate -i $input_file -o $cal_file -t $table_dir -F $fill -S $subfill $option_cal
fi

if [[ $skip_rec -eq 0 ]]; then
   echo "== Reconstruct =="
   time $build_dir/bin/Reconstruct -i $cal_file -o $rec_file -t $table_dir -F $fill -S $subfill $option_rec
fi

if [[ $skip_red -eq 0 ]]; then
   echo "== Reduction =="
   time $build_dir/bin/Reduction -i  $rec_file -o $red_file　-F $fill -S $subfill $option_red
fi 

echo "END"
date
