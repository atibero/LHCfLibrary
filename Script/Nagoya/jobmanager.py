#
# Script to manage the jobs in the CPU cluster at Nagoya (lhcfs2 or crcpu0)
# 
import os
import subprocess
import time
from logging import getLogger, StreamHandler, Formatter, FileHandler, DEBUG

#logger = getLogger(__name__)

#### Job Manager ###
class JobManager:
    def __init__(self, cluster='lhcfs2', max_jobs = -1, sleep_time=5, logfile='./submitlog.txt'):
        self.logger = self.setup_logger(logfile)
        self.sleep_time = sleep_time
        self.cluster = cluster
        if cluster == 'lhcfs2' and max_jobs < 0:
            max_jobs = 200
        elif cluster == 'crcpu0' and max_jobs < 0:
            max_jobs = 48
        self.max_jobs = max_jobs

    def setup_logger (self, logfile='./submitlog.txt'):
        logger = getLogger(__name__)
        logger.setLevel(DEBUG)

        sh = StreamHandler()
        sh.setLevel(DEBUG)
        formatter = Formatter('%(asctime)s: %(message)s')
        sh.setFormatter(formatter)
        logger.addHandler(sh)

        fh = FileHandler(logfile) #fh = file handler
        fh.setLevel(DEBUG)
        fh_formatter = Formatter('%(asctime)s : %(message)s')
        fh.setFormatter(fh_formatter)
        logger.addHandler(fh)
        return logger

    def njob (self) :
        username = os.getlogin()
        ret = subprocess.check_output('qstat | grep {} | grep -v " C " | wc'.format(username), shell=True)
        vals = ret.split()
        n = int(vals[0])
        return n

    def wait_job (self) :
        while True :
            if self.njob() <= self.max_jobs :
                break
            time.sleep(self.sleep_time)
        return

    def submit_job (self, scriptfile):

        ret = subprocess.run('qsub {} '.format(scriptfile), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.logger.info('submit {} ({})' .format(scriptfile, ret.stdout.decode().strip()))
        return 
    
    def loop_jobs (self, script_list, time_step=0):

        for scriptfile in script_list:
            self.wait_job()
            self.submit_job(scriptfile)
            time.sleep(time_step)
        
        return 
