'''
Example to submit jobs for Nagoya Clusters (lhcfs2 or crcpu0)

IMPORTANT)  
  This code works only with python3.  The default python version is 2.7 in both clusters. 
  Firstly you need to configure your terminal to use python3 as   
    source setup_lhcfs2.sh    or 
    source setup_crcpu0.sh 

Firstly this code generates the shell script for each run and saves them to 'runs' directory.
Then this code submit the job to the cluster system considering the number of running or waiting runs 
which should be less than the specified maximum number of jobs as max_jobs in JobManager

The directory path and filename of input files are automatically set.
The output parameters, directory path and file names, can be configured as you want 
while default are set automatically. 
For example, if you specified the base directory in data.Data_Fill8178_1( base ),
the output filename and path are 
    base/ -- cal/cal_runXXXXX.root 
          |- rec/rec_runXXXXX.root
          |- red/red_runXXXXX.root 

If you set your own path and filename, 
     a = data.Data_Fill8178_1('base')
     a.dir_rec = '/mnt/lhcfs5/data/tmp/test_rec/'  # must be a full path  
     a.file_rec = 'run{}_rec.root' # the run number is filled to {}
See Script/Nagoya/jobbase.py for other valuables 

Script files are generated base on the base script files:
    do_data.sh,  do_mc.sh etc.
The option for the application can be specified directly to the base script or 
you can set in this code like Create_Script_Data(red_option = '--disable-arm2')
'''

import os
import sys

sys.path.append('../Script/Nagoya')
from jobmanager import JobManager
import DataOp2015 as data
import McOp2015 as mc

output_base = '/mnt/lhcfs5/data/tmp/'  # set a full path
library_dir = os.getcwd() + '/../'
table_dir = library_dir + '/Tables/Op2015/'  # set the table dir

# Set the data sets which are processed 
data_list = []
data_list.append(data.Data_Fill3855_1(output_base + 'fill3855_1'))
data_list.append(data.Data_Fill3855_2(output_base + 'fill3855_2'))
# data_list.append( data.Data_Fill3851_1(output_base + 'fill3851_1') )
data_list.append(data.Data_Fill3851_2(output_base + 'fill3851_2'))  # 5mm high position
# data_list.append( data.Data_Fill3851_3(output_base + 'fill3851_3') )
# data_list.append( data.Data_Fill3851_4(output_base + 'fill3851_4') )
# data_list.append( data.Data_Fill3851_5(output_base + 'fill3851_5') )

# Generate the script files for runs -----
# Please comment/uncomment the lines which you would proceed
job_list = []
for d in data_list:
    job_list += d.Create_Script_Data(table_dir=table_dir,
                                     cal_option='--disable-arm2 --hist --ped',
                                     rec_option='--disable-arm2 -p full --level2',
                                     red_option='--disable-arm2')

    # You can change the default path/filename/parameter.
    # An example for process of rec and red by lvl3 data with -p none option (no position fitting)
    # d.dir_cal = d.dir_rec
    # d.file_cal = d.file_rec
    # d.dir_rec = d.dir_rec.replace('/rec', '/rec_mod1')
    # d.dir_red = d.dir_red.replace('/red', '/red_mod1')
    # d.dir_runs = d.dir_runs.replace('/runs', '/runs_mod1')
    # d.dir_log = d.dir_log.replace('/log', '/log_mod1')
    # job_list += d.Create_Script_Data(table_dir=table_dir,
    #                                 cal_option='--disable-arm2 --hist --ped',
    #                                 rec_option='--disable-arm2 -p none',
    #                                 red_option='--disable-arm2',
    #                                 skip_cal=True)
    # If you want to do only a part of the process steps, you can specify the skip options,
    # skip_cal, skip_rec, skip_red like skip_cal=True

# For MC
data_list = []
# pp 13 TeV collisions
# data_list.append( mc.Mc_QGSJET204_wopipe_arm1_beamcenter(output_base + 'mc_qgsjet2'))
# data_list.append( mc.Mc_EPOSLHC_wopipe_arm1_beamcenter(output_base + 'mc_eposlhc'))
# data_list.append( mc.Mc_QGSJET204_wpipe_arm1_beamcenter(output_base + 'mc_qgsjet2_wpipe'))
# data_list.append( mc.Mc_QGSJET204_wopipe_arm1_5mmhigh(output_base + 'mc_qgsjet2_5mmhigh'))
# specific MC
# data_list.append( mc.Mc_PurePi0_arm1_type1(output_base + 'mc_purepi0_type1'))
# data_list.append( mc.Mc_PurePi0_arm1_type2ts(output_base + 'mc_purepi0_type2ts'))
# data_list.append( mc.Mc_PurePi0_arm1_type2tl(output_base + 'mc_purepi0_type2tl'))

for d in data_list:
    job_list += d.Create_Script_Mc(table_dir=table_dir,
                                   mctolvl0_option='--arm1',
                                   cal_option='--disable-arm2 --hist --ped',
                                   rec_option='--disable-arm2 -p full --level2',
                                   red_option='--disable-arm2')

# Submit the jobs and 
manager = JobManager(cluster='crcpu0', max_jobs=24, logfile='./submit_log.txt')
manager.loop_jobs(job_list)
