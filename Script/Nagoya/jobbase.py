##
import os


class DataBaseClass:
    def __init__(self):
        self.file_raw = ''
        self.file_mctolvl0 = ''
        self.file_cal = ''
        self.file_rec = ''
        self.file_red = ''
        self.file_mc = ''  # filename for End2End text file
        self.format_mc = ''
        self.dir_raw = ''
        self.dir_common = ''
        self.dir_mctolvl0 = ''
        self.dir_cal = ''
        self.dir_rec = ''
        self.dir_red = ''
        self.dir_mc = ''
        self.dir_log = ''
        self.dir_runs = ''
        self.dir_tmp = ''
        self.runs = [-1, -1]  # [first_run, last_run]
        # self.subrun_num = -1
        self.subruns = [1, 1]  # subrun numbers for MC
        self.runs_exclude = []
        self.fill = -1
        self.subfill = -1
        self.mcmodel = ''
        self.flag_mc = False
        self.tag_data = ''

    def SetDefault(self):
        # Filename format
        if self.file_raw == '' and self.file_mc:
            self.file_raw = self.file_mc.replace('.out', '.root')  # for MC
        if self.file_mctolvl0 == '':
            self.file_mctolvl0 = 'mctolvl0_' + self.file_raw
        if self.file_cal == '':
            self.file_cal = 'cal_' + self.file_raw
        if self.file_rec == '':
            self.file_rec = 'rec_' + self.file_raw
        if self.file_red == '':
            self.file_red = 'red_' + self.file_raw
        # Output directories 
        if self.dir_common != '' and self.dir_raw == '':
            self.dir_raw = self.dir_common + '/raw/'  # for MC
        if self.dir_common != '' and self.dir_mctolvl0 == '':
            self.dir_mctolvl0 = self.dir_common + '/mctolvl0/'
        if self.dir_common != '' and self.dir_cal == '':
            self.dir_cal = self.dir_common + '/cal/'
        if self.dir_common != '' and self.dir_rec == '':
            self.dir_rec = self.dir_common + '/rec/'
        if self.dir_common != '' and self.dir_red == '':
            self.dir_red = self.dir_common + '/red/'
        if self.dir_common != '' and self.dir_log == '':
            self.dir_log = self.dir_common + '/log/'
        if self.dir_common != '' and self.dir_runs == '':
            self.dir_runs = self.dir_common + '/runs/'
        if self.dir_common != '' and self.dir_tmp == '':
            self.dir_tmp = self.dir_common + '/tmp/'
            # Data tag
        if self.tag_data == '' and self.mcmodel != '':
            self.tag_data == self.mcmodel
        if self.tag_data == '' and self.mcmodel == '':
            self.tag_data == 'data'

    # Create a bash script for each run : For Experimental Data
    def Create_Script_Data(self,
                           table_dir='', cal_option='', rec_option='', red_option='',
                           script_dir='', pbslog_dir='', dir_runs='',
                           skip_cal=False, skip_rec=False, skip_red=False):

        # set the default value
        if script_dir == '':
            script_dir = os.getcwd()
        if pbslog_dir == '':
            pbslog_dir = self.dir_log
        if dir_runs == '':
            dir_runs = self.dir_runs

        # Open base script
        try:
            f = open('do_data.sh')
        except OSError as e:
            print(e)
        else:
            base = f.read()
            f.close()

        if self.runs[0] < 0 or self.runs[1] < 0:
            print('please specify the runs')
            exit(-1)

        # Create the job script directory
        os.makedirs(dir_runs, exist_ok=True)

        # Create the output directories
        if skip_cal == False:
            os.makedirs(self.dir_cal, exist_ok=True)
        if skip_rec == False:
            os.makedirs(self.dir_rec, exist_ok=True)
        if skip_red == False:
            os.makedirs(self.dir_red, exist_ok=True)

        os.makedirs(self.dir_log, exist_ok=True)

        joblist = []
        for run in range(self.runs[0], self.runs[1] + 1):
            if run in self.runs_exclude:
                continue

            a = base
            a = a.replace('/tmp', pbslog_dir)
            a = a.replace('${SCRIPT_DIR}', script_dir)
            a = a.replace('$1', (self.dir_raw + self.file_raw).format(run))
            a = a.replace('$2', (self.dir_cal + self.file_cal).format(run))
            a = a.replace('$3', (self.dir_rec + self.file_rec).format(run))
            a = a.replace('$4', (self.dir_red + self.file_red).format(run))
            a = a.replace('$5', table_dir)
            a = a.replace('$6', str(self.fill))
            a = a.replace('$7', str(self.subfill))
            a = a.replace('$8', cal_option)
            a = a.replace('$9', rec_option)
            a = a.replace('${10}', red_option)
            a = a.replace('${11}', (self.dir_log + 'log_data_{}.txt').format(run))

            if skip_cal == True:
                a = a.replace('skip_cal=0', 'skip_cal=1')
            if skip_rec == True:
                a = a.replace('skip_rec=0', 'skip_rec=1')
            if skip_red == True:
                a = a.replace('skip_red=0', 'skip_red=1')

            job_filename = '{}/job_data_{}.sh'.format(dir_runs, run)
            f = open(job_filename, 'w')
            f.write(a)
            f.close()

            joblist.append(job_filename)

        return joblist

    # Create a bash script for each run : For MC samples
    def Create_Script_Mc(self,
                         table_dir='',
                         mctolvl0_option='', cal_option='', rec_option='', red_option='',
                         script_dir='', pbslog_dir='', dir_runs='',
                         skip_mctolvl0=False, skip_cal=False, skip_rec=False, skip_red=False):
        # set the default value
        if script_dir == '':
            script_dir = os.getcwd()
        if pbslog_dir == '':
            pbslog_dir = self.dir_log
        if dir_runs == '':
            dir_runs = self.dir_runs

        # Open base script 
        try:  # it's like 'if' sentence.
            f = open('do_mc.sh')
        except OSError as e:
            print(e)
        else:
            base = f.read()
            f.close()

        if self.runs[0] < 0 or self.runs[1] < 0:
            print('please specify the runs')
            exit(-1)

        # Create the job script directory 
        os.makedirs(dir_runs, exist_ok=True)

        # Create the output directories
        if skip_mctolvl0 == False:
            os.makedirs(self.dir_mctolvl0, exist_ok=True)
            os.makedirs(self.dir_tmp, exist_ok=True)
        if skip_cal == False:
            os.makedirs(self.dir_cal, exist_ok=True)
        if skip_rec == False:
            os.makedirs(self.dir_rec, exist_ok=True)
        if skip_red == False:
            os.makedirs(self.dir_red, exist_ok=True)

        os.makedirs(self.dir_log, exist_ok=True)

        joblist = []
        for run in range(self.runs[0], self.runs[1] + 1):
            if run in self.runs_exclude:
                continue

            dir_unzip = self.dir_tmp + '/unzip_run{}'.format(run)

            a = base  # base is 'do_mctolvl0_and_rec.sh'
            # Replace the variables in 'do_mctolvl0_and_rec.sh' file. 
            a = a.replace('/tmp', pbslog_dir)  # for log
            a = a.replace('${SCRIPT_DIR}', script_dir)  # for setup of the environment
            a = a.replace('${UNZIP_DIR}', dir_unzip)  # for unzip temporary
            a = a.replace('$1', self.dir_mc)  # raw data (End2End output for MC)
            a = a.replace('$2', (self.dir_mctolvl0 + self.file_mctolvl0).format(run))
            a = a.replace('$3', (self.dir_cal + self.file_cal).format(run))  # assigned by SetDefault(self)
            a = a.replace('$4', (self.dir_rec + self.file_rec).format(run))  # assigned by SetDefault(self)
            a = a.replace('$5', (self.dir_red + self.file_red).format(run))  # assigned by SetDefault(self)
            a = a.replace('$6', table_dir)
            a = a.replace('$7', str(-1*self.fill))  # by 'self' from McOp2022.py
            a = a.replace('$8', str(-1*self.subfill))  # by 'self' from McOp2022.py
            a = a.replace('$9', mctolvl0_option)
            a = a.replace('${10}', cal_option)
            a = a.replace('${11}', rec_option)
            a = a.replace('${12}', red_option)
            a = a.replace('${13}', (self.dir_log + 'log_mc_{}.txt').format(run))
            a = a.replace('${MC_ZIP_FILES}', self.file_mc.format(run))
            a = a.replace('${MCModel}', self.mcmodel)  # -k
            a = a.replace('${FILENUM}', str(run))  # -N
            a = a.replace('${COMBINE_SUBRUNS}', ('-f {} -l {}').format(self.subruns[0], self.subruns[1]))
            a = a.replace('${MC_FILENAME_FORMAT}', self.format_mc.format(run))

            if skip_mctolvl0 == True:
                a = a.replace('skip_mctolvl0=0', 'skip_mctolvl0=1')
            if skip_cal == True:
                a = a.replace('skip_cal=0', 'skip_cal=1')
            if skip_rec == True:
                a = a.replace('skip_rec=0', 'skip_rec=1')
            if skip_red == True:
                a = a.replace('skip_red=0', 'skip_red=1')

            job_filename = '{}/job_mc_{}_{}.sh'.format(dir_runs, run, self.tag_data)
            f = open(job_filename, 'w')
            f.write(a)
            f.close()

            joblist.append(job_filename)  # add the shell files of each runs to joblist.

        return joblist

    # Old function : Replaced it to the alias to Create_Script_Data or Create_Script_Mc
    def Create_Cal_Rec(self,
                       table_dir='', cal_option='', rec_option='',
                       script_dir='', pbslog_dir='', dir_runs=''):

        if self.flag_mc == False:
            joblist = self.Create_Script_Data(table_dir=table_dir, cal_option=cal_option, rec_option=rec_option,
                                              script_dir=script_dir, pbslog_dir=pbslog_dir, dir_runs=dir_runs,
                                              skip_cal=False, skip_rec=False, skip_red=True)
        else:
            joblist = self.Create_Script_Mc(table_dir=table_dir, cal_option=cal_option, rec_option=rec_option,
                                            script_dir=script_dir, pbslog_dir=pbslog_dir, dir_runs=dir_runs,
                                            skip_mctolvl0=True, skip_cal=False, skip_rec=False, skip_red=True)

        return joblist

    # Old function : Replaced it to the alias to Create_Script_Data or Create_Script_Mc
    def Create_Red(self,
                   red_option='',
                   script_dir='', pbslog_dir='', dir_runs=''):
        if self.flag_mc == False:
            joblist = self.Create_Script_Data(red_option=red_option,
                                               script_dir=script_dir, pbslog_dir=pbslog_dir, dir_runs=dir_runs,
                                               skip_cal=True, skip_rec=True, skip_red=False)
        else:
            joblist = self.Create_Script_Mc(red_option=red_option,
                                             script_dir=script_dir, pbslog_dir=pbslog_dir, dir_runs=dir_runs,
                                             skip_mctolvl0=True, skip_cal=True, skip_rec=True, skip_red=False)
        return joblist

    # Old function : Replaced it to the alias to Create_Script_Data or Create_Script_Mc
    def Create_Mctolvl0_Rec(self,
                            table_dir='', mctolvl0_option='', cal_option='', rec_option='',
                            script_dir='', pbslog_dir='', dir_runs=''):
        joblist = self.Create_Script_Mc(table_dir=table_dir, cal_option=cal_option, rec_option=rec_option,
                                        script_dir=script_dir, pbslog_dir=pbslog_dir, dir_runs=dir_runs,
                                        skip_mctolvl0=False, skip_cal=False, skip_rec=False, skip_red=True)
        return joblist
