'''
Example to submit jobs for Nagoya Clusters (lhcfs2 or crcpu0)

IMPORTANT)  
  This code works only with python3.  The default python version is 2.7 in both clusters. 
  Firstly you need to configure your terminal to use python3 as   
    source setup_lhcfs2.sh    or 
    source setup_crcpu0.sh 

Firstly this code generates the shell script for each run and saves them to 'runs' directory.
Then this code submit the job to the cluster system considering the number of running or waiting runs 
which should be less than the specified maximum number of jobs as max_jobs in JobManager

The directory path and filename of input files are automatically set.
The output parameters, directory path and file names, can be configured as you want 
while default are set automatically. 
For example, if you specified the base directory in data.Data_Fill8178_1( base ),
the output filename and path are 
    base/ -- cal/cal_runXXXXX.root 
          |- rec/rec_runXXXXX.root
          |- red/red_runXXXXX.root 

If you set your own path and filename, 
     a = data.Data_Fill8178_1('base')
     a.dir_rec = '/mnt/lhcfs5/data/tmp/test_rec/'  # must be a full path  
     a.file_rec = 'run{}_rec.root' # the run number is filled to {}
See Script/Nagoya/jobbase.py for other valuables 

Script files are generated base on the base script files:
    do_cal_rec.sh,  do_red.sh etc. 
The option for the application can be specified directly to the base script or 
you can set in this code like Create_Red(red_option = '--disable-arm2')
'''

import os
import sys

sys.path.append('../Script/Nagoya')
from jobmanager import JobManager
import DataOp2022 as data

output_base = '/mnt/lhcfs5/data/tmp/'  # set a full path
library_dir = os.getcwd() + '/../'
table_dir = library_dir + '/Tables/Op2022/'  # set the table dir

# Set the data sets which are processed 
data_list = []
data_list.append(data.Data_Fill8178_1(output_base + 'fill8178_1'))
data_list.append(data.Data_Fill8178_2(output_base + 'fill8178_2'))
data_list.append(data.Data_Fill8178_3(output_base + 'fill8178_3'))
data_list.append(data.Data_Fill8178_4(output_base + 'fill8178_4'))
# data_list.append( data.Data_Fill8177_1(output_base + 'fill8171_1') )
# data_list.append( data.Data_Fill8181_1(output_base + 'fill8181_1') )

# Generate the script files for runs ----- 
# Please comment/uncomment the lines which you would proceed 
job_list = []
for d in data_list:
    job_list += d.Create_Script_Data(table_dir=table_dir,
                                     cal_option='--disable-arm2 --hist --average-pedestal --ped',
                                     rec_option='--disable-arm2 -p full --level2',
                                     red_option='--disable-arm2 --enable-lvl2')

# Submit the jobs and 
manager = JobManager(cluster='crcpu0', max_jobs=24, logfile='./submit_log.txt')
manager.loop_jobs(job_list, time_step=60)
