import jobbase

#  Fill 8178 : detector ts center is on the beam center position in Op2022
#  Fill 3855 : the last and longest fill in Op 2022

class Mc_arm1_beamcenter(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfnas3/data/Simulation/Op2022/Data/End2End/v1/Arm1_Center/EPOSLHC/' # end2end output dir
        self.file_mc = 'e2e_a1c_EPOSLHC_{:06}_*.out.gz' # end2end output file
        self.format_mc = 'e2e_a1c_EPOSLHC_{:06}_%03d.out'
        self.file_raw = 'e2e_a1c_EPOSLHC_{:06d}.root'
        self.dir_common = output_base_directory
        self.fill = 8178
        self.subfill = 1
        self.runs = [1, 300]      # 10^5 collisions per run 
        self.subruns = [1, 100]   # 10^3 collisions per subrun 
        self.runs_exclude = []
        self.mcmodel = 'EPOSLHC'
        self.flag_mc = True
        self.SetDefault()