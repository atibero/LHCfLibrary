import jobbase

#  Fill 3855 : the last and longest fill in Op 2015

#  mu = 0.01 period 
class Mc_QGSJET204_wopipe_arm1_beamcenter(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfs3/data4/Simulation/pp13TeV/QGSJET2-4/End2End_nopipe/arm1'
        self.file_mc = 'qgs_13tevend2end.arm1.{:04d}*.out.bz2'
        self.format_mc = 'qgs_13tevend2end.arm1.{:04d}%02d.out'
        self.file_raw = 'e2e_arm1c_QGSJET204_{:04d}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [100, 1099]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'QGSJET2'
        self.flag_mc = True
        self.SetDefault()

class Mc_EPOSLHC_wopipe_arm1_beamcenter(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfs3/data4/Simulation/pp13TeV/EPOS-LHC/End2End_nopipe/arm1'
        self.file_mc = 'eposlhc_13tevend2end.arm1.{:04d}*.out.bz2'
        self.format_mc = 'eposlhc_13tevend2end.arm1.{:04d}%02d.out'
        self.file_raw = 'e2e_arm1c_EPOSLHC_{:04d}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [0, 499]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'EPOSLHC'
        self.flag_mc = True
        self.SetDefault()

class Mc_QGSJET204_wpipe_arm1_center(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfnas3/data/Simulation/Op2015/QGSJET2-4/End2End_wpipe/arm1'
        self.file_mc = 'qgs_13tevend2end.arm1.{:04d}*.out.bz2'
        self.format_mc = 'qgs_13tevend2end.arm1.{:04d}%02d.out'
        self.file_raw = 'e2e_arm1cw_QGSJET204_{:04d}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [0, 99]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'QGSJET2'
        self.flag_mc = True
        self.SetDefault()

class Mc_QGSJET204_wopipe_arm1_5mmhigh(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfnas3/data/Simulation/Op2015/QGSJET2-4/End2End_nopipe_5mmhigh/arm1'
        self.file_mc = 'qgs_13tevend2end.arm1_5mmhigh.{:04d}*.out.bz2'
        self.format_mc = 'qgs_13tevend2end.arm1_5mmhigh.{:04d}%02d.out.bz2'
        self.file_raw = 'e2e_arm1h_QGSJET204_{:04d}.root'
        self.dir_common = output_base_directory
        self.fill = 3851
        self.subfill = 1
        self.runs = [1100, 2099]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'QGSJET2'
        self.flag_mc = True
        self.SetDefault()

class Mc_PurePi0_arm1_type1(jobbase.DataBaseClass):
    '''Pure Pi0 MC Arm1 Type1'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfs2/data3/menjo/Simulation/pp13TeVPi0/PurePi0_Arm1_Type1/E2Eoutputs/' # end2end output dir
        self.file_mc = 'pi0type1_run{:03d}*.out.gz' # end2end output file
        self.format_mc = 'pi0type1_run{:03d}%02d.out'
        self.file_raw = 'e2e_arm1_pi0type1_{:03d}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [0, 120]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'EPOSLHC'
        self.flag_mc = True
        self.tag_data = 'purepi0_type1'
        self.SetDefault()

class Mc_PurePi0_arm1_type2ts(jobbase.DataBaseClass):
    '''Pure Pi0 MC Arm1 Type2 TS'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfs2/data3/menjo/Simulation/pp13TeVPi0/PurePi0_Arm1_Type2TS/E2Eoutputs/' # end2end output dir
        self.file_mc = 'pi0type2ts_run{:03d}*.out.gz' # end2end output file
        self.format_mc = 'pi0type2ts_run{:03d}%02d.out'
        self.file_raw = 'e2e_arm1_pi0type2ts_{:03d}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [0, 120]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'EPOSLHC'
        self.flag_mc = True
        self.tag_data = 'purepi0_type2ts'
        self.SetDefault()

class Mc_PurePi0_arm1_type2tl(jobbase.DataBaseClass):
    '''Pure Pi0 MC Arm1 Type2 TL'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_mc = '/mnt/lhcfs2/data3/menjo/Simulation/pp13TeVPi0/PurePi0_Arm1_Type2TL/E2Eoutputs/' # end2end output dir
        self.file_mc = 'pi0type2tl_run{:03d}*.out.gz' # end2end output file
        self.format_mc = 'pi0type2tl_run{:03d}%02d.out'
        self.file_raw = 'e2e_arm1_pi0type2tl_{:03d}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [0, 103]
        self.subruns = [0, 99]
        self.runs_exclude = []
        self.mcmodel = 'EPOSLHC'
        self.flag_mc = True
        self.tag_data = 'purepi0_type2tl'
        self.SetDefault()
