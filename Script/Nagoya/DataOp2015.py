import jobbase

#
#  Fill 3855 : the last and longest fill in Op 2022

#  mu = 0.01 period 
class Data_Fill3855_1(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 1
        self.runs = [44299, 44472]
        self.runs_exclude = [44356, 44413]
        self.SetDefault()
        
#  mu = 0.03 period 
class Data_Fill3855_2(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3855
        self.subfill = 2
        self.runs = [44482, 45106]
        self.runs_exclude = [44536, 44592, 44656, 44716, 44777, 44830, 44892, 44947, 45010, 45069, 45090, 45091]
        self.SetDefault()

#
#  Fill 3851 
#
class Data_Fill3851_1(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3851
        self.subfill = 1
        self.runs = [43162, 43317]
        self.runs_exclude = [43197, 43253]
        self.SetDefault()

class Data_Fill3851_2(jobbase.DataBaseClass):
    '''5mm Higher Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3851
        self.subfill = 2
        self.runs = [43321, 43598]
        self.runs_exclude = [43345, 43518, 43570]
        self.SetDefault()

class Data_Fill3851_3(jobbase.DataBaseClass):
    '''5mm Higher Position, High Gain'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3851
        self.subfill = 3
        self.runs = [43611, 43670]
        self.runs_exclude = [43621, 43631, 43641, 43651, 43661]
        self.SetDefault()

class Data_Fill3851_4(jobbase.DataBaseClass):
    '''Beam Center Position, High Gain'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3851
        self.subfill = 4
        self.runs = [43677, 43742]
        self.runs_exclude = [43687, 43698, 43710, 43721, 43732]
        self.SetDefault()

class Data_Fill3851_5(jobbase.DataBaseClass):
    '''Beam Center Position, Normal Gain'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs3/data1/DATA/Op2015/conv_files/'
        self.file_raw = 'runraw{}.root'
        self.file_cal = 'cal_run{}.root'
        self.file_rec = 'rec_run{}.root'
        self.file_red = 'red_run{}.root'
        self.dir_common = output_base_directory
        self.fill = 3851
        self.subfill = 5
        self.runs = [43769, 43829]
        self.runs_exclude = [43779]
        self.SetDefault()