import jobbase

#
#  Fill 8177 : Short Fill, mainly for DAQ commissioning 
#  
class Data_Fill8177_1(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs5/data/Op2022/Data/'
        self.file_raw = 'run{}.root'
        self.dir_common = output_base_directory
        self.fill = 8177
        self.subfill = 1
        self.runs = [80246, 80252]
        self.runs_exclude = []
        self.SetDefault()
        
#
#  Fill 8178 : Very long fill 
#  
class Data_Fill8178_1(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs5/data/Op2022/Data/'
        self.file_raw = 'run{}.root'
        self.dir_common = output_base_directory
        self.fill = 8178
        self.subfill = 1
        self.runs = [80262, 80346]
        self.runs_exclude = [80267, 80342, 80343, 80347, 80348, 80349, 80350]
        self.SetDefault()

class Data_Fill8178_2(jobbase.DataBaseClass):
    '''5mm Higher Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs5/data/Op2022/Data/'
        self.file_raw = 'run{}.root'
        self.dir_common = output_base_directory
        self.fill = 8178
        self.subfill = 2
        self.runs = [80351, 80431]
        self.runs_exclude = [80373, 80374, 80375, 80376]
        self.SetDefault()

class Data_Fill8178_3(jobbase.DataBaseClass):
    '''Beam Center Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs5/data/Op2022/Data/'
        self.file_raw = 'run{}.root'
        self.dir_common = output_base_directory
        self.fill = 8178
        self.subfill = 3
        self.runs = [80434, 80520]
        self.runs_exclude = [80466, 80467, 80468, 80469, 80470]
        self.SetDefault()


class Data_Fill8178_4(jobbase.DataBaseClass):
    '''5mm Higher Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs5/data/Op2022/Data/'
        self.file_raw = 'run{}.root'
        self.dir_common = output_base_directory
        self.fill = 8178
        self.subfill = 4
        self.runs = [80523, 80626]
        self.runs_exclude = [80531, 80532, 80533, 80534, 80535, 80612, 80613, 80614, 80615, 80616]
        self.SetDefault()

#
# Short fill after the long fill
#
class Data_Fill8181_1(jobbase.DataBaseClass):
    '''5mm Higher Position'''
    def __init__(self, output_base_directory):
        super().__init__()
        self.dir_raw = '/mnt/lhcfs5/data/Op2022/Data/'
        self.file_raw = 'run{}.root'
        self.dir_common = output_base_directory
        self.fill = 8181
        self.subfill = 1
        self.runs = [80635, 80646]
        self.runs_exclude = []
        self.SetDefault()

