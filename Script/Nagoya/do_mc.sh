#!/bin/bash
#PBS -q one_week
#PBS -j oe
#PBS -l nodes=1:ppn=1
#PBS -o /tmp/joblog_${PBS_JOBID}.txt

#usage="Usage: $0 build_dir input_file middle_file output_file fitmode table_dir fill subfill glop_opt option log_file"
#if [ $# -ne 11 ]; then
#    echo $usage
#    exit -1
#fi

exec 1>$log_file
exec 2>&1

source ${SCRIPT_DIR}/setup_common.sh
if [[ "$HOSTNAME" =~ "crcpu"  ]]; then
	source ${SCRIPT_DIR}/setup_crcpu.sh
elif [[ "$HOSTNAME" =~ "lhcfs" ]] || [[ "$HOSTNAME" =~ "lhcfcl" ]]; then
	source ${SCRIPT_DIR}/setup_lhcfs2.sh
fi

build_dir=${BUILD_DIR}
input_dir=$1
mctolevel0_file=$2
cal_file=$3
rec_file=$4
red_file=$5
table_dir=$6
fill=$7
subfill=$8
option_mctolvl0="$9"
option_cal="${10}"
option_rec="${11}"
option_red="${12}"
log_file=${13}
mcmodel=${MCModel}
filenum=${FILENUM}
mc_format="${MC_FILENAME_FORMAT}"
combine_subruns="${COMBINE_SUBRUNS}"
skip_mctolvl0=0
skip_cal=0
skip_rec=0
skip_red=0

cd $build_dir
echo $PWD

exec 1>$log_file
exec 2>&1

echo "START"
date
echo "INPUT DIR = ${input_dir}" 
echo "MCTOLEVEL0 FILE = ${mctolevel0_file}" 
echo "REC FILE = ${rec_file}" 
echo "OPTION_MCTOLVL0 : ${option_mctolvl0}"
echo "OPTION_REC : ${option_rec}"

echo "$build_dir/bin/ConvertMCtoLvl0 -i ${UNZIP_DIR} -o $mctolevel0_file -n $mc_format -t $table_dir -F $fill -S $subfill -k $mcmodel $combine_subruns -N $filenum $option_mctolvl0"
echo "$build_dir/bin/Calibrate -i $mctolevel0_file -o $cal_file -t $table_dir -F $fill -S $subfill $option_cal"
echo "$build_dir/bin/Reconstruct -i $cal_file -o $rec_file -t $table_dir -F $fill -S $subfill $option_rec"
echo "$build_dir/bin/Reduction -i $input_file -o $output_file  -F $fill -S $subfill --disable-bptx-cut --enable-true  $option_red"

if [[ $skip_mctolvl0 -eq 0 ]]; then
  # unzip E2E files
  mkdir -p ${UNZIP_DIR}
  cp ${input_dir}/${MC_ZIP_FILES} ${UNZIP_DIR}
  if [[ "${MC_ZIP_FILES}" =~ .gz ]]; then
     echo "unzip .gz files"
     gunzip ${UNZIP_DIR}/${MC_ZIP_FILES}
  elif [[ "${MC_ZIP_FILES}" =~ .bz2 ]]; then
     echo "unzip .bz2 files"
     bunzip2 ${UNZIP_DIR}/${MC_ZIP_FILES}
  fi

  # replace overflow values(*****) to dummy values
  for file in $(ls -A -1 ${UNZIP_DIR}/*.out); do
     echo $file
     mv $file ${UNZIP_DIR}/buf.txt
     sed -e "s/\*\*\*\*\*/1/g" ${UNZIP_DIR}/buf.txt > $file
  done

  echo "== ConvertMCtoLvl0 =="
  time $build_dir/bin/ConvertMCtoLvl0 -i ${UNZIP_DIR} -o $mctolevel0_file -n $mc_format -t $table_dir \
        -F $fill -S $subfill -k $mcmodel $combine_subruns -N $filenum $option_mctolvl0
  rm -r ${UNZIP_DIR}
fi

if [[ $skip_cal -eq 0 ]]; then
   echo "== Calibrate =="
   time $build_dir/bin/Calibrate -i $mctolevel0_file -o $cal_file -t $table_dir -F $fill -S $subfill \
                                $option_cal
fi

if [[ $skip_rec -eq 0 ]]; then
   echo "== Reconstruct =="
   time $build_dir/bin/Reconstruct -i $cal_file -o $rec_file -t $table_dir -F $fill -S $subfill $option_rec
fi

if [[ $skip_red -eq 0 ]]; then
   echo "== Reduction =="
   time $build_dir/bin/Reduction -i $rec_file -o $red_file  -F $fill -S $subfill \
                                 --disable-bptx-cut --enable-true  $option_red
fi

echo "END"
date
