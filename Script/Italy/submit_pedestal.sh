#!/bin/bash

usage="Usage: $0 data|data_p0.03|data_5mm|sps_2022|lhc_2022"

if [ $# -ne 1 ]; then
    echo $usage
    exit -1
fi

# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
option="--disable-arm1 --histogram"
queue=1
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables
input_dir=""
data_out=""

#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=1
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables
    fi
#----------------- Eugenio's custom configuration -----------------#
elif [ $USER = berti ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	main_dir=/home/berti/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=1
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
	#Temporary test
    	main_dir=/opt/exp_software/lhcf/berti/LHCfLibrary
   	build_dir=$main_dir/build
   	##############
    fi
fi
#------------------------------------------------------------------#

if [ $1 = "data" ]; then

    isMC=false
    fill=3855
    pileup=0.01
    dir_name=data
    name_short=dt1
    input_dir=/storage/gpfs_data/lhcf/DATA/LHC2015
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
    table_dir=$main_dir/Tables/Op2015
    first=44299; last=44472; 
    
elif [ $1 = "data_p0.03" ]; then

    isMC=false
    fill=3855
    pileup=0.03
    dir_name=data3
    name_short=dt3
    input_dir=/storage/gpfs_data/lhcf/DATA/LHC2015
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
    table_dir=$main_dir/Tables/Op2015
    first=44482; last=45106
    
elif [ $1 = "data_5mm" ]; then

    isMC=false
    fill=3851
    pileup=0.01
    dir_name=data5mm
    name_short=dt5
    input_dir=/storage/gpfs_data/lhcf/DATA/LHC2015
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
    table_dir=$main_dir/Tables/Op2015
    first=43321; last=43598
 
elif [ $1 = "sps_2022" ]; then

    isMC=false
    fill=3851
    pileup=0.01
    dir_name=.
    name_short=sps
    data_out=/storage/gpfs_data/lhcf/reconstructed/SPS2022
    input_dir=/storage/gpfs_data/lhcf/DATA/SPS2022
    table_dir=$main_dir/Tables/SPS2022
    #first=90046; last=90220 #Standard Arm2
    first=90606; last=90634 #Rotated Arm2

elif [ $1 = "lhc_2022" ]; then

    isMC=false
    fill=3851
    pileup=0.01
    dir_name=.
    name_short=lhc
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2022
    input_dir=/storage/gpfs_data/lhcf/DATA/LHC2022
    table_dir=$main_dir/Tables/LHC2022
    first=80246; last=80252

else

    echo $usage
    exit -1

fi

if [ ! -d $input_dir ]; then
    echo "Input directory doesn't exist!"
    exit -1
fi

# Set output directory
output_dir=$data_out/$dir_name/pedestal
mkdir -p $output_dir

log_dir=$output_dir/log
mkdir -p $log_dir

sub_dir=$output_dir/sub
htc_dir=$output_dir/htc
if [ $farm = "CNAF" ]; then
    mkdir -p $sub_dir
    mkdir -p $htc_dir
fi

# Run loop
irun=$first
while [ $irun -le $last ]
do
    # Check if script must be stopped
    switch=$(head -1 $build_dir/switch.inp)
    if [ $switch -eq 0 ]; then
	break
    fi
    
    current_run=$(printf %05d $irun)
    input_file=$input_dir/run$current_run.root
    output_file=$output_dir/ped_run$current_run.root

    log_file=$log_dir/ped_run$current_run.log
    htc_file=$htc_dir/ped_run$current_run.log
    sub_file=$sub_dir/ped_run$current_run.sub

    if [ $farm = "FARM" ]; then
	if [ $(bjobs | grep "P$name_short$current_run" -c) -ne 0 ]; then
	    echo "Run $current_run already submitted!"
	    irun=$(($irun+1))
	    continue
	elif [ ! -f $input_file ]; then
	    echo "File \"$input_file\" does not exist!"
	    irun=$(($irun+1))
	    continue
	elif [ -f $output_file ]; then
	    echo "File \"$output_file\" already exists!"
	    if [[ ! -f $log_file ]]; then
		echo "...but the log file does not exist"
		echo "Reprocessing \"$output_file\""
	    elif [[ $(tail -n1 $log_file | grep -c "Closing files... Done.") -lt 1 ]]; then
		echo "...but it had abnormal termination!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    else
		irun=$(($irun+1))
		continue
	    fi
	fi
    elif [ $farm = "CNAF" ]; then
	# !!! TODO for HTCondor !!!
	#if [ $(bjobs | grep "R$name_short$current_run" -c) -ne 0 ]; then
	#    echo "Run $current_run already submitted!"
	#    irun=$(($irun+1))
	#    continue
	#fi
	if [ ! -f $input_file ]; then
	    echo "File \"$input_file\" does not exist!"
	    irun=$(($irun+1))
	    continue
	elif [ -f $output_file ]; then
	    echo "File \"$output_file\" already exists!"
	    if [[ ! -f $log_file ]]; then
		echo "...but the log file does not exist"
		echo "Reprocessing \"$output_file\""
	    elif [[ $(tail -n1 $log_file | grep -c "Closing files... Done.") -lt 1 ]]; then
		echo "...but it had abnormal termination!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    else
		irun=$(($irun+1))
		continue
	    fi
	fi
    fi

    # Check number of running jobs
    if [ $farm = "FARM" ]; then
	njobs=$(bjobs -u $USER | grep $USER | grep -c "R")
    elif [ $farm = "CNAF" ]; then
	nrunning=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $2}') # running jobs
	if [ -z $nrunning ]; then
	    nrunning=0
	fi
	nidle=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $3}') # idle jobs
	if [ -z $nidle ]; then
	    nidle=0
	fi
	nheld=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $4}') # held jobs
	if [ -z $nheld ]; then
	    nheld=0
	fi
	njobs=$(($nrunning+$nidle+$nheld))
    fi
    # Set desired maximum number of running jobs
    maximum=$(tail -1 $build_dir/switch.inp)
    # Set submit time interval
    tsleep=$(tail -2 $build_dir/switch.inp | head -1)

    # Submit job
    if [ $njobs -lt $maximum ]; then
	echo "Run $irun/$last, running jobs = $njobs, max jobs = $maximum"

	# Submit
	if [ $farm = "FARM" ]; then

	    bsub -J P$name_short$current_run -q $queue -o $log_file "$build_dir/bin/CreatePedestalTree -i $input_file -o $output_file -t $table_dir $option"
	    ret_val=0 # TODO?

	elif [ $farm = "CNAF" ]; then
	    cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_ped.sh
arguments = "$build_dir $input_file $output_file $table_dir '$option' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue $queue
EOF
	    condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
	    ret_val=$?
	fi
	
	# Check if run is successfully submitted before going to next one
	if [ $ret_val = 0 ]; then
	    irun=$(($irun+1))
	fi
	usleep 100000

    fi

    echo "wait more $tsleep seconds..."
    sleep $tsleep
done
