#!/bin/bash
usage="Usage: $0 work_dir build_dir input_dir begin end output_file table_dir fill log_file pwd_dir disable_smearing mhlogfile"
if [ $# -ne 12 ]; then
    echo $usage
    exit -1
fi

work_dir=$1
build_dir=$2
input_dir=$3
begin=$4
end=$5
output_file=$6
table_dir=$7
fill=$8
log_file=$9
pwd_dir=${10}
disable_smearing=${11}
mhlogfile=${12}

exec 1>$log_file
exec 2>&1

cd $work_dir
$build_dir/gunzip_end2end.sh $input_dir $begin $end $work_dir
$build_dir/bin/ConvertMCtoLvl0 -i . -f $begin -l $end -o $output_file --arm2 -t $table_dir -F $fill $disable_smearing $mhlogfile
$build_dir/rm_end2end.sh $work_dir $begin $end
cd $pwd_dir
rm -rf $work_dir
