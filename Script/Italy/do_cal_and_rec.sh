#!/bin/bash

usage="Usage: $0 build_dir input_file middle_file output_file fitmode table_dir fill pileup glop_opt option log_file"
if [ $# -ne 11 ]; then
    echo $usage
    exit -1
fi

build_dir=$1
input_file=$2
middle_file=$3
output_file=$4
fitmode=$5
table_dir=$6
fill=$7
pileup=$8
globop="$9"
option="${10}"
log_file=${11}

exec 1>$log_file
exec 2>&1

$build_dir/bin/Calibrate -i $input_file -o $middle_file -t $table_dir -F $fill -S $pileup $globop
$build_dir/bin/Reconstruct -i $middle_file -o $output_file -p $fitmode -t $table_dir -F $fill -S $pileup $globop $option
rm $middle_file
