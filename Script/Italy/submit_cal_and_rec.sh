#!/bin/bash

usage="Usage: $0 data|data_p0.03|data_5mm|qgs|epos|dpm|qgs_5mm|qgs_5mm_mh_ideal|qgs_5mm_mh_observed|qgs_5mm_bpi|epos_5mm|epos_5mm_mh_ideal|epos_5mm_mh_observed|flat_small_dpm_5mm|flat_small_qgs_5mm|flat_large_dpm_5mm|flat_large_qgs_5mm|pure-t1|pure-t2s|pure-t2l|eta_qgs|eta_epos"

if [ $# -ne 1 ]; then
    echo $usage
    exit -1
fi

########### Options #############

# if both do_cal and do_rec are true, calibrated file is removed at the end of the job
do_cal=true # do Calibration
do_rec=true # do Reconstruction

truepos=false # use reconstructed position
#truepos=true # use true position (only available for MC)

fitmode=full # full position fit
#fitmode=fast # barycentre method
#fitmode=none # only reconstruct energy (needs input files reconstructed with --level2cal)

save_charge=false # don't save charge measurement in silicon
#save_charge=true # save charge measurement in silicon

smearing=true  #only to set input/output directories
#smearing=false  #only to set input/output directories

recname=$fitmode #used for the path of the output directory

globop="--disable-arm1" # both calibration and reconstruction
#option="--level2cal" # only reconstruction
option="--level2cal --disable-neutron" # only reconstruction

#################################

if [ "$truepos" = true ]; then
    option="$option --true-coor"
fi
if [ "$save_charge" = true ]; then
    option="$option --levelc"
fi
if [ "$truepos" = true ]; then
    recname="trueposition"
fi

if [ $do_cal != true ] && [ $do_rec != true ]; then
    echo "Both calibration and reconstruction are disabled!"
    exit -1
fi

if [ $fitmode = "none" ]; then
    do_cal=false
fi

# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
queue=
data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015
data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables/Op2015
if [ $farm = "FARM" ]; then
	input_dir=/wizard/bombur/data/LHCf/DATA/LHC2015
elif [ $farm = "CNAF" ]; then
	input_dir=/storage/gpfs_data/lhcf/DATA/LHC2015
fi

#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	data_in=/wizard/bombur/data/LHCf/reconstructed/LHC2015
	data_out=/wizard/bombur/data/LHCf/reconstructed/LHC2015
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables/Op2015
    elif [ $farm = "CNAF" ]; then
	queue=
	#data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/fixed_leakin
	#data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/fixed_leakin
	#main_dir=/opt/exp_software/lhcf/LHCfLibrary
	data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Pi0_ICRC
	data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Pi0_ICRC
	main_dir=/opt/exp_software/lhcf/tiberio/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	#table_dir=$main_dir/Tables/Op2015
	table_dir=/opt/exp_software/lhcf/LHCfLibrary/Tables/Op2015
    fi
#----------------- Eugenio's custom configuration -----------------#
elif [ $USER = berti ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	data_in=/home/berti/lhcf_data
	data_out=/home/berti/lhcf_data
	main_dir=/home/berti/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=
	data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    fi
fi
#------------------------------------------------------------------#


if [ $1 = "data" ]; then

    fill=3855
    pileup=1
    dir_name=data
    name_short=dt1
    first=44299; last=44472; 
    
elif [ $1 = "data_p0.03" ]; then

    fill=3855
    pileup=2
    dir_name=data
    name_short=dt3
    first=44482; last=45106
    
elif [ $1 = "data_5mm" ]; then

    fill=3851
    pileup=1
    dir_name=data
    name_short=dt5
    first=43321; last=43598
   
elif [ $1 = "qgs" ]; then

    # QGSJETII-04 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=qgsjetII04
    name_short=qgs
    first=101; last=1100
    
elif [ $1 = "epos" ]; then

    # EPOS-LHC (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=eposlhc
    name_short=epo
    first=1; last=500
    
elif [ $1 = "dpm" ]; then

    # DPMJET 3.06 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=dpmjetIII03
    name_short=dpm
    first=1; last=200
   
elif [ $1 = "qgs_5mm" ]; then

    # QGSJETII-04 - 5mm high (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=qgsjetII04_5mm_high
    name_short=qg5
    first=1101; last=2100
    
elif [ $1 = "qgs_5mm_mh_ideal" ]; then

    # QGSJETII-04 - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=qgsjetII04_5mm_high_multihit_ideal
    name_short=qg5mhi
    first=1101; last=2100
    
elif [ $1 = "qgs_5mm_mh_observed" ]; then

    # QGSJETII-04 - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=qgsjetII04_5mm_high_multihit_observed
    name_short=qg5mho
    first=1101; last=2100
	
elif [ $1 = "qgs_5mm_bpi" ]; then

    # QGSJETII-04 - 5mm high (with beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=qgsjetII04_5mm_high_bpi
    mcshort=qg5n
    first=1101; last=1200

elif [ $1 = "epos_5mm" ]; then

    # EPOS-LHC - 5mm high (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=eposlhc_5mm_high
    name_short=ep5
    first=1001; last=2000
     
elif [ $1 = "epos_5mm_mh_ideal" ]; then

    # EPOS-LHC - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=eposlhc_5mm_high_multihit_ideal
    name_short=ep5mhi
    first=1001; last=2000
    
elif [ $1 = "epos_5mm_mh_observed" ]; then

    # EPOS-LHC - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=eposlhc_5mm_high_multihit_observed
    name_short=ep5mho
    first=1001; last=2000
 
elif [ $1 = "flat_small_dpm_5mm" ]; then

    fill=-3851
    pileup=1
    dir_name=flat_dpm_small
    name_short=fds5
    first=1; last=160
    option="$option -Y -25"
 
elif [ $1 = "flat_large_dpm_5mm" ]; then

    fill=-3851
    pileup=1
    dir_name=flat_dpm_large
    name_short=fdl5
    first=501; last=660
    option="$option -Y -25"
   
elif [ $1 = "flat_small_qgs_5mm" ]; then

    fill=-3851
    pileup=1
    dir_name=flat_qgs_small
    name_short=fqs5
    first=1; last=100
    option="$option -Y -25"
  
elif [ $1 = "flat_large_qgs_5mm" ]; then

    fill=-3851
    pileup=1
    dir_name=flat_qgs_large
    name_short=fql5
    first=501; last=600
    option="$option -Y -25"

elif [ $1 = "pure-t1" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=pure_pi0_t1
    name_short=pt1
    first=1; last=1000
    
elif [ $1 = "pure-t2s" ]; then
    
    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=pure_pi0_t2s
    name_short=p2s
    first=1; last=1000
    
elif [ $1 = "pure-t2l" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=pure_pi0_t2l
    name_short=p2l
    first=1; last=1000

elif [ $1 = "eta_qgs" ]; then

    # eta-like QGSJETII-04 (without beam pipe interaction)
    data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Eta
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Eta
    fill=3855
    pileup=1
    dir_name=qgsjetII04
    name_short=qgs
    first=1; last=800
    
elif [ $1 = "eta_epos" ]; then

    # eta-like EPOS-LHC (without beam pipe interaction)
    data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Eta
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Eta
    fill=3855
    pileup=1
    dir_name=eposlhc
    name_short=epo
    first=1; last=500
    
else
    echo $usage
    exit -1
fi

# Check run interval
if [ $last -lt $first ]; then
    echo "Last run (=$last) less than first run (=$first)!"
    exit -1
fi

# Set input directory
if [[ $dir_name != *"data"* ]]; then
    if [ $smearing = false ] ; then
    	dir_name=${dir_name}_nosmear
    fi
    input_dir=$data_in/$dir_name/conv
fi
if [ ! -d $input_dir ]; then
    echo "Input directory doesn't exist!"
    exit -1
fi
inrec_dir=$data_out/$dir_name/full # only meaningful for "none" fitmode

# Set output directory
outcal_dir=$data_out/$dir_name/calib
outrec_dir=$data_out/$dir_name/$recname
if [ $do_rec != true ]; then
    output_dir=$outcal_dir
else
    output_dir=$outrec_dir
fi
log_dir=$output_dir/log
sub_dir=$output_dir/sub
htc_dir=$output_dir/htc
mkdir -p $outcal_dir
mkdir -p $outrec_dir
mkdir -p $log_dir
if [ $farm = "CNAF" ]; then
    mkdir -p $sub_dir
    mkdir -p $htc_dir
fi

echo -e "____________________\n"
echo    " FIT MODE = $fitmode"
echo    " SMEARING = $smearing"
echo    " OPTION   = $option"
echo -e "____________________\n"

# Run loop
irun=$first
while [ $irun -le $last ]
do
    # Check if script must be stopped
    switch=$(head -1 $build_dir/switch.inp)
    if [ $switch -eq 0 ]; then
	break
    fi
    
    current_run=$(printf %05d $irun)
    incal_file=$input_dir/run$current_run.root
    outcal_file=$outcal_dir/cal_run$current_run.root
    if [ $fitmode = "none" ]; then
	inrec_file=$inrec_dir/rec_run$current_run.root
    else
	inrec_file=$outcal_file
    fi
    outrec_file=$outrec_dir/rec_run$current_run.root
    if [ $do_rec != true ]; then
	log_file=$log_dir/cal_run$current_run.log
	htc_file=$htc_dir/cal_run$current_run.log
	sub_file=$sub_dir/cal_run$current_run.sub
    else
	log_file=$log_dir/rec_run$current_run.log
	htc_file=$htc_dir/rec_run$current_run.log
	sub_file=$sub_dir/rec_run$current_run.sub
    fi
	
    # Check if run has to be submitted or not
    if [ $do_cal = true ]; then
	input_file=$incal_file
    else
	input_file=$inrec_file
    fi
    if [ $do_rec != true ]; then
	output_file=$outcal_file
    else
	output_file=$outrec_file
    fi
    if [ $farm = "FARM" ]; then
	if [ $(bjobs | grep "R$name_short$current_run" -c) -ne 0 ]; then
	    echo "Run $current_run already submitted!"
	    irun=$(($irun+1))
	    continue
	elif [ ! -f $input_file ]; then
	    echo "File \"$input_file\" does not exist!"
	    irun=$(($irun+1))
	    continue
	elif [ -f $output_file ]; then
	    echo "File \"$output_file\" already exists!"
	    if [[ ! -f $log_file ]]; then
		echo "...but the log file does not exist"
		echo "Reprocessing \"$output_file\""
	    elif [[ $(tail -n1 $log_file | grep -c "Closing files... Done.") -lt 1 ]]; then
		echo "...but it had abnormal termination!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    else
		irun=$(($irun+1))
		continue
	    fi
	fi
    elif [ $farm = "CNAF" ]; then
	# !!! TODO for HTCondor !!!
	#if [ $(bjobs | grep "R$name_short$current_run" -c) -ne 0 ]; then
	#    echo "Run $current_run already submitted!"
	#    irun=$(($irun+1))
	#    continue
	#fi
	if [ ! -f $input_file ]; then
	    echo "File \"$input_file\" does not exist!"
	    irun=$(($irun+1))
	    continue
	elif [ -f $output_file ]; then
	    echo "File \"$output_file\" already exists!"
	    if [[ ! -f $log_file ]]; then
		echo "...but the log file does not exist"
		echo "Reprocessing \"$output_file\""
	    elif [[ $(grep -cw "Killed" $log_file) -ge 1 ]]; then
		echo "...but it was killed!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    elif [[ $(tail -n1 $log_file | grep -c "Closing files... Done.") -lt 1 ]]; then
		echo "...but it had abnormal termination!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    else
		irun=$(($irun+1))
		continue
	    fi
	fi
    fi

    # Check number of running jobs
    if [ $farm = "FARM" ]; then
	njobs=$(bjobs -u $USER | grep $USER | grep -c "R")
    elif [ $farm = "CNAF" ]; then
	nrunning=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $2}') # running jobs
	if [ -z $nrunning ]; then
	    nrunning=0
	fi
	nidle=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $3}') # idle jobs
	if [ -z $nidle ]; then
	    nidle=0
	fi
	nheld=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $4}') # held jobs
	if [ -z $nheld ]; then
	    nheld=0
	fi
	njobs=$(($nrunning+$nidle+$nheld))
    fi
    # Set desired maximum number of running jobs
    maximum=$(tail -1 $build_dir/switch.inp)
    # Set submit time interval
    tsleep=$(tail -2 $build_dir/switch.inp | head -1)

    # Submit job
    if [ $njobs -lt $maximum ]; then
	echo "Run $irun/$last, running jobs = $njobs, max jobs = $maximum"

	# Submit
	if [ $farm = "FARM" ]; then
	    if [ $do_cal = true ] && [ $do_rec = true ]; then
		bsub -J R$name_short$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" "$build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop ; $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -S $pileup $globop $option ; rm $outcal_file"
	        #bsub -J R$name_short$current_run -q $queue -o $log_file "$build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop ; $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -S $pileup $globop $option ; rm $outcal_file"
	    elif [ $do_cal = true ]; then
		bsub -J G$name_short$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" $build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop
		#bsub -J G$name_short$current_run -q $queue -o $log_file $build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop
	    elif [ $do_rec = true ]; then
		bsub -J R$name_short$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -S $pileup $globop $option
		#bsub -J R$name_short$current_run -q $queue -o $log_file $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -S $pileup $globop $option
	    fi
	    ret_val=0 # TODO?

	elif [ $farm = "CNAF" ]; then
	    if [ $do_cal = true ] && [ $do_rec = true ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_cal_and_rec.sh
arguments = "$build_dir $incal_file $outcal_file $outrec_file $fitmode $table_dir $fill $pileup '$globop' '$option' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	    elif [ $do_cal = true ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_cal.sh
arguments = "$build_dir $incal_file $outcal_file $table_dir $fill '$globop' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	    elif [ $do_rec = true ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_rec.sh
arguments = "$build_dir $inrec_file $outrec_file $fitmode $table_dir $fill $pileup '$globop' '$option' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	    fi
	    condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
	    ret_val=$?
	fi
	
	# Check if run is successfully submitted before going to next one
	if [ $ret_val = 0 ]; then
	    irun=$(($irun+1))
	fi
	usleep 100000
    fi

    echo "wait more $tsleep seconds..."
    sleep $tsleep
done
