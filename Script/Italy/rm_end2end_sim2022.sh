#!/bin/bash

usage="Usage: $0 <indir> <first> <last> <filenumber> <mcname>"

if [ $# -ne 5 ]; then
    echo $usage
    exit -1
fi

input_dir=$1
first=$2
last=$3
filenumber=$4
mcname=$5

for i in $(seq $first $last)
do
    name=$(printf "e2e_a2c_%s_%06d_%03d.out" $mcname $filenumber $i)
    echo "rm -f $input_dir/$name"
    rm -f $input_dir/$name
done
