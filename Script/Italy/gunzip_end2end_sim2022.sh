#!/bin/bash

usage="Usage: $0 <indir> <first> <last> <filenumber> <mcname> <outdir>"

if [ $# -ne 6 ]; then
    echo $usage
    exit -1
fi

input_dir=$1
first=$2
last=$3
filenumber=$4
mcname=$5
output_dir=$6

for i in $(seq $first $last)
do
    name5=$(printf "end2end.arm2.%05d.out" $i)
    name6=$(printf "end2end.%06d.out" $i)
    name7=$(printf "end2end.%07d.out" $i)
	name_newsim=$(printf "e2e_a2c_%s_%06d_%03d.out" $mcname $filenumber $i)
    nameout=$(printf "end2end.%06d.out" $i)
    for name in $name5
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
    nameout=$(printf "end2end.%06d.out" $i)
    for name in $name6
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
    nameout=$(printf "end2end.%07d.out" $i)
    for name in $name7
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
	nameout=$(printf "e2e_a2c_%s_%06d_%03d.out" $mcname $filenumber $i)
    for name in $name_newsim
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
done
