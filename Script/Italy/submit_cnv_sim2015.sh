#!/bin/bash

usage="Usage: $0 qgs|epos|dpm|qgs_5mm|qgs_5mm_mh_ideal|qgs_5mm_mh_observed|qgs_5mm_bpi|epos_5mm|epos_5mm_mh_ideal|epos_5mm_mh_observed|flat_small_dpm_5mm|flat_small_qgs_5mm|flat_large_dpm_5mm|flat_large_qgs_5mm|pure-t1|pure-t2s|pure-t2l|eta_qgs|eta_epos"

if [ $# -ne 1 ]; then
    echo $usage
    exit -1
fi

pwd_dir=$PWD

########### Options #############

# Number of input files to be merged in one output file
multifile=100

# Enable/disable smearing of MC
smearing=true
#smearing=false

#################################

if [ "$smearing" = false ] ; then
    disable_smearing="--disable-smearing"
else
    disable_smearing=""
fi

# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
queue=
data_in=/storage/gpfs_data/lhcf/simulation/LHC2015/End2End
data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables/Op2015
	    
#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	data_in=/wizard/bombur/data/LHCf/simulation/LHC2015/End2End
	data_out=/wizard/bombur/data/LHCf/reconstructed/LHC2015
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables/Op2015
    elif [ $farm = "CNAF" ]; then
	queue=
	data_in=/storage/gpfs_data/lhcf/simulation/LHC2015/End2End
	#data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	#main_dir=/opt/exp_software/lhcf/LHCfLibrary
	data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Pi0_ICRC
	main_dir=/opt/exp_software/lhcf/tiberio/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	#table_dir=$main_dir/Tables/Op2015
	table_dir=/opt/exp_software/lhcf/LHCfLibrary/Tables/Op2015
    fi
#----------------- Eugenio's custom configuration -----------------#
elif [ $USER = berti ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	data_in=/wizard/20/lhcf/simulation/LHC2015/End2End
	data_out=/home/berti/lhcf_data
	main_dir=/home/berti/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=
	data_in=/storage/gpfs_data/lhcf/simulation/LHC2015/End2End
	data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    fi
fi
#------------------------------------------------------------------#

# Set input parameters
mhlogfile=""
if [ $1 = "qgs" ]; then

    # QGSJETII-04 (without beam pipe interaction)
    fill=3855
    inname=qgsjetII04
    ouname=qgsjetII04
    mcshort=qgs
    first_dataset=1
    last_dataset=1
    first=10001;
    last=110000;
	
elif [ $1 = "epos" ]; then

    # EPOS-LHC (without beam pipe interaction)
    fill=3855
    inname=eposlhc
    ouname=eposlhc
    mcshort=epo
    first_dataset=1
    last_dataset=1
    first=1;
    last=50000;

elif [ $1 = "dpm" ]; then

    # DPMJET 3.06 (without beam pipe interaction)
    fill=3855
    inname=dpmjetIII03
    ouname=dpmjetIII03
    mcshort=dpm
    first_dataset=1
    last_dataset=1
    first=1;
    last=20000;

elif [ $1 = "qgs_5mm" ]; then

    # QGSJETII-04 - 5mm high (without beam pipe interaction)
    fill=3851
    inname=qgsjetII04_5mm_high
    ouname=qgsjetII04_5mm_high
    mcshort=qg5
    first_dataset=1
    last_dataset=1
    first=110001;
    last=210000;

elif [ $1 = "qgs_5mm_mh_ideal" ]; then

    # QGSJETII-04 - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    inname=qgsjetII04_5mm_high_multihit
    ouname=qgsjetII04_5mm_high_multihit_ideal
    mcshort=qg5mhi
    first_dataset=1
    last_dataset=1
    first=110001
    last=210000

elif [ $1 = "qgs_5mm_mh_observed" ]; then

    # QGSJETII-04 - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    inname=qgsjetII04_5mm_high_multihit
    ouname=qgsjetII04_5mm_high_multihit_observed
    mcshort=qg5mho
    first_dataset=1
    last_dataset=1
    first=110001
    last=210000
    mhlogfile="QGS"
	
elif [ $1 = "qgs_5mm_bpi" ]; then

    # QGSJETII-04 - 5mm high (with beam pipe interaction)
    fill=3851
    inname=qgsjetII04_5mm_high_bpi
    ouname=qgsjetII04_5mm_high_bpi
    mcshort=qg5n
    first_dataset=1
    last_dataset=1
    first=110001;
    last=120000;

elif [ $1 = "epos_5mm" ]; then

    # EPOS-LHC - 5mm high (without beam pipe interaction)
    fill=3851
    inname=eposlhc_5mm_high
    ouname=eposlhc_5mm_high
    mcshort=ep5
    first_dataset=1
    last_dataset=1
    first=100001;
    last=200000;

elif [ $1 = "epos_5mm_mh_ideal" ]; then

    # EPOS-LHC - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    inname=eposlhc_5mm_high_multihit
    ouname=eposlhc_5mm_high_multihit_ideal
    mcshort=ep5mhi
    first_dataset=1
    last_dataset=1
    first=100001
    last=200000

elif [ $1 = "epos_5mm_mh_observed" ]; then

    # EPOS-LHC - 5mm high - Multihit (without beam pipe interaction)
    fill=3851
    inname=eposlhc_5mm_high_multihit
    ouname=eposlhc_5mm_high_multihit_observed
    mcshort=ep5mho
    first_dataset=1
    last_dataset=1
    first=100001
    last=200000
    mhlogfile="EPOS"
 
elif [ $1 = "flat_small_dpm_5mm" ]; then

    data_in=/storage/gpfs_data/lhcf/simulation/FlatSpectra/DPM/Arm2/
    fill=3851
    inname=SmallTower
    ouname=flat_dpm_small
    mcshort=fds5
    first_dataset=1
    last_dataset=1
    first=1;
    last=16000;
 
elif [ $1 = "flat_large_dpm_5mm" ]; then

    data_in=/storage/gpfs_data/lhcf/simulation/FlatSpectra/DPM/Arm2/
    fill=3851
    inname=LargeTower
    ouname=flat_dpm_large
    mcshort=fdl5
    first_dataset=1
    last_dataset=1
    first=50001;
    last=66000;
   
elif [ $1 = "flat_small_qgs_5mm" ]; then

    data_in=/storage/gpfs_data/lhcf/simulation/FlatSpectra/QGS/Arm2/
    fill=3851
    inname=SmallTower
    ouname=flat_qgs_small
    mcshort=fqs5
    first_dataset=1
    last_dataset=1
    first=1;
    last=10000;
  
elif [ $1 = "flat_large_qgs_5mm" ]; then

    data_in=/storage/gpfs_data/lhcf/simulation/FlatSpectra/QGS/Arm2/
    fill=3851
    inname=LargeTower
    ouname=flat_qgs_large
    mcshort=fql5
    first_dataset=1
    last_dataset=1
    first=50001;
    last=60000;
 
elif [ $1 = "pure-t1" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    inname=PurePi0/typeI
    ouname=pure_pi0_t1
    mcshort=pt1
    first_dataset=1
    last_dataset=10
    
elif [ $1 = "pure-t2s" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    inname=PurePi0/typeII_TS
    ouname=pure_pi0_t2s
    mcshort=p2s
    first_dataset=1
    last_dataset=10
    
elif [ $1 = "pure-t2l" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    inname=PurePi0/typeII_TL
    ouname=pure_pi0_t2l
    mcshort=p2l
    first_dataset=1
    last_dataset=10

elif [ $1 = "eta_qgs" ]; then

    # eta-like QGSJETII-04 (without beam pipe interaction)
    data_in=/storage/gpfs_data/lhcf/simulation/LHC2015/Eta/End2End
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Eta
    multifile=10
    fill=3855
    inname=qgsjetII04
    ouname=qgsjetII04
    mcshort=qgs
    first_dataset=1
    last_dataset=1
    first=1;
    last=8000;
    
elif [ $1 = "eta_epos" ]; then

    # eta-like EPOS-LHC (without beam pipe interaction)
    data_in=/storage/gpfs_data/lhcf/simulation/LHC2015/Eta/End2End
    data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Eta
    multifile=10
    fill=3855
    inname=eposlhc
    ouname=eposlhc
    mcshort=epo
    first_dataset=1
    last_dataset=1
    first=1;
    last=5000;
else
    echo $usage
    exit -1
fi

# Set output directory
if [ $smearing = false ]; then
    output_dir=$data_out/${ouname}_nosmear/conv
else
    output_dir=$data_out/$ouname/conv
fi
log_dir=$output_dir/log
sub_dir=$output_dir/sub
htc_dir=$output_dir/htc
workspace=$output_dir/workspace

mkdir -p $output_dir
mkdir -p $log_dir
if [ $farm = "CNAF" ]; then
    mkdir -p $sub_dir
    mkdir -p $htc_dir
fi
mkdir -p $workspace

echo -e "____________________\n"
echo    " SMEARING = $smearing"
echo -e "____________________\n"

# Dataset loop (only relevant for pure pi0 simulations)
for i in $(seq $first_dataset $last_dataset)
do
    # Set input directory
    if [ $1 = "pure-t1" ] || [ $1 = "pure-t2s" ] || [ $1 = "pure-t2l" ]; then
	iset=$(printf "%05d" $i)
        input_dir=$data_in/$inname/Arm2/$iset
	first=$((($i-1)*10000+1))
        last=$(($i*10000))
    else
	input_dir=$data_in/$inname/Arm2
    	if [ ! -d $input_dir ]; then
		input_dir=$data_in/$inname
	fi
	if [ "$mhlogfile" != "" ]; then
	    mhlogfile="-m $data_in/$inname/$mhlogfile-$first-$last.root"
	fi
    fi

    if [ ! -d $input_dir ]; then
	echo "File \"$input_dir\" does not exist!"
	continue
    fi

    # Set run interval
    if [ $last -lt $first ]; then
	echo "Last run (=$last) less than first run (=$first)!"
	exit -1
    fi
    n_it=$(($last-$first))
    n_it=$(($n_it/$multifile))
    first_out_run=$(($first/$multifile+1))
    last_out_run=$(($first_out_run+$n_it))

    # Run loop
    irun=$first_out_run
    while [ $irun -le $last_out_run ]
    do
	# Check if script must be stopped
	switch=$(head -1 $build_dir/switch.inp)
	if [ $switch -eq 0 ]; then
	    break
	fi

	begin=$((($irun-$first_out_run)*$multifile+$first))
	end=$(($begin+$multifile-1))

	current_run=$(printf %05d $irun)
	output_file=$output_dir/run$current_run.root
	log_file=$log_dir/run$current_run.log
	htc_file=$htc_dir/run$current_run.log
	sub_file=$sub_dir/run$current_run.sub
	
	# Check if run has to be submitted or not
	if [ $farm = "FARM" ]; then
	    if [ $(bjobs | grep "C$mcshort$current_run" -c) -ne 0 ]; then
		echo "Run $current_run already submitted!"
		irun=$(($irun+1))
		continue
	    elif [ -f $output_file ]; then
		echo "File \"$output_file\" already exists!"
	    	if [[ ! -f $log_file ]]; then
		    echo "...but the log file does not exist"
		    echo "Reprocessing \"$output_file\""
	        elif [[ $(grep -c "Successfully completed" $log_file) -lt 1 ]]; then
		    echo "...but it had abnormal termination!"
		    echo "Reprocessing \"$output_file\""
		    rm $log_file
		else
		    irun=$(($irun+1))
		    continue
		fi
	    fi
	elif [ $farm = "CNAF" ]; then
	    # !!! TODO for HTCondor !!!
	    #if [ $(bjobs | grep "C$mcshort$current_run" -c) -ne 0 ]; then
	    #    echo "Run $current_run already submitted!"
	    #    irun=$(($irun+1))
	    #    continue
	    #fi
	    if [ -f $output_file ]; then
		echo "File \"$output_file\" already exists!"
	    	if [[ ! -f $log_file ]]; then
		    echo "...but the log file does not exist"
		    echo "Reprocessing \"$output_file\""
		elif [[ $(grep -cw "Killed" $log_file) -ge 1 ]]; then
		    echo "...but it was killed!"
		    echo "Reprocessing \"$output_file\""
		    rm $log_file
	        elif [[ $(grep -c "Closing output file... Done." $log_file) -lt 1 ]]; then
		    echo "...but it had abnormal termination!"
		    echo "Reprocessing \"$output_file\""
		    rm $log_file
		else
		    irun=$(($irun+1))
		    continue
		fi
	    fi
	fi

	# Check number of running jobs
	if [ $farm = "FARM" ]; then
	    njobs=$(bjobs -u $USER | grep $USER | grep -c "C")
	elif [ $farm = "CNAF" ]; then
	    nrunning=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $2}') # running jobs
	    if [ -z $nrunning ]; then
		nrunning=0
	    fi
	    nidle=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $3}') # idle jobs
	    if [ -z $nidle ]; then
		nidle=0
	    fi
	    nheld=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $4}') # held jobs
	    if [ -z $nheld ]; then
		nheld=0
	    fi
	    njobs=$(($nrunning+$nidle+$nheld))
	fi
	# Set desired maximum number of running jobs
	maximum=$(tail -1 $build_dir/switch.inp)
	# Set submit time interval
	tsleep=$(tail -2 $build_dir/switch.inp | head -1)

	# Submit job
	if [ $njobs -lt $maximum ]; then
	    echo "Run $irun/$last_out_run, running jobs = $njobs, max jobs = $maximum"
	    
	    # Create working directory
	    work_dir=$workspace/cnv_${ouname}_$current_run
	    mkdir -p $work_dir
	    cd $work_dir

	    # Submit
	    if [ $farm = "FARM" ]; then
		bsub -J C$mcshort$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" "cd $work_dir ; $build_dir/gunzip_end2end.sh $input_dir $begin $end $work_dir ; $build_dir/bin/ConvertMCtoLvl0 -i . -f $begin -l $end -o $output_file --arm2 -t $table_dir -F $fill $disable_smearing $mhlogfile ; $build_dir/rm_end2end.sh $work_dir $begin $end ; cd $pwd_dir ; rm -rf $work_dir"
		#bsub -J C$mcshort$current_run -q $queue -o $log_file "cd $work_dir ; $build_dir/gunzip_end2end.sh $input_dir $begin $end $work_dir ; $build_dir/bin/ConvertMCtoLvl0 -i . -f $begin -l $end -o $output_file --arm2 -t $table_dir -F $fill $disable_smearing $mhlogfile ; $build_dir/rm_end2end.sh $work_dir $begin $end ; cd $pwd_dir ; rm -rf $work_dir"
		ret_val=0 # TODO?
		
	    elif [ $farm = "CNAF" ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_cnv.sh
arguments = "$work_dir $build_dir $input_dir $begin $end $output_file $table_dir $fill $log_file $pwd_dir '$disable_smearing' '$mhlogfile'"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
		condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
		ret_val=$?
	    fi

	    # Check if run is successfully submitted before going to next one
	    if [ $ret_val = 0 ]; then
		irun=$(($irun+1))
	    fi
	    cd $pwd_dir
	    usleep 100000
	fi

	echo "wait more $tsleep seconds..."
	sleep $tsleep
    done
done
