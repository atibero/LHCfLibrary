#!/bin/bash

usage="Usage: $0 <indir> <first> <last> <outdir>"

if [ $# -ne 4 ]; then
    echo $usage
    exit -1
fi

input_dir=$1
first=$2
last=$3
output_dir=$4

for i in $(seq $first $last)
do
    name5=$(printf "end2end.arm2.%05d.out" $i)
    name6=$(printf "end2end.%06d.out" $i)
    name7=$(printf "end2end.%07d.out" $i)
    nameout=$(printf "end2end.%06d.out" $i)
    for name in $name5
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
    nameout=$(printf "end2end.%06d.out" $i)
    for name in $name6
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
    nameout=$(printf "end2end.%07d.out" $i)
    for name in $name7
    do
	if [ -f $input_dir/$name ]; then
	    echo "cp $input_dir/$name $output_dir/$nameout"
	    cp -p $input_dir/$name $output_dir/$nameout
	    break
	elif [ -f $input_dir/$name.gz ]; then
	    echo "gunzip -c $input_dir/$name.gz > $output_dir/$nameout"
	    gunzip -c $input_dir/$name.gz > $output_dir/$nameout
	    break
	fi
    done
done
