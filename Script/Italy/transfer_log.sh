#!/bin/bash
#while [ 1 ]
#do
#    date
#    condor_transfer_data -name sn-02.cr.cnaf.infn.it -constraint 'JobStatus == 4'
#    condor_rm -name sn-02.cr.cnaf.infn.it -constraint 'JobStatus == 4'
#    date
#
#    sleep 1800
#done

while [ 1 ]
do
    date
    job_list=$(condor_q -name sn-02.cr.cnaf.infn.it -constraint 'JobStatus == 4' |grep "$USER ID"| awk '{print $10}')
    for ijob in $job_list
    do
	condor_transfer_data -name sn-02.cr.cnaf.infn.it $ijob
	condor_rm -name sn-02.cr.cnaf.infn.it $ijob
    done
    date
    sleep 1800
done
