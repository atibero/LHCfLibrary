#!/bin/bash

usage="Usage: $0 <indir> <first> <last>"

if [ $# -ne 3 ]; then
    echo $usage
    exit -1
fi

input_dir=$1
first=$2
last=$3

for i in $(seq $first $last)
do
    name=$(printf "end2end.%06d.out" $i)
    echo "rm -f $input_dir/$name"
    rm -f $input_dir/$name
done
