#!/bin/bash

usage="Usage: $0 build_dir input_file output_file table_dir option log_file"
if [ $# -ne 6 ]; then
    echo $usage
    exit -1
fi

build_dir=$1
input_file=$2
output_file=$3
table_dir=$4
option="$5"
log_file=$6

exec 1>$log_file
exec 2>&1

$build_dir/bin/CreatePedestalTree -i $input_file -o $output_file -t $table_dir $option
