#!/bin/bash

usage="Usage: $0 data_p0.01|data_p0.03|data_5mm|qgs|epos|dpm|qgs_5mm|epos_5mm|pure-t1|pure-t2s|pure-t2l [-2.7ene|+2.7ene|-0.3bcx|+0.3bcx|-0.3bcy|+0.3bcy|pid85|pid95]"

if [ $# -lt 1 ]; then
    echo $usage
    exit -1
fi

########### Options #############
recname=full #used for the path of the input directory
options="--disable-arm1 --disable-photon --disable-neutron --disable-modification"
#################################

#--- Set sys error options (if 2nd argument is given) ---#
isys=
if [ $# -gt 1 ]; then
    if [ $2 = -2.7ene ]; then
	options="$options -E 0.973"
    elif [ $2 = +2.7ene ]; then
	options="$options -E 1.027"
    elif [ $2 = -0.3bcx ]; then
	options="$options -X -0.3"
    elif [ $2 = +0.3bcx ]; then
	options="$options -X +0.3"
    elif [ $2 = -0.3bcy ]; then
	options="$options -Y -0.3"
    elif [ $2 = +0.3bcy ]; then
	options="$options -Y +0.3"
    elif [ $2 = pid85 ]; then
	options="$options -P 85"
    elif [ $2 = pid95 ]; then
	options="$options -P 95"
    else
	echo "Invalid 2nd argument!"
	echo $usage
	exit -1
    fi
    isys="_$2"
fi
    
# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
queue=
data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015
data_out=/storage/gpfs_data/lhcf/reduced/LHC2015
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables/Op2015
#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	queue=wizardq
	#queue=wizardq_low
	data_in=/wizard/bombur/data/LHCf/reconstructed/LHC2015
	data_out=/wizard/bombur/data/LHCf/reduced/LHC2015
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables/Op2015
    elif [ $farm = "CNAF" ]; then
	queue=
	#data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/fixed_leakin
	#data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/fixed_leakin
	#main_dir=/opt/exp_software/lhcf/LHCfLibrary
	data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Pi0_ICRC
	data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/Pi0_ICRC/reduced
	main_dir=/opt/exp_software/lhcf/tiberio/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	#table_dir=$main_dir/Tables/Op2015
	table_dir=/opt/exp_software/lhcf/LHCfLibrary/Tables/Op2015
    fi
fi
#------------------------------------------------------------------#

if [ $1 = "data_p0.01" ]; then

    fill=3855
    pileup=1
    dir_name=data
    name_short=dt1
    first=44299; last=44472; 
    
elif [ $1 = "data_p0.03" ]; then

    fill=3855
    pileup=2
    dir_name=data
    name_short=dt3
    first=44482; last=45106
    
elif [ $1 = "data_5mm" ]; then

    fill=3851
    pileup=1
    dir_name=data
    name_short=dt5
    first=43321; last=43598
   
elif [ $1 = "qgs" ]; then

    # QGSJETII-04 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=qgsjetII04
    name_short=qgs
    first=101; last=1100
    
elif [ $1 = "epos" ]; then

    # EPOS-LHC (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=eposlhc
    name_short=epo
    first=1; last=500
    
elif [ $1 = "dpm" ]; then

    # DPMJET 3.06 (without beam pipe interaction)
    fill=3855
    pileup=1
    dir_name=dpmjetIII03
    name_short=dpm
    first=1; last=200
   
elif [ $1 = "qgs_5mm" ]; then

    # QGSJETII-04 - 5mm high (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=qgsjetII04_5mm_high
    name_short=qg5
    first=1101; last=2100
    
elif [ $1 = "epos_5mm" ]; then

    # EPOS-LHC - 5mm high (without beam pipe interaction)
    fill=3851
    pileup=1
    dir_name=eposlhc_5mm_high
    name_short=ep5
    first=1001; last=2000

elif [ $1 = "pure-t1" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    pileup=0
    dir_name=pure_pi0_t1
    name_short=pt1
    first=1; last=1000
    
elif [ $1 = "pure-t2s" ]; then
    
    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    pileup=0
    dir_name=pure_pi0_t2s
    name_short=p2s
    first=1; last=1000
    
elif [ $1 = "pure-t2l" ]; then

    # Pure Pi0 (without beam pipe interaction)
    fill=3855
    pileup=0
    dir_name=pure_pi0_t2l
    name_short=p2l
    first=1; last=1000
fi

if [[ $dir_name != *"data"* ]]; then
    # MC
    out_file=$data_out/${dir_name}$isys.root
    log_file=$data_out/${dir_name}$isys.log
    htc_file=$data_out/${dir_name}$isys.txt
    sub_file=$data_out/${dir_name}$isys.sub
    options="$options --enable-true --disable-bptx-cut"
else
    # Data
    out_file=$data_out/${1}$isys.root
    log_file=$data_out/${1}$isys.log
    htc_file=$data_out/${1}$isys.txt
    sub_file=$data_out/${1}$isys.sub
fi
input_dir=$data_in/$dir_name/$recname
if [ ! -d $input_dir ]; then
    echo "Input directory doesn't exist!"
    exit -1
fi

echo -e "____________________\n"
echo    " INPUT DIR = $input_dir"
echo    " OUTPUT FILE = $out_file"
echo    " RUNS = $first - $last"
echo    " EXEC = $build_dir/bin/Reduction"
echo    " TABLES = $table_dir"
echo    " OPTIONS = $options"
echo -e "____________________\n"

mkdir -p $data_out

ret_val=1
while [ $ret_val != 0 ]
do
    if [ $farm = "FARM" ]; then
	bsub -J red_$name_short -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" $build_dir/bin/Reduction -i $input_dir -o $out_file -f $first -l $last -F $fill -S $pileup $options
	ret_val=0 # TODO?
    elif [ $farm = "CNAF" ]; then
	cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/bin/Reduction
arguments = "-i $input_dir -o $out_file -f $first -l $last -F $fill -S $pileup $options"
output = $log_file
error = $log_file
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
	ret_val=$?
    fi
    sleep 1
done
