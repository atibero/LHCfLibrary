#!/bin/bash

usage="Usage: $0 build_dir input_dir fill run_inf run_sup option glob_opt output_file log_file"
if [ $# -ne 9 ]; then
    echo $usage
    exit -1
fi

build_dir=$1
input_dir=$2
fill=$3
run_inf=$4
run_sup=$5
option="$6"
globop="$7"
output_file=$8
log_file=$9

exec 1>$log_file
exec 2>&1

optionstring=""
for word in $option
do
    echo $word
    optionstring="$optionstring $word"
done
echo $optionstring
$build_dir/bin/CreateNeutronTree -i $input_dir -o $output_file -f $run_inf -l $run_sup -F $fill -O "$optionstring" $globop;
