#!/bin/bash

usage="Usage: $0 e200gev2014 | e149gev2022 | e197gev2022 | e244gev2022 | 2202veg791e | p150gev2022 | p350gev2022 | pion150gev2022"

if [ $# -ne 1 ]; then
    echo $usage
    exit -1
fi

########### Options #############

truepos=false
#truepos=true

#smearing=true
smearing=false

globop="--arm2"

#fitmode=full
fitmode=fast
if [ "$truepos" = true ]; then
	fitmode="trueposition"
fi

#################################

# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
queue=1
datadir=/storage/gpfs_data/lhcf/reconstructed/LHC2015
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables/SPS2022

#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	datadir=$HOME/lhcf_data
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=1
	datadir=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables
    fi
#----------------- Eugenio's custom configuration -----------------#
elif [ $USER = berti ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	datadir=/wizard/05/lhcf/Analysis/lhcf_data/
	main_dir=/home/berti/lhcf/LHCfLibrary/
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=1
	datadir=/storage/gpfs_data/lhcf/reconstructed/
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	#build_dir=$main_dir/build_oinegue
	#table_dir=$main_dir/Tables
    fi
fi
#------------------------------------------------------------------#

if [ $1 = "e200gev2014" ]; then

    dataname=SPS2014/electron/arm2/200_GeV
    mcshort=200_GeV
    #first=0; last=100
    #file_name="TS2014-Electron200GeV"
    first=50000; last=50160
    file_name="TL2014-Electron200GeV"
    fill=2022

elif [ $1 = "e149gev2022" ]; then

    dataname=SPS2022/electron/arm2/149.14GeV
    mcshort=e149gev
    #first=0; last=10; 
    #file_name="TS2022-Electron149.14GeV"
    first=50000; last=50016; 
    file_name="TL2022-Electron149.14GeV"
    fill=2022

elif [ $1 = "e197gev2022" ]; then

    dataname=SPS2022/electron/arm2/197.32GeV
    #dataname=SPS2022/electron/arm2/Wrong/2014Geometry_197.32GeV_TL
    mcshort=e197GeV
    first=1000; last=1010
    file_name="TS2022-Electron197.32GeV"
    #first=51000; last=51016; 
    #file_name="TL2022-Electron197.32GeV"
    fill=2022

elif [ $1 = "e244gev2022" ]; then

    dataname=SPS2022/electron/arm2/243.61GeV
    mcshort=e244gev
    #first=2000; last=2010; 
    #file_name="TS2022-Electron243.61GeV"
    first=52000; last=52016; 
    file_name="TL2022-Electron243.61GeV"
    fill=2022
  
elif [ $1 = "2202veg791e" ]; then

    dataname=SPS2022/electron/arm2/VeG23.791
    mcshort=veg791e
    #first=3000; last=3010; 
    #file_name="TS2022-VeG23.971nortcelE"
    first=53000; last=53016; 
    file_name="TL2022-VeG23.971nortcelE"
    fill=2022

elif [ $1 = "p150gev2022" ]; then

    dataname=SPS2022/proton/arm2/150GeV
    mcshort=p150gev
    #first=10000; last=10010; 
    #file_name="TS2022-Proton150GeV"
    first=60000; last=60016; 
    file_name="TL2022-Proton150GeV"
    fill=2022

elif [ $1 = "p350gev2022" ]; then

    dataname=SPS2022/proton/arm2/350GeV
    mcshort=p350gev
    #first=11000; last=11010; 
    #file_name="TL2022-Proton350GeV"
    first=61000; last=61016; 
    file_name="TL2022-Proton350GeV"
    fill=2022

elif [ $1 = "pion150gev2022" ]; then

    dataname=SPS2022/pion/arm2/150GeV
    mcshort=pion150gev
    #first=20000; last=20010; 
    #file_name="TS2022-Pion150GeV"
    first=70000; last=70016; 
    file_name="TL2022-Pion150GeV"
    fill=2022

else
    echo $usage
    exit -1
fi

if [ $last -lt $first ]; then
    echo "Last run (=$last) less than first run (=$first)!"
    exit -1
fi

echo -e "____________________\n"
echo    " FIT MODE = $fitmode"
echo	" SMEARING = $smearing"
echo -e "____________________\n"

if [[ $dataname != *"data"* ]]; then
	if [ "$smearing" = false ] ; then
		dataname=${dataname}_nosmear
	fi
fi

input_dir=$datadir/$dataname/$fitmode/$date
output_dir=$datadir/$dataname/check_$fitmode/$date

log_dir=$output_dir/log
sub_dir=$output_dir/sub
htc_dir=$output_dir/htc
mkdir -p $log_dir
if [ $farm = "CNAF" ]; then
    mkdir -p $sub_dir
    mkdir -p $htc_dir
fi

irun=$first
while [ $irun -le $last ]
do
    switch=$(head -1 $build_dir/switch.inp)
    if [ $switch -eq 0 ]; then
	break
    fi

    # Check number of running jobs
    if [ $farm = "FARM" ]; then
	njobs=$(bjobs -u $USER | grep $USER | grep -c "R")
    elif [ $farm = "CNAF" ]; then
	nrunning=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $2}') # running jobs
	if [ -z $nrunning ]; then
	    nrunning=0
	fi
	nidle=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $3}') # idle jobs
	if [ -z $nidle ]; then
	    nidle=0
	fi
	nheld=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $4}') # held jobs
	if [ -z $nheld ]; then
	    nheld=0
	fi
	njobs=$(($nrunning+$nidle+$nheld))
    fi
    # Set desired maximum number of running jobs
    maximum=$(tail -1 $build_dir/switch.inp)
    # Set submit time interval
    tsleep=$(tail -2 $build_dir/switch.inp | head -1)

    if [ $njobs -lt $maximum ]; then
	echo "Run $irun/$last, running jobs = $njobs, max jobs = $maximum"

	inow=$irun
	#inex=$(($irun+100))
	inex=$last
	if [[ $dataname == *"data"* || $dataname == *"flat"* ]]; then
		inex=$last
	fi
	current_run="$(printf %05d $inow)-$(printf %05d $inex)"
	
	output_file=$output_dir/${file_name}_${current_run}.root
	log_file=$log_dir/neutrontree_${file_name}_${current_run}.log
	htc_file=$htc_dir/neutrontree_${file_name}_${current_run}.log
	sub_file=$sub_dir/neutrontree_${file_name}_${current_run}.sub

	if [ ! -d $input_dir ]; then
	    echo "File \"$input_dir\" does not exist!"
	    irun=$(($inex+1))
	    continue
	fi

	# Submit
	if [ $farm = "FARM" ]; then
   	    if [ $(bjobs | grep "T$mcshort$current_run" -c) -ne 0 ]; then
	    	echo "Run $current_run already submitted!"
	    	irun=$(($inex+1))
		continue
	    fi

	    cmd="$build_dir/bin/CheckSimulation -i $input_dir -o $output_file -f $inow -l $inex -F $fill $globop;"
	    bsub -J T$mcshort$current_run -q $queue -o $log_file "$cmd"
	    ret_val=0 # TODO?

	elif [ $farm = "CNAF" ]; then
	    # !!! TODO for HTCondor !!!
#   	    if [ $(bjobs | grep "T$mcshort$current_run" -c) -ne 0 ]; then
#		echo "Run $current_run already submitted!"
#	    	irun=$(($inex+1))
#		continue
#	    fi

	    cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_chk_sim.sh
arguments = "$build_dir $input_dir $fill $inow $inex '$globop' $output_file $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue $queue
EOF
	    condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
	    ret_val=$?

	fi

	# Check if run is successfully submitted before going to next one
	if [ $ret_val = 0 ]; then
		if [[ $dataname != *"data"* || $dataname == *"flat"* ]]; then
	    		irun=$(($inex+1))
		else
			break;
		fi
	fi
	usleep 100000
    fi

    echo "wait more $tsleep seconds..."
    sleep $tsleep
done
