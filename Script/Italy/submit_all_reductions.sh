#!/bin/bash

for ired in data data_p0.03 pure-t1 pure-t2s pure-t2l qgs epos data_5mm #qgs_5mm epos_5mm
do
    ./submit_reduction.sh $ired
done

for ired in data data_p0.03 data_5mm
do
    for isys in -2.7ene +2.7ene -0.3bcx +0.3bcx -0.3bcy +0.3bcy pid85 pid95
    do
	./submit_reduction.sh $ired $isys
    done
done

for ired in pure-t1 pure-t2s pure-t2l #qgs epos qgs_5mm epos_5mm
do
    for isys in pid85 pid95
    do
	./submit_reduction.sh $ired $isys
    done
done
