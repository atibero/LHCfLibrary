#!/bin/bash

usage="Usage: $0 data|data_5mm|qgs|qgs_5mm|qgs_5mm_mh_ideal|qgs_5mm_mh_observed|qgs_5mm_bpi|epos|epos_5mm|epos_5mm_mh_ideal|epos_5mm_mh_observed|flat_small_dpm_5mm|flat_small_qgs_5mm|flat_large_dpm_5mm|flat_large_qgs_5mm|"

if [ $# -ne 1 ]; then
    echo $usage
    exit -1
fi

########### Options #############

truepos=false
#truepos=true

smearing=true
#smearing=false

multifile=100
#addoption="siedep"
addoption=""

globop=""
option=""

fitmode=full
#fitmode=fast
if [ "$truepos" = true ]; then
	fitmode="trueposition"
fi

#################################

# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
queue=1
datadir=/storage/gpfs_data/lhcf/reconstructed/LHC2015
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables

#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	datadir=$HOME/lhcf_data
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=1
	datadir=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables
    fi
#----------------- Eugenio's custom configuration -----------------#
elif [ $USER = berti ]; then
    if [ $farm = "FARM" ]; then
	#queue=wizardq
	queue=wizardq_low
	datadir=/wizard/05/lhcf/Analysis/lhcf_data/
	main_dir=/home/berti/lhcf/LHCfLibrary/
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=1
	datadir=/storage/gpfs_data/lhcf/reconstructed/LHC2015
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    fi
fi
#------------------------------------------------------------------#

if [ $1 = "data" ]; then

    option="data arm2 neutron ${addoption}"

    mcshort=dt
    first=44299; 
    last=44472; 
    dataname=data_unified
    fill=3855
    
    file_name="DataFill3855"

elif [ $1 = "data_5mm" ]; then

    option="data arm2 neutron ${addoption}"

    mcshort=dt5
    first=43321; 
    last=43598; 
    dataname=data_unified
    fill=3851
    
    file_name="DataFill3851"

elif [ $1 = "qgs" ]; then

    # QGSJETII-04 - 5mm high (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=qgsjetII04_unified
    mcshort=qg
    first=101; last=1100
    fill=3855
    
    file_name="QGS"

elif [ $1 = "qgs_5mm" ]; then

    # QGSJETII-04 - 5mm high (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=qgsjetII04_5mm_high_unified
    mcshort=qg5
    first=1101; last=2100
    fill=3851
    
    file_name="QGS"
   
elif [ $1 = "qgs_5mm_mh_ideal" ]; then

    # QGSJETII-04 - 5mm high - Multihit (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=qgsjetII04_5mm_high_multihit_ideal_unified
    mcshort=qg5mhi
    first=1101; last=2100
    fill=3851
  
    file_name="QGSIdeal"

elif [ $1 = "qgs_5mm_mh_observed" ]; then

    # QGSJETII-04 - 5mm high - Multihit (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=qgsjetII04_5mm_high_multihit_observed_unified
    mcshort=qg5mho
    first=1101; last=2100
    fill=3851
 
    file_name="QGSObserved"
	
elif [ $1 = "qgs_5mm_bpi" ]; then

    # QGSJETII-04 - 5mm high (with beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=qgsjetII04_5mm_high_bpi_unified
    mcshort=qg5n
    first=1101; last=1200
    fill=3851
 
    file_name="QGS_WithBeamPipeInteraction"
 
elif [ $1 = "epos" ]; then

    # EPOS-LHC - 5mm high (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=eposlhc_unified
    mcshort=ep
    first=1; last=500
    fill=3855

    file_name="EPOS"

elif [ $1 = "epos_5mm" ]; then

    # EPOS-LHC - 5mm high (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=eposlhc_5mm_high_unified
    mcshort=ep5
    #first=1001; last=2000
    first=1401; last=1600
    fill=3851
    
    file_name="EPOS"
   
elif [ $1 = "epos_5mm_mh_ideal" ]; then

    # EPOS-LHC - 5mm high - Multihit (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=eposlhc_5mm_high_multihit_ideal_unified
    mcshort=ep5mhi
    first=1001; last=2000
    fill=3851
  
    file_name="EPOSIdeal"

elif [ $1 = "epos_5mm_mh_observed" ]; then

    # EPOS-LHC - 5mm high - Multihit (without beam pipe interaction)

    option="mc arm2 neutron ${addoption}"

    dataname=eposlhc_5mm_high_multihit_observed_unified
    mcshort=ep5mho
    first=1001; last=2000
    fill=3851
 
    file_name="EPOSObserved"

elif [ $1 = "flat_small_dpm_5mm" ]; then

    option="mc arm2 neutron ${addoption}"

    dataname=flat_dpm_small_unified
    mcshort=fds5
    first=1; last=160
    fill=-3851
    globop="-Y -25"
 
    file_name="FlatDPM_SmallTower"
 
elif [ $1 = "flat_large_dpm_5mm" ]; then

    option="mc arm2 neutron ${addoption}"

    dataname=flat_dpm_large_unified
    mcshort=fdl5
    first=501; last=660
    fill=-3851
    globop="-Y -25"
 
    file_name="FlatDPM_LargeTower"

elif [ $1 = "flat_small_qgs_5mm" ]; then

    option="mc arm2 neutron ${addoption}"

    dataname=flat_qgs_small_unified
    mcshort=fqs5
    first=1; last=100
    fill=-3851
    globop="-Y -25"
 
    file_name="FlatQGS_SmallTower"

elif [ $1 = "flat_large_qgs_5mm" ]; then

    option="mc arm2 neutron ${addoption}"

    dataname=flat_qgs_large_unified
    mcshort=fql5
    first=501; last=600
    fill=-3851
    globop="-Y -25"
 
    file_name="FlatQGS_LargeTower"

else

    echo $usage
    exit -1

fi

if [ $last -lt $first ]; then
    echo "Last run (=$last) less than first run (=$first)!"
    exit -1
fi

echo -e "____________________\n"
echo    " FIT MODE = $fitmode"
echo	" SMEARING = $smearing"
echo	" OPTION   = $option"
echo -e "____________________\n"

if [[ $dataname != *"data"* ]]; then
	if [ "$smearing" = false ] ; then
		dataname=${dataname}_nosmear
	fi
fi

input_dir=$datadir/$dataname/$fitmode/$date
output_dir=$datadir/$dataname/tree_$fitmode/$date

log_dir=$output_dir/log
sub_dir=$output_dir/sub
htc_dir=$output_dir/htc
mkdir -p $log_dir
if [ $farm = "CNAF" ]; then
    mkdir -p $sub_dir
    mkdir -p $htc_dir
fi

irun=$first
while [ $irun -le $last ]
do
    switch=$(head -1 $build_dir/switch.inp)
    if [ $switch -eq 0 ]; then
	break
    fi

    # Check number of running jobs
    if [ $farm = "FARM" ]; then
	njobs=$(bjobs -u $USER | grep $USER | grep -c "R")
    elif [ $farm = "CNAF" ]; then
	nrunning=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $2}') # running jobs
	if [ -z $nrunning ]; then
	    nrunning=0
	fi
	nidle=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $3}') # idle jobs
	if [ -z $nidle ]; then
	    nidle=0
	fi
	nheld=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $4}') # held jobs
	if [ -z $nheld ]; then
	    nheld=0
	fi
	njobs=$(($nrunning+$nidle+$nheld))
    fi
    # Set desired maximum number of running jobs
    maximum=$(tail -1 $build_dir/switch.inp)
    # Set submit time interval
    tsleep=$(tail -2 $build_dir/switch.inp | head -1)

    if [ $njobs -lt $maximum ]; then
	echo "Run $irun/$last, running jobs = $njobs, max jobs = $maximum"

	inow=$irun
	inex=$(($irun+$multifile-1))
	if [[ $dataname == *"data"* || $dataname == *"flat"* ]]; then
		inex=$last
	fi
	current_run="$(printf %05d $inow)-$(printf %05d $inex)"
	
	output_file=$output_dir/${file_name}_${current_run}.root
	log_file=$log_dir/neutrontree_${file_name}_${current_run}.log
	htc_file=$htc_dir/neutrontree_${file_name}_${current_run}.log
	sub_file=$sub_dir/neutrontree_${file_name}_${current_run}.sub

	if [ ! -d $input_dir ]; then
	    echo "File \"$input_dir\" does not exist!"
	    irun=$(($inex+1))
	    continue
	fi

	# Submit
	if [ $farm = "FARM" ]; then
   	    if [ $(bjobs | grep "T$mcshort$current_run" -c) -ne 0 ]; then
	    	echo "Run $current_run already submitted!"
	    	irun=$(($inex+1))
		continue
	    fi

	    cmd="$build_dir/bin/CreateNeutronTree -i $input_dir -o $output_file -f $inow -l $inex -F $fill -O $option $globop;"
	    bsub -J T$mcshort$current_run -q $queue -o $log_file "$cmd"
	    ret_val=0 # TODO?

	elif [ $farm = "CNAF" ]; then
	    # !!! TODO for HTCondor !!!
#   	    if [ $(bjobs | grep "T$mcshort$current_run" -c) -ne 0 ]; then
#		echo "Run $current_run already submitted!"
#	    	irun=$(($inex+1))
#		continue
#	    fi

	    cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_sub_neu.sh
arguments = "$build_dir $input_dir $fill $inow $inex '$option' '$globop' $output_file $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue $queue
EOF
	    condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
	    ret_val=$?

	fi

	# Check if run is successfully submitted before going to next one
	if [ $ret_val = 0 ]; then
		if [[ $dataname != *"data"* || $dataname == *"flat"* ]]; then
	    		irun=$(($inex+1))
		else
			break;
		fi
	fi
	usleep 100000
    fi

    echo "wait more $tsleep seconds..."
    sleep $tsleep
done
