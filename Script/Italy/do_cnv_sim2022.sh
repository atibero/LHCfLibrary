#!/bin/bash
usage="Usage: $0 work_dir build_dir input_dir begin end filenumber mcname output_file table_dir fill subfill log_file pwd_dir disable_smearing"
if [ $# -ne 14 ]; then
    echo $usage
    exit -1
fi

work_dir=$1
build_dir=$2
input_dir=$3
begin=$4
end=$5
filenumber=$6
mcname=$7
output_file=$8
table_dir=$9
fill=${10}
subfill=${11}
log_file=${12}
pwd_dir=${13}
disable_smearing=${14}


exec 1>$log_file
exec 2>&1
cd $work_dir
$build_dir/gunzip_end2end_sim2022.sh $input_dir $begin $end $filenumber $mcname $work_dir
$build_dir/bin/ConvertMCtoLvl0 -i . -f $begin -l $end -N $filenumber -k $mcname -o $output_file --average-pedestal --arm2 -t $table_dir -F $fill -S $subfill $disable_smearing -M
$build_dir/rm_end2end_sim2022.sh $work_dir $begin $end $filenumber $mcname
cd $pwd_dir
rm -rf $work_dir
