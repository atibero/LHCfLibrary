#!/bin/bash

usage="Usage: $0 build_dir input_file output_file table_dir fill glob_opt log_file"
if [ $# -ne 7 ]; then
    echo $usage
    exit -1
fi

build_dir=$1
input_file=$2
output_file=$3
table_dir=$4
fill=$5
globop="$6"
log_file=$7

exec 1>$log_file
exec 2>&1

$build_dir/bin/Calibrate -i $input_file -o $output_file -t $table_dir -F $fill $globop
