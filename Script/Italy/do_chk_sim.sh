#!/bin/bash

usage="Usage: $0 build_dir input_dir fill run_inf run_sup glob_opt output_file log_file"
if [ $# -ne 8 ]; then
    echo $usage
    exit -1
fi

build_dir=$1
input_dir=$2
fill=$3
run_inf=$4
run_sup=$5
globop="$6"
output_file=$7
log_file=$8

exec 1>$log_file
exec 2>&1

$build_dir/bin/CheckSimulation -i $input_dir -o $output_file -f $run_inf -l $run_sup -F $fill $globop
