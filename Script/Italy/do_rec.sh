#!/bin/bash

usage="Usage: $0 build_dir input_file output_file fitmode table_dir fill pileup glob_opt option log_file"
if [ $# -ne 10 ]; then
    echo $usage
    exit -1
fi

build_dir=$1
input_file=$2
output_file=$3
fitmode=$4
table_dir=$5
fill=$6
pileup=$7
globop="$8"
option="$9"
log_file=${10}

exec 1>$log_file
exec 2>&1

$build_dir/bin/Reconstruct -i $input_file -o $output_file -p $fitmode -t $table_dir -F $fill -S $pileup $globop $option
