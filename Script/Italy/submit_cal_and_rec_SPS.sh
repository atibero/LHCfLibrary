#!/bin/bash

usage="Usage: $0 e200gev2014 | e200gev2015data | e149gev2022 | e197gev2022 | e244gev2022 | 2202veg791e | p150gev2022 | p350gev2022 | pion150gev2022"

if [ $# -ne 1 ]; then
    echo $usage
    exit -1
fi

########### Options #############

# if both do_cal and do_rec are true, calibrated file is removed at the end of the job
do_cal=true # do Calibration
do_rec=true # do Reconstruction

truepos=false # use reconstructed position
#truepos=true # use true position (only available for MC)

#fitmode=full # full position fit
fitmode=fast # barycentre method
#fitmode=none # only reconstruct energy (needs input files reconstructed with --level2cal)

save_charge=false # don't save charge measurement in silicon
#save_charge=true # save charge measurement in silicon

#smearing=true  #only to set input/output directories
smearing=false  #only to set input/output directories

recname=$fitmode #used for the path of the output directory

globop="--disable-arm1" # both calibration and reconstruction
option="--level2cal"    # only reconstruction

#################################

if [ "$truepos" = true ]; then
    option="$option --true-coor"
fi
if [ "$save_charge" = true ]; then
    option="$option --levelc"
fi
if [ "$truepos" = true ]; then
    recname="trueposition"
fi

if [ $do_cal != true ] && [ $do_rec != true ]; then
    echo "Both calibration and reconstruction are disabled!"
    exit -1
fi

if [ $fitmode = "none" ]; then
    do_cal=false
fi

# Check farm type
if [ $HOME = /home/$USER ]; then
    farm="FARM"
elif [ $HOME = /home/LHCF/$USER ]; then
    farm="CNAF"
else
    farm="unknown"
    echo "Cannot find farm type!"
    exit -1
fi

#--- Default configuration (CNAF) ---#
queue=
data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015
data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015
main_dir=/opt/exp_software/lhcf/LHCfLibrary
build_dir=$main_dir/build
table_dir=$main_dir/Tables/SPS2022

#----------------- Alessio's custom configuration -----------------#
if [ $USER = tiberio ]; then
    if [ $farm = "FARM" ]; then
	queue=wizardq_low
	data_in=/wizard/bombur/data/LHCf/reconstructed/LHC2015
	data_out=/wizard/bombur/data/LHCf/reconstructed/LHC2015
	main_dir=$HOME/lhcf/ammaccabanane
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables/Op2015
    elif [ $farm = "CNAF" ]; then
	queue=
	data_in=/storage/gpfs_data/lhcf/reconstructed/LHC2015/fixed_leakin
	data_out=/storage/gpfs_data/lhcf/reconstructed/LHC2015/fixed_leakin
	main_dir=/opt/exp_software/lhcf/LHCfLibrary
	build_dir=$main_dir/build_aresshio
	table_dir=$main_dir/Tables/Op2015
    fi
#----------------- Eugenio's custom configuration -----------------#
elif [ $USER = berti ]; then
    if [ $farm = "FARM" ]; then
	queue=wizardq_low
	data_in=/home/berti/lhcf_data
	data_out=/home/berti/lhcf_data
	main_dir=/home/berti/lhcf/LHCfLibrary
	build_dir=$main_dir/build_oinegue
	table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
	queue=
	data_in=/storage/gpfs_data/lhcf/reconstructed/
	data_out=/storage/gpfs_data/lhcf/reconstructed/
	main_dir=/opt/exp_software/lhcf/berti/LHCfLibrarySPS2015
	build_dir=$main_dir/build
	table_dir=/opt/exp_software/lhcf/LHCfLibrary/Tables/SPS2022
    fi

#------------------------------------------------------------------#

elif [ $USER = gpiparo ]; then
    if [ $farm = "FARM" ]; then
        queue=wizardq_low
        data_in=/wizard/20/lhcf/simulation/LHC2015/End2End
        data_out=/home/berti/lhcf_data
        main_dir=/home/berti/lhcf/LHCfLibrary
        build_dir=$main_dir/build_oinegue
        table_dir=$main_dir/Tables
    elif [ $farm = "CNAF" ]; then
        queue=
        data_in=/storage/gpfs_data/lhcf/reconstructed/
        data_out=/storage/gpfs_data/lhcf/reconstructed/
        main_dir=/opt/exp_software/lhcf/LHCfLibrary/
        #build_dir=$main_dir/build_oinegue
        #table_dir=/opt/exp_software/lhcf/LHCfLibrary/gpiparo/LHCfLibrary_gpiparo/Tables/SPS2022

	globop="--disable-arm1 --level1" # both calibration and reconstruction
        option="--level2cal --disable-neutron --level2"    # only reconstruction
    fi
fi




if [ $1 = "e200gev2014" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2014/electron/arm2/200_GeV
    name_short=e200gev
    first=1; last=100; 

elif [ $1 = "e200gev2015data" ]; then
    
    fill=2015
    pileup=0.01 #It should have no effect
    dir_name=SPS2015
    name_short=e200gevdata
    data_in=/storage/gpfs_data/lhcf/
    #first=0; last=10; 
    first=50355; last=50359; 

elif [ $1 = "e149gev2022" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/electron/arm2/149.14GeV
    name_short=e149gev
    #first=0; last=10; 
    first=50000; last=50016; 

elif [ $1 = "e197gev2022" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/electron/arm2/197.32GeV
    #dir_name=SPS2022/electron/arm2/Wrong/2014Geometry_197.32GeV_TL
    name_short=e197gev
    #first=1000; last=1010; 
    first=51000; last=51016; 
 
elif [ $1 = "e244gev2022" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/electron/arm2/243.61GeV
    name_short=e244gev
    #first=2000; last=2010; 
    first=52000; last=52016; 
  
elif [ $1 = "2202veg791e" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/electron/arm2/VeG23.791
    name_short=veg791e
    #first=3000; last=3010; 
    first=53000; last=53016; 

elif [ $1 = "p150gev2022" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/proton/arm2/150GeV
    name_short=p150gev
    #first=10000; last=10010; 
    first=60000; last=60016; 

elif [ $1 = "p350gev2022" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/proton/arm2/350GeV
    name_short=p350gev
    #first=11000; last=11010; 
    first=61000; last=61016; 

elif [ $1 = "pion150gev2022" ]; then

    fill=2022
    pileup=0.01 #It should have no effect
    dir_name=SPS2022/pion/arm2/150GeV
    name_short=pion150gev
    #first=20000; last=20010; 
    first=70000; last=70016; 

else
    echo $usage
    exit -1
fi

# Check run interval
if [ $last -lt $first ]; then
    echo "Last run (=$last) less than first run (=$first)!"
    exit -1
fi

# Set input directory
if [[ $name_short != *"data"* ]]; then
    if [ $smearing = false ] ; then
    	dir_name=${dir_name}_nosmear
    fi
    input_dir=$data_in/$dir_name/conv
else
    if [ $farm = "FARM" ]; then
	input_dir=/wizard/bombur/data/LHCf/DATA/SPS2015
    elif [ $farm = "CNAF" ]; then
	input_dir=/storage/gpfs_data/lhcf/DATA/SPS2015
    fi
fi
if [ ! -d $input_dir ]; then
    echo "Input directory $input_dir doesn't exist!"
    exit -1
fi
inrec_dir=$data_out/$dir_name/full # only meaningful for "none" fitmode

# Set output directory
outcal_dir=$data_out/$dir_name/calib
outrec_dir=$data_out/$dir_name/$recname
if [ $do_rec != true ]; then
    output_dir=$outcal_dir
else
    output_dir=$outrec_dir
fi
log_dir=$output_dir/log
sub_dir=$output_dir/sub
htc_dir=$output_dir/htc
mkdir -p $outcal_dir
mkdir -p $outrec_dir
mkdir -p $log_dir
if [ $farm = "CNAF" ]; then
    mkdir -p $sub_dir
    mkdir -p $htc_dir
fi

echo -e "____________________\n"
echo    " FIT MODE = $fitmode"
echo    " SMEARING = $smearing"
echo    " OPTION   = $option"
echo -e "____________________\n"

# Run loop
irun=$first
while [ $irun -le $last ]
do
    # Check if script must be stopped
    switch=$(head -1 $build_dir/switch.inp)
    if [ $switch -eq 0 ]; then
	break
    fi
    
    current_run=$(printf %05d $irun)
    incal_file=$input_dir/run$current_run.root
    outcal_file=$outcal_dir/cal_run$current_run.root
    if [ $fitmode = "none" ]; then
	inrec_file=$inrec_dir/rec_run$current_run.root
    else
	inrec_file=$outcal_file
    fi
    outrec_file=$outrec_dir/rec_run$current_run.root
    if [ $do_rec != true ]; then
	log_file=$log_dir/cal_run$current_run.log
	htc_file=$htc_dir/cal_run$current_run.log
	sub_file=$sub_dir/cal_run$current_run.sub
    else
	log_file=$log_dir/rec_run$current_run.log
	htc_file=$htc_dir/rec_run$current_run.log
	sub_file=$sub_dir/rec_run$current_run.sub
    fi
	
    # Check if run has to be submitted or not
    if [ $do_cal = true ]; then
	input_file=$incal_file
    else
	input_file=$inrec_file
    fi
    if [ $do_rec != true ]; then
	output_file=$outcal_file
    else
	output_file=$outrec_file
    fi
    if [ $farm = "FARM" ]; then
	if [ $(bjobs | grep "R$name_short$current_run" -c) -ne 0 ]; then
	    echo "Run $current_run already submitted!"
	    irun=$(($irun+1))
	    continue
	elif [ ! -f $input_file ]; then
	    echo "File \"$input_file\" does not exist!"
	    irun=$(($irun+1))
	    continue
	elif [ -f $output_file ]; then
	    echo "File \"$output_file\" already exists!"
	    if [[ ! -f $log_file ]]; then
		echo "...but the log file does not exist"
		echo "Reprocessing \"$output_file\""
	    elif [[ $(tail -n1 $log_file | grep -c "Closing files... Done.") -lt 1 ]]; then
		echo "...but it had abnormal termination!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    else
		irun=$(($irun+1))
		continue
	    fi
	fi
    elif [ $farm = "CNAF" ]; then
	# !!! TODO for HTCondor !!!
	#if [ $(bjobs | grep "R$name_short$current_run" -c) -ne 0 ]; then
	#    echo "Run $current_run already submitted!"
	#    irun=$(($irun+1))
	#    continue
	#fi
	if [ ! -f $input_file ]; then
	    echo "File \"$input_file\" does not exist!"
	    irun=$(($irun+1))
	    continue
	elif [ -f $output_file ]; then
	    echo "File \"$output_file\" already exists!"
	    if [[ ! -f $log_file ]]; then
		echo "...but the log file does not exist"
		echo "Reprocessing \"$output_file\""
	    elif [[ $(grep -cw "Killed" $log_file) -ge 1 ]]; then
		echo "...but it was killed!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    elif [[ $(tail -n1 $log_file | grep -c "Closing files... Done.") -lt 1 ]]; then
		echo "...but it had abnormal termination!"
		echo "Reprocessing \"$output_file\""
		rm $log_file
	    else
		irun=$(($irun+1))
		continue
	    fi
	fi
    fi

    # Check number of running jobs
    if [ $farm = "FARM" ]; then
	njobs=$(bjobs -u $USER | grep $USER | grep -c "R")
    elif [ $farm = "CNAF" ]; then
	nrunning=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $2}') # running jobs
	if [ -z $nrunning ]; then
	    nrunning=0
	fi
	nidle=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $3}') # idle jobs
	if [ -z $nidle ]; then
	    nidle=0
	fi
	nheld=$(condor_status -submitters | grep $USER | tail -1 | awk '{print $4}') # held jobs
	if [ -z $nheld ]; then
	    nheld=0
	fi
	njobs=$(($nrunning+$nidle+$nheld))
    fi
    # Set desired maximum number of running jobs
    maximum=$(tail -1 $build_dir/switch.inp)
    # Set submit time interval
    tsleep=$(tail -2 $build_dir/switch.inp | head -1)

    # Submit job
    if [ $njobs -lt $maximum ]; then
	echo "Run $irun/$last, running jobs = $njobs, max jobs = $maximum"

	# Submit
	if [ $farm = "FARM" ]; then
	    if [ $do_cal = true ] && [ $do_rec = true ]; then
		bsub -J R$name_short$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" "$build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop ; $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -P $pileup $globop $option ; rm $outcal_file"
	        #bsub -J R$name_short$current_run -q $queue -o $log_file "$build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop ; $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -P $pileup $globop $option ; rm $outcal_file"
	    elif [ $do_cal = true ]; then
		bsub -J G$name_short$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" $build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop
		#bsub -J G$name_short$current_run -q $queue -o $log_file $build_dir/bin/Calibrate -i $incal_file -o $outcal_file -t $table_dir -F $fill $globop
	    elif [ $do_rec = true ]; then
		bsub -J R$name_short$current_run -q $queue -o $log_file -m "farm-01 farm-02 farm-03 farm-04 farm-05 farm-06 farm-07 farm-10 farm-11 farm-12 farm-14 farm-16 farm-17 farm-19 farm-22 farm-24 farm-25 farm-26 farm-27 farm-34 farm-35 farm-36 farm-37" $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -P $pileup $globop $option
		#bsub -J R$name_short$current_run -q $queue -o $log_file $build_dir/bin/Reconstruct -i $inrec_file -o $outrec_file -p $fitmode -t $table_dir -F $fill -P $pileup $globop $option
	    fi
	    ret_val=0 # TODO?

	elif [ $farm = "CNAF" ]; then
	    if [ $do_cal = true ] && [ $do_rec = true ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_cal_and_rec.sh
arguments = "$build_dir $incal_file $outcal_file $outrec_file $fitmode $table_dir $fill $pileup '$globop' '$option' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	    elif [ $do_cal = true ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_cal.sh
arguments = "$build_dir $incal_file $outcal_file $table_dir $fill '$globop' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	    elif [ $do_rec = true ]; then
		cat > $sub_file <<EOF
universe = vanilla
executable = $build_dir/do_rec.sh
arguments = "$build_dir $inrec_file $outrec_file $fitmode $table_dir $fill $pileup '$globop' '$option' $log_file"
log = $htc_file
getenv = True
ShouldTransferFiles = YES
WhenToTransferOutput = ON_EXIT
queue 1
EOF
	    fi
	    condor_submit -name sn-02.cr.cnaf.infn.it -spool $sub_file
	    ret_val=$?
	fi
	
	# Check if run is successfully submitted before going to next one
	if [ $ret_val = 0 ]; then
	    irun=$(($irun+1))
	fi
	usleep 100000
    fi

    echo "wait more $tsleep seconds..."
    sleep $tsleep
done
