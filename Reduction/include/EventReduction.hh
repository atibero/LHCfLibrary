#ifndef EventReduction_HH
#define EventReduction_HH

#include <vector>

using namespace std;

#include <TROOT.h>
#include <TTree.h>

#include "EventCut.hh"
#include "Level2.hh"
#include "Level3.hh"

namespace nLHCf {
template <typename armclass, typename armrec, typename arman>
class EventReduction : public arman, public LHCfParams {
 public:
  EventReduction();
  ~EventReduction();

 public:
  TTree *fTree;
  Bool_t fPhotonEnable;
  Bool_t fNeutronEnable;
  Bool_t fPi0Enable;
  Bool_t fTrueEnable;
  Bool_t fLvl2Enable;
  Bool_t fLvl2PosEnable;

  /* For systematic errors study */
  Double_t fPhotonEnergyFactor = 1.;
  Double_t fBeamCentreOffset[2] = {0., 0.};
  Int_t fPionPidEff = 90;

  /* Variables for Tree */
  /* Event information*/
  Int_t fArm;
  Int_t fRun;
  Int_t fGnumber;
  Int_t fNumber;
  UInt_t fTime;      // CPU time
  UInt_t fTimeUsec;  // usec
  UInt_t fTrigger;   // Bit map for trigger information
  UInt_t fBeam;      // Beam presence
  UInt_t fBcid;      // Bunch ID
  UInt_t fQualityFlag;
  /* ATLAS Trigger Information */
  Bool_t fAtlasTriggerAccept;
  UInt_t fAtlasECR;
  UInt_t fAtlasL1id;
  /* Photon reconstruction results */
  vector<UInt_t> fPhotonNhit;
  vector<vector<UInt_t>> fPhotonFlag;
  vector<vector<Double_t>> fPhotonEnergy;
  vector<vector<Bool_t>> fPhotonPid;
  vector<vector<vector<Double_t>>> fPhotonPosCal;
  vector<vector<vector<Double_t>>> fPhotonPos;
  vector<Double_t> fPhotonL90;
  vector<Double_t> fPhotonL20;
  /* Neutron reconstruction results */
  vector<UInt_t> fNeutronFlag;
  vector<Double_t> fNeutronEnergy;
  vector<Bool_t> fNeutronPid;
  vector<vector<Double_t>> fNeutronPosCal;
  vector<vector<Double_t>> fNeutronPos;
  vector<Double_t> fNeutronL90;
  vector<Double_t> fNeutronL20;
  /* Pi0 reconstruction results */
  vector<UInt_t> fPi0Flag;
  vector<Int_t> fPi0Type;
  vector<Double_t> fPi0Energy;
  vector<Double_t> fPi0Mass;
  vector<vector<Double_t>> fPi0Momentum;
  vector<vector<Int_t>> fPi0PhotonTower;
  vector<vector<Double_t>> fPi0PhotonEnergy;
  vector<vector<vector<Double_t>>> fPi0PhotonPos;
  vector<Double_t> fPi0PhotonR;
  /* Pi0 true results */
  vector<UInt_t> fTruePi0Flag;
  vector<Int_t> fTruePi0Type;
  vector<Double_t> fTruePi0Energy;
  vector<Double_t> fTruePi0Mass;
  vector<vector<Double_t>> fTruePi0Momentum;
  vector<vector<Int_t>> fTruePi0PhotonTower;
  vector<vector<Double_t>> fTruePi0PhotonEnergy;
  vector<vector<vector<Double_t>>> fTruePi0PhotonPos;
  vector<Double_t> fTruePi0PhotonR;
  vector<Int_t> fTruePi0MultiHit;
  /* MC general true */
  vector<Int_t> fMcNhit;
  vector<vector<Int_t>> fMcPdgCode;
  vector<vector<Int_t>> fMcUserCode;
  vector<vector<Int_t>> fMcStatusCode;
  vector<vector<vector<Int_t>>> fMcParent;
  vector<vector<Double_t>> fMcEnergy;            // [tower][hit]
  vector<vector<vector<Double_t>>> fMcMomentum;  // [tower][x,y,z][hit]
  vector<vector<vector<Double_t>>> fMcPosCal;    // [tower][x,y][hit] calorimeter coordinate
  vector<vector<vector<Double_t>>> fMcPos;       // [tower][x,y][hit] beam center coordinate

  /* Detector(Lvl2) Information*/
  vector<vector<Double_t>> fdE;
  vector<Double_t> fFc;
  vector<Double_t> fZdc;
  /* Position Detector(Lvl2) Information*/
  vector<vector<vector<vector<Double_t>>>> fPosdE;

 public:
  void DisablePhoton() { fPhotonEnable = false; }

  void DisableNeutron() { fNeutronEnable = false; }

  void DisablePi0() { fPi0Enable = false; }

  void EnableTrue() { fTrueEnable = true; }

  void EnableLvl2() { fLvl2Enable = true; }

  void EnableLvl2Pos() { fLvl2PosEnable = true; }

  Int_t CreateTree();
  Int_t FillLvl3(Level3<armrec> *lvl3, EventCut<arman, armrec> *cut);
  Int_t FillLvl2(Level2<armclass> *lvl2);  // optional (dE, Zdc)
  Int_t FillMcEvent(McEvent *mc, EventCut<arman, armrec> *cut);
  void FillToTree();
  void WriteTree();
  void Clear();

  /* For systematic errors study */
  void SetPhotonEnergyFactor(Double_t factor) { fPhotonEnergyFactor = factor; }
  void SetBeamCentreOffset(Double_t offx, Double_t offy) {
    fBeamCentreOffset[0] = offx;
    fBeamCentreOffset[1] = offy;
  }
  void SetPionPidEff(Int_t eff) { fPionPidEff = eff; }

 private:
  void AllocateVectors();

 public:
  ClassDef(EventReduction, 1);
};

}  // namespace nLHCf

#endif
