#ifndef myselection_H
#define myselection_H

namespace nLHCf {

/*
 *   For personal event selection in Reduction.
 *    user can implement this function as you want, the function will be called in LHCfReduction::Reduction.
 *    if select the event, return true
 *    if reject the event, return false
 *
 *   argument should be
 *     level3 -> Level3<Arm1RecPars> or Level3<Arm2RecPars>
 *     evreduction -> EventReduction<Arm1Params, Arm1RecPars, Arm1AnPars> or
 *                    EventReduction<Arm2Params, Arm2RecPars, Arm2AnPars>
 *
 */

template <typename level3, typename evreduction>
Bool_t myselection(level3 *lvl3, evreduction) {
  /*
   *   IMPORTANT
   *   Do not merge to master branch if you modify this function for your own work.
   *   please change back this function to empty before merge.
   *
   */

  return true;
}

}  // namespace nLHCf

#endif