#ifndef LinkDefRec_H
#define LinkDefRec_H

#ifdef __CINT__
//#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ namespace nLHCf;
#pragma link C++ class nLHCf::EventReduction < nLHCf::Arm1Params, nLHCf::Arm1RecPars, nLHCf::Arm1AnPars> + ;
#pragma link C++ class nLHCf::EventReduction < nLHCf::Arm2Params, nLHCf::Arm2RecPars, nLHCf::Arm2AnPars> + ;
#pragma link C++ class nLHCf::LHCfReduction + ;

#endif

#endif
