#ifndef LHCfReduction_HH
#define LHCfReduction_HH

#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "Level2.hh"
#include "Level3.hh"

namespace nLHCf {

class LHCfReduction : public LHCfParams {
 public:
  LHCfReduction();
  ~LHCfReduction();

 private:
  /*--- Input/Output ---*/
  Int_t fFirstRun;
  Int_t fLastRun;
  TString fInputDir;
  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;
  TTree *fOutputTree;

  TString fComment;

  // enable/disable reconstruction for each Arm
  vector<Bool_t> fArmEnable;
  Bool_t fPhotonEnable;   // save photon reconstruction data
  Bool_t fNeutronEnable;  // save neutron reconstruction data
  Bool_t fPi0Enable;
  Bool_t fTrueEnable;
  Bool_t fLvl2Enable;
  Bool_t fLvl2PosEnable;
  UInt_t fTriggerFilter;       // Event filter for Trigger Flag
  Bool_t fModificationEnable;  // energy rescale
  Bool_t fBptxCut;             // should be true for data (default) and false for MC

  /* For systematic errors study */
  Double_t fPhotonEnergyFactor = 1.;
  Double_t fBeamCentreOffset[2] = {0., 0.};
  Int_t fPionPidEff = 90;

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }

  void SetFirstRun(Int_t first) { fFirstRun = first; }

  void SetLastRun(Int_t last) { fLastRun = last; }

  void SetOutputName(const Char_t *name) { fOutputName = name; }

  void DisableArm1() { fArmEnable[Arm1Params::kArmIndex] = false; }

  void DisableArm2() { fArmEnable[Arm2Params::kArmIndex] = false; }

  void DisablePhoton() { fPhotonEnable = false; }

  void DisableNeutron() { fNeutronEnable = false; }

  void DisablePi0() { fPi0Enable = false; }

  void EnableTrue() { fTrueEnable = true; }

  void EnableLvl2() { fLvl2Enable = true; }

  void EnableLvl2Pos() { fLvl2PosEnable = true; }

  void SetTriggerFilter(UInt_t filter) { fTriggerFilter = filter; }

  void DisableModification() { fModificationEnable = false; }

  void DisablefBptxCut() { fBptxCut = false; }

  void SetComment(const Char_t com[]) { fComment = com; }

  /* For systematic errors study */
  void SetPhotonEnergyFactor(Double_t factor) { fPhotonEnergyFactor = factor; }
  void SetBeamCentreOffset(Double_t offx, Double_t offy) {
    fBeamCentreOffset[0] = offx;
    fBeamCentreOffset[1] = offy;
  }
  void SetPionPidEff(Int_t eff) { fPionPidEff = eff; }

  /* Core function */
  void Reduction(Int_t first_ev, Int_t last_ev);

  void Reduction(Int_t last_ev) { Reduction(0, last_ev); }

  void Reduction() { Reduction(0, -1); }  // all events

 private:
  void SetInputTree();
  void OpenOutputFile();
  void WriteComment();
  void WriteToOutput();
  void CloseFiles();

  void AllocateVectors();

 public:
  ClassDef(LHCfReduction, 1);
};

}  // namespace nLHCf
#endif
