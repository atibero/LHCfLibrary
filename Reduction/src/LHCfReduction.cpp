#include "LHCfReduction.hh"

#include <dirent.h>

#include <cstdio>
using namespace std;

#include <TNamed.h>

#include "DataModification.hh"
#include "EventReduction.hh"
#include "Utils.hh"
#include "myselection.h"
using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(LHCfReduction);
#endif

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
LHCfReduction::LHCfReduction()
    : fInputDir("reconstructed.root"),
      fOutputName("reduction.root"),
      fPhotonEnable(true),
      fNeutronEnable(true),
      fPi0Enable(true),
      fTrueEnable(false),
      fLvl2Enable(false),
      fLvl2PosEnable(false),
      fTriggerFilter(0xFFF),
      fModificationEnable(true),
      fBptxCut(true),
      fComment("") {
  AllocateVectors();
  UT::ClearVector1D(fArmEnable, true);
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
LHCfReduction::~LHCfReduction() {}

/*------------------------*/
/*--- Open Input File  ---*/
/*------------------------*/
void LHCfReduction::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/rec_run%05d.root", fInputDir.Data(), irun));
      UT::Printf(UT::kPrintDebug, "adding file %s\n", iname.Data());
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
        UT::Printf(UT::kPrintDebug, "added file %s\n", iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void LHCfReduction::OpenOutputFile() {
  UT::Printf(UT::kPrintInfo, "Opening output file...");
  fflush(stdout);

  fOutputFile = new TFile(fOutputName.Data(), "RECREATE");
  if (!fOutputFile->IsOpen()) {
    UT::Printf(UT::kPrintError, "Error: output file \"%s\" not opened\n", fOutputName.Data());
    exit(EXIT_FAILURE);
  }
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void LHCfReduction::WriteComment() {
  if (fComment == "") return;

  TNamed info("info", fComment.Data());

  fOutputFile->cd();
  info.Write();
  gROOT->cd();
}

void LHCfReduction::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write("", TObject::kOverwrite);

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void LHCfReduction::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing files...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*------------------*/
/*--- Reduction  ---*/
/*------------------*/
void LHCfReduction::Reduction(Int_t first_ev, Int_t last_ev) {
  /* Initialization */
  // Tree
  SetInputTree();
  OpenOutputFile();

  // Reduction function
  EventReduction<Arm1Params, Arm1RecPars, Arm1AnPars> reduction_a1;
  EventReduction<Arm2Params, Arm2RecPars, Arm2AnPars> reduction_a2;

  fOutputFile->cd();
  if (fArmEnable[0]) {
    if (!fPhotonEnable) reduction_a1.DisablePhoton();
    if (!fNeutronEnable) reduction_a1.DisableNeutron();
    if (!fPi0Enable) reduction_a1.DisablePi0();
    if (fTrueEnable) reduction_a1.EnableTrue();
    if (fLvl2Enable) reduction_a1.EnableLvl2();
    if (fLvl2PosEnable) reduction_a1.EnableLvl2Pos();
    reduction_a1.SetPhotonEnergyFactor(fPhotonEnergyFactor);
    reduction_a1.SetBeamCentreOffset(fBeamCentreOffset[0], fBeamCentreOffset[1]);
    reduction_a1.SetPionPidEff(fPionPidEff);
    reduction_a1.CreateTree();
  }
  if (fArmEnable[1]) {
    if (!fPhotonEnable) reduction_a2.DisablePhoton();
    if (!fNeutronEnable) reduction_a2.DisableNeutron();
    if (!fPi0Enable) reduction_a2.DisablePi0();
    if (fTrueEnable) reduction_a2.EnableTrue();
    if (fLvl2Enable) reduction_a2.EnableLvl2();
    if (fLvl2PosEnable) reduction_a2.EnableLvl2Pos();
    reduction_a2.SetPhotonEnergyFactor(fPhotonEnergyFactor);
    reduction_a2.SetBeamCentreOffset(fBeamCentreOffset[0], fBeamCentreOffset[1]);
    reduction_a2.SetPionPidEff(fPionPidEff);
    reduction_a2.CreateTree();
  }
  gROOT->cd();

  // Event Cut
  EventCut<Arm1AnPars, Arm1RecPars> cut_a1;
  EventCut<Arm2AnPars, Arm2RecPars> cut_a2;
  TString photon_cuts = "pid energy position multi-hit";
  TString neutron_cuts = "pid energy position";
  if (fBptxCut) {
    photon_cuts += " bptx";
    neutron_cuts += " bptx";
  }
  cut_a1.SetPhotonCuts(photon_cuts);
  cut_a1.SetNeutronCuts(neutron_cuts);
  cut_a2.SetPhotonCuts(photon_cuts);
  cut_a2.SetNeutronCuts(neutron_cuts);

  // Lvl3 Modification
  DataModification<Arm1AnPars, Arm1RecPars, Arm1Params> a1_modify;
  DataModification<Arm2AnPars, Arm2RecPars, Arm2Params> a2_modify;

  /* ======== Event loop =========== */
  int nev = 0;
  int nfill_a1 = 0, nfill_a2 = 0;
  int nevmax = fInputTree->GetEntries();
  UT::Printf(UT::kPrintInfo, "Total events: %d\n", nevmax);
  UT::Printf(UT::kPrintInfo, "Start event loop....\n");
  for (int ie = 0; ie < nevmax; ie++) {
    Int_t ievlocal = fInputTree->LoadTree(ie);
    if (ievlocal < 0) break;

    if (ie % 1000 == 0) {
      UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    fInputTree->GetEntry(ie);

    /* Arm1 event */
    if (fArmEnable[0]) {
      // clear values and vectors
      reduction_a1.Clear();
      Bool_t fill_tree = false;
      Level3<Arm1RecPars> *lvl3_a1 = NULL;
      McEvent *true_a1 = NULL;
      if (fInputEv->Check("lvl3_a1")) {
        lvl3_a1 = (Level3<Arm1RecPars> *)fInputEv->Get("lvl3_a1");
        if (fModificationEnable) a1_modify.Modify(lvl3_a1);
        Int_t fill_lvl3 = reduction_a1.FillLvl3(lvl3_a1, &cut_a1);
        fill_tree = fill_tree || fill_lvl3;
      }
      if (fInputEv->Check("true_a1")) {
        true_a1 = (McEvent *)fInputEv->Get("true_a1");
        Int_t fill_mc = reduction_a1.FillMcEvent(true_a1, &cut_a1);
        fill_tree = fill_tree || fill_mc;
      }
      if (fLvl2Enable && fInputEv->Check("lvl2_a1")) {
        Int_t fill_lvl2 = reduction_a1.FillLvl2((Level2<Arm1Params> *)fInputEv->Get("lvl2_a1"));
        fill_tree = fill_tree || fill_lvl2;
      }
      // event selection
      if (fill_tree && ((reduction_a1.fTrigger & fTriggerFilter) || true_a1 != NULL) &&
          myselection(lvl3_a1, &reduction_a1)) {
        reduction_a1.FillToTree();
        nfill_a1++;
      }
    }
    /* Arm2 event */
    if (fArmEnable[1]) {
      // clear values and vectors
      reduction_a2.Clear();
      Bool_t fill_tree = false;
      Level3<Arm2RecPars> *lvl3_a2 = NULL;
      McEvent *true_a2 = NULL;
      if (fInputEv->Check("lvl3_a2")) {
        lvl3_a2 = (Level3<Arm2RecPars> *)fInputEv->Get("lvl3_a2");
        Int_t fill_lvl3 = reduction_a2.FillLvl3(lvl3_a2, &cut_a2);
        fill_tree = fill_tree || fill_lvl3;
      }
      if (fInputEv->Check("true_a2")) {
        true_a2 = (McEvent *)fInputEv->Get("true_a2");
        Int_t fill_mc = reduction_a2.FillMcEvent(true_a2, &cut_a2);
        fill_tree = fill_tree || fill_mc;
      }
      if (fLvl2Enable && fInputEv->Check("lvl2_a2")) {
        Int_t fill_lvl2 = reduction_a2.FillLvl2((Level2<Arm2Params> *)fInputEv->Get("lvl2_a2"));
        fill_tree = fill_tree || fill_lvl2;
      }
      // event selection
      if (fill_tree && ((reduction_a2.fTrigger & fTriggerFilter) || true_a2 != NULL) &&
          myselection(lvl3_a2, &reduction_a2)) {
        reduction_a2.FillToTree();
        nfill_a2++;
      }
    }

    /* Clear event */
    fInputEv->HeaderClear();
    fInputEv->ObjDelete();
    nev++;

    if (last_ev > 0 && nev >= last_ev) break;
  }

  UT::Printf(UT::kPrintInfo, "\nConverted total %d events (A1 filled %d, A2 filled %d)\n", nev, nfill_a1, nfill_a2);

  /* Write to file */
  WriteComment();
  WriteToOutput();
  CloseFiles();
}

/*------------------------------*/
/*--- Allocate/clear vectors ---*/
/*------------------------------*/
void LHCfReduction::AllocateVectors() { Utils::AllocateVector1D(fArmEnable, this->kNarm); }
