#include "EventReduction.hh"

#include <TROOT.h>
#include <TVector3.h>

#include "Arm1AnPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "PionRec.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(EventReduction);
#endif

typedef Utils UT;
typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass, typename armrec, typename arman>
EventReduction<armclass, armrec, arman>::EventReduction()
    : fTree(NULL),
      fPhotonEnable(true),
      fNeutronEnable(true),
      fPi0Enable(true),
      fTrueEnable(false),
      fLvl2Enable(false),
      fLvl2PosEnable(false) {
  ;
}

template <typename armclass, typename armrec, typename arman>
EventReduction<armclass, armrec, arman>::~EventReduction() {
  ;
}

template <typename armclass, typename armrec, typename arman>
Int_t EventReduction<armclass, armrec, arman>::CreateTree() {
  TString treename;
  if (this->kArmIndex == Arm1Params::kArmIndex) {
    treename = "lhcfarm1";
  } else if (this->kArmIndex == Arm2Params::kArmIndex) {
    treename = "lhcfarm2";
  } else {
    return -1;
  }

  fTree = new TTree(treename, "LHCf data");

  /* == Create branches ==  */
  AllocateVectors();
  /* Event Information */
  fTree->Branch("arm", &fArm, "arm/I");
  fTree->Branch("run", &fRun, "run/I");
  fTree->Branch("gnumber", &fGnumber, "gnumber/I");
  fTree->Branch("number", &fNumber, "number/I");
  fTree->Branch("time", &fTime, "time/i");
  fTree->Branch("time_usec", &fTimeUsec, "time_usec/i");
  fTree->Branch("trigger", &fTrigger, "trigger/i");
  fTree->Branch("beam", &fBeam, "beam/i");
  fTree->Branch("bcid", &fBcid, "bcid/i");
  fTree->Branch("quality_flag", &fQualityFlag, "quality_flag/i");
  fTree->Branch("atlas_trigger_accept", &fAtlasTriggerAccept, "atlas_trigger_accept/O");
  fTree->Branch("atlas_ecr", &fAtlasECR, "atlas_ecr/i");
  fTree->Branch("atlas_l1id", &fAtlasL1id, "atlas_l1id/i");

  /* Photon Reconstruction Results*/
  if (fPhotonEnable) {
    fTree->Branch("photon_nhit", &fPhotonNhit);
    fTree->Branch("photon_flag", &fPhotonFlag);
    fTree->Branch("photon_energy", &fPhotonEnergy);
    fTree->Branch("photon_pid", &fPhotonPid);
    fTree->Branch("photon_poscal", &fPhotonPosCal);
    fTree->Branch("photon_pos", &fPhotonPos);
    fTree->Branch("photon_l20", &fPhotonL20);
    fTree->Branch("photon_l90", &fPhotonL90);
  }
  /* Neutron Reconstruction Results*/
  if (fNeutronEnable) {
    fTree->Branch("neutron_flag", &fNeutronFlag);
    fTree->Branch("neutron_energy", &fNeutronEnergy);
    fTree->Branch("neutron_pid", &fNeutronPid);
    fTree->Branch("neutron_poscal", &fNeutronPosCal);
    fTree->Branch("neutron_pos", &fNeutronPos);
    fTree->Branch("neutron_l20", &fNeutronL20);
    fTree->Branch("neutron_l90", &fNeutronL90);
  }
  /* Pi0 Reconstruction Results*/
  if (fPi0Enable) {
    fTree->Branch("pi0_flag", &fPi0Flag);
    fTree->Branch("pi0_type", &fPi0Type);
    fTree->Branch("pi0_energy", &fPi0Energy);
    fTree->Branch("pi0_mass", &fPi0Mass);
    fTree->Branch("pi0_momentum", &fPi0Momentum);
    fTree->Branch("pi0_photon_tower", &fPi0PhotonTower);
    fTree->Branch("pi0_photon_energy", &fPi0PhotonEnergy);
    fTree->Branch("pi0_photon_pos", &fPi0PhotonPos);
    fTree->Branch("pi0_photon_r", &fPi0PhotonR);
  }
  /* Mc True */
  if (fTrueEnable) {
    fTree->Branch("mc_nhit", &fMcNhit);
    fTree->Branch("mc_pdgcode", &fMcPdgCode);
    fTree->Branch("mc_usercode", &fMcUserCode);
    fTree->Branch("mc_statuscode", &fMcStatusCode);
    fTree->Branch("mc_parent", &fMcParent);
    fTree->Branch("mc_energy", &fMcEnergy);
    fTree->Branch("mc_momentum", &fMcMomentum);
    fTree->Branch("mc_pos", &fMcPos);
    fTree->Branch("mc_poscal", &fMcPosCal);
    if (fPi0Enable) {
      fTree->Branch("pi0_true_flag", &fTruePi0Flag);
      fTree->Branch("pi0_true_type", &fTruePi0Type);
      fTree->Branch("pi0_true_energy", &fTruePi0Energy);
      fTree->Branch("pi0_true_mass", &fTruePi0Mass);
      fTree->Branch("pi0_true_momentum", &fTruePi0Momentum);
      fTree->Branch("pi0_true_photon_tower", &fTruePi0PhotonTower);
      fTree->Branch("pi0_true_photon_energy", &fTruePi0PhotonEnergy);
      fTree->Branch("pi0_true_photon_pos", &fTruePi0PhotonPos);
      fTree->Branch("pi0_true_photon_r", &fTruePi0PhotonR);
      fTree->Branch("pi0_true_multihit", &fTruePi0MultiHit);
    }
  }

  /* data from Lvl2 */
  if (fLvl2Enable) {
    fTree->Branch("dE", &fdE);
    fTree->Branch("fc", &fFc);
    fTree->Branch("zdc", &fZdc);
  }

  /*position detector data from Lvl2 */
  if (fLvl2PosEnable) {
    fTree->Branch("PosdE", &fPosdE);
  }

  return 0;
}

template <typename armclass, typename armrec, typename arman>
void EventReduction<armclass, armrec, arman>::Clear() {
  fArm = 0;
  fRun = 0;
  fNumber = 0;
  fGnumber = 0;
  fTime = 0;
  fTimeUsec = 0;
  fBcid = 0;
  fTrigger = 0;
  fBeam = 0;
  fQualityFlag = 0;

  /* ATLAS info*/
  fAtlasTriggerAccept = 0;
  fAtlasECR = 0;
  fAtlasL1id = 0;

  if (fPhotonEnable) {
    UT::ClearVector1D(fPhotonNhit);
    UT::ClearVector2D(fPhotonFlag);
    UT::ClearVector2D(fPhotonEnergy);
    UT::ClearVector2D(fPhotonPid);
    UT::ClearVector3D(fPhotonPos);
    UT::ClearVector3D(fPhotonPosCal);
    UT::ClearVector1D(fPhotonL90);
    UT::ClearVector1D(fPhotonL20);
  }
  if (fNeutronEnable) {
    UT::ClearVector1D(fNeutronFlag);
    UT::ClearVector1D(fNeutronEnergy);
    UT::ClearVector1D(fNeutronPid);
    UT::ClearVector2D(fNeutronPos);
    UT::ClearVector2D(fNeutronPosCal);
    UT::ClearVector1D(fNeutronL90);
    UT::ClearVector1D(fNeutronL20);
  }
  if (fPi0Enable) {
    UT::ClearVector1D(fPi0Flag);
    UT::ClearVector1D(fPi0Type);
    UT::ClearVector1D(fPi0Energy);
    UT::ClearVector1D(fPi0Mass);
    UT::ClearVector2D(fPi0Momentum);
    UT::ClearVector2D(fPi0PhotonTower);
    UT::ClearVector2D(fPi0PhotonEnergy);
    UT::ClearVector3D(fPi0PhotonPos);
    UT::ClearVector1D(fPi0PhotonR);
  }
  if(fTrueEnable) {
    UT::ClearVector1D(fMcNhit);
    Utils::AllocateVector2D(fMcPdgCode, this->kCalNtower + 1, 0);
    Utils::AllocateVector2D(fMcUserCode, this->kCalNtower + 1, 0);
    Utils::AllocateVector2D(fMcStatusCode, this->kCalNtower + 1, 0);
    Utils::AllocateVector3D(fMcParent, this->kCalNtower + 1, 2, 0);
    Utils::AllocateVector2D(fMcEnergy, this->kCalNtower + 1, 0);
    Utils::AllocateVector3D(fMcMomentum, this->kCalNtower + 1, this->kPosNview + 1, 0);
    Utils::AllocateVector3D(fMcPos, this->kCalNtower + 1, this->kPosNview, 0);
    Utils::AllocateVector3D(fMcPosCal, this->kCalNtower + 1, this->kPosNview, 0);

    if (fPi0Enable) {
      UT::ClearVector1D(fTruePi0Flag);
      UT::ClearVector1D(fTruePi0Type);
      UT::ClearVector1D(fTruePi0Energy);
      UT::ClearVector1D(fTruePi0Mass);
      UT::ClearVector2D(fTruePi0Momentum);
      UT::ClearVector2D(fTruePi0PhotonTower);
      UT::ClearVector2D(fTruePi0PhotonEnergy);
      UT::ClearVector3D(fTruePi0PhotonPos);
      UT::ClearVector1D(fTruePi0PhotonR);
    }
  }
}

template <typename armclass, typename armrec, typename arman>
Int_t EventReduction<armclass, armrec, arman>::FillLvl3(Level3<armrec> *lvl3, EventCut<arman, armrec> *cut) {
  // TODO [TrackProjection]: This was moved to ArmXAnPars.cpp, but needed here to keep beam center uncertainty
  /*
  CT::SetBeamCrossingAngle(this->kArmIndex, this->kCrossingAngle);
  CT::SetDetectorPosition(this->kArmIndex, this->kDetectorOffset[1]);
  CT::SetMeasuredBeamCentre(this->kArmIndex,  //
                            this->kBeamCentre[0] + fBeamCentreOffset[0],
                            this->kBeamCentre[1] + fBeamCentreOffset[1],  //
                            this->kBeamCentreAverageLayer[0], this->kBeamCentreAverageLayer[1]);
  CT::SetDetectorPositionVector(this->kArmIndex);
  CT::SetBeamCentreVector(this->kArmIndex);
  CT::PrintParameters(this->kArmIndex, "EventReduction");
  */
  //

  // for energy scale systematic error estimation
  for (int it = 0; it < this->kCalNtower; ++it) {
    lvl3->fPhotonEnergy[it] *= fPhotonEnergyFactor;
    lvl3->fPhotonEnergyLin[it] *= fPhotonEnergyFactor;
    for (int ip = 0; ip < lvl3->fPhotonEnergyMH[it].size(); ++ip) lvl3->fPhotonEnergyMH[it][ip] *= fPhotonEnergyFactor;
  }

  /* Event Information */
  fArm = lvl3->kArmIndex + 1;
  fRun = lvl3->fRun;
  fNumber = lvl3->fEvent;
  fGnumber = lvl3->fGevent;
  fTime = (UInt_t)(lvl3->fTime[0] + 0.001);
  fTimeUsec = (UInt_t)(lvl3->fTime[1] + 0.001);
  fBcid = lvl3->BunchID();
  fTrigger = (lvl3->IsShowerTrg() ? 1 : 0) + (lvl3->IsPi0Trg() ? 2 : 0) + (lvl3->IsHighEMTrg() ? 4 : 0) +
             (lvl3->IsHadronTrg() ? 8 : 0) + (lvl3->IsZdcTrg() ? 16 : 0) + (lvl3->IsFcTrg() ? 32 : 0) +
             (lvl3->IsL1tTrg() ? 64 : 0) + (lvl3->IsLaserTrg() ? 128 : 0) + (lvl3->IsPedestalTrg() ? 256 : 0);
  fBeam = (lvl3->IsBeam1() ? 1 : 0) + (lvl3->IsBeam2() ? 0 : 2);
  fQualityFlag = 0x1;  // TODO

  /* ATLAS info*/
  fAtlasTriggerAccept = lvl3->IsAtlasL1A();
  fAtlasECR = lvl3->AtlasEcr();
  fAtlasL1id = lvl3->AtlasL1ID();

  /* Photon Reconstruction Results */
  Bool_t filled_photon = false;
  if (fPhotonEnable) {
    for (int i = 0; i < this->kCalNtower; ++i) {
      // The number of hit in the tower
      // Currently it is always =1. This is for future extension.
      fPhotonNhit[i] = 1;

      fPhotonEnergy[0][i] = lvl3->fPhotonEnergy[i];
      fPhotonPid[0][i] = lvl3->fIsPhoton[i];
      fPhotonPosCal[0][i][0] = lvl3->fPhotonPosition[i][0];
      fPhotonPosCal[0][i][1] = lvl3->fPhotonPosition[i][1];
      TVector3 gpos = cut->PhotonGlobalPosition(lvl3, i);
      fPhotonPos[0][i][0] = gpos.X();
      fPhotonPos[0][i][1] = gpos.Y();
      fPhotonL20[i] = lvl3->fPhotonL20[i];
      fPhotonL90[i] = lvl3->fPhotonL90[i];
      /* analysis flag */
      fPhotonFlag[0][i] = 0;
      if (cut->BPTXCut(lvl3)) fPhotonFlag[0][i] += (1 << 4);               // selection of colliding bunch events
      if (lvl3->fSoftwareTrigger[i]) fPhotonFlag[0][i] += (1 << 5);        // software trigger
      if (cut->PhotonPIDCut(lvl3, i)) fPhotonFlag[0][i] += (1 << 6);       // PID selection
      if (cut->PhotonEnergyCut(lvl3, i)) fPhotonFlag[0][i] += (1 << 7);    // energy cut
      if (cut->PhotonPositionCut(lvl3, i)) fPhotonFlag[0][i] += (1 << 8);  // Position cut (2mm edge cut)
      if (cut->PhotonMultiHitCut(lvl3, i)) fPhotonFlag[0][i] += (1 << 9);  // Multi-hit cut
      if (cut->CutPhotonEventTower(lvl3, i)) fPhotonFlag[0][i] += 1;       // overall selection
    }
    filled_photon = true;
  }

  /* Neutron Reconstruction Results */
  Bool_t filled_neutron = false;
  if (fNeutronEnable) {
    for (int i = 0; i < this->kCalNtower; ++i) {
      fNeutronEnergy[i] = lvl3->fNeutronEnergy[i];
      fNeutronPid[i] = lvl3->fIsNeutron[i];
      fNeutronPosCal[i][0] = lvl3->fNeutronPosition[i][0];
      fNeutronPosCal[i][1] = lvl3->fNeutronPosition[i][1];
      TVector3 gpos = cut->NeutronGlobalPosition(lvl3, i);
      fNeutronPos[i][0] = gpos.X();
      fNeutronPos[i][1] = gpos.Y();
      fNeutronL20[i] = lvl3->fNeutronL20[i];
      fNeutronL90[i] = lvl3->fNeutronL90[i];
      /* analysis flag */
      fNeutronFlag[i] = 0;
      if (cut->BPTXCut(lvl3)) fNeutronFlag[i] += (1 << 4);                // selection of colliding bunch events
      if (lvl3->fSoftwareTrigger[i]) fNeutronFlag[i] += (1 << 5);         // software trigger
      if (cut->NeutronPIDCut(lvl3, i)) fNeutronFlag[i] += (1 << 6);       // PID selection
      if (cut->NeutronEnergyCut(lvl3, i)) fNeutronFlag[i] += (1 << 7);    // energy cut
      if (cut->NeutronPositionCut(lvl3, i)) fNeutronFlag[i] += (1 << 8);  // Position cut (2mm edge cut)
      if (cut->NeutronMultiHitCut(lvl3, i)) fNeutronFlag[i] += (1 << 9);  // Multi-hit cut
      if (cut->CutNeutronEventTower(lvl3, i)) fNeutronFlag[i] += 1;       // overall selection
    }
    filled_neutron = true;
  }

  /* Pi0 Reconstruction Results */
  Bool_t filled_pi0 = false;
  if (fPi0Enable) {
    PionRec pirec;
    pirec.Reconstruct<Level3<armrec>, EventCut<arman, armrec>>(lvl3, cut, fPionPidEff);

    for (int i = 0; i < PionRec::kNtypes; ++i) {
      fPi0Flag[i] = pirec.fResult[i].fFlag;
      fPi0Type[i] = pirec.fResult[i].GetType();
      fPi0Energy[i] = pirec.fResult[i].GetEnergy();
      fPi0Mass[i] = pirec.fResult[i].GetMass();
      fPi0Momentum[i][0] = pirec.fResult[i].GetMomentum().X();
      fPi0Momentum[i][1] = pirec.fResult[i].GetMomentum().Y();
      fPi0Momentum[i][2] = pirec.fResult[i].GetMomentum().Z();

      for (int j = 0; j < 2; ++j) {
        fPi0PhotonTower[i][j] = pirec.fResult[i].fPhotonTower[j];
        fPi0PhotonEnergy[i][j] = pirec.fResult[i].fPhotonEnergy[j];
        fPi0PhotonPos[i][j][0] = pirec.fResult[i].fPhotonPos[j][0];
        fPi0PhotonPos[i][j][1] = pirec.fResult[i].fPhotonPos[j][1];
      }
      fPi0PhotonR[i] = pirec.fResult[i].fPhotonR;
      if (fPi0Flag[i] & 0x1) filled_pi0 = true;
    }
  }
  return filled_photon || filled_neutron || filled_pi0;
}

template <typename armclass, typename armrec, typename arman>
Int_t EventReduction<armclass, armrec, arman>::FillMcEvent(McEvent *mc, EventCut<arman, armrec> *cut) {
  const Double_t energy_threshold = 0.;
  const Double_t edge_cut = 0.;

  Bool_t filled_mc = false, filled_mc_pi0 = false;

  if (fTrueEnable && mc != NULL) {
    /* === Mc General === */

    /* Particle */
    Int_t n = mc->GetN();
    Int_t nhit[3] = {0};
    for (int i = 0; i < n; ++i) {
      McParticle *p = mc->Get(i);
      int hit = 2; // 0: TS, 1:TL, 2: out of the acceptance
      for (int j = 0; j < this->kCalNtower; ++j) {
        if(p->CheckHit(this->kArmIndex, j, energy_threshold, edge_cut)) {
          hit = j;
          break;
        }
      }
      fMcPdgCode[hit].push_back(p->PdgCode());
      fMcUserCode[hit].push_back(p->fUserCode);
      fMcStatusCode[hit].push_back(p->fStatusCode);
      if(this->fOperation == kLHC2022) {
        fMcParent[hit][0].push_back(p->GetParentID());
        fMcParent[hit][1].push_back(p->GetChildrenID());
      } else {
        fMcParent[hit][0].push_back(p->fParent[0]);
        fMcParent[hit][1].push_back(p->fParent[1]);
      }
      fMcEnergy[hit].push_back(p->Energy());
      fMcMomentum[hit][0].push_back(p->MomentumX());
      fMcMomentum[hit][1].push_back(p->MomentumY());
      fMcMomentum[hit][2].push_back(p->MomentumZ());
      fMcPos[hit][0].push_back(p->X_Col(cut->kArmIndex));
      fMcPos[hit][1].push_back(p->Y_Col(cut->kArmIndex));
      if(hit < this->kCalNtower ) {
        fMcPosCal[hit][0].push_back(p->X_Cal(cut->kArmIndex, hit));
        fMcPosCal[hit][1].push_back(p->Y_Cal(cut->kArmIndex, hit));
      }
      nhit[hit]++;
    }

    fMcNhit[0] = nhit[0];
    fMcNhit[1] = nhit[1];
    fMcNhit[2] = nhit[2];

    /* === Pi0 Specific === */
    if (fPi0Enable) {
      PionRec pirec;
      pirec.Reconstruct<EventCut<arman, armrec>>(mc, cut);
      for (int i = 0; i < PionRec::kNtypes; ++i) {
        fTruePi0Flag[i] = pirec.fResult[i].fFlag;
        fTruePi0Type[i] = pirec.fResult[i].GetType();
        fTruePi0Energy[i] = pirec.fResult[i].GetEnergy();
        fTruePi0Mass[i] = pirec.fResult[i].GetMass();
        fTruePi0Momentum[i][0] = pirec.fResult[i].GetMomentum().X();
        fTruePi0Momentum[i][1] = pirec.fResult[i].GetMomentum().Y();
        fTruePi0Momentum[i][2] = pirec.fResult[i].GetMomentum().Z();
        for (int j = 0; j < 2; ++j) {
          fTruePi0PhotonTower[i][j] = pirec.fResult[i].fPhotonTower[j];
          fTruePi0PhotonEnergy[i][j] = pirec.fResult[i].fPhotonEnergy[j];
          fTruePi0PhotonPos[i][j][0] = pirec.fResult[i].fPhotonPos[j][0];
          fTruePi0PhotonPos[i][j][1] = pirec.fResult[i].fPhotonPos[j][1];
        }
        fTruePi0PhotonR[i] = pirec.fResult[i].fPhotonR;
        fTruePi0MultiHit[i] = pirec.fResult[i].fTrueMH ? 1 : 0;
        if (fTruePi0Flag[i] & 0x1) filled_mc_pi0 = true;
      }
    }
  }

  return filled_mc || filled_mc_pi0;
}

template <typename armclass, typename armrec, typename arman>
Int_t EventReduction<armclass, armrec, arman>::FillLvl2(Level2<armclass> *lvl2) {
  Bool_t filled_lvl2 = false, filled_lvl2_pos = false;
  if (fLvl2Enable) {
    for (Int_t i = 0; i < this->kCalNtower; ++i) {
      for (Int_t j = 0; j < this->kCalNlayer; ++j) {
        fdE[i][j] = lvl2->fCalorimeter[i][j];
      }
    }
    for (Int_t i = 0; i < this->kFcNlayer; ++i) {
      fFc[i] = lvl2->fFrontCounter[i];
    }
    for (Int_t i = 0; i < this->kZdcNchannel; ++i) {
      fZdc[i] = lvl2->fZdc[i];
    }
    filled_lvl2 = true;
  }
  if (fLvl2PosEnable) {
    Int_t sample = this->kArmIndex == 0 ? 0 : 1;
    for (Int_t t = 0; t < this->kPosNtower; ++t) {
      for (Int_t i = 0; i < this->kPosNlayer; ++i) {
        for (Int_t j = 0; j < this->kPosNview; ++j) {
          for (Int_t k = 0; k < this->kPosNchannel[t]; ++k) {
            fPosdE[t][i][j][k] = lvl2->fPosDet[t].at(i).at(j).at(k).at(sample);
          }
        }
      }
    }
    filled_lvl2_pos = true;
    UT::Printf(UT::kPrintDebug, "\nFilled pos detector lvl2\n");
  }
  return filled_lvl2 || filled_lvl2_pos;
}

template <typename armclass, typename armrec, typename arman>
void EventReduction<armclass, armrec, arman>::FillToTree() {
  fTree->Fill();
}

template <typename armclass, typename armrec, typename arman>
void EventReduction<armclass, armrec, arman>::WriteTree() {
  fTree->Write("", TObject::kOverwrite);
}

template <typename armclass, typename armrec, typename arman>
void EventReduction<armclass, armrec, arman>::AllocateVectors() {
  if (fPhotonEnable) {
    const UInt_t default_nhit = 1;
    Utils::AllocateVector1D(fPhotonNhit, this->kCalNtower);
    Utils::AllocateVector2D(fPhotonFlag, default_nhit, this->kCalNtower);
    Utils::AllocateVector2D(fPhotonEnergy, default_nhit, this->kCalNtower);
    Utils::AllocateVector2D(fPhotonPid, default_nhit, this->kCalNtower);
    Utils::AllocateVector3D(fPhotonPosCal, default_nhit, this->kCalNtower, this->kPosNview);
    Utils::AllocateVector3D(fPhotonPos, default_nhit, this->kCalNtower, this->kPosNview);
    Utils::AllocateVector1D(fPhotonL20, this->kCalNtower);
    Utils::AllocateVector1D(fPhotonL90, this->kCalNtower);
  }

  if (fNeutronEnable) {
    Utils::AllocateVector1D(fNeutronFlag, this->kCalNtower);
    Utils::AllocateVector1D(fNeutronEnergy, this->kCalNtower);
    Utils::AllocateVector1D(fNeutronPid, this->kCalNtower);
    Utils::AllocateVector2D(fNeutronPosCal, this->kCalNtower, this->kPosNview);
    Utils::AllocateVector2D(fNeutronPos, this->kCalNtower, this->kPosNview);
    Utils::AllocateVector1D(fNeutronL20, this->kCalNtower);
    Utils::AllocateVector1D(fNeutronL90, this->kCalNtower);
  }

  if (fPi0Enable) {
    Utils::AllocateVector1D(fPi0Flag, PionRec::kNtypes);
    Utils::AllocateVector1D(fPi0Type, PionRec::kNtypes);
    Utils::AllocateVector1D(fPi0Energy, PionRec::kNtypes);
    Utils::AllocateVector1D(fPi0Mass, PionRec::kNtypes);
    Utils::AllocateVector2D(fPi0Momentum, PionRec::kNtypes, 3);
    Utils::AllocateVector2D(fPi0PhotonTower, PionRec::kNtypes, 2);
    Utils::AllocateVector2D(fPi0PhotonEnergy, PionRec::kNtypes, 2);
    Utils::AllocateVector3D(fPi0PhotonPos, PionRec::kNtypes, 2, this->kPosNview);
    Utils::AllocateVector1D(fPi0PhotonR, PionRec::kNtypes);
  }

  if (fTrueEnable) {
    Utils::AllocateVector1D(fMcNhit, this->kCalNtower + 1);
    Utils::AllocateVector2D(fMcPdgCode, this->kCalNtower + 1, 0);
    Utils::AllocateVector2D(fMcUserCode, this->kCalNtower + 1, 0);
    Utils::AllocateVector2D(fMcStatusCode, this->kCalNtower + 1, 0);
    Utils::AllocateVector3D(fMcParent, this->kCalNtower + 1, 2, 0);
    Utils::AllocateVector2D(fMcEnergy, this->kCalNtower + 1, 0);
    Utils::AllocateVector3D(fMcMomentum, this->kCalNtower + 1, this->kPosNview + 1, 0);
    Utils::AllocateVector3D(fMcPos, this->kCalNtower + 1, this->kPosNview, 0);
    Utils::AllocateVector3D(fMcPosCal, this->kCalNtower + 1, this->kPosNview, 0);
    if (fPi0Enable) {
      Utils::AllocateVector1D(fTruePi0Flag, PionRec::kNtypes);
      Utils::AllocateVector1D(fTruePi0Type, PionRec::kNtypes);
      Utils::AllocateVector1D(fTruePi0Energy, PionRec::kNtypes);
      Utils::AllocateVector1D(fTruePi0Mass, PionRec::kNtypes);
      Utils::AllocateVector2D(fTruePi0Momentum, PionRec::kNtypes, 3);
      Utils::AllocateVector2D(fTruePi0PhotonTower, PionRec::kNtypes, 2);
      Utils::AllocateVector2D(fTruePi0PhotonEnergy, PionRec::kNtypes, 2);
      Utils::AllocateVector3D(fTruePi0PhotonPos, PionRec::kNtypes, 2, this->kPosNview);
      Utils::AllocateVector1D(fTruePi0PhotonR, PionRec::kNtypes);
      Utils::AllocateVector1D(fTruePi0MultiHit, PionRec::kNtypes);
    }
  }

  if (fLvl2Enable) {
    Utils::AllocateVector2D(fdE, this->kCalNtower, this->kCalNlayer);
    Utils::AllocateVector1D(fFc, this->kFcNlayer);
    Utils::AllocateVector1D(fZdc, this->kZdcNchannel);
  }

  if (fLvl2PosEnable) {
    Int_t nPosTow = this->kArmIndex == 0 ? 1 : 0;
    Utils::AllocateVector4D(fPosdE, this->kPosNtower, this->kPosNlayer, this->kPosNview, this->kPosNchannel[nPosTow]);
  }
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventReduction<Arm1Params, Arm1RecPars, Arm1AnPars>;
template class EventReduction<Arm2Params, Arm2RecPars, Arm2AnPars>;
}  // namespace nLHCf
