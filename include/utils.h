#include <getopt.h>

#include <cstdio>
#include <cstdlib>

#define STRLEN 1024

/* Automatically build getopt option-characters string */
void build_getopt_str(const struct option *opt, int nopt, char *str);
