#ifndef PionData_HH
#define PionData_HH

#include <Math/Vector4D.h>
#include <TMath.h>
#include <TNamed.h>
#include <TVector3.h>

#include "Level3.hh"
#include "McEvent.hh"

using namespace std;

namespace nLHCf {

enum PionType { kTypeI = 0, kTypeII_ST = 1, kTypeII_LT = 2, kNone = -1 };

class PionData : public TNamed {
 public:
  PionData() { Clear(); }
  PionData(const char *name, const char *title) : TNamed(name, title) { Clear(); }
  ~PionData() { ; }

  void SetType(PionType type) { fType = type; }
  PionType GetType() { return fType; }

  // void SetMass(Double_t mass) { fMass = mass; }
  // Double_t GetMass() { return fMass; }
  Double_t GetMass() { return fMomentum.M() * 1000.; }  // [MeV]

  void SetLorentzVector(ROOT::Math::PxPyPzEVector mom) { fMomentum = mom; }
  ROOT::Math::PxPyPzEVector GetLorentzVector() { return fMomentum; }

  void SetMomentum(TVector3 mom) {
    fMomentum.SetPx(mom.X());
    fMomentum.SetPy(mom.Y());
    fMomentum.SetPz(mom.Z());
  }
  TVector3 GetMomentum() { return TVector3(fMomentum.Px(), fMomentum.Py(), fMomentum.Pz()); }

  void SetEnergy(Double_t ene) { fMomentum.SetE(ene); }
  Double_t GetEnergy() { return fMomentum.E(); }

  Double_t GetRapidity() { return fMomentum.Rapidity(); }
  Double_t GetPt() { return fMomentum.Pt(); }
  Double_t GetPz() { return fMomentum.Pz(); }

  Bool_t IsTypeI() { return fType == kTypeI; }
  Bool_t IsTypeII() { return IsTypeII_ST() || IsTypeII_LT(); }
  Bool_t IsTypeII_ST() { return fType == kTypeII_ST; }
  Bool_t IsTypeII_LT() { return fType == kTypeII_LT; }

  void SetTrue(Bool_t istrue = true) { fIsTrue = istrue; }
  Bool_t IsTrue() { return fIsTrue; }

  void Clear();

 private:
  PionType fType;
  // Double_t fMass;
  ROOT::Math::PxPyPzEVector fMomentum;
  Bool_t fIsTrue;

 public:
  /* for check */
  UInt_t fFlag;
  Int_t fPhotonTower[2];
  Double_t fPhotonEnergy[2];
  Double_t fPhotonPos[2][2];
  Double_t fPhotonR;
  /* true MH */
  Bool_t fTrueMH;  // TypeI: 2 hits in tower, TypeII: 3 hits in tower

 public:
  ClassDef(PionData, 5);
};

}  // namespace nLHCf
#endif
