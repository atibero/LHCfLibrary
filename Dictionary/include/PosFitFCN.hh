#ifndef PosFitFCN_HH
#define PosFitFCN_HH

#include <Rtypes.h>
#include <TF1.h>
#include <TMath.h>

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

namespace nLHCf {

enum class FuncType {
  kOrgMultiHitFCN = 0,     // original
  kAreaMultiHitFCN = 1,    // p1, p3, p5 are area of peaks (/pi)
  kHeightMultiHitFCN = 2,  // p1, p3, p5 are height of peaks
  kHeightMultiHitDL_FCN = 3,
  kNoneHitFCN = -1
};

class PosFitBaseFCN {
 public:
  PosFitBaseFCN() : fNpoint(0) {}
  virtual ~PosFitBaseFCN() {}

  template <typename armrec>
  static PosFitBaseFCN *CreateInstance(FuncType type);

  Double_t operator()(const Double_t *par) { return DoEval(par); }

  /* to be implemented by derived classes */
  virtual Double_t FitFunc(const Double_t *x, const Double_t *par) { return 0.; }

  /* Compute chi2 */
  Double_t DoEval(const Double_t *par) {
    Double_t chi2 = 0.;

    for (UInt_t ip = 0; ip < fY.size(); ++ip) {
      Double_t meas = fY[ip];
      Double_t err = fE[ip];
      Double_t pos = fX[ip];
      Double_t fit = FitFunc(&pos, par);

      if (err > 0.) chi2 += TMath::Power((meas - fit) / err, 2.);
    }

    return chi2;
  }

  void SetDataVectors(const Int_t np, const Double_t *xp, const Double_t *yp, const Double_t *ey) {
    ClearDataVectors();

    Int_t npoint = 0;
    for (Int_t ip = 0; ip < np; ++ip) {
      fX.push_back(xp[ip]);
      fY.push_back(yp[ip]);
      fE.push_back(ey[ip]);
      if (ey[ip] > 0.) ++npoint;
    }
    fNpoint = npoint;
  }

  void SetDataVectors(vector<Double_t> &xp, vector<Double_t> &yp, vector<Double_t> &ey) {
    if (fX.size() != fY.size() || fE.size() != fY.size()) {
      cerr << "PosFitBaseFCN::SetDataVectors: size mismath in input vectors!" << endl;
      exit(EXIT_FAILURE);
    }
    ClearDataVectors();
    fX = xp;
    fY = yp;
    fE = ey;

    Int_t npoint = 0;
    for (UInt_t ip = 0; ip < fE.size(); ++ip)
      if (fE[ip] > 0.) ++npoint;
    fNpoint = npoint;
  }

  void ClearDataVectors() {
    fX.clear();
    fY.clear();
    fE.clear();
  }

  virtual Int_t GetNpoint() const { return fNpoint; }
  virtual Int_t GetNparam() const { return fNparam; }
  virtual FuncType GetFtype() const { return fFtype; }

  virtual void SetNhit(Int_t nhit) = 0;
  virtual Int_t GetNhit() const = 0;
  virtual Int_t GetNparamSingle() const = 0;
  virtual Int_t CheckNhit(const vector<Double_t> &par) const = 0;

  virtual void InitializePars(TF1 *func, vector<Double_t> xpeak, vector<Double_t> ypeak) = 0;

  virtual Double_t GetArea(const vector<Double_t> &par, Int_t ipart = 0) = 0;
  virtual Double_t GetPos(const vector<Double_t> &par, Int_t ipart = 0) = 0;
  virtual Double_t GetTotalHeight(const vector<Double_t> &par, Double_t pos) = 0;
  virtual Double_t GetHeight(const vector<Double_t> &par, Int_t ipart, Double_t pos = -1.) = 0;

  template <typename armrec>
  static Double_t GetArea(const vector<Double_t> &par, Int_t ipart, FuncType type);
  template <typename armrec>
  static Double_t GetPos(const vector<Double_t> &par, Int_t ipart, FuncType type);
  template <typename armrec>
  static Double_t GetTotalHeight(const vector<Double_t> &par, Double_t pos, FuncType type);
  template <typename armrec>
  static Double_t GetHeight(const vector<Double_t> &par, Int_t ipart, Double_t pos, FuncType type);

 protected:
  vector<Double_t> fX;
  vector<Double_t> fY;
  vector<Double_t> fE;

  Int_t fNpoint;  // number of points used in the fit (i.e., with error > 0)
  Int_t fNparam;  // number of parameters
  FuncType fFtype;

 public:
  ClassDef(PosFitBaseFCN, 3);
};

/*---------------------------------------------------------------------------------------------*/

template <typename armrec>
class PosFitMultiHitFCN_Height : public PosFitBaseFCN, public armrec {
 public:
  PosFitMultiHitFCN_Height(Int_t nhit = 2) : PosFitBaseFCN(), fNparamSingle(armrec::kPosNpars) {
    SetNhit(nhit);
    fFtype = FuncType::kHeightMultiHitFCN;
  }

  virtual ~PosFitMultiHitFCN_Height() {}

  void SetNhit(Int_t nhit) {
    fNhit = nhit;
    fNparam = fNparamSingle * fNhit;
  }

  /* 3-component Lorentzian for transverse position fit (multi-hit) */
  virtual Double_t FitFunc(const Double_t *x, const Double_t *par) {
    Double_t fval = 0.;
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Int_t ipar = ih * fNparamSingle;
      const Double_t dx = x[0] - par[0 + ipar];
      const Double_t dx1 = dx / (par[2 + ipar] * par[2 + ipar] + 0.000001);
      const Double_t dx2 = dx / (par[4 + ipar] * par[4 + ipar] + 0.000001);
      const Double_t dx3 = dx / (par[6 + ipar] * par[6 + ipar] + 0.000001);
      fval += par[1 + ipar] * par[1 + ipar] / (1. + dx1 * dx1) + par[3 + ipar] * par[3 + ipar] / (1. + dx2 * dx2) +
              par[5 + ipar] * par[5 + ipar] / (1. + dx3 * dx3);
    }
    return fval;
  }

  virtual void InitializePars(TF1 *func, vector<Double_t> xpeak, vector<Double_t> ypeak) {
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Int_t ipar = ih * fNparamSingle;
      Double_t a1 = 0.7 * ypeak[ih];
      Double_t a2 = 0.2 * ypeak[ih];
      Double_t a3 = 0.1 * ypeak[ih];
      Double_t w1 = 2.0 * this->kPeakSigmaPhoton;
      Double_t w2 = 1.0 * this->kPeakSigmaPhoton;
      Double_t w3 = 10. * this->kPeakSigmaPhoton;
      func->SetParameter(0 + ipar, xpeak[ih]);        // Position
      func->SetParameter(1 + ipar, TMath::Sqrt(a1));  // 1st area
      func->SetParameter(2 + ipar, TMath::Sqrt(w1));  // 1st width
      func->SetParameter(3 + ipar, TMath::Sqrt(a2));  // 2nd area
      func->SetParameter(4 + ipar, TMath::Sqrt(w2));  // 2nd width
      func->SetParameter(5 + ipar, TMath::Sqrt(a3));  // 3rd area
      func->SetParameter(6 + ipar, TMath::Sqrt(w3));  // 3rd width
    }
  }

  virtual Double_t GetArea(const vector<Double_t> &par, Int_t ipart = 0) {
    Double_t a1 = TMath::Power(par[1 + ipart * fNparamSingle] * par[2 + ipart * fNparamSingle], 2.);
    Double_t a2 = TMath::Power(par[3 + ipart * fNparamSingle] * par[4 + ipart * fNparamSingle], 2.);
    Double_t a3 = TMath::Power(par[5 + ipart * fNparamSingle] * par[6 + ipart * fNparamSingle], 2.);
    return TMath::Pi() * (a1 + a2 + a3);
  }

  virtual Double_t GetPos(const vector<Double_t> &par, Int_t ipart = 0) { return par[0 + ipart * armrec::kPosNpars]; }

  virtual Double_t GetTotalHeight(const vector<Double_t> &par, Double_t pos) {
    SetNhit(CheckNhit(par));
    return FitFunc(&pos, &(par[0]));
  }

  virtual Double_t GetHeight(const vector<Double_t> &par, Int_t ipart, Double_t pos = -1.) {
    SetNhit(CheckNhit(par));
    Double_t tmp_pos = (pos < 0. ? par[0 + ipart * armrec::kPosNpars] : pos);
    vector<Double_t> tmp_par = par;
    for (Int_t i = 0; i < fNhit; ++i) {
      if (ipart != i) {
        tmp_par[i * fNparamSingle + 1] = 0.;
        tmp_par[i * fNparamSingle + 3] = 0.;
        tmp_par[i * fNparamSingle + 5] = 0.;
      }
    }
    Double_t height = FitFunc(&tmp_pos, &tmp_par[0]);
    return height;
  }

  virtual Int_t GetNhit() const { return fNhit; }
  virtual Int_t GetNparamSingle() const { return fNparamSingle; }
  virtual Int_t CheckNhit(const vector<Double_t> &par) const {
    Int_t nhit = 0;
    Int_t nmax = par.size() / fNparamSingle;
    for (int i = 0; i < nmax; ++i) {
      if (par[i * fNparamSingle + 1] > 0 || par[i * fNparamSingle + 3] > 0 || par[i * fNparamSingle + 5] > 0) {
        nhit++;
      }
    }
    return nhit;
  }

 protected:
  Int_t fNhit;
  Int_t fNparamSingle;

 public:
  ClassDef(PosFitMultiHitFCN_Height, 3);
};

/*---------------------------------------------------------------------------------------------*/

template <typename armrec>
class PosFitMultiHitFCN_Area : public PosFitBaseFCN, public armrec {
 public:
  PosFitMultiHitFCN_Area(Int_t nhit = 2) : PosFitBaseFCN(), fNparamSingle(armrec::kPosNpars) {
    SetNhit(nhit);
    fFtype = FuncType::kAreaMultiHitFCN;
  }

  virtual ~PosFitMultiHitFCN_Area() {}

  void SetNhit(Int_t nhit) {
    fNhit = nhit;
    fNparam = fNparamSingle * fNhit;
  }

  /* 3-component Lorentzian for transverse position fit (multi-hit) */
  virtual Double_t FitFunc(const Double_t *x, const Double_t *par) {
    Double_t fval = 0.;
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Int_t ipar = ih * fNparamSingle;
      const Double_t dx = x[0] - par[0 + ipar];
      const Double_t dx1 = dx / (par[2 + ipar] * par[2 + ipar]);
      const Double_t dx2 = dx / (par[4 + ipar] * par[4 + ipar]);
      const Double_t dx3 = dx / (par[6 + ipar] * par[6 + ipar]);
      fval += 1. / TMath::Pi() *
              (par[1 + ipar] * par[1 + ipar] / (par[2 + ipar] * par[2 + ipar]) / (1. + dx1 * dx1) +
               par[3 + ipar] * par[3 + ipar] / (par[4 + ipar] * par[4 + ipar]) / (1. + dx2 * dx2) +
               par[5 + ipar] * par[5 + ipar] / (par[6 + ipar] * par[6 + ipar]) / (1. + dx3 * dx3));
    }
    return fval;
  }

  virtual void InitializePars(TF1 *func, vector<Double_t> xpeak, vector<Double_t> ypeak) {
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Int_t ipar = ih * fNparamSingle;
      Double_t w1 = 2.0 * this->kPeakSigmaPhoton;
      Double_t w2 = 1.0 * this->kPeakSigmaPhoton;
      Double_t w3 = 10. * this->kPeakSigmaPhoton;
      Double_t a1 = 0.7 * TMath::Pi() * ypeak[ih] * w1;
      Double_t a2 = 0.2 * TMath::Pi() * ypeak[ih] * w2;
      Double_t a3 = 0.1 * TMath::Pi() * ypeak[ih] * w3;
      func->SetParameter(0 + ipar, xpeak[ih]);        // Position
      func->SetParameter(1 + ipar, TMath::Sqrt(a1));  // 1st area
      func->SetParameter(2 + ipar, TMath::Sqrt(w1));  // 1st width
      func->SetParameter(3 + ipar, TMath::Sqrt(a2));  // 2nd area
      func->SetParameter(4 + ipar, TMath::Sqrt(w2));  // 2nd width
      func->SetParameter(5 + ipar, TMath::Sqrt(a3));  // 3rd area
      func->SetParameter(6 + ipar, TMath::Sqrt(w3));  // 3rd width
    }
  }

  virtual Double_t GetArea(const vector<Double_t> &par, Int_t ipart = 0) {
    return TMath::Power(par[1 + ipart * fNparamSingle], 2.) + TMath::Power(par[3 + ipart * fNparamSingle], 2.) +
           TMath::Power(par[5 + ipart * fNparamSingle], 2.);
  }

  virtual Double_t GetPos(const vector<Double_t> &par, Int_t ipart = 0) { return par[0 + ipart * fNparamSingle]; }

  virtual Double_t GetTotalHeight(const vector<Double_t> &par, Double_t pos) {
    SetNhit(CheckNhit(par));
    return FitFunc(&pos, &par[0]);
  }

  virtual Double_t GetHeight(const vector<Double_t> &par, Int_t ipart = 0, Double_t pos = -1.) {
    SetNhit(CheckNhit(par));
    Double_t tmp_pos = (pos < 0. ? par[0 + ipart * fNparamSingle] : pos);
    vector<Double_t> tmp_par = par;
    for (Int_t i = 0; i < fNhit; ++i) {
      if (ipart != i) {
        tmp_par[i * fNparamSingle + 1] = 0.;
        tmp_par[i * fNparamSingle + 3] = 0.;
        tmp_par[i * fNparamSingle + 5] = 0.;
      }
    }
    Double_t height = FitFunc(&tmp_pos, &tmp_par[0]);
    return height;
  }

  virtual Int_t GetNhit() const { return fNhit; }
  virtual Int_t GetNparamSingle() const { return fNparamSingle; }
  virtual Int_t CheckNhit(const vector<Double_t> &par) const {
    Int_t nhit = 0;
    Int_t nmax = par.size() / fNparamSingle;
    for (int i = 0; i < nmax; ++i) {
      if (par[i * fNparamSingle + 1] > 0 || par[i * fNparamSingle + 3] > 0 || par[i * fNparamSingle + 5] > 0) {
        nhit++;
      }
    }
    return nhit;
  }

 protected:
  Int_t fNhit;
  Int_t fNparamSingle;

 public:
  ClassDef(PosFitMultiHitFCN_Area, 3);
};

/*---------------------------------------------------------------------------------------------*/

template <typename armrec>
class PosFitMultiHitFCN : public PosFitBaseFCN, public armrec {
 public:
  PosFitMultiHitFCN(Int_t nhit = 2) : PosFitBaseFCN(), fNparamSingle(armrec::kPosNpars) {
    SetNhit(nhit);
    fFtype = FuncType::kOrgMultiHitFCN;
  }

  virtual ~PosFitMultiHitFCN() {}

  void SetNhit(Int_t nhit) {
    fNhit = nhit;
    fNparam = fNparamSingle * fNhit;
  }

  /* 3-component Lorentzian for transverse position fit (multi-hit) */
  virtual Double_t FitFunc(const Double_t *x, const Double_t *par) {
    Double_t fval = 0.;
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Int_t ipar = ih * fNparamSingle;
      const Double_t dx = TMath::Power(x[0] - par[1 + ipar], 2.);
      // fval += par[ipar] * (par[2 + ipar] / (dx / par[3 + ipar] + par[3 + ipar]) +
      //                      par[4 + ipar] / (dx / par[5 + ipar] + par[5 + ipar]) +
      //                      (1. - par[2 + ipar] - par[4 + ipar]) / (dx / par[6 + ipar] + par[6 + ipar]));
      fval += par[ipar] * (par[2 + ipar] / (dx / (par[3 + ipar] + 0.0000001) + par[3 + ipar]) +
                           par[4 + ipar] / (dx / par[5 + ipar] + par[5 + ipar]) +
                           (1. - par[2 + ipar] - par[4 + ipar]) / (dx / par[6 + ipar] + par[6 + ipar]));
    }
    return fval;
  }

  virtual void InitializePars(TF1 *func, vector<Double_t> xpeak, vector<Double_t> ypeak) {
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Int_t ipar = ih * fNparamSingle;
      func->SetParameter(0 + ipar, ypeak[ih] * 5.);  // Area
      func->SetParameter(1 + ipar, xpeak[ih]);       // Position
      func->SetParameter(2 + ipar, 0.8);             // 1st fraction
      func->SetParLimits(2 + ipar, 0.5, 1.0);
      func->SetParameter(3 + ipar, 2. * this->kPeakSigmaPhoton);  // 1st width
      func->SetParameter(4 + ipar, 0.3);                          // 2nd fraction
      func->SetParLimits(4 + ipar, 0.0, 0.5);
      func->SetParameter(5 + ipar, this->kPeakSigmaPhoton);        // 2nd width
      func->SetParameter(6 + ipar, 10. * this->kPeakSigmaPhoton);  // 3rd width
    }
  }

  virtual Double_t GetArea(const vector<Double_t> &par, Int_t ipart = 0) { return par[0 + ipart * armrec::kPosNpars]; }

  virtual Double_t GetPos(const vector<Double_t> &par, Int_t ipart = 0) { return par[1 + ipart * armrec::kPosNpars]; }

  virtual Double_t GetTotalHeight(const vector<Double_t> &par, Double_t pos) {
    SetNhit(CheckNhit(par));
    return FitFunc(&pos, &par[0]);
  }

  virtual Double_t GetHeight(const vector<Double_t> &par, Int_t ipart = 0, Double_t pos = -1.) {
    SetNhit(CheckNhit(par));
    Double_t tmp_pos = (pos < 0. ? par[1 + ipart * fNparamSingle] : pos);
    vector<Double_t> tmp_par = par;
    for (Int_t i = 0; i < fNhit; ++i) {
      if (ipart != i) {
        tmp_par[i * fNparamSingle + 0] = 0.;
      }
    }
    Double_t height = FitFunc(&tmp_pos, &tmp_par[0]);
    return height;
  }

  virtual Int_t GetNhit() const { return fNhit; }
  virtual Int_t GetNparamSingle() const { return fNparamSingle; }
  virtual Int_t CheckNhit(const vector<Double_t> &par) const {
    Int_t nhit = 0;
    Int_t nmax = par.size() / fNparamSingle;
    for (int i = 0; i < nmax; ++i) {
      if (par[i * fNparamSingle + 0] > 0 || par[i * fNparamSingle + 2] > 0 || par[i * fNparamSingle + 4] > 0) {
        nhit++;
      }
    }
    return nhit;
  }

 protected:
  Int_t fNhit;
  Int_t fNparamSingle;

 public:
  ClassDef(PosFitMultiHitFCN, 3);
};

/*-------------------------------------------------------------------------------------------*/
template <typename armrec>
class PosFitMultiHitFCN_DL_Height : public PosFitMultiHitFCN_Height<armrec> {
 public:
  PosFitMultiHitFCN_DL_Height(Int_t nhit = 2) : PosFitMultiHitFCN_Height<armrec>(nhit) {
    this->fFtype = FuncType::kHeightMultiHitDL_FCN;
  }

  virtual ~PosFitMultiHitFCN_DL_Height() {}

  virtual void InitializePars(TF1 *func, vector<Double_t> xpeak, vector<Double_t> ypeak) {
    for (Int_t ih = 0; ih < this->fNhit; ++ih) {
      const Int_t ipar = ih * this->fNparamSingle;
      Double_t a1 = 0.7 * ypeak[ih];
      Double_t a2 = 0.3 * ypeak[ih];
      Double_t w1 = 2.0 * this->kPeakSigmaPhoton;
      Double_t w2 = 1.0 * this->kPeakSigmaPhoton;
      func->SetParameter(0 + ipar, xpeak[ih]);        // Position
      func->SetParameter(1 + ipar, TMath::Sqrt(a1));  // 1st area
      func->SetParameter(2 + ipar, TMath::Sqrt(w1));  // 1st width
      func->SetParameter(3 + ipar, TMath::Sqrt(a2));  // 2nd area
      func->SetParameter(4 + ipar, TMath::Sqrt(w2));  // 2nd width
      func->FixParameter(5 + ipar, 0.);               // 3rd area
      func->FixParameter(6 + ipar, 0.001);            // 3rd width
      // parameter limit
      func->SetParLimits(1 + ipar, 0., 10000.);
      func->SetParLimits(2 + ipar, 0.2*this->kPeakSigmaPhoton, 10000.);
      func->SetParLimits(3 + ipar, 0., 10000.);
      func->SetParLimits(4 + ipar, 0.3*this->kPeakSigmaPhoton, 10000.);
    }
  }

  virtual Double_t FitFunc(const Double_t *x, const Double_t *par) {
    const Int_t nstep = 20;
    const Double_t dx = 1./nstep;
    Double_t fval = 0.;

    // calculate the average of FitFuncTmp with nstep
    // emulate the integral for the range of [x-0.5, x+0.5]
    for (int i=0;i<nstep;i++) {
      Double_t x_tmp = x[0] - 0.5 + dx*(i+0.5);
      fval += FitFuncTmp(&x_tmp, par);
    }
    //cerr << "FitFunc " << x[0] << " " << fval/nstep << endl;

    return fval/nstep;
  }

  /* 3-component Lorentzian for transverse position fit (multi-hit) */
  virtual Double_t FitFuncTmp(const Double_t *x, const Double_t *par) {
    Double_t fval = 0.;
    for (Int_t ih = 0; ih < this->fNhit; ++ih) {
      const Int_t ipar = ih * this->fNparamSingle;
      const Double_t dx = x[0] - par[0 + ipar];
      const Double_t dx1 = dx / (par[2 + ipar] * par[2 + ipar] + 0.000001);
      const Double_t dx2 = dx / (par[4 + ipar] * par[4 + ipar] + 0.000001);
      const Double_t dx3 = dx / (par[6 + ipar] * par[6 + ipar] + 0.000001);
      fval += par[1 + ipar] * par[1 + ipar] / (1. + dx1 * dx1) + par[3 + ipar] * par[3 + ipar] / (1. + dx2 * dx2) +
              par[5 + ipar] * par[5 + ipar] / (1. + dx3 * dx3);
    }
    return fval;
  }

 public:
  ClassDef(PosFitMultiHitFCN_DL_Height, 3);
};

// template <typename armrec>
// class PosFitMultiHitFCN_DL_Height : public PosFitBaseFCN, public armrec {
//  public:
//   PosFitMultiHitFCN_DL_Height(Int_t nhit = 2) : PosFitBaseFCN(), fNparamSingle(armrec::kPosNpars) {
//     SetNhit(nhit);
//     fFtype = FuncType::kHeightMultiHitDL_FCN;
//   }
//
//   virtual ~PosFitMultiHitFCN_DL_Height() {}
//
//   void SetNhit(Int_t nhit) {
//     fNhit = nhit;
//     fNparam = fNparamSingle * fNhit;
//   }
//
//   /* 3-component Lorentzian for transverse position fit (multi-hit) */
//   Double_t FitFunc(const Double_t *x, const Double_t *par) {
//     Double_t fval = 0.;
//     for (Int_t ih = 0; ih < fNhit; ++ih) {
//       const Int_t ipar = ih * fNparamSingle;
//       const Double_t dx = x[0] - par[0 + ipar];
//       const Double_t dx1 = dx / (par[2 + ipar] * par[2 + ipar] + 0.000001);
//       const Double_t dx2 = dx / (par[4 + ipar] * par[4 + ipar] + 0.000001);
//       const Double_t dx3 = dx / (par[6 + ipar] * par[6 + ipar] + 0.000001);
//       fval += par[1 + ipar] * par[1 + ipar] / (1. + dx1 * dx1) + par[3 + ipar] * par[3 + ipar] / (1. + dx2 * dx2) +
//               par[5 + ipar] * par[5 + ipar] / (1. + dx3 * dx3);
//     }
//     return fval;
//   }
//
//   void InitializePars(TF1 *func, vector<Double_t> xpeak, vector<Double_t> ypeak) {
//     for (Int_t ih = 0; ih < fNhit; ++ih) {
//       const Int_t ipar = ih * fNparamSingle;
//       Double_t a1 = 0.7 * ypeak[ih];
//       Double_t a2 = 0.3 * ypeak[ih];
//       Double_t w1 = 2.0 * this->kPeakSigmaPhoton;
//       Double_t w2 = 1.0 * this->kPeakSigmaPhoton;
//       func->SetParameter(0 + ipar, xpeak[ih]);        // Position
//       func->SetParameter(1 + ipar, TMath::Sqrt(a1));  // 1st area
//       func->SetParameter(2 + ipar, TMath::Sqrt(w1));  // 1st width
//       func->SetParameter(3 + ipar, TMath::Sqrt(a2));  // 2nd area
//       func->SetParameter(4 + ipar, TMath::Sqrt(w2));  // 2nd width
//       func->FixParameter(5 + ipar, 0.);  // 3rd area
//       func->FixParameter(6 + ipar, 0.001);  // 3rd width
//     }
//   }
//
//   Double_t GetArea(const vector<Double_t> &par, Int_t ipart = 0) {
//     Double_t a1 = TMath::Power(par[1 + ipart * fNparamSingle] * par[2 + ipart * fNparamSingle], 2.);
//     Double_t a2 = TMath::Power(par[3 + ipart * fNparamSingle] * par[4 + ipart * fNparamSingle], 2.);
//     Double_t a3 = TMath::Power(par[5 + ipart * fNparamSingle] * par[6 + ipart * fNparamSingle], 2.);
//     return TMath::Pi() * (a1 + a2 + a3);
//   }
//
//   Double_t GetPos(const vector<Double_t> &par, Int_t ipart = 0) { return par[0 + ipart * armrec::kPosNpars]; }
//
//   Double_t GetTotalHeight(const vector<Double_t> &par, Double_t pos) {
//     SetNhit(CheckNhit(par));
//     return FitFunc(&pos, &(par[0]));
//   }
//
//   Double_t GetHeight(const vector<Double_t> &par, Int_t ipart, Double_t pos = -1.) {
//     SetNhit(CheckNhit(par));
//     Double_t tmp_pos = (pos < 0. ? par[0 + ipart * armrec::kPosNpars] : pos);
//     vector<Double_t> tmp_par = par;
//     for (Int_t i = 0; i < fNhit; ++i) {
//       if(ipart != i) {
//         tmp_par[i*fNparamSingle + 1] = 0.;
//         tmp_par[i*fNparamSingle + 3] = 0.;
//         tmp_par[i*fNparamSingle + 5] = 0.;
//       }
//     }
//     Double_t height = FitFunc(&tmp_pos, &tmp_par[0]);
//     return height;
//   }
//
//   Int_t GetNhit() const { return fNhit; }
//   Int_t GetNparamSingle() const { return fNparamSingle; }
//   Int_t CheckNhit(const vector<Double_t> &par) const {
//     Int_t nhit = 0;
//     Int_t nmax = par.size()/fNparamSingle;
//     for(int i = 0; i < nmax; ++i) {
//       if (par[i*fNparamSingle+1] > 0 || par[i*fNparamSingle+3] > 0 || par[i*fNparamSingle+5] > 0) {nhit++;}
//     }
//     return nhit;
//   }
//
//  protected:
//   Int_t fNhit;
//   Int_t fNparamSingle;
//
//  public:
//   ClassDef(PosFitMultiHitFCN_DL_Height, 3);
// };

}  // namespace nLHCf
#endif
