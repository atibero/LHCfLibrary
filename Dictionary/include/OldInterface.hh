#ifndef OldInterface_HH
#define OldInterface_HH

// #include <TFile.h>
#include <TChain.h>

#include "Level2.hh"
#include "Level3.hh"

namespace nLHCf {

template <typename armclass, typename armrec>
class OldInterface : public armrec {
 public:
  OldInterface(TString fname, Bool_t load_lvl2 = false);
  OldInterface(TString fname, Int_t first, Int_t last, Bool_t load_lvl2 = false);
  ~OldInterface();

 private:
  /* Debug: get old tree */
  TString fInName;
  Int_t fFirstRun;
  Int_t fLastRun;
  // TFile            *fFile;

  /* Level 2 */
  Bool_t fLoadLvl2;
  TChain *fForTree;
  TBranch *fCalBr;
  TBranch *fSilBr;
  vector<double> *fCalVec;
  vector<double> *fSilVec;
  Level2<armclass> *fLvl2;

  /* Level 3 */
  TChain *fRecTree;
  TBranch *fCalRecBr;
  TBranch *fEneBr;
  TBranch *fPosBr;
  TBranch *fMaxBr;
  TBranch *fL90Br;
  TBranch *fMulBr;
  vector<double> *fCalRecVec;
  vector<double> *fEneVec;
  vector<double> *fPosVec;
  vector<int> *fMaxVec;
  vector<double> *fL90Vec;
  vector<bool> *fMulVec;
  Level2<armclass> *fRecLvl2;
  Level3<armrec> *fLvl3;

 public:
  void Open();
  void Close();
  Level2<armclass> *GetLevel2() { return fLvl2; }
  Level2<armclass> *GetRecLevel2() { return fRecLvl2; }
  Level3<armrec> *GetLevel3() { return fLvl3; }
  Int_t GetEvent(Int_t ev);
  void Print(Int_t ev = -1);

 private:
  void OpenLevel2();
  void OpenLevel3();
  void CopyOldLevel2(Level2<armclass> *lvl2, vector<double> *cvec, vector<double> *svec);
  void CopyOldLevel3(Level2<armclass> *lvl2, Level3<armrec> *lvl3, vector<double> *cvec, vector<double> *evec,
                     vector<double> *pvec, vector<int> *mvec, vector<double> *lvec, vector<bool> *hvec);
  Double_t L90Boundary(Int_t tower, Double_t energy, int eff_thr = 90);
  void PrintLevel2(Level2<armclass> *lvl2);
  void PrintLevel3(Level2<armclass> *lvl2, Level3<armrec> *lvl3);

 public:
  ClassDef(OldInterface, 1);
};
}  // namespace nLHCf
#endif
