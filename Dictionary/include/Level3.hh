#ifndef Level3_HH
#define Level3_HH

#include "LHCfParams.hh"
#include "LevelBase.hh"
#include "PosFitFCN.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class Level3 : public LHCfParams, public LevelBase<armclass> {
 public:
  Level3();
  Level3(const char *name, const char *title);
  ~Level3();

  /* Reconstructed position */
  vector<vector<Double_t>> fPhotonPosition;
  vector<vector<vector<Double_t>>> fPhotonPositionMH;
  vector<vector<Double_t>> fNeutronPosition;
  /* Position fit parameters */
  vector<vector<vector<vector<Double_t>>>> fPhotonPosFitPar;
  vector<vector<vector<vector<vector<Double_t>>>>> fPhotonPosFitErr;
  vector<vector<vector<Double_t>>> fPhotonPosFitChi2;
  vector<vector<vector<Double_t>>> fPhotonPosFitNDF;
  vector<vector<vector<Double_t>>> fPhotonPosFitEDM;
  FuncType fPhotonFuncType;
  vector<vector<vector<vector<Double_t>>>> fNeutronPosFitPar;
  vector<vector<vector<vector<vector<Double_t>>>>> fNeutronPosFitErr;
  vector<vector<vector<Double_t>>> fNeutronPosFitChi2;
  vector<vector<vector<Double_t>>> fNeutronPosFitNDF;
  vector<vector<vector<Double_t>>> fNeutronPosFitEDM;
  FuncType fNeutronFuncType;
  /* Multi-hit flag */
  vector<Bool_t> fPhotonMultiHit;
  vector<Bool_t> fNeutronMultiHit;
  /* Position detector layer with maximum energy deposit */
  vector<vector<Int_t>> fPosMaxLayer;
  /* Saturation flag (only for silicon) */
  vector<vector<vector<vector<Bool_t>>>> fSilSaturated;
  /* Energy (EM) */
  vector<Double_t> fPhotonEnergy;            ///< Photon Reconstructed Energy [tower]
  vector<Double_t> fPhotonEnergyLin;         ///< Energy with leak-in corr. [tower]
  vector<vector<Double_t>> fPhotonEnergyMH;  ///< Energy in Multi-hit [tower][hit]
  /* Energy (hadronic) */
  vector<Double_t> fNeutronEnergy;  ///< Neutron Reconstructed Energy [tower]

  /* L90% */
  vector<Double_t> fPhotonL90;   ///< L90% parameter in Photon analysis ? [tower]
  vector<Double_t> fNeutronL90;  ///< L90% parameter in Neutron analysis ? [tower]
  /* L20% */
  vector<Double_t> fPhotonL20;
  vector<Double_t> fNeutronL20;
  /* Photon flag */
  vector<Bool_t> fIsPhoton;
  /* Hadron flag */
  vector<Bool_t> fIsNeutron;

  /* Software trigger */
  vector<Bool_t> fSoftwareTrigger;  ///< Result of the software trigger [tower]

  // Eugenio
  /* Hadron multihit algorithm */
  vector<vector<UInt_t>> fMultiParticleSiID;                     // identity in silicon
  vector<vector<vector<vector<Double_t>>>> fMultiParticleSiPos;  // position in silicon
  vector<vector<vector<vector<Double_t>>>> fMultiParticleSiDep;  // edeposit in silicon
  vector<vector<vector<Double_t>>> fMultiParticleCaldE;          // edeposit in scintillator
  /* Silicon shower width */
  vector<vector<vector<Double_t>>> fShowerWidth;  // layer by layer width
  vector<Double_t> fShowerWidthTotal;             // global width
  /* First hadronic interaction */
  vector<Int_t> fFirstInteraction;  // first hadronic interaction point
  /* Silicon charge */
  vector<vector<vector<Double_t>>> fSiliconCharge;

  /* Getters */
  // In the following function, if the type is kNoneHitFCN, the type will be replaced to fPhotonFuncType/
  // fNeutronFuncType
  Int_t GetPhotonNhits(Int_t tower, Int_t layer, Int_t view);
  Int_t GetNeutronNhits(Int_t tower, Int_t layer, Int_t view);
  Double_t PhotonHeight(Int_t tower, Int_t layer, Int_t view, Double_t pos, FuncType type = FuncType::kNoneHitFCN);
  Double_t PhotonPositionMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type = FuncType::kNoneHitFCN);
  Double_t PhotonAreaMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type = FuncType::kNoneHitFCN);
  Double_t PhotonHeightMH(Int_t tower, Int_t layer, Int_t view, Int_t part, Double_t pos,
                          FuncType type = FuncType::kNoneHitFCN);
  Double_t NeutronPositionMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type = FuncType::kNoneHitFCN);
  Double_t NeutronAreaMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type = FuncType::kNoneHitFCN);

  Int_t GetMaxLayerMH(Int_t tower, Int_t view);

  FuncType GetPhotonFuncType() { return fPhotonFuncType; }
  FuncType GetNeutronFuncType() { return fNeutronFuncType; }

 private:
  void AllocateVectors();

 public:
  void Clear(Option_t *option = "");                  // Clear name,title also. No recomendation to use
  void Copy(Level3 *, Option_t *option = "");         // Copy all of valuables in Level2 and TNamed also
  void DataClear(Option_t *option = "all");           // Clear only the data of Level2 (not name, title)
  void DataCopy(Level3 *, Option_t *option = "all");  // Copy only the valuables in Level2.

  void Print(Option_t *option = "photon neutron");

 public:
  ClassDef(Level3, 4);
};

}  // namespace nLHCf
#endif
