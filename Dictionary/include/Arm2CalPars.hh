#ifndef Arm2CalPars_HH
#define Arm2CalPars_HH

#include <TString.h>

#include "Arm2Params.hh"

namespace nLHCf {

class Arm2CalPars : public Arm2Params {
 public:
  static TString fFillNumber;
  static TString fSubfillNumber;

  static Double_t kCalSat;
  static Double_t kCalBaseHV;
  static Double_t kCalAttFactor0;
  static Double_t kCalAttFactor1;
  static Double_t kCalAttFactor2;
  static Double_t kPosBaseHV;
  static Double_t kPosAttFactor;   // dummy

  static Int_t kMaxNrun;
  static Int_t kFirstRun;
  static Int_t kLastRun;

  static TString fMidasTree;
  static TString fMidasMiniTree;
  static TString fPosDetKey;
  static TString fPosDetIdFile;
  static TString fCalPedShiftFile;
  static TString fCalDelShiftFile;
  static TString fCalRangeFactorFile;
  static TString fCalHVFile;
  static TString fCalGainFactorFile;
  static TString fCalConvFactorFile;
  static TString fPosHVFile;
  static TString fPosGainFactorFile;
  static TString fPosConvFactorFile;
  static TString fCalPedMeanFile;
  static TString fCalPedRealFile;
  static TString fPosPedMeanFile;
  static TString fPosPedRealFile;
  static TString fPosCrossTalkFile;
  static TString fPosSilCorrFile;

  static Int_t nPedSubIters;
  static Int_t nPedSubSigma;

#if defined(LHC2025)
  static const Int_t kNtime = 2;
  static const Int_t kNadc0 = 64;
  static const Int_t kNadc1 = 64;
  static const Int_t kNadc2 = 64;
  static const Int_t kNadc3 = 4;
  static const Int_t kNadc4 = 0;
  static const Int_t kNcad0 = 5;
  static const Int_t kNcad1 = 5;
  static const Int_t kNtdc0 = 256;
  static const Int_t kNscl0 = 0;
  static const Int_t kNgpi0 = 24;
  static const Int_t kNgpi1 = 0;
  static const Int_t kNposd = 19008;
  static const Int_t kNfsil = 0;
  static const Int_t kNruni = 3;
#elif defined(LHC2022)
  static const Int_t kNtime = 2;
  static const Int_t kNadc0 = 64;
  static const Int_t kNadc1 = 64;
  static const Int_t kNadc2 = 64;
  static const Int_t kNadc3 = 4;
  static const Int_t kNadc4 = 0;
  static const Int_t kNcad0 = 5;
  static const Int_t kNcad1 = 5;
  static const Int_t kNtdc0 = 256;
  static const Int_t kNscl0 = 16;
  static const Int_t kNgpi0 = 35;
  static const Int_t kNgpi1 = 0;
  static const Int_t kNposd = 19008;
  static const Int_t kNfsil = 0;
  static const Int_t kNruni = 3;
#elif defined(SPS2022)
  static const Int_t kNtime = 2;
  static const Int_t kNadc0 = 64;
  static const Int_t kNadc1 = 64;
  static const Int_t kNadc2 = 64;
  static const Int_t kNadc3 = 4;
  static const Int_t kNadc4 = 0;
  static const Int_t kNcad0 = 5;
  static const Int_t kNcad1 = 5;
  static const Int_t kNtdc0 = 256;
  static const Int_t kNscl0 = 16;
  static const Int_t kNgpi0 = 35;
  static const Int_t kNgpi1 = 0;
  static const Int_t kNposd = 19008;
  static const Int_t kNfsil = 15400;
  static const Int_t kNruni = 0;  // For Menjo-san: should be 3?
#elif defined(SPS2021)
  static const Int_t kNtime = 2;
  static const Int_t kNadc0 = 64;
  static const Int_t kNadc1 = 64;
  static const Int_t kNadc2 = 64;
  static const Int_t kNadc3 = 4;
  static const Int_t kNadc4 = 0;
  static const Int_t kNcad0 = 5;
  static const Int_t kNcad1 = 5;
  static const Int_t kNtdc0 = 256;
  static const Int_t kNscl0 = 16;
  static const Int_t kNgpi0 = 35;
  static const Int_t kNgpi1 = 0;
  static const Int_t kNposd = 19008;
  static const Int_t kNfsil = 15400;
  static const Int_t kNruni = 0;
#elif defined(SPS2015)
  static const Int_t kNtime = 2;
  static const Int_t kNadc0 = 64;
  static const Int_t kNadc1 = 64;
  static const Int_t kNadc2 = 64;
  static const Int_t kNadc3 = 4;
  static const Int_t kNadc4 = 0;
  static const Int_t kNcad0 = 5;
  static const Int_t kNcad1 = 5;
  static const Int_t kNtdc0 = 256;
  static const Int_t kNscl0 = 16;
  static const Int_t kNgpi0 = 35;  // This was 50 in Mitsuka-software!?
  static const Int_t kNgpi1 = 0;
  static const Int_t kNposd = 14280;
  static const Int_t kNfsil = 15400;
  static const Int_t kNruni = 0;
#else  // LHC2015
  static const Int_t kNtime = 2;
  static const Int_t kNadc0 = 64;
  static const Int_t kNadc1 = 64;
  static const Int_t kNadc2 = 64;
  static const Int_t kNadc3 = 4;
  static const Int_t kNadc4 = 0;
  static const Int_t kNcad0 = 5;
  static const Int_t kNcad1 = 5;
  static const Int_t kNtdc0 = 256;
  static const Int_t kNscl0 = 16;
  static const Int_t kNgpi0 = 35;  // This was 50 in Mitsuka-software!?
  static const Int_t kNgpi1 = 0;
  static const Int_t kNposd = 14280;
  static const Int_t kNfsil = 0;
  static const Int_t kNruni = 0;
#endif

  struct {
    Int_t Run;
    Int_t Number;
    UInt_t Time;
  } Number;

  struct {
    Int_t nTIME;
    UInt_t TIME[kNtime];
  } TIME;

  struct {
    Int_t nSCL0;
    UInt_t SCL0[kNscl0];
  } SCL0;

  struct {
    Int_t nADC0;
    UShort_t ADC0[kNadc0];
  } ADC0;

  struct {
    Int_t nADC1;
    UShort_t ADC1[kNadc1];
  } ADC1;

  struct {
    Int_t nADC2;
    UShort_t ADC2[kNadc2];
  } ADC2;

  struct {
    Int_t nADC3;
    UShort_t ADC3[kNadc3];
  } ADC3;

  struct {
    Int_t nADC4;
    UShort_t ADC4[kNadc4];
  } ADC4;

  struct {
    Int_t nCAD0;
    UInt_t CAD0[kNcad0];
  } CAD0;

  struct {
    Int_t nCAD1;
    UInt_t CAD1[kNcad1];
  } CAD1;

  struct {
    Int_t nARM2;
    char ARM2[kNposd];
  } POSDET;

  struct {
    Int_t nTDC0;
    UInt_t TDC0[kNtdc0];
  } TDC0;

  struct {
    Int_t nGPI0;
    UInt_t GPI0[kNgpi0];
  } GPI0;

  struct {
    Int_t nGPI1;
    UInt_t GPI1[kNgpi1];
  } GPI1;

  struct {
    Int_t nFSIL;
    Char_t FSIL[kNfsil];
  } FSIL;  // ADAMO

  struct {
    Int_t nRUNI;
    UInt_t RUNI[kNruni];
  } RUNI;  // New for Op2022

  // Auxiliary structure to check event alignment in 2025
  struct {
    Int_t nMiniGPI0;
    UInt_t MiniGPI0[kNgpi0];
  } MiniGPI0;
  // Auxiliary structure to check event alignment in 2025
  struct {
    Int_t nMiniRUNI;
    UInt_t MiniRUNI[kNruni];
  } MiniRUNI;

  Int_t GetPOSDETEntries() { return POSDET.nARM2; }
  char GetPOSDETElements(const Int_t &ie) { return POSDET.ARM2[ie]; }

  static void SetFill(Int_t fill, Int_t subfill = 0);

  static class ParInit {
   public:
    ParInit();
  } fInitialiser;

 public:
  ClassDef(Arm2CalPars, 1);
};
}  // namespace nLHCf
#endif
