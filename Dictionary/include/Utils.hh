#ifndef Utils_HH
#define Utils_HH

#include <Rtypes.h>
#include <TCollection.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TLatex.h>
#include <TMath.h>
#include <TPad.h>
#include <TString.h>

#include <cstdarg>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

namespace nLHCf {

class Utils {
 public:
  /* Verbose level */
  static Int_t fVerbose;
  static const Int_t kPrintError = 0;
  static const Int_t kPrintInfo = 1;
  static const Int_t kPrintDebug = 2;
  static const Int_t kPrintDebugFull = 3;

 public:
  Utils() {}

  ~Utils() {}

  static void SetVerbose(Int_t verbose) { fVerbose = verbose; }

 public:
  /* Wrapper for printf with verbose settings */
  static void Printf(Int_t verbose, const char *format, ...) {
    if (fVerbose >= verbose) {
      va_list args;
      va_start(args, format);
      vprintf(format, args);
      va_end(args);
      if (verbose == kPrintError) fflush(stdout);
      if (verbose == kPrintDebugFull || verbose == kPrintDebug) fflush(stdout);
    }
  }

  /* Linear interpolation */
  template <typename type>
  static type LinearXinterpolation(type x0, type x1, type y0, type y1, type y) {
    const Double_t m = (y1 - y0) / (x1 - x0);
    return x0 + (y - y0) / m;
  }

  template <typename type>
  static type LinearYinterpolation(type x0, type x1, type y0, type y1, type x) {
    const Double_t m = (y1 - y0) / (x1 - x0);
    return y0 + m * (x - x0);
  }

  static Double_t LinearInterpolation(Int_t nbin, Double_t *xdata, Double_t *ydata, Double_t x) {
    for (Int_t i = 0; i < nbin - 1; ++i) {
      if (x >= xdata[i] && x < xdata[i + 1]) {
        double diffl = abs(x - xdata[i]);
        double diffr = abs(x - xdata[i + 1]);
        double eval = (ydata[i] * diffr + ydata[i + 1] * diffl) / (xdata[i + 1] - xdata[i]);
        return eval;
      }
    }
    return -1.;
  }

  /* Compute Mean and RMS from a vector */
  template <typename type>
  static void GetMeanRMS(vector<type> vec, Double_t &mean, Double_t &rms) {
    Double_t sum = 0.0;
    Double_t sqr = 0.0;

    // calc the sum and square
    Int_t nvector = vec.size();
    for (Int_t iv = 0; iv < nvector; iv++) {
      Double_t element = (Double_t)vec[iv];
      sum += element;
      sqr += element * element;
    }

    mean = sum / (Double_t)nvector;
    rms = sqr - (sum * sum / (Double_t)nvector);
    rms = rms / (Double_t)(nvector - 1);
    rms = TMath::Sqrt(rms);
  }

  /* Allocate multidimensional vector */
  template <typename type>
  static void AllocateVector1D(vector<type> &vec, Int_t dim1) {
    vec.resize(dim1);
  }

  template <typename type>
  static void AllocateVector2D(vector<vector<type> > &vec, Int_t dim1, Int_t dim2) {
    vec.resize(dim1);
    for (Int_t i = 0; i < dim1; ++i) vec[i].resize(dim2);
  }

  template <typename type>
  static void AllocateVector3D(vector<vector<vector<type> > > &vec, Int_t dim1, Int_t dim2, Int_t dim3) {
    vec.resize(dim1);
    for (Int_t i = 0; i < dim1; ++i) {
      vec[i].resize(dim2);
      for (Int_t j = 0; j < dim2; ++j) vec[i][j].resize(dim3);
    }
  }

  template <typename type>
  static void AllocateVector4D(vector<vector<vector<vector<type> > > > &vec, Int_t dim1, Int_t dim2, Int_t dim3,
                               Int_t dim4) {
    vec.resize(dim1);
    for (Int_t i = 0; i < dim1; ++i) {
      vec[i].resize(dim2);
      for (Int_t j = 0; j < dim2; ++j) {
        vec[i][j].resize(dim3);
        for (Int_t k = 0; k < dim3; ++k) vec[i][j][k].resize(dim4);
      }
    }
  }

  template <typename type>
  static void AllocateVector5D(vector<vector<vector<vector<type> > > > &vec, Int_t dim1, Int_t dim2, Int_t dim3,
                               Int_t dim4, Int_t dim5) {
    vec.resize(dim1);
    for (Int_t i = 0; i < dim1; ++i) {
      vec[i].resize(dim2);
      for (Int_t j = 0; j < dim2; ++j) {
        vec[i][j].resize(dim3);
        for (Int_t k = 0; k < dim3; ++k) {
          vec[i][j][k].resize(dim4);
          for (Int_t l = 0; l < dim4; ++l) vec[i][j][k][l].resize(dim5);
        }
      }
    }
  }

  /* Clear multidimensional vector */
  template <typename type>
  static void ClearVector1D(vector<type> &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) vec[i] = val;
  }

  template <typename type>
  static void ClearVector2D(vector<vector<type> > &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) vec[i][j] = val;
    }
  }

  template <typename type>
  static void ClearVector3D(vector<vector<vector<type> > > &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) vec[i][j][k] = val;
      }
    }
  }

  template <typename type>
  static void ClearVector4D(vector<vector<vector<vector<type> > > > &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) vec[i][j][k][l] = val;
        }
      }
    }
  }

  template <typename type>
  static void ClearVector5D(vector<vector<vector<vector<vector<type> > > > > &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) {
            const Int_t dim5 = vec[i][j][k][l].size();
            for (Int_t m = 0; m < dim5; ++m) vec[i][j][k][l][m] = val;
          }
        }
      }
    }
  }

  /* Copy multidimensional vector */

  ///////////////////////////////////////
  /// @brief Copy vec2 to vec1
  /// @param vec1 [in] To
  /// @param vec2 [in] From
  template <typename type>
  static void CopyVector1D(vector<type> &vec1, vector<type> &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) vec1[i] = vec2[i];
  }

  template <typename type>
  static void CopyVector2D(vector<vector<type> > &vec1, vector<vector<type> > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) vec1[i][j] = vec2[i][j];
    }
  }

  template <typename type>
  static void CopyVector3D(vector<vector<vector<type> > > &vec1, vector<vector<vector<type> > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) vec1[i][j][k] = vec2[i][j][k];
      }
    }
  }

  template <typename type>
  static void CopyVector4D(vector<vector<vector<vector<type> > > > &vec1,
                           vector<vector<vector<vector<type> > > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec1[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) vec1[i][j][k][l] = vec2[i][j][k][l];
        }
      }
    }
  }

  template <typename type>
  static void CopyVector5D(vector<vector<vector<vector<vector<type> > > > > &vec1,
                           vector<vector<vector<vector<vector<type> > > > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec1[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) {
            const Int_t dim5 = vec1[i][j][k][l].size();
            for (Int_t m = 0; m < dim5; ++m) vec1[i][j][k][l][m] = vec2[i][j][k][l][m];
          }
        }
      }
    }
  }

  /* Add multidimensional vector */
  ///////////////////////////////////////
  /// @brief Add vec2 to vec1
  /// @param vec1 [in] To
  /// @param vec2 [in] From
  template <typename type>
  static void AddVector1D(vector<type> &vec1, vector<type> &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) vec1[i] += vec2[i];
  }

  template <typename type>
  static void AddVector2D(vector<vector<type> > &vec1, vector<vector<type> > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) vec1[i][j] += vec2[i][j];
    }
  }

  template <typename type>
  static void AddVector3D(vector<vector<vector<type> > > &vec1, vector<vector<vector<type> > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) vec1[i][j][k] += vec2[i][j][k];
      }
    }
  }

  template <typename type>
  static void AddVector4D(vector<vector<vector<vector<type> > > > &vec1,
                          vector<vector<vector<vector<type> > > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec1[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) vec1[i][j][k][l] += vec2[i][j][k][l];
        }
      }
    }
  }

  template <typename type>
  static void AddVector5D(vector<vector<vector<vector<vector<type> > > > > &vec1,
                          vector<vector<vector<vector<vector<type> > > > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec1[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) {
            const Int_t dim5 = vec1[i][j][k][l].size();
            for (Int_t m = 0; m < dim5; ++m) vec1[i][j][k][l][m] += vec2[i][j][k][l][m];
          }
        }
      }
    }
  }

  /* Subtract multidimensional vector */
  ///////////////////////////////////////
  /// @brief Subtract vec2 from vec1
  /// @param vec1 [in] To
  /// @param vec2 [in] From
  template <typename type>
  static void SubVector1D(vector<type> &vec1, vector<type> &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) vec1[i] -= vec2[i];
  }

  template <typename type>
  static void SubVector2D(vector<vector<type> > &vec1, vector<vector<type> > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) vec1[i][j] -= vec2[i][j];
    }
  }

  template <typename type>
  static void SubVector3D(vector<vector<vector<type> > > &vec1, vector<vector<vector<type> > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) vec1[i][j][k] -= vec2[i][j][k];
      }
    }
  }

  template <typename type>
  static void SubVector4D(vector<vector<vector<vector<type> > > > &vec1,
                          vector<vector<vector<vector<type> > > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec1[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) vec1[i][j][k][l] -= vec2[i][j][k][l];
        }
      }
    }
  }

  template <typename type>
  static void SubVector5D(vector<vector<vector<vector<vector<type> > > > > &vec1,
                          vector<vector<vector<vector<vector<type> > > > > &vec2) {
    const Int_t dim1 = vec1.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec1[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec1[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec1[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) {
            const Int_t dim5 = vec1[i][j][k][l].size();
            for (Int_t m = 0; m < dim5; ++m) vec1[i][j][k][l][m] -= vec2[i][j][k][l][m];
          }
        }
      }
    }
  }

  /* Multiply a factor to multidimensional vector */
  ///////////////////////////////////////
  /// @brief Copy vec2 to vec1
  /// @param vec1 [in] To
  /// @param val  [in] Factor
  template <typename type>
  static void MultiplyVector1D(vector<type> &vec, type val) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) vec[i] *= val;
  }

  template <typename type>
  static void MultiplyVector2D(vector<vector<type> > &vec, type val) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) vec[i][j] *= val;
    }
  }

  template <typename type>
  static void MultiplyVector3D(vector<vector<vector<type> > > &vec, type val) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) vec[i][j][k] *= val;
      }
    }
  }

  template <typename type>
  static void MultiplyVector4D(vector<vector<vector<vector<type> > > > &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) vec[i][j][k][l] *= val;
        }
      }
    }
  }

  template <typename type>
  static void MultiplyVector5D(vector<vector<vector<vector<vector<type> > > > > &vec, type val = 0) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) {
            const Int_t dim5 = vec[i][j][k][l].size();
            for (Int_t m = 0; m < dim5; ++m) vec[i][j][k][l][m] *= val;
          }
        }
      }
    }
  }

  /* Build histograms */
  static void MakeTH1D_1D(vector<TH1D *> &vec, const char *name, const char *title, const Int_t xbin,
                          const Double_t xmin, const Double_t xmax) {
    Double_t xvec[xbin + 1];
    const Double_t xstep = (xmax - xmin) / Double_t(xbin);
    for (Int_t ib = 0; ib < xbin + 1; ++ib) xvec[ib] = xmin + Double_t(ib) * xstep;
    MakeTH1D_1D(vec, name, title, xbin, xvec);
  }

  static void MakeTH1D_1D(vector<TH1D *> &vec, const char *name, const char *title, const Int_t xbin,
                          const Double_t *xvec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i)
      vec[i] = new TH1D(TString::Format("%s_%d", name, i), TString::Format("%s (%d)", title, i), xbin, xvec);
  }

  static void MakeTH1D_2D(vector<vector<TH1D *> > &vec, const char *name, const char *title, const Int_t xbin,
                          const Double_t xmin, const Double_t xmax) {
    Double_t xvec[xbin + 1];
    const Double_t xstep = (xmax - xmin) / Double_t(xbin);
    for (Int_t ib = 0; ib < xbin + 1; ++ib) xvec[ib] = xmin + Double_t(ib) * xstep;
    MakeTH1D_2D(vec, name, title, xbin, xvec);
  }

  static void MakeTH1D_2D(vector<vector<TH1D *> > &vec, const char *name, const char *title, const Int_t xbin,
                          const Double_t *xvec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j)
        vec[i][j] =
            new TH1D(TString::Format("%s_%d_%d", name, i, j), TString::Format("%s (%d, %d)", title, i, j), xbin, xvec);
    }
  }

  static void MakeTH1D_3D(vector<vector<vector<TH1D *> > > &vec, const char *name, const char *title, const Int_t xbin,
                          const Double_t xmin, const Double_t xmax) {
    Double_t xvec[xbin + 1];
    const Double_t xstep = (xmax - xmin) / Double_t(xbin);
    for (Int_t ib = 0; ib < xbin + 1; ++ib) xvec[ib] = xmin + Double_t(ib) * xstep;
    MakeTH1D_3D(vec, name, title, xbin, xvec);
  }

  static void MakeTH1D_3D(vector<vector<vector<TH1D *> > > &vec, const char *name, const char *title, const Int_t xbin,
                          const Double_t *xvec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k)
          vec[i][j][k] = new TH1D(TString::Format("%s_%d_%d_%d", name, i, j, k),
                                  TString::Format("%s (%d, %d, %d)", title, i, j, k), xbin, xvec);
      }
    }
  }

  static void MakeTH1D_4D(vector<vector<vector<vector<TH1D *> > > > &vec, const char *name, const char *title,
                          const Int_t xbin, const Double_t xmin, const Double_t xmax) {
    Double_t xvec[xbin + 1];
    const Double_t xstep = (xmax - xmin) / Double_t(xbin);
    for (Int_t ib = 0; ib < xbin + 1; ++ib) xvec[ib] = xmin + Double_t(ib) * xstep;
    MakeTH1D_4D(vec, name, title, xbin, xvec);
  }

  static void MakeTH1D_4D(vector<vector<vector<vector<TH1D *> > > > &vec, const char *name, const char *title,
                          const Int_t xbin, const Double_t *xvec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l)
            vec[i][j][k][l] = new TH1D(TString::Format("%s_%d_%d_%d_%d", name, i, j, k, l),
                                       TString::Format("%s (%d, %d, %d, %d)", title, i, j, k, l), xbin, xvec);
        }
      }
    }
  }

  static void MakeTH2F_1D(vector<TH2F *> &vec, const char *name, const char *title, const Int_t xbin,
                          const Float_t xmin, const Float_t xmax, const Int_t ybin, const Float_t ymin,
                          const Float_t ymax) {
    Float_t xvec[xbin + 1];
    const Double_t xstep = (xmax - xmin) / Double_t(xbin);
    for (Int_t ib = 0; ib < xbin + 1; ++ib) xvec[ib] = xmin + Double_t(ib) * xstep;
    Float_t yvec[ybin + 1];
    const Double_t ystep = (ymax - ymin) / Double_t(ybin);
    for (Int_t ib = 0; ib < ybin + 1; ++ib) yvec[ib] = ymin + Double_t(ib) * ystep;
    MakeTH2F_1D(vec, name, title, xbin, xvec, ybin, yvec);
  }

  static void MakeTH2F_1D(vector<TH2F *> &vec, const char *name, const char *title, const Int_t xbin,
                          const Float_t *xvec, const Int_t ybin, const Float_t *yvec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i)
      vec[i] =
          new TH2F(TString::Format("%s_%d", name, i), TString::Format("%s (%d)", title, i), xbin, xvec, ybin, yvec);
  }

  static void DeleteTH1D_1D(vector<TH1D *> &vec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      if (vec[i] != NULL) {
        delete vec[i];
        vec[i] = NULL;
      }
    }
    return;
  }

  static void DeleteTH1D_2D(vector<vector<TH1D *> > &vec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        if (vec[i][j] != NULL) {
          delete vec[i][j];
          vec[i][j] = NULL;
        }
      }
    }
    return;
  }

  static void DeleteTH1D_3D(vector<vector<vector<TH1D *> > > &vec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          if (vec[i][j][k] != NULL) {
            delete vec[i][j][k];
            vec[i][j][k] = NULL;
          }
        }
      }
    }
    return;
  }

  static void DeleteTH1D_4D(vector<vector<vector<vector<TH1D *> > > > &vec) {
    const Int_t dim1 = vec.size();
    for (Int_t i = 0; i < dim1; ++i) {
      const Int_t dim2 = vec[i].size();
      for (Int_t j = 0; j < dim2; ++j) {
        const Int_t dim3 = vec[i][j].size();
        for (Int_t k = 0; k < dim3; ++k) {
          const Int_t dim4 = vec[i][j][k].size();
          for (Int_t l = 0; l < dim4; ++l) {
            if (vec[i][j][k][l] != NULL) {
              delete vec[i][j][k][l];
              vec[i][j][k][l] = NULL;
            }
          }
        }
      }
    }
    return;
  }

  /* Utilities for Art works */
  // It might be better to move them to another class or file
  static void SetPadMargin(TPad *pad, Double_t top, Double_t bottom, Double_t right, Double_t left) {
    pad->SetTopMargin(top);
    pad->SetBottomMargin(bottom);
    pad->SetRightMargin(right);
    pad->SetLeftMargin(left);
  }

  static void SetHistTitles(TH1 *h, const char *title, const char *xtitle = "", const char *ytitle = "") {
    h->SetTitle(title);
    h->SetXTitle(xtitle);
    h->SetYTitle(ytitle);
  }

  static void SetHistAxisTexts(TH1 *h, const char *sel = "x", const char *title = "", Double_t title_size = 0.05,
                               Double_t title_offset = 1.0, Double_t label_size = 0.05, Double_t label_offset = 0.005,
                               Int_t ndivisions = 510) {
    TString opt = sel;
    opt.ToLower();

    TAxis *axis;
    if (opt == "x")
      axis = h->GetXaxis();
    else if (opt == "y")
      axis = h->GetYaxis();
    else if (opt == "z")
      axis = h->GetZaxis();
    else
      return;

    axis->SetTitle(title);
    axis->SetTitleSize(title_size);
    axis->SetTitleOffset(title_offset);
    axis->SetLabelSize(label_size);
    axis->SetLabelOffset(label_offset);
    axis->SetNdivisions(ndivisions);
  }

  static void OptimizeHistYRange(TPad *pad, Double_t offset_min = 0.03, Double_t offset_max = 0.05) {
    TObject *obj;
    TH1 *hbase = NULL;
    double min = 1.E10, max = -1.E10;

    if (pad != NULL) {
      TIter next((TCollection *)pad->GetListOfPrimitives());
      while ((obj = next())) {
        if (strstr(obj->ClassName(), "TH1") != 0) {
          TString option = next.GetOption();
          option.ToLower();
          if (!option.Contains("same")) {
            hbase = (TH1 *)obj;
            if (hbase->TestBit(TH1::kIsZoomed)) break;
            hbase->SetMinimum();
            hbase->SetMaximum();
          }
          if (((TH1 *)obj)->GetMinimum() < min) {
            min = ((TH1 *)obj)->GetMinimum();
          }
          if (((TH1 *)obj)->GetMaximum() > max) {
            max = ((TH1 *)obj)->GetMaximum();
          }
        }
      }
      if (hbase != NULL && !hbase->TestBit(TH1::kIsZoomed)) {
        if (max > min) {
          hbase->SetMinimum(min - (max - min) * offset_min);
          hbase->SetMaximum(max + (max - min) * offset_max);
        }
      }
    }
    return;
  }

  static TH1 *GetTH1OnPad(TPad *pad) {
    TObject *obj;
    TH1 *hbase = NULL;
    if (pad != NULL) {
      TIter next((TCollection *)pad->GetListOfPrimitives());
      while ((obj = next())) {
        if (strstr(obj->ClassName(), "TH1") != 0) {
          if (strstr(next.GetOption(), "same") == NULL) {
            hbase = (TH1 *)obj;
            break;
          }
        }
      }
    }
    return hbase;
  }

  static TLatex *DrawTextNDC(Double_t x, Double_t y, const char *text, Double_t size = 0.08, Font_t font = 62,
                             Color_t color = kBlack) {
    TLatex *latex = new TLatex();
    latex->SetNDC();
    latex->SetTextFont(font);
    latex->SetTextSize(size);
    latex->SetTextColor(color);
    latex->SetX(x);
    latex->SetY(y);
    latex->SetTitle(text);
    latex->Draw();
    return latex;
  }

 public:
  ClassDef(Utils, 1);
};

}  // namespace nLHCf
#endif
