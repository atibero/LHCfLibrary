//
// Created by MENJO Hiroaki on 2018/12/04.
//

#ifndef LHCFSOFTWARE_COORDINATETRANSFORMATION_H
#define LHCFSOFTWARE_COORDINATETRANSFORMATION_H

#include <TLorentzVector.h>
#include <TMath.h>
#include <TRotation.h>
#include <TVector3.h>

namespace nLHCf {

class CoordinateTransformation {
 public:
  // Constant values
  static const Double_t kDetectorZpos;
  static const Double_t kEnd2EndInZpos;
  static const Double_t kLHCftolayer0;

 protected:
  // Old variables
  static TRotation sRotationLhcToDetector[2];
  static TRotation sRotationDetectorToCollision[2];
  // New variables
  static Double_t fCrossingAngle[2];                 ///< vertical beam crossing angle
  static Double_t fTanCrossAngle[2];                 ///< tangent of beam crossing angle
  static Double_t fDetectorPositionY[2];             ///< the detector position in the LHC coordinate.
  static Double_t fBeamCentrePosition[2][2];         ///< beam center from "Detector" to "Beam" r.s.
  static Double_t fBeamCentreAverageLayer[2][2];     ///< average layer where beam center was estimated
  static Double_t fArmTowerGap[2][2];  			     ///< gap size between towers [mm] (default: Arm1:(0, 5.0,) Arm2:(1.8,1.8)))
  static Double_t fAlignmentCorrection[2][2][4][2];  ///< relative alignment correction factors in channel [strip]
  // Global vectors
  static TVector3 vBeamCentrePosition[2];
  static TVector3 vDetectorPosition[2];
  // Boolean variables
  static Bool_t fUseRotation;
  static Bool_t fRotatedArm;

 public:
  CoordinateTransformation();
  ~CoordinateTransformation();

  // Set the parameters
  static void SetBeamCrossingAngle(Int_t armindex, Double_t);
  static void SetDetectorPosition(Int_t armindex, Double_t);
  static void SetMeasuredBeamCentre(Int_t armindex,  //
                                    Double_t xpos, Double_t ypos, Double_t xlay, Double_t ylay);
  static void SetArmTowerGap(Int_t armindex, Double_t, Double_t);
  static void SetDetectorPositionVector(Int_t armindex);
  static void SetBeamCentreVector(Int_t armindex);
  static void SetAlignmentValues(Int_t armindex);
  static void SetUseRotation(Bool_t rotation);
  static void SetRotatedArm(Bool_t armated);
  static void WriteParameters(Int_t armindex, Option_t *option = "");
  static void PrintParameters(Int_t armindex, Option_t *option = "");
  static Double_t ComputeBeamCentreZ(Int_t armindex, Int_t viewindex);
  static Double_t ComputeBeamCentreXY(Int_t armindex, Int_t viewindex, Double_t realz);

  // Old function
  static void SetBeamCentrePosition(Int_t armindex, Double_t x, Double_t y, Double_t z);

  // Extrapolate to a given Z coordinate
  static TVector3 ExtrapolateToZ(TVector3, Double_t);
  // Transform from (to) channel to( from) calorimeter
  static TVector3 ChannelToCalorimeter(Int_t armindex, Int_t towerindex, Int_t lx, Int_t ly, Double_t px, Double_t py);
  static TVector3 CalorimeterToChannel(Int_t armindex, Int_t towerindex, Int_t lx, Int_t ly, Double_t px, Double_t py);
  static Double_t GetCrossingAngle(Int_t armindex);
  // Return the crossing angle y offset at a given z
  static Double_t GetCrossingAngleOffset(Int_t armindex, Double_t z);

  /***********************************************************
   ********************** NEW FUNCTIONS **********************
   ***********************************************************/

 private:
  // Auxiliary method for properly add/subtract beam center position and detector offset
  static TVector3 SubBeamCentrePosition(Int_t armindex, TVector3);
  static TVector3 AddBeamCentrePosition(Int_t armindex, TVector3);
  static TVector3 SubCrossAngleOffset(Int_t armindex, TVector3);
  static TVector3 AddCrossAngleOffset(Int_t armindex, TVector3);
  static TVector3 SubDetectorYOffset(Int_t armindex, TVector3);
  static TVector3 AddDetectorYOffset(Int_t armindex, TVector3);
  // Double coordinates
  static TVector3 FromCalorimeterToDetector(Int_t armindex, Int_t towerindex, Double_t x, Double_t y, Double_t z);
  static TVector3 FromDetectorToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y, Double_t z);
  // TVector coordinates
  static TVector3 FromCalorimeterToDetector(Int_t armindex, Int_t towerindex, TVector3);
  static TVector3 FromDetectorToCalorimeter(Int_t armindex, Int_t towerindex, TVector3);
  static TVector3 FromDetectorToCollision(Int_t armindex, TVector3);
  static TVector3 FromCollisionToDetector(Int_t armindex, TVector3);
  static TVector3 FromLHCToCollision(Int_t armindex, TVector3);
  static TVector3 FromCollisionToLHC(Int_t armindex, TVector3);
  // SPS coordinates
  static TVector3 FromSPSToDetector(Int_t armindex, Double_t x, Double_t y, Double_t z);
  static TVector3 FromDetectorToSPS(Int_t armindex, Double_t x, Double_t y, Double_t z);
  static TVector3 FromSPSToDetector(Int_t armindex, TVector3);
  static TVector3 FromDetectorToSPS(Int_t armindex, TVector3);

  // In SPS simulations, we have SPS r.s. instead of LHC r.s. with a different origin
  // For Arm2 simulations the [x, y] offset is:
  // [50., 80.] for standard geometry
  // [48., 80.] for rotated geometry
  // SPS simulation: Get RECO SPS r.s. coordinates starting from Detector r.s. coordinates
  // Global input is Detector coordinates
  static TVector3 GetSPSRecoSPSCoordinates(Int_t armindex, Double_t x, Double_t y);
  static TVector3 GetSPSRecoSPSCoordinates(Int_t armindex, TVector3);
  // SPS simulation: Get TRUE Detector r.s. coordinates starting from SPS r.s. coordinates
  // Global input is SPS coordinates
  static TVector3 GetSPSTrueDetectorCoordinates(Int_t armindex, Double_t x, Double_t y);
  static TVector3 GetSPSTrueDetectorCoordinates(Int_t armindex, TVector3);

  // LHC Flat simulation: Get TRUE Detector r.s. coordinates starting from LHC r.s. coordinates
  // In Flat simulations, the particle are all parallel and orthogonal to the detector
  // Therefore there is no crossing angle offset and average measured beam center is subtracted
  // The LHC r.s. is the same, with a -20mm shift of detector on y axis
  // FLAT simulation:
  // This conversion is embedded in the private methods

 public:
  // Get RECONSTRUCTED detector, collision, LHC r.s. coordinates starting from calorimeter r.s. coordinates
  // Global input is calorimeter coordinates
  static TVector3 GetRecoDetectorCoordinates(Int_t armindex, Int_t towerindex, Double_t xpos, Double_t ypos, Int_t xlay,
                                             Int_t ylay);
  static TVector3 GetRecoDetectorCoordinates(Int_t armindex, Int_t towerindex, TVector3, TVector3);
  static TVector3 GetRecoCollisionCoordinates(Int_t armindex, Int_t towerindex, Double_t xpos, Double_t ypos,
                                              Int_t xlay, Int_t ylay);
  static TVector3 GetRecoCollisionCoordinates(Int_t armindex, Int_t towerindex, TVector3, TVector3);
  static TVector3 GetRecoLHCCoordinates(Int_t armindex, Int_t towerindex, Double_t xpos, Double_t ypos, Int_t xlay,
                                        Int_t ylay);
  static TVector3 GetRecoLHCCoordinates(Int_t armindex, Int_t towerindex, TVector3, TVector3);

  // LHC: Get TRUE collision, detector, calorimeter r.s. coordinates starting from LHC r.s. coordinates
  // Global input is LHC coordinates
  static TVector3 GetTrueDetectorCoordinates(Int_t armindex, Double_t x, Double_t y);
  static TVector3 GetTrueDetectorCoordinates(Int_t armindex, TVector3);
  static TVector3 GetTrueCollisionCoordinates(Int_t armindex, Double_t x, Double_t y);
  static TVector3 GetTrueCollisionCoordinates(Int_t armindex, TVector3);
  static TVector3 GetTrueCalorimeterCoordinates(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);
  static TVector3 GetTrueCalorimeterCoordinates(Int_t armindex, Int_t towerindex, TVector3);

  /***********************************************************
   ********************** OLD FUNCTIONS **********************
   ***********************************************************/
 private:
  // Transform from global to global
  static TVector3 LhcToDetector(Int_t armindex, TVector3);
  static TVector3 DetectorToCollision(Int_t armindex, TVector3);
  static TVector3 LhcToCollision(Int_t armindex, TVector3);
  static TVector3 DetectorToLhc(Int_t armindex, TVector3);
  static TVector3 CollisionToDetector(Int_t armindex, TVector3);
  static TVector3 CollisionToLhc(Int_t armindex, TVector3);

  // aliases to (x,y)
  static TVector3 LhcToDetector(Int_t armindex, Double_t, Double_t);
  static TVector3 DetectorToCollision(Int_t armindex, Double_t, Double_t);
  static TVector3 LhcToCollision(Int_t armindex, Double_t, Double_t);
  static TVector3 DetectorToLhc(Int_t armindex, Double_t, Double_t);
  static TVector3 CollisionToDetector(Int_t armindex, Double_t, Double_t);
  static TVector3 CollisionToLhc(Int_t armindex, Double_t, Double_t);

  // aliases for TLorentzVector (Transformation of 4D-momentum vector)
  static TLorentzVector LhcToDetector(Int_t armindex, TLorentzVector);
  static TLorentzVector DetectorToCollision(Int_t armindex, TLorentzVector);
  static TLorentzVector LhcToCollision(Int_t armindex, TLorentzVector);
  static TLorentzVector DetectorToLhc(Int_t armindex, TLorentzVector);
  static TLorentzVector CollisionToDetector(Int_t armindex, TLorentzVector);
  static TLorentzVector CollisionToLhc(Int_t armindex, TLorentzVector);

  // Transform from(to) calorimeter to(from) global
  static TVector3 CalorimeterToDetector(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);
  static TVector3 CalorimeterToLhc(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);
  static TVector3 CalorimeterToCollision(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);

  static TVector3 CalorimeterToDetector(Int_t armindex, Int_t towerindex, TVector3);
  static TVector3 CalorimeterToLhc(Int_t armindex, Int_t towerindex, TVector3);
  static TVector3 CalorimeterToCollision(Int_t armindex, Int_t towerindex, TVector3);

  static TVector3 DetectorToCalorimeter(Int_t armindex, Int_t towerindex, TVector3);
  static TVector3 LhcToCalorimeter(Int_t armindex, Int_t towerindex, TVector3);
  static TVector3 CollisionToCalorimeter(Int_t armindex, Int_t towerindex, TVector3);

  // aliases to (Int_t armindex, Int_t towerindex, x,y)
  static TVector3 DetectorToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);
  static TVector3 LhcToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);
  static TVector3 CollisionToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y);

 public:
  ClassDef(CoordinateTransformation, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_COORDINATETRANSFORMATION_H
