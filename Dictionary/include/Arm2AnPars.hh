#ifndef Arm2AnPars_HH
#define Arm2AnPars_HH

#include <Rtypes.h>

#include "Arm2Params.hh"

namespace nLHCf {

class Arm2AnPars : public Arm2Params {
 public:
  /* Analysis constants */
  static Double_t fFiducial;   // fiducial border [mm]
  static Double_t kEnergyThr;  // energy threshold [GeV]

  static Double_t kBeamCentre[];              // measured beam centre [mm]
  static Double_t kBeamCentreAverageLayer[];  // average layer where beam centre was measured
  static Double_t kDetectorOffset[];          // detector offset in true mc coordinates due to beam crossing angle
  static Double_t kCrossingAngle;             // vertical crossing angle for LHC operations

  static Bool_t kIsMC;  // is simulation not data
  static Bool_t kIsFlat;  // is flat neutron simulation?

  static Double_t kPosLayerDepth[][2];  // depth of position detector layers

  static Int_t RapToTower(Int_t rap) { return rap <= 2 ? 0 : 1; }  // get tower containing rapidity region

  static Int_t kPhotonNrap;              // number of rapidity regions (photons)
  static Int_t kNeutronNrap;             // number of rapidity regions (neutrons)
  static Int_t kPionNrapTypeI;           // number of rapidity regions (type I pions)
  static Double_t kPionRapLowTypeI;      // lower rapidity value (type I pions)
  static Double_t kPionRapHighTypeI;     // higher rapidity value (type I pions)
  static Int_t kPionNrapTypeII[];        // number of rapidity regions (type II pions)
  static Double_t kPionRapLowTypeII[];   // lower rapidity value (type II pions)
  static Double_t kPionRapHighTypeII[];  // higher rapidity value (type II pions)

  static Double_t kPionMassRange[3][2];    // pi0 mass cut criteria [MeV], ((type), (min,max))
  static Double_t kEnergyRescaleFactor[];  // artificial energy rescale factor (tower)

  static TString fPosAlignmentFile;

  static void SetFiducialBorder(Double_t);
  static void SetFill(Int_t fill, Int_t subfill = 0);

 public:
  ClassDef(Arm2AnPars, 1);
};
}  // namespace nLHCf
#endif
