#ifndef ReadTableRec_HH
#define ReadTableRec_HH

#include <TFile.h>
#include <TH2F.h>
#include <TRandom3.h>
#include <TString.h>
#include <TTree.h>

#include "LHCfParams.hh"

using namespace std;

namespace nLHCf {

template <typename armrec>
class ReadTableRec : public armrec, public LHCfParams {
 public:
  ReadTableRec();
  ~ReadTableRec();

  /*--- Reconstruction data from tables ---*/

  // leakage & efficiency maps
  vector<vector<TH2F *> > fPhotonLeakEffMap;
  vector<vector<vector<vector<Double_t> > > > fPhotonLeakInMap;
  vector<TH2F *> fNeutronLeakMap;
  vector<vector<vector<vector<Double_t> > > > fNeutronLeakInMap;
  vector<vector<TH2F *> > fNeutronEffMap;
  // energy conversion parameters
  vector<vector<Double_t> > fPhotonEconvPars;
  vector<vector<Double_t> > fNeutronEconvPars;
  // dead channels
  vector<vector<vector<vector<vector<Bool_t> > > > > fMaskPosDet;

 public:
  void ReadTables();

 private:
  void AllocateVectors();

  /* Set reconstruction parameters */
  void SetLeakageMap();
  void SetEfficiencyMap();
  void SetEconvFunction();
  void SetDeadChannels();

 public:
  ClassDef(ReadTableRec, 1);
};
}  // namespace nLHCf

#endif
