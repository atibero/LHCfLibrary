//
// Created by MENJO Hiroaki on 2018/12/11.
//

#ifndef LHCFSOFTWARE_MCEVENT_HH
#define LHCFSOFTWARE_MCEVENT_HH

#include <TNamed.h>

#include <McParticle.hh>
#include <vector>

namespace nLHCf {

class McEvent : public TNamed {
 public:
  Int_t fRun;
  Int_t fEvent;

  TObjArray fParticleArray;  ///< Array of McParticle

  std::vector<McParticle*> fReference;  ///<! pointers to the particles selected in CheckHit
  Int_t fRefArm;                        ///<! kArmIndex for which CheckParticleInTower was run
  Int_t fRefTower;                      ///<! tower for which CheckParticleInTower was run

 public:
  McEvent();
  McEvent(const char*, const char*);
  ~McEvent();

  Int_t GetN() { return fParticleArray.GetEntries(); }
  McParticle* Get(Int_t i) { return (McParticle*)fParticleArray.At(i); }
  McParticle* GetRef(Int_t i) { return fReference[i]; }

  void SortByEnergy(std::vector<McParticle*>& v);
  int CheckParticleInTower(Int_t armindex, Int_t towerindex, Double_t energythreshold = 0., Double_t edgecut = 2.0);
  void Add(McEvent mc);

  void Print(Option_t* option = "header,lhc");
  void DataClear();

 public:
  ClassDef(McEvent, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_MCEVENT_H
