#ifndef LinkDefDict_H
#define LinkDefDict_H

#ifdef __CINT__
//#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ namespace nLHCf;
#pragma link C++ class nLHCf::Utils + ;
#pragma link C++ class nLHCf::LHCfParams + ;
#pragma link C++ class nLHCf::Arm1Params + ;
#pragma link C++ class nLHCf::Arm2Params + ;
#pragma link C++ class nLHCf::Arm1CalPars + ;
#pragma link C++ class nLHCf::Arm2CalPars + ;
#pragma link C++ class nLHCf::Arm1RecPars + ;
#pragma link C++ class nLHCf::Arm2RecPars + ;
#pragma link C++ class nLHCf::Arm1AnPars + ;
#pragma link C++ class nLHCf::Arm2AnPars + ;
#pragma link C++ class nLHCf::ReadTableCal < nLHCf::Arm1CalPars> + ;
#pragma link C++ class nLHCf::ReadTableCal < nLHCf::Arm2CalPars> + ;
#pragma link C++ class nLHCf::ReadTableRec < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::ReadTableRec < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::LevelBase < nLHCf::Arm1Params> + ;
#pragma link C++ class nLHCf::LevelBase < nLHCf::Arm2Params> + ;
#pragma link C++ class nLHCf::LevelBase < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::LevelBase < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::Level0 < nLHCf::Arm1Params> + ;
#pragma link C++ class nLHCf::Level0 < nLHCf::Arm2Params> + ;
#pragma link C++ class nLHCf::Level1 < nLHCf::Arm1Params> + ;
#pragma link C++ class nLHCf::Level1 < nLHCf::Arm2Params> + ;
#pragma link C++ class nLHCf::Level2 < nLHCf::Arm1Params> + ;
#pragma link C++ class nLHCf::Level2 < nLHCf::Arm2Params> + ;
#pragma link C++ class nLHCf::Level3 < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::Level3 < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::PosFitBaseFCN + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN_Height < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN_Height < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN_Area < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN_Area < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN_DL_Height < nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::PosFitMultiHitFCN_DL_Height < nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::PionData + ;
#pragma link C++ class nLHCf::LHCfEvent + ;
#pragma link C++ class nLHCf::EventHistograms < nLHCf::Arm1Params> + ;
#pragma link C++ class nLHCf::EventHistograms < nLHCf::Arm2Params> + ;
#pragma link C++ class nLHCf::EventDisplay < nLHCf::Arm1Params, nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::EventDisplay < nLHCf::Arm2Params, nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::CoordinateTransformation + ;
#pragma link C++ class nLHCf::McParticle + ;
#pragma link C++ class nLHCf::McEvent + ;
#pragma link C++ class nLHCf::UtilsMc + ;
#pragma link C++ class nLHCf::OldInterface < nLHCf::Arm1Params, nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::OldInterface < nLHCf::Arm2Params, nLHCf::Arm2RecPars> + ;

#pragma link C++ class nLHCf::SPSAdamo + ;
#pragma link C++ class nLHCf::SPSAdamoHist + ;

#endif

#endif
