#include "Level3.hh"
#include "McEvent.hh"

namespace nLHCf {
class CutFunctions {
 public:
  template <typename armrec, typename arman>
  static Bool_t BPTXCut(Level3<armrec> *lvl3);
  template <typename armrec, typename arman>
  static Bool_t L2TShowerCut(Level3<armrec> *lvl3);

  template <typename armrec, typename arman>
  static Bool_t PhotonDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t PhotonPIDCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t PhotonEnergyLinCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower, Int_t part);
  template <typename armrec, typename arman>
  static Bool_t PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower, Int_t part);
  template <typename armrec, typename arman>
  static Bool_t PhotonRapidityCut(Level3<armrec> *lvl3, Int_t rap);
  template <typename armrec, typename arman>
  static TVector3 PhotonGlobalPosition(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t PhotonMultiHitCut(Level3<armrec> *lvl3, Int_t rap);

  template <typename armrec, typename arman>
  static Bool_t NeutronDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronPIDCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronEnergyCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronPositionCut(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronRapidityCut(Level3<armrec> *lvl3, Int_t rap);
  template <typename armrec, typename arman>
  static TVector3 NeutronGlobalPosition(Level3<armrec> *lvl3, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronMultiHitCut(Level3<armrec> *lvl3, Int_t rap);

  template <typename armrec, typename arman>
  static Bool_t PhotonTruePIDCut(McEvent *mcev, Int_t tower, Int_t part = 0);
  template <typename armrec, typename arman>
  static Bool_t PhotonTrueEnergyCut(McEvent *mcev, Int_t tower, Int_t part = 0);
  template <typename armrec, typename arman>
  static Bool_t PhotonTruePositionCut(McEvent *mcev, Int_t tower, Int_t part = 0);
  template <typename armrec, typename arman>
  static Bool_t PhotonTrueRapidityCut(McEvent *mcev, Int_t rap, Int_t part = 0);
  template <typename armrec, typename arman>
  static Bool_t PhotonTrueMultiHitCut(McEvent *mcev, Int_t tower);

  template <typename armrec, typename arman>
  static Bool_t NeutronTruePIDCut(McEvent *mcev, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronTrueEnergyCut(McEvent *mcev, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronTruePositionCut(McEvent *mcev, Int_t tower);
  template <typename armrec, typename arman>
  static Bool_t NeutronTrueRapidityCut(McEvent *mcev, Int_t rap);
  template <typename armrec, typename arman>
  static Bool_t NeutronTrueMultiHitCut(McEvent *mcev, Int_t tower);

  template <typename armrec, typename arman>
  static Bool_t PionPIDCut(Level3<armrec> *lvl3, Int_t tower, Int_t type, Int_t eff = 90);
  template <typename armrec, typename arman>
  static Double_t PionL90Boundary(Int_t tower, Double_t energy, Int_t type, Int_t eff = 90);
};
}  // namespace nLHCf
