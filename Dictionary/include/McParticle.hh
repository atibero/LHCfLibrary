//
// Created by MENJO Hiroaki on 2018/12/04.
//

#ifndef LHCFSOFTWARE_MCPARTICLE_HH
#define LHCFSOFTWARE_MCPARTICLE_HH

#include <TLorentzVector.h>
#include <TNamed.h>
#include <TVector3.h>

#include <Arm1AnPars.hh>
#include <Arm2AnPars.hh>
#include <UtilsMc.hh>

namespace nLHCf {

class McParticle : public TNamed {
 public:
  Int_t fIndex;              ///< Index number
  Int_t fPdgCode;            ///< Pdg code
  TVector3 fPosition;        ///< Position (Lhc coordinate)
  TLorentzVector fMomentum;  ///< 4-momemtum vector (Lhc coordinate)
  Int_t fUserCode;           ///< User code in Epics output
  Int_t fStatusCode;         ///< Status code (Not implemented yet)
  Int_t fParent[2];          ///< Parent Particle

 public:
  McParticle();
  McParticle(const char *, const char *);
  McParticle(McParticle* );
  ~McParticle();

  virtual void Clear(Option_t *option = "");
  void Copy(McParticle *);
  void DataClear(Option_t *option = "");
  void DataCopy(McParticle *);
  void SetFlag();

  bool isflat[2];

  Int_t PdgCode() { return fPdgCode; }              /// return Pdg code
  TVector3 Position() { return fPosition; }         /// return Positon vector (!!! converted to mm !!!)
  TLorentzVector Momemtum() { return fMomentum; };  /// return Momentum vector
  Double_t X() { return Position().X(); }           /// original value
  Double_t Y() { return Position().Y(); }           /// original value
  Double_t Z() { return Position().Z(); }           /// original value
  Double_t MomentumX() { return fMomentum.X(); }    /// original value
  Double_t MomentumY() { return fMomentum.Y(); }    /// original value
  Double_t MomentumZ() { return fMomentum.Z(); }    /// original value
  Double_t Energy() { return fMomentum.Energy(); }

  const char *Name() { return UtilsMc::GetName(fPdgCode); }  /// get the paritlce name

  Double_t Mass() { return UtilsMc::Mass(fPdgCode) * 1000.; }  /// return the paritlce mass [MeV]

  Double_t Charge() { return UtilsMc::Charge(fPdgCode); }  /// return the particle charge


  TVector3 Pos_Collision(Int_t armindex);
  Double_t X_Col(Int_t armindex) { return Pos_Collision(armindex).X(); }
  Double_t Y_Col(Int_t armindex) { return Pos_Collision(armindex).Y(); }
  TVector3 Pos_Detector(Int_t armindex);
  Double_t X_Det(Int_t armindex) { return Pos_Detector(armindex).X(); }
  Double_t Y_Det(Int_t armindex) { return Pos_Detector(armindex).Y(); }
  TVector3 Pos_Calorimeter(Int_t armindex, Int_t towerindex);
  Double_t X_Cal(Int_t armindex, Int_t towerindex) { return Pos_Calorimeter(armindex, towerindex).X(); }
  Double_t Y_Cal(Int_t armindex, Int_t towerindex) { return Pos_Calorimeter(armindex, towerindex).Y(); }

  Int_t    GetEventID(){return fUserCode/10000;}
  Int_t    GetParentID(){return (fUserCode/100)%100;}
  Int_t    GetChildrenID(){return fUserCode%100;}

  Bool_t CheckHit(Int_t armindex, Int_t towerindex, Double_t energythreshold = 0., Double_t edgecut = 2.0);
  void Print(Option_t *option = "header,lhc");

  //		Double_t Rapidity();
  //		Double_t y();

 public:
  ClassDef(McParticle, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_MCPARTICLE_HH
