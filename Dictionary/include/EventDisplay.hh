#ifndef LHCFSOFTWARE_EVENTDISPLAY_H
#define LHCFSOFTWARE_EVENTDISPLAY_H

#include <TCanvas.h>
#include <TLatex.h>
#include <TLine.h>
#include <TPad.h>

#include <EventHistograms.hh>
#include <Level3.hh>

namespace nLHCf {

template <typename armclass, typename armrec>
class EventDisplay : public EventHistograms<armclass> {
 protected:
  // Parameters
  static const Int_t kNpad;
  static const Int_t kNpadPos;
  static const Int_t kNtext;
  Int_t fCanvasSize[2];
  Color_t fCalLineColor[2];
  Color_t fPosLineColor[4];
  Color_t fFcLineColor[2];

  // For Calorimter informations
  TCanvas *fCanvas1;
  vector<TPad *> fPad;              ///<
  vector<vector<TPad *> > fPadPos;  ///< [view][kNpadPos]
  vector<TLatex *> fText;           ///<
  vector<TLine *> fLine;            ///< for PACE edges

  // For Flags, TDC, Counters
  static const Int_t kNpad2;

  TCanvas *fCanvas2;     // for Flag,Counter,TDC
  vector<TPad *> fPad2;  ///<
  vector<vector<TLatex *> > fFlagBit;
  vector<TLatex *> fCounters;
  vector<vector<TLatex *> > fFifoCounters;
  vector<vector<TLatex *> > fTdc;

  // For Level3 infreoomation
  Bool_t fLvl2flag;        ///<
  Bool_t fLvl3flag;        ///<
  Level3<armrec> fLvl3;  ///<

 public:
  EventDisplay();
  EventDisplay(const char *name, const char *title);
  ~EventDisplay();

  void Initialize();
  void Delete();
  void Fill(Level0<armclass> *);
  void Fill(Level1<armclass> *);
  void Fill(Level2<armclass> *);
  void Fill(Level3<armrec> *);  // Additional information
  void Draw(Option_t *option = "canvas1, canvas2");
  void DrawCanvas1(Option_t *option = "canvas1");
  void DrawCanvas2(Option_t *option = "canvas2");
  void DrawLvl3onCanvas1();
  void Update(Option_t* option="");
  void Print(const char *filename = "");
  void Print(const char *filename, Option_t *option);

  TCanvas *GetCanvas1() { return fCanvas1; }
  TCanvas *GetCanvas2() { return fCanvas2; }

  void SetCanvasSize(Int_t x, Int_t y) {
    fCanvasSize[0] = x;
    fCanvasSize[1] = y;
  };

  void SetCalLineColor(Color_t c0, Color_t c1) {
    fCalLineColor[0] = c0;
    fCalLineColor[1] = c1;
  }

  void SetPosLineColor(Color_t c0, Color_t c1, Color_t c2, Color_t c3) {
    fPosLineColor[0] = c0;
    fPosLineColor[1] = c1;
    fPosLineColor[2] = c2;
    fPosLineColor[3] = c3;
  }

 private:
  void DrawText(TLatex *t, Double_t x, Double_t y, Double_t size = 0.05, Font_t font = 62);
  Double_t ConvertDepthToLayer(Double_t val); // conv. of L20, L90

 public:
  ClassDef(EventDisplay, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_EVENTDISPLAY_H
