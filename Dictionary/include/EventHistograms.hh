#ifndef LHCFSOFTWARE_EVENTHISTOGRAMS_HH
#define LHCFSOFTWARE_EVENTHISTOGRAMS_HH

#include <TH1D.h>
#include <TNamed.h>

#include <LHCfParams.hh>
#include <Level0.hh>
#include <Level1.hh>
#include <Level2.hh>

namespace nLHCf {

template <typename armclass>
class EventHistograms : public armclass, public LHCfParams, public TNamed {
 public:
  enum DATATYPE { LEVEL0, LEVEL1, LEVEL2, NONE };

  EventHistograms();
  EventHistograms(const char *name, const char *title);
  ~EventHistograms();

  DATATYPE fDataType;
  vector<vector<TH1D *> > fHistCalorimeter;               ///< [tower][ADC range]
  vector<vector<vector<vector<TH1D *> > > > fHistPosDet;  ///< [tower][layer][xy][sample]
  vector<TH1D *> fHistFrontCounter;                       ///< [range] in 2022
  vector<TH1D *> fHistZdc;                                ///< [ADC range]

 private:
  void Initialize();
  void AllocateHistograms();
  void DeleteHistograms();

 public:
  void Delete();
  void Fill(Level0<armclass> *);
  void Fill(Level1<armclass> *);
  void Fill(Level2<armclass> *);

 public:
  ClassDef(EventHistograms, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_EVENTHISTOGRAMS_HH
