#ifndef Arm2Params_HH
#define Arm2Params_HH

#include <TString.h>

#include "LHCfParams.hh"

namespace nLHCf {

class Arm2Params {
 public:
  static Int_t kArmIndex;

  /* Operation type */
  static OpType fOperation;

  /* Tables directory */
  static TString kTableDirectory;

  /* Calorimeters constants */
  static Int_t kCalNtower;
  static Int_t kCalNlayer;
  static Int_t kCalNrange;

  static Double_t kSampleStep;
  static Int_t kDoubleStepLayer;

  /* Position detectors constants */
  static Int_t kPosNsample;
  static Int_t kPosNtower;
  static Int_t kPosNlayer;
  static Int_t kPosNview;
  static Int_t kPosNchannel[];
  static Double_t kPosPitch;
  static Double_t kTowerSize[];

  /* Front counters constants */
  static Int_t kFcNlayer;
  static Int_t kFcNrange;

  /* ZDC constants */
  static Int_t kZdcNchannel;
  static Int_t kZdcNrange;

  /* Level0 constants */
  static Int_t kNflag;
  static Int_t kNevFlag;
  static Int_t kOpenAdcNch;
  static Int_t kLaserNch;
  static Int_t kTdcNch;
  static Int_t kTdcNhit;
  static Int_t kScalerNch;
  static Int_t kNcounter;
  static Int_t kNfifo1;
  static Int_t kNfifo2;
  static Int_t kTime;

  static Char_t kTdcLabel[][10];
  static Char_t kCounterLabel[][12];
  static Char_t kFlagBitLabel[][12];

 public:
  static void SetTableDirectory(const Char_t *dir) { kTableDirectory = dir; }
  static TString GetTableDirectory() { return kTableDirectory; }

  static class ParInit {
   public:
    ParInit();
  } fInitialiser;

 public:
  ClassDef(Arm2Params, 2);
};
}  // namespace nLHCf
#endif
