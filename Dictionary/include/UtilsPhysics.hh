//
// Created by MENJO Hiroaki on 2018/12/06.
//

#ifndef LHCFSOFTWARE_UTILSPHYSICS_HH
#define LHCFSOFTWARE_UTILSPHYSICS_HH

#include <TLorentzVector.h>
#include <TNamed.h>

namespace nLHCf {

class UtilsPhysics {
 public:
  static TLorentzVector ReconstructTwoParticle(TLorentzVector, TLorentzVector);
  static TLorentzVector ReconstructPi0(TLorentzVector, TLorentzVector);

 public:
  ClassDef(UtilsPhysics, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_UTILSPHYSICS_H
