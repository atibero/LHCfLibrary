#ifndef SILICON_DECODE_H
#define SILICON_DECODE_H

const int NMDAQ = 4;
const int NMDAQ_SEC = 2;
const int NCHXPACE3 = 32;
const int NPACE3XSEC = 12;
const int NSAMPLES = 3;

const int NSTRIPXLAYER = NPACE3XSEC * NCHXPACE3;
const int NMDAQSECTIONS = NMDAQ * NMDAQ_SEC;
const int NSILAYERS = NMDAQSECTIONS;

const int DATA_PACKET_1ST_LEN = 1168;
const int DATA_PACKET_2ND_LEN = 1208;

typedef struct {
  unsigned char ID0;  // = 73
  unsigned char ID1;  // = 21
  unsigned char BLK;  // = 1 o 2 (index of transmitted packet
  unsigned char MSI;  // = 3 bit MDAQ board identifier (0 --> NMDAQ-1)
  unsigned int ELCC;  // Clock (LHC) counter
  unsigned int EVC;   // Event counter
  unsigned short VAL[(int)((DATA_PACKET_1ST_LEN - 16) * 0.5)];
  unsigned char CRC;  // = CRC of previous bytes
  unsigned char BA;   // = BA
  unsigned char CA;   // = CA
  unsigned char DA;   // = DA
} RawPackA;

typedef struct {
  unsigned char ID0;  // = 73
  unsigned char ID1;  // = 21
  unsigned char BLK;  // = 1 o 2 (index of transmitted packet
  unsigned char MSI;  // = 3 bit MDAQ board identifier (0 --> NMDAQ-1)
  unsigned int ELCC;  // Clock (LHC) counter
  unsigned int EVC;   // Event counter
  unsigned short VAL[(int)((DATA_PACKET_1ST_LEN - 16) * 0.5)];
  unsigned char CAD[3][NPACE3XSEC];  //# Column Addresses 3/chip for all chips on 1 section (12) = 3x12 values,
  unsigned char REG[4];
  unsigned char CRC;  // = CRC of previous bytes
  unsigned char BA;   // = BA
  unsigned char CA;   // = CA
  unsigned char DA;   // = DA
} RawPackB;

#endif
