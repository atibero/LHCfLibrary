#ifndef Level2_HH
#define Level2_HH

#include "LevelBase.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class Level2 : public LevelBase<armclass> {
 public:
  Level2();
  Level2(const char *name, const char *title);
  ~Level2();

  vector<Bool_t> fEventFlag;
  vector<vector<Double_t> > fTdc;    ///< TDC values [channel][hit]
  vector<vector<UInt_t> > fTdcFlag;  ///< TDC flag   [channel][hit]
  vector<Int_t> fQualityFlag;        ///< Quality flags of the Analysis [i]
  vector<Double_t> fOpenADC;
  vector<Double_t> fScaler;
  vector<vector<UInt_t> > fFifoCounter;

  vector<vector<Double_t> > fCalorimeter;  ///< dE of layers [tower][layer]
  vector<vector<Bool_t> > fCalSatFlag;     ///< saturation flag [tower][layer]
  vector<vector<vector<vector<vector<Double_t> > > > >
      fPosDet;                     ///< dE of Position detector [tower][layer][xy][channel][sample]
  vector<Double_t> fFrontCounter;  ///< dE of FrontCounter [channel]
  vector<vector<vector<vector<vector<Double_t> > > > >
      fErrPosDet;  ///< Error of fPosDet [tower][layer][xy][channel][sample]

  vector<Double_t> fLaser;
  vector<Double_t> fZdc;  ///< dE of ATLAS ZDC [channel]

 public:
  void AllocateVectors();
  void Clear(Option_t *option = "");           // Clear name,title also. No recomendation to use
  void Copy(Level2 *, Option_t *option = "");  // Copy all of valuables in Level2 and TNamed also
  void DataClear(Option_t *option = "all");
  void DataCopy(Level2 *d, Option_t *option = "all");
  void Add(Level2 *d, Option_t *option = "all");
  void Multiply(Double_t val, Option_t *option = "all");
  void Print();

 public:
  ClassDef(Level2, 3);
};

}  // namespace nLHCf
#endif
