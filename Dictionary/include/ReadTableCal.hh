#ifndef ReadTableCal_HH
#define ReadTableCal_HH

#include <TFile.h>
#include <TH2F.h>
#include <TMatrixD.h>
#include <TRandom3.h>
#include <TString.h>
#include <TTree.h>
#include <TSpline.h>

#include "LHCfParams.hh"

using namespace std;

namespace nLHCf {

template <typename armcal>
class ReadTableCal : public armcal, public LHCfParams {
 public:
  ReadTableCal();
  ~ReadTableCal();

  /* GSO bars ID */
  vector<vector<vector<vector<Int_t> > > > fPosDetID;

  /*--- Calibration data from tables ---*/
  vector<vector<vector<vector<Double_t> > > > fCalPedShift;
  vector<vector<vector<vector<Double_t> > > > fCalDelShift;
  vector<vector<Double_t> > fCalRangeFactor;
  vector<vector<Double_t> > fCalHV;
  vector<vector<Double_t> > fCalGainFactor;
  vector<vector<Double_t> > fCalAttFactor;
  vector<vector<Double_t> > fCalConvFactor;
  vector<vector<vector<Double_t> > > fCalPedMean;
  vector<vector<vector<Double_t> > > fCalPedRms;
  Double_t fPosHV;
  vector<vector<vector<vector<Double_t> > > > fPosGainFactor;
  vector<vector<vector<vector<vector<Double_t> > > > > fPosConvFactor;
  vector<vector<vector<vector<vector<Double_t> > > > > fPosPedMean;
  vector<vector<vector<vector<vector<Double_t> > > > > fPosPedRms;
  vector<Double_t> fFcConvFactor;
  vector<TMatrixD> fPosCrossTalk;
  vector<TMatrixD> fPosCrossTalkInv;
  TSpline3 fPosSilCorr;
  TSpline3 fPosSilCorrInv;

  /*--- Pedestal from ROOT file (for smearing) ---*/
  TRandom3 fRandom;
  TFile *fCalPedFile;
  TTree *fCalPedTree;
  TBranch *fCalPedBranch;
  vector<Double_t> *fCalPedVec;
  Int_t fCalPedEntries;

  TFile *fPosPedFile;
  TTree *fPosPedTree;
  TBranch *fPosPedBranch;
  vector<Double_t> *fPosPedVec;
  Int_t fPosPedEntries;

 public:
  void SetSeed(ULong_t seed) { fRandom.SetSeed(seed); }
  void ReadTables();
  void SetPosDetID();

 private:
  void AllocateVectors();

  /* Set calibration parameters */
  void SetCalPedShift();
  void SetCalDelShift();
  void SetCalRangeFactor();
  void SetCalHV();
  void SetCalGainFactor();
  void SetCalAttFactor();
  void SetCalConvFactor();
  void SetCalPedestal();
  void SetPosHV();
  void SetPosGainFactor();
  void SetPosConvFactor();
  void SetPosPedestal();
  void SetPosCrossTalk();
  void SetPosSilCorr();

 public:
  ClassDef(ReadTableCal, 1);
};
}  // namespace nLHCf

#endif
