#ifndef LHCfEvent_HH
#define LHCfEvent_HH

#include <TNamed.h>
#include <TObjArray.h>

//______________________________________________________________________________
// Hello !!

namespace nLHCf {

class LHCfEvent : public TNamed {
 public:
  Int_t fRun;           //  Run Number
  Int_t fGnumber;       //  Global Event Number
  Int_t fA1number;      //  Arm#1  Event Number
  Int_t fA2number;      //  Arm#2  Event Number
  UInt_t fA1flag[2];    //  Arm#1  Event Flag [0]:GPI0[0] [1]:GPI1[0]
  UInt_t fA2flag[2];    //  Arm#1  Event Flag [0]:GPI0[0] [1]:GPI1[0]
  Int_t fIfile;         //  File Identification Number
  Int_t fTnumber;       //  Event Number in the file
  UInt_t fTag;          //  Tags for the event type
  Int_t fTmp;           //  Temporal Number, each user can use freely
  TObjArray fObjarray;  //  Data Stock in the Event

 public:
  LHCfEvent();
  LHCfEvent(const char *name, const char *title);
  ~LHCfEvent();

  void Initialize();
  void Delete();

  Int_t GetRun() { return fRun; }
  Int_t GetGNumber() { return fGnumber; }
  Int_t GetA1Number() { return fA1number; }
  Int_t GetA2Number() { return fA2number; }
  UInt_t *GetA1Flag() { return fA1flag; }
  UInt_t GetA1Flag(Int_t i) { return fA1flag[i]; }
  UInt_t *GetA2Flag() { return fA2flag; }
  UInt_t GetA2Flag(Int_t i) { return fA2flag[i]; }
  Int_t GetIFile() { return fIfile; }
  Int_t GetTNumber() { return fTnumber; }
  UInt_t GetTag() { return fTag; }
  Int_t GetTmp() { return fTmp; }
  TObjArray *GetObjAarray() { return &fObjarray; }
  void SetTmp(Int_t n) { fTmp = n; }

  void Add(TObject *obj) { fObjarray.Add(obj); }

  TObject *FindObject(const char *name) { return fObjarray.FindObject(name); }
  TObject *GetObject(const char *name) { return FindObject(name); }
  TObject *Get(const char *name) { return FindObject(name); }
  Int_t Check(const char *name) { return FindObject(name) == 0 ? 0 : 1; }

  void Clear() {
    HeaderClear();
    ObjClear();
  }
  void HeaderClear();
  void ObjClear() { fObjarray.Clear(); }    // Remove all objects from the objarray. Does not delete the objects
  void ObjDelete() { fObjarray.Delete(); }  // ObjClear + Delete all objectes

  void HeaderCopy(LHCfEvent *d);  // Copy header informations from d to this.
  Int_t CheckTag(UInt_t mask = 0xFFFFFFFF) { return (fTag & mask) ? 1 : 0; }
  Int_t CheckA1Trg() { return CheckTag(0x000001); }
  Int_t CheckA1Scl() { return CheckTag(0x000010); }
  Int_t CheckA2Trg() { return CheckTag(0x000100); }
  Int_t CheckA2Scl() { return CheckTag(0x001000); }
  Int_t CheckSC() { return CheckTag(0x010000); }
  Int_t CheckA1Sim() { return CheckTag(0x10000000); }
  Int_t CheckA2Sim() { return CheckTag(0x20000000); }

  Int_t CheckA1Flag(Int_t i = 0, UInt_t mask = 0xFFFF) { return (fA1flag[i] & mask) ? 1 : 0; }
  Int_t CheckA2Flag(Int_t i = 0, UInt_t mask = 0xFFFF) { return (fA2flag[i] & mask) ? 1 : 0; }
  Int_t IsA1BeamFlag() { return CheckA1Flag(0, 0x1000); }
  Int_t IsA1PedeFlag() { return CheckA1Flag(0, 0x2000); }
  Int_t IsA1LaserFlag() { return CheckA1Flag(0, 0x4000); }
  Int_t IsA1EnableFlag() { return CheckA1Flag(0, 0x8000); }
  Int_t IsA1BTPX1Flag() { return CheckA1Flag(0, 0x0001); }
  Int_t IsA1BTPX2Flag() { return CheckA2Flag(0, 0x0002); }
  Int_t IsA1L2TAFlag() { return CheckA1Flag(0, 0x0010); }
  Int_t IsA1L2TPFlag() { return CheckA1Flag(0, 0x0020); }
  Int_t IsA1L2TEXTFlag() { return CheckA1Flag(0, 0x0040); }
  Int_t IsA1STRGFlag() { return CheckA1Flag(0, 0x0400); }
  Int_t IsA1LTRGFlag() { return CheckA1Flag(0, 0x0800); }
  Int_t IsA1L2TASPFlag() { return CheckA1Flag(0, 0x0080); }
  Int_t IsA1L2TAMBFlag() { return CheckA1Flag(0, 0x0200); }
  Int_t IsA2BeamFlag() { return CheckA2Flag(0, 0x1000); }
  Int_t IsA2PedeFlag() { return CheckA2Flag(0, 0x2000); }
  Int_t IsA2LaserFlag() { return CheckA2Flag(0, 0x4000); }
  Int_t IsA2EnableFlag() { return CheckA2Flag(0, 0x8000); }
  Int_t IsA2BTPX1Flag() { return CheckA2Flag(0, 0x0001); }
  Int_t IsA2BTPX2Flag() { return CheckA2Flag(0, 0x0002); }
  Int_t IsA2L2TAFlag() { return CheckA2Flag(0, 0x0010); }
  Int_t IsA2L2TPFlag() { return CheckA2Flag(0, 0x0020); }
  Int_t IsA2L2TEXTFlag() { return CheckA2Flag(0, 0x0040); }
  Int_t IsA2STRGFlag() { return CheckA2Flag(0, 0x0400); }
  Int_t IsA2LTRGFlag() { return CheckA2Flag(0, 0x0800); }
  Int_t IsA2L2TASPFlag() { return CheckA2Flag(0, 0x0080); }
  Int_t IsA2L2TAMBFlag() { return CheckA2Flag(0, 0x0200); }

  Int_t EventTime();

  void Show();
  void Print() { Show(); }  // Alias of Show();

 public:
  ClassDef(LHCfEvent, 1)  // Data store for LHCf one event
};

}  // namespace nLHCf
#endif
