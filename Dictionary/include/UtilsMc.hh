//
// Created by MENJO Hiroaki on 2018/12/07.
//

#ifndef LHCFSOFTWARE_UTILSMC_H
#define LHCFSOFTWARE_UTILSMC_H

#include <TDatabasePDG.h>
#include <TString.h>

namespace nLHCf {

class UtilsMc {
 protected:
  static TDatabasePDG *fDatabasePdg;
  static const Int_t kNlistEpics2Pdg = 47;
  static const Int_t kTableEpics2Pdg[kNlistEpics2Pdg][4];

 public:
  UtilsMc();
  ~UtilsMc();

  static TParticlePDG *GetParticle(Int_t pdgcode) { return fDatabasePdg->GetParticle(pdgcode); }

  static TString GetName(Int_t pdgcode) { return GetParticle(pdgcode)->GetName(); }

  static Double_t Mass(Int_t pdgcode) { return GetParticle(pdgcode)->Mass(); }

  static Double_t Charge(Int_t pdgcode) { return GetParticle(pdgcode)->Charge(); }

  // Epics Code <-> PDG
  static Int_t EpicsToPdgCode(Int_t code, Int_t subcode, Int_t charge);
  static void PdgToEpicsCode(Int_t pdg, Int_t &code, Int_t &subcode, Int_t &charge);

 public:
  ClassDef(UtilsMc, 1);
};

}  // namespace nLHCf

#endif  // LHCFSOFTWARE_UTILSMC_H
