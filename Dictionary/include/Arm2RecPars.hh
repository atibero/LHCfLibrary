#ifndef Arm2RecPars_HH
#define Arm2RecPars_HH

#include <TString.h>

#include "Arm2Params.hh"

namespace nLHCf {

class Arm2RecPars : public Arm2Params {
 public:
  /* Calorimeters constants */
  static Int_t kTrgDscNlayer;
  static Double_t kTrgDscThr;
  static Double_t kTrgDscThrByLayer[2][16];

  static Int_t kLeakInNpar;
  static Int_t kLeakBins[];
  static Int_t kEconvNpar;

  static Int_t kPhotonFirstLayer;
  static Int_t kPhotonLastLayer;
  static Int_t kNeutronFirstLayer;
  static Int_t kNeutronLastLayer;

  /* Position detectors constants */
  static Int_t kAvgWindowPhoton;
  static Int_t kDeconIterPhoton;
  static Double_t kPeakSigmaPhoton;
  static Double_t kPeakRatioThrPhoton;
  static Double_t kPeakAbsThrPhoton;
  static Int_t kAvgWindowNeutron;
  static Int_t kDeconIterNeutron;
  static Double_t kPeakSigmaNeutron;
  static Double_t kPeakRatioThrNeutron;
  static Double_t kPeakAbsThrNeutron;

  static Int_t kDefaultPhotonFitType;
  static Int_t kDefaultNeutronFitType;
  static Int_t kDefaultPhotonErecType;
  //static Int_t kDefaultNeutronErecType;

  static Int_t kPosNpars;
  static Int_t kMaxNparticle;
  static Int_t kClusterInformation;
  static Int_t kPosNlayerEM;
  static Double_t kMinDistPhoton;
  static Double_t kMinDistNeutron;

  static Double_t kPosSaturationThr;

  /* Depth in X0 */
  static Double_t kCalLayerX0[2][16];    // depth of scintillator detector layers in terms of radiation length
  static Double_t kPosLayerX0[2][4][2];  // depth of position detector layers in terms of radiation length

  /* Table files */
  static TString fPhotonLeakageFile;
  static TString fPhotonLeakInFile;
  static TString fNeutronLeakageFile;
  static TString fNeutronLeakInFile;
  static TString fNeutronEffFile;
  static TString fPhotonEconvFile;
  static TString fNeutronEconvFile;
  static TString fPosDeadChFile;

  static class ParInit {
   public:
    ParInit();
  } fInitialiser;

 public:
  ClassDef(Arm2RecPars, 1);
};
}  // namespace nLHCf
#endif
