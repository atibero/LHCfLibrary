#ifndef LHCfParams_HH
#define LHCfParams_HH

#include <Rtypes.h>

enum OpType { kSPS2014, kSPS2015, kLHC2015, kLHC2016, kSPS2021, kSPS2022, kLHC2022, kLHC2025 };

namespace nLHCf {

class LHCfParams {
 public:
  /* Global LHCf constants */
  static const Int_t kNxyz;
  static const Int_t kNxy;
  static const Int_t kNarm;

  static Double_t kL20Thr;
  static Double_t kL90Thr;

 public:
  ClassDef(LHCfParams, 1);
};
}  // namespace nLHCf
#endif
