#ifndef LevelBase_HH
#define LevelBase_HH

#include <Rtypes.h>
#include <TNamed.h>

using namespace std;

namespace nLHCf {

template <typename armclass>
class LevelBase : public armclass, public TNamed {
 public:
  LevelBase();
  LevelBase(const char *name, const char *title);
  ~LevelBase();

  /* Header */
  Int_t fRun;               ///< Run number
  Int_t fEvent;             ///< Arm event number
  Int_t fGevent;            ///< Global event number
  Double_t fTime[2];        ///< Time [0:sec, 1:usec]
  vector<UInt_t> fFlag;     ///< GPIO flags (GPIO0: 2ch + GPIO: 1ch)
  vector<Long_t> fCounter;  ///< Counter values [counter]

 protected:
  virtual void AllocateVectors();

 public:
  // General
  Int_t Run() { return fRun; }
  Int_t EventNumber() { return fEvent; }
  Int_t GEventNumber() { return fGevent; }
  Double_t Time() { return (double)fTime[0] + 1.E-6 * fTime[1]; }
  UInt_t Flag(int i) { return fFlag[i]; }
  Long_t Counter(int i) { return fCounter[i]; }

  // == DAQ Flags ==
  // Event status (combination of some flags)
  Bool_t IsBeam();
  Bool_t IsPedestal();
  // Trigger Mode
  Bool_t IsShowerTrg();   // Shower Trigger
  Bool_t IsSpecialTrg();  // = Pi0 Trigger
  Bool_t IsPi0Trg();
  Bool_t IsHighEMTrg();
  Bool_t IsHadronTrg();
  Bool_t IsZdcTrg();
  Bool_t IsLaserTrg();
  Bool_t IsFcTrg();
  Bool_t IsL1tTrg();
  Bool_t IsPedestalTrg();
  // Beam Condition and ATLAS Trigger
  Bool_t IsBeam1();     //  Presence of Beam 1
  Bool_t IsBeam2();     //  Presence of Beam 2
  Bool_t IsAtlasL1A();  //  ATLAS L1A signal
  // Status DAQ flag
  Bool_t IsDaqFlagBeam();
  Bool_t IsDaqFlagPedestal();
  Bool_t IsDaqFlagOtherArmL3T();
  Bool_t IsDaqFlagOtherArmL1T();
  Bool_t IsDaqFlagOtherArmEnable();
  Bool_t IsDaqFlagOtherArmShowerTrg();
  //
  Bool_t IsDsc(Int_t tower, Int_t layer);  // Discriminator Flag
  Bool_t IsFcDsc(Int_t channel);           // Discriminator Flag for FC

  // == Counter Values ==
  Int_t BunchID();     // Bunch ID
  Int_t Beam1ID();     // Beam1 ID (Beam number in Beam1 ring)
  Int_t Beam2ID();     // Beam2 ID (Beam number in Beam1 ring)
  UInt_t AtlasEcr();   // ATLAS Level1 ECR
  UInt_t AtlasL1ID();  // ATLAS Level1 ID

  virtual void Clear(Option_t *option = "");                     // Clear name,title also. No recomendation to use
  virtual void Copy(LevelBase *, Option_t *option = "");         // Copy all of valuables in Level2 and TNamed also
  virtual void DataClear(Option_t *option = "all");              // Clear only the data of Level2 (not name, title)
  virtual void DataCopy(LevelBase *, Option_t *option = "all");  // Copy only the valuables in Level2.
  virtual void Print();

 public:
  ClassDef(LevelBase, 1);
};

}  // namespace nLHCf
#endif
