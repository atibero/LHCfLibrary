#ifndef Level0_HH
#define Level0_HH

#include "LevelBase.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class Level0 : public LevelBase<armclass> {
 public:
  Level0();
  Level0(const char *name, const char *title);
  ~Level0();

  vector<Bool_t> fEventFlag;
  vector<vector<Double_t> > fTdc;    ///< TDC values [channel][hit]
  vector<vector<UInt_t> > fTdcFlag;  ///< TDC flag   [channel][hit]
  vector<Int_t> fQualityFlag;        ///< Quality flags of the Analysis [i]
  vector<Double_t> fOpenADC;
  vector<Double_t> fScaler;
  vector<vector<UInt_t> > fFifoCounter;

  vector<vector<vector<Double_t> > > fCalorimeter;  ///< dE of layers [tower][layer][range]
  vector<vector<vector<Bool_t> > > fCalSatFlag;     ///< saturation flag [tower][layer][range]
  vector<vector<vector<vector<vector<Double_t> > > > >
      fPosDet;                              ///< dE of Position detector [tower][layer][xy][channel][sample]
  vector<vector<Double_t> > fFrontCounter;  ///< dE of FrontCounter [channel][range]
  vector<Double_t> fLaser;
  vector<vector<Double_t> > fZdc;  ///< dE of ATLAS ZDC [channel][range]

 public:
  void AllocateVectors();
  void Clear(Option_t *option = "");           // Clear name,title also. No recomendation to use
  void Copy(Level0 *, Option_t *option = "");  // Copy all of valuables in Level0 and TNamed also
  void DataClear(Option_t *option = "all");
  void DataCopy(Level0 *d, Option_t *option = "all");
  void Add(Level0 *d, Option_t *option = "all");
  void Multiply(Double_t val, Option_t *option = "all");
  void Print();

 public:
  ClassDef(Level0, 3);
};

}  // namespace nLHCf
#endif
