#include "Arm1CalPars.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm1CalPars);
#endif

typedef Utils UT;

TString Arm1CalPars::fFillNumber = "";

Double_t Arm1CalPars::kCalSat = 4096;
Double_t Arm1CalPars::kCalBaseHV = 600.;
Double_t Arm1CalPars::kCalAttFactor0 = 0.9016;  // 1
Double_t Arm1CalPars::kCalAttFactor1 = 0.9016;  // 1
Double_t Arm1CalPars::kCalAttFactor2 = 0.9016;  // 1
Double_t Arm1CalPars::kPosBaseHV = 950.;
Double_t Arm1CalPars::kPosAttFactor = 140.;   //  [mm] GSO bar attenuation length obtained from Suzuki-san's GSO bar paper

Int_t Arm1CalPars::kMaxNrun = 1000;
Int_t Arm1CalPars::kFirstRun = -1;
Int_t Arm1CalPars::kLastRun = -1;

TString Arm1CalPars::fMidasTree = "Trigger1";
TString Arm1CalPars::fMidasMiniTree = "Trigger1m";  // Meaningless
TString Arm1CalPars::fPosDetKey = "SCIF";
TString Arm1CalPars::fPosDetIdFile = "a1_pos_table.dat";
TString Arm1CalPars::fCalPedShiftFile = "a1_cal_ped_shift_fill" + fFillNumber + ".dat";
TString Arm1CalPars::fCalDelShiftFile = "a1_cal_del_shift_fill" + fFillNumber + ".dat";
TString Arm1CalPars::fCalRangeFactorFile = "a1_cal_range_factor.dat";
TString Arm1CalPars::fCalHVFile = "a1_cal_hv.dat";
TString Arm1CalPars::fCalGainFactorFile = "a1_cal_gain_factor.dat";
TString Arm1CalPars::fCalConvFactorFile = "a1_cal_conv_factor.dat";
TString Arm1CalPars::fPosHVFile = "a1_pos_hv.dat";
TString Arm1CalPars::fPosGainFactorFile = "a1_pos_gain_factor.dat";
TString Arm1CalPars::fPosConvFactorFile = "a1_pos_conv_factor.dat";
TString Arm1CalPars::fCalPedMeanFile = "a1_cal_ped_mean_fill" + fFillNumber + ".dat";
TString Arm1CalPars::fCalPedRealFile = "a1_cal_ped_fill" + fFillNumber + ".root";
TString Arm1CalPars::fPosPedMeanFile = "a1_pos_ped_mean_fill" + fFillNumber + ".dat";
TString Arm1CalPars::fPosPedRealFile = "a1_pos_ped_fill" + fFillNumber + ".root";
TString Arm1CalPars::fPosCrossTalkFile = "a1_pos_crosstalk.root";
TString Arm1CalPars::fPosSilCorrFile = "";

Int_t Arm1CalPars::nPedSubIters = 3;
Int_t Arm1CalPars::nPedSubSigma = 5;

void Arm1CalPars::SetFill(Int_t fill, Int_t subfill) {
  fill = TMath::Abs(fill);
  subfill = TMath::Abs(subfill);

  UT::Printf(UT::kPrintInfo, "Arm1CalPars: setting parameters for fill %d\n", fill);

  /* This is the most generic case : To be used only for preliminary study */
  if (fill == 0) {
    kFirstRun = -1;
    kLastRun = -1;
    fFillNumber = Form("_dummy");
  }
  /* SPS */
  else if (fOperation == kSPS2022 || fOperation == kSPS2021 || fOperation == kSPS2015) {
    kFirstRun = -1;
    kLastRun = -1;
    fFillNumber = Form("_dummy");
  }
  /* LHC 2025 */
  else if (fOperation == kLHC2025) {  // Meaningless
    kFirstRun = -1;
    kLastRun = -1;
    fFillNumber = Form("_dummy");
  }
  /* LHC 2022 */
  else if (fOperation == kLHC2022) {
    // TODO: Estimate this factors for all fills (and subfills)
    // Fill 8178 - Divided in four subgroups
    if (fill == 8178) {
      if (subfill == 1) {
        kFirstRun = 80262;
        kLastRun = 80346;
      } else if (subfill == 2) {
        kFirstRun = 80351;
        kLastRun = 80372;
      } else if (subfill == 3) {
        kFirstRun = 80377;
        kLastRun = 80431;
      } else if (subfill == 4) {
        kFirstRun = 80434;
        kLastRun = 80520;
      } else if (subfill == 5) {
        kFirstRun = 80523;
        kLastRun = 80626;
      } else {
        UT::Printf(UT::kPrintError, "Arm1CalPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }
      fFillNumber = Form("%d_%d_%d", fill, kFirstRun, kLastRun);
    } else if (fill == 8181) {
      kFirstRun = 80635;
      kLastRun = 80646;
      fFillNumber = Form("%d", fill);
    } else {
      UT::Printf(UT::kPrintError, "Arm1CalPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  }
  /* LHC 2015 */
  else if (fOperation == kLHC2015) {
    /* Fill 3855 (detector in nominal position) */
    if (fill == 3855) {
      kFirstRun = 44299;
      kLastRun = 45106;
      fFillNumber = Form("%d", fill);
    }
    /* Fill 3851 (detector shifted up by 5 mm) */
    else if (fill == 3851) {
      kFirstRun = 43321;
      kLastRun = 43598;
      fFillNumber = Form("%d", fill);
    } else {
      UT::Printf(UT::kPrintError, "Arm1CalPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  } else {
    UT::Printf(UT::kPrintError, "Arm1CalPars::SetFill: unknown fill, exit...\n");
    exit(EXIT_FAILURE);
  }

  fCalPedShiftFile = "a1_cal_ped_shift_fill" + fFillNumber + ".dat";
  fCalDelShiftFile = "a1_cal_del_shift_fill" + fFillNumber + ".dat";
  fCalPedMeanFile = "a1_cal_ped_mean_fill" + fFillNumber + ".dat";
  fCalPedRealFile = "a1_cal_ped_fill" + fFillNumber + ".root";
  fPosPedMeanFile = "a1_pos_ped_mean_fill" + fFillNumber + ".dat";
  fPosPedRealFile = "a1_pos_ped_fill" + fFillNumber + ".root";

  return;
}

Arm1CalPars::ParInit::ParInit() {
  UT::Printf(UT::kPrintInfo, "Arm1CalPars: initialising parameters...");
  fflush(stdout);

  if (fOperation == kLHC2015) {
    UT::Printf(UT::kPrintInfo, " (LHC2015)");
    fflush(stdout);
  } else if (fOperation == kSPS2015) {
    UT::Printf(UT::kPrintInfo, " (SPS2015)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2021) {
    UT::Printf(UT::kPrintInfo, " (SPS2021)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2022) {
    UT::Printf(UT::kPrintInfo, " (SPS2022)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kLHC2022) {
    UT::Printf(UT::kPrintInfo, " (LHC2022)");
    fflush(stdout);
  } else if (fOperation == kLHC2025) {  // Meaningless
    UT::Printf(UT::kPrintInfo, " (LHC2025)");
    fflush(stdout);
  } else {
    UT::Printf(UT::kPrintError, "Arm1CalPars::Initialise: unknown operation\n");
    exit(EXIT_FAILURE);
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

Arm1CalPars::ParInit Arm1CalPars::fInitialiser;
