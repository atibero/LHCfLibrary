#include "Arm1RecPars.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm1RecPars);
#endif

typedef Utils UT;

/* Calorimeters constants */
Int_t Arm1RecPars::kTrgDscNlayer = 3;                    // number of consecutive layers over threshold
Double_t Arm1RecPars::kTrgDscThr = 0.65;                 // trigger threshold [GeV]
Double_t Arm1RecPars::kTrgDscThrByLayer[2][16] = {{0}};  // this value is later assigned

Int_t Arm1RecPars::kLeakInNpar = 3;         // # of paramaters of Leak-in correction
Int_t Arm1RecPars::kLeakBins[] = {40, 80};  // # of Leak-in bins
Int_t Arm1RecPars::kEconvNpar = 3;          // # of paramaters in SumdE -> E function

Int_t Arm1RecPars::kPhotonFirstLayer = 1;   // first layer used for SumdE (photons)
Int_t Arm1RecPars::kPhotonLastLayer = 12;   // last layer used for SumdE (photons)
Int_t Arm1RecPars::kNeutronFirstLayer = 2;  // first layer used for SumdE (neutrons)
Int_t Arm1RecPars::kNeutronLastLayer = 15;  // last layer used for SumdE (neutrons)

/* Position detectors constants */
Int_t Arm1RecPars::kAvgWindowPhoton = 3;            // TSpetrum average window
Int_t Arm1RecPars::kDeconIterPhoton = 1;            // TSpetrum deconvolution iterations
Double_t Arm1RecPars::kPeakSigmaPhoton = 1;         // TSpetrum peak width
Double_t Arm1RecPars::kPeakRatioThrPhoton = 0.05;   // TSpetrum ratio thr.
Double_t Arm1RecPars::kPeakAbsThrPhoton = 0.0015;   // TSpetrum absolute thr. [GeV]
Int_t Arm1RecPars::kAvgWindowNeutron = 3;           // TSpetrum average window
Int_t Arm1RecPars::kDeconIterNeutron = 3;           // TSpetrum deconvolution iterations
Double_t Arm1RecPars::kPeakSigmaNeutron = 7;        // TSpetrum peak width
Double_t Arm1RecPars::kPeakRatioThrNeutron = 0.10;  // TSpetrum ratio thr.
Double_t Arm1RecPars::kPeakAbsThrNeutron = 0.001;   // TSpetrum absolute thr. [GeV]

Int_t Arm1RecPars::kDefaultPhotonFitType = 3;       // Default type of the position fitting function for photons
Int_t Arm1RecPars::kDefaultNeutronFitType = 3;      // Default type of the position fitting function for neutrons
Int_t Arm1RecPars::kDefaultPhotonErecType = 2;      // Default type of the energy reconstruction for MH photons
//Int_t Arm1RecPars::kDefaultNeutronErecType = 2;   // For future implementation

Int_t Arm1RecPars::kPosNpars = 7;            // number of fit function parameters
Int_t Arm1RecPars::kMaxNparticle = 3;        // maximum # of hit fitted
Int_t Arm1RecPars::kClusterInformation = 3;  // maximum # of allowed track cluster
Int_t Arm1RecPars::kPosNlayerEM = 2;         // number of layers for photon analysis
Double_t Arm1RecPars::kMinDistPhoton = 1.;   // min. distance for a multi-hit in photon reconctruction
Double_t Arm1RecPars::kMinDistNeutron =
    2.;  // min. distance for a multi-hit in neutron reconstruction //Eugenio optimize this

Double_t Arm1RecPars::kPosSaturationThr = 1E30;  // pos. detector saturation threshold (!!! TODO !!!)

/* Depth in X0 */

// TODO: This numbers refer to the upgraded detector, need to define an external table for elder data

Double_t Arm1RecPars::kCalLayerX0[2][16] = {
    {2., 4., 6., 8., 10., 12., 14., 16., 18., 20., 22., 26., 30., 34., 38., 42.},  // ST
    {2., 4., 6., 8., 10., 12., 14., 16., 18., 20., 22., 26., 30., 34., 38., 42.}   // LT
};  // depth of scintillator detector layers in terms of radiation length
Double_t Arm1RecPars::kPosLayerX0[2][4][2] = {
    {{6., 6.}, {10., 10.}, {30., 30.}, {42., 42.}},  // ST
    {{6., 6.}, {10., 10.}, {30., 30.}, {42., 42.}}   // LT
};                                                   // depth of position detector layers in terms of radiation length

/* Table files */
TString Arm1RecPars::fPhotonLeakageFile = "a1_photon_leakage-efficiency_20181129.root";
TString Arm1RecPars::fPhotonLeakInFile = "a1_old_photon_leakage-in.dat";
TString Arm1RecPars::fNeutronLeakageFile = "a1_neutron_leakage_20181129.root";
TString Arm1RecPars::fNeutronLeakInFile = "a1_old_neutorn_leakage-in.dat";
// TString Arm1RecPars::fNeutronEffFile = "a1_neutron_efficiency.root";
TString Arm1RecPars::fNeutronEffFile = "a1_neutron_efficiency_20181129.root";
TString Arm1RecPars::fPhotonEconvFile = "a1_photon_energy-conversion.dat";
TString Arm1RecPars::fNeutronEconvFile = "a1_neutron_energy-conversion.dat";
TString Arm1RecPars::fPosDeadChFile = "a1_pos_dead_channels.dat";

Arm1RecPars::ParInit::ParInit() {
  UT::Printf(UT::kPrintInfo, "Arm1RecPars: initialising parameters...");
  fflush(stdout);

  for (Int_t it = 0; it < 2; ++it)
    for (Int_t il = 0; il < 16; ++il) {
      kTrgDscThrByLayer[it][il] = kTrgDscThr;
    }

  if (fOperation == kLHC2015) {
    UT::Printf(UT::kPrintInfo, " (LHC2015)");
    fflush(stdout);
  } else if (fOperation == kSPS2015) {
    UT::Printf(UT::kPrintInfo, " (SPS2015)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2021) {
    UT::Printf(UT::kPrintInfo, " (SPS2021)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2022) {
    UT::Printf(UT::kPrintInfo, " (SPS2022)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kLHC2022) {
    UT::Printf(UT::kPrintInfo, " (LHC2022)");
    fflush(stdout);
  } else if (fOperation == kLHC2025) {  // Meaningless
    UT::Printf(UT::kPrintInfo, " (LHC2025)");
    fflush(stdout);
  } else {
    UT::Printf(UT::kPrintError, "Arm1RecPars::Initialise: unknown operation\n");
    exit(EXIT_FAILURE);
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

Arm1RecPars::ParInit Arm1RecPars::fInitialiser;
