#include "Arm2AnPars.hh"

#include <TMath.h>

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm2AnPars);
#endif

typedef Utils UT;
typedef CoordinateTransformation CT;

/* Analysis constants, default values */
Double_t Arm2AnPars::fFiducial = 2.;     // fiducial border [mm]
Double_t Arm2AnPars::kEnergyThr = 200.;  // energy threshold [GeV]

Double_t Arm2AnPars::kPosLayerDepth[][2] = {{46.66 + 1.99, 46.66 + 0.00},
                                            {46.66 + 37.02, 46.66 + 35.04},
                                            {46.66 + 103.71, 46.66 + 69.33},
                                            {46.66 + 177.69, 46.66 + 139.76}};  // depth of position detector layers

Int_t Arm2AnPars::kPhotonNrap = 6;                       // number of rapidity regions (photons)
Int_t Arm2AnPars::kNeutronNrap = 6;                      // number of rapidity regions (neutrons)
Int_t Arm2AnPars::kPionNrapTypeI = 9;                    // number of rapidity regions (type I pions)
Double_t Arm2AnPars::kPionRapLowTypeI = 0.1;             // lower rapidity value (type I pions)
Double_t Arm2AnPars::kPionRapHighTypeI = 1.0;            // higher rapidity value (type I pions)
Int_t Arm2AnPars::kPionNrapTypeII[] = {9, 9};            // number of rapidity regions (type II pions)
Double_t Arm2AnPars::kPionRapLowTypeII[] = {0.1, 0.1};   // lower rapidity value (type II pions)
Double_t Arm2AnPars::kPionRapHighTypeII[] = {1.0, 1.0};  // higher rapidity value (type II pions)

Double_t Arm2AnPars::kDetectorOffset[] = {
    0.000, -20.000};                            // detector offset in true mc coordinates due to beam crossing angle
Double_t Arm2AnPars::kBeamCentre[] = {0., 0.};  // measured beam centre [mm] *** Fill 3855 - First half ***
Double_t Arm2AnPars::kBeamCentreAverageLayer[] = {0., 0.};  // layer where beam centre was measured
Double_t Arm2AnPars::kCrossingAngle = -145e-6;              // vertical crossing angle [rad] for LHC operations

Bool_t Arm2AnPars::kIsMC = false;    // is simulation not data
Bool_t Arm2AnPars::kIsFlat = false;  // is flat neutron simulation?

Double_t Arm2AnPars::kPionMassRange[3][2] = {{125.0, 145.0}, {125.0, 145.0}, {125.0, 145.0}};  // pi0 mass range
Double_t Arm2AnPars::kEnergyRescaleFactor[] = {1.0, 1.0};

TString Arm2AnPars::fPosAlignmentFile = "a2_pos_alignment.dat";

void Arm2AnPars::SetFiducialBorder(Double_t border) { fFiducial = border; }

void Arm2AnPars::SetFill(Int_t fill, Int_t subfill) {
  UT::Printf(UT::kPrintInfo, "Arm2AnPars: setting parameters for fill %d [%d]\n", fill, subfill);

  kIsFlat = false;

  /* This is the most generic case : To be used only for preliminary study */
  if (fill == 0) {
    kCrossingAngle = 0.;

    kBeamCentre[0] = 0.;
    kBeamCentre[1] = 0.;

    kBeamCentreAverageLayer[0] = 0;
    kBeamCentreAverageLayer[1] = 0;

    kDetectorOffset[0] = 0.;
    kDetectorOffset[1] = 0.;
  }
  /* SPS */
  else if (fOperation == kSPS2022 || fOperation == kSPS2021 || fOperation == kSPS2015) {
    kCrossingAngle = 0.;

    kBeamCentre[0] = 0.;
    kBeamCentre[1] = 0.;

    kBeamCentreAverageLayer[0] = 0;
    kBeamCentreAverageLayer[1] = 0;

    kDetectorOffset[0] = 0.;
    kDetectorOffset[1] = 0.;
  }
  /* LHC 2025 */
  else if (fOperation == kLHC2025) {
    kCrossingAngle = -145e-6;

    kBeamCentre[0] = 0.;
    kBeamCentre[1] = 0.;

    kBeamCentreAverageLayer[0] = 1;
    kBeamCentreAverageLayer[1] = 1;

    kDetectorOffset[0] = 0.;
    kDetectorOffset[1] = -20.;
    //        if (fill == XXXX) {
    //          if (subfill == 1) {
    //            kBeamCentre[0] = 0.;
    //            kBeamCentre[1] = 0.;
    //
    //            kBeamCentreAverageLayer[0] = 1;
    //            kBeamCentreAverageLayer[1] = 1;
    //
    //            kDetectorOffset[0] = 0.;
    //            kDetectorOffset[1] = -20.;
    //
    //            kEnergyRescaleFactor[0] = 1.;
    //            kEnergyRescaleFactor[1] = 1.;
    //          }
    //        }
  }
  /* LHC 2022 */
  else if (fOperation == kLHC2022) {
    kCrossingAngle = -145e-6;

    /*
     * Data
     */

    // Fill 8178 - Divided in four subgroups
    if (fill == 8178) {
      // Group 1 of 5 from Fill 8178: Nominal position
      if (subfill == 1) {
        kIsMC = false;

        kBeamCentre[0] = -3.322;
        kBeamCentre[1] = -1.220;

        kBeamCentreAverageLayer[0] = 1.573;
        kBeamCentreAverageLayer[1] = 1.707;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* Giuseppe's estimation on 6 June 2023 */
        // kEnergyRescaleFactor[0] = 1.1125;
        // kEnergyRescaleFactor[1] = 1.1125;

        /* Eugenio's estimation on 14 February 2024 from Type I */
        // kEnergyRescaleFactor[0] = 1.1293003;
        // kEnergyRescaleFactor[1] = 1.1293003;

        /* Eugenio's estimation on 14 February 2024 from Type II */
        // kEnergyRescaleFactor[0] = 1.1067663;
        // kEnergyRescaleFactor[1] = 1.1340164;

        /* Temporary value for recalibration */
        // Elena iter1
        // kEnergyRescaleFactor[0] = 1.0095270;
        // kEnergyRescaleFactor[1] = 1.0100462;
        // Elena iter2
        // kEnergyRescaleFactor[0] = 0.99788482;
        // kEnergyRescaleFactor[1] = 0.99878024;
        // Elena iter3
        kEnergyRescaleFactor[0] = 0.99942316;
        kEnergyRescaleFactor[1] = 0.99964594;
      }
      // TODO: Re-estimate beam center for all other runs!
      // Group 2 of 5 from Fill 8178: 5mm Higher position
      else if (subfill == 2) {
        kIsMC = false;

        kBeamCentre[0] = -3.660;
        kBeamCentre[1] = -6.602;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* Giuseppe's estimation on 6 June 2023 */
        kEnergyRescaleFactor[0] = 1.1125;
        kEnergyRescaleFactor[1] = 1.1125;
      }
      // Group 3 of 5 from Fill 8178: 5mm Higher position
      else if (subfill == 3) {
        kIsMC = false;

        kBeamCentre[0] = -3.654;
        kBeamCentre[1] = -5.985;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* Giuseppe's estimation on 6 June 2023 */
        kEnergyRescaleFactor[0] = 1.1125;
        kEnergyRescaleFactor[1] = 1.1125;
      }
      // Group 4 of 5 from Fill 8178: Nominal position
      else if (subfill == 4) {
        kIsMC = false;

        kBeamCentre[0] = -3.650;
        kBeamCentre[1] = -0.971;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* Giuseppe's estimation on 6 June 2023 */
        kEnergyRescaleFactor[0] = 1.1125;
        kEnergyRescaleFactor[1] = 1.1125;
      }
      // Group 5 of 5 from Fill 8178: 5mm Higher position
      else if (subfill == 5) {
        kIsMC = false;

        kBeamCentre[0] = -3.680;
        kBeamCentre[1] = -5.827;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* Giuseppe's estimation on 6 June 2023 */
        kEnergyRescaleFactor[0] = 1.1125;
        kEnergyRescaleFactor[1] = 1.1125;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }
      // Fill 8181 - Only at nominal position
    } else if (fill == 8181) {
      kIsMC = false;

      kBeamCentre[0] = -3.517;
      kBeamCentre[1] = -0.954;

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

      /* Giuseppe's estimation on 6 June 2023 */
      kEnergyRescaleFactor[0] = 1.1125;
      kEnergyRescaleFactor[1] = 1.1125;
    }

    /*
     * MC
     */

    // Fill 8178 - Divided in four subgroups
    else if (fill == -8178) {
      // Group 1 of 5 from Fill 8178: Nominal position
      if (subfill == -1) {
        kIsMC = true;

        kBeamCentre[0] = -3.322;
        kBeamCentre[1] = -1.220;

        kBeamCentreAverageLayer[0] = 1.573;
        kBeamCentreAverageLayer[1] = 1.707;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }
      // Group 2 of 5 from Fill 8178: 5mm Higher position
      else if (subfill == -2) {
        kIsMC = true;

        kBeamCentre[0] = -3.660;
        kBeamCentre[1] = -6.602;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }
      // Group 3 of 5 from Fill 8178: 5mm Higher position
      else if (subfill == -3) {
        kIsMC = true;

        kBeamCentre[0] = -3.654;
        kBeamCentre[1] = -5.985;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }
      // Group 4 of 5 from Fill 8178: Nominal position
      else if (subfill == -4) {
        kIsMC = true;

        kBeamCentre[0] = -3.650;
        kBeamCentre[1] = -0.971;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }
      // Group 5 of 5 from Fill 8178: 5mm Higher position
      else if (subfill == -5) {
        kIsMC = true;

        kBeamCentre[0] = -3.680;
        kBeamCentre[1] = -5.827;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }
      // Fill 8181 - Only at nominal position
    } else if (fill == -8181) {
      kIsMC = true;

      kBeamCentre[0] = -3.517;
      kBeamCentre[1] = -0.954;

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

      kEnergyRescaleFactor[0] = 1.;
      kEnergyRescaleFactor[1] = 1.;
    }

    //

    else {
      // TODO: Add Fill 8181
      UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  }
  /* LHC 2015 */
  else if (fOperation == kLHC2015) {
    kCrossingAngle = -145e-6;

    /*
     * Data
     */

    /* Fill 3855 (detector in nominal position) */
    if (fill == 3855) {
      if (subfill == 1) {  // pile-up = 0.01
        kBeamCentre[0] = -3.272;
        kBeamCentre[1] = -2.689;
      } else if (subfill == 2) {  // pile-up = 0.03
        kBeamCentre[0] = -3.488;
        kBeamCentre[1] = -2.706;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }

      kEnergyRescaleFactor[0] = 1.;
      kEnergyRescaleFactor[1] = 1.;

      kBeamCentreAverageLayer[0] = 1.701;
      kBeamCentreAverageLayer[1] = 1.830;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;
    }
    /* Fill 3851 (detector shifted up by 5 mm) */
    else if (fill == 3851) {
      if (subfill == 1) {  // pile-up = 0.01
        kBeamCentre[0] = -3.284;
        kBeamCentre[1] = -2.897;
        //
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }

      kEnergyRescaleFactor[0] = 1.;
      kEnergyRescaleFactor[1] = 1.;

      kBeamCentreAverageLayer[0] = 1.701;
      kBeamCentreAverageLayer[1] = 1.830;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -15.;
    }

    /*
     * MC
     */

    else if (fill == -3855) {
      if (subfill == 0) {  // pure pi0 MC
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0.;
      } else if (subfill == -1) {  // simulations (beam center is slightly different)
        kBeamCentre[0] = -3.272;
        kBeamCentre[1] = -2.689 - 0.4682233;
      } else if (subfill == -2) {  // flat simulations
        kIsFlat = true;
        kCrossingAngle = 0.;
        //
        kBeamCentre[0] = -3.272;
        kBeamCentre[1] = -2.689;
      }

      if (kIsFlat) {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      } else {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }

      kBeamCentreAverageLayer[0] = 1.701;
      kBeamCentreAverageLayer[1] = 1.830;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

    } else if (fill == -3851) {
      if (subfill == 0) {  // pure pi0 MC
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0.;
      } else if (subfill == -1) {  // simulations (beam center is slightly different)
        kBeamCentre[0] = -3.284;
        kBeamCentre[1] = -2.897 - 0.4682233;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }

      if (kIsFlat) {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      } else {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }

      kBeamCentreAverageLayer[0] = 1.701;
      kBeamCentreAverageLayer[1] = 1.830;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -15.;

    } else {
      UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  } else {
    UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown fill, exit...\n");
    exit(EXIT_FAILURE);
  }

  // Print parameters
  UT::Printf(UT::kPrintInfo, "        Energy rescale factors for TS and TL = %6.4lf, %6.4lf\n", kEnergyRescaleFactor[0],
             kEnergyRescaleFactor[1]);

  // IMPORTANT: Set Coordinate Transformation parameters here!
  CT::SetBeamCrossingAngle(1, kCrossingAngle);
  CT::SetDetectorPosition(1, kDetectorOffset[1]);
  CT::SetMeasuredBeamCentre(1,                               //
                            kBeamCentre[0], kBeamCentre[1],  //
                            kBeamCentreAverageLayer[0], kBeamCentreAverageLayer[1]);
  CT::SetDetectorPositionVector(1);
  CT::SetBeamCentreVector(1);
  if (!kIsMC) {
    if (fOperation == kLHC2022) {
      CT::SetAlignmentValues(1);
    }
  }
  //
  CT::PrintParameters(1, "Arm2AnPars");

  return;
}
