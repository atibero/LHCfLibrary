//
// Created by MENJO Hiroaki on 2018/12/11.
//

#include "McEvent.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(McEvent);
#endif

typedef Utils UT;

McEvent::McEvent() { DataClear(); }

McEvent::McEvent(const char* name, const char* title) : TNamed(name, title) {}

void McEvent::SortByEnergy(std::vector<McParticle*>& v) {
  sort(v.begin(), v.end(), [](McParticle* a, McParticle* b) { return a->Energy() > b->Energy(); });
}

int McEvent::CheckParticleInTower(Int_t armindex, Int_t towerindex, Double_t energythreshold, Double_t edgecut) {
  int nhit = 0;

  fReference.clear();
  fRefArm = armindex;
  fRefTower = towerindex;
  for (int iP = 0; iP < this->GetN(); iP++) {
    McParticle* particle = this->Get(iP);
    if (particle->CheckHit(armindex, towerindex, energythreshold, edgecut)) {
      fReference.push_back(particle);
      ++nhit;
    }
  }
  SortByEnergy(fReference);

  return nhit;
}

void McEvent::Print(Option_t* option) {
  UT::Printf(UT::kPrintInfo, "\n#Run %6d\n#Event %8d\n", fRun, fEvent);
  for (int iP = 0; iP < GetN(); iP++) Get(iP)->Print(option);
}

void McEvent::DataClear() {
  fRun = 0;
  fEvent = 0;
  fRefArm = -1;
  fRefTower = -1;
  fReference.clear();
  fParticleArray.Delete();
}

void McEvent::Add(McEvent mc) {
  for (unsigned int i = 0; i < mc.fReference.size(); ++i) this->fReference.push_back(mc.fReference[i]);
  for (unsigned int i = 0; i < mc.fParticleArray.GetEntries(); ++i) this->fParticleArray.AddLast(new McParticle((McParticle*)mc.fParticleArray[i]));
}

McEvent::~McEvent() {}
