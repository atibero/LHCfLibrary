#include "Level0.hh"

#include <iomanip>
#include <iostream>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "Utils.hh"

#if !defined(__CINT__)
templateClassImp(Level0);
#endif

/**
 *  @class nLHCf::Level0
 *  @brief The data containor containing the calibrated data
 *  @details This class contains all detector outputs after the gain calibration
 *  as well as the event header, the quality flag etc.
 */

using namespace nLHCf;

template <typename armclass>
Level0<armclass>::Level0() : LevelBase<armclass>::LevelBase() {
  AllocateVectors();
}

template <typename armclass>
Level0<armclass>::Level0(const char *name, const char *title) : LevelBase<armclass>::LevelBase(name, title) {
  AllocateVectors();
}

template <typename armclass>
Level0<armclass>::~Level0() {}

//////////////////////////////////////////////////
/// @brief Allocations of vectors
/// @tparam armclass

template <typename armclass>
void Level0<armclass>::AllocateVectors() {
  Utils::AllocateVector3D(fCalorimeter, this->kCalNtower, this->kCalNlayer, this->kCalNrange);
  Utils::AllocateVector3D(fCalSatFlag, this->kCalNtower, this->kCalNlayer, this->kCalNrange);

  Utils::AllocateVector1D(fPosDet, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    Utils::AllocateVector4D(fPosDet[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it], this->kPosNsample);

  Utils::AllocateVector2D(fFrontCounter, this->kFcNlayer, this->kFcNrange);

  Utils::AllocateVector2D(fZdc, this->kZdcNchannel, this->kZdcNrange);

  Utils::AllocateVector1D(fEventFlag, this->kNevFlag);
  Utils::AllocateVector1D(fOpenADC, this->kOpenAdcNch);
  Utils::AllocateVector1D(fLaser, this->kLaserNch);
  Utils::AllocateVector2D(fTdc, this->kTdcNch, this->kTdcNhit);
  Utils::AllocateVector2D(fTdcFlag, this->kTdcNch, this->kTdcNhit);
  Utils::AllocateVector1D(fScaler, this->kScalerNch);
  Utils::AllocateVector2D(fFifoCounter, this->kNfifo1, this->kNfifo2);
}

/////////////////////////////////////////////////////
/// @brief  Clear all data of Level0 and TNamed. (No recomendation to use)
/// @tparam armclass
/// @param option [in] Selection of data. \n
///                    option: default = "" ("" is replaced to "all" in this function.) \n
///                    For details, refer DataClear().

template <typename armclass>
void Level0<armclass>::Clear(Option_t *option) {
  TNamed::Clear(option);  // Clear all parameters of TNamed like name, title also
  TString opt = option;
  if (opt == "")
    DataClear("all");
  else
    DataClear(option);
  return;
}

////////////////////////////////////////////////////////////////////////////////
/// @brief  Clear the data. All values are set to the defaults (0.)
/// @tparam armclass
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    header, calorimeter, posdep, frontcounter, tdc, counters, qualityflag
/// @details An example of Clear with specifying data : Clear("calorimeter, posdep, tdc")
template <typename armclass>
void Level0<armclass>::DataClear(Option_t *option) {
  LevelBase<armclass>::DataClear(option);

  TString opt = option;
  opt.ToLower();

  if (opt.Contains("all") || opt.Contains("calorimeter")) {
    Utils::template ClearVector3D<Double_t>(fCalorimeter, 0.);
  }
  if (opt.Contains("all") || opt.Contains("posdet")) {
    Utils::template ClearVector5D<Double_t>(fPosDet, 0.);
  }
  if (opt.Contains("all") || opt.Contains("frontcounter")) {
    Utils::template ClearVector2D<Double_t>(fFrontCounter, 0.);
  }
  if (opt.Contains("all") || opt.Contains("zdc")) {
    Utils::template ClearVector2D<Double_t>(fZdc, 0.);
  }
  if (opt.Contains("all") || opt.Contains("tdc")) {
    Utils::template ClearVector2D<Double_t>(fTdc, -1.);
    Utils::template ClearVector2D<UInt_t>(fTdcFlag, 0);
  }
  if (opt.Contains("all") || opt.Contains("qualityflag")) {
    Utils::template ClearVector1D<Int_t>(fQualityFlag, -1);
  }

  return;
}

/////////////////////////////////////////////////////
/// @brief  Copy all data of Level0 and TNamed. (No recomendation to use)
/// @tparam armclass
/// @param d      [in] Copy from d to this
/// @param option [in] Selection of data. \n
///                    option: default = "" ("" is replaced to "all" in this function.) \n
///                    For details, refer DataClear().

template <typename armclass>
void Level0<armclass>::Copy(Level0 *d, Option_t *option) {
  // Clear all parameters of TNamed like name, title also
  TNamed::Copy(*d);

  TString opt = option;
  if (opt == "")
    DataCopy(d, "all");
  else
    DataCopy(d, option);
  return;
}

/////////////////////////////////////////////////////
/// @brief  Copy data from d to this
/// @tparam armclass
/// @param d      [in] Copy from d to this
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    header, calorimeter, posdep, frontcounter, tdc, counters, qualityflag
/// @details An example of Clear with specifying data : Copy(d, "calorimeter, posdep, tdc")
template <typename armclass>
void Level0<armclass>::DataCopy(Level0 *d, Option_t *option) {
  LevelBase<armclass>::DataCopy(d, option);

  TString opt = option;
  opt.ToLower();

  if (opt.Contains("all") || opt.Contains("calorimeter")) {
    Utils::template CopyVector3D<Double_t>(fCalorimeter, d->fCalorimeter);
  }
  if (opt.Contains("all") || opt.Contains("posdet")) {
    Utils::template CopyVector5D<Double_t>(fPosDet, d->fPosDet);
  }
  if (opt.Contains("all") || opt.Contains("frontcounter")) {
    Utils::template CopyVector2D<Double_t>(fFrontCounter, d->fFrontCounter);
  }
  if (opt.Contains("all") || opt.Contains("zdc")) {
    Utils::template CopyVector2D<Double_t>(fZdc, d->fZdc);
  }
  if (opt.Contains("all") || opt.Contains("tdc")) {
    Utils::template CopyVector2D<Double_t>(fTdc, d->fTdc);
    Utils::template CopyVector2D<UInt_t>(fTdcFlag, d->fTdcFlag);
  }
  if (opt.Contains("all") || opt.Contains("qualityflag")) {
    Utils::template CopyVector1D<Int_t>(fQualityFlag, d->fQualityFlag);
  }
  if (opt.Contains("all") || opt.Contains("ZDC")) {
    Utils::template CopyVector1D<Double_t>(fOpenADC, d->fOpenADC);
  }

  return;
}

/////////////////////////////////////////////////////
/// @brief  Add the data of d to this
/// @tparam armclass
/// @param d      [in] Add d to this
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    calorimeter, posdep, frontcounter
/// @details An example of Add with specifying data : Add(d, "calorimeter, posdep")
template <typename armclass>
void Level0<armclass>::Add(Level0 *d, Option_t *option) {
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("all") || opt.Contains("calorimeter")) {
    Utils::template AddVector3D<Double_t>(fCalorimeter, d->fCalorimeter);
  }
  if (opt.Contains("all") || opt.Contains("posdet")) {
    Utils::template AddVector5D<Double_t>(fPosDet, d->fPosDet);
  }
  if (opt.Contains("all") || opt.Contains("frontcounter")) {
    Utils::template AddVector2D<Double_t>(fFrontCounter, d->fFrontCounter);
  }
  if (opt.Contains("all") || opt.Contains("zdc")) {
    Utils::template AddVector2D<Double_t>(fZdc, d->fZdc);
  }

  return;
}

/////////////////////////////////////////////////////
/// @brief  Multiply a factor to the data
/// @tparam armclass
/// @param val    [in] A factor
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    calorimeter, posdep, frontcounter
/// @details An example of Multiply with specifying data : Multiply(d, "calorimeter, posdep")
template <typename armclass>
void Level0<armclass>::Multiply(Double_t val, Option_t *option) {
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("all") || opt.Contains("calorimeter")) {
    Utils::template MultiplyVector3D<Double_t>(fCalorimeter, val);
  }
  if (opt.Contains("all") || opt.Contains("posdet")) {
    Utils::template MultiplyVector5D<Double_t>(fPosDet, val);
  }
  if (opt.Contains("all") || opt.Contains("frontcounter")) {
    Utils::template MultiplyVector2D<Double_t>(fFrontCounter, val);
  }
  if (opt.Contains("all") || opt.Contains("zdc")) {
    Utils::template MultiplyVector2D<Double_t>(fZdc, val);
  }

  return;
}

//////////////////////////////////////////////////////////////
/// @brief Print-out data contained in this class.
template <typename armclass>
void Level0<armclass>::Print() {
  cout << "Level0 data" << endl;

  LevelBase<armclass>::Print();

  cout << "EventFlags:" << endl;
  for (Int_t i = 0; i < this->kNflag; i++) {
    cout << setw(5) << this->fEventFlag[i] << " ";
  }
  cout << endl;

  cout << "Scintillator Layers:" << endl;
  for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        cout << " " << fCalorimeter[it][il][ir] << flush;
      }
      cout << endl;
    }
  }

  cout << "Position Layers: " << endl;
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is) cout << " " << fPosDet[it][il][iv][ic][is] << flush;
  cout << endl;

  cout << "TDC:" << endl;
  for (int ich = 0; ich < this->kTdcNch; ich++) {
    cout << "ch" << setw(2) << ich << " : ";
    for (int i = 0; i < this->fTdc[ich].size(); i++) {
      cout << setw(6) << this->fTdc[ich][i] << " ";
    }
    cout << endl;
  }
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class Level0<Arm1Params>;

template class Level0<Arm2Params>;
}  // namespace nLHCf
