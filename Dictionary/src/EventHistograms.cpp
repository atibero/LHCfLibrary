#include <Arm1Params.hh>
#include <Arm2Params.hh>
#include <Utils.hh>
#include <iomanip>
#include <iostream>

using namespace std;

#include "EventHistograms.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(EventHistograms);
#endif

/**
 * @class nLHCf::EventHistograms
 * @brief
 * @details
 * This class contains all information of recostructed results from Level2
 *
 */

template <typename armclass>
EventHistograms<armclass>::EventHistograms() : TNamed() {
  static int icall = 0;
  this->SetName(Form("hists_a%d_%d", this->kArmIndex + 1, icall));
  this->SetTitle("");
  icall++;

  Initialize();
  return;
}

template <typename armclass>
EventHistograms<armclass>::EventHistograms(const char *name, const char *title) : TNamed(name, title) {
  Initialize();
}

template <typename armclass>
EventHistograms<armclass>::~EventHistograms() {
  Delete();
}

///////////////////////////////////////////////
/// Memory allocation of histograms
/// \tparam armclass
template <typename armclass>
void EventHistograms<armclass>::Initialize() {
  fDataType = NONE;
  AllocateHistograms();
}

///////////////////////////////////////////////
/// Memory allocation of histograms
/// \tparam armclass
template <typename armclass>
void EventHistograms<armclass>::AllocateHistograms() {
  Utils::AllocateVector2D(fHistCalorimeter, this->kCalNtower, this->kCalNrange);
  Utils::AllocateVector4D(fHistPosDet, this->kPosNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  Utils::AllocateVector1D(fHistFrontCounter, this->kFcNrange);
  Utils::AllocateVector1D(fHistZdc, this->kZdcNrange);

  TH1::SetDefaultSumw2();
  Utils::MakeTH1D_2D(fHistCalorimeter, TString::Format("%s_cal", this->GetName()), "Calorimeter", this->kCalNlayer, 0.,
                     (Double_t)this->kCalNlayer);

  for (int itower = 0; itower < this->kPosNtower; itower++) {
    Utils::MakeTH1D_3D(fHistPosDet[itower], TString::Format("%s_pos_%d", this->GetName(), itower), "Position Detector",
                       this->kPosNchannel[itower], 0, this->kPosNchannel[itower]);
  }

  Utils::MakeTH1D_1D(fHistFrontCounter, TString::Format("%s_fc", this->GetName()), "FrontCounter", this->kFcNlayer, 0.,
                     (Double_t)this->kFcNlayer);

  if (this->kZdcNchannel) {
    Utils::AllocateVector1D(fHistZdc, this->kZdcNrange);
    Utils::MakeTH1D_1D(fHistZdc, TString::Format("%s_zdc", this->GetName()), "ZDC", this->kZdcNchannel, 0.,
                       (Double_t)this->kZdcNchannel);
  }

  return;
}

///////////////////////////////////////////////
/// Delete Histograms
/// \tparam armclass
template <typename armclass>
void EventHistograms<armclass>::Delete() {
  DeleteHistograms();
}

///////////////////////////////////////////////
/// Delete Histograms
/// \tparam armclass
template <typename armclass>
void EventHistograms<armclass>::DeleteHistograms() {
  Utils::DeleteTH1D_2D(fHistCalorimeter);
  Utils::DeleteTH1D_4D(fHistPosDet);
  Utils::DeleteTH1D_1D(fHistFrontCounter);
  if (this->kZdcNchannel) Utils::DeleteTH1D_1D(fHistZdc);
}

///////////////////////////////////////////////
/// Fill the data to the histograms
/// \tparam armclass
/// \param d [in]
template <typename armclass>
void EventHistograms<armclass>::Fill(Level0<armclass> *d) {
  fDataType = LEVEL0;

  // Calorimeter
  for (int itower = 0; itower < this->kCalNtower; ++itower) {
    for (int ilayer = 0; ilayer < this->kCalNlayer; ++ilayer) {
      for (int ir = 0; ir < this->kCalNrange; ++ir) {
        fHistCalorimeter[itower][ir]->SetBinContent(ilayer + 1, d->fCalorimeter[itower][ilayer][ir]);
      }
    }
  }

  // Position Detector
  if (d->fPosDet.size())
    for (int itower = 0; itower < this->kPosNtower; ++itower)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer)
        for (int ixy = 0; ixy < this->kPosNview; ++ixy)
          for (int isample = 0; isample < this->kPosNsample; ++isample)
            for (int ich = 0; ich < this->kPosNchannel[itower]; ++ich)
              fHistPosDet[itower][ilayer][ixy][isample]->SetBinContent(ich + 1,
                                                                       d->fPosDet[itower][ilayer][ixy][ich][isample]);

  // Front Counter
  for (int i = 0; i < this->kFcNrange; ++i)
    for (int ilayer = 0; ilayer < this->kFcNlayer; ++ilayer) {
      //	fHistFrontCounter[iarm]->SetBinContent(ilayer, d->fFrontCounter[iarm][ilayer][0]);
      fHistFrontCounter[i]->SetBinContent(ilayer + 1, d->fFrontCounter[ilayer][i]);
    }

  // Zdc
  if (this->kZdcNchannel)
    for (int i = 0; i < this->kZdcNchannel; ++i)
      for (int j = 0; j < this->kZdcNrange; ++j) {
        fHistZdc[j]->SetBinContent(i + 1, d->fZdc[i][j]);
      }

  return;
}

///////////////////////////////////////////////
/// Fill the data to the histograms
/// \tparam armclass
/// \param d [in]
template <typename armclass>
void EventHistograms<armclass>::Fill(Level1<armclass> *d) {
  fDataType = LEVEL1;

  // Calorimeter
  for (int itower = 0; itower < this->kCalNtower; ++itower) {
    for (int ilayer = 0; ilayer < this->kCalNlayer; ++ilayer) {
      for (int ir = 0; ir < this->kCalNrange; ++ir) {
        fHistCalorimeter[itower][0]->SetBinContent(ilayer + 1, d->fCalorimeter[itower][ilayer][ir]);
      }
    }
  }

  // Position Detector
  if (d->fPosDet.size())
    for (int itower = 0; itower < this->kPosNtower; ++itower)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer)
        for (int ixy = 0; ixy < this->kPosNview; ++ixy)
          for (int isample = 0; isample < this->kPosNsample; ++isample)
            for (int ich = 0; ich < this->kPosNchannel[itower]; ++ich) {
              fHistPosDet[itower][ilayer][ixy][isample]->SetBinContent(ich + 1,
                                                                       d->fPosDet[itower][ilayer][ixy][ich][isample]);
            }

  // Front Counter
  for (int i = 0; i < this->kFcNrange; ++i)  // For Op2022
    for (int ilayer = 0; ilayer < this->kFcNlayer; ++ilayer) {
      //	fHistFrontCounter[iarm]->SetBinContent(ilayer, d->fFrontCounter[iarm][ilayer][0]);
      fHistFrontCounter[i]->SetBinContent(ilayer + 1, d->fFrontCounter[ilayer][i]);
    }

  // Zdc
  if (this->kZdcNchannel)
    for (int i = 0; i < this->kZdcNchannel; ++i)
      for (int j = 0; j < this->kZdcNrange; ++j) fHistZdc[j]->SetBinContent(i + 1, d->fZdc[i][j]);

  return;
}

///////////////////////////////////////////////
/// Fill the data to the histograms
/// \tparam armclass
/// \param d [in]
template <typename armclass>
void EventHistograms<armclass>::Fill(Level2<armclass> *d) {
  fDataType = LEVEL2;

  // Calorimeter
  for (int itower = 0; itower < this->kCalNtower; ++itower) {
    for (int ilayer = 0; ilayer < this->kCalNlayer; ++ilayer) {
      fHistCalorimeter[itower][0]->SetBinContent(ilayer + 1, d->fCalorimeter[itower][ilayer]);
    }
  }

  // Position Detector
  int isample = 0;
  if (d->fPosDet.size())
    for (int itower = 0; itower < this->kPosNtower; ++itower)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer)
        for (int ixy = 0; ixy < this->kPosNview; ++ixy)
          for (int ich = 0; ich < this->kPosNchannel[itower]; ++ich) {
            if (this->kPosNsample != 1 ) isample = 1;  // Arm2
            fHistPosDet[itower][ilayer][ixy][isample]->SetBinContent(ich + 1, d->fPosDet[itower][ilayer][ixy][ich][isample]);
            fHistPosDet[itower][ilayer][ixy][isample]->SetBinError(ich + 1, d->fErrPosDet[itower][ilayer][ixy][ich][isample]);
          }


  // Front Counter
  // for (int iarm = 0; iarm < this->kNarm; ++iarm) {
  for (int ilayer = 0; ilayer < this->kFcNlayer; ++ilayer) {
    //	fHistFrontCounter[iarm]->SetBinContent(ilayer, d->fFrontCounter[iarm][ilayer]);
    fHistFrontCounter[0]->SetBinContent(ilayer + 1, d->fFrontCounter[ilayer]);
  }
  //}

  // Zdc
  if (this->kZdcNchannel)
    for (int i = 0; i < this->kZdcNchannel; ++i) fHistZdc[0]->SetBinContent(i + 1, d->fZdc[i]);

  return;
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventHistograms<Arm1Params>;

template class EventHistograms<Arm2Params>;
}  // namespace nLHCf
