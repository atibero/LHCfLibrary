#include "LHCfParams.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(LHCfParams);
#endif

/* Global LHCf constants */
const Int_t LHCfParams::kNxyz = 3;
const Int_t LHCfParams::kNxy = 2;
const Int_t LHCfParams::kNarm = 2;
Double_t LHCfParams::kL20Thr = 0.2;  // L20% threshold
Double_t LHCfParams::kL90Thr = 0.9;  // L90% threshold
