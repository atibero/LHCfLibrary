#include "Arm2RecPars.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm2RecPars);
#endif

typedef Utils UT;

/* Calorimeters constants */
Int_t Arm2RecPars::kTrgDscNlayer = 3;                    // number of consecutive layers over threshold
Double_t Arm2RecPars::kTrgDscThr = 0.85;                 // trigger threshold [GeV]
Double_t Arm2RecPars::kTrgDscThrByLayer[2][16] = {{0}};  // this is later assigned

Int_t Arm2RecPars::kLeakInNpar = 3;           // # of paramaters of Leak-in correction
Int_t Arm2RecPars::kLeakBins[] = {100, 128};  // # of Leak-in bins
Int_t Arm2RecPars::kEconvNpar = 3;            // # of paramaters in SumdE -> E function

Int_t Arm2RecPars::kPhotonFirstLayer = 1;   // first layer used for SumdE (photons)
Int_t Arm2RecPars::kPhotonLastLayer = 12;   // last layer used for SumdE (photons)
Int_t Arm2RecPars::kNeutronFirstLayer = 2;  // first layer used for SumdE (neutrons)
Int_t Arm2RecPars::kNeutronLastLayer = 15;  // last layer used for SumdE (neutrons)

/* Position detectors constants */
Int_t Arm2RecPars::kAvgWindowPhoton = 3;           // TSpetrum average window
Int_t Arm2RecPars::kDeconIterPhoton = 1;           // TSpetrum deconvolution iterations
Double_t Arm2RecPars::kPeakSigmaPhoton = 5;        // TSpetrum peak width
Double_t Arm2RecPars::kPeakRatioThrPhoton = 0.05;  // TSpetrum ratio thr.
Double_t Arm2RecPars::kPeakAbsThrPhoton = 0.0015;  // TSpetrum absolute thr. [GeV]
Int_t Arm2RecPars::kAvgWindowNeutron = 3;          // TSpetrum average window //Eugenio: 3->5?
Int_t Arm2RecPars::kDeconIterNeutron =
    3;  // TSpetrum deconvolution iterations //Eugenio: no change observed, set it to 1?
Double_t Arm2RecPars::kPeakSigmaNeutron = 7;        // TSpetrum peak width //Eugenio: 7->5?
Double_t Arm2RecPars::kPeakRatioThrNeutron = 0.10;  // TSpetrum ratio thr.
Double_t Arm2RecPars::kPeakAbsThrNeutron = 0.001;   // TSpetrum absolute thr. [GeV]
/*Eugenio:
  - Removing background removal does not change so much shape, but add an undesired offset
  - Removing Markov fit better to data points (as expected), but may find undesired peaks?
*/

Int_t Arm2RecPars::kDefaultPhotonFitType = 0;       // Default type of the position fitting function for photons
Int_t Arm2RecPars::kDefaultNeutronFitType = 0;      // Default type of the position fitting function for neutrons
Int_t Arm2RecPars::kDefaultPhotonErecType = 1;      // Default type of the energy reconstruction for MH photons
//Int_t Arm2RecPars::kDefaultNeutronErecType = 1;   // For future implementation

Int_t Arm2RecPars::kPosNpars = 7;            // number of fit function parameters
Int_t Arm2RecPars::kMaxNparticle = 3;        // maximum # of hit fitted
Int_t Arm2RecPars::kClusterInformation = 3;  // maximum # of allowed track cluster
Int_t Arm2RecPars::kPosNlayerEM = 2;         // number of layers for photon analysis
Double_t Arm2RecPars::kMinDistPhoton = 1.;   // min. distance for a multi-hit in photon reconctruction
Double_t Arm2RecPars::kMinDistNeutron =
    2.;  // min. distance for a multi-hit in neutron reconstruction //Eugenio: need to optimize this

Double_t Arm2RecPars::kPosSaturationThr = 1000.;  // pos. detector saturation threshold (!!! TODO !!!)

/* Depth in X0 */

// TODO: This numbers refer to the upgraded detector, need to define an external table for elder data

Double_t Arm2RecPars::kCalLayerX0[2][16] = {
    {2.062, 4.164, 6.285, 8.392, 10.490, 12.611, 14.712, 16.814, 18.899, 20.992, 23.085, 27.185, 31.301, 35.396, 39.466,
     43.587},  // ST
    {2.044, 4.132, 6.237, 8.319, 10.403, 12.502, 14.586, 16.667, 18.758, 20.838, 22.928, 27.022, 31.089, 35.166, 39.260,
     43.356}  // LT
};            // depth of scintillator detector layers in terms of radiation length
Double_t Arm2RecPars::kPosLayerX0[2][4][2] = {
    // NB: Y must be before X!
    {{6.244, 6.228}, {12.570, 12.554}, {25.141, 18.852}, {41.551, 33.348}},  // ST
    {{6.197, 6.181}, {12.461, 12.445}, {24.983, 18.711}, {41.308, 33.125}}   // LT
};  // depth of position detector layers in terms of radiation length

/* Table files */
TString Arm2RecPars::fPhotonLeakageFile = "a2_photon_leakage-efficiency.root";
TString Arm2RecPars::fPhotonLeakInFile = "a2_old_photon_leakage-in.dat";
TString Arm2RecPars::fNeutronLeakageFile = "a2_neutron_leakage.root";
TString Arm2RecPars::fNeutronLeakInFile = "a2_old_neutron_leakage-in.dat";
TString Arm2RecPars::fNeutronEffFile = "a2_neutron_efficiency.root";
TString Arm2RecPars::fPhotonEconvFile = "a2_photon_energy-conversion.dat";
TString Arm2RecPars::fNeutronEconvFile = "a2_neutron_energy-conversion.dat";
TString Arm2RecPars::fPosDeadChFile = "a2_pos_dead_channels.dat";

Arm2RecPars::ParInit::ParInit() {
  UT::Printf(UT::kPrintInfo, "Arm2RecPars: initialising parameters...");
  fflush(stdout);

  for (Int_t it = 0; it < 2; ++it)
    for (Int_t il = 0; il < 16; ++il) {
      kTrgDscThrByLayer[it][il] = kTrgDscThr;
    }

  if (fOperation == kLHC2015) {
    UT::Printf(UT::kPrintInfo, " (LHC2015)");
    fflush(stdout);
    kPosSaturationThr = 1000;  // ADC
  } else if (fOperation == kSPS2015) {
    UT::Printf(UT::kPrintInfo, " (SPS2015)");
    fflush(stdout);
    kPosSaturationThr = 1000;  // ADC
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2021) {
    UT::Printf(UT::kPrintInfo, " (SPS2021)");
    fflush(stdout);
    kPosSaturationThr = 5000;  // ADC (!!! TODO !!!)
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2022) {
    UT::Printf(UT::kPrintInfo, " (SPS2022)");
    fflush(stdout);
    kPosSaturationThr = 5000;  // ADC (!!! TODO !!!)
    /* !!! TODO !!! */
  } else if (fOperation == kLHC2022) {
    UT::Printf(UT::kPrintInfo, " (LHC2022)");
    fflush(stdout);
    kPosSaturationThr = 1000000;  // This is meaningless now

    Double_t kTrgDscThr99[2][16] = {
        {0.53242, 0.565503, 0.588973, 0.621501, 1.18885, 6.58392, 1.65133, 0.514235, 0.681353, 1.24568, 0.497217,
         1.12938, 0.75516, 1.13262, 1.15236, 0.573391},  // Small Tower
        {0.69968, 0.675582, 0.690171, 0.703855, 0.82589, 6.02084, 0.650602, 0.73455, 0.845967, 0.81544, 0.519089,
         0.69916, 0.38498, 0.51875, 0.67539, 0.552278}  // Large Tower
    };  // value corresponding to 99% efficiency of discriminator threshold for LHC2022
    for (Int_t it = 0; it < 2; ++it) {
      for (Int_t il = 0; il < 16; ++il) {
        kTrgDscThrByLayer[it][il] = kTrgDscThr99[it][il];
      }
    }
  } else if (fOperation == kLHC2025) {
    UT::Printf(UT::kPrintInfo, " (LHC2025)");
    fflush(stdout);
    kPosSaturationThr = 5000;  // ADC (!!! TODO !!!)
  } else {
    UT::Printf(UT::kPrintError, "Arm2RecPars::Initialise: unknown operation\n");
    exit(EXIT_FAILURE);
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

Arm2RecPars::ParInit Arm2RecPars::fInitialiser;
