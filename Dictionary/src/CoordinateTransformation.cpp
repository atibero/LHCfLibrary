//
// Created by MENJO Hiroaki on 2018/12/04.
// Heavily modified by Eugenio on 2024/04/01
//

#include "CoordinateTransformation.hh"

#include <Arm1AnPars.hh>
#include <Arm1Params.hh>
#include <Arm2AnPars.hh>
#include <Arm2Params.hh>
#include <Utils.hh>
#include <fstream>
#include <iomanip>
#include <iostream>

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(CoordinateTransformation);
#endif

typedef Utils UT;

/**
 * \class nLHCf::CoordinateTransformation
 * \brief Coordinate transformation among the system used in LHCf analysis
 * \details
 *
 * \par Coordinate systems
 *
 * - Global coordinates (x,y,z) \n
 * These coordinates' origin is always IP. Z axis is mostly beam direction (different between Arm1 and Arm2). Y axis is
vertical direction. \n
 * The difference among the followings are the direction of Z axis, the projection point of Z-axis on the detector
plain. \n
 * The coordinate transformation among them is treated as a rotation.
 *  - LHC coordinate     The z-axis projection is on the "beam pipe center".
 *  - Detector coordinate  The z-axis projection is on the defined "detector center".
 *  - Collision coordinate  The z-axis projection is on the "beam center".
 * - Local coordinate (x,y) \n
 * The system are defined for each calorimeter tower. One of the corners is the origin. \n
 * The coordinate transformation between calorimeter coordinate and the global coordinate is treated as a shift (+ 45
degree rotation for Arm1)
 *  - Arm1(Arm2) small tower coordinate
 *  - Arm1(Arm2) large tower coordinate
 *　
 *
 * Coordinate transformations among these system)\n
 * - LHC <--(rotation around x axis)--> Detector <--(rotation around perpendicular vector of these z-axis)--> Collisions
\n
 * This works also for the coordinate transformation not only for position but also for momentum vector.
 * - Detector <--(shirt+otation)--> Calorimeters
 *
 *
 * \par How to use
 * This class has only static members and functions. \n
 * Firstly you need to set the main parameters, detector position
 * and the beam center position. These are affected to any results of functions.
 * Then you can call any functions for coordinate transformations.
 *
 * The return value is always TVector3 class (or TLorentzVector if input is this class).
 *
\code
  #include <CoordinateTransformation.hh>
        using namespace nLHCf;

        // Set the parameters, Detector position and Beam center.
        CoordinateTransformation::SetDetectorPosition(-20.0);      // "mm" unit.
        CoordinateTransformation::SetBeamCentrePosition(3.0, 1.0); // "mm". The beam center position on "Detector"
coordinate

  // Coordinate transformation among global coordinates.
  TVector3 pos_in(10., -0.1, 141050.);  // Note) Do not forget to give Z value
  TVector3 pos_result;
  pos_result = CoordinateTransformation::LhcToDetector(pos_in); // From LHC to Detector
  pos_result.Print();
  pos_result = CoordinateTransformation::LhcToCollision(pos_in); // From LHC to Collision
  pos_result.Print();
  pos_result = CoordinateTransformation::LhcToCollision(10., -0.1);  // If x,y on Detector plain, it works.
  pos_result.Print();

  // Member functions of TVector3 can be called X(), Y(), Z(), y();
  cout << CoordinateTransformation::LhcToCollision(10., -0.1).X() << "  "
       << CoordinateTransformation::LhcToCollision(10., -0.1).Y() << endl;

  // For momentum bector
  TLorentzVector p_in, p_result;
  p_in.SetXYZM(0.1,0.05,1000.,0.); // set px, py, pz, mass for photon
  p_result = CoordinateTransformation::LhcToCollision(p_in);
  p_result.Print();

  // Coordinate transformation from calorimeter to detector;
  TVector3 pos_result;
  pos_result = CoordinateTransformation::CalorimeterToDetector(10.0,5.0, Arm1Params::kArmIndex, 0);
  // Argumenets (x, y);
  //   armindex should be given as Arm1Params::kArmIndex or Arm2Params::kArmIndex
  //   towerindex is 0 for the small tower and 1 for the large tower.
  pos_result.Print();

  // Recommend to use typedef as following because the class name is too long.
  typedef nLHCf::CoordinateTransformation CT;
  cout << CT::DetectorToLhc(10.5, 11.2).X() << "  "
       << CT::DetectorToCollision(10.5, 11.2).X() << endl;

 \endcode
 *
 * \note Use only this code for the coordinate transformation (do not implement yourself). Otherwise, we make confusion.
 *
 * \image html Coordinates.png
 *
 */

const Double_t CoordinateTransformation::kDetectorZpos = 141050.;   // [mm]
const Double_t CoordinateTransformation::kEnd2EndInZpos = 139800.;  // [mm]
const Double_t CoordinateTransformation::kLHCftolayer0 = 46.66;     // [mm]

/*
 * NB: fDetectorPositionY and fCrossingAngle are both negative by definition
 * 	   For this reason when we subtract them we mean to a positive quantity
 * 	   (and vice versa)
 */
// Old variables
TRotation CoordinateTransformation::sRotationLhcToDetector[2];
TRotation CoordinateTransformation::sRotationDetectorToCollision[2];
// New variables
Double_t CoordinateTransformation::fCrossingAngle[2] = {0., 0.};  // [rad]
Double_t CoordinateTransformation::fTanCrossAngle[2] = {0., 0.};  // Tangent
Double_t CoordinateTransformation::fDetectorPositionY[2] = {0., 0.};
Double_t CoordinateTransformation::fBeamCentrePosition[2][2] = {{0., 0.}, {0., 0.}};
Double_t CoordinateTransformation::fBeamCentreAverageLayer[2][2] = {{0., 0.}, {0., 0.}};
Double_t CoordinateTransformation::fArmTowerGap[2][2] = {{0.0, 5.0}, {1.8, 1.8}};
Double_t CoordinateTransformation::fAlignmentCorrection[2][2][4][2] = {
    // Arm1
    {{{0., 0.}, {0., 0.}, {0., 0.}, {0., 0.}},   // TS
     {{0., 0.}, {0., 0.}, {0., 0.}, {0., 0.}}},  // TL
    // Arm2
    {{{0., 0.}, {0., 0.}, {0., 0.}, {0., 0.}},  // TS
     {{0., 0.}, {0., 0.}, {0., 0.}, {0., 0.}}}  // TL
};
// Global vectors
TVector3 CoordinateTransformation::vBeamCentrePosition[2] = {TVector3(0., 0., 0.), TVector3(0., 0., 0.)};
TVector3 CoordinateTransformation::vDetectorPosition[2] = {TVector3(0., 0., 0.), TVector3(0., 0., 0.)};
// Bolean variables
Bool_t CoordinateTransformation::fUseRotation = false;
Bool_t CoordinateTransformation::fRotatedArm = false;

CoordinateTransformation::CoordinateTransformation() { WriteParameters(-1e6); }

CoordinateTransformation::~CoordinateTransformation() { ; }

////////////////////////////////////////
/// Write the parameters of this class
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param option [in]
void CoordinateTransformation::WriteParameters(Int_t armindex, Option_t *option) {
  // Arm1
  if (armindex < 0 || armindex == Arm1Params::kArmIndex) {
    SetBeamCrossingAngle(0, fCrossingAngle[0]);
    SetDetectorPosition(0, fDetectorPositionY[0]);
    SetMeasuredBeamCentre(0, fBeamCentrePosition[0][0],  //
                          fBeamCentrePosition[0][1], fBeamCentreAverageLayer[0][0], fBeamCentreAverageLayer[0][1]);
    SetDetectorPositionVector(0);
    SetBeamCentreVector(0);
    SetAlignmentValues(0);
  }
  // Arm2
  if (armindex < 0 || armindex == Arm2Params::kArmIndex) {
    SetBeamCrossingAngle(1, fCrossingAngle[1]);
    SetDetectorPosition(1, fDetectorPositionY[1]);
    SetMeasuredBeamCentre(1, fBeamCentrePosition[1][0],  //
                          fBeamCentrePosition[1][1], fBeamCentreAverageLayer[1][0], fBeamCentreAverageLayer[1][1]);
    SetDetectorPositionVector(1);
    SetBeamCentreVector(1);
    SetAlignmentValues(1);
  }
  // Print Class Parameters
  PrintParameters(armindex, option);
}

////////////////////////////////////////
/// Print the parameters of this class
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param option [in]
void CoordinateTransformation::PrintParameters(Int_t armindex, Option_t *option) {
  TString call = option;

  if (armindex < 0) {
    UT::Printf(UT::kPrintInfo, "\nInitialize Cooordinate Transformation with default parameters\n");
    return;
  }

  UT::Printf(UT::kPrintInfo, "\n%s - Parameters of Cooordinate Transformation for Arm%d:\n", call.Data(), armindex + 1);
  UT::Printf(UT::kPrintInfo, "\tSubtraction Method: %s\n", fUseRotation ? "Rotation" : "Offset");
  UT::Printf(UT::kPrintInfo, "\tDetector Geometry: %s\n", fRotatedArm ? "Rotated" : "Standard");
  UT::Printf(UT::kPrintInfo, "\tBeam crossing angle: %g rad\n", fCrossingAngle[armindex]);
  UT::Printf(UT::kPrintInfo, "\tDetector Y position: %f mm\n", fDetectorPositionY[armindex]);
  UT::Printf(UT::kPrintInfo, "\tArm tower gap:\n");
  UT::Printf(UT::kPrintInfo, "\t\tx: %f mm\n", fArmTowerGap[armindex][0]);
  UT::Printf(UT::kPrintInfo, "\t\ty: %f mm\n", fArmTowerGap[armindex][1]);
  UT::Printf(UT::kPrintInfo, "\tBeam center position:\n");
  UT::Printf(UT::kPrintInfo, "\t\tx: %f mm\n", fBeamCentrePosition[armindex][0]);
  UT::Printf(UT::kPrintInfo, "\t\ty: %f mm\n", fBeamCentrePosition[armindex][1]);
  UT::Printf(UT::kPrintInfo, "\tBeam center average layer depth:\n");
  UT::Printf(UT::kPrintInfo, "\t\tx: %f mm\n", ComputeBeamCentreZ(armindex, 0));
  UT::Printf(UT::kPrintInfo, "\t\ty: %f mm\n", ComputeBeamCentreZ(armindex, 1));
  UT::Printf(UT::kPrintInfo, "\tReal detector position vector:\n");
  UT::Printf(UT::kPrintInfo, "\t\tx: %f mm\n", vDetectorPosition[armindex].X());
  UT::Printf(UT::kPrintInfo, "\t\ty: %f mm\n", vDetectorPosition[armindex].Y());
  UT::Printf(UT::kPrintInfo, "\t\tz: %f mm\n", vDetectorPosition[armindex].Z());
  UT::Printf(UT::kPrintInfo, "\tReal beam center vector:\n");
  UT::Printf(UT::kPrintInfo, "\t\tx: %f mm\n", vBeamCentrePosition[armindex].X());
  UT::Printf(UT::kPrintInfo, "\t\ty: %f mm\n", vBeamCentrePosition[armindex].Y());
  UT::Printf(UT::kPrintInfo, "\t\tz: %f mm\n", vBeamCentrePosition[armindex].Z());
  UT::Printf(UT::kPrintInfo, "\tAlignment correction factors:\n");
  UT::Printf(UT::kPrintInfo, "\t\tSmall Tower\n");
  for (Int_t il = 0; il < 4; ++il) {
    for (Int_t iv = 0; iv < 2; ++iv) {
      UT::Printf(UT::kPrintInfo, "\t\t\t%d%s: %.3f strip\n", il, iv == 0 ? "X" : "Y",
                 fAlignmentCorrection[armindex][0][il][iv]);
    }
  }
  UT::Printf(UT::kPrintInfo, "\t\tLarge Tower\n");
  for (Int_t il = 0; il < 4; ++il) {
    for (Int_t iv = 0; iv < 2; ++iv) {
      UT::Printf(UT::kPrintInfo, "\t\t\t%d%s: %.3f strip\n", il, iv == 0 ? "X" : "Y",
                 fAlignmentCorrection[armindex][1][il][iv]);
    }
  }
  UT::Printf(UT::kPrintInfo, "\n");

  if (fUseRotation) {
    UT::Printf(UT::kPrintInfo, "\nRotation subtraction method is discouraged! Exit...\n");
    exit(EXIT_FAILURE);
  }

  return;
}

//////////////////////////////////////////////
/// \brief Static function for setting the crossing angle
/// \details The input (angle) is the half beam crossing angle. The unit is "rad"
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param angle [in]
void CoordinateTransformation::SetBeamCrossingAngle(Int_t armindex, Double_t angle) {
  fCrossingAngle[armindex] = angle;
  fTanCrossAngle[armindex] = TMath::Tan(fCrossingAngle[armindex]);
}

///////////////////////////////////////////////
/// @brief Static function for setting the detector position.
/// @details The input is the detector vertical position in "mm". \n
/// The value must be relative to the beam pipe center. \n
/// for example, nominal position in Op2015 is -20.0 and 5mmHigh is -15.0.
///
/// \param yposition [in] detector vertical position with the unit of "mm"
///
void CoordinateTransformation::SetDetectorPosition(Int_t armindex, Double_t yposition) {
  fDetectorPositionY[armindex] = yposition;
}

///////////////////////////////////////////////
/// \brief Static function for setting the beam center position.
/// \details The input (xpos,ypos) is the beam center position in the detector coordinate system. The unit is "mm"
/// \details The input (xlay,ylay) is the corresponding average layer where center is computed. The unit is "layer"
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param xpos [in]
/// \param ypos [in]
/// \param xlay [in]
/// \param ylay [in]
void CoordinateTransformation::SetMeasuredBeamCentre(Int_t armindex,  //
                                                     Double_t xpos, Double_t ypos, Double_t xlay, Double_t ylay) {
  fBeamCentrePosition[armindex][0] = xpos;
  fBeamCentrePosition[armindex][1] = ypos;
  fBeamCentreAverageLayer[armindex][0] = xlay;
  fBeamCentreAverageLayer[armindex][1] = ylay;
}

/////////////////////////////////////////////////
/// \brief Set the gap size between calorimeter towers. [mm]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param x [in]
/// \param y [in]
void CoordinateTransformation::SetArmTowerGap(Int_t armindex, Double_t x, Double_t y) {
  fArmTowerGap[armindex][0] = x;
  fArmTowerGap[armindex][1] = y;
}

/////////////////////////////////////////////////
/// \brief Set the real detector position offset to be subtracted for the given Arm
/// \param armindex [in]    specify as Arm*Params::kArmIndex
void CoordinateTransformation::SetDetectorPositionVector(Int_t armindex) {
  /*
   * SPS geometry
   */
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015) {
    vDetectorPosition[armindex].SetXYZ(0., 0., 0.);
    return;
  }

  /*
   * Flat simulation with LHC geometry
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    vDetectorPosition[armindex].SetXYZ(0, fDetectorPositionY[armindex], 0);
    return;
  }

  /*
   * LHC geometry
   */
  // Default method as offset
  vDetectorPosition[armindex].SetXYZ(0, fDetectorPositionY[armindex], 0);

  // Discouraged method as rotation
  if (fUseRotation) {
    Double_t theta = TMath::ATan(fDetectorPositionY[armindex] / kDetectorZpos);
    // Initialize
    sRotationLhcToDetector[armindex].SetToIdentity();
    // Rotate around Y axis
    sRotationLhcToDetector[armindex].RotateX(theta);
  }
}

/////////////////////////////////////////////////
/// \brief Set the real beam center vector to be subtracted for the given Arm
/// \param armindex [in]    specify as Arm*Params::kArmIndex
void CoordinateTransformation::SetBeamCentreVector(Int_t armindex) {
  /*
   * SPS geometry
   */
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015) {
    vBeamCentrePosition[armindex].SetXYZ(0., 0., 0.);
    return;
  }

  /*
   * Flat simulation with LHC geometry (compromise between beam center and cross angle offset)
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    // Double_t zaxis = 0.5*(ComputeBeamCentreZ(armindex, 0)+ComputeBeamCentreZ(armindex, 1));
    Double_t realy = fBeamCentrePosition[armindex][1];
    Double_t realx = fBeamCentrePosition[armindex][0];
    vBeamCentrePosition[armindex].SetX(realx);
    vBeamCentrePosition[armindex].SetY(realy);
    vBeamCentrePosition[armindex].SetZ(0.);
    return;
  }

  /*
   * LHC geometry
   */
  // The average z position refers only to y, since x is not affected by crossing angle
  // Double_t realz = 0.5*(ComputeBeamCentreZ(armindex, 0)+ComputeBeamCentreZ(armindex, 1));
  Double_t zaxis = ComputeBeamCentreZ(armindex, 1);
  Double_t realy = ComputeBeamCentreXY(armindex, 1, zaxis);
  Double_t realx = ComputeBeamCentreXY(armindex, 0, zaxis);
  // Default method as offset
  vBeamCentrePosition[armindex].SetX(realx);
  vBeamCentrePosition[armindex].SetY(realy);
  vBeamCentrePosition[armindex].SetZ(0);
  // Discouraged method as rotation
  if (fUseRotation) {
    Double_t zpos = ComputeBeamCentreZ(armindex, 1);
    Double_t ypos = fBeamCentrePosition[armindex][1];
    Double_t xpos = fBeamCentrePosition[armindex][0];
    // Initialize
    sRotationDetectorToCollision[armindex].SetToIdentity();
    // Rotate around the vector of cross product detector center and beam center
    Double_t z = kDetectorZpos;
    TVector3 v0(0., 0., zpos);
    TVector3 v1(xpos, ypos, zpos);
    TVector3 vc = v0.Cross(v1);
    Double_t theta = -1. * TMath::Sqrt(xpos * xpos + ypos * ypos) / zpos;
    //
    sRotationDetectorToCollision[armindex].Rotate(theta, vc);
  }
}

/////////////////////////////////////////////////
/// \brief Set the relative alignment correction offset
/// \param armindex [in]    specify as Arm*Params::kArmIndex
void CoordinateTransformation::SetAlignmentValues(Int_t armindex) {
  TString tableName;
  if (armindex == Arm1Params::kArmIndex) {
    tableName = Arm1Params::kTableDirectory + "/" + Arm1AnPars::fPosAlignmentFile;
  } else {
    tableName = Arm2Params::kTableDirectory + "/" + Arm2AnPars::fPosAlignmentFile;
  }
  ifstream infile(tableName.Data(), ifstream::in);

  if (!infile) {
    UT::Printf(UT::kPrintError,
               "CoordinateTransformation::SetAlignmentValues:"
               " Alignment File %s does not exist: Exit...\n",
               tableName.Data());
    exit(EXIT_FAILURE);
  }

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer, view;
    Double_t offset;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;     // Skip comment lines
    if (!strcmp(buf, "")) continue;  // Skip empty lines
    sscanf(buf, "%d %d %d %lf", &tower, &layer, &view, &offset);
    fAlignmentCorrection[armindex][tower][layer][view] = offset;
  }

  infile.close();
}

/////////////////////////////////////////////////
/// \brief Compute beam centre average z position for x or y
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param viewindex [in]    specify if computing for x(0) or y(1)
/// \return
Double_t CoordinateTransformation::ComputeBeamCentreZ(Int_t armindex, Int_t viewindex) {
  Int_t truncation = (Int_t)fBeamCentreAverageLayer[armindex][viewindex];
  Int_t suplay = truncation + 1;
  Int_t inflay = truncation;
  Double_t suppos = 0.;
  Double_t infpos = 0.;
  //
  if (armindex == Arm1Params::kArmIndex) {
    infpos = Arm1AnPars::kPosLayerDepth[inflay][viewindex];
    suppos = Arm1AnPars::kPosLayerDepth[suplay][viewindex];
  } else {
    infpos = Arm2AnPars::kPosLayerDepth[inflay][viewindex];
    suppos = Arm2AnPars::kPosLayerDepth[suplay][viewindex];
  }
  //
  Double_t average = fBeamCentreAverageLayer[armindex][viewindex];
  Double_t angular = (suppos - infpos) / (suplay - inflay);
  Double_t axiscoo = kDetectorZpos + angular * (average - inflay) + infpos;
  //
  return axiscoo;
}

/////////////////////////////////////////////////
/// \brief Compute beam centre x or y position taking into account crossing angle offset
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param viewindex [in]    specify if computing for x(0) or y(1)
/// \param zedcoo [in]    is the zed coordinate where beam centre was measured
/// \return
Double_t CoordinateTransformation::ComputeBeamCentreXY(Int_t armindex, Int_t viewindex, Double_t zedcoo) {
  TVector3 posOrg(fBeamCentrePosition[armindex][0], fBeamCentrePosition[armindex][1], zedcoo);
  TVector3 posOff = AddDetectorYOffset(armindex, SubCrossAngleOffset(armindex, posOrg));
  //
  if (viewindex == 0)
    return posOff.X();
  else
    return posOff.Y();
}

/////////////////////////////////////////////////
/// \brief Set beam center as a rotation instead of a shift
/// \param rotation [in]
void CoordinateTransformation::SetUseRotation(Bool_t rotation) { fUseRotation = rotation; }

/////////////////////////////////////////////////
/// \brief Set the coordinates for rotated SPS geometry
/// \param rotation [in]
void CoordinateTransformation::SetRotatedArm(Bool_t armated) { fRotatedArm = armated; }

//////////////// Old function //////////////////
/// \brief Static function for setting the beam center position.
/// \details The input (x,y) is the beam center position in the detector coordinate system. The unit is "mm"
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param x [in]
/// \param y [in]
/// \param z [in]
void CoordinateTransformation::SetBeamCentrePosition(Int_t armindex, Double_t x, Double_t y, Double_t z) {
  // Copy to the static variable
  fBeamCentrePosition[armindex][0] = x;
  fBeamCentrePosition[armindex][1] = y;

  if (fUseRotation) {
    // Initialize
    sRotationDetectorToCollision[armindex].SetToIdentity();
    // Rotate around the vector of cross product detector center and beam center
    TVector3 v0(0., 0., z);
    TVector3 v1(x, y, z);
    TVector3 vc = v0.Cross(v1);
    Double_t theta = -1. * TMath::Sqrt(x * x + y * y) / z;
    //
    sRotationDetectorToCollision[armindex].Rotate(theta, vc);
  } else {  // DEFAULT
    vBeamCentrePosition[armindex].SetXYZ(x, y, 0.);
  }

  return;
}

////////////////////////////////////////
/// \brief Coordinate extrapolation of 3D-vector to a given Z coordinate
/// \param pos [in] position
/// \param z [in] Z
/// \return the position at the given Z coordinate
TVector3 CoordinateTransformation::ExtrapolateToZ(TVector3 pos, Double_t z) {
  /*
   * Flat simulation with LHC geometry
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    return TVector3(pos.X(), pos.Y(), z);
  }

  /*
   * LHC geometry
   */
  Double_t t = z / pos.Z();
  TVector3 extrapolated(t * pos.X(), t * pos.Y(), z);
  return extrapolated;
}

////////////////////////////////////////
/// \brief alias
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param lx [in]	Layer x
/// \param ly [in]	Layer y
/// \param px [in]	Position x
/// \param py [in]	Position y
/// \return
TVector3 CoordinateTransformation::ChannelToCalorimeter(Int_t armindex, Int_t towerindex, Int_t lx, Int_t ly,
                                                        Double_t px, Double_t py) {
  TVector3 cal_pos;

  /* Without tilt correction */
  if (armindex == Arm1Params::kArmIndex) {
    /*-------- Arm1 (GSO bars) --------*/
    const Double_t pitch = Arm1Params::kPosPitch;
    cal_pos.SetXYZ(px + pitch / 2., py + pitch / 2., kDetectorZpos);
  } else if (armindex == Arm2Params::kArmIndex) {
    /*-------- Arm2 (Silicon) --------*/
    const Double_t pitch = Arm2Params::kPosPitch;
    if (towerindex == 0)  // small tower
      cal_pos.SetXYZ(pitch * px - 34.90, 60.08 - pitch * py, kDetectorZpos);
    else  // large tower
      cal_pos.SetXYZ(pitch * px - 1.10, 33.28 - pitch * py, kDetectorZpos);
  } else {
    Utils::Printf(Utils::kPrintError, "ChannelToCalorimeter: invalid armindex\n");
    exit(EXIT_FAILURE);
  }

  /* Apply shift (not tilt) correction */
  if (lx >= 0) {
    cal_pos.SetX(cal_pos.X() - fAlignmentCorrection[armindex][towerindex][lx][0]);
  }
  if (ly >= 0) {
    cal_pos.SetY(cal_pos.Y() - fAlignmentCorrection[armindex][towerindex][lx][1]);
  }

  return cal_pos;
}

////////////////////////////////////////
/// \brief alias
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param lx [in]	Layer x
/// \param ly [in]	Layer y
/// \param px [in]	Position x
/// \param py [in]	Position y
/// \return
TVector3 CoordinateTransformation::CalorimeterToChannel(Int_t armindex, Int_t towerindex, Int_t lx, Int_t ly,
                                                        Double_t px, Double_t py) {
  TVector3 ch_pos;

  /* Without tilt correction */
  if (armindex == Arm1Params::kArmIndex) {
    /*-------- Arm1 (GSO bars) --------*/
    const Double_t pitch = Arm1Params::kPosPitch;
    ch_pos.SetXYZ(px - pitch / 2., py - pitch / 2., kDetectorZpos);
  } else if (armindex == Arm2Params::kArmIndex) {
    /*-------- Arm2 (Silicon) --------*/
    const Double_t pitch = Arm2Params::kPosPitch;
    if (towerindex == 0)  // small tower
      ch_pos.SetXYZ((px + 34.90) / pitch, (60.08 - py) / pitch, kDetectorZpos);
    else  // large tower
      ch_pos.SetXYZ((px + 1.10) / pitch, (33.28 - py) / pitch, kDetectorZpos);
  } else {
    Utils::Printf(Utils::kPrintError, "CalorimeterToChannel: invalid armindex\n");
    exit(EXIT_FAILURE);
  }

  /* Apply shift (not tilt) correction */
  if (lx >= 0) {
    ch_pos.SetX(ch_pos.X() + fAlignmentCorrection[armindex][towerindex][lx][0]);
  }
  if (ly >= 0) {
    ch_pos.SetY(ch_pos.Y() + fAlignmentCorrection[armindex][towerindex][ly][1]);
  }

  return ch_pos;
}

////////////////////////////////////////
/// \brief Return crossing angle
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \return
Double_t CoordinateTransformation::GetCrossingAngle(Int_t armindex) { return fCrossingAngle[armindex]; };

////////////////////////////////////////
/// \brief Compute the y crossing angle offset wrt to LHC coordinates at a given z
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param z [in] position where to compute the offset
/// \return the position in the "Collision" coordinate
Double_t CoordinateTransformation::GetCrossingAngleOffset(Int_t armindex, Double_t z) {
  return z * fTanCrossAngle[armindex];
}

/***********************************************************
 ********************** NEW FUNCTIONS **********************
 ***********************************************************/

////////////////////////////////////////
/// \brief Subtract beam centre
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position
/// \return
TVector3 CoordinateTransformation::SubBeamCentrePosition(Int_t armindex, TVector3 pos) {
  return pos - vBeamCentrePosition[armindex];
}

////////////////////////////////////////
/// \brief Add beam centre
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position
/// \return
TVector3 CoordinateTransformation::AddBeamCentrePosition(Int_t armindex, TVector3 pos) {
  return pos + vBeamCentrePosition[armindex];
}

////////////////////////////////////////
/// \brief Subtract MC beam centre offset
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position
/// \return
TVector3 CoordinateTransformation::SubCrossAngleOffset(Int_t armindex, TVector3 pos) {
  Double_t zed = pos.Z();
  TVector3 off(0., GetCrossingAngleOffset(armindex, zed), .0);
  return pos - off;
}

////////////////////////////////////////
/// \brief Add MC beam centre offset
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position
/// \return
TVector3 CoordinateTransformation::AddCrossAngleOffset(Int_t armindex, TVector3 pos) {
  Double_t zed = pos.Z();
  TVector3 off(0., GetCrossingAngleOffset(armindex, zed), 0.);
  return pos + off;
}

////////////////////////////////////////
/// \brief Subtract MC beam centre offset
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position
/// \return
TVector3 CoordinateTransformation::SubDetectorYOffset(Int_t armindex, TVector3 pos) {
  return pos - vDetectorPosition[armindex];
}

////////////////////////////////////////
/// \brief Add MC beam centre offset
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position
/// \return
TVector3 CoordinateTransformation::AddDetectorYOffset(Int_t armindex, TVector3 pos) {
  return pos + vDetectorPosition[armindex];
}

////////////////////////////////////////
/// \brief Convert from "Detector" to "Collisions" (by default is applied to reco data/MC information)
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position in "Detector" coordinate
/// \return the position in the "Collision" coordinate
TVector3 CoordinateTransformation::FromDetectorToCollision(Int_t armindex, TVector3 pos) {
  /*
   * SPS geometry: "Collision" and "Detector" r.s. are the same
   */
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015)
    return pos;

  /*
   * Flat simulation with LHC geometry
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    // yCollision = yLHC - BeamCentre
    return SubBeamCentrePosition(armindex, pos);
  }

  /*
   * LHC geometry
   */
  /*
  UT::Printf(UT::kPrintInfo, "[CoordinateTransformation::FromDetectorToCollision]\n");
  pos.Print();
  SubBeamCentrePosition(armindex, pos).Print();
  SubCrossAngleOffset(armindex, SubBeamCentrePosition(armindex, pos)).Print();
  AddDetectorYOffset(armindex, SubCrossAngleOffset(armindex, SubBeamCentrePosition(armindex, pos))).Print();
  */

  // yCollision = yDetector - BeamCentre - [CrossAngle*zDetector - yOffset]
  return AddDetectorYOffset(armindex, SubCrossAngleOffset(armindex, SubBeamCentrePosition(armindex, pos)));
}

////////////////////////////////////////
/// \brief Convert from "Collisions" to "Detector" (by default is applied to reco data/MC information)
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position in "Collision" coordinate
/// \return the position in the "Detector" coordinate
TVector3 CoordinateTransformation::FromCollisionToDetector(Int_t armindex, TVector3 pos) {
  /*
   * SPS geometry: "Collision" and "Detector" r.s. are the same
   */
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015)
    return pos;

  /*
   * Flat simulation with LHC geometry
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    // yCollision = yLHC + BeamCentre
    return AddBeamCentrePosition(armindex, pos);
  }

  /*
   * LHC geometry
   */
  // yDetector = yCollision + BeamCentre + [CrossAngle*zDetector - yOffset]
  return SubDetectorYOffset(armindex, AddCrossAngleOffset(armindex, AddBeamCentrePosition(armindex, pos)));
}

////////////////////////////////////////
/// \brief Convert from "LHC" to "Collisions" (by default is applied to true MC information)
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position in "LHC" coordinate
/// \return the position in the "Collisions" coordinate
TVector3 CoordinateTransformation::FromLHCToCollision(Int_t armindex, TVector3 pos) {
  /*
   * Flat simulation with LHC geometry
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    // yCollision = yLHC - BeamCentre - yOffset
    return SubDetectorYOffset(armindex, SubBeamCentrePosition(armindex, pos));
  }

  /*
   * LHC geometry
   */
  // yCollision = yLHC - BeamCentre - CrossAngle*zDetector
  return SubCrossAngleOffset(armindex, SubBeamCentrePosition(armindex, pos));
}

////////////////////////////////////////
/// \brief Convert from "Collisions" to "LHC" (by default is applied to true MC information)
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos [in] position in "Collisions" coordinate
/// \return the position in the "LHC" coordinate
TVector3 CoordinateTransformation::FromCollisionToLHC(Int_t armindex, TVector3 pos) {
  /*
   * Flat simulation with LHC geometry
   */
  if (Arm1AnPars::kIsFlat || Arm2AnPars::kIsFlat) {
    // yCollision = yLHC + BeamCentre + yOffset
    return AddDetectorYOffset(armindex, AddBeamCentrePosition(armindex, pos));
  }

  /*
   * LHC geometry
   */
  // yCollision = yLHC + BeamCentre + CrossAngle*zDetector
  return AddCrossAngleOffset(armindex, AddBeamCentrePosition(armindex, pos));
}

////////////////////////////////////////
/// \brief Convert from "Detector" to "SPS (by default is applied to true MC information)
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param x [in] in "Detector" coordinate
/// \param y [in] in "Detector" coordinate
/// \param z [in] in "Detector" coordinate
/// \return the position in the "SPS" coordinate
TVector3 CoordinateTransformation::FromDetectorToSPS(Int_t armindex, Double_t x, Double_t y, Double_t z) {
  TVector2 SPSCoo(x, y);

  TVector2 offCoo;
  if (armindex == Arm1Params::kArmIndex) {
    offCoo.Set(0., 0.);  // TODO [TrackProjection]:Arm1 - Which is the correct origin?
  } else if (armindex == Arm2Params::kArmIndex) {
    if (fRotatedArm) {
      offCoo.Set(48., 80.);
    } else {
      offCoo.Set(50., 80.);
    }
  } else {
    // Printf(Utils::kPrintError, "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex");
    cerr << "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex:" << armindex << endl;
  }

  TVector2 detCoo = SPSCoo + offCoo;
  if (fRotatedArm) {
    detCoo.SetX(-detCoo.X());
  }

  return TVector3(detCoo.X(), detCoo.Y(), z);
}

////////////////////////////////////////
/// \brief Convert from "Detector" to "SPS" (by default is applied to true MC information)
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param pos [in] position in "Detector" coordinate
/// \return the position in the "SPS" coordinate
TVector3 CoordinateTransformation::FromDetectorToSPS(Int_t armindex, TVector3 pos) {
  return FromDetectorToSPS(armindex, pos.X(), pos.Y(), pos.Z());
}

////////////////////////////////////////
/// \brief Convert from "SPS" to "Detector" (by default is applied to true MC information)
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param x [in] in "SPS" coordinate
/// \param y [in] in "SPS" coordinate
/// \param z [in] in "SPS" coordinate
/// \return the position in the "Detector" coordinate
TVector3 CoordinateTransformation::FromSPSToDetector(Int_t armindex, Double_t x, Double_t y, Double_t z) {
  TVector2 SPSCoo(x, y);

  TVector2 offCoo;
  if (armindex == Arm1Params::kArmIndex) {
    offCoo.Set(0., 0.);  // TODO [TrackProjection]:Arm1 - Which is the correct origin?
  } else if (armindex == Arm2Params::kArmIndex) {
    if (fRotatedArm) {
      offCoo.Set(48., 80.);
    } else {
      offCoo.Set(50., 80.);
    }
  } else {
    // Printf(Utils::kPrintError, "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex");
    cerr << "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex:" << armindex << endl;
  }

  TVector2 detCoo = SPSCoo - offCoo;
  if (fRotatedArm) {
    detCoo.SetX(-detCoo.X());
  }

  return TVector3(detCoo.X(), detCoo.Y(), z);
}

////////////////////////////////////////
/// \brief Convert from "SPS" to "Detector" (by default is applied to true MC information)
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param pos [in] position in "SPS" coordinate
/// \return the position in the "Detector" coordinate
TVector3 CoordinateTransformation::FromSPSToDetector(Int_t armindex, TVector3 pos) {
  return FromSPSToDetector(armindex, pos.X(), pos.Y(), pos.Z());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Detector
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param x [in]
/// \param y [in]
/// \param z [in]
/// \return
TVector3 CoordinateTransformation::FromCalorimeterToDetector(Int_t armindex, Int_t towerindex, Double_t x, Double_t y,
                                                             Double_t z) {
  // alias of sqrt(2.0);
  const Double_t sq2 = TMath::Sqrt(2.0);

  TVector2 pos_cal(x, y);
  TVector2 offset;

  if (armindex == Arm1Params::kArmIndex) {
    // For New Arm1 detector

    if (towerindex == 0)
      offset.Set(0., -1. * Arm1Params::kTowerSize[0] / sq2);
    else
      offset.Set(0. + fArmTowerGap[0][0], Arm1Params::kTowerSize[0] / sq2 + fArmTowerGap[0][1]);

    pos_cal = pos_cal.Rotate(TMath::Pi() / 4.);  // 45 degrees rotation
    pos_cal += offset;

  } else if (armindex == Arm2Params::kArmIndex) {
    // For New Arm2 detector

    if (towerindex == 0)  // Small Tower
      offset.Set(-8., -1. * Arm2Params::kTowerSize[0] / 2.);
    else  // Large Tower
      offset.Set(-8. - fArmTowerGap[1][0] - Arm2Params::kTowerSize[1],
                 Arm2Params::kTowerSize[0] / 2. + fArmTowerGap[1][1]);

    pos_cal += offset;

  } else {
    cerr << "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex" << endl;
  }

  /*
          UT::Printf(UT::kPrintInfo, "[CoordinateTransformation::FromCalorimeterToDetector]\n");
          UT::Printf(UT::kPrintInfo, "%d : (%f, %f, %f)\n", towerindex, x, y, z);
          pos_cal.Print();
  */

  return TVector3(pos_cal.X(), pos_cal.Y(), z);
}

/////////////////////////////////////////////
/// \brief  Coordinate Transformation from Detector to Calorimeter
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param pos [in]
/// \return
TVector3 CoordinateTransformation::FromCalorimeterToDetector(Int_t armindex, Int_t towerindex, TVector3 pos) {
  return FromCalorimeterToDetector(armindex, towerindex, pos.X(), pos.Y(), pos.Z());
}

////////////////////////////////////////
/// \brief alias
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param x [in]
/// \param y [in]
/// \param z [in]
/// \return
TVector3 CoordinateTransformation::FromDetectorToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y,
                                                             Double_t z) {
  // alias of sqrt(2.0);
  const Double_t sq2 = TMath::Sqrt(2.0);

  TVector2 pos_cal(x, y);
  TVector2 offset;

  if (armindex == Arm1Params::kArmIndex) {
    // For New Arm1 detector

    if (towerindex == 0)
      offset.Set(0., -1. * Arm1Params::kTowerSize[0] / sq2);
    else
      offset.Set(0. + fArmTowerGap[0][0], Arm1Params::kTowerSize[0] / sq2 + fArmTowerGap[0][1]);

    pos_cal -= offset;
    pos_cal = pos_cal.Rotate(-1. * TMath::Pi() / 4.);  // 45 degrees rotation

  } else if (armindex == Arm2Params::kArmIndex) {
    // For New Arm2 detector

    if (towerindex == 0)  // Small Tower
      offset.Set(-8., -1. * Arm2Params::kTowerSize[0] / 2.);
    else  // Large Tower
      offset.Set(-8. - fArmTowerGap[1][0] - Arm2Params::kTowerSize[1],
                 Arm2Params::kTowerSize[0] / 2. + fArmTowerGap[1][1]);

    pos_cal -= offset;

  } else {
    // Printf(Utils::kPrintError, "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex");
    cerr << "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex:" << armindex << endl;
  }

  /*
          UT::Printf(UT::kPrintInfo, "[CoordinateTransformation::FromDetectorToCalorimeter]\n");
          UT::Printf(UT::kPrintInfo, "%d : (%f, %f, %f)\n", towerindex, x, y, z);
          pos_cal.Print();
  */

  return TVector3(pos_cal.X(), pos_cal.Y(), x);
}

///////////////////////////////////////////
/// \brief Coordinate Transformation from Detector to Calorimeter
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param pos [in]
/// \return
TVector3 CoordinateTransformation::FromDetectorToCalorimeter(Int_t armindex, Int_t towerindex, TVector3 pos) {
  TVector3 pos_cal = FromDetectorToCalorimeter(armindex, towerindex, pos.X(), pos.Y(), pos.Z());
  return pos_cal;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Detector to be used on Level3 reconstructed information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param x position [in]
/// \param y position [in]
/// \param x maxlayer [in]
/// \param y maxlayer [in]
/// \return
TVector3 CoordinateTransformation::GetRecoDetectorCoordinates(Int_t armindex, Int_t towerindex, Double_t xpos,
                                                              Double_t ypos, Int_t xlay, Int_t ylay) {
  /*
   * SPS geometry
   */
  //"Collision" and "Detector" r.s. are the same
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015)
    return FromCalorimeterToDetector(armindex, towerindex, xpos, ypos, 0.);

  /*
   * LHC geometry
   */
  Double_t zpos = kDetectorZpos;

  if (armindex == Arm1Params::kArmIndex) {
    zpos += 0.5 * (Arm1AnPars::kPosLayerDepth[xlay][0] + Arm1AnPars::kPosLayerDepth[ylay][1]);
  } else {
    zpos += 0.5 * (Arm2AnPars::kPosLayerDepth[xlay][0] + Arm2AnPars::kPosLayerDepth[ylay][1]);
  }

  return FromCalorimeterToDetector(armindex, towerindex, xpos, ypos, zpos);
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Detector to be used on Level3 reconstructed information
/// \param pos (reconstructed position) [in]
/// \param lay (maximum position layer) [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::GetRecoDetectorCoordinates(Int_t armindex, Int_t towerindex, TVector3 pos,
                                                              TVector3 lay) {
  return GetRecoDetectorCoordinates(armindex, towerindex, pos.X(), pos.Y(), lay.X(), lay.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Collision to be used on Level3 reconstructed information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param x position [in]
/// \param y position [in]
/// \param x maxlayer [in]
/// \param y maxlayer [in]
/// \return
TVector3 CoordinateTransformation::GetRecoCollisionCoordinates(Int_t armindex, Int_t towerindex, Double_t xpos,
                                                               Double_t ypos, Int_t xlay, Int_t ylay) {
  /*
   * SPS geometry
   */
  //"Collision" and "Detector" r.s. are the same
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015)
    return GetRecoDetectorCoordinates(armindex, towerindex, xpos, ypos, xlay, ylay);

  /*
   * LHC geometry
   */
  // Detector coordinates
  TVector3 detCooAtMax = GetRecoDetectorCoordinates(armindex, towerindex, xpos, ypos, xlay, ylay);
  // Collision coordinates
  TVector3 colCooAtMax = FromDetectorToCollision(armindex, detCooAtMax);
  // Extrapolate to fixed Z
  TVector3 posCooAtLHCf = ExtrapolateToZ(colCooAtMax, kDetectorZpos);

  /*
  UT::Printf(UT::kPrintInfo, "[CoordinateTransformation::GetRecoCollisionCoordinates]\n");
  UT::Printf(UT::kPrintInfo, "(%f, %f) at (%d, %d)\n", xpos, ypos, xlay, ylay);
  detCooAtMax.Print();
  colCooAtMax.Print();
  posCooAtLHCf.Print();
  */

  return posCooAtLHCf;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Collision to be used on Level3 reconstructed information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param pos (reconstructed position) [in]
/// \param lay (maximum position layer) [in]
/// \return
TVector3 CoordinateTransformation::GetRecoCollisionCoordinates(Int_t armindex, Int_t towerindex, TVector3 pos,
                                                               TVector3 lay) {
  return GetRecoCollisionCoordinates(armindex, towerindex, pos.X(), pos.Y(), lay.X(), lay.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to LHC to be used on Level3 reconstructed information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param x position [in]
/// \param y position [in]
/// \param x maxlayer [in]
/// \param y maxlayer [in]
/// \return
TVector3 CoordinateTransformation::GetRecoLHCCoordinates(Int_t armindex, Int_t towerindex, Double_t xpos, Double_t ypos,
                                                         Int_t xlay, Int_t ylay) {
  // Collision coordinates
  TVector3 colCoo = GetRecoCollisionCoordinates(armindex, towerindex, xpos, ypos, xlay, ylay);
  // Extrapolate to fixed Z
  TVector3 posCoo = ExtrapolateToZ(colCoo, kEnd2EndInZpos);
  // LHC coordinates
  TVector3 lhcCoo = FromCollisionToLHC(armindex, posCoo);

  return posCoo;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to LHC to be used on Level3 reconstructed information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \param pos (reconstructed position) [in]
/// \param lay (maximum position layer) [in]
/// \return
TVector3 CoordinateTransformation::GetRecoLHCCoordinates(Int_t armindex, Int_t towerindex, TVector3 pos, TVector3 lay) {
  return GetRecoLHCCoordinates(armindex, towerindex, pos.X(), pos.Y(), lay.X(), lay.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Collision to be used on McEvent true information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::GetTrueCollisionCoordinates(Int_t armindex, Double_t x, Double_t y) {
  /*
   * SPS geometry
   */
  if (Arm1Params::fOperation == kSPS2022 || Arm1Params::fOperation == kSPS2021 || Arm1Params::fOperation == kSPS2015 ||
      Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2015)
    return GetSPSTrueDetectorCoordinates(armindex, x, y);

  /*
   * LHC geometry
   */
  // Get the true position information in LHC coordinates at TAN
  TVector3 LhcCooAtTAN = TVector3(x, y, kEnd2EndInZpos);
  // Correct for crossing angle offset to get Collision coordinates at TAN
  TVector3 ColCooAtTAN = FromLHCToCollision(armindex, LhcCooAtTAN);
  // Extrapolate to fixed Z = kDetectorZpos (at LHCf entrance)
  TVector3 ColCooAtLHCf = ExtrapolateToZ(ColCooAtTAN, kDetectorZpos);

  /*
          UT::Printf(UT::kPrintInfo, "[CoordinateTransformation::GetTrueCollisionCoordinates]\n");
          LhcCooAtTAN.Print();
          ColCooAtTAN.Print();
          ColCooAtLHCf.Print();
  */

  return ColCooAtLHCf;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Collision to be used McEvent true information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos (reconstructed position) [in]
/// \return
TVector3 CoordinateTransformation::GetTrueCollisionCoordinates(Int_t armindex, TVector3 pos) {
  return GetTrueCollisionCoordinates(armindex, pos.X(), pos.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Detector to be used on McEvent true information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::GetTrueDetectorCoordinates(Int_t armindex, Double_t x, Double_t y) {
  return FromCollisionToDetector(armindex, GetTrueCollisionCoordinates(armindex, x, y));
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Detector to be used McEvent true information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos (reconstructed position) [in]
/// \return
TVector3 CoordinateTransformation::GetTrueDetectorCoordinates(Int_t armindex, TVector3 pos) {
  return GetTrueDetectorCoordinates(armindex, pos.X(), pos.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Calorimeter to be used on McEvent true information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::GetTrueCalorimeterCoordinates(Int_t armindex, Int_t towerindex, Double_t x,
                                                                 Double_t y) {
  return FromDetectorToCalorimeter(armindex, towerindex, GetTrueDetectorCoordinates(armindex, x, y));
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Calorimeter to be used McEvent true information
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param pos (reconstructed position) [in]
/// \return
TVector3 CoordinateTransformation::GetTrueCalorimeterCoordinates(Int_t armindex, Int_t towerindex, TVector3 pos) {
  return GetTrueCalorimeterCoordinates(armindex, towerindex, pos.X(), pos.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Detector LHC to be used on Level3 reconstructed information
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param x [in] in "Detector==Collision" coordinate
/// \param y [in] in "Detector==Collision" coordinate
/// \return the position in the "SPS" coordinate
TVector3 CoordinateTransformation::GetSPSRecoSPSCoordinates(Int_t armindex, Double_t x, Double_t y) {
  // Reco position in detector coordinates
  TVector3 posDetector(x, y, 0.);
  // Correct for detector position to get LHC coordinates
  TVector3 posSPS = FromDetectorToSPS(armindex, posDetector);

  return posSPS;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Detector to LHC to be used on Level3 reconstructed information
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param pos [in] in "Detector==Collision" coordinate
/// \return the position in the "SPS" coordinate
TVector3 CoordinateTransformation::GetSPSRecoSPSCoordinates(Int_t armindex, TVector3 pos) {
  return GetSPSRecoSPSCoordinates(armindex, pos.X(), pos.Y());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Detector to be used on McEvent true information
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param x [in] in "SPS" coordinate
/// \param y [in] in "SPS" coordinate
/// \return the position in the "Detector==Collision" coordinate
TVector3 CoordinateTransformation::GetSPSTrueDetectorCoordinates(Int_t armindex, Double_t x, Double_t y) {
  // True position in LHC coordinates
  TVector3 posSPS(x, y, 0.);
  // Correct for detector position to get Detector coordinates
  TVector3 posDetector = FromSPSToDetector(armindex, posSPS);

  /*
  UT::Printf(UT::kPrintInfo, "[CoordinateTransformation::GetSPSTrueDetectorCoordinates]\n");
  posSPS.Print();
  posDetector.Print();
  */

  return posDetector;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from LHC to Detector to be used on McEvent true information
/// \param armindex [in] specify as Arm*Params::kArmIndex
/// \param pos [in] in "SPS" coordinate
/// \return the position in the "Detector==Collision" coordinate
TVector3 CoordinateTransformation::GetSPSTrueDetectorCoordinates(Int_t armindex, TVector3 pos) {
  return GetSPSTrueDetectorCoordinates(armindex, pos.X(), pos.Y());
}

/***********************************************************
 ********************** OLD FUNCTIONS **********************
 ***********************************************************/

////////////////////////////////////////
/// \brief Coordinate Transformation of 3D-vector from "Lhc" to "Detector"
/// \param pos [in] position
/// \return the position in the "Detector" coordinate
TVector3 CoordinateTransformation::LhcToDetector(Int_t armindex, TVector3 pos) {
  return sRotationLhcToDetector[armindex] * pos;
}

////////////////////////////////////////
/// \brief Coordinate Transformation of 3D-vector from "Detector" to "Collisions"
/// \param pos [in] position in "Detector" coordinate
/// \return the position in the "Collision" coordinate
TVector3 CoordinateTransformation::DetectorToCollision(Int_t armindex, TVector3 pos) {
  if (fUseRotation) {
    return sRotationDetectorToCollision[armindex] * pos;
  } else {
    return pos - vBeamCentrePosition[armindex];
  }
}

////////////////////////////////////////
/// \brief Coordinate Transformation of 3D-vector from "Lhc" to "Collisions"
/// \param pos [in] position in "Lhc" coordinate
/// \return the position in the "Collision" coordinate
TVector3 CoordinateTransformation::LhcToCollision(Int_t armindex, TVector3 pos) {
  return DetectorToCollision(armindex, LhcToDetector(armindex, pos));
}

////////////////////////////////////////
/// \brief Coordinate Transformation of 3D-vector from "Detector" to "Lhc"
/// \param pos [in]
/// \return
TVector3 CoordinateTransformation::DetectorToLhc(Int_t armindex, TVector3 pos) {
  return sRotationLhcToDetector[armindex].Inverse() * pos;
}

////////////////////////////////////////
/// \brief Coordinate Transformation of 3D-vector from "Collisions" to "Detector"
/// \param pos_col [in]
/// \return
TVector3 CoordinateTransformation::CollisionToDetector(Int_t armindex, TVector3 pos_col) {
  if (fUseRotation) {
    return sRotationDetectorToCollision[armindex].Inverse() * pos_col;
  } else {
    return pos_col + vBeamCentrePosition[armindex];
  }
}

////////////////////////////////////////
/// \brief Coordinate Transformation of 3D-vector from "Collisions" to "Lhc"
/// \param pos_col [in]
/// \return
TVector3 CoordinateTransformation::CollisionToLhc(Int_t armindex, TVector3 pos_col) {
  return DetectorToLhc(armindex, CollisionToDetector(armindex, pos_col));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::LhcToDetector(Int_t armindex, Double_t x, Double_t y) {
  return LhcToDetector(armindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::DetectorToCollision(Int_t armindex, Double_t x, Double_t y) {
  return DetectorToCollision(armindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::LhcToCollision(Int_t armindex, Double_t x, Double_t y) {
  return LhcToCollision(armindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::DetectorToLhc(Int_t armindex, Double_t x, Double_t y) {
  return DetectorToLhc(armindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::CollisionToDetector(Int_t armindex, Double_t x, Double_t y) {
  return CollisionToDetector(armindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \return
TVector3 CoordinateTransformation::CollisionToLhc(Int_t armindex, Double_t x, Double_t y) {
  return CollisionToLhc(armindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias for TLorentzVector (Momentum vector)
/// \param v [in]
/// \return
TLorentzVector CoordinateTransformation::LhcToDetector(Int_t armindex, TLorentzVector v) {
  return TLorentzVector(LhcToDetector(armindex, v.Vect()), v.T());
}

////////////////////////////////////////
/// \brief alias for TLorentzVector (Momentum vector)
/// \param v [in]
/// \return
TLorentzVector CoordinateTransformation::DetectorToCollision(Int_t armindex, TLorentzVector v) {
  return TLorentzVector(DetectorToCollision(armindex, v.Vect()), v.T());
}

////////////////////////////////////////
/// \brief alias for TLorentzVector (Momentum vector)
/// \param v [in]
/// \return
TLorentzVector CoordinateTransformation::LhcToCollision(Int_t armindex, TLorentzVector v) {
  return TLorentzVector(LhcToCollision(armindex, v.Vect()), v.T());
}

////////////////////////////////////////
/// \brief alias for TLorentzVector (Momentum vector)
/// \param v [in]
/// \return
TLorentzVector CoordinateTransformation::DetectorToLhc(Int_t armindex, TLorentzVector v) {
  return TLorentzVector(DetectorToLhc(armindex, v.Vect()), v.T());
}

////////////////////////////////////////
/// \brief alias for TLorentzVector (Momentum vector)
/// \param v [in]
/// \return
TLorentzVector CoordinateTransformation::CollisionToDetector(Int_t armindex, TLorentzVector v) {
  return TLorentzVector(CollisionToDetector(armindex, v.Vect()), v.T());
}

////////////////////////////////////////
/// \brief alias for TLorentzVector (Momentum vector)
/// \param v [in]
/// \return
TLorentzVector CoordinateTransformation::CollisionToLhc(Int_t armindex, TLorentzVector v) {
  return TLorentzVector(CollisionToLhc(armindex, v.Vect()), v.T());
}

//----------------------------------------------------------------------------------------------------------
// Conversion from Calorimeter coordinate to Global coodinate

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Detector
/// \param x [in]
/// \param y [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CalorimeterToDetector(Int_t armindex, Int_t towerindex, Double_t x, Double_t y) {
  // alias of sqrt(2.0);
  const Double_t sq2 = TMath::Sqrt(2.0);

  TVector2 pos_cal(x, y);
  TVector2 offset;

  if (armindex == Arm1Params::kArmIndex) {
    // For New Arm1 detector

    if (towerindex == 0)
      offset.Set(0., -1. * Arm1Params::kTowerSize[0] / sq2);
    else
      offset.Set(0. + fArmTowerGap[0][0], Arm1Params::kTowerSize[0] / sq2 + fArmTowerGap[0][1]);

    pos_cal = pos_cal.Rotate(TMath::Pi() / 4.);  // 45 degrees rotation
    pos_cal += offset;

  } else if (armindex == Arm2Params::kArmIndex) {
    // For New Arm2 detector

    if (towerindex == 0)  // Small Tower
      offset.Set(-8., -1. * Arm2Params::kTowerSize[0] / 2.);
    else  // Large Tower
      offset.Set(-8. - fArmTowerGap[1][0] - Arm2Params::kTowerSize[1],
                 Arm2Params::kTowerSize[0] / 2. + fArmTowerGap[1][1]);

    pos_cal += offset;

  } else {
    cerr << "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex" << endl;
  }

  return TVector3(pos_cal.X(), pos_cal.Y(), kDetectorZpos);
}

///////////////////////////////////////////
/// \brief Coordinate Transformation from Detector to Calorimeter
/// \param pos [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::DetectorToCalorimeter(Int_t armindex, Int_t towerindex, TVector3 pos) {
  // alias of sqrt(2.0);
  const Double_t sq2 = TMath::Sqrt(2.0);

  TVector2 pos_cal(pos.X(), pos.Y());
  TVector2 offset;

  if (armindex == Arm1Params::kArmIndex) {
    // For New Arm1 detector

    if (towerindex == 0)
      offset.Set(0., -1. * Arm1Params::kTowerSize[0] / sq2);
    else
      offset.Set(0. + fArmTowerGap[0][0], Arm1Params::kTowerSize[0] / sq2 + fArmTowerGap[0][1]);

    pos_cal -= offset;
    pos_cal = pos_cal.Rotate(-1. * TMath::Pi() / 4.);  // 45 degrees rotation

  } else if (armindex == Arm2Params::kArmIndex) {
    // For New Arm2 detector

    if (towerindex == 0)  // Small Tower
      offset.Set(-8., -1. * Arm2Params::kTowerSize[0] / 2.);
    else  // Large Tower
      offset.Set(-8. - fArmTowerGap[1][0] - Arm2Params::kTowerSize[1],
                 Arm2Params::kTowerSize[0] / 2. + fArmTowerGap[1][1]);

    pos_cal -= offset;

  } else {
    // Printf(Utils::kPrintError, "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex");
    cerr << "[CoordinateTransformation::CalorimeterToDetector] Error: Unknown armindex:" << armindex << endl;
  }

  return TVector3(pos_cal.X(), pos_cal.Y(), pos.Z());
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Lhc
/// \param x [in]
/// \param y [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CalorimeterToLhc(Int_t armindex, Int_t towerindex, Double_t x, Double_t y) {
  return DetectorToLhc(armindex, CalorimeterToDetector(armindex, towerindex, x, y));
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Collision
/// \param x [in]
/// \param y [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CalorimeterToCollision(Int_t armindex, Int_t towerindex, Double_t x, Double_t y) {
  return DetectorToCollision(armindex, CalorimeterToDetector(armindex, towerindex, x, y));
}

/////////////////////////////////////////////
/// \brief  Coordinate Transformation from Detector to Calorimeter
/// \param pos [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CalorimeterToDetector(Int_t armindex, Int_t towerindex, TVector3 pos) {
  TVector3 pos_det = CalorimeterToDetector(armindex, towerindex, pos.X(), pos.Y());
  pos_det.SetZ(pos.Z());
  return pos_det;
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Lhc
/// \param pos [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CalorimeterToLhc(Int_t armindex, Int_t towerindex, TVector3 pos) {
  TVector3 pos_det = CalorimeterToDetector(armindex, towerindex, pos.X(), pos.Y());
  pos_det.SetZ(pos.Z());
  return DetectorToLhc(armindex, pos_det);
}

/////////////////////////////////////////////
/// \brief Coordinate Transformation from Calorimeter to Collision
/// \param pos [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CalorimeterToCollision(Int_t armindex, Int_t towerindex, TVector3 pos) {
  TVector3 pos_det = CalorimeterToDetector(armindex, towerindex, pos.X(), pos.Y());
  pos_det.SetZ(pos.Z());
  return DetectorToCollision(armindex, pos_det);
}

///////////////////////////////////////////
/// \brief Coordinate Transformation from Lhc to Calorimeter
/// \param pos [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::LhcToCalorimeter(Int_t armindex, Int_t towerindex, TVector3 pos) {
  return DetectorToCalorimeter(armindex, towerindex, LhcToDetector(armindex, pos));
}

///////////////////////////////////////////
/// \brief Coordinate Transformation from Collision to Calorimeter
/// \param pos [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CollisionToCalorimeter(Int_t armindex, Int_t towerindex, TVector3 pos) {
  return DetectorToCalorimeter(armindex, towerindex, CollisionToDetector(armindex, pos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::DetectorToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y) {
  return DetectorToCalorimeter(armindex, towerindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::LhcToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y) {
  return LhcToCalorimeter(armindex, towerindex, TVector3(x, y, kDetectorZpos));
}

////////////////////////////////////////
/// \brief alias
/// \param x [in]
/// \param y [in]
/// \param armindex [in]    specify as Arm*Params::kArmIndex
/// \param towerindex [in]  0: TS, 1:TL
/// \return
TVector3 CoordinateTransformation::CollisionToCalorimeter(Int_t armindex, Int_t towerindex, Double_t x, Double_t y) {
  return CollisionToCalorimeter(armindex, towerindex, TVector3(x, y, kDetectorZpos));
}

/* Allocate for calling constructor*/
class CoordinateTransformation;
