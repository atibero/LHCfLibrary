#include "Arm2CalPars.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm2CalPars);
#endif

typedef Utils UT;

TString Arm2CalPars::fFillNumber = "";
TString Arm2CalPars::fSubfillNumber = "";

Double_t Arm2CalPars::kCalSat = 3700;
Double_t Arm2CalPars::kCalBaseHV = 600.;
Double_t Arm2CalPars::kCalAttFactor0 = 0.9020;
Double_t Arm2CalPars::kCalAttFactor1 = 0.8869;
Double_t Arm2CalPars::kCalAttFactor2 = 0.9078;
Double_t Arm2CalPars::kPosBaseHV = 1.;  // dummy
Double_t Arm2CalPars::kPosAttFactor = 1.E6;   //  dummy

Int_t Arm2CalPars::kMaxNrun = 1000;
Int_t Arm2CalPars::kFirstRun = -1;
Int_t Arm2CalPars::kLastRun = -1;

TString Arm2CalPars::fMidasTree = "Trigger2";
TString Arm2CalPars::fMidasMiniTree = "Trigger2m";
TString Arm2CalPars::fPosDetKey = "ARM2";
TString Arm2CalPars::fPosDetIdFile = "a2_pos_table.dat";  // dummy
TString Arm2CalPars::fCalPedShiftFile = "a2_cal_ped_shift_fill" + fFillNumber + ".dat";
TString Arm2CalPars::fCalDelShiftFile = "a2_cal_del_shift_fill" + fFillNumber + ".dat";
TString Arm2CalPars::fCalRangeFactorFile = "a2_cal_range_factor.dat";
TString Arm2CalPars::fCalHVFile = "a2_cal_hv.dat";
TString Arm2CalPars::fCalGainFactorFile = "a2_cal_gain_factor.dat";
TString Arm2CalPars::fCalConvFactorFile = "a2_cal_conv_factor.dat";
TString Arm2CalPars::fPosHVFile = "a2_pos_hv.dat";                   // dummy
TString Arm2CalPars::fPosGainFactorFile = "a2_pos_gain_factor.dat";  // dummy
TString Arm2CalPars::fPosConvFactorFile = "a2_pos_conv_factor.dat";
TString Arm2CalPars::fCalPedMeanFile = "a2_cal_ped_mean_fill" + fFillNumber + ".dat";
TString Arm2CalPars::fCalPedRealFile = "a2_cal_ped_fill" + fFillNumber + ".root";
TString Arm2CalPars::fPosPedMeanFile = "a2_pos_ped_mean_fill" + fFillNumber + ".dat";
TString Arm2CalPars::fPosPedRealFile = "a2_pos_ped_fill" + fFillNumber + ".root";
TString Arm2CalPars::fPosCrossTalkFile = "";
TString Arm2CalPars::fPosSilCorrFile = "a2_pos_silcorr.root";

Int_t Arm2CalPars::nPedSubIters = 3;
Int_t Arm2CalPars::nPedSubSigma = 5;

void Arm2CalPars::SetFill(Int_t fill, Int_t subfill) {
  fill = TMath::Abs(fill);
  subfill = TMath::Abs(subfill);

  UT::Printf(UT::kPrintInfo, "Arm2CalPars: setting parameters for fill %d\n", fill);

  /* This is the most generic case : To be used only for preliminary study */
  if (fill == 0) {
    kFirstRun = -1;
    kLastRun = -1;
    fFillNumber = Form("_dummy");
  }
  /* SPS */
  else if (fOperation == kSPS2022 || fOperation == kSPS2021 || fOperation == kSPS2015) {
    kFirstRun = -1;
    kLastRun = -1;
    fFillNumber = Form("_dummy");
  }
  /* LHC 2025 */
  else if (fOperation == kLHC2025) {
    kFirstRun = -1;
    kLastRun = -1;
    fFillNumber = Form("_dummy");
    //      if (fill == XXXX) {
    //        if (subfill == 1) {
    //          kFirstRun = YYYYY;
    //          kLastRun = ZZZZZ;
    //        } else {
    //          UT::Printf(UT::kPrintError, "Arm2CalPars::SetFill: unknown subfill, exit...\n");
    //          exit(EXIT_FAILURE);
    //        }
    //        fFillNumber = Form("%d_%d_%d", fill, kFirstRun, kLastRun);
    //      } else {
    //        UT::Printf(UT::kPrintError, "Arm2CalPars::SetFill: unknown fill, exit...\n");
    //        exit(EXIT_FAILURE);
    //      }
  }
  /* LHC 2022 */
  else if (fOperation == kLHC2022) {
    // Fill 8178 - Divided in four subgroups
    if (fill == 8178) {
      if (subfill == 1) {
        kFirstRun = 80262;
        kLastRun = 80346;
      } else if (subfill == 2) {
        kFirstRun = 80351;
        kLastRun = 80372;
      } else if (subfill == 3) {
        kFirstRun = 80377;
        kLastRun = 80431;
      } else if (subfill == 4) {
        kFirstRun = 80434;
        kLastRun = 80520;
      } else if (subfill == 5) {
        kFirstRun = 80523;
        kLastRun = 80626;
      } else {
        UT::Printf(UT::kPrintError, "Arm2CalPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }
      fFillNumber = Form("%d_%d_%d", fill, kFirstRun, kLastRun);
    } else if (fill == 8181) {
      kFirstRun = 80635;
      kLastRun = 80646;
      fFillNumber = Form("%d", fill);
    } else {
      UT::Printf(UT::kPrintError, "Arm2CalPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  }
  /* LHC 2015 */
  else if (fOperation == kLHC2015) {
    /* Fill 3855 (detector in nominal position) */
    if (fill == 3855) {
      kFirstRun = 44299;
      kLastRun = 45106;
      fFillNumber = Form("%d", fill);
    }
    /* Fill 3851 (detector shifted up by 5 mm) */
    else if (fill == 3851) {
      kFirstRun = 43321;
      kLastRun = 43598;
      fFillNumber = Form("%d", fill);
    } else {
      UT::Printf(UT::kPrintError, "Arm2CalPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  } else {
    UT::Printf(UT::kPrintError, "Arm2CalPars::SetFill: unknown fill, exit...\n");
    exit(EXIT_FAILURE);
  }

  fCalPedShiftFile = "a2_cal_ped_shift_fill" + fFillNumber + ".dat";
  fCalDelShiftFile = "a2_cal_del_shift_fill" + fFillNumber + ".dat";
  fCalPedMeanFile = "a2_cal_ped_mean_fill" + fFillNumber + ".dat";
  fCalPedRealFile = "a2_cal_ped_fill" + fFillNumber + ".root";
  fPosPedMeanFile = "a2_pos_ped_mean_fill" + fFillNumber + ".dat";
  fPosPedRealFile = "a2_pos_ped_fill" + fFillNumber + ".root";

  return;
}

Arm2CalPars::ParInit::ParInit() {
  UT::Printf(UT::kPrintInfo, "Arm2CalPars: initialising parameters...");
  fflush(stdout);

  if (fOperation == kLHC2015) {
    UT::Printf(UT::kPrintInfo, " (LHC2015)");
    fflush(stdout);
  } else if (fOperation == kSPS2015) {
    UT::Printf(UT::kPrintInfo, " (SPS2015)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2021) {
    UT::Printf(UT::kPrintInfo, " (SPS2021)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kSPS2022) {
    UT::Printf(UT::kPrintInfo, " (SPS2022)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kLHC2022) {
    UT::Printf(UT::kPrintInfo, " (LHC2022)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else if (fOperation == kLHC2025) {
    UT::Printf(UT::kPrintInfo, " (LHC2025)");
    fflush(stdout);
    /* !!! TODO !!! */
  } else {
    UT::Printf(UT::kPrintError, "Arm2CalPars::Initialise: unknown operation\n");
    exit(EXIT_FAILURE);
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

Arm2CalPars::ParInit Arm2CalPars::fInitialiser;
