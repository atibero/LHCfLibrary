//
// Created by MENJO Hiroaki on 2018/12/04.
//

#include "McParticle.hh"

#include <iomanip>
#include <iostream>

using namespace std;

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(McParticle);
#endif

typedef CoordinateTransformation CT;

McParticle::McParticle() { DataClear(); }

McParticle::McParticle(const char *name, const char *title) : TNamed(name, title) { DataClear(); }

McParticle::McParticle(McParticle *d) { this->Copy(d); }

McParticle::~McParticle() { ; }

void McParticle::Clear(Option_t *option) {
  TNamed::Clear(option);
  DataClear(option);
  return;
}

void McParticle::Copy(McParticle *d) {
  this->TNamed::Copy(*d);
  DataCopy(d);
  return;
}

void McParticle::DataClear(Option_t *option) {
  fIndex = 0;
  fPdgCode = 0;
  fPosition.SetXYZ(0., 0., 0.);
  fMomentum.SetXYZT(0., 0., 0., 0.);
  fUserCode = 0;
  fStatusCode = 0;
  fParent[0] = 0;
  fParent[1] = 0;
  return;
}

void McParticle::DataCopy(McParticle *d) {
  fIndex = d->fIndex;
  fPdgCode = d->fPdgCode;
  fPosition = d->fPosition;
  fMomentum = d->fMomentum;
  fUserCode = d->fUserCode;
  fStatusCode = d->fStatusCode;
  fParent[0] = d->fParent[0];
  fParent[1] = d->fParent[1];
  return;
}

void McParticle::SetFlag() {
  isflat[0] = Arm1AnPars::kIsFlat;
  isflat[1] = Arm2AnPars::kIsFlat;
}

TVector3 McParticle::Pos_Collision(Int_t armindex) { return CT::GetTrueCollisionCoordinates(armindex, Position()); }

TVector3 McParticle::Pos_Calorimeter(Int_t armindex, Int_t towerindex) {
  return CT::GetTrueCalorimeterCoordinates(armindex, towerindex, Position());
}

TVector3 McParticle::Pos_Detector(Int_t armindex) { return CT::GetTrueDetectorCoordinates(armindex, Position()); }

Bool_t McParticle::CheckHit(Int_t armindex, Int_t towerindex, Double_t energythreshold, Double_t edgecut) {
  // Energy of the particle
  Double_t energy = this->Energy();

  Double_t calorimetersize = 0.;
  if (armindex == Arm1Params::kArmIndex)
    calorimetersize = Arm1Params::kTowerSize[towerindex];
  else if (armindex == Arm2Params::kArmIndex)
    calorimetersize = Arm2Params::kTowerSize[towerindex];
  else {
    Utils::Printf(Utils::kPrintError, "McParticle::CheckHit: unknown armindex: %d\n", armindex);
    exit(EXIT_FAILURE);
  }

  TVector3 pos = Pos_Calorimeter(armindex, towerindex);

  if (pos.X() > 0. + edgecut && pos.X() < calorimetersize - edgecut && pos.Y() > 0. + edgecut &&
      pos.Y() < calorimetersize - edgecut && energy > energythreshold) {
    return kTRUE;
  }

  return kFALSE;
}

void McParticle::Print(Option_t *option) {
  string opt = option;
  transform(opt.begin(), opt.end(), opt.begin(), ::tolower);

  Bool_t check_header = kFALSE;
  TVector3 pos[2];
  Int_t check_coordinate = 2;  // 0:Lhc,1:Detector,2:Collisions,3:Arm1,4:Arm2
  TString label;

  if (opt.find("header") != std::string::npos) {
    check_header = kTRUE;
  }
  if (opt.find("lhc") != std::string::npos) {
    check_coordinate = 0;
    pos[0] = Position();
    label = "on LHC coordinates";
  }
  if (opt.find("arm1") != std::string::npos) {
    if (opt.find("collision") != std::string::npos) {
      check_coordinate = 1;
      pos[0] = CT::GetTrueCollisionCoordinates(Arm1Params::kArmIndex, Position());
      label = "on Collision coordinates";
    }
    if (opt.find("detector") != std::string::npos) {
      check_coordinate = 2;
      pos[0] = CT::GetTrueDetectorCoordinates(Arm1Params::kArmIndex, Position());
      label = "on Detector coordinates";
    }
    if (opt.find("calorimeter") != std::string::npos) {
      check_coordinate = 3;
      pos[0] = CT::GetTrueCalorimeterCoordinates(Arm1Params::kArmIndex, 0, Position());
      pos[1] = CT::GetTrueCalorimeterCoordinates(Arm1Params::kArmIndex, 1, Position());
      label = "on Arm1 cal. coordinates";
    }
  }
  if (opt.find("arm2") != std::string::npos) {
    if (opt.find("collision") != std::string::npos) {
      check_coordinate = 4;
      pos[0] = CT::GetTrueCollisionCoordinates(Arm2Params::kArmIndex, Position());
      label = "on Collision coordinates";
    }
    if (opt.find("detector") != std::string::npos) {
      check_coordinate = 5;
      pos[0] = CT::GetTrueDetectorCoordinates(Arm2Params::kArmIndex, Position());
      label = "on Detector coordinates";
    }
    if (opt.find("calorimeter") != std::string::npos) {
      check_coordinate = 6;
      pos[0] = CT::GetTrueCalorimeterCoordinates(Arm2Params::kArmIndex, 0, Position());
      pos[1] = CT::GetTrueCalorimeterCoordinates(Arm2Params::kArmIndex, 1, Position());
      label = "on Arm2 cal. coordinates";
    }
  }

  // Print header
  if (check_header) {
    cout << "  Name  UserCode  Energy[GeV]  Position " << label << endl;
  }

  cout << "  " << setw(12) << fPdgCode << " " << setw(5) << fUserCode << " " << setw(8) << setprecision(4) << Energy()
       << " ";
  cout << "(" << setw(7) << setprecision(3) << pos[0].X() << ", " << setw(7) << setprecision(3) << pos[0].Y() << ")  ";
  if (check_coordinate >= 4) {
    cout << "(" << setw(7) << setprecision(3) << pos[1].X() << ", " << setw(7) << setprecision(3) << pos[1].Y() << ") ";
  }
  cout << endl;

  return;
}
