//
// Created by MENJO Hiroaki on 2018/12/07.
//

#include "UtilsMc.hh"

#include <Utils.hh>

using namespace nLHCf;

#include <iomanip>
#include <iostream>

using namespace std;

#if !defined(__CINT__)
templateClassImp(UtilsMc);
#endif

/**
 * @class nLHCf::UtilsMc
 * @brief Utility functions for MC simulations
 */

const Int_t UtilsMc::kNlistEpics2Pdg;
const Int_t UtilsMc::kTableEpics2Pdg[UtilsMc::kNlistEpics2Pdg][4] = {
    {1, 0, 0, 22},       // photon
    {2, 0, -1, 11},      // e-
    {2, 0, 1, -11},      // e+
    {3, 0, -1, 13},      // muon-
    {3, 0, 1, -13},      // muon-
    {4, 0, 0, 111},      // pion 0
    {4, 0, 1, 211},      // pion +
    {4, 0, -1, -211},    // pion -
    {5, 5, 0, 130},      // Kaon 0 L
    {5, 4, 0, 310},      // Kaon 0 S
    {5, 0, 0, 311},      // Kaon 0
    {5, 0, 1, 321},      // Kaon +
    {5, 0, -1, -321},    // Kaon -
    {6, 0, 1, 2212},     // proton
    {6, 0, -1, -2212},   // anti-proton
    {6, -1, 0, 2112},    // neutron
    {6, 1, 0, -2112},    // anti-neutron
    {7, -1, 0, 12},      // electron neutrino
    {7, 1, 0, -12},      // electron anti-neutrino
    {8, -1, 0, 14},      // muon neutrino
    {8, 1, 0, -14},      // muon anti-neutrino
    {16, -1, 0, 421},    // D0
    {16, 1, 0, -421},    // anti-D0
    {16, 0, 1, 411},     // D+
    {16, 0, -1, -411},   // D-
    {18, -1, 0, 3122},   // Lambda
    {18, 1, 0, -3122},   // anti-Lambda
    {19, -1, 0, 3212},   // Sigma 0
    {19, 1, 0, -3212},   // anti-Sigma 0
    {19, -1, 1, 3222},   // Sigma +
    {19, 1, 1, -3222},   // anti-Sigma +
    {19, -1, -1, 3112},  // Sigma -
    {19, 1, 1, -3112},   // anti-Sigma -
    {20, -1, 0, 3322},   // Gzai 0
    {20, 1, 0, -3322},   // anti-Gzai 0
    {20, 0, -1, 3312},   // Gzai -
    {20, 0, 1, -3312},   // anti-Gzai -
    {21, 0, 1, 4122},    // Lambda c
    {21, 0, -1, -4122},  // anti-Lambda c
    {22, 0, -1, 3334},   // Omega
    {22, 0, 1, -3334},   // anti-Omega
    {25, 0, 0, 113},     // rho
    {25, 0, 1, 213},     // rho + ??
    {25, 0, -1, -213},   // rho - ??
    {26, 0, 0, 223},     // omega meson
    {27, 0, 0, 333},     // phi meson
    {28, 0, 0, 221},     // eta
};

TDatabasePDG *UtilsMc::fDatabasePdg;

UtilsMc::UtilsMc() { fDatabasePdg = new TDatabasePDG(); }

UtilsMc::~UtilsMc() {
  if (fDatabasePdg) delete fDatabasePdg;
}

///////////////////////////////////////
/// Convert Epics code to PDG code
/// \param code  [in]
/// \param subcode [in]
/// \param charge [in]
/// \return pdgcode
Int_t UtilsMc::EpicsToPdgCode(Int_t code, Int_t subcode, Int_t charge) {
  Int_t pdgcode = 0;

  for (int i = 0; i < kNlistEpics2Pdg; ++i) {
    if (kTableEpics2Pdg[i][0] != code) continue;
    if (kTableEpics2Pdg[i][2] != charge) continue;

    // Check subcode only in case of nonzero value
    if (kTableEpics2Pdg[i][1] != 0) {
      if (kTableEpics2Pdg[i][1] == subcode) {
        pdgcode = kTableEpics2Pdg[i][3];
        break;
      }
    } else {
      pdgcode = kTableEpics2Pdg[i][3];
      break;
    }
  }

  if (pdgcode == 0) {
    // Printf(Utils::kPrintError, "Error: Unknown Epics Particle Code (%d,%d,%d)", code, subcode, charge);
    cout << "[UtilMc::EpicsToPdgCode] Error: Unknown Epics Particle Code (" << code << "," << subcode << "," << charge
         << ")" << endl;
  }

  return pdgcode;
}

//////////////////////////////////////////////////
/// Convert PDG Code to Epics Code
/// \param pdgcode [in]
/// \param code    [out]
/// \param subcode [out]
/// \param charge  [out]
void UtilsMc::PdgToEpicsCode(Int_t pdgcode, Int_t &code, Int_t &subcode, Int_t &charge) {
  code = 0;
  subcode = 0;
  charge = 0;

  for (int i = 0; i < kNlistEpics2Pdg; ++i) {
    if (pdgcode == kTableEpics2Pdg[i][3]) {
      code = kTableEpics2Pdg[i][0];
      subcode = kTableEpics2Pdg[i][1];
      charge = kTableEpics2Pdg[i][2];
      break;
    }
  }

  return;
}

/* Instance for calling constructor*/
class CoordinateTransformation;
