#include "CutFunctions.hh"

#include "Arm1AnPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "PionData.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*
 * Common variables
 */

template <typename armrec, typename arman>
Bool_t CutFunctions::BPTXCut(Level3<armrec> *lvl3) {
  UT::Printf(UT::kPrintDebugFull, "bptx cut\n");
  UT::Printf(UT::kPrintDebugFull, " %s && %s\n", lvl3->IsBeam1() ? "beam1" : "not beam1",
             lvl3->IsBeam2() ? "beam2" : "not beam2");
  if (lvl3->IsBeam1() && lvl3->IsBeam2()) return true;

  return false;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::L2TShowerCut(Level3<armrec> *lvl3) {
  UT::Printf(UT::kPrintDebugFull, "l2t cut\n");
  UT::Printf(UT::kPrintDebugFull, " %s && %s\n", lvl3->IsShowerTrg() ? "shower trigger" : "not shower trigger");
  if (lvl3->IsShowerTrg()) return true;

  return false;
}

/*
 * Photon RECO
 */

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon Discriminator cut\n");
  UT::Printf(UT::kPrintDebugFull, " %s\n", lvl3->fSoftwareTrigger[tower] ? "yes" : "no");
  return lvl3->fSoftwareTrigger[tower];
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonPIDCut(Level3<armrec> *lvl3, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon PID cut\n");
  UT::Printf(UT::kPrintDebugFull, " %s\n", lvl3->fIsPhoton[tower] ? "photon" : "not photon");
  return lvl3->fIsPhoton[tower];
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon energy cut\n");
  UT::Printf(UT::kPrintDebugFull, " %lf > %lf\n", lvl3->fPhotonEnergy[tower], arman::kEnergyThr);
  return lvl3->fPhotonEnergy[tower] > arman::kEnergyThr;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonEnergyLinCut(Level3<armrec> *lvl3, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon energy cut Lin\n");
  UT::Printf(UT::kPrintDebugFull, " %lf > %lf\n", lvl3->fPhotonEnergyLin[tower], arman::kEnergyThr);
  return lvl3->fPhotonEnergyLin[tower] > arman::kEnergyThr;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower, Int_t part) {
  UT::Printf(UT::kPrintDebugFull, "photon energy cut MH\n");
  UT::Printf(UT::kPrintDebugFull, " %lf > %lf\n", lvl3->fPhotonEnergyMH[tower][part], arman::kEnergyThr);
  return lvl3->fPhotonEnergyMH[tower][part] > arman::kEnergyThr;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon position cut\n");
  const Double_t x = lvl3->fPhotonPosition[tower][0];
  const Double_t y = lvl3->fPhotonPosition[tower][1];
  const Double_t edge_low = arman::fFiducial;
  const Double_t edge_high = lvl3->kTowerSize[tower] - arman::fFiducial;
  UT::Printf(UT::kPrintDebugFull, " %lf < %lf < %lf, %lf < %lf < %lf\n", edge_low, x, edge_high, edge_low, y,
             edge_high);
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower, Int_t part) {
  UT::Printf(UT::kPrintDebugFull, "photon position cut MH\n");
  const Double_t x = lvl3->fPhotonPositionMH[tower][0][part];
  const Double_t y = lvl3->fPhotonPositionMH[tower][1][part];
  const Double_t edge_low = arman::fFiducial;
  const Double_t edge_high = lvl3->kTowerSize[tower] - arman::fFiducial;
  UT::Printf(UT::kPrintDebugFull, " %lf < %lf < %lf, %lf < %lf < %lf\n", edge_low, x, edge_high, edge_low, y,
             edge_high);
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonRapidityCut(Level3<armrec> *lvl3, Int_t rap) {
  UT::Printf(UT::kPrintDebugFull, "photon rapidity cut\n");
  const Int_t tower = arman::RapToTower(rap);
  TVector3 lhcpos = PhotonGlobalPosition<armrec, arman>(lvl3, tower);
  const Double_t x = lhcpos.X();
  const Double_t y = lhcpos.Y();
  const Double_t r = TMath::Sqrt(x * x + y * y);
  const Double_t phi = TMath::ATan2(y, x) / TMath::Pi() * 180.;  // deg
  UT::Printf(UT::kPrintDebugFull, " r=%lf, phi=%lf\n", r, phi);
  switch (rap) {
    case 0:
      return r < 5. && x > 0.;
    case 1:
      return r > 5. && r < 10. && phi > -20. && phi < 50.;
    case 2:
      return r > 10. && r < 15. && phi > -7.5 && phi < 37.5;
    case 3:
      return r > 28. && r < 35. && phi > 115. && phi < 135.;
    case 4:
      return r > 35. && r < 42. && phi > 115. && phi < 135.;
    case 5:
      return r > 42. && r < 49. && phi > 115. && phi < 135.;
  }

  // const Double_t phi = TMath::ATan(-y/x) / TMath::Pi() * 180.; // deg
  // switch (rap) {
  // case 0:
  //   return r < 5. && x > 0.;
  // case 1:
  //   return r > 35. && r < 42. && phi > 45. && phi < 65.;
  // }

  return false;
}

template <typename armrec, typename arman>
TVector3 CutFunctions::PhotonGlobalPosition(Level3<armrec> *lvl3, Int_t tower) {
  // TODO [TrackProjection]: Check this change
  Int_t xlay = lvl3->fPosMaxLayer[tower][0];
  Int_t ylay = lvl3->fPosMaxLayer[tower][1];
  Double_t xpos = lvl3->fPhotonPosition[tower][0];
  Double_t ypos = lvl3->fPhotonPosition[tower][1];
  return CT::GetRecoCollisionCoordinates(arman::kArmIndex, tower, xpos, ypos, xlay, ylay);
  //
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonMultiHitCut(Level3<armrec> *lvl3, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon multi-hit cut\n");
  UT::Printf(UT::kPrintDebugFull, " %s\n", lvl3->fPhotonMultiHit[tower] ? "MH" : "SH");
  return !lvl3->fPhotonMultiHit[tower];
}

/*
 * Neutron RECO
 */

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower) {
  return lvl3->fSoftwareTrigger[tower];
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronPIDCut(Level3<armrec> *lvl3, Int_t tower) {
  return lvl3->fIsNeutron[tower];
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronEnergyCut(Level3<armrec> *lvl3, Int_t tower) {
  return lvl3->fNeutronEnergy[tower] > arman::kEnergyThr;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronPositionCut(Level3<armrec> *lvl3, Int_t tower) {
  const Double_t x = lvl3->fNeutronPosition[tower][0];
  const Double_t y = lvl3->fNeutronPosition[tower][1];
  const Double_t edge_low = arman::fFiducial;
  const Double_t edge_high = lvl3->kTowerSize[tower] - arman::fFiducial;
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronRapidityCut(Level3<armrec> *lvl3, Int_t rap) {
  const Int_t tower = arman::RapToTower(rap);
  TVector3 lhcpos = NeutronGlobalPosition<armrec, arman>(lvl3, tower);
  const Double_t x = lhcpos.X();
  const Double_t y = lhcpos.Y();
  const Double_t r = TMath::Sqrt(x * x + y * y);
  const Double_t phi = TMath::ATan2(y, x) / TMath::Pi() * 180.;  // deg

  switch (rap) {
    case 0:
      return r < 6. && x > 0.;
    case 1:
      return r > 6. && r < 12. && phi > -35. && phi < 45.;
    case 2:
      return r > 12. && r < 18. && phi > -20. && phi < 30.;
    case 3:
      return r > 28. && r < 35. && phi > 110. && phi < 135.;
    case 4:
      return r > 35. && r < 42. && phi > 110. && phi < 135.;
    case 5:
      return r > 42. && r < 49. && phi > 110. && phi < 135.;
  }

  return false;
}

template <typename armrec, typename arman>
TVector3 CutFunctions::NeutronGlobalPosition(Level3<armrec> *lvl3, Int_t tower) {
  // TODO [TrackProjection]: Check this change
  Int_t xlay = lvl3->fPosMaxLayer[tower][0];
  Int_t ylay = lvl3->fPosMaxLayer[tower][1];
  Double_t xpos = lvl3->fNeutronPosition[tower][0];
  Double_t ypos = lvl3->fNeutronPosition[tower][1];
  return CT::GetRecoCollisionCoordinates(arman::kArmIndex, tower, xpos, ypos, xlay, ylay);
  //
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronMultiHitCut(Level3<armrec> *lvl3, Int_t tower) {
  return !lvl3->fNeutronMultiHit[tower];
}

/*
 * Photon TRUE
 */

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonTruePIDCut(McEvent *mcev, Int_t tower, Int_t part) {
  UT::Printf(UT::kPrintDebugFull, "photon true PID cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  if (part >= mcev->fReference.size()) return false;
  return mcev->fReference[part]->PdgCode() == 22;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonTrueEnergyCut(McEvent *mcev, Int_t tower, Int_t part) {
  UT::Printf(UT::kPrintDebugFull, "photon true energy cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  if (part >= mcev->fReference.size()) return false;
  return mcev->fReference[part]->Energy() > arman::kEnergyThr;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonTruePositionCut(McEvent *mcev, Int_t tower, Int_t part) {
  UT::Printf(UT::kPrintDebugFull, "photon true position cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  if (part >= mcev->fReference.size()) return false;
  // TODO [TrackProjection]: Check this change
  TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(arman::kArmIndex, tower, mcev->fReference[part]->Position());
  //
  const Double_t x = cal_pos.X();
  const Double_t y = cal_pos.Y();
  const Double_t edge_low = arman::fFiducial;
  const Double_t edge_high = arman::kTowerSize[tower] - arman::fFiducial;
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonTrueRapidityCut(McEvent *mcev, Int_t rap, Int_t part) {
  UT::Printf(UT::kPrintDebugFull, "photon true rapidity cut\n");
  const Int_t tower = arman::RapToTower(rap);
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  if (part >= mcev->fReference.size()) return false;
  // TODO [TrackProjection]: Check this change
  TVector3 col_pos = CT::GetTrueCollisionCoordinates(arman::kArmIndex, mcev->fReference[part]->Position());
  //
  const Double_t x = col_pos.X();
  const Double_t y = col_pos.Y();
  const Double_t r = TMath::Sqrt(x * x + y * y);
  const Double_t phi = TMath::ATan2(y, x) / TMath::Pi() * 180.;  // deg

  switch (rap) {
    case 0:
      return r < 5. && x > 0.;
    case 1:
      return r > 5. && r < 10. && phi > -20. && phi < 50.;
    case 2:
      return r > 10. && r < 15. && phi > -7.5 && phi < 37.5;
    case 3:
      return r > 28. && r < 35. && phi > 115. && phi < 135.;
    case 4:
      return r > 35. && r < 42. && phi > 115. && phi < 135.;
    case 5:
      return r > 42. && r < 49. && phi > 115. && phi < 135.;
  }

  return false;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::PhotonTrueMultiHitCut(McEvent *mcev, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "photon true multi-hit cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  Int_t nhit = mcev->fReference.size();
  if (nhit > 1) return mcev->fReference[1]->Energy() / mcev->fReference[0]->Energy() < armrec::kPeakRatioThrPhoton;

  return true;
}

/*
 * Neutron TRUE
 */

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronTruePIDCut(McEvent *mcev, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "neutron true PID cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  return mcev->fReference[0]->PdgCode() == 2112;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronTrueEnergyCut(McEvent *mcev, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "neutron true energy cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  return mcev->fReference[0]->Energy() > arman::kEnergyThr;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronTruePositionCut(McEvent *mcev, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "neutron true position cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  // TODO [TrackProjection]: Check this change
  TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(arman::kArmIndex, tower, mcev->fReference[0]->Position());
  //
  const Double_t x = cal_pos.X();
  const Double_t y = cal_pos.Y();
  const Double_t edge_low = arman::fFiducial;
  const Double_t edge_high = arman::kTowerSize[tower] - arman::fFiducial;
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronTrueRapidityCut(McEvent *mcev, Int_t rap) {
  UT::Printf(UT::kPrintDebugFull, "neutron true rapidity cut\n");
  const Int_t tower = arman::RapToTower(rap);
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  // TODO [TrackProjection]: Check this change
  TVector3 col_pos = CT::GetTrueCollisionCoordinates(arman::kArmIndex, mcev->fReference[0]->Position());
  //
  const Double_t x = col_pos.X();
  const Double_t y = col_pos.Y();
  const Double_t r = TMath::Sqrt(x * x + y * y);
  const Double_t phi = TMath::ATan2(y, x) / TMath::Pi() * 180.;  // deg

  switch (rap) {
    case 0:
      return r < 6. && x > 0.;
    case 1:
      return r > 6. && r < 12. && phi > -35. && phi < 45.;
    case 2:
      return r > 12. && r < 18. && phi > -20. && phi < 30.;
    case 3:
      return r > 28. && r < 35. && phi > 110. && phi < 135.;
    case 4:
      return r > 35. && r < 42. && phi > 110. && phi < 135.;
    case 5:
      return r > 42. && r < 49. && phi > 110. && phi < 135.;
  }

  return false;
}

template <typename armrec, typename arman>
Bool_t CutFunctions::NeutronTrueMultiHitCut(McEvent *mcev, Int_t tower) {
  UT::Printf(UT::kPrintDebugFull, "neutron true multi-hit cut\n");
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  Int_t nhit = mcev->fReference.size();
  if (nhit > 1) return mcev->fReference[1]->Energy() / mcev->fReference[0]->Energy() < armrec::kPeakRatioThrNeutron;

  return true;
}

/*
 * Neutral Pion
 */

template <typename armrec, typename arman>
Bool_t CutFunctions::PionPIDCut(Level3<armrec> *lvl3, Int_t tower, Int_t type, Int_t eff) {
  return lvl3->fPhotonL90[tower] < PionL90Boundary<armrec, arman>(tower, lvl3->fPhotonEnergyLin[tower], type, eff);
}

template <typename armrec, typename arman>
Double_t CutFunctions::PionL90Boundary(Int_t tower, Double_t energy, Int_t type, Int_t eff) {
  Double_t *p0;
  Double_t *p1;
  Double_t *p2;
  Double_t *p3;
  Double_t *p4;

  if (type == 1) {
    /* Type I pi0 */

    // // Values for 85% selection efficiency threshold
    // Double_t p0_85[] = {4.41e+00, 1.40e+01};
    // Double_t p1_85[] = {1.02e-02, 1.72e-04};
    // Double_t p2_85[] = {5.75e+01, 3.63e+00};

    // // Values for 90% selection efficiency threshold
    // Double_t p0_90[] = {4.46e+00, 1.36e+01};
    // Double_t p1_90[] = {1.10e-02, 1.86e-04};
    // Double_t p2_90[] = {6.33e+01, 3.97e+00};

    // // Values for 95% selection efficiency threshold
    // Double_t p0_95[] = {4.42e+00, 1.81e+01};
    // Double_t p1_95[] = {1.28e-02, 1.02e-04};
    // Double_t p2_95[] = {8.44e+01, 2.95e+00};

    // Values for 85% selection efficiency threshold
    Double_t p0_85[] = {4.56482, 0.772699};
    Double_t p1_85[] = {0.00013645, 0.00206078};
    Double_t p2_85[] = {18.9017, 18.747};
    Double_t p3_85[] = {51.7907, 32.7345};
    Double_t p4_85[] = {-0.0102358, -0.0106355};

    // Values for 90% selection efficiency threshold
    Double_t p0_90[] = {3.52502, 77.3101};
    Double_t p1_90[] = {0.000253718, 7.96561e-06};
    Double_t p2_90[] = {19.2238, 19.4391};
    Double_t p3_90[] = {46.1364, 26.3802};
    Double_t p4_90[] = {-0.00910986, -0.00929002};

    // Values for 95% selection efficiency threshold
    Double_t p0_95[] = {0.618053, 0.643958};
    Double_t p1_95[] = {0.0557198, 0.041083};
    Double_t p2_95[] = {18.7894, 18.7704};
    Double_t p3_95[] = {58.3132, 51.1602};
    Double_t p4_95[] = {-0.00956966, -0.0104987};

    switch (eff) {
      case 85:
        p0 = p0_85;
        p1 = p1_85;
        p2 = p2_85;
        p3 = p3_85;
        p4 = p4_85;
        break;
      case 95:
        p0 = p0_95;
        p1 = p1_95;
        p2 = p2_95;
        p3 = p3_95;
        p4 = p4_95;
        break;
      default:
        p0 = p0_90;
        p1 = p1_90;
        p2 = p2_90;
        p3 = p3_90;
        p4 = p4_90;
    }

    return p0[tower] * log(1. + p1[tower] * energy) + p2[tower] + p3[tower] * exp(p4[tower] * energy);
  } else if (type == 2) {
    /* Type II pi0 */

    // Values for 85% selection efficiency threshold
    Double_t p0_85[] = {2.17912, 2.21911};
    Double_t p1_85[] = {1.81191, 1.75501};
    Double_t p2_85[] = {1809.46, 1722.06};

    // Values for 90% selection efficiency threshold
    Double_t p0_90[] = {2.00599, 2.19519};
    Double_t p1_90[] = {5.10412, 2.34534};
    Double_t p2_90[] = {4441.86, 2441.19};

    // Values for 95% selection efficiency threshold
    Double_t p0_95[] = {2.10612, 2.65565};
    Double_t p1_95[] = {5.32962, 0.55918};
    Double_t p2_95[] = {2548.74, 906.015};

    switch (eff) {
      case 85:
        p0 = p0_85;
        p1 = p1_85;
        p2 = p2_85;
        break;
      case 95:
        p0 = p0_95;
        p1 = p1_95;
        p2 = p2_95;
        break;
      default:
        p0 = p0_90;
        p1 = p1_90;
        p2 = p2_90;
    }

    return p0[tower] * log(p1[tower] * energy + p2[tower]);
  }

  UT::Printf(UT::kPrintError, "PionL90Boundary: invalid \"type\" specified\n");
  exit(EXIT_FAILURE);
  return -1;
}

namespace nLHCf {
template Bool_t CutFunctions::BPTXCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3);
template Bool_t CutFunctions::L2TShowerCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3);

template Bool_t CutFunctions::PhotonDiscriminatorCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonPIDCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonEnergyCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonEnergyLinCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonEnergyCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower,
                                                                       Int_t part);
template Bool_t CutFunctions::PhotonPositionCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonPositionCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower,
                                                                         Int_t part);
template Bool_t CutFunctions::PhotonRapidityCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t rap);
template TVector3 CutFunctions::PhotonGlobalPosition<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonMultiHitCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t rap);

template Bool_t CutFunctions::NeutronDiscriminatorCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronPIDCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronEnergyCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronPositionCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template TVector3 CutFunctions::NeutronGlobalPosition<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronRapidityCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t rap);
template Bool_t CutFunctions::NeutronMultiHitCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t rap);

template Bool_t CutFunctions::PhotonTruePIDCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower, Int_t part = 0);
template Bool_t CutFunctions::PhotonTrueEnergyCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower, Int_t part = 0);
template Bool_t CutFunctions::PhotonTruePositionCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower,
                                                                             Int_t part = 0);
template Bool_t CutFunctions::PhotonTrueRapidityCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t rap, Int_t part = 0);
template Bool_t CutFunctions::PhotonTrueMultiHitCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower);

template Bool_t CutFunctions::NeutronTruePIDCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower);
template Bool_t CutFunctions::NeutronTrueEnergyCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower);
template Bool_t CutFunctions::NeutronTruePositionCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower);
template Bool_t CutFunctions::NeutronTrueRapidityCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t rap);
template Bool_t CutFunctions::NeutronTrueMultiHitCut<Arm1RecPars, Arm1AnPars>(McEvent *mcev, Int_t tower);

template Bool_t CutFunctions::PionPIDCut<Arm1RecPars, Arm1AnPars>(Level3<Arm1RecPars> *lvl3, Int_t tower, Int_t type,
                                                                  Int_t eff = 90);
template Double_t CutFunctions::PionL90Boundary<Arm1RecPars, Arm1AnPars>(Int_t tower, Double_t energy, Int_t type,
                                                                         Int_t eff = 90);

template Bool_t CutFunctions::BPTXCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3);
template Bool_t CutFunctions::L2TShowerCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3);

template Bool_t CutFunctions::PhotonDiscriminatorCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonPIDCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonEnergyCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonEnergyLinCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonEnergyCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower,
                                                                       Int_t part);
template Bool_t CutFunctions::PhotonPositionCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonPositionCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower,
                                                                         Int_t part);
template Bool_t CutFunctions::PhotonRapidityCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t rap);
template TVector3 CutFunctions::PhotonGlobalPosition<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::PhotonMultiHitCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t rap);

template Bool_t CutFunctions::NeutronDiscriminatorCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronPIDCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronEnergyCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronPositionCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template TVector3 CutFunctions::NeutronGlobalPosition<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower);
template Bool_t CutFunctions::NeutronRapidityCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t rap);
template Bool_t CutFunctions::NeutronMultiHitCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t rap);

template Bool_t CutFunctions::PhotonTruePIDCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower, Int_t part = 0);
template Bool_t CutFunctions::PhotonTrueEnergyCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower, Int_t part = 0);
template Bool_t CutFunctions::PhotonTruePositionCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower,
                                                                             Int_t part = 0);
template Bool_t CutFunctions::PhotonTrueRapidityCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t rap, Int_t part = 0);
template Bool_t CutFunctions::PhotonTrueMultiHitCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower);

template Bool_t CutFunctions::NeutronTruePIDCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower);
template Bool_t CutFunctions::NeutronTrueEnergyCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower);
template Bool_t CutFunctions::NeutronTruePositionCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower);
template Bool_t CutFunctions::NeutronTrueRapidityCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t rap);
template Bool_t CutFunctions::NeutronTrueMultiHitCut<Arm2RecPars, Arm2AnPars>(McEvent *mcev, Int_t tower);

template Bool_t CutFunctions::PionPIDCut<Arm2RecPars, Arm2AnPars>(Level3<Arm2RecPars> *lvl3, Int_t tower, Int_t type,
                                                                  Int_t eff = 90);
template Double_t CutFunctions::PionL90Boundary<Arm2RecPars, Arm2AnPars>(Int_t tower, Double_t energy, Int_t type,
                                                                         Int_t eff = 90);
}  // namespace nLHCf
