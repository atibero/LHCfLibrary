#include "LHCfEvent.hh"

#include <iomanip>
#include <iostream>

using namespace std;
using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(LHCfEvent);
#endif

LHCfEvent::LHCfEvent() { Initialize(); }

LHCfEvent::LHCfEvent(const char *name, const char *title) : TNamed(name, title) { Initialize(); }

LHCfEvent::~LHCfEvent() { Delete(); }

//______________________________________________________________________________
void LHCfEvent::Initialize() {
  // Initalization of this class.
  // Clear all data.

  Clear();
}

void LHCfEvent::Delete() { ObjDelete(); }

void LHCfEvent::HeaderClear() {
  this->SetName("");
  this->SetTitle("");
  fRun = 0;
  fGnumber = 0;
  fA1number = 0;
  fA2number = 0;
  fA1flag[0] = 0;
  fA1flag[1] = 0;
  fA2flag[0] = 0;
  fA2flag[1] = 0;
  fIfile = 0;
  fTnumber = 0;
  fTag = 0;
  fTmp = 0;

  return;
}

void LHCfEvent::HeaderCopy(LHCfEvent *d) {
  this->SetName(d->GetName());
  this->SetTitle(d->GetTitle());
  this->fRun = d->fRun;
  this->fGnumber = d->fGnumber;
  this->fA1number = d->fA1number;
  this->fA2number = d->fA2number;
  this->fA1flag[0] = d->fA1flag[0];
  this->fA1flag[1] = d->fA1flag[1];
  this->fA2flag[0] = d->fA2flag[0];
  this->fA2flag[1] = d->fA2flag[1];
  this->fIfile = d->fIfile;
  this->fTnumber = d->fTnumber;
  this->fTag = d->fTag;
  this->fTmp = d->fTmp;
  return;
}

Int_t LHCfEvent::EventTime() {
  // if(Check("a1scl")) {return (Int_t)(((A1Scl*)Get("a1scl"))->time[0]);}
  // if(Check("a2scl")) {return (Int_t)(((A2Scl*)Get("a2scl"))->time[0]);}
  // if(Check("a1raw")) {return (Int_t)(((A1Raw*)Get("a1raw"))->time[0]);}
  // if(Check("a2raw")) {return (Int_t)(((A2Raw*)Get("a2raw"))->time[0]);}
  // if(Check("a1cal1")){return (Int_t)(((A1Cal1*)Get("a1cal1"))->time[0]);}
  // if(Check("a2cal1")){return (Int_t)(((A2Cal1*)Get("a2cal1"))->time[0]);}
  // if(Check("a1cal2")){return (Int_t)(((A1Cal2*)Get("a1cal2"))->time[0]);}
  // if(Check("a2cal2")){return (Int_t)(((A2Cal2*)Get("a2cal2"))->time[0]);}
  // if(Check("a1phys")){return (Int_t)(((A1Phys*)Get("a1phys"))->time[0]);}
  // if(Check("a2phys")){return (Int_t)(((A2Phys*)Get("a2phys"))->time[0]);}
  cout << "LHCfEvent::EventTime() not implemented!" << endl;
  return -1;
}

void LHCfEvent::Show() {
  cout << " ==========================  LHCfEvent "
       << "===============================" << endl;
  cout << " NAME: " << setw(10) << GetName() << "     "
       << "TITLE: " << GetTitle() << endl
       << " RUN: " << setw(5) << fRun << "  "
       << "GEVENT: " << setw(8) << fGnumber << "   "
       << "A1NUMBER: " << setw(8) << fA1number << "  "
       << "A2NUMBER: " << setw(8) << fA2number << endl
       << " A1FLAGS:  "
       << "0x" << setw(8) << hex << setfill('0') << fA1flag[0] << "  "
       << "0x" << setw(8) << hex << setfill('0') << fA1flag[1] << "     "
       << "A2FLAGS:  "
       << "0x" << setw(8) << hex << setfill('0') << fA2flag[0] << "  "
       << "0x" << setw(8) << hex << setfill('0') << fA2flag[1] << endl
       << dec << setfill(' ') << " IFILE: " << setw(6) << fIfile << "   "
       << "TNUMBER: " << setw(8) << fTnumber << "   "
       << "TAG: "
       << "0x" << setw(8) << hex << setfill('0') << fTag << "       "
       << "TMP: " << dec << setfill(' ') << fTmp << endl;

  TObject *obj;
  obj = (TObject *)fObjarray.First();
  cout << left << flush;
  cout << "   OBJs ---CLASS----  ---NAME---    ---------TITLE---------------" << endl;

  Int_t i = 0;
  while (obj != 0) {
    cout << "    " << setw(2) << i << "  " << setw(12) << obj->ClassName() << "  " << setw(10) << obj->GetName()
         << "    " << obj->GetTitle() << endl;
    obj = (TObject *)fObjarray.After(obj);
    ++i;
  }
  cout << right << flush;
}
