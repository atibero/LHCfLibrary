#include "ReadTableCal.hh"

#include <TROOT.h>

#include <fstream>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(ReadTableCal);
#endif

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armcal>
ReadTableCal<armcal>::ReadTableCal() {
  AllocateVectors();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armcal>
ReadTableCal<armcal>::~ReadTableCal() {}

template <typename armcal>
void ReadTableCal<armcal>::AllocateVectors() {
  UT::AllocateVector1D(fPosDetID, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    UT::AllocateVector3D(fPosDetID[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it]);

  UT::AllocateVector4D(fCalPedShift, this->kCalNtower, this->kCalNlayer, this->kCalNrange,
                       this->kLastRun - this->kFirstRun + 1);
  UT::AllocateVector4D(fCalDelShift, this->kCalNtower, this->kCalNlayer, this->kCalNrange,
                       this->kLastRun - this->kFirstRun + 1);
  UT::AllocateVector2D(fCalRangeFactor, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector2D(fCalHV, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector2D(fCalGainFactor, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector2D(fCalAttFactor, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector2D(fCalConvFactor, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector3D(fCalPedMean, this->kCalNtower, this->kCalNlayer, this->kCalNrange);
  UT::AllocateVector3D(fCalPedRms, this->kCalNtower, this->kCalNlayer, this->kCalNrange);

  UT::AllocateVector1D(fPosGainFactor, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    UT::AllocateVector3D(fPosGainFactor[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it]);
  UT::AllocateVector1D(fPosConvFactor, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    UT::AllocateVector4D(fPosConvFactor[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it],
                         this->kPosNsample);
  UT::AllocateVector1D(fPosPedMean, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    UT::AllocateVector4D(fPosPedMean[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it], this->kPosNsample);
  UT::AllocateVector1D(fPosPedRms, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    UT::AllocateVector4D(fPosPedRms[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it], this->kPosNsample);
  // skip allocation of fPosCrossTalk. It will be done in SetPosCrossTalk;
  return;
}

/*-----------------------*/
/*--- Read parameters ---*/
/*-----------------------*/
template <typename armcal>
void ReadTableCal<armcal>::ReadTables() {
  UT::Printf(UT::kPrintInfo, "Reading Arm%d calibration tables from %s\n", this->kArmIndex + 1,
             this->kTableDirectory.Data());

  SetPosDetID();
  SetCalPedShift();
  SetCalDelShift();
  SetCalRangeFactor();
  SetCalHV();
  SetCalGainFactor();
  SetCalAttFactor();
  SetCalConvFactor();
  SetPosHV();
  SetPosGainFactor();
  SetPosConvFactor();
  SetPosCrossTalk();
  if (this->fOperation == kLHC2022) SetPosSilCorr();

  SetCalPedestal();
  SetPosPedestal();
}

/*----------------------------------*/
/*--- Set calibration parameters ---*/
/*----------------------------------*/
template <typename armcal>
void ReadTableCal<armcal>::SetPosDetID() {
  ifstream infile((this->kTableDirectory + "/" + this->fPosDetIdFile).Data(), ifstream::in);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t iposdet, tower, layer, xy, channel, jposdet;
    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %d %d %d", &iposdet, &tower, &layer, &xy, &channel);
    // sscanf(buf, "%d %d %d %d %d %d", &iposdet, &tower, &layer, &xy, &channel, &jposdet);

    // For Arm1
    if (this->kArmIndex == 0) channel = this->kPosNchannel[tower] - channel - 1;

    if (tower >= 0 && tower < this->kPosNtower && layer >= 0 && layer < this->kPosNlayer && xy >= 0 &&
        xy < this->kPosNview)
      if (channel >= 0 && channel < this->kPosNchannel[tower]) fPosDetID[tower][layer][xy][channel] = iposdet;
  }

  infile.close();
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalPedShift() {
  ifstream infile((this->kTableDirectory + "/" + this->fCalPedShiftFile).Data(), ifstream::in);

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Pedestal Shift Corrections:\n");
  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t run, tower, layer;
    Double_t nradc, wradc;

    infile.getline(buf, buf_size);
    if (!strcmp(buf, "")) continue;  // Skip empty lines
    if (buf[0] == '#') continue;     // Skip comment lines
    sscanf(buf, "%d %d %d %lf %lf", &run, &tower, &layer, &nradc, &wradc);
    // sscanf(buf, "%d %d %d %d %d %d", &iposdet, &tower, &layer, &xy, &channel, &jposdet);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer) {
      if (run >= this->kFirstRun && run <= this->kLastRun) {
        fCalPedShift[tower][layer][0][run - this->kFirstRun] = nradc;
        fCalPedShift[tower][layer][1][run - this->kFirstRun] = wradc;
        UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%d\t%lf\t%lf\n", run, tower, layer,
                   fCalPedShift[tower][layer][0][run - this->kFirstRun],
                   fCalPedShift[tower][layer][1][run - this->kFirstRun]);
      } else {
        UT::Printf(UT::kPrintError, "Warning: ignoring run %d outside range!\n", run);
      }
    } else {
      UT::Printf(UT::kPrintError, "Error: (tower=%d, layer=%d) outside range!\n", tower, layer);
      exit(EXIT_FAILURE);
    }
  }
  UT::Printf(UT::kPrintDebug, "\n");
  infile.close();
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalDelShift() {
  ifstream infile((this->kTableDirectory + "/" + this->fCalDelShiftFile).Data(), ifstream::in);

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Delayed Shift Corrections:\n");
  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t run, tower, layer;
    Double_t nradc, wradc;

    infile.getline(buf, buf_size);
    if (!strcmp(buf, "")) continue;  // Skip empty lines
    if (buf[0] == '#') continue;     // Skip comment lines
    sscanf(buf, "%d %d %d %lf %lf", &run, &tower, &layer, &nradc, &wradc);
    // sscanf(buf, "%d %d %d %d %d %d", &iposdet, &tower, &layer, &xy, &channel, &jposdet);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer) {
      if (run >= this->kFirstRun && run <= this->kLastRun) {
        fCalDelShift[tower][layer][0][run - this->kFirstRun] = nradc;
        fCalDelShift[tower][layer][1][run - this->kFirstRun] = wradc;
        UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%d\t%lf\t%lf\n", run, tower, layer,
                   fCalDelShift[tower][layer][0][run - this->kFirstRun],
                   fCalDelShift[tower][layer][1][run - this->kFirstRun]);
      } else {
        UT::Printf(UT::kPrintError, "Warning: ignoring run %d outside range!\n", run);
      }
    } else {
      UT::Printf(UT::kPrintError, "Error: (tower=%d, layer=%d) outside range!\n", tower, layer);
      exit(EXIT_FAILURE);
    }
  }
  UT::Printf(UT::kPrintDebug, "\n");
  infile.close();
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalRangeFactor() {
  ifstream infile((this->kTableDirectory + "/" + this->fCalRangeFactorFile).Data(), ifstream::in);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer;
    Double_t factor;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %lf", &tower, &layer, &factor);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer) {
      fCalRangeFactor[tower][layer] = factor;
    }
  }

  infile.close();

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Range Factors:\n");
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%lf\n", it, il, fCalRangeFactor[it][il]);
  UT::Printf(UT::kPrintDebug, "\n");
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalHV() {
  ifstream infile((this->kTableDirectory + "/" + this->fCalHVFile).Data(), ifstream::in);

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter HV Factors:\n");
  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer;
    Double_t hv;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %lf", &tower, &layer, &hv);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer) {
      fCalHV[tower][layer] = hv;
      UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%lf\n", tower, layer, fCalHV[tower][layer]);
      fflush(stdout);
    }
  }
  UT::Printf(UT::kPrintDebug, "\n");

  infile.close();
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalGainFactor() {
  ifstream infile((this->kTableDirectory + "/" + this->fCalGainFactorFile).Data(), ifstream::in);

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter HV Gain Change Factors:\n");
  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer, idummy;
    Char_t sdummy[1024];
    const Int_t ndata = 11;
    Double_t xx[ndata] = {375., 400., 425., 450., 475., 500., 550., 600., 800., 1000., 1100.};
    Double_t yy[ndata];

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %s %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", &tower, &layer, sdummy, &idummy, &yy[10],
           &yy[9], &yy[8], &yy[7], &yy[6], &yy[5], &yy[4], &yy[3], &yy[2], &yy[1], &yy[0]);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer) {
      fCalGainFactor[tower][layer] = UT::LinearInterpolation(ndata, xx, yy, fCalHV[tower][layer]) /
                                     UT::LinearInterpolation(ndata, xx, yy, this->kCalBaseHV);
      UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%lf\n", tower, layer, fCalGainFactor[tower][layer]);
    }
  }
  UT::Printf(UT::kPrintDebug, "\n");

  infile.close();
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalAttFactor() {
  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Cable Attenuation Factors:\n");
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      fCalAttFactor[it][il] = 1.;
      if (this->fOperation == kLHC2025 || this->fOperation == kLHC2022 || this->fOperation == kLHC2016 ||
          this->fOperation == kLHC2015) {
        if (it == 0) {
          if (il == 15)
            fCalAttFactor[it][il] = this->kCalAttFactor1;
          else
            fCalAttFactor[it][il] = this->kCalAttFactor0;
        } else {
          if (il > 0 && il < 10)
            fCalAttFactor[it][il] = this->kCalAttFactor2;
          else
            fCalAttFactor[it][il] = this->kCalAttFactor1;
        }
      }
      UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%lf\n", it, il, fCalAttFactor[it][il]);
    }
  UT::Printf(UT::kPrintDebug, "\n");
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalConvFactor() {
  ifstream infile((this->kTableDirectory + "/" + this->fCalConvFactorFile).Data(), ifstream::in);

  UT::ClearVector2D(fCalConvFactor, -1.);

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Initial Conversion Factors:\n");
  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer;
    Double_t adcfactor;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %lf", &tower, &layer, &adcfactor);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer) {
      fCalConvFactor[tower][layer] = adcfactor;
      UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%lf\n", tower, layer, fCalConvFactor[tower][layer]);
    }
  }
  UT::Printf(UT::kPrintDebug, "\n");

  infile.close();

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Final Factors:\n");
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      if (fCalConvFactor[it][il] > 0.) {
        fCalConvFactor[it][il] = 1. / (fCalConvFactor[it][il] * fCalAttFactor[it][il] * fCalGainFactor[it][il]);
        UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%lf\n", it, il, fCalConvFactor[it][il]);
      } else {
        UT::Printf(UT::kPrintError, "Error: missing conversion factor! (tower=%d, layer=%d, factor=%lf)\n", it, il,
                   fCalConvFactor[it][il]);
        exit(EXIT_FAILURE);
      }
  UT::Printf(UT::kPrintDebug, "\n");
}

template <typename armcal>
void ReadTableCal<armcal>::SetCalPedestal() {
  /* Set pedestal mean */
  ifstream infile((this->kTableDirectory + "/" + this->fCalPedMeanFile).Data(), ifstream::in);

  UT::ClearVector3D(fCalPedMean, -1.);
  UT::ClearVector3D(fCalPedRms, -1.);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer, range;
    Double_t mean, rms;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %d %lf %lf", &tower, &layer, &range, &mean, &rms);
    if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer && range >= 0 &&
        range < this->kCalNrange) {
      fCalPedMean[tower][layer][range] = mean;
      fCalPedRms[tower][layer][range] = rms;
    }
  }

  infile.close();

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir)
        if (fCalPedMean[it][il][ir] < 0.) {
          UT::Printf(UT::kPrintError, "Error: missing pedestal mean! (tower=%d, layer=%d, range=%d, mean=%lf)\n", it,
                     il, ir, fCalPedMean[it][il][ir]);
          exit(EXIT_FAILURE);
        }

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir)
        if (fCalPedRms[it][il][ir] < 0.) {
          UT::Printf(UT::kPrintError, "Error: missing pedestal rms! (tower=%d, layer=%d, range=%d, mean=%lf)\n", it, il,
                     ir, fCalPedRms[it][il][ir]);
          exit(EXIT_FAILURE);
        }

  UT::Printf(UT::kPrintDebug, "Setting Calorimeter Pedestal Mean and RMS:\n");
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
        UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%d\t%lf\t%lf\n", it, il, ir, fCalPedMean[it][il][ir],
                   fCalPedRms[it][il][ir]);
      }
  UT::Printf(UT::kPrintDebug, "\n");

  /* Open tree with real pedestals */
  fCalPedFile = new TFile((this->kTableDirectory + "/" + this->fCalPedRealFile).Data());
  fCalPedFile->GetObject("ped_tree", fCalPedTree);
  fCalPedBranch = fCalPedTree->GetBranch("pedestal");
  fCalPedVec = new vector<Double_t>();
  fCalPedTree->SetBranchAddress("pedestal", &fCalPedVec, &fCalPedBranch);
  fCalPedEntries = fCalPedTree->GetEntries();
}

template <typename armcal>
void ReadTableCal<armcal>::SetPosHV() {
  ifstream infile((this->kTableDirectory + "/" + this->fPosHVFile).Data(), ifstream::in);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Double_t hv;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%lf", &hv);
    fPosHV = hv;
    UT::Printf(UT::kPrintInfo, "fPosHV : %lf ", fPosHV);
  }
  UT::Printf(UT::kPrintDebug, "\n");
  infile.close();
}

template <typename armcal>
void ReadTableCal<armcal>::SetPosGainFactor() {
  if (this->kArmIndex == 0) {  // Arm1

    ifstream infile((this->kTableDirectory + "/" + this->fPosGainFactorFile).Data(), ifstream::in);

    const Int_t ndata = 4;
    Double_t xx[ndata] = {480., 550., 600., 950.};
    Double_t yy[ndata] = {-1., -1., -1., -1.};

    while (infile.good()) {
      const Int_t buf_size = 1024;
      Char_t buf[buf_size];
      Int_t tower, layer, view, ch, hv;
      Double_t gain;

      infile.getline(buf, buf_size);
      if (buf[0] == '#') continue;
      sscanf(buf, "%d %d %d %d %d %lf", &tower, &layer, &view, &ch, &hv, &gain);
      if (tower >= 0 && tower < this->kPosNtower && layer >= 0 && layer < this->kPosNlayer && view >= 0 &&
          view < this->kPosNview)
        if (ch >= 0 && ch < this->kPosNchannel[tower]) {
          if (hv == 480)
            yy[0] = gain;
          else if (hv == 550)
            yy[1] = gain;
          else if (hv == 600)
            yy[2] = gain;
          else if (hv == 950)
            yy[3] = gain;
        }
    }

    infile.close();

    for (Int_t i = 0; i < ndata; ++i) {
      if (yy[i] < 0.) {
        UT::Printf(UT::kPrintError, "Error: missing GSO bar gain value! (x[i]=%lf, y[i]=%lf\n", xx[i], yy[i]);
        exit(EXIT_FAILURE);
      }
      UT::Printf(UT::kPrintDebugFull, "GSO bar gain value! (x[i]=%lf, y[i]=%lf\n", xx[i], yy[i]);
    }

    for (Int_t it = 0; it < this->kPosNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
            fPosGainFactor[it][il][iv][ic] = UT::LinearInterpolation(ndata, xx, yy, fPosHV - 0.0001) /
                                             UT::LinearInterpolation(ndata, xx, yy, this->kPosBaseHV - 0.0001);
            UT::Printf(UT::kPrintDebugFull, "fPosGainFactor[%d][%d][%d][%d] = %lf\n", it, il, iv, ic,
                       fPosGainFactor[it][il][iv][ic]);
          }

  } else {  // Arm2
    for (Int_t it = 0; it < this->kPosNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) fPosGainFactor[it][il][iv][ic] = 1.;
  }
}

template <typename armcal>
void ReadTableCal<armcal>::SetPosConvFactor() {
  ifstream infile((this->kTableDirectory + "/" + this->fPosConvFactorFile).Data(), ifstream::in);

  UT::ClearVector5D(fPosConvFactor, -1.);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer, view, channel, sample;
    Double_t adcfactor;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    if (this->kArmIndex == 0) {  // Arm1
      sample = 0;
      sscanf(buf, "%d %d %d %d %lf", &tower, &layer, &view, &channel, &adcfactor);
    } else {  // Arm2
      tower = 0;
      sscanf(buf, "%d %d %d %d %lf", &sample, &layer, &view, &channel, &adcfactor);
    }
    if (tower >= 0 && tower < this->kPosNtower && layer >= 0 && layer < this->kPosNlayer && view >= 0 &&
        view < this->kPosNview && sample >= 0 && sample < this->kPosNsample)
      if (channel >= 0 && channel < this->kPosNchannel[tower]) {
        fPosConvFactor[tower][layer][view][channel][sample] = adcfactor;
      }
  }

  infile.close();

  UT::Printf(UT::kPrintDebug, "Setting PosDet Conversion Factors:\n");
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is)
            if (fPosConvFactor[it][il][iv][ic][is] > 0.) {
              fPosConvFactor[it][il][iv][ic][is] =
                  1. / (fPosConvFactor[it][il][iv][ic][is] * fPosGainFactor[it][il][iv][ic]);
              UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%d\t%d\t%d\t%lf\n", it, il, iv, ic, is,
                         fPosConvFactor[it][il][iv][ic][is]);
            } else {
              // Since LHC2022, silicon gains for sample 0 and 2 are set to -1 since they are not used
              if (this->kArmIndex != Arm2Params::kArmIndex || this->fOperation != kLHC2022 || is == 1) {
                UT::Printf(UT::kPrintError,
                           "Error: missing conversion factor! (tower=%d, layer=%d, view=%d, channel=%d, sample=%d, "
                           "factor=%lf)\n",
                           it, il, iv, ic, is, fPosConvFactor[it][il][iv][ic][is]);
                exit(EXIT_FAILURE);
              }
            }
  UT::Printf(UT::kPrintDebug, "\n");
}

template <typename armcal>
void ReadTableCal<armcal>::SetPosPedestal() {
  /* Set pedestal mean */
  ifstream infile((this->kTableDirectory + "/" + this->fPosPedMeanFile).Data(), ifstream::in);

  UT::ClearVector5D(fPosPedMean, -1.);
  UT::ClearVector5D(fPosPedRms, -1.);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer, view, channel, sample;
    Double_t mean, rms;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    if (this->kArmIndex == 0) {  // Arm1
      sample = 0;
      sscanf(buf, "%d %d %d %d %lf %lf", &tower, &layer, &view, &channel, &mean, &rms);
    } else {  // Arm2
      tower = 0;
      sscanf(buf, "%d %d %d %d %lf %lf", &sample, &layer, &view, &channel, &mean, &rms);
    }
    if (tower >= 0 && tower < this->kPosNtower && layer >= 0 && layer < this->kPosNlayer && view >= 0 &&
        view < this->kPosNview && sample >= 0 && sample < this->kPosNsample)
      if (channel >= 0 && channel < this->kPosNchannel[tower]) {
        fPosPedMean[tower][layer][view][channel][sample] = mean;
        fPosPedRms[tower][layer][view][channel][sample] = rms;
      }
  }

  infile.close();

  for (Int_t it = 0; it < this->kPosNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is)
            if (fPosPedMean[it][il][iv][ic][is] < 0.) {
              UT::Printf(
                  UT::kPrintError,
                  "Error: missing pedestal mean! (tower=%d, layer=%d, view=%d, channel=%d, sample=%d, mean=%lf)\n", it,
                  il, iv, ic, is, fPosPedMean[it][il][iv][ic][is]);
              exit(EXIT_FAILURE);
            }

  for (Int_t it = 0; it < this->kPosNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is)
            if (fPosPedRms[it][il][iv][ic][is] < 0.) {
              UT::Printf(
                  UT::kPrintError,
                  "Error: missing pedestal rms! (tower=%d, layer=%d, view=%d, channel=%d, sample=%d, mean=%lf)\n", it,
                  il, iv, ic, is, fPosPedRms[it][il][iv][ic][is]);
              exit(EXIT_FAILURE);
            }

  UT::Printf(UT::kPrintDebug, "Setting PosDet Pedestal Mean and RMS:\n");
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            UT::Printf(UT::kPrintDebug, "\t%d\t%d\t%d\t%d\t%d\t%lf\t%lf\n", it, il, iv, ic, is,
                       fPosPedMean[it][il][iv][ic][is], fPosPedRms[it][il][iv][ic][is]);
          }
  UT::Printf(UT::kPrintDebug, "\n");

  /* Open tree with real pedestals */
  fPosPedFile = new TFile((this->kTableDirectory + "/" + this->fPosPedRealFile).Data());
  fPosPedFile->GetObject("ped_tree", fPosPedTree);
  fPosPedBranch = fPosPedTree->GetBranch("pedestal");
  fPosPedVec = new vector<Double_t>();
  fPosPedTree->SetBranchAddress("pedestal", &fPosPedVec, &fPosPedBranch);
  fPosPedEntries = fPosPedTree->GetEntries();
}

template <typename armcal>
void ReadTableCal<armcal>::SetPosCrossTalk() {
  // Skip Arm2
  if (this->fPosCrossTalkFile == "") return;

  TFile file(this->kTableDirectory + "/" + this->fPosCrossTalkFile);
  if (file.IsZombie()) {
    UT::Printf(UT::kPrintError, "Error: No Pos CrossTalk file %s", this->fPosCrossTalkFile.Data());
    return;
  }

  Int_t nmap = this->kPosNlayer * this->kPosNview;  // it must be 8
  for (Int_t imap = 0; imap < nmap; ++imap) {
    TMatrixD* m = (TMatrixD*)file.Get(Form("Matrix_MAPMT%d", imap));
    if (m == NULL) UT::Printf(UT::kPrintError, "No TMatrixD (%s)", Form("Matrix_MAPMT%d", imap));
    fPosCrossTalk.push_back(*m);
  }
  // Inverse matrix (used for MC)
  for (Int_t imap = 0; imap < nmap; ++imap) {
    TMatrixD* m = (TMatrixD*)file.Get(Form("Before_Matrix_MAPMT%d", imap));
    if (m == NULL) UT::Printf(UT::kPrintError, "No TMatrixD (%s)", Form("Before_Matrix_MAPMT%d", imap));
    fPosCrossTalkInv.push_back(*m);
  }

  file.Close();
  return;
}

template <typename armcal>
void ReadTableCal<armcal>::SetPosSilCorr() {
  // Skip Arm1
  if (this->kArmIndex == 0) return;

  TFile file(this->kTableDirectory + "/" + this->fPosSilCorrFile);
  if (file.IsZombie()) {
    UT::Printf(UT::kPrintError, "Error: No Pos SilCorr file %s", this->fPosSilCorrFile.Data());
    return;
  }

  TSpline3* s = (TSpline3*)file.Get("Sample1CorrectionCurve");
  if (s == NULL) UT::Printf(UT::kPrintError, "No TSpline3 (Sample1CorrectionCurve)");
  fPosSilCorr = *s;

  TSpline3* s_inv = (TSpline3*)file.Get("Sample1InverseCorrectionCurve");
  if (s_inv == NULL) UT::Printf(UT::kPrintError, "No TSpline3 (Sample1InverseCorrectionCurve)");
  fPosSilCorrInv = *s_inv;

  file.Close();
  return;
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class ReadTableCal<Arm1CalPars>;

template class ReadTableCal<Arm2CalPars>;
}  // namespace nLHCf
