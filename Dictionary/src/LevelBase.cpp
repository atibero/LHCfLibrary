#include "LevelBase.hh"

#include <iomanip>
#include <iostream>

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "Utils.hh"

#if !defined(__CINT__)
templateClassImp(LevelBase);
#endif

/**
 *  @class nLHCf::LevelBase
 *  @brief Base data container
 *  @details This class contains common data to all data levels (run #, event #, global event #, time, flag, counter)
 */

using namespace nLHCf;

template <typename armclass>
LevelBase<armclass>::LevelBase() {
  AllocateVectors();
}

template <typename armclass>
LevelBase<armclass>::LevelBase(const char *name, const char *title) : TNamed(name, title) {
  AllocateVectors();
}

template <typename armclass>
LevelBase<armclass>::~LevelBase() {}

//////////////////////////////////////////////////
/// @brief Allocations of vectors
/// @tparam armclass
template <typename armclass>
void LevelBase<armclass>::AllocateVectors() {
  Utils::AllocateVector1D(fFlag, this->kNflag);
  Utils::AllocateVector1D(fCounter, this->kNcounter);
}

///////////////////////////
/// @brief  Beam DAQ Flag
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsDaqFlagBeam() {
  return ((fFlag[0] >> 12) & 0x1) != 0;
}

///////////////////////////
/// @brief  Pedestal DAQ Flag
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsDaqFlagPedestal() {
  return ((fFlag[0] >> 13) & 0x1) != 0;
}

///////////////////////////
/// @brief  DAQ Flag for L3T Trigger in the other Arm
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsDaqFlagOtherArmL3T() {
  if (this->fOperation == kLHC2015 || this->fOperation == kLHC2022 || this->fOperation == kLHC2025)
    return ((fFlag[0] >> 21) & 0x1) != 0;
  else
    return kFALSE;
}

///////////////////////////
/// @brief  DAQ Flag for Enable in the other Arm
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsDaqFlagOtherArmEnable() {
  if (this->fOperation == kLHC2015 || this->fOperation == kLHC2022 || this->fOperation == kLHC2025)
    return ((fFlag[0] >> 22) & 0x1) != 0;
  else
    return kFALSE;
}

///////////////////////////
/// @brief  DAQ Flag for Shower Trigger in the other Arm
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsDaqFlagOtherArmShowerTrg() {
  if (this->fOperation == kLHC2015 || this->fOperation == kLHC2022 || this->fOperation == kLHC2025)
    return ((fFlag[0] >> 23) & 0x1) != 0;
  else
    return kFALSE;
}

///////////////////////////
/// @brief  DAQ Flag for L1T Trigger in the other Arm
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsDaqFlagOtherArmL1T() {
  if (this->fOperation == kLHC2015 || this->fOperation == kLHC2022 || this->fOperation == kLHC2025)
    return ((fFlag[0] >> 20) & 0x1) != 0;
  else
    return kFALSE;
}

///////////////////////////
/// @brief  Event category (Beam) defined using Beam DAQ flag (LHC) or L1T_Trg (SPS)
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsBeam() {
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    return IsL1tTrg();  // No beam flag in SPS2015, SPS2021 and SPS2022
  }

  // default (LHC2015, LHC2022, LHC2025)
  return IsDaqFlagBeam();
}

///////////////////////////
/// @brief  Event category (Pedestal) defined using Beam DAQ flag (LHC) or L1T_Trg (SPS)
/// @tparam armclass
/// @return
template <typename armclass>
Bool_t LevelBase<armclass>::IsPedestal() {
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    return IsPedestalTrg();  // Only Pedestal Trg flag worked in SPS2021 and 2022
  }

  // default (LHC2015, LHC2022, kLHC2025)
  return (this->fFlag[1] == 0 ? IsDaqFlagPedestal() & IsPedestalTrg() : 0);
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsShowerTrg() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 4) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 4) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 4) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    return IsL1tTrg();  // No shower trigger in SPS tests. This is for convenience.
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsSpecialTrg() {
  return IsPi0Trg();
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsPi0Trg() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 5) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 5) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 5) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsHighEMTrg() {
  if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 6) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 6) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsHadronTrg() {
  if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 7) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 7) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsZdcTrg() {
  if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 10) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 10) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsLaserTrg() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 3) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 2) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 2) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsFcTrg() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 8) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 9) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 9) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsL1tTrg() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 7) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 8) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 8) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2015) {  // Ask to Menjo-san: This value is from Mitsuka-san software
    return (fFlag[0] >> 12) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2021) {
    return (fFlag[0] >> 8) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2022) {
    return (fFlag[0] >> 8) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsPedestalTrg() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 6) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 3) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 3) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2015) {  // Ask to Menjo-san: This value is from Mitsuka-san software
    return (fFlag[0] >> 13) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2021) {
    return (fFlag[0] >> 5) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kSPS2022) {
    return (fFlag[0] >> 5) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

//---------------------------------------------------------------
//                   Beam & ATLAS Flag
//---------------------------------------------------------------

template <typename armclass>
Bool_t LevelBase<armclass>::IsBeam1() {
  if (this->fOperation == kLHC2015) {
    return fFlag[0] & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return fFlag[0] & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return fFlag[0] & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsBeam2() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 1) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 1) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 1) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsAtlasL1A() {
  if (this->fOperation == kLHC2015) {
    return (fFlag[0] >> 15) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2022) {
    return (fFlag[0] >> 15) & 0x1 ? kTRUE : kFALSE;
  } else if (this->fOperation == kLHC2025) {
    return (fFlag[0] >> 15) & 0x1 ? kTRUE : kFALSE;
  }
  return kFALSE;
}

//---------------------------------------------------------------
//                   Discriminator Flag
//---------------------------------------------------------------

template <typename armclass>
Bool_t LevelBase<armclass>::IsDsc(Int_t tower, Int_t layer) {
  // default for LHC 2015 and LHC 2022
  return (fFlag[1] >> (tower * 16 + layer)) & 0x1 ? kTRUE : kFALSE;
}

template <typename armclass>
Bool_t LevelBase<armclass>::IsFcDsc(Int_t channel) {
  // default for LHC 2015 and LHC 2022
  return (fFlag[0] >> (24 + channel)) & 0x1 ? kTRUE : kFALSE;
}

//---------------------------------------------------------------
//                   Counter Values
//---------------------------------------------------------------

template <typename armclass>
Int_t LevelBase<armclass>::BunchID() {
  if (this->fOperation == kLHC2022) {
    return fCounter[10];
  } else if (this->fOperation == kLHC2025) {
    return fCounter[10];
  }
  // Not yet for other format
  return 0;
}

template <typename armclass>
Int_t LevelBase<armclass>::Beam1ID() {
  if (this->fOperation == kLHC2022) {
    return fCounter[11];
  } else if (this->fOperation == kLHC2025) {
    return fCounter[11];
  }
  // Not yet for other format
  return 0;
}

template <typename armclass>
Int_t LevelBase<armclass>::Beam2ID() {
  if (this->fOperation == kLHC2022) {
    return fCounter[12];
  } else if (this->fOperation == kLHC2025) {
    return fCounter[12];
  }
  // Not yet for other format
  return 0;
}

template <typename armclass>
UInt_t LevelBase<armclass>::AtlasEcr() {
  if (this->fOperation == kLHC2022) {
    return fCounter[16];
  } else if (this->fOperation == kLHC2025) {
    return fCounter[16];
  }
  // Not yet for other format
  return 0;
}

template <typename armclass>
UInt_t LevelBase<armclass>::AtlasL1ID() {
  if (this->fOperation == kLHC2022) {
    return fCounter[17] - 1;  // -1 is an offset to match with ID in ATLAS data.
  } else if (this->fOperation == kLHC2025) {
    return fCounter[17] - 1;  // -1 is an offset to match with ID in ATLAS data.
  }
  // Not yet for other format
  return 0;
}

/////////////////////////////////////////////////////
/// @brief  Clear all data of LevelBase and TNamed. (No recomendation to use)
/// @tparam armclass
/// @param option [in] Selection of data. \n
///                    option: default = "" ("" is replaced to "all" in this function.) \n
///                    For details, refer DataClear().
template <typename armclass>
void LevelBase<armclass>::Clear(Option_t *option) {
  TNamed::Clear(option);  // Clear all parameters of TNamed like name, title also
  TString opt = option;
  if (opt == "")
    DataClear("all");
  else
    DataClear(option);
  return;
}

////////////////////////////////////////////////////////////////////////////////
/// @brief  Clear the data. All values are set to the defaults (0.)
/// @tparam armclass
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    header, calorimeter, posdep, frontcounter, tdc, counters, qualityflag
/// @details An example of Clear with specifying data : Clear("calorimeter, posdep, tdc")
template <typename armclass>
void LevelBase<armclass>::DataClear(Option_t *option) {
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("all") || opt.Contains("header")) {
    fRun = -1;
    fEvent = -1;
    fGevent = -1;
    for (int i = 0; i < 2; ++i) {
      fTime[i] = 0;
    }
    for (int j = 0; j < 3; ++j) {
      fFlag[j] = 0;
    }
  }

  if (opt.Contains("all") || opt.Contains("counters")) {
    Utils::template ClearVector1D<Long_t>(fCounter, -1);
  }

  return;
}

/////////////////////////////////////////////////////
/// @brief  Copy all data of LevelBase and TNamed. (No recomendation to use)
/// @tparam armclass
/// @param d      [in] Copy from d to this
/// @param option [in] Selection of data. \n
///                    option: default = "" ("" is replaced to "all" in this function.) \n
///                    For details, refer DataClear().
template <typename armclass>
void LevelBase<armclass>::Copy(LevelBase *d, Option_t *option) {
  // Clear all parameters of TNamed like name, title also
  TNamed::Copy(*d);

  TString opt = option;
  if (opt == "")
    DataCopy(d, "all");
  else
    DataCopy(d, option);
  return;
}

/////////////////////////////////////////////////////
/// @brief  Copy data from d to this
/// @tparam armclass
/// @param d      [in] Copy from d to this
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    header, calorimeter, posdep, frontcounter, tdc, counters, qualityflag
/// @details An example of Clear with specifying data : Copy(d, "calorimeter, posdep, tdc")
template <typename armclass>
void LevelBase<armclass>::DataCopy(LevelBase *d, Option_t *option) {
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("all") || opt.Contains("header")) {
    fRun = d->fRun;
    fEvent = d->fEvent;
    fGevent = d->fGevent;
    for (int i = 0; i < 2; ++i) {
      fTime[i] = d->fTime[i];
    }
    for (int j = 0; j < 3; ++j) {
      fFlag[j] = d->fFlag[j];
    }
  }
  if (opt.Contains("all") || opt.Contains("counters")) {
    Utils::template CopyVector1D<Long_t>(fCounter, d->fCounter);
  }

  return;
}

//////////////////////////////////////////////////////////////
/// @brief Print-out data contained in this class.
template <typename armclass>
void LevelBase<armclass>::Print() {
  cout << "#RUN " << setw(6) << fRun << "#Event " << setw(8) << fEvent << "#Gevent " << setw(8) << fGevent << endl;

  // cout << "Flags:" << endl;
  // for(Int_t i=0;i < this->kNflag; i++){
  // 	cout << hex << setw(8) << "0x" << fFlag[i] << " ";
  // }
  // cout << dec << endl;

  // DAQ Flags
  cout << endl << "Flags: ";
  for (Int_t i = 0; i < this->kNflag; ++i) cout << Form("%08x  ", fFlag[i]);
  cout << endl;

  // counters
  cout << endl << "Counters" << endl;
  Int_t nline = this->kNcounter / 8 + 1;
  Int_t c_id = 0;
  for (int il = 0; il < nline; ++il) {
    // label
    for (int i = 0; i < 8; ++i) {
      c_id = il * 8 + i;
      if (c_id == this->kNcounter) break;
      cout << Form("%11s ", this->kCounterLabel[c_id]);
    }
    cout << endl;
    // values
    for (int i = 0; i < 8; ++i) {
      c_id = il * 8 + i;
      if (c_id == this->kNcounter) break;
      cout << Form("%11ld ", fCounter[c_id]);
    }
    cout << endl;
  }
  cout << endl;
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class LevelBase<Arm1Params>;

template class LevelBase<Arm2Params>;

template class LevelBase<Arm1RecPars>;

template class LevelBase<Arm2RecPars>;
}  // namespace nLHCf
