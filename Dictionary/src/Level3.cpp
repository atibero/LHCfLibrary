#include "Level3.hh"

#include <TVector3.h>

#include <iomanip>
#include <iostream>

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "LHCfParams.hh"
#include "Utils.hh"

#if !defined(__CINT__)
templateClassImp(Level3);
#endif

/**
 * @class nLHCf::Level3
 * @brief
 * @details
 * This class contains all information of recostructed results from Level2
 *
 */

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

template <typename armclass>
Level3<armclass>::Level3() : LevelBase<armclass>::LevelBase() {
  AllocateVectors();
}

template <typename armclass>
Level3<armclass>::Level3(const char *name, const char *title) : LevelBase<armclass>::LevelBase(name, title) {
  AllocateVectors();
}

template <typename armclass>
Level3<armclass>::~Level3() {}

template <typename armclass>
void Level3<armclass>::AllocateVectors() {
  UT::AllocateVector2D(fPhotonPosition, this->kCalNtower, this->kPosNview);
  UT::AllocateVector3D(fPhotonPositionMH, this->kCalNtower, this->kPosNview, this->kMaxNparticle);
  UT::AllocateVector2D(fNeutronPosition, this->kCalNtower, this->kPosNview);
  UT::AllocateVector4D(fPhotonPosFitPar, this->kCalNtower, this->kPosNlayer, this->kPosNview,
                       this->kMaxNparticle * this->kPosNpars);
  UT::AllocateVector5D(fPhotonPosFitErr, this->kCalNtower, this->kPosNlayer, this->kPosNview,
                       this->kMaxNparticle * this->kPosNpars, 2);
  UT::AllocateVector3D(fPhotonPosFitChi2, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector3D(fPhotonPosFitNDF, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector3D(fPhotonPosFitEDM, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector1D(fPhotonMultiHit, this->kCalNtower);
  UT::AllocateVector4D(fNeutronPosFitPar, this->kCalNtower, this->kPosNlayer, this->kPosNview,
                       this->kMaxNparticle * this->kPosNpars);
  UT::AllocateVector5D(fNeutronPosFitErr, this->kCalNtower, this->kPosNlayer, this->kPosNview,
                       this->kMaxNparticle * this->kPosNpars, 2);
  UT::AllocateVector3D(fNeutronPosFitChi2, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector3D(fNeutronPosFitNDF, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector3D(fNeutronPosFitEDM, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector1D(fNeutronMultiHit, this->kCalNtower);
  UT::AllocateVector2D(fPosMaxLayer, this->kCalNtower, this->kPosNview);
  UT::AllocateVector4D(fSilSaturated, this->kCalNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);

  UT::AllocateVector1D(fPhotonEnergy, this->kCalNtower);
  UT::AllocateVector1D(fPhotonEnergyLin, this->kCalNtower);
  UT::AllocateVector2D(fPhotonEnergyMH, this->kCalNtower, this->kMaxNparticle);
  UT::AllocateVector1D(fNeutronEnergy, this->kCalNtower);

  UT::AllocateVector1D(fPhotonL20, this->kCalNtower);
  UT::AllocateVector1D(fPhotonL90, this->kCalNtower);
  UT::AllocateVector1D(fNeutronL20, this->kCalNtower);
  UT::AllocateVector1D(fNeutronL90, this->kCalNtower);
  UT::AllocateVector1D(fIsPhoton, this->kCalNtower);
  UT::AllocateVector1D(fIsNeutron, this->kCalNtower);

  UT::AllocateVector1D(fSoftwareTrigger, this->kCalNtower);

  // For Elena : Need to define a Double_t variable with the following dimensions
  // UT::AllocateVector4D(fSiliconDeposit, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview);
  // UT::AllocateVector4D(fSiliconArea, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview);
  // grep one of the other variables (fSoftwareTrigger) and copy the relevant lines for the new variables

  // Eugenio
  UT::AllocateVector2D(fMultiParticleSiID, this->kCalNtower, this->kMaxNparticle);
  UT::AllocateVector4D(fMultiParticleSiPos, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector4D(fMultiParticleSiDep, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector3D(fMultiParticleCaldE, this->kCalNtower, this->kMaxNparticle, this->kCalNlayer);
  //
  UT::AllocateVector3D(fShowerWidth, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  UT::AllocateVector1D(fShowerWidthTotal, this->kCalNtower);
  //
  UT::AllocateVector1D(fFirstInteraction, this->kCalNtower);
  //
  UT::AllocateVector3D(fSiliconCharge, this->kCalNtower, this->kPosNlayer, this->kPosNview);
}

/////////////////////////////////////////////////////
/// @brief  Clear all data of Level3 and TNamed. (No recomendation to use)
/// @tparam armclass
/// @param option [in] Selection of data. \n
///                    option: default = "" ("" is replaced to "all" in this function.) \n
///                    For details, refer DataClear().

template <typename armclass>
void Level3<armclass>::Clear(Option_t *option) {
  TNamed::Clear(option);  // Clear all parameters of TNamed like name, title also
  TString opt = option;
  if (opt == "")
    DataClear("all");
  else
    DataClear(option);
  return;
}

/////////////////////////////////////////////////////
/// @brief  Clear only data of Level3.
/// @tparam armclass
/// @param option [in] Selection of data. \n
///                    option: default = "all" \n
///                    all = all of the followings \n
///                    header, photon_energy, photon_pos, photon_pid, photon_trigger, \n
///                    neutron_energy, neutron_pos, neutron_pid, neutron_trigger, \n
///                    common, and pi0_all.
///                    (photon(neutron)_all = _energy + _pos + _pid + _trigger) \n
/// @details An example of Clear with specifying data : Clear(d, "calorimeter, posdep, tdc")

template <typename armclass>
void Level3<armclass>::DataClear(Option_t *option) {
  LevelBase<armclass>::DataClear(option);

  TString opt = option;
  opt.ToLower();

  // Clear Photon Position reconstructed results
  if (opt.Contains("all") || opt.Contains("photon_all") || opt.Contains("photon_pos")) {
    UT::ClearVector2D(fPhotonPosition, -1.);
    UT::ClearVector3D(fPhotonPositionMH, -1.);
    UT::ClearVector4D(fPhotonPosFitPar, -1.);
    UT::ClearVector5D(fPhotonPosFitErr, -1.);
    UT::ClearVector3D(fPhotonPosFitChi2, -1.);
    UT::ClearVector3D(fPhotonPosFitNDF, -1.);
    UT::ClearVector3D(fPhotonPosFitEDM, -1.);
    UT::ClearVector1D(fPhotonMultiHit, false);
    fPhotonFuncType = FuncType::kOrgMultiHitFCN;
  }

  // Clear Photon PID result
  if (opt.Contains("all") || opt.Contains("photon_all") || opt.Contains("photon_pid")) {
    UT::ClearVector1D(fPhotonL20, -1.);
    UT::ClearVector1D(fPhotonL90, -1.);
    UT::ClearVector1D(fIsPhoton, false);
  }

  // Clear Photon Energy result
  if (opt.Contains("all") || opt.Contains("photon_all") || opt.Contains("photon_energy")) {
    UT::ClearVector1D(fPhotonEnergy, -1.);
    UT::ClearVector1D(fPhotonEnergyLin, -1.);
    UT::ClearVector2D(fPhotonEnergyMH, -1.);
  }

  // Clear Neutron Position reconstructed results
  if (opt.Contains("all") || opt.Contains("neutron_all") || opt.Contains("neutron_pos")) {
    UT::ClearVector2D(fNeutronPosition, -1.);
    UT::ClearVector4D(fNeutronPosFitPar, -1.);
    UT::ClearVector5D(fNeutronPosFitErr, -1.);
    UT::ClearVector3D(fNeutronPosFitChi2, -1.);
    UT::ClearVector3D(fNeutronPosFitNDF, -1.);
    UT::ClearVector3D(fNeutronPosFitEDM, -1.);
    UT::ClearVector1D(fNeutronMultiHit, false);
    fNeutronFuncType = FuncType::kOrgMultiHitFCN;
  }

  // Clear Neutron PID result
  if (opt.Contains("all") || opt.Contains("neutron_all") || opt.Contains("neutron_pid")) {
    UT::ClearVector1D(fNeutronL20, -1.);
    UT::ClearVector1D(fNeutronL90, -1.);
    UT::ClearVector1D(fIsNeutron, false);
  }

  // Clear Neutron Energy result
  if (opt.Contains("all") || opt.Contains("neutron_all") || opt.Contains("neutron_energy")) {
    UT::ClearVector1D(fNeutronEnergy, -1.);
  }

  // Clear Common valuables for photon and neutron analyses
  if (opt.Contains("all") || opt.Contains("common")) {
    UT::ClearVector2D(fPosMaxLayer, -1);
    UT::ClearVector4D(fSilSaturated, false);
    UT::ClearVector1D(fSoftwareTrigger, false);
  }

  // Clear Development variables
  if (opt.Contains("development")) {                   // Eugenio
    UT::ClearVector2D(fMultiParticleSiID, (UInt_t)0);  // Without cast, it may fails depending on the compiler error
    UT::ClearVector4D(fMultiParticleSiPos, -1.);
    UT::ClearVector4D(fMultiParticleSiDep, -1.);
    UT::ClearVector3D(fMultiParticleCaldE, -1.);
    //
    UT::ClearVector1D(fShowerWidthTotal, 0.);
    UT::ClearVector3D(fShowerWidth, 0.);
    //
    UT::ClearVector1D(fFirstInteraction, 1);
    //
    UT::ClearVector3D(fSiliconCharge, 0.);
  }

  return;
}

/////////////////////////////////////////////////////
/// @brief  Copy all data of Level3 and TNamed. (No recomendation to use)
/// @tparam armclass
/// @param d      [in] Copy from d to this
/// @param option [in] Selection of data. \n
///                    option: default = "" ("" is replaced to "all" in this function.) \n
///                    For details, refer DataClear().

template <typename armclass>
void Level3<armclass>::Copy(Level3 *d, Option_t *option) {
  // Clear all parameters of TNamed like name, title also
  TNamed::Copy(*d);

  TString opt = option;
  if (opt == "")
    DataCopy(d, "all");
  else
    DataCopy(d, option);
  return;
}

/////////////////////////////////////////////////////
/// @brief  Copy data from d to this
/// @tparam armclass
/// @param d      [in] Copy from d to this
/// @param option [in] Selection of data. \n
///                    option: all = all of the followings \n
///                    header, photon_energy, photon_pos, photon_pid, photon_trigger, \n
///                    neutron_energy, neutron_pos, neutron_pid, neutron_trigger and \n
///                    common.
///                    (photon(neutron)_all = _energy + _pos + _pid + _trigger
/// @details An example of DataCopy with specifying data : DataCopy(d, "calorimeter, posdep, tdc")

template <typename armclass>
void Level3<armclass>::DataCopy(Level3 *d, Option_t *option) {
  LevelBase<armclass>::DataCopy(d, option);

  TString opt = option;
  opt.ToLower();

  // Copy Photon Position reconstructed results
  if (opt.Contains("all") || opt.Contains("photon_all") || opt.Contains("photon_pos")) {
    UT::CopyVector2D(fPhotonPosition, d->fPhotonPosition);
    UT::CopyVector3D(fPhotonPositionMH, d->fPhotonPositionMH);
    UT::CopyVector4D(fPhotonPosFitPar, d->fPhotonPosFitPar);
    UT::CopyVector5D(fPhotonPosFitErr, d->fPhotonPosFitErr);
    UT::CopyVector3D(fPhotonPosFitChi2, d->fPhotonPosFitChi2);
    UT::CopyVector3D(fPhotonPosFitNDF, d->fPhotonPosFitNDF);
    UT::CopyVector3D(fPhotonPosFitEDM, d->fPhotonPosFitEDM);
    UT::CopyVector1D(fPhotonMultiHit, d->fPhotonMultiHit);
    fPhotonFuncType = d->fPhotonFuncType;
  }

  // Copy Photon PID result
  if (opt.Contains("all") || opt.Contains("photon_all") || opt.Contains("photon_pid")) {
    UT::CopyVector1D(fPhotonL20, d->fPhotonL20);
    UT::CopyVector1D(fPhotonL90, d->fPhotonL90);
    UT::CopyVector1D(fIsPhoton, d->fIsPhoton);
  }

  // Copy Photon Energy result
  if (opt.Contains("all") || opt.Contains("photon_all") || opt.Contains("photon_energy")) {
    UT::CopyVector1D(fPhotonEnergy, d->fPhotonEnergy);
    UT::CopyVector1D(fPhotonEnergyLin, d->fPhotonEnergyLin);
    UT::CopyVector2D(fPhotonEnergyMH, d->fPhotonEnergyMH);
  }

  // Copy Neutron Position reconstructed results
  if (opt.Contains("all") || opt.Contains("neutron_all") || opt.Contains("neutron_pos")) {
    UT::CopyVector2D(fNeutronPosition, d->fNeutronPosition);
    UT::CopyVector4D(fNeutronPosFitPar, d->fNeutronPosFitPar);
    UT::CopyVector5D(fNeutronPosFitErr, d->fNeutronPosFitErr);
    UT::CopyVector3D(fNeutronPosFitChi2, d->fNeutronPosFitChi2);
    UT::CopyVector3D(fNeutronPosFitNDF, d->fNeutronPosFitNDF);
    UT::CopyVector3D(fNeutronPosFitEDM, d->fNeutronPosFitEDM);
    UT::CopyVector1D(fNeutronMultiHit, d->fNeutronMultiHit);
    fNeutronFuncType = d->fNeutronFuncType;
  }

  // Copy Neutron PID result
  if (opt.Contains("all") || opt.Contains("neutron_all") || opt.Contains("neutron_pid")) {
    UT::CopyVector1D(fNeutronL20, d->fNeutronL20);
    UT::CopyVector1D(fNeutronL90, d->fNeutronL90);
    UT::CopyVector1D(fIsNeutron, d->fIsNeutron);
  }

  // Copy Neutron Energy result
  if (opt.Contains("all") || opt.Contains("neutron_all") || opt.Contains("neutron_energy")) {
    UT::CopyVector1D(fNeutronEnergy, d->fNeutronEnergy);
  }

  // Copy Common valuables for photon and neutron analyses
  if (opt.Contains("all") || opt.Contains("common")) {
    UT::CopyVector2D(fPosMaxLayer, d->fPosMaxLayer);
    UT::CopyVector4D(fSilSaturated, d->fSilSaturated);
    UT::CopyVector1D(fSoftwareTrigger, d->fSoftwareTrigger);
  }

  // Copy Development variables
  if (opt.Contains("development")) {  // Eugenio
    UT::CopyVector2D(fMultiParticleSiID, d->fMultiParticleSiID);
    UT::CopyVector4D(fMultiParticleSiPos, d->fMultiParticleSiPos);
    UT::CopyVector4D(fMultiParticleSiDep, d->fMultiParticleSiDep);
    UT::CopyVector3D(fMultiParticleCaldE, d->fMultiParticleCaldE);
    //
    UT::CopyVector1D(fShowerWidthTotal, d->fShowerWidthTotal);
    UT::CopyVector3D(fShowerWidth, d->fShowerWidth);
    //
    UT::CopyVector1D(fFirstInteraction, d->fFirstInteraction);
    //
    UT::CopyVector3D(fSiliconCharge, d->fSiliconCharge);
  }

  // Skip the implementation for pi0's until the final decision of the structure.

  return;
}

template <typename armclass>
Int_t Level3<armclass>::GetPhotonNhits(Int_t tower, Int_t layer, Int_t view) {
  Int_t nhits = 0;
  Int_t nmax = fPhotonPosFitPar[tower][layer][view].size()/this->kPosNpars;
  for(int i=0; i<nmax; i++) {
    Bool_t check=false; // check if the value is initial one.
    for(int j=0; j<this->kPosNpars; j++) {
      if(TMath::Abs(fPhotonPosFitPar[tower][layer][view][this->kPosNpars*i+1] + 1.) > 0.001) {
        check=true;
        break;
      }
    }
    if(check) nhits++;
  }
  return nhits;
}

template <typename armclass>
Int_t Level3<armclass>::GetNeutronNhits(Int_t tower, Int_t layer, Int_t view) {
  Int_t nhits = 0;
  Int_t nmax = fNeutronPosFitPar[tower][layer][view].size()/this->kPosNpars;
  for(int i=0; i<nmax; i++) {
    Bool_t check=false; // check if the value is initial one.
    for(int j=0; j<this->kPosNpars; j++) {
      if(TMath::Abs(fNeutronPosFitPar[tower][layer][view][this->kPosNpars*i+1] + 1.) > 0.001) {
        check=true;
        break;
      }
    }
    if(check) nhits++;
  }
  return nhits;
}

template <typename armclass>
Double_t Level3<armclass>::PhotonHeight(Int_t tower, Int_t layer, Int_t view, Double_t pos, FuncType type) {
  if (type == FuncType::kNoneHitFCN) type = this->GetPhotonFuncType();
  return PosFitBaseFCN::GetTotalHeight<armclass>(fPhotonPosFitPar[tower][layer][view], pos, type);
}

template <typename armclass>
Double_t Level3<armclass>::PhotonPositionMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type) {
  if (type == FuncType::kNoneHitFCN) type = this->GetPhotonFuncType();
  Double_t x = PosFitBaseFCN::GetPos<armclass>(fPhotonPosFitPar[tower][layer][0], part, type);
  Double_t y = PosFitBaseFCN::GetPos<armclass>(fPhotonPosFitPar[tower][layer][1], part, type);
  TVector3 cal_pos = CT::ChannelToCalorimeter(this->kArmIndex, tower, layer, layer, x, y);
  if (view == 0)
    return cal_pos.X();
  else
    return cal_pos.Y();
}

template <typename armclass>
Double_t Level3<armclass>::PhotonAreaMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type) {
  if (type == FuncType::kNoneHitFCN) type = this->GetPhotonFuncType();
  return PosFitBaseFCN::GetArea<armclass>(fPhotonPosFitPar[tower][layer][view], part, type);
}

template <typename armclass>
Double_t Level3<armclass>::PhotonHeightMH(Int_t tower, Int_t layer, Int_t view, Int_t part, Double_t pos,
                                          FuncType type) {
  // return the peak height of the single-hit function. (-1 means the peak height )
  if (type == FuncType::kNoneHitFCN) type = this->GetPhotonFuncType();
  return PosFitBaseFCN::GetHeight<armclass>(fPhotonPosFitPar[tower][layer][view], part, pos, type);
}

template <typename armclass>
Double_t Level3<armclass>::NeutronPositionMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type) {
  if (type == FuncType::kNoneHitFCN) type = this->GetNeutronFuncType();
  Double_t x = PosFitBaseFCN::GetPos<armclass>(fNeutronPosFitPar[tower][layer][0], part, type);
  Double_t y = PosFitBaseFCN::GetPos<armclass>(fNeutronPosFitPar[tower][layer][1], part, type);
  TVector3 cal_pos = CT::ChannelToCalorimeter(this->kArmIndex, tower, layer, layer, x, y);
  if (view == 0)
    return cal_pos.X();
  else
    return cal_pos.Y();
}

template <typename armclass>
Double_t Level3<armclass>::NeutronAreaMH(Int_t tower, Int_t layer, Int_t view, Int_t part, FuncType type) {
  if (type == FuncType::kNoneHitFCN) type = this->GetNeutronFuncType();
  return PosFitBaseFCN::GetArea<armclass>(fNeutronPosFitPar[tower][layer][view], part, type);
}

template <typename armclass>
Int_t Level3<armclass>::GetMaxLayerMH(Int_t tower, Int_t view) {
  Int_t layer = fPosMaxLayer[tower][view];
  PosFitBaseFCN *func = PosFitBaseFCN::CreateInstance<armclass>(fPhotonFuncType);
  if (func->GetPos(fPhotonPosFitPar[tower][layer][view], 1) < 0.) {  // no multi-hit in max layer
    layer = -1;
    for (Int_t il = 0; il < this->kPosNlayerEM; ++il) {  // search it in other layer
      if (func->GetPos(fPhotonPosFitPar[tower][il][view], 1) >= 0.) {
        layer = il;
        break;
      }
    }
  }
  delete func;
  return layer;
}

template <typename armclass>
void Level3<armclass>::Print(Option_t *option) {
  UT::Printf(UT::kPrintInfo, "--- ARM%d Level3 data ---\n", this->kArmIndex + 1);

  LevelBase<armclass>::Print();

  TString opt = option;
  opt.ToLower();
  if (opt.Contains("photon")) {
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      if (it == 0)
        UT::Printf(UT::kPrintInfo, "\tPhoton - small tower\n");
      else
        UT::Printf(UT::kPrintInfo, "\tPhoton - large tower\n");
      UT::Printf(UT::kPrintInfo, "\t\tenergy = %.1lf\n", fPhotonEnergyLin[it]);
      UT::Printf(UT::kPrintInfo, "\t\t(x,y)  = %.1lf, %.1lf\n", fPhotonPosition[it][0], fPhotonPosition[it][1]);
      UT::Printf(UT::kPrintInfo, "\t\tenergy 1st = %.1lf\n", fPhotonEnergyMH[it][0]);
      UT::Printf(UT::kPrintInfo, "\t\tenergy 2nd = %.1lf\n", fPhotonEnergyMH[it][1]);
      UT::Printf(UT::kPrintInfo, "\t\t(x,y) 1st  = %.1lf, %.1lf\n", fPhotonPositionMH[it][0][0],
                 fPhotonPositionMH[it][1][0]);
      UT::Printf(UT::kPrintInfo, "\t\t(x,y) 2nd  = %.1lf, %.1lf\n", fPhotonPositionMH[it][0][1],
                 fPhotonPositionMH[it][1][1]);
      UT::Printf(UT::kPrintInfo, "\t\t(x,y) 3rd  = %.1lf, %.1lf\n", fPhotonPositionMH[it][0][2],
                 fPhotonPositionMH[it][1][2]);
      UT::Printf(UT::kPrintInfo, "\t\tL90%%   = %.1lf\n", fPhotonL90[it]);
      if (fIsPhoton[it])
        UT::Printf(UT::kPrintInfo, "\t\tphoton-like\n");
      else
        UT::Printf(UT::kPrintInfo, "\t\thadron-like\n");
      if (fPhotonMultiHit[it])
        UT::Printf(UT::kPrintInfo, "\t\tmulti-hit\n");
      else
        UT::Printf(UT::kPrintInfo, "\t\tsingle-hit\n");
      Double_t dist = TMath::Sqrt(TMath::Power(fPhotonPositionMH[it][0][0] - fPhotonPositionMH[it][0][1], 2.) +
                                  TMath::Power(fPhotonPositionMH[it][1][0] - fPhotonPositionMH[it][1][1], 2.));
      Double_t mgg =
          TMath::Sqrt(fPhotonEnergyMH[it][0] * fPhotonEnergyMH[it][1]) * dist / (CT::kDetectorZpos + CT::kLHCftolayer0);
      UT::Printf(UT::kPrintInfo, "\t\tM   = %.1lf\n", mgg * 1000.);
    }
  }
  if (opt.Contains("neutron")) {
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      if (it == 0)
        UT::Printf(UT::kPrintInfo, "\tNeutron - small tower\n");
      else
        UT::Printf(UT::kPrintInfo, "\tNeutron - large tower\n");
      UT::Printf(UT::kPrintInfo, "\t\tenergy = %.1lf\n", fNeutronEnergy[it]);
      UT::Printf(UT::kPrintInfo, "\t\t(x,y)  = %.1lf, %.1lf\n", fNeutronPosition[it][0], fNeutronPosition[it][1]);
      UT::Printf(UT::kPrintInfo, "\t\tL2D    = %.1lf\n", fNeutronL90[it] - 0.25 * fNeutronL20[it]);
      if (fIsNeutron[it])
        UT::Printf(UT::kPrintInfo, "\t\tphoton-like\n");
      else
        UT::Printf(UT::kPrintInfo, "\t\thadron-like\n");
      if (fNeutronMultiHit[it])
        UT::Printf(UT::kPrintInfo, "\t\tmulti-hit\n");
      else
        UT::Printf(UT::kPrintInfo, "\t\tsingle-hit\n");
    }
  }
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class Level3<Arm1RecPars>;
template class Level3<Arm2RecPars>;
}  // namespace nLHCf
