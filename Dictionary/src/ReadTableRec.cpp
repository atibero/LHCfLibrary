#include "ReadTableRec.hh"

#include <TROOT.h>

#include <fstream>

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(ReadTableRec);
#endif

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armrec>
ReadTableRec<armrec>::ReadTableRec() {
  AllocateVectors();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armrec>
ReadTableRec<armrec>::~ReadTableRec() {}

template <typename armrec>
void ReadTableRec<armrec>::AllocateVectors() {
  UT::AllocateVector2D(fPhotonLeakEffMap, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector1D(fPhotonLeakInMap, this->kCalNtower);
  for (Int_t it = 0; it < this->kCalNtower; it++)
    UT::AllocateVector3D(fPhotonLeakInMap[it], this->kLeakBins[it], this->kLeakBins[it], this->kLeakInNpar);
  UT::AllocateVector1D(fNeutronLeakMap, this->kCalNtower);
  UT::AllocateVector1D(fNeutronLeakInMap, this->kCalNtower);
  for (Int_t it = 0; it < this->kCalNtower; it++)
    UT::AllocateVector3D(fNeutronLeakInMap[it], this->kLeakBins[it], this->kLeakBins[it], this->kLeakInNpar);
  UT::AllocateVector2D(fNeutronEffMap, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector2D(fPhotonEconvPars, this->kCalNtower, this->kEconvNpar);
  UT::AllocateVector2D(fNeutronEconvPars, this->kCalNtower, this->kEconvNpar);

  UT::AllocateVector1D(fMaskPosDet, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    UT::AllocateVector4D(fMaskPosDet[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it], this->kPosNsample);
}

/*-----------------------*/
/*--- Read parameters ---*/
/*-----------------------*/
template <typename armrec>
void ReadTableRec<armrec>::ReadTables() {
  UT::Printf(UT::kPrintInfo, "Reading Arm%d reconstruction tables from %s\n", this->kArmIndex + 1,
             this->kTableDirectory.Data());

  SetLeakageMap();
  UT::Printf(UT::kPrintDebug, "\n finish to set leakage map");
  SetEfficiencyMap();
  UT::Printf(UT::kPrintDebug, "\n finish to set efficiency map");
  SetEconvFunction();
  UT::Printf(UT::kPrintDebug, "\n finish to set Econv function");
  SetDeadChannels();
  UT::Printf(UT::kPrintDebug, "\n finish to set dead channels");
}

/*-------------------------------------*/
/*--- Set reconstruction parameters ---*/
/*-------------------------------------*/

/*** Load leakaga/efficiency map ***/
template <typename armrec>
void ReadTableRec<armrec>::SetLeakageMap() {
  /*--- Leakage In ---*/

  /* Photons */
  TFile p_input_file((this->kTableDirectory + "/" + this->fPhotonLeakageFile).Data(), "READ");
  if (p_input_file.IsZombie()) {
    UT::Printf(UT::kPrintError, "File does not exist : %s, \n",
               (this->kTableDirectory + "/" + this->fPhotonLeakageFile).Data());
    exit(-1);
  }
  gROOT->cd();

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      TH2F *leakmap;
      leakmap = (TH2F *)p_input_file.Get(Form("leakage_map_%d_%02d", it, il));
      if (!leakmap) {
        UT::Printf(UT::kPrintError, "histogram could not be fined : %s, \n", Form("leakage_map_%d_%02d", it, il));
        exit(-1);
      }
      fPhotonLeakEffMap[it][il] =
          (TH2F *)leakmap->Clone(Form("photon_leak_map_a%d_%d_%02d", this->kArmIndex + 1, it, il));
      // The class of Arm1 efficiency map is TH2D, not TH2F. GetObject does not work (cast is required)
      // p_input_file.GetObject(Form("leakage_map_%d_%02d", it, il), leakmap);
      // p_input_file.GetObject(Form("leakage_map_%d_%02d", it, il), fPhotonLeakEffMap[it][il]);
      // fPhotonLeakEffMap[it][il]->SetName(Form("photon_leak_map_a%d_%d_%02d", this->kArmIndex+1, it, il));
    }  // layer loop
  }    // tower loop
  p_input_file.Close();

  /* Neutrons */
  TFile n_input_file((this->kTableDirectory + "/" + this->fNeutronLeakageFile).Data(), "READ");
  if (n_input_file.IsZombie()) {
    UT::Printf(UT::kPrintError, "File does not exist : %s, \n",
               (this->kTableDirectory + "/" + this->fNeutronLeakageFile).Data());
    exit(-1);
  }
  gROOT->cd();

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    TH2F *leakmap;
    leakmap = (TH2F *)n_input_file.Get(Form("leakage_map_%d", it));
    if (!leakmap) {
      UT::Printf(UT::kPrintError, "histogram could not be fined : %s, \n", Form("leakage_map_%d", it));
      exit(-1);
    }
    fNeutronLeakMap[it] = (TH2F *)leakmap->Clone(Form("neutron_leak_map_a%d_%d", this->kArmIndex + 1, it));
  }  // tower loop

  n_input_file.Close();

  /*--- Leakage In ---*/
  /* Photons */
  ifstream pfile((this->kTableDirectory + "/" + this->fPhotonLeakInFile).Data(), ifstream::in);

  while (pfile.good()) {
    const Double_t big_neg_num = -1E30;
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, ix, iy;
    tower = ix = iy = -1;
    Double_t a, b, c;
    a = b = c = big_neg_num;
    pfile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %d %*s %*s %*s %lf %lf %lf", &tower, &ix, &iy, &a, &b, &c);
    if (tower >= 0 && tower < this->kCalNtower && a > big_neg_num && b > big_neg_num && c > big_neg_num)
      if (ix >= 0 && ix < this->kLeakBins[tower] && iy >= 0 && iy < this->kLeakBins[tower]) {
        fPhotonLeakInMap[tower][ix][iy][0] = a;
        fPhotonLeakInMap[tower][ix][iy][1] = b;
        fPhotonLeakInMap[tower][ix][iy][2] = c;
      }
  }
  pfile.close();

  /* Neutrons */
  ifstream nfile((this->kTableDirectory + "/" + this->fNeutronLeakInFile).Data(), ifstream::in);

  while (nfile.good()) {
    const Double_t big_neg_num = -1E30;
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, ix, iy;
    tower = ix = iy = -1;
    Double_t a, b, c;
    a = b = c = big_neg_num;
    nfile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %d %d %*s %*s %*s %lf %lf %lf", &tower, &ix, &iy, &a, &b, &c);
    if (tower >= 0 && tower < this->kCalNtower && a > big_neg_num && b > big_neg_num && c > big_neg_num)
      if (ix >= 0 && ix < this->kLeakBins[tower] && iy >= 0 && iy < this->kLeakBins[tower]) {
        fNeutronLeakInMap[tower][ix][iy][0] = a;
        fNeutronLeakInMap[tower][ix][iy][1] = b;
        fNeutronLeakInMap[tower][ix][iy][2] = c;
      }
  }
  nfile.close();

  // for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //   for (Int_t ip = 0; ip < this->kEconvNpar; ++ip) {
  //     if (fPhotonLeakInMap[it][ip] <= big_neg_num) {
  // 	UT::Printf(UT::kPrintError, "SetLeakageMap (leak-in): missing parameter %d in tower %d of Arm%d\n", ip, it,
  // this->kArmIndex+1); 	exit(EXIT_FAILURE);
  //     }
  //   }
  // }
}

template <typename armrec>
void ReadTableRec<armrec>::SetEfficiencyMap() {
  /* Photons */
  // already included in leakage maps!

  /* Neutrons */
  TString file_name = this->fNeutronEffFile;
  TFile input_file((this->kTableDirectory + "/" + file_name).Data(), "READ");
  if (input_file.IsZombie()) {
    UT::Printf(UT::kPrintError, "File does not exist : %s, \n", (this->kTableDirectory + "/" + file_name).Data());
    exit(-1);
  }
  gROOT->cd();

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      TH2F *effmap;
      if (this->kArmIndex == 0) {
        effmap = (TH2F *)input_file.Get(Form("efficiency_map_%d_%02d", it, il));
      } else {
        effmap = (TH2F *)input_file.Get(Form("leakage_map_%d_%02d", it, il));
      }
      fNeutronEffMap[it][il] = (TH2F *)effmap->Clone(Form("neutron_eff_map_a%d_%d_%02d", this->kArmIndex + 1, it, il));
    }  // layer loop
  }    // tower loop
  input_file.Close();
}

/*** Load Sumde -> Energy conversion parameters ***/
template <typename armrec>
void ReadTableRec<armrec>::SetEconvFunction() {
  const Double_t big_neg_num = -1E30;

  /* Photons */
  ifstream pfile((this->kTableDirectory + "/" + this->fPhotonEconvFile).Data(), ifstream::in);

  while (pfile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower = -1;
    Double_t a, b, c;
    a = b = c = big_neg_num;
    pfile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %lf %lf %lf", &tower, &a, &b, &c);
    if (tower >= 0 && tower < this->kCalNtower && a > big_neg_num && b > big_neg_num && c > big_neg_num) {
      fPhotonEconvPars[tower][0] = a;
      fPhotonEconvPars[tower][1] = b;
      fPhotonEconvPars[tower][2] = c;
    } else {
      // UT::Printf(UT::kPrintError, "SetEconvFunction: error when reading line: \"%s\"\n", buf);
      // exit(EXIT_FAILURE);
    }
  }
  pfile.close();

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t ip = 0; ip < this->kEconvNpar; ++ip) {
      if (fPhotonEconvPars[it][ip] <= big_neg_num) {
        UT::Printf(UT::kPrintError, "SetEconvFunction (photon): missing parameter %d in tower %d of Arm%d\n", ip, it,
                   this->kArmIndex + 1);
        exit(EXIT_FAILURE);
      }
    }
  }

  /* Neutrons */
  ifstream nfile((this->kTableDirectory + "/" + this->fNeutronEconvFile).Data(), ifstream::in);

  while (nfile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower = -1;
    Double_t a, b, c;
    a = b = c = big_neg_num;
    nfile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    sscanf(buf, "%d %lf %lf %lf", &tower, &a, &b, &c);
    if (tower >= 0 && tower < this->kCalNtower && a > big_neg_num && b > big_neg_num && c > big_neg_num) {
      fNeutronEconvPars[tower][0] = a;
      fNeutronEconvPars[tower][1] = b;
      fNeutronEconvPars[tower][2] = c;
    } else {
      // UT::Printf(UT::kPrintError, "SetEconvFunction: error when reading line: \"%s\"\n", buf);
      // exit(EXIT_FAILURE);
    }
  }
  nfile.close();

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t ip = 0; ip < this->kEconvNpar; ++ip)
      if (fNeutronEconvPars[it][ip] <= big_neg_num) {
        UT::Printf(UT::kPrintError, "SetEconvFunction (neutron): missing parameter %d in tower %d of Arm%d\n", ip, it,
                   this->kArmIndex + 1);
        exit(EXIT_FAILURE);
      }
}

template <typename armrec>
void ReadTableRec<armrec>::SetDeadChannels() {
  ifstream infile((this->kTableDirectory + "/" + this->fPosDeadChFile).Data(), ifstream::in);

  UT::ClearVector5D(fMaskPosDet, true);

  while (infile.good()) {
    const Int_t buf_size = 1024;
    Char_t buf[buf_size];
    Int_t tower, layer, view, channel, sample;

    infile.getline(buf, buf_size);
    if (buf[0] == '#') continue;
    if (this->kArmIndex == 0) {  // Arm1
      sample = 0;
      sscanf(buf, "%d %d %d %d", &tower, &layer, &view, &channel);
    } else {  // Arm2
      tower = 0;
      sscanf(buf, "%d %d %d %d", &sample, &layer, &view, &channel);
    }
    if (tower >= 0 && tower < this->kPosNtower && layer >= 0 && layer < this->kPosNlayer && view >= 0 &&
        view < this->kPosNview && sample >= 0 && sample < this->kPosNsample)
      if (channel >= 0 && channel < this->kPosNchannel[tower]) {
        fMaskPosDet[tower][layer][view][channel][sample] = false;
      }
  }

  infile.close();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class ReadTableRec<Arm1RecPars>;
template class ReadTableRec<Arm2RecPars>;
}  // namespace nLHCf
