#include "Arm1Params.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm1Params);
#endif

typedef Utils UT;

Int_t Arm1Params::kArmIndex = 0;  // Arm1=0, Arm2=1

/* Operation type */
#ifdef LHC2025
OpType Arm1Params::fOperation = kLHC2025;
#elif defined(LHC2022)
OpType Arm1Params::fOperation = kLHC2022;
#elif defined(SPS2022)
OpType Arm1Params::fOperation = kSPS2022;
#elif defined(SPS2021)
OpType Arm1Params::fOperation = kSPS2021;
#elif defined(SPS2015)
OpType Arm1Params::fOperation = kSPS2015;
#else  // LHC2015
OpType Arm1Params::fOperation = kLHC2015;
#endif

/* Tables directory */
TString Arm1Params::kTableDirectory = "../Tables/Op2015";

/* Calorimeters constants */
Int_t Arm1Params::kCalNtower = 2;
Int_t Arm1Params::kCalNlayer = 16;
Int_t Arm1Params::kCalNrange = 2;

Double_t Arm1Params::kSampleStep = 2.;    // single sample step [X0]
Int_t Arm1Params::kDoubleStepLayer = 11;  // first layer with double sample step

/* Position detectors constants */
Int_t Arm1Params::kPosNsample = 1;
Int_t Arm1Params::kPosNtower = 2;
Int_t Arm1Params::kPosNlayer = 4;
Int_t Arm1Params::kPosNview = 2;
Int_t Arm1Params::kPosNchannel[] = {20, 40};
Double_t Arm1Params::kPosPitch = 1.;
Double_t Arm1Params::kTowerSize[] = {20., 40.};

/* Front counters constants */
Int_t Arm1Params::kFcNlayer = 4;
Int_t Arm1Params::kFcNrange = 2;

/* ZDC constants */
Int_t Arm1Params::kZdcNchannel = 3;
Int_t Arm1Params::kZdcNrange = 2;

/* Level0 constants */
Int_t Arm1Params::kNflag;
Int_t Arm1Params::kNevFlag;
Int_t Arm1Params::kOpenAdcNch;
Int_t Arm1Params::kLaserNch;
Int_t Arm1Params::kTdcNch;
Int_t Arm1Params::kTdcNhit;
Int_t Arm1Params::kScalerNch;
Int_t Arm1Params::kNcounter;
Int_t Arm1Params::kNfifo1;
Int_t Arm1Params::kNfifo2;
Int_t Arm1Params::kTime;

Char_t Arm1Params::kTdcLabel[12][10];
Char_t Arm1Params::kCounterLabel[35][12];
Char_t Arm1Params::kFlagBitLabel[32][12];

Arm1Params::ParInit::ParInit() {
  UT::Printf(UT::kPrintInfo, "Arm1Params: initialising parameters...");
  fflush(stdout);

  if (fOperation == kLHC2015) {
    UT::Printf(UT::kPrintInfo, " (LHC2015)");
    fflush(stdout);

    SetTableDirectory("../Tables/Op2015");

    kNflag = 3;
    kNevFlag = 3;
    kOpenAdcNch = 8;
    kLaserNch = 2;
    kTdcNch = 12;
    kTdcNhit = 16;
    kScalerNch = 16;
    kNcounter = 35;
    kNfifo1 = 2;
    kNfifo2 = 4;
    kTime = 2;
    strcpy(kTdcLabel[0], "CLK");
    strcpy(kTdcLabel[1], "L3T");
    strcpy(kTdcLabel[2], "BPTX1");
    strcpy(kTdcLabel[3], "BPTX2");
    strcpy(kTdcLabel[4], "Shower");
    strcpy(kTdcLabel[5], "ATLAS-L1A");
    strcpy(kTdcLabel[6], "A1-FC");
    strcpy(kTdcLabel[7], "A2-FC");
    strcpy(kTdcLabel[8], "TS-OR");
    strcpy(kTdcLabel[9], "TL-OR");
    strcpy(kTdcLabel[10], "ADC Gate");
    strcpy(kTdcLabel[11], "-");
    strcpy(kCounterLabel[0], "CLK");
    strcpy(kCounterLabel[1], "BPTX1");
    strcpy(kCounterLabel[2], "BPTX2");
    strcpy(kCounterLabel[3], "B1&B2");
    strcpy(kCounterLabel[4], "ORBIT");
    strcpy(kCounterLabel[5], "L1T");
    strcpy(kCounterLabel[6], "L1T_BE");
    strcpy(kCounterLabel[7], "STRG");
    strcpy(kCounterLabel[8], "LTRG");
    strcpy(kCounterLabel[9], "SLOGIC");
    strcpy(kCounterLabel[10], "LLOGIC");
    strcpy(kCounterLabel[11], "SHOWER");
    strcpy(kCounterLabel[12], "SHOWER_B");
    strcpy(kCounterLabel[13], "SPECIAL");
    strcpy(kCounterLabel[14], "SPECIAL_B");
    strcpy(kCounterLabel[15], "TRG_SHOWER");
    strcpy(kCounterLabel[16], "TRG_SPE");
    strcpy(kCounterLabel[17], "TRG_PEDE");
    strcpy(kCounterLabel[18], "TRG_L1T");
    strcpy(kCounterLabel[19], "L3T");
    strcpy(kCounterLabel[20], "L3T_OR");
    strcpy(kCounterLabel[21], "SHW/L1_LHCF");
    strcpy(kCounterLabel[22], "BUNCH_POS");
    strcpy(kCounterLabel[23], "BUNCH_B1");
    strcpy(kCounterLabel[24], "BUNCH_B2");
    strcpy(kCounterLabel[25], "ATLAS_CLK");
    strcpy(kCounterLabel[26], "ATLAS_L1A");
    strcpy(kCounterLabel[27], "SFC(0)");
    strcpy(kCounterLabel[28], "SFC(1)");
    strcpy(kCounterLabel[29], "SFC(2)");
    strcpy(kCounterLabel[30], "SFC(3)");
    strcpy(kCounterLabel[31], "FC_LOGIC");
    strcpy(kCounterLabel[32], "FC_TRG");
    strcpy(kCounterLabel[33], "A1SHW&A2FC");
    strcpy(kCounterLabel[34], "A1FC&A2FC");
    // Not yet
    for (int i = 0; i < 32; i++) {
      strcpy(kFlagBitLabel[i], "-");
    }
  } else if (fOperation == kSPS2015) {
    UT::Printf(UT::kPrintInfo, " (SPS2015)");
    fflush(stdout);

    SetTableDirectory("../Tables/SPS2015");

    kNflag = 3;
    kNevFlag = 3;
    kOpenAdcNch = 8;
    kLaserNch = 2;
    kTdcNch = 12;
    kTdcNhit = 16;
    kScalerNch = 16;
    kNcounter = 35;
    kNfifo1 = 2;
    kNfifo2 = 4;
    kTime = 2;
    strcpy(kTdcLabel[0], "CLK");
    strcpy(kTdcLabel[1], "L3T");
    strcpy(kTdcLabel[2], "BPTX1");
    strcpy(kTdcLabel[3], "BPTX2");
    strcpy(kTdcLabel[4], "Shower");
    strcpy(kTdcLabel[5], "ATLAS-L1A");
    strcpy(kTdcLabel[6], "A1-FC");
    strcpy(kTdcLabel[7], "A2-FC");
    strcpy(kTdcLabel[8], "TS-OR");
    strcpy(kTdcLabel[9], "TL-OR");
    strcpy(kTdcLabel[10], "ADC Gate");
    strcpy(kTdcLabel[11], "-");
    strcpy(kCounterLabel[0], "CLK");
    strcpy(kCounterLabel[1], "BPTX1");
    strcpy(kCounterLabel[2], "BPTX2");
    strcpy(kCounterLabel[3], "B1&B2");
    strcpy(kCounterLabel[4], "ORBIT");
    strcpy(kCounterLabel[5], "L1T");
    strcpy(kCounterLabel[6], "L1T_BE");
    strcpy(kCounterLabel[7], "STRG");
    strcpy(kCounterLabel[8], "LTRG");
    strcpy(kCounterLabel[9], "SLOGIC");
    strcpy(kCounterLabel[10], "LLOGIC");
    strcpy(kCounterLabel[11], "SHOWER");
    strcpy(kCounterLabel[12], "SHOWER_B");
    strcpy(kCounterLabel[13], "SPECIAL");
    strcpy(kCounterLabel[14], "SPECIAL_B");
    strcpy(kCounterLabel[15], "TRG_SHOWER");
    strcpy(kCounterLabel[16], "TRG_SPE");
    strcpy(kCounterLabel[17], "TRG_PEDE");
    strcpy(kCounterLabel[18], "TRG_L1T");
    strcpy(kCounterLabel[19], "L3T");
    strcpy(kCounterLabel[20], "L3T_OR");
    strcpy(kCounterLabel[21], "SHW/L1_LHCF");
    strcpy(kCounterLabel[22], "BUNCH_POS");
    strcpy(kCounterLabel[23], "BUNCH_B1");
    strcpy(kCounterLabel[24], "BUNCH_B2");
    strcpy(kCounterLabel[25], "ATLAS_CLK");
    strcpy(kCounterLabel[26], "ATLAS_L1A");
    strcpy(kCounterLabel[27], "SFC(0)");
    strcpy(kCounterLabel[28], "SFC(1)");
    strcpy(kCounterLabel[29], "SFC(2)");
    strcpy(kCounterLabel[30], "SFC(3)");
    strcpy(kCounterLabel[31], "FC_LOGIC");
    strcpy(kCounterLabel[32], "FC_TRG");
    strcpy(kCounterLabel[33], "A1SHW&A2FC");
    strcpy(kCounterLabel[34], "A1FC&A2FC");

    // Not yet
    for (int i = 0; i < 32; i++) {
      strcpy(kFlagBitLabel[i], "-");
    }
  } else if (fOperation == kSPS2021) {
    UT::Printf(UT::kPrintInfo, " (SPS2021)");
    fflush(stdout);

    SetTableDirectory("../Tables/SPS2021");

    kNflag = 3;
    kNevFlag = 3;
    kOpenAdcNch = 8;
    kLaserNch = 2;
    kTdcNch = 12;
    kTdcNhit = 16;
    kScalerNch = 16;
    kNcounter = 35;
    kNfifo1 = 2;
    kNfifo2 = 4;
    kTime = 2;
    strcpy(kTdcLabel[0], "CLK");
    strcpy(kTdcLabel[1], "L3T");
    strcpy(kTdcLabel[2], "BPTX1");
    strcpy(kTdcLabel[3], "BPTX2");
    strcpy(kTdcLabel[4], "Shower");
    strcpy(kTdcLabel[5], "ATLAS-L1A");
    strcpy(kTdcLabel[6], "A1-FC");
    strcpy(kTdcLabel[7], "A2-FC");
    strcpy(kTdcLabel[8], "TS-OR");
    strcpy(kTdcLabel[9], "TL-OR");
    strcpy(kTdcLabel[10], "ADC Gate");
    strcpy(kTdcLabel[11], "-");
    strcpy(kCounterLabel[0], "CLK");
    strcpy(kCounterLabel[1], "BPTX1");
    strcpy(kCounterLabel[2], "BPTX2");
    strcpy(kCounterLabel[3], "B1&B2");
    strcpy(kCounterLabel[4], "ORBIT");
    strcpy(kCounterLabel[5], "L1T");
    strcpy(kCounterLabel[6], "L1T_BE");
    strcpy(kCounterLabel[7], "STRG");
    strcpy(kCounterLabel[8], "LTRG");
    strcpy(kCounterLabel[9], "SLOGIC");
    strcpy(kCounterLabel[10], "LLOGIC");
    strcpy(kCounterLabel[11], "SHOWER");
    strcpy(kCounterLabel[12], "SHOWER_B");
    strcpy(kCounterLabel[13], "SPECIAL");
    strcpy(kCounterLabel[14], "SPECIAL_B");
    strcpy(kCounterLabel[15], "TRG_SHOWER");
    strcpy(kCounterLabel[16], "TRG_SPE");
    strcpy(kCounterLabel[17], "TRG_PEDE");
    strcpy(kCounterLabel[18], "TRG_L1T");
    strcpy(kCounterLabel[19], "L3T");
    strcpy(kCounterLabel[20], "L3T_OR");
    strcpy(kCounterLabel[21], "SHW/L1_LHCF");
    strcpy(kCounterLabel[22], "BUNCH_POS");
    strcpy(kCounterLabel[23], "BUNCH_B1");
    strcpy(kCounterLabel[24], "BUNCH_B2");
    strcpy(kCounterLabel[25], "ATLAS_CLK");
    strcpy(kCounterLabel[26], "ATLAS_L1A");
    strcpy(kCounterLabel[27], "SFC(0)");
    strcpy(kCounterLabel[28], "SFC(1)");
    strcpy(kCounterLabel[29], "SFC(2)");
    strcpy(kCounterLabel[30], "SFC(3)");
    strcpy(kCounterLabel[31], "FC_LOGIC");
    strcpy(kCounterLabel[32], "FC_TRG");
    strcpy(kCounterLabel[33], "A1SHW&A2FC");
    strcpy(kCounterLabel[34], "A1FC&A2FC");

    // Not yet
    for (int i = 0; i < 32; i++) {
      strcpy(kFlagBitLabel[i], "-");
    }
  } else if (fOperation == kSPS2022) {
    UT::Printf(UT::kPrintInfo, " (SPS2022)");
    fflush(stdout);

    SetTableDirectory("../Tables/SPS2022");

    // For Menjo-san: Some modifications are needed?
    kNflag = 3;
    kNevFlag = 3;
    kOpenAdcNch = 8;
    kLaserNch = 2;
    kTdcNch = 12;
    kTdcNhit = 16;
    kScalerNch = 16;
    kNcounter = 35;
    kNfifo1 = 2;
    kNfifo2 = 4;
    kTime = 2;
    strcpy(kTdcLabel[0], "CLK");
    strcpy(kTdcLabel[1], "L3T");
    strcpy(kTdcLabel[2], "BPTX1");
    strcpy(kTdcLabel[3], "BPTX2");
    strcpy(kTdcLabel[4], "Shower");
    strcpy(kTdcLabel[5], "ATLAS-L1A");
    strcpy(kTdcLabel[6], "A1-FC");
    strcpy(kTdcLabel[7], "A2-FC");
    strcpy(kTdcLabel[8], "TS-OR");
    strcpy(kTdcLabel[9], "TL-OR");
    strcpy(kTdcLabel[10], "ADC Gate");
    strcpy(kTdcLabel[11], "-");
    strcpy(kCounterLabel[0], "CLK");
    strcpy(kCounterLabel[1], "BPTX1");
    strcpy(kCounterLabel[2], "BPTX2");
    strcpy(kCounterLabel[3], "B1&B2");
    strcpy(kCounterLabel[4], "ORBIT");
    strcpy(kCounterLabel[5], "L1T");
    strcpy(kCounterLabel[6], "L1T_BE");
    strcpy(kCounterLabel[7], "STRG");
    strcpy(kCounterLabel[8], "LTRG");
    strcpy(kCounterLabel[9], "SLOGIC");
    strcpy(kCounterLabel[10], "LLOGIC");
    strcpy(kCounterLabel[11], "SHOWER");
    strcpy(kCounterLabel[12], "SHOWER_B");
    strcpy(kCounterLabel[13], "SPECIAL");
    strcpy(kCounterLabel[14], "SPECIAL_B");
    strcpy(kCounterLabel[15], "TRG_SHOWER");
    strcpy(kCounterLabel[16], "TRG_SPE");
    strcpy(kCounterLabel[17], "TRG_PEDE");
    strcpy(kCounterLabel[18], "TRG_L1T");
    strcpy(kCounterLabel[19], "L3T");
    strcpy(kCounterLabel[20], "L3T_OR");
    strcpy(kCounterLabel[21], "SHW/L1_LHCF");
    strcpy(kCounterLabel[22], "BUNCH_POS");
    strcpy(kCounterLabel[23], "BUNCH_B1");
    strcpy(kCounterLabel[24], "BUNCH_B2");
    strcpy(kCounterLabel[25], "ATLAS_CLK");
    strcpy(kCounterLabel[26], "ATLAS_L1A");
    strcpy(kCounterLabel[27], "SFC(0)");
    strcpy(kCounterLabel[28], "SFC(1)");
    strcpy(kCounterLabel[29], "SFC(2)");
    strcpy(kCounterLabel[30], "SFC(3)");
    strcpy(kCounterLabel[31], "FC_LOGIC");
    strcpy(kCounterLabel[32], "FC_TRG");
    strcpy(kCounterLabel[33], "A1SHW&A2FC");
    strcpy(kCounterLabel[34], "A1FC&A2FC");

    // Not yet
    for (int i = 0; i < 32; i++) {
      strcpy(kFlagBitLabel[i], "-");
    }
  } else if (fOperation == kLHC2022) {
    UT::Printf(UT::kPrintInfo, " (LHC2022)");
    fflush(stdout);

    SetTableDirectory("../Tables/Op2022");

    kNflag = 3;
    kNevFlag = 3;
    kOpenAdcNch = 8;
    kLaserNch = 2;
    kTdcNch = 12;
    kTdcNhit = 16;
    kScalerNch = 0;
    kNcounter = 18;
    kNfifo1 = 2;
    kNfifo2 = 3;
    kTime = 2;
    strcpy(kTdcLabel[0], "CLK");
    strcpy(kTdcLabel[1], "L3T");
    strcpy(kTdcLabel[2], "BPTX1");
    strcpy(kTdcLabel[3], "BPTX2");
    strcpy(kTdcLabel[4], "Shower");
    strcpy(kTdcLabel[5], "ATLAS-L1A");
    strcpy(kTdcLabel[6], "A1-FC");
    strcpy(kTdcLabel[7], "A2-FC");
    strcpy(kTdcLabel[8], "TS-OR");
    strcpy(kTdcLabel[9], "TL-OR");
    strcpy(kTdcLabel[10], "ADC Gate");
    strcpy(kTdcLabel[11], "-");
    strcpy(kCounterLabel[0], "CLK");
    strcpy(kCounterLabel[1], "BPTX1");
    strcpy(kCounterLabel[2], "BPTX2");
    strcpy(kCounterLabel[3], "B1&B2");
    strcpy(kCounterLabel[4], "ORBIT");
    strcpy(kCounterLabel[5], "L1T");
    strcpy(kCounterLabel[6], "L1T_BE");
    strcpy(kCounterLabel[7], "L3T");
    strcpy(kCounterLabel[8], "L3T_OR");
    strcpy(kCounterLabel[9], "L3T_AND");
    strcpy(kCounterLabel[10], "BUNCH_POS");
    strcpy(kCounterLabel[11], "BUNCH_B1");
    strcpy(kCounterLabel[12], "BUNCH_B2");
    strcpy(kCounterLabel[13], "ToATLAS");
    strcpy(kCounterLabel[14], "ATLAS_CLK");
    strcpy(kCounterLabel[15], "ATLAS_CLKH");
    strcpy(kCounterLabel[16], "ATLAS_ECR");
    strcpy(kCounterLabel[17], "ATLAS_L1A");
    strcpy(kFlagBitLabel[0], "BPTX1");
    strcpy(kFlagBitLabel[1], "BPTX2");
    strcpy(kFlagBitLabel[2], "Laser");
    strcpy(kFlagBitLabel[3], "Pede");
    strcpy(kFlagBitLabel[4], "SHW");
    strcpy(kFlagBitLabel[5], "PI0");
    strcpy(kFlagBitLabel[6], "HighEM");
    strcpy(kFlagBitLabel[7], "Had.");
    strcpy(kFlagBitLabel[8], "L1T");
    strcpy(kFlagBitLabel[9], "FC");
    strcpy(kFlagBitLabel[10], "ZDC");
    strcpy(kFlagBitLabel[11], "-");
    strcpy(kFlagBitLabel[12], "BEAM");
    strcpy(kFlagBitLabel[13], "PEDE");
    strcpy(kFlagBitLabel[14], "ToATL");
    strcpy(kFlagBitLabel[15], "L1A");
    strcpy(kFlagBitLabel[16], "ZDC1");
    strcpy(kFlagBitLabel[17], "ZDC2");
    strcpy(kFlagBitLabel[18], "-");
    strcpy(kFlagBitLabel[19], "-");
    strcpy(kFlagBitLabel[20], "A2_L1T");
    strcpy(kFlagBitLabel[21], "A2_L3T");
    strcpy(kFlagBitLabel[22], "A2_ENB");
    strcpy(kFlagBitLabel[23], "A2_SHW");
    strcpy(kFlagBitLabel[24], "A1FC0");
    strcpy(kFlagBitLabel[25], "A1FC1");
    strcpy(kFlagBitLabel[26], "A1FC2");
    strcpy(kFlagBitLabel[27], "A1FC3");
    strcpy(kFlagBitLabel[28], "A2FC0");
    strcpy(kFlagBitLabel[29], "A2FC1");
    strcpy(kFlagBitLabel[30], "A2FC2");
    strcpy(kFlagBitLabel[31], "A2FC3");
  } else if (fOperation == kLHC2025) {  // Meaningless
    UT::Printf(UT::kPrintInfo, " (LHC2025)");
    fflush(stdout);

    SetTableDirectory("../Tables/Op2025");

    kNflag = 3;
    kNevFlag = 3;
    kOpenAdcNch = 8;
    kLaserNch = 2;
    kTdcNch = 12;
    kTdcNhit = 16;
    kScalerNch = 0;
    kNcounter = 18;
    kNfifo1 = 2;
    kNfifo2 = 3;
    kTime = 2;
    strcpy(kTdcLabel[0], "CLK");
    strcpy(kTdcLabel[1], "L3T");
    strcpy(kTdcLabel[2], "BPTX1");
    strcpy(kTdcLabel[3], "BPTX2");
    strcpy(kTdcLabel[4], "Shower");
    strcpy(kTdcLabel[5], "ATLAS-L1A");
    strcpy(kTdcLabel[6], "A1-FC");
    strcpy(kTdcLabel[7], "A2-FC");
    strcpy(kTdcLabel[8], "TS-OR");
    strcpy(kTdcLabel[9], "TL-OR");
    strcpy(kTdcLabel[10], "ADC Gate");
    strcpy(kTdcLabel[11], "-");
    strcpy(kCounterLabel[0], "CLK");
    strcpy(kCounterLabel[1], "BPTX1");
    strcpy(kCounterLabel[2], "BPTX2");
    strcpy(kCounterLabel[3], "B1&B2");
    strcpy(kCounterLabel[4], "ORBIT");
    strcpy(kCounterLabel[5], "L1T");
    strcpy(kCounterLabel[6], "L1T_BE");
    strcpy(kCounterLabel[7], "L3T");
    strcpy(kCounterLabel[8], "L3T_OR");
    strcpy(kCounterLabel[9], "L3T_AND");
    strcpy(kCounterLabel[10], "BUNCH_POS");
    strcpy(kCounterLabel[11], "BUNCH_B1");
    strcpy(kCounterLabel[12], "BUNCH_B2");
    strcpy(kCounterLabel[13], "ToATLAS");
    strcpy(kCounterLabel[14], "ATLAS_CLK");
    strcpy(kCounterLabel[15], "ATLAS_CLKH");
    strcpy(kCounterLabel[16], "ATLAS_ECR");
    strcpy(kCounterLabel[17], "ATLAS_L1A");
    strcpy(kFlagBitLabel[0], "BPTX1");
    strcpy(kFlagBitLabel[1], "BPTX2");
    strcpy(kFlagBitLabel[2], "Laser");
    strcpy(kFlagBitLabel[3], "Pede");
    strcpy(kFlagBitLabel[4], "SHW");
    strcpy(kFlagBitLabel[5], "PI0");
    strcpy(kFlagBitLabel[6], "HighEM");
    strcpy(kFlagBitLabel[7], "Had.");
    strcpy(kFlagBitLabel[8], "L1T");
    strcpy(kFlagBitLabel[9], "FC");
    strcpy(kFlagBitLabel[10], "ZDC");
    strcpy(kFlagBitLabel[11], "-");
    strcpy(kFlagBitLabel[12], "BEAM");
    strcpy(kFlagBitLabel[13], "PEDE");
    strcpy(kFlagBitLabel[14], "ToATL");
    strcpy(kFlagBitLabel[15], "L1A");
    strcpy(kFlagBitLabel[16], "ZDC1");
    strcpy(kFlagBitLabel[17], "ZDC2");
    strcpy(kFlagBitLabel[18], "-");
    strcpy(kFlagBitLabel[19], "-");
    strcpy(kFlagBitLabel[20], "A2_L1T");
    strcpy(kFlagBitLabel[21], "A2_L3T");
    strcpy(kFlagBitLabel[22], "A2_ENB");
    strcpy(kFlagBitLabel[23], "A2_SHW");
    strcpy(kFlagBitLabel[24], "A1FC0");
    strcpy(kFlagBitLabel[25], "A1FC1");
    strcpy(kFlagBitLabel[26], "A1FC2");
    strcpy(kFlagBitLabel[27], "A1FC3");
    strcpy(kFlagBitLabel[28], "A2FC0");
    strcpy(kFlagBitLabel[29], "A2FC1");
    strcpy(kFlagBitLabel[30], "A2FC2");
    strcpy(kFlagBitLabel[31], "A2FC3");
  } else {
    UT::Printf(UT::kPrintError, "Arm1Params::Initialise: unknown operation\n");
    exit(EXIT_FAILURE);
  }

  const char *table_env = std::getenv("LHCFLIBRARYTABLES");
  if (table_env != nullptr)
    if (TString(table_env) != TString("")) SetTableDirectory(table_env);

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

Arm1Params::ParInit Arm1Params::fInitialiser;
