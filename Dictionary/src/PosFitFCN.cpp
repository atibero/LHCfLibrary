#include "PosFitFCN.hh"

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(PosFitBaseFCN);
templateClassImp(PosFitMultiHitFCN_Height);
templateClassImp(PosFitMultiHitFCN_Area);
templateClassImp(PosFitMultiHitFCN);
templateClassImp(PosFitMultiHitFCN_DL_Height);
#endif

template <typename armrec>
PosFitBaseFCN *PosFitBaseFCN::CreateInstance(FuncType type) {
  switch (type) {
    case (FuncType::kHeightMultiHitFCN):
      return (PosFitBaseFCN *)new PosFitMultiHitFCN_Height<armrec>;
      break;
    case (FuncType::kAreaMultiHitFCN):
      return (PosFitBaseFCN *)new PosFitMultiHitFCN_Area<armrec>;
      break;
    case (FuncType::kOrgMultiHitFCN):
      return (PosFitBaseFCN *)new PosFitMultiHitFCN<armrec>;
      break;
    case (FuncType::kHeightMultiHitDL_FCN):
      return (PosFitBaseFCN *)new PosFitMultiHitFCN_DL_Height<armrec>;
    break;
    default:
      return (PosFitBaseFCN *)new PosFitMultiHitFCN<armrec>;
    break;
  }
  return nullptr;
}

template <typename armrec>
Double_t PosFitBaseFCN::GetArea(const vector<Double_t> &par, Int_t ipart, FuncType type) {
  PosFitBaseFCN *fcn = CreateInstance<armrec>(type);
  Double_t area = fcn->GetArea(par, ipart);
  delete fcn;
  return area;
}

template <typename armrec>
Double_t PosFitBaseFCN::GetPos(const vector<Double_t> &par, Int_t ipart, FuncType type) {
  PosFitBaseFCN *fcn = CreateInstance<armrec>(type);
  Double_t pos = fcn->GetPos(par, ipart);
  delete fcn;
  return pos;
}

template <typename armrec>
Double_t PosFitBaseFCN::GetTotalHeight(const vector<Double_t> &par, Double_t pos, FuncType type) {
  PosFitBaseFCN *fcn = CreateInstance<armrec>(type);
  Double_t height = fcn->GetTotalHeight(par, pos);
  delete fcn;
  return height;
}

template <typename armrec>
Double_t PosFitBaseFCN::GetHeight(const vector<Double_t> &par, Int_t ipart, Double_t pos, FuncType type) {
  PosFitBaseFCN *fcn = CreateInstance<armrec>(type);
  Double_t height = fcn->GetHeight(par, ipart, pos);
  delete fcn;
  return height;
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template PosFitBaseFCN *PosFitBaseFCN::CreateInstance<Arm1RecPars>(FuncType);
template PosFitBaseFCN *PosFitBaseFCN::CreateInstance<Arm2RecPars>(FuncType);
template Double_t PosFitBaseFCN::GetArea<Arm1RecPars>(const vector<Double_t> &, Int_t, FuncType);
template Double_t PosFitBaseFCN::GetArea<Arm2RecPars>(const vector<Double_t> &, Int_t, FuncType);
template Double_t PosFitBaseFCN::GetPos<Arm1RecPars>(const vector<Double_t> &, Int_t, FuncType);
template Double_t PosFitBaseFCN::GetPos<Arm2RecPars>(const vector<Double_t> &, Int_t, FuncType);
template Double_t PosFitBaseFCN::GetTotalHeight<Arm1RecPars>(const vector<Double_t> &, Double_t, FuncType);
template Double_t PosFitBaseFCN::GetTotalHeight<Arm2RecPars>(const vector<Double_t> &, Double_t, FuncType);
template Double_t PosFitBaseFCN::GetHeight<Arm1RecPars>(const vector<Double_t> &, Int_t, Double_t, FuncType);
template Double_t PosFitBaseFCN::GetHeight<Arm2RecPars>(const vector<Double_t> &, Int_t, Double_t, FuncType);
template class PosFitMultiHitFCN_Height<Arm1RecPars>;
template class PosFitMultiHitFCN_Height<Arm2RecPars>;
template class PosFitMultiHitFCN_Area<Arm1RecPars>;
template class PosFitMultiHitFCN_Area<Arm2RecPars>;
template class PosFitMultiHitFCN<Arm1RecPars>;
template class PosFitMultiHitFCN<Arm2RecPars>;
template class PosFitMultiHitFCN_DL_Height<Arm1RecPars>;
template class PosFitMultiHitFCN_DL_Height<Arm2RecPars>;
}  // namespace nLHCf
