//
// Created by MENJO Hiroaki on 2018/12/06.
//

#include "UtilsPhysics.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(UtilsPhysics);
#endif

/**
 * @class nLHCf::UtilsPhysics
 * @brief Utility functions for Physics
 */

TLorentzVector UtilsPhysics::ReconstructTwoParticle(TLorentzVector p0, TLorentzVector p1) { return (p0 + p1); }

TLorentzVector UtilsPhysics::ReconstructPi0(TLorentzVector p0, TLorentzVector p1) {
  return ReconstructTwoParticle(p0, p1);
}
