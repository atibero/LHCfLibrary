#include "OldInterface.hh"

#include <TFile.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdio>

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(OldInterface);
#endif

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass, typename armrec>
OldInterface<armclass, armrec>::OldInterface(TString fname, Bool_t load_lvl2)
    : fInName(fname), fFirstRun(-1), fLastRun(-1), fLoadLvl2(load_lvl2) {
  fRecLvl2 = new Level2<armclass>();
  fForTree = new TChain("Format");
  fRecTree = new TChain("Reconstruction");
}

template <typename armclass, typename armrec>
OldInterface<armclass, armrec>::OldInterface(TString fname, Int_t first, Int_t last, Bool_t load_lvl2)
    : fInName(fname), fFirstRun(first), fLastRun(last), fLoadLvl2(load_lvl2) {
  fRecLvl2 = new Level2<armclass>();
  fForTree = new TChain("Format");
  fRecTree = new TChain("Reconstruction");
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass, typename armrec>
OldInterface<armclass, armrec>::~OldInterface() {
  delete fRecLvl2;
}

/*----------------------*/
/*--- Initialisation ---*/
/*----------------------*/
template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::Open() {
  Utils::Printf(Utils::kPrintInfo, "Setting old tree...");
  fflush(stdout);

  /* Open input file(s) */
  if (fFirstRun >= 0 && fLastRun >= 0) {
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/run%05d_rec.root", fInName.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        Utils::Printf(Utils::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fForTree->Add(iname);
        fRecTree->Add(iname);
      }
    }
  } else if (fInName.EndsWith(".root")) {
    TFile ifile(fInName.Data(), "READ");
    if (!ifile.IsOpen()) {
      ifile.Close();
      Utils::Printf(Utils::kPrintInfo, "skipping file %s\n", fInName.Data());
    } else {
      ifile.Close();
      fForTree->Add(fInName.Data());
      fRecTree->Add(fInName.Data());
    }
  } else {
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInName.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInName + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            Utils::Printf(Utils::kPrintInfo, "skipping file %s\n", (fInName + "/" + iname).Data());
          } else {
            ifile.Close();
            fForTree->Add((fInName + "/" + iname).Data());
            fRecTree->Add((fInName + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      Utils::Printf(Utils::kPrintError, "Cannot open input directory!");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  OpenLevel2();
  OpenLevel3();

  const Int_t nentries = fRecTree->GetEntries();
  if (fForTree->GetEntries() != nentries) {
    Utils::Printf(Utils::kPrintError, "Format entries != Reconstruction entries! (%d != %d)\n", fForTree->GetEntries(),
                  nentries);
  }
  Utils::Printf(Utils::kPrintInfo, " Done.\n");
  Utils::Printf(Utils::kPrintInfo, "Total old events: %d\n", nentries);
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::OpenLevel2() {
  if (fLoadLvl2) {
    fCalBr = fRecTree->GetBranch("calorimeter");
    fCalVec = new vector<double>();
    fCalVec->resize(this->kCalNtower * this->kCalNlayer);
    fForTree->SetBranchAddress("calorimeter", &fCalVec, &fCalBr);

    fSilBr = fRecTree->GetBranch("posdet");
    fSilVec = new vector<double>();
    fSilVec->resize(this->kPosNsample * this->kPosNlayer * this->kPosNview * this->kPosNchannel[0]);
    fForTree->SetBranchAddress("posdet", &fSilVec, &fSilBr);
  }

  fLvl2 = new Level2<armclass>(Form("old_lvl2_a%d", this->kArmIndex + 1), "Old Level2");
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::OpenLevel3() {
  fCalRecBr = fRecTree->GetBranch("cal_rec");
  fCalRecVec = new vector<double>();
  fCalRecVec->resize(this->kCalNtower * this->kCalNlayer);
  fRecTree->SetBranchAddress("cal_rec", &fCalRecVec, &fCalRecBr);

  fEneBr = fRecTree->GetBranch("energy");
  fEneVec = new vector<double>();
  fEneVec->resize(this->kCalNtower * 2);
  fRecTree->SetBranchAddress("energy", &fEneVec, &fEneBr);

  fPosBr = fRecTree->GetBranch("vertex0");
  fPosVec = new vector<double>();
  fPosVec->resize(this->kCalNtower * this->kPosNlayer * this->kPosNview);
  fRecTree->SetBranchAddress("vertex0", &fPosVec, &fPosBr);

  fMaxBr = fRecTree->GetBranch("maxposdetlayer");
  fMaxVec = new vector<int>();
  fMaxVec->resize(this->kCalNtower * this->kPosNview);
  fRecTree->SetBranchAddress("maxposdetlayer", &fMaxVec, &fMaxBr);

  fL90Br = fRecTree->GetBranch("pid");
  fL90Vec = new vector<double>();
  fL90Vec->resize(this->kCalNtower * 4);
  fRecTree->SetBranchAddress("pid", &fL90Vec, &fL90Br);

  fMulBr = fRecTree->GetBranch("multihit");
  fMulVec = new vector<bool>();
  fMulVec->resize(this->kCalNtower);
  fRecTree->SetBranchAddress("multihit", &fMulVec, &fMulBr);

  fLvl3 = new Level3<armrec>(Form("old_lvl3_a%d", this->kArmIndex + 1), "Old Level3");
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::Close() {
  /* Close input file */
  // fFile->Close();
  // delete fFile;

  if (fLoadLvl2) {
    delete fCalVec;
    delete fSilVec;
  }
  delete fLvl2;

  delete fCalRecVec;
  delete fEneVec;
  delete fPosVec;
  delete fMaxVec;
  delete fL90Vec;
  delete fMulVec;
  delete fLvl3;
}

template <typename armclass, typename armrec>
Int_t OldInterface<armclass, armrec>::GetEvent(Int_t ev) {
  if (ev < fRecTree->GetEntries()) {
    if (fLoadLvl2) {
      fCalBr->GetEntry(ev);
      fSilBr->GetEntry(ev);
      CopyOldLevel2(fLvl2, fCalVec, fSilVec);
    }

    fCalRecBr->GetEntry(ev);
    fEneBr->GetEntry(ev);
    fPosBr->GetEntry(ev);
    fMaxBr->GetEntry(ev);
    fL90Br->GetEntry(ev);
    fMulBr->GetEntry(ev);
    CopyOldLevel3(fRecLvl2, fLvl3, fCalRecVec, fEneVec, fPosVec, fMaxVec, fL90Vec, fMulVec);

  } else {
    Utils::Printf(Utils::kPrintError, "Error: event number (%d) too large for old tree (max = %d)\n", ev,
                  fRecTree->GetEntries() - 1);
    // exit(EXIT_FAILURE);
    return -1;
  }

  return 0;
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::CopyOldLevel2(Level2<armclass> *lvl2, vector<double> *cvec, vector<double> *svec) {
  Int_t ivc = 0;
  for (Int_t it = 0; it < lvl2->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) {
      lvl2->fCalorimeter[it][il] = cvec->at(ivc);
      ++ivc;
    }

  Int_t ivs = 0;
  Int_t tower = 0;
  for (Int_t is = 0; is < lvl2->kPosNsample; ++is)  // tower = sample in old software
    for (int il = 0; il < lvl2->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < lvl2->kPosNview; ++iv)
        for (Int_t ic = 0; ic < lvl2->kPosNchannel[tower]; ++ic) {
          // old format: posdet_(is, il, iv, ic)
          lvl2->fPosDet[tower][il][iv][ic][is] = svec->at(ivs);
          ++ivs;
        }
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::CopyOldLevel3(Level2<armclass> *lvl2, Level3<armrec> *lvl3, vector<double> *cvec,
                                                   vector<double> *evec, vector<double> *pvec, vector<int> *mvec,
                                                   vector<double> *lvec, vector<bool> *hvec) {
  Int_t ivc = 0;
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl3->kCalNlayer; ++il) {
      lvl2->fCalorimeter[it][il] = cvec->at(ivc);
      ++ivc;
    }

  Int_t ive = 0;
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it)
    for (Int_t ip = 0; ip < 2; ++ip) {
      if (ip == 0) lvl3->fPhotonEnergy[it] = evec->at(ive);
      ++ive;
    }

  Int_t ivm = 0;
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it)
    for (Int_t iv = 0; iv < lvl3->kPosNview; ++iv) {
      lvl3->fPosMaxLayer[it][iv] = mvec->at(ivm);
      ++ivm;
    }

  Int_t ivp = 0;
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it)
    for (int il = 0; il < lvl3->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < lvl3->kPosNview; ++iv) {
        if (il == lvl3->fPosMaxLayer[it][iv]) lvl3->fPhotonPosition[it][iv] = pvec->at(ivp);
        ++ivp;
      }

  Int_t ivl = 0;
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it) {
    for (Int_t ip = 0; ip < 4; ++ip) {  // L20, L90, gamma/neutron, ???
      if (ip == 1) lvl3->fPhotonL90[it] = lvec->at(ivl);
      ++ivl;
    }
    if (lvl3->fPhotonL90[it] < L90Boundary(it, lvl3->fPhotonEnergy[it]))
      lvl3->fIsPhoton[it] = true;
    else
      lvl3->fIsPhoton[it] = false;
  }

  Int_t ivh = 0;
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it) {
    lvl3->fPhotonMultiHit[it] = hvec->at(ivh);
    ++ivh;
  }
}

template <typename armclass, typename armrec>
Double_t OldInterface<armclass, armrec>::L90Boundary(Int_t tower, Double_t energy, int eff_thr) {
  Double_t l90boundary;
  if (eff_thr == 85) {
    /*--- 85% ---*/
    const double p0[] = {2.42448, 1.57536};
    const double p1[] = {0.812548, 99.6024};
    const double p2[] = {1020.80, 36364.5};
    l90boundary = p0[tower] * log(p1[tower] * energy + p2[tower]);
  } else if (eff_thr == 95) {
    /*--- 95% ---*/
    const double p0[] = {2.90891, 1.76900};
    const double p1[] = {0.265344, 42.7327};
    const double p2[] = {582.268, 38810.9};
    l90boundary = p0[tower] * log(p1[tower] * energy + p2[tower]);
  } else {
    /*--- 90% ---*/
    const double p0[] = {2.15520, 1.65621};
    const double p1[] = {3.00547, 69.2783};
    const double p2[] = {3046.61, 35019.9};
    l90boundary = p0[tower] * log(p1[tower] * energy + p2[tower]);
  }
  return l90boundary;
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::Print(Int_t ev) {
  if (ev < 0)
    Utils::Printf(Utils::kPrintInfo, "\n\n--- Arm%d event ---\n", this->kArmIndex + 1);
  else
    Utils::Printf(Utils::kPrintInfo, "\n\n--- Arm%d event %d ---\n", this->kArmIndex + 1, ev);
  if (fLoadLvl2) PrintLevel2(fLvl2);
  PrintLevel3(fRecLvl2, fLvl3);
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::PrintLevel2(Level2<armclass> *lvl2) {
  Utils::Printf(Utils::kPrintInfo, "\tOLD CALORIMETER\n");
  for (Int_t it = 0; it < lvl2->kCalNtower; ++it) {
    Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
    Utils::Printf(Utils::kPrintInfo, "\t\tcal =");
    for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) {
      Utils::Printf(Utils::kPrintInfo, " %.1lf", lvl2->fCalorimeter[it][il]);
    }
    Utils::Printf(Utils::kPrintInfo, "\n");
  }
  Utils::Printf(Utils::kPrintInfo, "\n");

  // Utils::Printf(Utils::kPrintInfo, "\tOLD SILICON\n");
  // for (Int_t it = 0; it < lvl2->kPosNtower; ++it) {
  //   Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
  //   for (int il = 0; il < lvl2->kPosNlayer; ++il) {
  //     Utils::Printf(Utils::kPrintInfo, "\t\t%d", il + 1);
  //     for (Int_t iv = 0; iv < lvl2->kPosNview; ++iv) {
  // 	if (iv == 0)
  // 	  Utils::Printf(Utils::kPrintInfo, "X =");
  // 	else
  // 	  Utils::Printf(Utils::kPrintInfo, "Y =");
  // 	for (Int_t ic = 0; ic < lvl2->kPosNchannel[it]; ++ic) {
  // 	  const Int_t is = 1;
  // 	  Utils::Printf(Utils::kPrintInfo, " %.1lf", lvl2->fPosDet[it][il][iv][ic][is]);
  // 	}
  // 	Utils::Printf(Utils::kPrintInfo, "\n");
  //     }
  //   }
  // }
  // Utils::Printf(Utils::kPrintInfo, "\n");
}

template <typename armclass, typename armrec>
void OldInterface<armclass, armrec>::PrintLevel3(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  Utils::Printf(Utils::kPrintInfo, "\tOLD CALORIMETER (CORRECTED)\n");
  for (Int_t it = 0; it < lvl2->kCalNtower; ++it) {
    Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
    Utils::Printf(Utils::kPrintInfo, "\t\tcal =");
    for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) {
      Utils::Printf(Utils::kPrintInfo, " %.1lf", lvl2->fCalorimeter[it][il]);
    }
    Utils::Printf(Utils::kPrintInfo, "\n");
  }
  Utils::Printf(Utils::kPrintInfo, "\n");

  Utils::Printf(Utils::kPrintInfo, "\tOLD RECONSTRUCTED\n");
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it) {
    Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
    Utils::Printf(Utils::kPrintInfo, "\t\tenergy = %.1lf\n", lvl3->fPhotonEnergy[it]);
    Utils::Printf(Utils::kPrintInfo, "\t\t(x,y)  = %.1lf, %.1lf\n", lvl3->fPhotonPosition[it][0],
                  lvl3->fPhotonPosition[it][1]);
    Utils::Printf(Utils::kPrintInfo, "\t\tL90\%   = %.1lf\n", lvl3->fPhotonL90[it]);
    if (lvl3->fIsPhoton[it])
      Utils::Printf(Utils::kPrintInfo, "\t\tphoton-like\n");
    else
      Utils::Printf(Utils::kPrintInfo, "\t\thadron-like\n");
    if (lvl3->fPhotonMultiHit[it])
      Utils::Printf(Utils::kPrintInfo, "\t\tmulti-hit\n");
    else
      Utils::Printf(Utils::kPrintInfo, "\t\tsingle-hit\n");
  }
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class OldInterface<Arm1Params, Arm1RecPars>;
template class OldInterface<Arm2Params, Arm2RecPars>;
}  // namespace nLHCf
