#include <Arm2RecPars.hh>
#include <iomanip>
#include <iostream>

using namespace std;

#include <TGraph.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMarker.h>
#include <TStyle.h>

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "EventDisplay.hh"
#include "PosFitFCN.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(EventDisplay);
#endif

/**
 * @class nLHCf::EventDisplay
 * @brief This class draw the histograms of level2.
 *
 * An example code:
\code

 EventDisplay<Arm1Params> *ed = new EventDisplay("ev_a1","Event Display for Arm1");
 ed->Draw();

   for(int i=0;i<nentries;i++){

     ev->Fill(lvl2_a1); // Set level2 data
      ev->Update();      // Update the canvas

   }
\endcode
 *
 *
*/

// Constants
template <typename armclass, typename armrec>
const Int_t EventDisplay<armclass, armrec>::kNpad = 7;
template <typename armclass, typename armrec>
const Int_t EventDisplay<armclass, armrec>::kNpadPos = armclass::kPosNtower;
template <typename armclass, typename armrec>
const Int_t EventDisplay<armclass, armrec>::kNtext = 10;
template <typename armclass, typename armrec>
const Int_t EventDisplay<armclass, armrec>::kNpad2 = 4;

template <typename armclass, typename armrec>
EventDisplay<armclass, armrec>::EventDisplay() : EventHistograms<armclass>() {
  static int icall = 0;
  this->SetName(Form("ed_a%d_%d", this->kArmIndex + 1, icall));
  this->SetTitle("Event Display");
  icall++;

  Initialize();
  return;
}

template <typename armclass, typename armrec>
EventDisplay<armclass, armrec>::EventDisplay(const char *name, const char *title)
    : EventHistograms<armclass>(name, title) {
  Initialize();
  return;
}

template <typename armclass, typename armrec>
EventDisplay<armclass, armrec>::~EventDisplay() {
  Delete();
}

template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Initialize() {
  // Allocation of vectors
  Utils::AllocateVector1D(fPad, kNpad);
  Utils::AllocateVector2D(fPadPos, this->kNxy, kNpadPos);
  Utils::AllocateVector1D(fText, kNtext);
  Utils::AllocateVector1D(fLine, 12);

  Utils::AllocateVector1D(fPad2, kNpad2);
  Utils::AllocateVector2D(fFlagBit, this->kNflag, 32);
  Utils::AllocateVector1D(fCounters, this->kNcounter);
  Utils::AllocateVector2D(fFifoCounters, this->kNfifo1, this->kNfifo2);
  Utils::AllocateVector2D(fTdc, this->kTdcNch, this->kTdcNhit);

  // Initialize the pointer addresses
  fCanvas1 = NULL;
  Utils::ClearVector1D(fPad, (TPad *)0);
  Utils::ClearVector2D(fPadPos, (TPad *)0);
  Utils::ClearVector1D(fText, (TLatex *)0);
  Utils::ClearVector1D(fLine, (TLine *)0);

  fCanvas2 = NULL;
  Utils::ClearVector1D(fPad2, (TPad *)0);
  Utils::ClearVector2D(fFlagBit, (TLatex *)0);
  Utils::ClearVector1D(fCounters, (TLatex *)0);
  Utils::ClearVector2D(fFifoCounters, (TLatex *)0);
  Utils::ClearVector2D(fTdc, (TLatex *)0);

  // Instance
  for (int i = 0; i < kNtext; ++i) fText[i] = new TLatex();

  for (int i = 0; i < this->kNflag; ++i)
    for (int j = 0; j < 32; j++) fFlagBit[i][j] = new TLatex();

  for (int i = 0; i < this->kNcounter; ++i) fCounters[i] = new TLatex();

  for (int i = 0; i < this->kNfifo1; ++i)
    for (int j = 0; j < this->kNfifo2; j++) fFifoCounters[i][j] = new TLatex();

  for (int i = 0; i < this->kTdcNch; ++i)
    for (int j = 0; j < this->kTdcNhit; j++) fTdc[i][j] = new TLatex();

  // Set the default falues
  fCanvasSize[0] = 1000;
  fCanvasSize[1] = 800;

  fCalLineColor[0] = kBlack;     // for range = 0
  fCalLineColor[1] = kGray + 1;  // for range = 1

  fPosLineColor[0] = kMagenta;
  fPosLineColor[1] = kRed;
  fPosLineColor[2] = kGreen + 2;
  fPosLineColor[3] = kBlue;

  if (this->fOperation == kLHC2015) {
    fFcLineColor[0] = kRed;
    fFcLineColor[1] = kBlue;
  } else {
    fFcLineColor[0] = kBlack;
    fFcLineColor[1] = kGray + 1;
  }

  fLvl3.Clear();
  fLvl2flag = false;
  fLvl3flag = false;
}

template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Delete() {
  // Delete histograms
  EventHistograms<armclass>::Delete();

  for (int i = 0; i < 12; ++i)
    if (fLine[i]) {
      delete fLine[i];
      fLine[i] = NULL;
    }

  for (int i = 0; i < kNtext; ++i)
    if (fText[i]) {
      delete fText[i];
      fText[i] = NULL;
    }

  for (int i = 0; i < this->kNflag; ++i)
    for (int j = 0; j < 32; j++)
      if (fFlagBit[i][j]) {
        delete fFlagBit[i][j];
        fFlagBit[i][j] = NULL;
      }

  for (int i = 0; i < this->kNcounter; ++i)
    if (fCounters[i]) {
      delete fCounters[i];
      fCounters[i] = NULL;
    }

  for (int i = 0; i < this->kNfifo1; ++i)
    for (int j = 0; j < this->kNfifo2; j++)
      if (fFifoCounters[i][j]) {
        delete fFifoCounters[i][j];
        fFifoCounters[i][j] = NULL;
      }

  for (int i = 0; i < this->kTdcNch; ++i)
    for (int j = 0; j < this->kTdcNhit; j++)
      if (fTdc[i][j]) {
        delete fTdc[i][j];
        fTdc[i][j] = NULL;
      }

  // Maybe Canvas::Delete is enough to delete all pads.
  for (int j = 0; j < this->kNxy; ++j)
    for (int i = 0; i < kNpadPos; ++i)
      if (fPadPos[j][i]) {
        delete fPadPos[j][i];
        fPadPos[j][i] = NULL;
      }

  for (int i = 0; i < kNpad; ++i)
    if (fPad[i]) {
      delete fPad[i];
      fPad[i] = NULL;
    }

  if (fCanvas1) {
    delete fCanvas1;
    fCanvas1 = NULL;
  }

  for (int i = 0; i < kNpad2; ++i)
    if (fPad2[i]) {
      delete fPad2[i];
      fPad2[i] = NULL;
    }

  if (fCanvas2) {
    delete fCanvas2;
    fCanvas2 = NULL;
  }

  return;
}

//////////////////////////////////////////
/// Fill the data to histograms.
/// \tparam armclass
/// \param d [in]  Level1 Class
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Fill(Level0<armclass> *d) {
  // Fill the data to histograms
  EventHistograms<armclass>::Fill(d);

  // Fill the data to run info
  fText[0]->SetTitle(TString::Format("%d", d->fRun));
  fText[1]->SetTitle(TString::Format("%d", d->fEvent));
  fText[2]->SetTitle(TString::Format("%d", d->fGevent));
  fText[3]->SetTitle(TString::Format("%d", (Int_t)d->fTime[0]));
  fText[4]->SetTitle(TString::Format("x%08x", d->fFlag[0]));
  fText[5]->SetTitle(TString::Format("x%08x", d->fFlag[1]));

  // Fill Flags
  for (int i = 0; i < this->kNflag; i++) {
    for (int j = 0; j < 32; j++) {
      if ((d->fFlag[i] >> j) & 0x1)
        fFlagBit[i][j]->SetTextColor(kBlack);
      else
        fFlagBit[i][j]->SetTextColor(kGray);
    }
  }

  // counters
  for (int i = 0; i < this->kNcounter; ++i)
    fCounters[i]->SetTitle(TString::Format("%u", (unsigned int)(d->fCounter[i])));

  // fifo counters
  for (int i = 0; i < this->kNfifo1; ++i)
    for (int j = 0; j < this->kNfifo2; j++)
      fFifoCounters[i][j]->SetTitle(TString::Format("%u", (unsigned int)(d->fFifoCounter[i][j])));

  for (int i = 0; i < this->kTdcNch; ++i)
    for (int j = 0; j < this->kTdcNhit; j++)
      if (d->fTdcFlag[i][j] == 1)
        fTdc[i][j]->SetTitle(TString::Format("%d", (int)(d->fTdc[i][j])));
      else
        fTdc[i][j]->SetTitle("");

  fLvl2flag = false;
  return;
}

//////////////////////////////////////////
/// Fill the data to histograms.
/// \tparam armclass
/// \param d [in]  Level1 Class
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Fill(Level1<armclass> *d) {
  // Fill the data to histograms
  EventHistograms<armclass>::Fill(d);

  // Fill the data to run info
  fText[0]->SetTitle(TString::Format("%d", d->fRun));
  fText[1]->SetTitle(TString::Format("%d", d->fEvent));
  fText[2]->SetTitle(TString::Format("%d", d->fGevent));
  fText[3]->SetTitle(TString::Format("%d", (Int_t)d->fTime[0]));
  fText[4]->SetTitle(TString::Format("x%08x", d->fFlag[0]));
  fText[5]->SetTitle(TString::Format("x%08x", d->fFlag[1]));

  // Fill Flags
  for (int i = 0; i < this->kNflag; i++) {
    for (int j = 0; j < 32; j++) {
      if ((d->fFlag[i] >> j) & 0x1)
        fFlagBit[i][j]->SetTextColor(kBlack);
      else
        fFlagBit[i][j]->SetTextColor(kGray);
    }
  }

  // counters
  for (int i = 0; i < this->kNcounter; ++i)
    fCounters[i]->SetTitle(TString::Format("%u", (unsigned int)(d->fCounter[i])));

  // fifo counters
  for (int i = 0; i < this->kNfifo1; ++i)
    for (int j = 0; j < this->kNfifo2; j++)
      fFifoCounters[i][j]->SetTitle(TString::Format("%u", (unsigned int)(d->fFifoCounter[i][j])));

  for (int i = 0; i < this->kTdcNch; ++i)
    for (int j = 0; j < this->kTdcNhit; j++)
      if (d->fTdcFlag[i][j] == 1) fTdc[i][j]->SetTitle(TString::Format("%d", (int)(d->fTdc[i][j])));

  fLvl2flag = false;
  return;
}

///////////////////////////////////////////
/// Fill the data to histograms.
/// \tparam armclass
/// \param d [in]  Level2 Class
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Fill(Level2<armclass> *d) {
  // Fill the data to histograms

  EventHistograms<armclass>::Fill(d);

  // Fill the data to run info
  fText[0]->SetTitle(TString::Format("%d", d->fRun));
  fText[1]->SetTitle(TString::Format("%d", d->fEvent));
  fText[2]->SetTitle(TString::Format("%d", d->fGevent));
  fText[3]->SetTitle(TString::Format("%d", (Int_t)d->fTime[0]));
  fText[4]->SetTitle(TString::Format("x%08x", d->fFlag[0]));
  fText[5]->SetTitle(TString::Format("x%08x", d->fFlag[1]));

  // Fill Flags
  for (int i = 0; i < this->kNflag; i++) {
    for (int j = 0; j < 32; j++) {
      if ((d->fFlag[i] >> j) & 0x1)
        fFlagBit[i][j]->SetTextColor(kBlack);
      else
        fFlagBit[i][j]->SetTextColor(kGray);
    }
  }

  // counters
  for (int i = 0; i < this->kNcounter; ++i)
    fCounters[i]->SetTitle(TString::Format("%u", (unsigned int)(d->fCounter[i])));

  // fifo counters
  for (int i = 0; i < this->kNfifo1; ++i)
    for (int j = 0; j < this->kNfifo2; j++)
      fFifoCounters[i][j]->SetTitle(TString::Format("%u", (unsigned int)(d->fFifoCounter[i][j])));

  for (int i = 0; i < this->kTdcNch; ++i)
    for (int j = 0; j < this->kTdcNhit; j++)
      if (d->fTdcFlag[i][j] == 1) fTdc[i][j]->SetTitle(TString::Format("%d", (int)(d->fTdc[i][j])));

  fLvl2flag = true;
  return;
}

///////////////////////////////////////////
/// Fill the data to histograms.
/// \tparam armclass
/// \param d [in]  Level3 Class
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Fill(Level3<armrec> *d) {
  fLvl3.DataCopy(d);
  fLvl3flag = true;
}

template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Draw(Option_t *option) {
  DrawCanvas2(option);
  DrawCanvas1(option);
}

///////////////////////////////////////////
/// Draw the canvas
/// \tparam armclass
/// \param option [in]  Not used for the moment.
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::DrawCanvas1(Option_t *option) {
  TString opt = option;
  opt.ToLower();
  if (opt.Index("canvas1") == kNPOS) return;

  Bool_t isLvl2 = true;
  if (opt.Index("lvl2") == kNPOS) {
    if ((opt.Index("lvl1") != kNPOS) || (opt.Index("lvl0") != kNPOS)) {
      isLvl2 = false;
    }
  }

  // For Canvas
  if (!fCanvas1) {
    fCanvas1 =
        new TCanvas(TString::Format("%s_canvas", this->GetName()), this->GetTitle(), fCanvasSize[0], fCanvasSize[1]);
  }

  // For Pad
  const Double_t padsize[kNpad][4] = {{0.00, 0.77, 0.40, 1.00},  // pad0 general info
                                      {0.40, 0.77, 0.70, 1.00},  // pad1 fc
                                      {0.0, 0.5, 0.5, 0.77},     // pad2 calorimeter TS
                                      {0.50, 0.5, 1.0, 0.77},    // pad3 calorimeter TL
                                      {0.0, 0.25, 1.00, 0.50},   // pad4 posdet x
                                      {0.0, 0.0, 1.0, 0.25},     // pad5 posdet y
                                      {0.70, 0.77, 1.0, 1.00}};  // pad6 zdc

  for (int i = 0; i < kNpad; i++) {
    fCanvas1->cd();
    if (!fPad[i]) {
      fPad[i] = new TPad(TString::Format("%s_pad%d", this->GetName(), i), "Pad ", padsize[i][0], padsize[i][1],
                         padsize[i][2], padsize[i][3]);
      fPad[i]->SetBorderSize(1);
      fPad[i]->Draw();
    }
  }

  // For pad for position detector
  const Double_t padposborder = (kNpadPos == 1 ? 1.0 : 0.4);

  for (int i = 0; i < this->kNxy; ++i) {
    for (int j = 0; j < kNpadPos; ++j) {
      if (!fPadPos[i][j]) {
        double pos[4];
        if (j == 0) {
          pos[0] = 0.0;
          pos[1] = 0.0;
          pos[2] = padposborder;
          pos[3] = 1.0;
        } else {
          pos[0] = padposborder;
          pos[1] = 0.0;
          pos[2] = 1.0;
          pos[3] = 1.0;
        }

        fPad[4 + i]->cd();
        fPadPos[i][j] = new TPad(TString::Format("%s_padpos_%d_%d", this->GetName(), i, j), "Pad Pos", pos[0], pos[1],
                                 pos[2], pos[3]);
        fPadPos[i][j]->SetBorderSize(1);
        fPadPos[i][j]->Draw();
      }
    }
  }

  for (int ichip = 0; ichip < 12; ++ichip) {
    if (!fLine[ichip]) {
      Double_t xmin = 32 * ichip;
      Double_t xmax = 32 * ichip;
      fLine[ichip] = new TLine();
      fLine[ichip]->SetLineColor(kBlack);
      fLine[ichip]->SetLineStyle(3);
      fLine[ichip]->SetX1(xmin);
      fLine[ichip]->SetX2(xmax);
    }
  }

  // General setup
  gStyle->SetOptStat(0);

  // Draw Run Information ----------------------------
  fPad[0]->cd();
  TText *text = new TText();
  text->SetTextFont(72);
  text->SetTextSize(0.15);
  text->DrawTextNDC(0.05, 0.82, (this->kArmIndex == 0 ? "LHCf-Arm1" : "LHCf-Arm2"));
  text->SetTextSize(0.08);
  text->DrawTextNDC(0.07, 0.70, "RUN");
  text->DrawTextNDC(0.07, 0.60, "Number");
  text->DrawTextNDC(0.07, 0.50, "Gnumber");
  text->DrawTextNDC(0.07, 0.35, "DAQ Time");
  text->DrawTextNDC(0.07, 0.25, "DAQ Flag");

  Double_t textsize = 0.08;
  Font_t textfont = 42;
  DrawText(fText[0], 0.35, 0.70, textsize, textfont);  // run
  DrawText(fText[1], 0.35, 0.60, textsize, textfont);  // number
  DrawText(fText[2], 0.35, 0.50, textsize, textfont);  // gnumber
  DrawText(fText[3], 0.35, 0.35, textsize, textfont);  // Time
  DrawText(fText[4], 0.35, 0.25, textsize, textfont);  // flag 0
  DrawText(fText[5], 0.35, 0.15, textsize, textfont);  // flag 1

  // Draw Histograms ---------------------------------
  for (int itower = 0; itower < this->kCalNtower; ++itower) {
    fPad[2 + itower]->cd();
    Utils::SetPadMargin(fPad[2 + itower], 0.1, 0.15, 0.05, 0.15);

    this->fHistCalorimeter[itower][0]->SetLineColor(fCalLineColor[0]);
    this->fHistCalorimeter[itower][0]->Draw("hist");
    if (this->fDataType == EventHistograms<armclass>::LEVEL0 || this->fDataType == EventHistograms<armclass>::LEVEL1) {
      this->fHistCalorimeter[itower][1]->SetLineColor(fCalLineColor[1]);
      this->fHistCalorimeter[itower][1]->Draw("hist,same");
    }

    Utils::SetHistTitles(this->fHistCalorimeter[itower][0], "", "", "");
    Utils::SetHistAxisTexts(this->fHistCalorimeter[itower][0], "x", "#Layer", 0.07, 0.7, 0.06, 0.005);
    Utils::SetHistAxisTexts(this->fHistCalorimeter[itower][0], "y", isLvl2 ? "dE [GeV]" : "dE [ADC]", 0.07, 0.7, 0.06,
                            0.005, 505);

    // Draw the title
    Utils::DrawTextNDC(0.20, 0.91, (itower == 0 ? "Small Tower" : "Large Tower"), 0.08, 42, kBlack);
  }

  // Draw Position Detector  --------------------------
  for (int iview = 0; iview < this->kNxy; ++iview) {
    for (int itower = 0; itower < this->kPosNtower; ++itower) {
      fPadPos[iview][itower]->cd();
      Utils::SetPadMargin(fPadPos[iview][itower], 0.1, 0.15, 0.03, 0.12);

      for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer) {
        for (int isample = 0; isample < this->kPosNsample; ++isample) {
          TString drawoption = (ilayer == 0 && isample == 0 ? "hist" : "hist,same");
          this->fHistPosDet[itower][ilayer][iview][isample]->SetLineColor(fPosLineColor[ilayer]);
          if (this->kPosNsample != 1) {
            if (isample == 0)
              this->fHistPosDet[itower][ilayer][iview][isample]->SetLineStyle(2);
            else if (isample == 1)
              this->fHistPosDet[itower][ilayer][iview][isample]->SetLineStyle(1);
            else
              this->fHistPosDet[itower][ilayer][iview][isample]->SetLineStyle(3);
          }
          this->fHistPosDet[itower][ilayer][iview][isample]->Draw(drawoption);

          Utils::SetHistTitles(this->fHistPosDet[itower][ilayer][iview][isample], "");
          Utils::SetHistAxisTexts(this->fHistPosDet[itower][ilayer][iview][isample], "x", "#channel ", 0.07, 1.05, 0.06,
                                  0.005);
          Utils::SetHistAxisTexts(this->fHistPosDet[itower][ilayer][iview][isample], "y",
                                  isLvl2 ? "dE [GeV/channel]" : "dE [ADC/channel]", 0.07, 0.8, 0.06, 0.005, 505);
        }
      }

      // Draw Title
      Utils::DrawTextNDC(0.15, 0.91, (iview == 0 ? "X-view" : "Y-view"), 0.08, 42, kBlack);

      // Draw Legend
      if (itower != this->kPosNtower - 1) continue;
      TLegend *legend_pos = new TLegend(0.3, 0.9, 0.9, 1.0);
      legend_pos->SetNColumns(this->kPosNlayer);
      for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer) {
        legend_pos->AddEntry(this->fHistPosDet[itower][ilayer][iview][(this->kPosNsample == 1 ? 0 : 1)],
                             TString::Format("Layer %d", ilayer), "lf");
      }
      legend_pos->SetBorderSize(0);
      legend_pos->SetFillStyle(0);
      legend_pos->Draw();
    }
  }

  // Draw Front Counter -----------------------------
  fPad[1]->cd();
  Utils::SetPadMargin(fPad[1], 0.1, 0.15, 0.05, 0.18);
  for (int iarm = 0; iarm < this->kNarm; ++iarm) {
    this->fHistFrontCounter[iarm]->SetLineColor(fFcLineColor[iarm]);
    this->fHistFrontCounter[iarm]->Draw(iarm == 0 ? "hist" : "hist,same");

    Utils::SetHistTitles(this->fHistFrontCounter[iarm], "", "", "");
    Utils::SetHistAxisTexts(this->fHistFrontCounter[iarm], "x", "#channel", 0.07, 1.0, 0.07, 0.005, 505);
    Utils::SetHistAxisTexts(this->fHistFrontCounter[iarm], "y", "dE", 0.07, 1.0, 0.07, 0.005, 505);

    // Draw the title
    Utils::DrawTextNDC(0.20, 0.91, "Front Counter", 0.08, 42, kBlack);
  }

  // Draw Zdc --------------------------------------
  if (this->kZdcNchannel) {
    fPad[6]->cd();
    Utils::SetPadMargin(fPad[6], 0.1, 0.15, 0.05, 0.18);
    for (int i = 0; i < this->kZdcNrange; ++i) {
      this->fHistZdc[i]->SetLineColor(fCalLineColor[i]);
      this->fHistZdc[i]->Draw(i == 0 ? "hist" : "hist,same");

      Utils::SetHistTitles(this->fHistZdc[i], "", "", "");
      Utils::SetHistAxisTexts(this->fHistZdc[i], "x", "#channel", 0.07, 1.0, 0.07, 0.005, 505);
      Utils::SetHistAxisTexts(this->fHistZdc[i], "y", "dE", 0.07, 1.0, 0.07, 0.005, 505);

      // Draw the title
      Utils::DrawTextNDC(0.20, 0.91, "ATLAS-ZDC", 0.08, 42, kBlack);
    }
  }

  // Lvl3 Information =================================================
  // The Lvl3 are draw in Update

  Update(option);

  return;
}

///////////////////////////////////////////
/// Draw the canvas
/// \tparam armclass
/// \param option [in]  Not used for the moment.
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::DrawCanvas2(Option_t *option) {
  TString opt = option;
  opt.ToLower();
  if (opt.Index("canvas2") == kNPOS) return;

  // For Canvas
  if (!fCanvas2) {
    fCanvas2 =
        new TCanvas(TString::Format("%s_canvas2", this->GetName()), this->GetTitle(), fCanvasSize[0], fCanvasSize[1]);
  }

  // Work only kNpad2==4
  if (kNpad2 != 4) {
    Utils::Printf(Utils::kPrintError, "kNpad2 is not 4");
    return;
  }

  // For Pad
  const Double_t padsize[kNpad2][4] = {
      {0.0, 0.77, 1.00, 1.00}, {0.0, 0.50, 1.0, 0.77}, {0.0, 0.30, 1.0, 0.50}, {0.0, 0.0, 1.0, 0.30}};

  for (int i = 0; i < kNpad2; i++) {
    fCanvas2->cd();
    if (!fPad2[i]) {
      fPad2[i] = new TPad(TString::Format("%s_pad%d", this->GetName(), i), "Pad ", padsize[i][0], padsize[i][1],
                          padsize[i][2], padsize[i][3]);
      fPad2[i]->SetBorderSize(1);
      fPad2[i]->Draw();
    }
  }

  TText *t;
  double x, y;

  // Flags ----------------------------------
  fPad2[0]->cd();

  t = new TText();
  t->SetTextSize(0.090);
  t->SetTextColor(kBlack);
  t->DrawTextNDC(0.01, 0.88, "FLAG0 :");
  t->DrawTextNDC(0.01, 0.60, "FLAG1 :");
  t->DrawTextNDC(0.01, 0.32, "FLAG2 :");

  for (int i = 0; i < 32; i++) {
    if (i < 16) {
      x = 0.10 + i * 0.056;
      y = 0.80;
    } else {
      x = 0.10 + (i - 16) * 0.056;
      y = 0.70;
    }
    fFlagBit[0][i]->SetNDC();
    fFlagBit[0][i]->SetText(x, y, this->kFlagBitLabel[i]);
    fFlagBit[0][i]->SetTextSize(0.080);
    fFlagBit[0][i]->SetTextFont(102);
    fFlagBit[0][i]->Draw();
  }

  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      char text[16];
      if (it == 0) {
        sprintf(text, "TS-%02d", il);
      }
      if (it == 1) {
        sprintf(text, "TL-%02d", il);
      }
      x = 0.10 + il * 0.056;
      y = 0.50 - 0.10 * it;
      fFlagBit[1][il + 16 * it]->SetNDC();
      fFlagBit[1][il + 16 * it]->SetText(x, y, text);
      fFlagBit[1][il + 16 * it]->SetTextSize(0.080);
      fFlagBit[1][il + 16 * it]->SetTextFont(102);
      fFlagBit[1][il + 16 * it]->Draw();
    }
  }

  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      char text[16];
      if (it == 0) {
        sprintf(text, "TS-%02d", il);
      }
      if (it == 1) {
        sprintf(text, "TL-%02d", il);
      }
      x = 0.10 + il * 0.056;
      y = 0.25 - 0.10 * it;
      fFlagBit[2][il + 16 * it]->SetNDC();
      fFlagBit[2][il + 16 * it]->SetText(x, y, text);
      fFlagBit[2][il + 16 * it]->SetTextSize(0.080);
      fFlagBit[2][il + 16 * it]->SetTextFont(102);
      fFlagBit[2][il + 16 * it]->Draw();
    }
  }

  // Counters ----------------------------------
  fPad2[1]->cd();
  t->SetTextSize(0.090);
  t->DrawTextNDC(0.01, 0.88, "Counters");

  t->SetTextSize(0.075);
  t->SetTextFont(102);
  for (int i = 0; i < this->kNcounter; ++i) {
    x = 0.02 + 0.110 * (i % 8);
    y = 0.62 - 0.18 * (i / 8);
    fCounters[i]->SetTextSize(0.075);
    fCounters[i]->SetTextFont(102);
    fCounters[i]->SetX(x);
    fCounters[i]->SetY(y);
    fCounters[i]->Draw();
    // label
    x = 0.02 + 0.110 * (i % 8);
    y = 0.72 - 0.18 * (i / 8);
    t->SetTextColor(kGray + 2);
    t->DrawTextNDC(x, y, this->kCounterLabel[i]);
  }

  // FIFO counter
  t->SetTextColor(kBlack);
  t->SetTextSize(0.090);
  t->DrawTextNDC(0.01, 0.15, "FIFO Counters");
  for (int i = 0; i < this->kNfifo1; ++i) {
    for (int j = 0; j < this->kNfifo2; j++) {
      int pos = i * this->kNfifo2 + j;
      if (i == 1) pos += 1;
      x = 0.10 + 0.10 * pos + (pos / 4) * 0.05;
      y = 0.05;
      fFifoCounters[i][j]->SetTextSize(0.075);
      fFifoCounters[i][j]->SetTextFont(102);
      fFifoCounters[i][j]->SetX(x);
      fFifoCounters[i][j]->SetY(y);
      fFifoCounters[i][j]->Draw();
    }
  }

  // TDC
  fPad2[3]->cd();

  t->SetTextColor(kBlack);
  t->SetTextSize(0.090);
  t->DrawTextNDC(0.01, 0.92, "TDC");

  for (int ich = 0; ich < this->kTdcNch; ich++) {
    t->SetTextSize(0.075);
    t->DrawTextNDC(0.01, 0.85 - 0.068 * ich, this->kTdcLabel[ich]);
    for (int ihit = 0; ihit < this->kTdcNhit; ihit++) {
      x = 0.15 + 0.056 * ihit;
      y = 0.85 - 0.068 * ich - 0.02 * (ich / 4);
      fTdc[ich][ihit]->SetX(x);
      fTdc[ich][ihit]->SetY(y);
      fTdc[ich][ihit]->SetTextSize(0.070);
      fTdc[ich][ihit]->SetTextFont(102);
      fTdc[ich][ihit]->Draw();
    }
  }

  return;
}

template <typename armclass, typename armrec>
Double_t EventDisplay<armclass, armrec>::ConvertDepthToLayer(Double_t val) {
  if (val < this->kDoubleStepLayer * this->kSampleStep)
    return val / this->kSampleStep - 1.;
  else
    return (val - this->kDoubleStepLayer * this->kSampleStep) / (2 * this->kSampleStep) + this->kDoubleStepLayer - 1.;
}

/////////////////////////////////////////////////
/// Update the canvas.
/// \tparam armclass
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::DrawLvl3onCanvas1() {
  static int icall = 0;
  static vector<vector<TLatex *>> fLabelCal;        // Text for reconstructed energies
  static vector<vector<TMarker *>> fL20L90;         // L20% L90%
  static vector<vector<vector<TGraph *>>> fPosFit;  // position fit results
  if (icall == 0) {
    Utils::AllocateVector2D(fLabelCal, this->kCalNtower, 2);
    Utils::ClearVector2D(fLabelCal, (TLatex *)nullptr);
    Utils::AllocateVector2D(fL20L90, this->kCalNtower, 4); // L20, L90 x Photon or Neutron
    Utils::ClearVector2D(fL20L90, (TMarker *)nullptr);
    Utils::AllocateVector3D(fPosFit, this->kCalNtower, this->kPosNlayer, this->kPosNview);
    Utils::ClearVector3D(fPosFit, (TGraph *)nullptr);
  } else {
    // Clear the old information
    // Reconstructed energies
    for (int itower = 0; itower < this->kCalNtower; ++itower)
      for (int i = 0; i < 2; ++i)
        if (fLabelCal[itower][i] != nullptr) {
          delete fLabelCal[itower][i];
        }

    // L20, L90
    for (int itower = 0; itower < this->kCalNtower; ++itower)
      for (int i = 0; i < 4; ++i)
        if (fL20L90[itower][i] != nullptr) {
          delete fL20L90[itower][i];
        }

    // lateral fit result
    for (int iview = 0; iview < this->kPosNview; ++iview) {
      for (int itower = 0; itower < this->kCalNtower; ++itower) {
        for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer) {
          if (fPosFit[itower][ilayer][iview] != nullptr) delete fPosFit[itower][ilayer][iview];
        }
      }
    }
  }

  if (fLvl2flag && fLvl3flag) {
    // Reconstructed Energies
    for (int itower = 0; itower < this->kCalNtower; ++itower) {
      fPad[itower + 2]->cd();
      fLabelCal[itower][0] =
          new TLatex(0.75, 0.82, TString::Format("E_{rec,1#gamma} = %d GeV", (Int_t)fLvl3.fPhotonEnergy[itower]));
      fLabelCal[itower][1] =
          new TLatex(0.75, 0.72, TString::Format("E_{rec,1n} = %d GeV", (Int_t)fLvl3.fNeutronEnergy[itower]));

      // PID cut
      fLabelCal[itower][0]->SetTextColor(fLvl3.fIsPhoton[itower] ? kBlack : kGray);
      fLabelCal[itower][1]->SetTextColor(fLvl3.fIsNeutron[itower] ? kBlack : kGray);

      for (int i = 0; i < 2; ++i) {
        fLabelCal[itower][i]->SetNDC();
        fLabelCal[itower][i]->SetTextSize(0.06);
        fLabelCal[itower][i]->Draw();
      }
    }

    // L20, L90
    for (int itower = 0; itower < this->kCalNtower; ++itower) {
      fPad[itower + 2]->cd();
      Double_t ymin = fPad[itower + 2]->GetUymin();
      fL20L90[itower][0] = new TMarker(ConvertDepthToLayer(fLvl3.fPhotonL20[itower])+0.5, ymin, 20);
      fL20L90[itower][1] = new TMarker(ConvertDepthToLayer(fLvl3.fPhotonL90[itower])+0.5, ymin, 21);
      fL20L90[itower][2] = new TMarker(ConvertDepthToLayer(fLvl3.fNeutronL20[itower])+0.5, ymin, 24);
      fL20L90[itower][3] = new TMarker(ConvertDepthToLayer(fLvl3.fNeutronL90[itower])+0.5, ymin, 25);
      for (int i = 0; i < 4; ++i) {
        fL20L90[itower][i]->SetMarkerSize(1.0);
        fL20L90[itower][i]->SetMarkerColor(kRed);
        fL20L90[itower][i]->Draw();
      }
    }

    // Position Fit Result
    for (int iview = 0; iview < this->kPosNview; ++iview) {
      for (int itower = 0; itower < this->kCalNtower; ++itower) {
        for (int ilayer = 0; ilayer < this->kPosNlayer; ++ilayer) {
          if (this->kPosNtower == this->kCalNtower)
            fPadPos[iview][itower]->cd();
          else
            fPadPos[iview][0]->cd();

          // To-do only for Arm1 Photon
          const int step = (this->kArmIndex == Arm1Params::kArmIndex ? 4 : 1);
          int n = 0;
          Double_t tmpx, tmpy;
          vector<Double_t> x, y;
          for (int i = 0; i < this->kPosNchannel[itower]; ++i) {
            for (int j = 0; j < step; j++) {
              tmpx = 0.5 + i + j * (1. / step);
              tmpy = fLvl3.PhotonHeight(itower, ilayer, iview, tmpx - 0.5);
              x.push_back(tmpx);
              y.push_back(tmpy);
              ++n;
            }
          }

          fPosFit[itower][ilayer][iview] = new TGraph(n, &x[0], &y[0]);
          fPosFit[itower][ilayer][iview]->SetLineStyle(7);
          fPosFit[itower][ilayer][iview]->SetLineColor(fPosLineColor[ilayer]);
          fPosFit[itower][ilayer][iview]->Draw("L");
        }
      }
    }
  }

  ++icall;
  return;
}

/////////////////////////////////////////////////
/// Update the canvas.
/// \tparam armclass
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Update(Option_t *option) {
  // Check the option
  Bool_t flag_lvl3 = false;
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("level3") || opt.Contains("lvl3")) flag_lvl3 = true;

  for (int i = 0; i < kNpad; ++i) {
    if (fPad[i]) {
      Utils::OptimizeHistYRange(fPad[i]);
      fPad[i]->Modified();
      fPad[i]->Update();
    }
  }
  for (int i = 0; i < this->kNxy; ++i) {
    for (int j = 0; j < kNpadPos; ++j) {
      if (fPadPos[i][j]) {
        Utils::OptimizeHistYRange(fPadPos[i][j]);
        fPadPos[i][j]->Modified();
        fPadPos[i][j]->Update();

        if (this->kPosNsample != 1) {  // Draw the lines on PACE edges
          fPadPos[i][j]->cd();
          Double_t ymin = this->fHistPosDet[j][0][i][0]->GetMinimum();
          Double_t ymax = this->fHistPosDet[j][0][i][0]->GetMaximum();
          for (int ichip = 0; ichip < 12; ++ichip) {
            fLine[ichip]->SetY1(ymin);
            fLine[ichip]->SetY2(ymax);
            fLine[ichip]->Draw("SAME");
          }
        }

        fPadPos[i][j]->Modified();
        fPadPos[i][j]->Update();
      }
    }
  }
  if (fCanvas1) {
    fCanvas1->Modified();
    fCanvas1->Update();

    if (flag_lvl3) {
      DrawLvl3onCanvas1();
    }
  }

  // Canvas 2
  for (int i = 0; i < kNpad2; ++i) {
    if (fPad2[i]) {
      fPad2[i]->Modified();
      fPad2[i]->Update();
    }
  }

  if (fCanvas2) {
    fCanvas2->Modified();
    fCanvas2->Update();
  }

  return;
}

/////////////////////////////////////////////////
/// Make a figure file with a format of eps or pdf or ....
/// \tparam armclass
/// \param filename [in] Filename
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Print(const char *filename) {
  fCanvas1->Print(filename);
  return;
}

/////////////////////////////////////////////////
/// Make a figure file with a format of eps or pdf or ....
/// \tparam armclass
/// \param filename [in] Filename
/// \param option [in] Option for TCanvas::Print(filename,option);
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::Print(const char *filename, Option_t *option) {
  fCanvas1->Print(filename, option);
  return;
}

////////////////////////////////////////////////
/// Draw the text on (x,y)
/// \tparam armclass
/// \param t [in,out]
/// \param x [in]      X in NDC
/// \param y [in]      Y in NDC
template <typename armclass, typename armrec>
void EventDisplay<armclass, armrec>::DrawText(TLatex *t, Double_t x, Double_t y, Double_t size, Font_t font) {
  t->SetNDC();
  t->SetTextSize(size);
  t->SetTextFont(font);
  t->SetX(x);
  t->SetY(y);
  t->Draw();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventDisplay<Arm1Params, Arm1RecPars>;
template class EventDisplay<Arm2Params, Arm2RecPars>;
}  // namespace nLHCf
