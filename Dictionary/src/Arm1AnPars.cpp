#include "Arm1AnPars.hh"

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(Arm1AnPars);
#endif

typedef Utils UT;
typedef CoordinateTransformation CT;

/* Analysis constants */
Double_t Arm1AnPars::fFiducial = 2.;     // fiducial border [mm]
Double_t Arm1AnPars::kEnergyThr = 200.;  // energy threshold [GeV]

Double_t Arm1AnPars::kPosLayerDepth[][2] = {
    {29.68, 30.74}, {54.70, 55.77}, {159.81, 160.88}, {213.16, 214.22}};  // depth of position detector layers

Int_t Arm1AnPars::kPhotonNrap = 6;                        // number of rapidity regions (photons)
Int_t Arm1AnPars::kNeutronNrap = 6;                       // number of rapidity regions (neutrons)
Int_t Arm1AnPars::kPionNrapTypeI = 6;                     // number of rapidity regions (type I pions)
Double_t Arm1AnPars::kPionRapLowTypeI = 9.2;              // lower rapidity value (type I pions)
Double_t Arm1AnPars::kPionRapHighTypeI = 10.4;            // higher rapidity value (type I pions)
Int_t Arm1AnPars::kPionNrapTypeII[] = {8, 4};             // number of rapidity regions (type II pions)
Double_t Arm1AnPars::kPionRapLowTypeII[] = {9.6, 8.6};    // lower rapidity value (type II pions)
Double_t Arm1AnPars::kPionRapHighTypeII[] = {11.2, 9.4};  // higher rapidity value (type II pions)

Double_t Arm1AnPars::kDetectorOffset[] = {0.000, -20.000};  // detector offset in true mc coordinates due to beam
                                                            // crossing angle
Double_t Arm1AnPars::kBeamCentre[] = {0., 0.};              // measured beam centre [mm]
Double_t Arm1AnPars::kBeamCentreAverageLayer[] = {0., 0.};  // layer where beam centre was measured
Double_t Arm1AnPars::kCrossingAngle = -145e-6;              // vertical crossing angle [rad] for LHC operations

Bool_t Arm1AnPars::kIsMC = false;    // is simulation not data
Bool_t Arm1AnPars::kIsFlat = false;  // is flat neutron simulation?

Double_t Arm1AnPars::kPionMassRange[3][2] = {{125.0, 145.0}, {125.0, 145.0}, {125.0, 145.0}};  // pi0 mass range
Double_t Arm1AnPars::kEnergyRescaleFactor[] = {1.0, 1.0};

TString Arm1AnPars::fPosAlignmentFile = "a1_pos_alignment.dat";

void Arm1AnPars::SetFiducialBorder(Double_t border) { fFiducial = border; }

void Arm1AnPars::SetFill(Int_t fill, Int_t subfill) {
  UT::Printf(UT::kPrintInfo, "Arm1AnPars: setting parameters for fill %d [%d]\n", fill, subfill);

  kIsFlat = false;

  /* This is the most generic case : To be used only for preliminary study */
  if (fill == 0) {
    kCrossingAngle = 0.;

    kBeamCentre[0] = 0.;
    kBeamCentre[1] = 0.;

    kBeamCentreAverageLayer[0] = 0;
    kBeamCentreAverageLayer[1] = 0;

    kDetectorOffset[0] = 0.;
    kDetectorOffset[1] = 0.;
  }
  /* SPS2022 */
  else if (fOperation == kSPS2022 || fOperation == kSPS2021 || fOperation == kSPS2015) {
    kCrossingAngle = 0.;

    kBeamCentre[0] = 0.;
    kBeamCentre[1] = 0.;

    kBeamCentreAverageLayer[0] = 0;
    kBeamCentreAverageLayer[1] = 0;

    kDetectorOffset[0] = 0.;
    kDetectorOffset[1] = 0.;
  }
  /* LHC 2025 */
  else if (fOperation == kLHC2025) {  // Meaningless
    kCrossingAngle = -145e-6;

    kBeamCentre[0] = 0.;
    kBeamCentre[1] = 0.;

    kBeamCentreAverageLayer[0] = 1;
    kBeamCentreAverageLayer[1] = 1;

    kDetectorOffset[0] = 0.;
    kDetectorOffset[1] = -20.;
  }
  /* LHC 2022 */
  else if (fOperation == kLHC2022) {
    kCrossingAngle = -145e-6;

    /*
     * Data
     */

    // Fill 8178 - Divided in four subgroups
    if (fill == 8178) {
      // TODO: Carefully estimate beam center for each of the five subfill in
      // Fill 8178! Fill 8178: Nominal position
      if (subfill == 1 || subfill == 4) {
        kBeamCentre[0] = -1.27;
        kBeamCentre[1] = 0.17;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* temporal result obtained from run80262 */
        kEnergyRescaleFactor[0] = 1.0;
        kEnergyRescaleFactor[1] = 1.0;
        //// This rescale factor for the result using SPS2022 conv table was
        /// obtained by Kitagami
        // kEnergyRescaleFactor[0] = 1.0 / (1. - 0.1359);
        // kEnergyRescaleFactor[1] = 1.0 / (1. - 0.1359);
      }
      // Fill 8178: 5mm Higher position
      else if (subfill == 2 || subfill == 3 || subfill == 5) {
        // Temporal value estimated for Group 1 with 5mm vertical-shift
        // assumption.
        kBeamCentre[0] = -1.27;
        kBeamCentre[1] = 0.17 - 5.0;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        /* temporal result obtained from run80262 */
        kEnergyRescaleFactor[0] = 1.0;
        kEnergyRescaleFactor[1] = 1.0;
        //// This rescale factor for the result using SPS2022 conv table was
        /// obtained by Kitagami
        // kEnergyRescaleFactor[0] = 1.0 / (1. - 0.1359);
        // kEnergyRescaleFactor[1] = 1.0 / (1. - 0.1359);
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }
      // Fill 8181 - Only at nominal position
    } else if (fill == 8181) {
      // TODO: Carefully estimate beam center for Fill 8181!
      kBeamCentre[0] = 0.;
      kBeamCentre[1] = 0.;

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

      /* temporal result obtained from run80262 */
      kEnergyRescaleFactor[0] = 135.0 / 96.08;
      kEnergyRescaleFactor[1] = 135.0 / 96.08;
    }

    /*
     * MC
     */

    // Fill 8178 - Divided in four subgroups
    else if (fill == -8178) {
      if (subfill == -1 || subfill == -4) {
        kBeamCentre[0] = -1.27;
        kBeamCentre[1] = 0.17;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.0;
        kEnergyRescaleFactor[1] = 1.0;
      }
      // Fill 8178: 5mm Higher position
      else if (subfill == -2 || subfill == -3 || subfill == -5) {
        // Temporal value estimated for Group 1 with 5mm vertical-shift
        // assumption.
        kBeamCentre[0] = -1.27;
        kBeamCentre[1] = 0.17 - 5.0;

        kBeamCentreAverageLayer[0] = 1;
        kBeamCentreAverageLayer[1] = 1;

        kDetectorOffset[0] = 0.;
        kDetectorOffset[1] = -20.;

        kEnergyRescaleFactor[0] = 1.0;
        kEnergyRescaleFactor[1] = 1.0;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }
      // Fill 8181 - Only at nominal position
    } else if (fill == -8181) {
      // TODO: Carefully estimate beam center for Fill 8181!
      kBeamCentre[0] = 0.;
      kBeamCentre[1] = 0.;

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

      kEnergyRescaleFactor[0] = 1.0;
      kEnergyRescaleFactor[1] = 1.0;
    }

    //

    else {
      UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  }
  /* LHC 2015 */
  else if (fOperation == kLHC2015) {
    kCrossingAngle = -145e-6;

    /*
     * Data
     */

    /* Fill 3855 (detector in nominal position) */
    if (fill == 3855) {
      if (subfill == 1) {  // pile-up = 0.01
        kBeamCentre[0] = -2.03;
        kBeamCentre[1] = 1.41;
      } else if (subfill == 2) {  // pile-up = 0.03
        kBeamCentre[0] = -2.03;
        kBeamCentre[1] = 1.41;
      } else {
        UT::Printf(UT::kPrintError, "Arm1AnPars::SetFill: unknown subfill, doing nothing...\n");
        return;
      }

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;  // TODO [TrackProjection]:Arm1 - Which are the correct x and y
                                       // average layers?
      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

      kEnergyRescaleFactor[0] = 1.035;
      kEnergyRescaleFactor[1] = 1.035;
    }
    /* Fill 3851 (detector shifted up by 5 mm) */
    else if (fill == 3851) {
      if (subfill == 0) {  // pure pi0 MC
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0.;
      } else if (subfill == 1) {  // pile-up = 0.01
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0.;
      } else if (subfill == -1) {  // simulations (beam center is slightly different)
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0. - 0.45670730;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -15.;

      /* Temporal, the energy scale factor for Fill 3851 is not checked yet. */
      kEnergyRescaleFactor[0] = 1.035;
      kEnergyRescaleFactor[1] = 1.035;
    }

    /*
     * MC
     */

    else if (fill == -3855) {
      if (subfill == 0) {  // pure pi0 MC
        kBeamCentre[0] = -2.03;
        kBeamCentre[1] = 1.41;
      } else if (subfill == -1) {  // simulations (beam center is slightly different)
        kBeamCentre[0] = 0.94;
        kBeamCentre[1] = 1.10;
        //    1.41 - 0.45670730;     // TODO [TrackProjection]:Arm1 - Fixed by
        //    Eugenio for kBeamCentreAverageLayer[1] = 1
      } else if (subfill == -2) {  // flat simulations
        kIsFlat = true;
        kCrossingAngle = 0.;
        //
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0.;
      } else {
        UT::Printf(UT::kPrintError, "Arm1AnPars::SetFill: unknown subfill, doing nothing...\n");
        return;
      }

      if (kIsFlat) {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      } else {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -20.;

    } else if (fill == -3851) {
      if (subfill == 0) {  // pure pi0 MC
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0.;
      } else if (subfill == -1) {  // simulations (beam center is slightly different)
        kBeamCentre[0] = 0.;
        kBeamCentre[1] = 0. - 0.45670730;
      } else {
        UT::Printf(UT::kPrintError, "Arm2AnPars::SetFill: unknown subfill, exit...\n");
        exit(EXIT_FAILURE);
      }

      if (kIsFlat) {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      } else {
        kEnergyRescaleFactor[0] = 1.;
        kEnergyRescaleFactor[1] = 1.;
      }

      kBeamCentreAverageLayer[0] = 1;
      kBeamCentreAverageLayer[1] = 1;

      kDetectorOffset[0] = 0.;
      kDetectorOffset[1] = -15.;

    } else {
      UT::Printf(UT::kPrintError, "ArmqAnPars::SetFill: unknown fill, exit...\n");
      exit(EXIT_FAILURE);
    }
  } else {
    UT::Printf(UT::kPrintError, "ArmqAnPars::SetFill: unknown fill, exit...\n");
    exit(EXIT_FAILURE);
  }

  // Print parameters
  UT::Printf(UT::kPrintInfo, "        Energy rescale factors for TS and TL = %6.4lf, %6.4lf\n", kEnergyRescaleFactor[0],
             kEnergyRescaleFactor[1]);

  // IMPORTANT: Set Coordinate Transformation parameters here!
  CT::SetBeamCrossingAngle(0, kCrossingAngle);
  CT::SetDetectorPosition(0, kDetectorOffset[1]);
  CT::SetMeasuredBeamCentre(0,                               //
                            kBeamCentre[0], kBeamCentre[1],  //
                            kBeamCentreAverageLayer[0], kBeamCentreAverageLayer[1]);
  CT::SetDetectorPositionVector(0);
  CT::SetBeamCentreVector(0);
  if (!kIsMC) {
    if (fOperation == kLHC2022) {
      CT::SetAlignmentValues(0);
    }
  }
  //
  CT::PrintParameters(0, "Arm1AnPars");

  return;
}
