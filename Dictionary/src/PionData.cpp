#include "PionData.hh"

#include "Arm1AnPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"

using namespace nLHCf;

typedef CoordinateTransformation CT;

#if !defined(__CINT__)
ClassImp(PionData);
#endif

void PionData::Clear() {
  fType = PionType::kNone;
  fMomentum.SetPx(0.);
  fMomentum.SetPy(0.);
  fMomentum.SetPz(0.);
  fMomentum.SetE(0.);
  fIsTrue = false;

  fFlag = 0;
  for (int i = 0; i < 2; ++i) {
    fPhotonTower[i] = -1;
    fPhotonEnergy[i] = 0.;
    fPhotonPos[i][0] = -1.;
    fPhotonPos[i][0] = -1.;
  }
  fPhotonR = -1.;

  fTrueMH = false;
}
