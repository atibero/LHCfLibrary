#ifndef SiliconLatency_HH
#define SiliconLatency_HH

#include <TCanvas.h>
#include <TChain.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TSpline.h>
#include <TLatex.h>
#include <TMultiGraph.h>
#include <TProfile.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>
#include <TSystem.h>

#include <cstdlib>
#include <fstream>
#include <iomanip>

#include "Arm1AnPars.hh"
#include "Arm1CalPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2CalPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "Utils.hh"

using namespace std;

namespace nLHCf {

template <typename armcal, typename armrec, typename arman, typename armclass>
class SiliconLatency : public armcal, public armrec, public arman, public LHCfParams {
 public:
  SiliconLatency();
  ~SiliconLatency();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;
  Int_t fInputLevel;
  Double_t fMinValue;
  Double_t fMaxValue;
  Double_t fMinEnergy;
  Double_t fMaxEnergy;

  TString fOutputName;
  TFile *fOutputFile;
  ofstream fOfstream;

  Bool_t fModify;

  Bool_t fMaximumLayer;
  Bool_t fVertexStrip;
  Bool_t fPhoton;
  Bool_t fNeutron;
  Bool_t fSinglehit;

  Bool_t fTDC;
  Bool_t fCorrection;
  Bool_t fEqualize;

  /*--- Level0/1/2 ---*/
  Level0<armclass> *fLvl0;
  Level0<armclass> *fPed0;
  Level1<armclass> *fLvl1;
  Level2<armclass> *fLvl2;
  Level3<armrec> *fLvl3;

  /*--- Histograms ---*/

  // Hitmap
  vector<vector<TH1D *> > fHitmap;
  // Sample signal
  vector<TH1D *> fSampleSignalTotal;
  vector<vector<vector<TH1D *> > > fSampleSignalLayer;
  vector<vector<vector<vector<TH1D *> > > > fSampleSignalChip;
  // Position dependence of sample signal
  vector<vector<vector<TH2D *> > > fPositionVsSignal;
  vector<vector<vector<TH2D *> > > fPosVsSigWeight;
  vector<vector<vector<TH2D *> > > fPosVsEneWeight;
  // Sample ratio
  vector<TH1D *> fSampleRatioTotal;
  vector<vector<vector<TH1D *> > > fSampleRatioLayer;
  vector<vector<vector<vector<TH1D *> > > > fSampleRatioChip;
  // Sample signal-TDC correlation
  vector<TH2D *> fTDCvsSignalTotal;
  vector<vector<vector<TH2D *> > > fTDCvsSignalLayer;
  vector<vector<vector<vector<TH2D *> > > > fTDCvsSignalChip;
  vector<vector<vector<TProfile *> > > fSubTDCvsRefSignal;
  // Sample ratio-TDC correlation
  vector<TH2D *> fTDCvsRatioTotal;
  vector<vector<vector<TH2D *> > > fTDCvsRatioLayer;
  vector<vector<vector<vector<TH2D *> > > > fTDCvsRatioChip;
  vector<vector<vector<TProfile *> > > fSubTDCvsRefRatio;
  // Sample signal-TDC correlation projection
  vector<TProfile *> fProfileSignalTotal;
  vector<vector<vector<TProfile *> > > fProfileSignalLayer;
  vector<vector<vector<vector<TProfile *> > > > fProfileSignalChip;
  // Position dependence of sample projection
  vector<vector<vector<TProfile *> > > fProfilePositionSignal;
  vector<vector<vector<TProfile *> > > fProfilePosSigWeight;
  vector<vector<vector<TProfile *> > > fProfilePosEneWeight;
  vector<vector<vector<TProfile *> > > fProfilePosSigAverage;
  // Sample ratio-TDC correlation projection
  vector<TProfile *> fProfileRatioTotal;
  vector<vector<vector<TProfile *> > > fProfileRatioLayer;
  vector<vector<vector<vector<TProfile *> > > > fProfileRatioChip;
  // Sample signal-TDC correlation
  vector<vector<vector<vector<TH2D *> > > > fTDCvsShaperPerSample;
  vector<vector<vector<TH2D *> > > fTDCvsShaperGlobal;
  // Sample signal-TDC correlation projection
  vector<vector<vector<vector<TProfile *> > > > fProfileShaperPerSample;
  vector<vector<vector<TProfile *> > > fProfileShaperGlobal;
  // Variables needed for correction
  vector<vector<vector<TProfile *> > > fTDCSignalForCorrection;
  vector<vector<vector<TProfile *> > > fTDCRatioForCorrection;
  vector<vector<vector<TH1D *> > > fTDCCorrection;
  vector<vector<vector<TGraphErrors *> > > fRatioVsSignal;
  vector<vector<vector<TSpline3 *> > > fTDCSignalSpline;
  vector<vector<vector<TSpline3 *> > > fTDCRatioSpline;
  vector<vector<vector<TSpline3 *> > > fTDCCorrectionSpline;
  // Sample 1 TDC time correction
  vector<vector<vector<TH1D *> > > fTDCvsection;
  // Sample signal-TDC correlation
  vector<vector<vector<TH2D *> > > fPositionDependence;
  // Energy reconstructed using GSO
  vector<TH1D *> fParticleEnergy;

  vector<vector<vector<Int_t> > > fSiliconRange;

  TH2D *fTDCCorrelation;
  TH1D *fTDCDifference;

  vector<TString> fXY;
  vector<TString> fUnit;

  const Double_t fLHCPeriod = 24.950100;  // ns

  //static const Int_t fSampleRef = 0;  // 1 - Front calibration, 0 - Back calibration
  static const Int_t fSampleRef = 1;  // 1 - Front calibration, 0 - Back calibration
  static const Int_t fChipRef = 9;
  static const Int_t fBinRef = 5;

  const Double_t fTDCRef = -104.5;
  const Double_t fTDCMin = -127.0;
  const Double_t fTDCMax = -103.0;
  const Int_t fTDCSubBin = 8;

  Double_t fTDCStep = 3;
  vector<Double_t> fTDCSubAve;
  vector<Double_t> fTDCSubMin;
  vector<Double_t> fTDCSubMax;

  static const Int_t fNPacePerHybrid = 12;
  static const Int_t fNPaceChannels = 32;
  static const Int_t fNHybridChannels = fNPacePerHybrid * fNPaceChannels;

  const Double_t fZoomMin = 0.0;
  const Double_t fZoomMax = 0.5;

  TColor *fColor[fNPacePerHybrid] = {
      gROOT->GetColor(kBlack),      gROOT->GetColor(kRed + 1),   gROOT->GetColor(kGreen + 1),
      gROOT->GetColor(kBlue),       gROOT->GetColor(kOrange),    gROOT->GetColor(kMagenta),
      gROOT->GetColor(kCyan),       gROOT->GetColor(kAzure + 1), gROOT->GetColor(kPink + 1),
      gROOT->GetColor(kViolet + 1), gROOT->GetColor(kGray),      gROOT->GetColor(kGray + 2)};

  Int_t fPACEID[fNPacePerHybrid] = {0, 2, 4, 6, 8, 10, 11, 9, 7, 5, 3, 1};

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SetInputFile(const Char_t *name) { fInputFile = name; }
  void SetInputLevel(const Int_t level) { fInputLevel = level; }
  void SetStripThreshold(const Double_t minthr, const Double_t maxthr) {
    fMinValue = minthr;
    fMaxValue = maxthr;
  }
  void SetEnergyThreshold(const Double_t minene, const Double_t maxene) {
    fMinEnergy = minene;
    fMaxEnergy = maxene;
  }
  void SelectMaximumLayer() { fMaximumLayer = true; }
  void SelectVertexStrip() { fVertexStrip = true; }
  void SelectPhoton() { fPhoton = true; }
  void SelectNeutron() { fNeutron = true; }
  void SelectSinglehit() { fSinglehit = true; }
  void AddTDC() { fTDC = true; }
  void ComputeCorrection() { fCorrection = true; }
  void ComputeEqualization() { fEqualize = true; }

  void Find();

 private:
  void SetInputTree();
  void InitTDCSubBins();
  void InitSiliconRange();
  void MakeHistograms();
  void ClearEvent();
  void EventLoop();
  void GetProfile();
  void GetProfilePosition();
  void GetProfileTDC();
  void GetCorrection();
  void SaveS0S1Ratio();
  void PlotHisto();
  void PlotPositionHisto();
  void PlotHitmapHisto();
  void PlotLayerSignalHisto();
  void PlotChipSignalHisto();
  void PlotLayerRatioHisto();
  void PlotChipRatioHisto();
  void PlotSubTDCSignalHisto();
  void PlotSubTDCRatioHisto();
  void PlotSubTDCHisto();
  void PlotSignalHisto();
  void PlotRatioHisto();
  void PlotCorrelationHisto();
  void PlotCorrectionHisto();
  void WriteToOutput();
  void CloseFiles();

  TString GetPathName(TString sin);
  TGraph *HistoToGraph(TH1 *histo);
  TSpline3 *HistoToSpline(TH1 *histo);
  Int_t FromStripToTower(Int_t view, Int_t strip);
};
}  // namespace nLHCf
#endif
