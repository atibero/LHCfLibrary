#include "SiliconLatency.hh"

#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
SiliconLatency<armcal, armrec, arman, armclass>::SiliconLatency()
    : fFirstRun(-1),
      fLastRun(-1),
      fInputLevel(-1),
      fMinValue(0.0),
      fMaxValue(1e6),
      fMinEnergy(0.0),
      fMaxEnergy(1e6),
      fModify(true),
      fMaximumLayer(false),
      fVertexStrip(false),
      fPhoton(false),
      fNeutron(false),
      fSinglehit(false),
      fTDC(false),
      fCorrection(false),
      fEqualize(false) {
  fOutputFile = nullptr;
  fInputEv = nullptr;

  fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
SiliconLatency<armcal, armrec, arman, armclass>::~SiliconLatency() {
  delete fOutputFile;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      Bool_t isFound = false;
      // Check for calibration run
      TString calname(Form("%s/cal_run%05d.root", fInputDir.Data(), irun));
      if (!gSystem->AccessPathName(calname.Data())) {
        TFile calfile(calname.Data(), "READ");
        if (calfile.IsZombie()) {
          calfile.Close();
        } else {
          calfile.Close();
          fInputTree->Add(calname.Data());
          isFound = true;
        }
      }
      if (isFound) {
        continue;
      }
      TString recname(Form("%s/rec_run%05d.root", fInputDir.Data(), irun));
      if (!gSystem->AccessPathName(recname.Data())) {
        TFile recfile(recname.Data(), "READ");
        if (recfile.IsZombie()) {
          recfile.Close();
        } else {
          recfile.Close();
          fInputTree->Add(recname.Data());
          isFound = true;
        }
      }
      if (!isFound) {
        UT::Printf(UT::kPrintInfo, "Run %05d cannot be found\n", irun);
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::InitSiliconRange() {
  Utils::AllocateVector3D(fSiliconRange, this->kCalNtower, this->kPosNview, 2);

  fSiliconRange[0][0][0] = 218;
  fSiliconRange[0][0][1] = 375;  // ST X
  fSiliconRange[0][1][0] = 219;
  fSiliconRange[0][1][1] = 376;  // ST Y
  fSiliconRange[1][0][0] = 6;
  fSiliconRange[1][0][1] = 207;  // LT X
  fSiliconRange[1][1][0] = 8;
  fSiliconRange[1][1][1] = 209;  // LT Y
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::InitTDCSubBins() {
  UT::AllocateVector1D(fTDCSubAve, fTDCSubBin);
  UT::AllocateVector1D(fTDCSubMin, fTDCSubBin);
  UT::AllocateVector1D(fTDCSubMax, fTDCSubBin);

  fTDCStep = (fTDCMax - fTDCMin) / fTDCSubBin;

  for (Int_t ib = 0; ib < fTDCSubBin; ++ib) {
    fTDCSubMin[ib] = fTDCMin + ib * fTDCStep;
    fTDCSubMax[ib] = fTDCMin + (ib + 1.0) * fTDCStep;
    fTDCSubAve[ib] = fTDCMin + (ib + 0.5) * fTDCStep;
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::MakeHistograms() {
  UT::AllocateVector1D(fXY, this->kPosNview);
  fXY[0] = "X";
  fXY[1] = "Y";

  UT::AllocateVector1D(fUnit, 2);
  fUnit[0] = "ADC";
  fUnit[1] = "GeV";

  UT::AllocateVector2D(fHitmap, this->kPosNlayer, this->kPosNview);
  //
  UT::AllocateVector1D(fSampleSignalTotal, this->kPosNsample);
  UT::AllocateVector3D(fSampleSignalLayer, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector4D(fSampleSignalChip, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
  if (fEqualize) {
    UT::AllocateVector3D(fPositionVsSignal, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    UT::AllocateVector3D(fPosVsSigWeight, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    UT::AllocateVector3D(fPosVsEneWeight, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  }
  if (fInputLevel == 1) {
    UT::AllocateVector1D(fSampleRatioTotal, this->kPosNsample);
    UT::AllocateVector3D(fSampleRatioLayer, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    UT::AllocateVector4D(fSampleRatioChip, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
  }
  if (fTDC) {
    UT::AllocateVector1D(fTDCvsSignalTotal, this->kPosNsample);
    UT::AllocateVector3D(fTDCvsSignalLayer, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    UT::AllocateVector4D(fTDCvsSignalChip, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
    UT::AllocateVector3D(fSubTDCvsRefSignal, this->kPosNlayer, this->kPosNview, fTDCSubBin);
    UT::AllocateVector1D(fTDCvsRatioTotal, this->kPosNsample);
    UT::AllocateVector3D(fTDCvsRatioLayer, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    UT::AllocateVector4D(fTDCvsRatioChip, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
    UT::AllocateVector3D(fSubTDCvsRefRatio, this->kPosNlayer, this->kPosNview, fTDCSubBin);
    //
    UT::AllocateVector4D(fTDCvsShaperPerSample, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
    UT::AllocateVector3D(fTDCvsShaperGlobal, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
    //
    UT::AllocateVector3D(fPositionDependence, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  }
  //
  UT::AllocateVector1D(fParticleEnergy, this->kCalNtower);

  TString pos_title = this->kArmIndex == Arm1Params::kArmIndex ? "GSO bars" : "Silicon";

  Double_t dif_bin = 100;
  Double_t dif_min = -10;
  Double_t dif_max = +10.;

  Double_t tdc_bin = 250;
  Double_t tdc_min = -150;
  Double_t tdc_max = -100.;

  Double_t shp_bin = 500;
  Double_t shp_min = -200;
  Double_t shp_max = -100.;

  Double_t rat_bin = 300;
  Double_t rat_min = -1.;
  Double_t rat_max = +5.;

  Double_t dep_bin = fInputLevel == 1 ? 425 : 500;
  Double_t dep_min = fInputLevel == 1 ? 0. : 0.;
  Double_t dep_max = fInputLevel == 1 ? 8500. : 0.5;

  Double_t wgt_bin = fInputLevel == 1 ? 500 : 500;
  Double_t wgt_min = fInputLevel == 1 ? 0. : 0.;
  Double_t wgt_max = fInputLevel == 1 ? 6000000. : 3500.;

  Double_t skw_bin = 500;
  Double_t skw_min = -10.;
  Double_t skw_max = +10.;

  Double_t ene_bin = 560;
  Double_t ene_min = 0.;
  Double_t ene_max = 14000.;

  if (this->fOperation == kLHC2022) {
    rat_bin *= 2.;
  }

  for (UInt_t il = 0; il < fHitmap.size(); ++il) {
    for (UInt_t iv = 0; iv < fHitmap[il].size(); ++iv) {
      fHitmap[il][iv] =
          new TH1D(Form("Hitmap_Layer%d%s", il, fXY[iv].Data()), Form("Layer %d%s; Strip; Counts", il, fXY[iv].Data()),
                   fNHybridChannels, -0.5, fNHybridChannels - 0.5);
      fHitmap[il][iv]->SetLineColor(fColor[il]->GetNumber());
    }
  }
  //
  for (UInt_t is = 0; is < fSampleSignalTotal.size(); ++is) {
    fSampleSignalTotal[is] =
        new TH1D(Form("TotalSignal_Sample%d", is),
                 Form("Sample %d; Signal[%s]; Counts", is, fUnit[fInputLevel - 1].Data()), dep_bin, dep_min, dep_max);
    fSampleSignalTotal[is]->SetLineColor(fColor[0]->GetNumber());
  }
  for (UInt_t il = 0; il < fSampleSignalLayer.size(); ++il) {
    for (UInt_t iv = 0; iv < fSampleSignalLayer[il].size(); ++iv) {
      for (UInt_t is = 0; is < fSampleSignalLayer[il][iv].size(); ++is) {
        fSampleSignalLayer[il][iv][is] = new TH1D(
            Form("LayerSignal_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
            Form("Layer %d%s - Sample%d; Signal[%s]; Counts", il, fXY[iv].Data(), is, fUnit[fInputLevel - 1].Data()),
            dep_bin, dep_min, dep_max);
        fSampleSignalLayer[il][iv][is]->SetLineColor(fColor[il * this->kPosNview + iv]->GetNumber());
      }
    }
  }
  for (UInt_t il = 0; il < fSampleSignalChip.size(); ++il) {
    for (UInt_t iv = 0; iv < fSampleSignalChip[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fSampleSignalChip[il][iv].size(); ++ic) {
        for (UInt_t is = 0; is < fSampleSignalChip[il][iv][ic].size(); ++is) {
          fSampleSignalChip[il][iv][ic][is] =
              new TH1D(Form("ChipSignal_Layer%d%s_Chip%d_Sample%d", il, fXY[iv].Data(), ic, is),
                       Form("Layer %d%s - Chip %d - Sample %d; Signal[%s]; Counts", il, fXY[iv].Data(), ic, is,
                            fUnit[fInputLevel - 1].Data()),
                       dep_bin, dep_min, dep_max);
          fSampleSignalChip[il][iv][ic][is]->SetLineColor(fColor[ic]->GetNumber());
        }
      }
    }
  }
  //
  if (fEqualize) {
    for (UInt_t il = 0; il < fPositionVsSignal.size(); ++il) {
      for (UInt_t iv = 0; iv < fPositionVsSignal[il].size(); ++iv) {
        for (UInt_t is = 0; is < fPositionVsSignal[il][iv].size(); ++is) {
          fPositionVsSignal[il][iv][is] = new TH2D(
              Form("SignalVsPosition_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
              Form("Layer %d%s - Sample%d; Strip; dE [%s]", il, fXY[iv].Data(), is, fUnit[fInputLevel - 1].Data()),
              fNHybridChannels, -0.5, fNHybridChannels - 0.5, dep_bin, dep_min, dep_max);
          fPositionVsSignal[il][iv][is]->SetLineColor(fColor[il]->GetNumber());
        }
      }
    }
    for (UInt_t il = 0; il < fPosVsSigWeight.size(); ++il) {
      for (UInt_t iv = 0; iv < fPosVsSigWeight[il].size(); ++iv) {
        for (UInt_t is = 0; is < fPosVsSigWeight[il][iv].size(); ++is) {
          fPosVsSigWeight[il][iv][is] =
              new TH2D(Form("SignalVsPosition_Weighted_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
                       Form("Layer %d%s - Sample%d; Strip; Energy [GeV] * dE [%s]", il, fXY[iv].Data(), is,
                            fUnit[fInputLevel - 1].Data()),
                       fNHybridChannels, -0.5, fNHybridChannels - 0.5, wgt_bin, wgt_min, wgt_max);
          fPosVsSigWeight[il][iv][is]->SetLineColor(fColor[il]->GetNumber());
        }
      }
    }
    for (UInt_t il = 0; il < fPosVsEneWeight.size(); ++il) {
      for (UInt_t iv = 0; iv < fPosVsEneWeight[il].size(); ++iv) {
        for (UInt_t is = 0; is < fPosVsEneWeight[il][iv].size(); ++is) {
          fPosVsEneWeight[il][iv][is] =
              new TH2D(Form("EnergyVsPosition_Weighted_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
                       Form("Layer %d%s - Sample%d; Strip; Energy [GeV]", il, fXY[iv].Data(), is), fNHybridChannels,
                       -0.5, fNHybridChannels - 0.5, ene_bin, ene_min, ene_max);
          fPosVsEneWeight[il][iv][is]->SetLineColor(fColor[il]->GetNumber());
        }
      }
    }
  }
  //
  if (fInputLevel == 1) {
    for (UInt_t is = 0; is < fSampleRatioTotal.size(); ++is) {
      fSampleRatioTotal[is] = new TH1D(Form("TotalRatio_Sample%d", is),
                                       Form("; Sample%d/Sample%d; Counts", is, fSampleRef), rat_bin, rat_min, rat_max);
      fSampleRatioTotal[is]->SetLineColor(fColor[0]->GetNumber());
    }
    for (UInt_t il = 0; il < fSampleRatioLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fSampleRatioLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fSampleRatioLayer[il][iv].size(); ++is) {
          fSampleRatioLayer[il][iv][is] =
              new TH1D(Form("LayerRatio_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
                       Form("Layer %d%s; Sample%d/Sample%d; Counts", il, fXY[iv].Data(), is, fSampleRef), rat_bin,
                       rat_min, rat_max);
          fSampleRatioLayer[il][iv][is]->SetLineColor(fColor[il * this->kPosNview + iv]->GetNumber());
        }
      }
    }
    for (UInt_t il = 0; il < fSampleRatioChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fSampleRatioChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fSampleRatioChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fSampleRatioChip[il][iv][ic].size(); ++is) {
            fSampleRatioChip[il][iv][ic][is] = new TH1D(
                Form("ChipRatio_Layer%d%s_Chip%d_Sample%d", il, fXY[iv].Data(), ic, is),
                Form("Layer %d%s - Chip %d; Sample%d/Sample%d; Counts", il, fXY[iv].Data(), ic, is, fSampleRef),
                rat_bin, rat_min, rat_max);
            fSampleRatioChip[il][iv][ic][is]->SetLineColor(fColor[ic]->GetNumber());
          }
        }
      }
    }
  }
  //
  //
  //
  if (fTDC) {
    for (UInt_t is = 0; is < fTDCvsSignalTotal.size(); ++is) {
      fTDCvsSignalTotal[is] =
          new TH2D(Form("TotalSignalVsTDC_Sample%d", is), Form("Sample %d; TDC [ns]; Signal[ADC]", is), tdc_bin,
                   tdc_min, tdc_max, dep_bin, dep_min, dep_max);
      fTDCvsSignalTotal[is]->SetLineColor(fColor[0]->GetNumber());
    }
    for (UInt_t il = 0; il < fTDCvsSignalLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsSignalLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fTDCvsSignalLayer[il][iv].size(); ++is) {
          fTDCvsSignalLayer[il][iv][is] = new TH2D(Form("LayerSignalVsTDC_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
                                                   Form("Layer %d%s;TDC [ns]; Sample%d", il, fXY[iv].Data(), is),
                                                   tdc_bin, tdc_min, tdc_max, dep_bin, dep_min, dep_max);
          fTDCvsSignalLayer[il][iv][is]->SetLineColor(fColor[il * this->kPosNview + iv]->GetNumber());
        }
      }
    }
    for (UInt_t il = 0; il < fTDCvsSignalChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsSignalChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsSignalChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsSignalChip[il][iv][ic].size(); ++is) {
            fTDCvsSignalChip[il][iv][ic][is] =
                new TH2D(Form("ChipSignalVsTDC_Layer%d%s_Chip%d_Sample%d", il, fXY[iv].Data(), ic, is),
                         Form("Layer %d%s - Chip %d;TDC [ns]; Sample%d", il, fXY[iv].Data(), ic, is), tdc_bin, tdc_min,
                         tdc_max, dep_bin, dep_min, dep_max);
            fTDCvsSignalChip[il][iv][ic][is]->SetLineColor(fColor[ic]->GetNumber());
          }
        }
      }
    }
    for (UInt_t il = 0; il < fSubTDCvsRefSignal.size(); ++il) {
      for (UInt_t iv = 0; iv < fSubTDCvsRefSignal[il].size(); ++iv) {
        for (UInt_t ib = 0; ib < fSubTDCvsRefSignal[il][iv].size(); ++ib) {
          fSubTDCvsRefSignal[il][iv][ib] =
              new TProfile(Form("SignalVsTDC_Layer%d%s_Chip%d_Sample%d_TDC%.0fns", il, fXY[iv].Data(), fChipRef,
                                fSampleRef, fTDCSubAve[ib]),
                           Form("TDC %.0fns - Layer %d%s - Chip %d - Sample %d;Channel; <Signal [ADC]>", fTDCSubAve[ib],
                                il, fXY[iv].Data(), fChipRef, fSampleRef),
                           fNPaceChannels, -0.5, fNPaceChannels - 0.5, dep_min, dep_max);
          fSubTDCvsRefSignal[il][iv][ib]->SetLineColor(fColor[ib]->GetNumber());
        }
      }
    }
    //
    for (UInt_t is = 0; is < fTDCvsRatioTotal.size(); ++is) {
      fTDCvsRatioTotal[is] =
          new TH2D(Form("TotalRatioVsTDC_Sample%d", is), Form(";TDC [ns]; Sample%d/Sample%d", is, fSampleRef), tdc_bin,
                   tdc_min, tdc_max, rat_bin, rat_min, rat_max);
      fTDCvsRatioTotal[is]->SetLineColor(fColor[0]->GetNumber());
    }

    for (UInt_t il = 0; il < fTDCvsRatioLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsRatioLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fTDCvsRatioLayer[il][iv].size(); ++is) {
          fTDCvsRatioLayer[il][iv][is] =
              new TH2D(Form("LayerRatioVsTDC_Layer%d%s_Sample%d", il, fXY[iv].Data(), is),
                       Form("Layer %d%s;TDC [ns]; Sample%d/Sample%d", il, fXY[iv].Data(), is, fSampleRef), tdc_bin,
                       tdc_min, tdc_max, rat_bin, rat_min, rat_max);
          fTDCvsRatioLayer[il][iv][is]->SetLineColor(fColor[il * this->kPosNview + iv]->GetNumber());
        }
      }
    }
    for (UInt_t il = 0; il < fTDCvsRatioChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsRatioChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsRatioChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsRatioChip[il][iv][ic].size(); ++is) {
            fTDCvsRatioChip[il][iv][ic][is] = new TH2D(
                Form("ChipRatioVsTDC_Layer%d%s_Chip%d_Sample%d", il, fXY[iv].Data(), ic, is),
                Form("Layer %d%s - Chip %d;TDC [ns]; Sample%d/Sample%d", il, fXY[iv].Data(), ic, is, fSampleRef),
                tdc_bin, tdc_min, tdc_max, rat_bin, rat_min, rat_max);
            fTDCvsRatioChip[il][iv][ic][is]->SetLineColor(fColor[ic]->GetNumber());
          }
        }
      }
    }
    for (UInt_t il = 0; il < fSubTDCvsRefRatio.size(); ++il) {
      for (UInt_t iv = 0; iv < fSubTDCvsRefRatio[il].size(); ++iv) {
        for (UInt_t ib = 0; ib < fSubTDCvsRefRatio[il][iv].size(); ++ib) {
          fSubTDCvsRefRatio[il][iv][ib] = new TProfile(
              Form("RatioVsTDC_Layer%d%s_Chip%d_TDC%.0fns", il, fXY[iv].Data(), fChipRef, fSampleRef, fTDCSubAve[ib]),
              Form("TDC %.0fns - Layer %d%s - Chip %d;Channel; <Sample%d/Sample%d>", fTDCSubAve[ib], il, fXY[iv].Data(),
                   fChipRef, 1 - fSampleRef, fSampleRef),
              fNPaceChannels, -0.5, fNPaceChannels - 0.5, rat_min, rat_max);
          fSubTDCvsRefRatio[il][iv][ib]->SetLineColor(fColor[ib]->GetNumber());
        }
      }
    }
    //
    for (UInt_t il = 0; il < fTDCvsShaperPerSample.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsShaperPerSample[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsShaperPerSample[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsShaperPerSample[il][iv][ic].size(); ++is) {
            fTDCvsShaperPerSample[il][iv][ic][is] =
                new TH2D(Form("Shaper_Layer%d%s_Chip%d_Sample%d", il, fXY[iv].Data(), ic, is),
                         Form("Layer %d%s - Chip %d; TDC [ns]; Sample %d", il, fXY[iv].Data(), ic, is), shp_bin,
                         shp_min, shp_max, dep_bin, dep_min, dep_max);
            fTDCvsShaperPerSample[il][iv][ic][is]->SetLineColor(fColor[ic]->GetNumber());
          }
        }
      }
    }
    //
    for (UInt_t il = 0; il < fTDCvsShaperGlobal.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsShaperGlobal[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsShaperGlobal[il][iv].size(); ++ic) {
          fTDCvsShaperGlobal[il][iv][ic] =
              new TH2D(Form("Shaper_Layer%d%s_Chip%d_Global", il, fXY[iv].Data(), ic),
                       Form("Layer %d%s - Chip %d; TDC [ns]; Signal", il, fXY[iv].Data(), ic, fSampleRef), shp_bin,
                       shp_min, shp_max, dep_bin, dep_min, dep_max);
          fTDCvsShaperGlobal[il][iv][ic]->SetLineColor(fColor[ic]->GetNumber());
        }
      }
    }
    //
    for (UInt_t il = 0; il < fPositionDependence.size(); ++il) {
      for (UInt_t iv = 0; iv < fPositionDependence[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fPositionDependence[il][iv].size(); ++ic) {
          fPositionDependence[il][iv][ic] =
              new TH2D(Form("PositionDependence_Layer%d%s_Chip%d", il, fXY[iv].Data(), ic),
                       Form("Layer %d%s - Chip %d; Channel; Signal", il, fXY[iv].Data(), ic), fNPaceChannels, -0.5,
                       fNPaceChannels - 0.5, dep_bin, dep_min, dep_max);
          fPositionDependence[il][iv][ic]->SetLineColor(fColor[ic]->GetNumber());
        }
      }
    }
    //
    fTDCCorrelation =
        new TH2D("TDC2CorrTDC3", ";TDC2[ns];TDC3[ns]", tdc_bin, tdc_min, tdc_max, tdc_bin, tdc_min, tdc_max);
    fTDCDifference = new TH1D("TDC2DiffTDC3", ";TDC2-TDC3[ns]; counts", dif_bin, dif_min, dif_max);
  }
  //
  for (UInt_t it = 0; it < fParticleEnergy.size(); ++it) {
    fParticleEnergy[it] = new TH1D(Form("ParticleEnergy_Tower%d", it), Form("Tower %d; Energy [GeV]; Counts", it),
                                   ene_bin, ene_min, ene_max);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::ClearEvent() {
  fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::EventLoop() {
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  // Int_t nentries = 10000;
  Int_t nentries = fInputTree->GetEntries();
  for (Int_t ie = 0; ie <= nentries; ++ie) {
    if (ie % 100 == 0 || ie == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d", ie);
      fflush(stdout);
    }
    fInputTree->GetEntry(ie);

    Double_t tdc2, tdc3;
    Double_t tdc, dif;
    if (fInputLevel == 1) {
      fLvl1 = (Level1<armclass> *)fInputEv->Get(Form("lvl1_a%d", this->kArmIndex + 1));
      if (!fLvl1) {
        UT::Printf(UT::kPrintError, "Level1 not found in event %d\n", ie);
        continue;
      }
      if (!fLvl1->IsBeam()) continue;
      if (fTDC) {
        tdc2 = fLvl1->fTdc[2][0];
        tdc3 = fLvl1->fTdc[3][0];
        if (this->fOperation == kSPS2022) {
          tdc2 -= fLvl1->fTdc[0][1];
          tdc3 -= fLvl1->fTdc[0][1];
        }
        tdc = tdc2;
        dif = tdc2 - tdc3;
      }
    } else {
      fLvl2 = (Level2<armclass> *)fInputEv->Get(Form("lvl2_a%d", this->kArmIndex + 1));
      if (!fLvl2) {
        UT::Printf(UT::kPrintError, "Level2 not found in event %d\n", ie);
        continue;
      }
      if (!fLvl2->IsBeam()) continue;
      if (fTDC) {
        tdc2 = fLvl2->fTdc[2][0];
        tdc3 = fLvl2->fTdc[3][0];
        if (this->fOperation == kSPS2022) {
          tdc2 -= fLvl2->fTdc[0][1];
          tdc3 -= fLvl2->fTdc[0][1];
        }
        tdc = tdc2;
        dif = tdc2 - tdc3;
      }
    }

    // Retrieve Lvl3 for some kinds of cuts
    if (fMinEnergy != fMaxEnergy || fMaximumLayer || fVertexStrip || fPhoton || fNeutron || fSinglehit || fEqualize) {
      fLvl3 = (Level3<armrec> *)fInputEv->Get(Form("lvl3_a%d", this->kArmIndex + 1));
      if (!fLvl3) {
        UT::Printf(UT::kPrintError, "Level3 not found in event %d\n", ie);
        continue;
      }
    }

    // Get information for the following energy cut
    Double_t energy[this->kCalNtower] = {-1., -1.};
    if (fMinEnergy != fMaxEnergy) {
      for (UInt_t it = 0; it < this->kCalNtower; ++it) {
        if (fNeutron)
          energy[it] = fLvl3->fNeutronEnergy[it];
        else if (fPhoton)
          energy[it] = fLvl3->fPhotonEnergy[it];
        else
          energy[it] = fLvl3->fPhotonEnergy[it];
        if (fModify) {
          energy[it] *= this->kEnergyRescaleFactor[it];
        }
      }
    }

    // Get information for the following singlehit cut
    Bool_t multihit[this->kCalNtower] = {false, false};
    if (fSinglehit) {
      for (UInt_t it = 0; it < this->kCalNtower; ++it) {
        multihit[it] = fLvl3->fPhotonMultiHit[it];
      }
    }

    // Get information for the following vertex cut
    Int_t shoxis[this->kCalNtower][this->kPosNview] = {{-1, -1}, {-1, -1}};
    Int_t shoyer[this->kCalNtower][this->kPosNview] = {{-1, -1}, {-1, -1}};
    if (fMaximumLayer || fVertexStrip) {
      for (UInt_t it = 0; it < this->kCalNtower; ++it) {
        // Get maximum z position on silicon
        Double_t mx = fLvl3->fPosMaxLayer[it][0];
        Double_t my = fLvl3->fPosMaxLayer[it][1];
        // Get reconstructed calorimeter position
        Double_t px = fLvl3->fPhotonPosition[it][0];
        Double_t py = fLvl3->fPhotonPosition[it][1];
        // Remove events hitting ADAMO aluminium frame
        if (this->fOperation == kSPS2022) {
                if(it==1 && px<10.) {
                        continue;
                }
        }
        //
        TVector3 pos = CT::CalorimeterToChannel(this->kArmIndex, it, mx, my, px, py);
        Int_t sx = (Int_t)(pos.X() + 0.5);
        Int_t sy = (Int_t)(pos.Y() + 0.5);
        // if(TMath::Abs(sx - pos.X())<0.1)
        shoxis[it][0] = (Int_t)(pos.X() + 0.5);
        shoyer[it][0] = mx;
        // if(TMath::Abs(sy - pos.Y())<0.1)
        shoxis[it][1] = (Int_t)(pos.Y() + 0.5);
        shoyer[it][1] = my;
      }
    }

    // Get information for the following PID cut
    Double_t l90[this->kCalNtower] = {-1., -1.};
    Double_t l2d[this->kCalNtower] = {-1., -1.};
    if (fPhoton || fNeutron) {
      for (UInt_t it = 0; it < this->kCalNtower; ++it) {
        l90[it] = fLvl3->fPhotonL90[it];
        l2d[it] = fLvl3->fNeutronL90[it] - 0.25 * fLvl3->fNeutronL20[it];
      }
    }

    if (fTDC) {
      if (tdc < fTDCMin || tdc >= fTDCMax) {
        continue;
      }

      fTDCCorrelation->Fill(tdc2, tdc3);
      fTDCDifference->Fill(tdc2 - tdc3);
    }

    /*
     * Fill histograms by looking the signal strip-per-strip
     */
    for (UInt_t it = 0; it < this->kPosNtower; ++it) {
      for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
        for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
          for (UInt_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
            Int_t caltower = -1;

            if (fMinEnergy != fMaxEnergy || fMaximumLayer || fVertexStrip || fPhoton || fNeutron) {
              caltower = FromStripToTower(iv, ic);
              if (caltower < 0) {
                continue;
              }
            }

            // Energy cut
            if (fMinEnergy != fMaxEnergy) {
              if (energy[caltower] < fMinEnergy || energy[caltower] >= fMaxEnergy) {  // GeV
                continue;
              }
            }
            // Multihit cut
            if (fSinglehit) {
              if (multihit[caltower]) {
                continue;
              }
            }
            // Vertex cut
            if (fMaximumLayer) {
              if (shoyer[caltower][iv] != il) {
                continue;
              }
            }
            if (fVertexStrip) {
              if (shoxis[caltower][iv] != ic) {
                continue;
              }
            }
            // PID cut
            if (fPhoton) {
              if (l90[caltower] > 0. && l90[caltower] > 15.) {
                continue;
              } else if (l2d[caltower] > 0. && l2d[caltower] > 15.)
                continue;
            }
            if (fNeutron) {
              if (l2d[caltower] > 0. && l2d[caltower] < 25.) {
                continue;
              } else if (l90[caltower] > 0. && l90[caltower] < 25.)
                continue;
            }

            // Skip dead channels
            if (it == 0 && il == 2 && iv == 0 && ic == 96) {
              continue;
            }

            if (fMinEnergy != fMaxEnergy) {
              fParticleEnergy[caltower]->Fill(energy[caltower]);
            }

            /*
             *  Fill signal histograms
             */
            for (UInt_t is = 0; is < this->kPosNsample; ++is) {
              Double_t val = (fInputLevel == 1) ?  //
                                 fLvl1->fPosDet[it][il][iv][ic][is]
                                                : fLvl2->fPosDet[it][il][iv][ic][is];
              if (val > fMinValue && val < fMaxValue) {
                Int_t chip = ic / fNPaceChannels;
                Int_t chan = ic % fNPaceChannels;
                // For safety skip the first and the last channel
                if (chan == 0 || chan == fNPaceChannels - 1) {
                  continue;
                }
                fHitmap[il][iv]->Fill(ic);
                fSampleSignalTotal[is]->Fill(val);
                fSampleSignalLayer[il][iv][is]->Fill(val);
                fSampleSignalChip[il][iv][chip][is]->Fill(val);
                if (fEqualize) {
                  fPositionVsSignal[il][iv][is]->Fill(ic, val);
                  fPosVsSigWeight[il][iv][is]->Fill(ic, val * energy[caltower]);
                  fPosVsEneWeight[il][iv][is]->Fill(ic, energy[caltower]);
                }
                if (fTDC) {
                  fTDCvsSignalTotal[is]->Fill(tdc, val);
                  fTDCvsSignalLayer[il][iv][is]->Fill(tdc, val);
                  fTDCvsSignalChip[il][iv][chip][is]->Fill(tdc, val);
                  // if (tdc >= fTDCMin && tdc < fTDCMax) {
                  if (is == fSampleRef && chip == fChipRef) {
                    Int_t bin = (tdc - fTDCMin) / (fTDCStep);
                    fSubTDCvsRefSignal[il][iv][bin]->Fill(chan, val);
                    }
                    //}
                }
                if (fTDC) {  // Time structure of the signal
                  fTDCvsShaperPerSample[il][iv][chip][is]->Fill(tdc - is * fLHCPeriod, val);
                  fTDCvsShaperGlobal[il][iv][chip]->Fill(tdc - is * fLHCPeriod, val);
                }
                if (fTDC) {  // Time structure of the signal
                  if (is == fSampleRef) fPositionDependence[il][iv][chip]->Fill(chan, val);
                }
              }
            }  // sample

            if (fInputLevel == 2) {
              continue;
            }

            /*
             *  Fill ratio histograms
             */
            Double_t ref = (fInputLevel == 1) ? fLvl1->fPosDet[it][il][iv][ic][fSampleRef]
                                              : fLvl2->fPosDet[it][il][iv][ic][fSampleRef];
            if (ref > fMinValue && ref < fMaxValue) {
              for (UInt_t is = 0; is < this->kPosNsample; ++is) {
                if (is == fSampleRef) continue;
                Double_t val = (fInputLevel == 1) ?  //
                                   fLvl1->fPosDet[it][il][iv][ic][is]
                                                  : fLvl2->fPosDet[it][il][iv][ic][is];
                Double_t ratio = val / ref;
                Int_t chip = ic / fNPaceChannels;
                Int_t chan = ic % fNPaceChannels;
                fSampleRatioTotal[is]->Fill(ratio);
                fSampleRatioLayer[il][iv][is]->Fill(ratio);
                fSampleRatioChip[il][iv][chip][is]->Fill(ratio);
                if (fTDC) {
                  fTDCvsRatioTotal[is]->Fill(tdc, ratio);
                  fTDCvsRatioLayer[il][iv][is]->Fill(tdc, ratio);
                  fTDCvsRatioChip[il][iv][chip][is]->Fill(tdc, ratio);
                  // if (tdc >= fTDCMin && tdc < fTDCMax) {
                  if (is == 1 - fSampleRef && chip == fChipRef) {
                    Int_t bin = (tdc - fTDCMin) / (fTDCStep);
                    fSubTDCvsRefRatio[il][iv][bin]->Fill(chan, ratio);
                    }
                    //}
                }
              }  // sample
            }
            /*
             *
             */
          }  // channel
        }    // view
      }      // layer
    }        // posdet-tower

    ClearEvent();
  }

  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::GetProfile() {
  if (fEqualize) {
    GetProfilePosition();
  }
  if (fTDC) {
    GetProfileTDC();
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::GetProfilePosition() {
  UT::AllocateVector3D(fProfilePositionSignal, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector3D(fProfilePosSigWeight, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector3D(fProfilePosEneWeight, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector3D(fProfilePosSigAverage, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  //
  for (UInt_t il = 0; il < fPositionVsSignal.size(); ++il) {
    for (UInt_t iv = 0; iv < fPositionVsSignal[il].size(); ++iv) {
      for (UInt_t is = 0; is < fPositionVsSignal[il][iv].size(); ++is) {
        fProfilePositionSignal[il][iv][is] = (TProfile *)fPositionVsSignal[il][iv][is]->ProfileX(
            Form("Profile_SignalVsPosition_Layer%d%s_Sample%d", il, fXY[iv].Data(), is));
        fProfilePositionSignal[il][iv][is]->SetTitle(fPositionVsSignal[il][iv][is]->GetTitle());
        fProfilePositionSignal[il][iv][is]->GetXaxis()->SetTitle(fPositionVsSignal[il][iv][is]->GetXaxis()->GetTitle());
        fProfilePositionSignal[il][iv][is]->GetYaxis()->SetTitle(fPositionVsSignal[il][iv][is]->GetYaxis()->GetTitle());
      }
    }
  }
  //
  for (UInt_t il = 0; il < fPosVsSigWeight.size(); ++il) {
    for (UInt_t iv = 0; iv < fPosVsSigWeight[il].size(); ++iv) {
      for (UInt_t is = 0; is < fPosVsSigWeight[il][iv].size(); ++is) {
        fProfilePosSigWeight[il][iv][is] = (TProfile *)fPosVsSigWeight[il][iv][is]->ProfileX(
            Form("Profile_SignalVsPosition_Weight_Layer%d%s_Sample%d", il, fXY[iv].Data(), is));
      }
    }
  }
  for (UInt_t il = 0; il < fPosVsEneWeight.size(); ++il) {
    for (UInt_t iv = 0; iv < fPosVsEneWeight[il].size(); ++iv) {
      for (UInt_t is = 0; is < fPosVsEneWeight[il][iv].size(); ++is) {
        fProfilePosEneWeight[il][iv][is] = (TProfile *)fPosVsEneWeight[il][iv][is]->ProfileX(
            Form("Profile_EnergyVsPosition_Weight_Layer%d%s_Sample%d", il, fXY[iv].Data(), is));
      }
    }
  }

  for (UInt_t il = 0; il < fPosVsSigWeight.size(); ++il) {
    for (UInt_t iv = 0; iv < fPosVsSigWeight[il].size(); ++iv) {
      for (UInt_t is = 0; is < fPosVsSigWeight[il][iv].size(); ++is) {
        fProfilePosSigAverage[il][iv][is] = (TProfile *)fProfilePosSigWeight[il][iv][is]->Clone();
        fProfilePosSigAverage[il][iv][is]->Divide(fProfilePosEneWeight[il][iv][is]);
        fProfilePosSigAverage[il][iv][is]->SetName(
            Form("Profile_weightedSignalVsPosition_Layer%d%s_Sample%d", il, fXY[iv].Data(), is));
        fProfilePosSigAverage[il][iv][is]->SetTitle(Form("Layer %d%s - Sample%d; Strip; weighted dE [%s]", il,
                                                         fXY[iv].Data(), is, fUnit[fInputLevel - 1].Data()));
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::GetProfileTDC() {
  UT::AllocateVector1D(fProfileSignalTotal, this->kPosNsample);
  UT::AllocateVector3D(fProfileSignalLayer, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector4D(fProfileSignalChip, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
  UT::AllocateVector1D(fProfileRatioTotal, this->kPosNsample);
  UT::AllocateVector3D(fProfileRatioLayer, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector4D(fProfileRatioChip, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
  //
  UT::AllocateVector4D(fProfileShaperPerSample, this->kPosNlayer, this->kPosNview, fNPacePerHybrid, this->kPosNsample);
  UT::AllocateVector3D(fProfileShaperGlobal, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);

  for (UInt_t is = 0; is < fTDCvsSignalTotal.size(); ++is) {
    fProfileSignalTotal[is] =
        (TProfile *)fTDCvsSignalTotal[is]->ProfileX(Form("Profile_TotalSignalVsTDC_Sample%d", is));
    fProfileSignalTotal[is]->SetTitle(fTDCvsSignalTotal[is]->GetTitle());
    fProfileSignalTotal[is]->GetXaxis()->SetTitle(fTDCvsSignalTotal[is]->GetXaxis()->GetTitle());
    fProfileSignalTotal[is]->GetYaxis()->SetTitle(fTDCvsSignalTotal[is]->GetYaxis()->GetTitle());
    for (UInt_t il = 0; il < fTDCvsSignalLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsSignalLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fTDCvsSignalLayer[il][iv].size(); ++is) {
          fProfileSignalLayer[il][iv][is] = (TProfile *)fTDCvsSignalLayer[il][iv][is]->ProfileX(
              Form("Profile_LayerSignalVsTDC_Layer%d%s_Sample%d", il, fXY[iv].Data(), is));
          fProfileSignalLayer[il][iv][is]->SetTitle(fTDCvsSignalLayer[il][iv][is]->GetTitle());
          fProfileSignalLayer[il][iv][is]->GetXaxis()->SetTitle(fTDCvsSignalLayer[il][iv][is]->GetXaxis()->GetTitle());
          fProfileSignalLayer[il][iv][is]->GetYaxis()->SetTitle(fTDCvsSignalLayer[il][iv][is]->GetYaxis()->GetTitle());
        }
      }
    }
    for (UInt_t il = 0; il < fTDCvsSignalChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsSignalChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsSignalChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsSignalChip[il][iv][ic].size(); ++is) {
            fProfileSignalChip[il][iv][ic][is] = (TProfile *)fTDCvsSignalChip[il][iv][ic][is]->ProfileX(
                Form("Profile_ChipSignalVsTDC_Layer%d%s_Sample%d_Chip%d", il, fXY[iv].Data(), is, ic));
            fProfileSignalChip[il][iv][ic][is]->SetTitle(fTDCvsSignalChip[il][iv][ic][is]->GetTitle());
            fProfileSignalChip[il][iv][ic][is]->GetXaxis()->SetTitle(
                fTDCvsSignalChip[il][iv][ic][is]->GetXaxis()->GetTitle());
            fProfileSignalChip[il][iv][ic][is]->GetYaxis()->SetTitle(
                fTDCvsSignalChip[il][iv][ic][is]->GetYaxis()->GetTitle());
          }
        }
      }
    }
  }
  //
  for (UInt_t is = 0; is < fTDCvsRatioTotal.size(); ++is) {
    if (is == fSampleRef) continue;
    fProfileRatioTotal[is] = (TProfile *)fTDCvsRatioTotal[is]->ProfileX(Form("Profile_TotalRatioVsTDC_Sample%d", is));
    fProfileRatioTotal[is]->SetTitle(fTDCvsRatioTotal[is]->GetTitle());
    fProfileRatioTotal[is]->GetXaxis()->SetTitle(fTDCvsRatioTotal[is]->GetXaxis()->GetTitle());
    fProfileRatioTotal[is]->GetYaxis()->SetTitle(fTDCvsRatioTotal[is]->GetYaxis()->GetTitle());
  }
  for (UInt_t il = 0; il < fTDCvsRatioLayer.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCvsRatioLayer[il].size(); ++iv) {
      for (UInt_t is = 0; is < fTDCvsRatioLayer[il][iv].size(); ++is) {
        if (is == fSampleRef) continue;
        fProfileRatioLayer[il][iv][is] = (TProfile *)fTDCvsRatioLayer[il][iv][is]->ProfileX(
            Form("Profile_LayerRatioVsTDC_Layer%d%s_Sample%d", il, fXY[iv].Data(), is));
        fProfileRatioLayer[il][iv][is]->SetTitle(fTDCvsRatioLayer[il][iv][is]->GetTitle());
        fProfileRatioLayer[il][iv][is]->GetXaxis()->SetTitle(fTDCvsRatioLayer[il][iv][is]->GetXaxis()->GetTitle());
        fProfileRatioLayer[il][iv][is]->GetYaxis()->SetTitle(fTDCvsRatioLayer[il][iv][is]->GetYaxis()->GetTitle());
      }
    }
  }
  for (UInt_t il = 0; il < fTDCvsRatioChip.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCvsRatioChip[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fTDCvsRatioChip[il][iv].size(); ++ic) {
        for (UInt_t is = 0; is < fTDCvsRatioChip[il][iv][ic].size(); ++is) {
          if (is == fSampleRef) continue;
          fProfileRatioChip[il][iv][ic][is] = (TProfile *)fTDCvsRatioChip[il][iv][ic][is]->ProfileX(
              Form("Profile_ChipRatioVsTDC_Layer%d%s_Sample%d_Chip%d", il, fXY[iv].Data(), is, ic));
          fProfileRatioChip[il][iv][ic][is]->SetTitle(fTDCvsRatioChip[il][iv][ic][is]->GetTitle());
          fProfileRatioChip[il][iv][ic][is]->GetXaxis()->SetTitle(
              fTDCvsRatioChip[il][iv][ic][is]->GetXaxis()->GetTitle());
          fProfileRatioChip[il][iv][ic][is]->GetYaxis()->SetTitle(
              fTDCvsRatioChip[il][iv][ic][is]->GetYaxis()->GetTitle());
        }
      }
    }
  }
  //
  for (UInt_t il = 0; il < fTDCvsShaperPerSample.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCvsShaperPerSample[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fTDCvsShaperPerSample[il][iv].size(); ++ic) {
        for (UInt_t is = 0; is < fTDCvsShaperPerSample[il][iv][ic].size(); ++is) {
          fProfileShaperPerSample[il][iv][ic][is] = (TProfile *)fTDCvsShaperPerSample[il][iv][ic][is]->ProfileX(
              Form("Profile_Shaper_Layer%d%s_Chip%d_Sample%d", il, fXY[iv].Data(), ic, is));
          fProfileShaperPerSample[il][iv][ic][is]->SetTitle(fTDCvsShaperPerSample[il][iv][ic][is]->GetTitle());
          fProfileShaperPerSample[il][iv][ic][is]->GetXaxis()->SetTitle(
              fTDCvsShaperPerSample[il][iv][ic][is]->GetXaxis()->GetTitle());
          fProfileShaperPerSample[il][iv][ic][is]->GetYaxis()->SetTitle(
              fTDCvsShaperPerSample[il][iv][ic][is]->GetYaxis()->GetTitle());
        }
      }
    }
  }
  //
  for (UInt_t il = 0; il < fTDCvsShaperGlobal.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCvsShaperGlobal[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fTDCvsShaperGlobal[il][iv].size(); ++ic) {
        fProfileShaperGlobal[il][iv][ic] = (TProfile *)fTDCvsShaperGlobal[il][iv][ic]->ProfileX(
            Form("Profile_Shaper_Layer%d%s_Chip%d_Global", il, fXY[iv].Data(), ic));
        fProfileShaperGlobal[il][iv][ic]->SetTitle(fTDCvsShaperGlobal[il][iv][ic]->GetTitle());
        fProfileShaperGlobal[il][iv][ic]->GetXaxis()->SetTitle(fTDCvsShaperGlobal[il][iv][ic]->GetXaxis()->GetTitle());
        fProfileShaperGlobal[il][iv][ic]->GetYaxis()->SetTitle(fTDCvsShaperGlobal[il][iv][ic]->GetYaxis()->GetTitle());
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::GetCorrection() {
  /*
   * Correction is necessary for two different effects:
   * 1) Signal dependence on TDC trigger time (i.e. latency)
   * 2) Different S0/S1 (i.e. working point) for the 12 chips
   * Both these effects are simultaneously corrected!
   */

  UT::Printf(UT::kPrintInfo, "Computing correction...");

  /*
   * Get TDC time profile with more coarse binning for better interpolation
   */
  UT::AllocateVector3D(fTDCSignalForCorrection, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  UT::AllocateVector3D(fTDCSignalSpline, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  for (UInt_t il = 0; il < fTDCSignalForCorrection.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCSignalForCorrection[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fTDCSignalForCorrection[il][iv].size(); ++ic) {
        // Clone + Rebin
        TString name = Form("SignalForCorrection_Layer%d%s_Chip%d", il, fXY[iv].Data(), ic);
        TString title = Form("Layer %d%s - Chip %d; TDC [ns]; Sample %d", il, fXY[iv].Data(), ic, fSampleRef);
        TH2D *auxiliary = (TH2D *)fTDCvsSignalChip[il][iv][ic][fSampleRef]->Clone();
        auxiliary->Rebin2D(fBinRef, 1);  // Rebin only x axis (TDC time)
        if (this->fOperation == kSPS2022) {
          if (il == 2) {  // Very unlucky statistics case
            auxiliary->Rebin2D(2 * fBinRef, 1);
          }
        }
        fTDCSignalForCorrection[il][iv][ic] = (TProfile *)auxiliary->ProfileX();
        fTDCSignalForCorrection[il][iv][ic]->SetName(name.Data());
        fTDCSignalForCorrection[il][iv][ic]->SetTitle(title.Data());
        fTDCSignalSpline[il][iv][ic] = HistoToSpline(fTDCSignalForCorrection[il][iv][ic]);
        fTDCSignalSpline[il][iv][ic]->SetName(Form("Spline_%s", name.Data()));
        fTDCSignalSpline[il][iv][ic]->SetTitle(title.Data());
        delete auxiliary;
      }
    }
  }
  /*
   * Get TDC ratio profile with more coarse binning for better interpolation
   */
  UT::AllocateVector3D(fTDCRatioForCorrection, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  UT::AllocateVector3D(fTDCRatioSpline, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  for (UInt_t il = 0; il < fTDCRatioForCorrection.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCRatioForCorrection[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fTDCRatioForCorrection[il][iv].size(); ++ic) {
        // Clone + Rebin
        TString name = Form("RatioForCorrection_Layer%d%s_Chip%d", il, fXY[iv].Data(), ic);
        TString title = Form("Layer %d%s - Chip %d; TDC [ns]; Sample %d / Sample %d", il, fXY[iv].Data(), ic,
                             1 - fSampleRef, fSampleRef);
        TH2D *auxiliary = (TH2D *)fTDCvsRatioChip[il][iv][ic][1 - fSampleRef]->Clone();
        auxiliary->Rebin2D(fBinRef, 1);  // Rebin only x axis (TDC time)
        if (this->fOperation == kSPS2022) {
          if (il == 2) {  // Very unlucky statistics case
            auxiliary->Rebin2D(2 * fBinRef, 1);
          }
        }
        fTDCRatioForCorrection[il][iv][ic] = (TProfile *)auxiliary->ProfileX();
        fTDCRatioForCorrection[il][iv][ic]->SetName(name.Data());
        fTDCRatioForCorrection[il][iv][ic]->SetTitle(title.Data());
        fTDCRatioSpline[il][iv][ic] = HistoToSpline(fTDCRatioForCorrection[il][iv][ic]);
        fTDCRatioSpline[il][iv][ic]->SetName(Form("Spline_%s", name.Data()));
        fTDCRatioSpline[il][iv][ic]->SetTitle(title.Data());
        delete auxiliary;
      }
    }
  }
  /*
   * Get the graph relative to normalized signal in Sample1 vs Sample0/Sample1
   */
  UT::AllocateVector3D(fRatioVsSignal, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  for (UInt_t il = 0; il < fRatioVsSignal.size(); ++il) {
    for (UInt_t iv = 0; iv < fRatioVsSignal[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fRatioVsSignal[il][iv].size(); ++ic) {
        TString name = Form("SignalVsRatio_Layer%d%s_Chip%d", il, fXY[iv].Data(), ic);
        TString title = Form("Layer %d%s - Chip %d; Sample %d / Sample %d; Sample %d", il, fXY[iv].Data(), ic,
                             1 - fSampleRef, fSampleRef, fSampleRef);
        fRatioVsSignal[il][iv][ic] = new TGraphErrors();
        fRatioVsSignal[il][iv][ic]->SetName(name.Data());
        fRatioVsSignal[il][iv][ic]->SetTitle(title.Data());
        fRatioVsSignal[il][iv][ic]->SetLineColor(fColor[ic]->GetNumber());
        fRatioVsSignal[il][iv][ic]->SetMarkerColor(fColor[ic]->GetNumber());
        Int_t sbin = fTDCSignalForCorrection[il][iv][ic]->GetXaxis()->GetNbins();
        Int_t rbin = fTDCRatioForCorrection[il][iv][ic]->GetXaxis()->GetNbins();
        Int_t nbin = TMath::Min(sbin, rbin);
        Int_t reference_tdcbin = fTDCSignalForCorrection[il][iv][ic]->FindBin(fTDCRef);
        Double_t reference_signal = fTDCSignalForCorrection[il][iv][ic]->GetBinContent(reference_tdcbin);
        if (reference_signal <= 0) {
          continue;
        }
        Int_t ip = 0;
        for (Int_t ib = 0; ib < nbin; ++ib) {
          Double_t stdc = fTDCSignalForCorrection[il][iv][ic]->GetXaxis()->GetBinCenter(1 + ib);
          Double_t rtdc = fTDCRatioForCorrection[il][iv][ic]->GetXaxis()->GetBinCenter(1 + ib);
          if (rtdc != stdc) {
            UT::Printf(UT::kPrintError, "Bin mismatch while computing correction: Exit...");
            exit(EXIT_FAILURE);
          }
          if (stdc < fTDCMin || rtdc > fTDCMax) {
            continue;
          }
          Double_t scnt = fTDCSignalForCorrection[il][iv][ic]->GetBinContent(1 + ib) / reference_signal;
          Double_t rcnt = fTDCRatioForCorrection[il][iv][ic]->GetBinContent(1 + ib);
          if (scnt <= 0 || rcnt <= 0) {
            continue;
          }
          fRatioVsSignal[il][iv][ic]->SetPoint(ip, rcnt, scnt);
          Double_t serr = fTDCSignalForCorrection[il][iv][ic]->GetBinError(1 + ib) / reference_signal;
          Double_t rerr = fTDCRatioForCorrection[il][iv][ic]->GetBinError(1 + ib);
          //
          fRatioVsSignal[il][iv][ic]->SetPointError(ip, rerr, serr);
          //
          ++ip;
        }
      }
    }
  }

  /*
   * Get TDC and Chip dependent correction factors
   */
  UT::AllocateVector3D(fTDCCorrection, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  UT::AllocateVector3D(fTDCCorrectionSpline, this->kPosNlayer, this->kPosNview, fNPacePerHybrid);
  for (UInt_t il = 0; il < fTDCCorrection.size(); ++il) {
    for (UInt_t iv = 0; iv < fTDCCorrection[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fTDCCorrection[il][iv].size(); ++ic) {
        // Initialize histogram
        Int_t nbin = fTDCSignalForCorrection[il][iv][ic]->GetXaxis()->GetNbins();
        Double_t xmin = fTDCSignalForCorrection[il][iv][ic]->GetXaxis()->GetBinLowEdge(1);
        Double_t xmax = fTDCSignalForCorrection[il][iv][ic]->GetXaxis()->GetBinUpEdge(nbin);
        TString name = Form("Correction_Layer%d%s_Chip%d", il, fXY[iv].Data(), ic);
        TString title = Form("Layer %d%s - Chip %d; TDC [ns]; Correction", il, fXY[iv].Data(), ic);
        fTDCCorrection[il][iv][ic] = new TH1D(name.Data(), title.Data(), nbin, xmin, xmax);
        fTDCCorrection[il][iv][ic]->SetLineColor(fColor[ic]->GetNumber());
       
	// Define reference values
        
        Int_t tdcbin = fTDCSignalForCorrection[il][iv][fChipRef]->FindBin(fTDCRef);
        // For correction 1) TDC time dependence
        Double_t auto_signal = fTDCSignalForCorrection[il][iv][ic]->GetBinContent(tdcbin);
        // For correction 2) S0/S1 dependence
        Double_t stdn_ratio = fTDCRatioForCorrection[il][iv][fChipRef]->GetBinContent(tdcbin);
        
        // For correction 1) TDC time dependence
	if(fTDCSignalSpline[il][iv][ic]->GetNp()>0) {
		//printf("AAA %d\n", fTDCSignalSpline[il][iv][ic]->GetNp());
	        auto_signal = fTDCSignalSpline[il][iv][ic]->Eval(fTDCRef);
	}
        // For correction 2) S0/S1 dependence	
	if(fTDCRatioSpline[il][iv][fChipRef]->GetNp()>0) {
		//printf("BBB %d\n", fTDCRatioSpline[il][iv][fChipRef]->GetNp());
	        stdn_ratio = fTDCRatioSpline[il][iv][fChipRef]->Eval(fTDCRef);
	}

        /*
        // First approach: the chip-by-chip difference is corrected interpolating on S1 vs S0/S1 at _TDC_
        //
        // Compute correction as
        // 		C[chip, S0/S1, TDC] =  S[chip, _S0/S1_, _TDC_] / S[chip, S0/S1, TDC]
        // where:
        //		+ _TDC_ is a fixed TDC reference time
        //		+ _S0/S1_ is the gain ratio at _TDC_ of the reference _CHIP_ in the view
        //		+ S[chip, _S0/S1_, _TDC_] is obtained by scaling the signal in each chip to:
        //			- The value it would have for TDC=_TDC_ on the same chip, i.e. S[chip,_TDC_]/S[chip,TDC]
        //			- The value it would have for chip=_CHIP_, i.e. S[_CHIP_,_S0/S1_]/S[chip,S0/S1]
        //			- NB: Having normalized S1vsS0/S1 to S1 value for TDC=_TDC_ the last denominator is 1
        //
        Double_t reference = auto_signal;
        // UT::Printf(UT::kPrintInfo, "\n%d %d %d : %f %f : %f\t", il, iv, ic, auto_signal, stdn_ratio, reference);
        Double_t interpolation = fRatioVsSignal[il][iv][ic]->Eval(stdn_ratio, NULL, "S");
        if (interpolation > 0) reference *= interpolation;
        // UT::Printf(UT::kPrintInfo, "\t%f\n", reference);
        */

        // Second approach: the chip-by-chip difference is corrected stupidly scaling the amplitude at _TDC_
        //
        // Compute correction as
        // 		C[chip, TDC] =  S[_CHIP_, _TDC_] / S[chip, TDC]
        // where:
        //		+ _TDC_ is a fixed TDC reference time
        //		+ _CHIP_ is the reference chip in the view
        //
        
        Double_t reference = fTDCSignalForCorrection[il][iv][fChipRef]->GetBinContent(tdcbin);
        if(fTDCSignalSpline[il][iv][fChipRef]->GetNp()>0) {
		//printf("CCC %d\n", fTDCSignalSpline[il][iv][fChipRef]->GetNp());
        	reference = fTDCSignalSpline[il][iv][fChipRef]->Eval(fTDCRef);
	}
        // UT::Printf(UT::kPrintInfo, "\n%d %d %d : %f %f : %f\t", il, iv, ic, auto_signal, stdn_ratio, reference);

        for (UInt_t ib = 0; ib < fTDCCorrection[il][iv][ic]->GetXaxis()->GetNbins(); ++ib) {
          Double_t center = fTDCSignalForCorrection[il][iv][ic]->GetXaxis()->GetBinCenter(ib + 1);
          Double_t content = fTDCSignalForCorrection[il][iv][ic]->GetBinContent(ib + 1);
          Double_t error = fTDCSignalForCorrection[il][iv][ic]->GetBinError(ib + 1);
          // if (center > fTDCMin && center < fTDCMax && content > 0) {
          if (content > 0) {
            Double_t correct = reference / content;
            Double_t rorre = correct * error / content;
            //
            fTDCCorrection[il][iv][ic]->SetBinContent(ib + 1, correct);
            fTDCCorrection[il][iv][ic]->SetBinError(ib + 1, rorre);
          }
        }
        fTDCCorrectionSpline[il][iv][ic] = HistoToSpline(fTDCCorrection[il][iv][ic]);
        fTDCCorrectionSpline[il][iv][ic]->SetName(Form("Spline_%s", name.Data()));
        fTDCCorrectionSpline[il][iv][ic]->SetTitle(title.Data());
      }  // chip loop
    }    // view loop
  }      // layer loop

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::SaveS0S1Ratio() {
  /*
   * Correction is necessary for two different effects:
   * 1) Signal dependence on TDC trigger time (i.e. latency)
   * 2) Different S0/S1 (i.e. working point) for the 12 chips
   * Both these effects are simultaneously corrected!
   */

  UT::Printf(UT::kPrintInfo, "Saving ratio to file...");

  fOfstream.open(Form("%s/S%dS%dRatio-Run%d-%d.dat",  //
                      GetPathName(fOutputName).Data(), 1 - fSampleRef, fSampleRef, fFirstRun, fLastRun));

  fOfstream << Form("#Sample ratio computed at an average S%d value", fSampleRef) << endl;
  fOfstream << Form("#Reference time %.3f ns", fTDCRef) << endl;
  fOfstream << Form("#Reference chip %d", fChipRef) << endl;
  fOfstream << Form("#") << endl;
  fOfstream << Form("#Layer\tView\tChip\tS%d/S%d\tS%d[ADC]", 1 - fSampleRef, fSampleRef, fSampleRef) << endl;

  if (fTDC && fCorrection) {
    for (UInt_t il = 0; il < fTDCRatioForCorrection.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCRatioForCorrection[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCRatioForCorrection[il][iv].size(); ++ic) {
          /*
                  Int_t bin = fTDCRatioForCorrection[il][iv][ic]->FindBin(fTDCRef);
          Double_t center = fTDCRatioForCorrection[il][iv][ic]->GetXaxis()->GetBinCenter(bin);
          Double_t ratio = fTDCRatioForCorrection[il][iv][ic]->GetBinContent(bin);
          Double_t value = fTDCSignalForCorrection[il][iv][ic]->GetBinContent(bin);
          if (center != fTDCRef) {
            UT::Printf(UT::kPrintError, "Mismatch in TDC reference bin center: Exit...\n");
            exit(EXIT_FAILURE);
          }
          */
          /*
                  Double_t ratio = fTDCRatioForCorrection[il][iv][ic]->Interpolate(fTDCRef);
          Double_t value = fTDCSignalForCorrection[il][iv][ic]->Interpolate(fTDCRef);
          fOfstream << Form("%d\t%d\t%d\t%.3f\t%.3f", il, iv, ic, ratio, value) << endl;
          */
	  if(fTDCSignalSpline[il][iv][ic]->GetNp()<=0 || fTDCRatioSpline[il][iv][fChipRef]->GetNp()<=0)
		continue;
          Double_t ratio = fTDCRatioSpline[il][iv][ic]->Eval(fTDCRef);
          Double_t value = fTDCSignalSpline[il][iv][ic]->Eval(fTDCRef);
          fOfstream << Form("%d\t%d\t%d\t%.3f\t%.3f", il, iv, ic, ratio, value) << endl;
        }
      }
    }
  } else {
    // Here the procedure is much simpler
    // We just need to take the mean value!
    for (UInt_t il = 0; il < fSampleRatioChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fSampleRatioChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fSampleRatioChip[il][iv].size(); ++ic) {
          Double_t ratio = fSampleRatioChip[il][iv][ic][1 - fSampleRef]->GetMean();
          Double_t value = fSampleSignalChip[il][iv][ic][fSampleRef]->GetMean();
          fOfstream << Form("%d\t%d\t%d\t%.3f\t%.3f", il, iv, ic, ratio, value) << endl;
        }
      }
    }
  }

  fOfstream.close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotHisto() {
  gROOT->SetBatch(true);
  gStyle->SetOptStat("m");
  TGaxis::SetMaxDigits(3);

  PlotHitmapHisto();
  if (fEqualize) {
    PlotPositionHisto();
  }

  PlotLayerSignalHisto();
  PlotChipSignalHisto();

  if (fInputLevel == 1) {
    PlotLayerRatioHisto();
    PlotChipRatioHisto();
  }

  if (fTDC && fCorrection) {
    PlotSubTDCHisto();
    PlotSignalHisto();
    PlotRatioHisto();
    PlotCorrelationHisto();
    PlotCorrectionHisto();
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotHitmapHisto() {
  TCanvas *ch = new TCanvas("ch", "", 1400, 700);
  ch->Divide(2, 1);

  vector<vector<TLine *> > fLine;
  UT::AllocateVector2D(fLine, this->kPosNview, fNPacePerHybrid);

  for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
    ch->cd(1 + iv);

    Double_t ymin = 1e+6;
    Double_t ymax = 1e-6;
    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      fHitmap[il][iv]->Draw("HIST");
      Double_t lmin = fHitmap[il][iv]->GetMinimum();
      Double_t lmax = fHitmap[il][iv]->GetMaximum();
      if (lmin < ymin) ymin = lmin;
      if (lmax > ymax) ymax = lmax;
    }
    //
    TH1D *h = (TH1D *)fHitmap[0][iv]->Clone();
    h->SetTitle(fXY[iv].Data());
    h->Draw("HIST");
    h->SetStats(0);
    // if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
    h->SetMinimum(0.995 * ymin);
    h->SetMaximum(1.005 * ymax);
    gPad->Modified();
    gPad->Update();
    //
    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      fHitmap[il][iv]->Draw("SAME HIST");
      fHitmap[il][iv]->SetStats(0);
    }
    gPad->Modified();
    gPad->Update();
    for (int ichip = 0; ichip < fNPacePerHybrid; ++ichip) {
      Double_t xmin = fNPaceChannels * ichip;
      Double_t xmax = fNPaceChannels * ichip;
      fLine[iv][ichip] = new TLine();
      fLine[iv][ichip]->SetLineColor(kBlack);
      fLine[iv][ichip]->SetLineStyle(3);
      fLine[iv][ichip]->SetX1(xmin);
      fLine[iv][ichip]->SetX2(xmax);
      fLine[iv][ichip]->SetY1(h->GetMinimum());
      fLine[iv][ichip]->SetY2(h->GetMaximum());
      fLine[iv][ichip]->Draw("SAME");
    }
    gPad->Modified();
    gPad->Update();
  }

  ch->Print(Form("%sHitmap_Run_%d_%d.eps", GetPathName(fOutputName).Data(), fFirstRun, fLastRun));
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotPositionHisto() {
  for (Int_t is = 0; is < this->kPosNsample; ++is) {
    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      vector<vector<TLine *> > fLine;
      UT::AllocateVector2D(fLine, this->kPosNview, fNPacePerHybrid);
      TCanvas *ch = new TCanvas(Form("ch%d_%d", is, il), "", 1400, 700);
      ch->Divide(2, 1);
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        ch->cd(1 + iv);
        fProfilePositionSignal[il][iv][is]->Draw("HIST");
        fProfilePositionSignal[il][iv][is]->SetStats(0);
        for (int ichip = 0; ichip < fNPacePerHybrid; ++ichip) {
          Double_t xmin = fNPaceChannels * ichip;
          Double_t xmax = fNPaceChannels * ichip;
          fLine[iv][ichip] = new TLine();
          fLine[iv][ichip]->SetLineColor(kBlack);
          fLine[iv][ichip]->SetLineStyle(3);
          fLine[iv][ichip]->SetX1(xmin);
          fLine[iv][ichip]->SetX2(xmax);
          fLine[iv][ichip]->SetY1(fProfilePositionSignal[il][iv][is]->GetMinimum());
          fLine[iv][ichip]->SetY2(fProfilePositionSignal[il][iv][is]->GetMaximum());
          fLine[iv][ichip]->Draw("SAME");
        }
        gPad->Modified();
        gPad->Update();
      }
      ch->Print(Form("%sPosition_Sample%d_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), is, il, fFirstRun,
                     fLastRun));
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotLayerSignalHisto() {
  for (Int_t is = 0; is < this->kPosNsample; ++is) {
    if (is == fSampleRef) continue;

    TCanvas *cc = new TCanvas(Form("CC%d", is), "", 1400, 700);
    cc->Divide(2, 1);

    cc->cd(1);
    //
    TH1D *reference = (TH1D *)fSampleSignalTotal[is]->Clone();
    reference->SetLineColor(kGray + 1);
    reference->SetFillColor(kGray);
    reference->Draw("HIST");
    reference->SetTitle("");
    reference->SetStats(1);

    cc->cd(2);
    //
    Double_t ymin = 1e+6;
    Double_t ymax = 1e-6;
    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        fSampleSignalLayer[il][iv][is]->Draw("HIST");
        Double_t lmin = fSampleSignalLayer[il][iv][is]->GetMinimum();
        Double_t lmax = fSampleSignalLayer[il][iv][is]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }
    }
    //
    TH1D *h = (TH1D *)fSampleSignalLayer[0][0][is]->Clone();
    h->Draw("HIST");
    h->SetTitle("");
    h->SetStats(0);
    if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
    h->SetMinimum(0.995 * ymin);
    h->SetMaximum(1.005 * ymax);
    gPad->Modified();
    gPad->Update();

    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        fSampleSignalLayer[il][iv][is]->Draw("SAME HIST");
        fSampleSignalLayer[il][iv][is]->SetStats(0);

        gPad->Modified();
        gPad->Update();

        TString text = Form("%d%s = %.1f", il, fXY[iv].Data(), fSampleSignalLayer[il][iv][is]->GetMean());
        Double_t xmin = h->GetXaxis()->GetBinLowEdge(1);
        Double_t xmax = h->GetXaxis()->GetBinUpEdge(h->GetXaxis()->GetNbins());
        if (this->fOperation == kLHC2022) {
          xmin = fZoomMin;
          xmax = fZoomMax;
        }
        Double_t xscale = 0.210 * (xmax - xmin);
        Double_t px = xmax - xscale;
        Double_t yscale = 0.350 * (ymax - ymin) / (this->kPosNlayer * this->kPosNview);
        Double_t py = ymax - (il * this->kPosNview + iv + 1) * yscale;

        TLatex *ltex = new TLatex(px, py, text.Data());
        ltex->SetTextColor(fSampleSignalLayer[il][iv][is]->GetLineColor());
        ltex->SetTextSize(0.035);
        ltex->SetTextFont(42);
        gPad->Update();
        cc->Modified();

        ltex->Draw("SAME");

        gPad->Modified();
        gPad->Update();
      }
    }

    cc->Print(Form("%sSample%dToSample%dSignal_Run_%d_%d.eps",  //
                   GetPathName(fOutputName).Data(), is, fSampleRef, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotChipSignalHisto() {
  for (Int_t is = 0; is < this->kPosNsample; ++is) {
    if (is == fSampleRef) continue;

    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        TCanvas *cc = new TCanvas(Form("CC%d%d%d", is, il, iv), "", 1400, 700);
        cc->Divide(2, 1);

        cc->cd(1);
        //
        TH1D *reference = (TH1D *)fSampleSignalLayer[il][iv][is]->Clone();
        reference->SetLineColor(kGray + 1);
        reference->SetFillColor(kGray);
        reference->Draw("HIST");
        reference->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
        reference->SetStats(1);

        cc->cd(2);
        //
        Double_t ymin = 1e+6;
        Double_t ymax = 1e-6;
        for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {
          fSampleSignalChip[il][iv][ic][is]->Draw("HIST");
          Double_t lmin = fSampleSignalChip[il][iv][ic][is]->GetMinimum();
          Double_t lmax = fSampleSignalChip[il][iv][ic][is]->GetMaximum();
          if (lmin < ymin) ymin = lmin;
          if (lmax > ymax) ymax = lmax;
        }
        //
        TH1D *h = (TH1D *)fSampleSignalChip[0][0][0][is]->Clone();
        h->Draw("HIST");
        h->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
        h->SetStats(0);
        if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
        h->SetMinimum(0.995 * ymin);
        h->SetMaximum(1.005 * ymax);
        gPad->Modified();
        gPad->Update();

        for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {  // This variable is on PACE3 in the software
          Int_t thisPACE = fPACEID[ic];                    // This loop is on PACE3 on the hybrid

          fSampleSignalChip[il][iv][ic][is]->Draw("SAME HIST");
          fSampleSignalChip[il][iv][ic][is]->SetStats(0);

          gPad->Modified();
          gPad->Update();

          TString text = Form("PACE %02d(%02d) = %.1f", thisPACE, ic, fSampleSignalChip[il][iv][ic][is]->GetMean());
          Double_t xmin = h->GetXaxis()->GetBinLowEdge(1);
          Double_t xmax = h->GetXaxis()->GetBinUpEdge(h->GetXaxis()->GetNbins());
          if (this->fOperation == kLHC2022) {
            xmin = fZoomMin;
            xmax = fZoomMax;
          }
          Double_t xscale = 0.400 * (xmax - xmin);
          Double_t px = xmax - xscale;
          Double_t yscale = 0.500 * (ymax - ymin) / fNPacePerHybrid;
          Double_t py = ymax - (thisPACE + 1) * yscale;

          TLatex *ltex = new TLatex(px, py, text.Data());
          ltex->SetTextColor(fSampleSignalChip[il][iv][ic][is]->GetLineColor());
          ltex->SetTextSize(0.035);
          ltex->SetTextFont(42);
          gPad->Update();
          cc->Modified();

          ltex->Draw("SAME");

          gPad->Modified();
          gPad->Update();
        }

        cc->Print(Form("%sSample%dToSample%dSignal_Layer%d%s_Run_%d_%d.eps",  //
                       GetPathName(fOutputName).Data(), is, fSampleRef, il, fXY[iv].Data(), fFirstRun, fLastRun));
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotLayerRatioHisto() {
  for (Int_t is = 0; is < this->kPosNsample; ++is) {
    if (is == fSampleRef) continue;

    TCanvas *cc = new TCanvas(Form("cc%d", is), "", 1400, 700);
    cc->Divide(2, 1);

    cc->cd(1);
    //
    TH1D *reference = (TH1D *)fSampleRatioTotal[is]->Clone();
    reference->SetLineColor(kGray + 1);
    reference->SetFillColor(kGray);
    reference->Draw("HIST");
    reference->SetTitle("");
    reference->SetStats(1);
    if (this->fOperation == kLHC2022) reference->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);

    cc->cd(2);
    //
    Double_t ymin = 1e+6;
    Double_t ymax = 1e-6;
    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        fSampleRatioLayer[il][iv][is]->Draw("HIST");
        Double_t lmin = fSampleRatioLayer[il][iv][is]->GetMinimum();
        Double_t lmax = fSampleRatioLayer[il][iv][is]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }
    }
    //
    TH1D *h = (TH1D *)fSampleRatioLayer[0][0][is]->Clone();
    h->Draw("HIST");
    h->SetTitle("");
    h->SetStats(0);
    if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
    h->SetMinimum(0.995 * ymin);
    h->SetMaximum(1.005 * ymax);
    gPad->Modified();
    gPad->Update();

    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        fSampleRatioLayer[il][iv][is]->Draw("SAME HIST");
        fSampleRatioLayer[il][iv][is]->SetStats(0);

        gPad->Modified();
        gPad->Update();

        TString text = Form("%d%s = %.3f", il, fXY[iv].Data(), fSampleRatioLayer[il][iv][is]->GetMean());
        Double_t xmin = h->GetXaxis()->GetBinLowEdge(1);
        Double_t xmax = h->GetXaxis()->GetBinUpEdge(h->GetXaxis()->GetNbins());
        if (this->fOperation == kLHC2022) {
          xmin = fZoomMin;
          xmax = fZoomMax;
        }
        Double_t xscale = 0.210 * (xmax - xmin);
        Double_t px = xmax - xscale;
        Double_t yscale = 0.350 * (ymax - ymin) / (this->kPosNlayer * this->kPosNview);
        Double_t py = ymax - (il * this->kPosNview + iv + 1) * yscale;

        TLatex *ltex = new TLatex(px, py, text.Data());
        ltex->SetTextColor(fSampleRatioLayer[il][iv][is]->GetLineColor());
        ltex->SetTextSize(0.035);
        ltex->SetTextFont(42);
        gPad->Update();
        cc->Modified();

        ltex->Draw("SAME");

        gPad->Modified();
        gPad->Update();
      }
    }

    cc->Print(Form("%sSample%dToSample%dRatio_Run_%d_%d.eps",  //
                   GetPathName(fOutputName).Data(), is, fSampleRef, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotChipRatioHisto() {
  for (Int_t is = 0; is < this->kPosNsample; ++is) {
    if (is == fSampleRef) continue;

    for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
      for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
        TCanvas *cc = new TCanvas(Form("cc%d%d%d", is, il, iv), "", 1400, 700);
        cc->Divide(2, 1);

        cc->cd(1);
        //
        TH1D *reference = (TH1D *)fSampleRatioLayer[il][iv][is]->Clone();
        reference->SetLineColor(kGray + 1);
        reference->SetFillColor(kGray);
        reference->Draw("HIST");
        reference->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
        reference->SetStats(1);
        if (this->fOperation == kLHC2022) reference->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);

        cc->cd(2);
        //
        Double_t ymin = 1e+6;
        Double_t ymax = 1e-6;
        for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {
          fSampleRatioChip[il][iv][ic][is]->Draw("HIST");
          Double_t lmin = fSampleRatioChip[il][iv][ic][is]->GetMinimum();
          Double_t lmax = fSampleRatioChip[il][iv][ic][is]->GetMaximum();
          if (lmin < ymin) ymin = lmin;
          if (lmax > ymax) ymax = lmax;
        }
        //
        TH1D *h = (TH1D *)fSampleRatioChip[0][0][0][is]->Clone();
        h->Draw("HIST");
        h->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
        h->SetStats(0);
        if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
        h->SetMinimum(0.995 * ymin);
        h->SetMaximum(1.005 * ymax);
        gPad->Modified();
        gPad->Update();

        for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {  // This variable is on PACE3 in the software
          Int_t thisPACE = fPACEID[ic];                    // This loop is on PACE3 on the hybrid

          fSampleRatioChip[il][iv][ic][is]->Draw("SAME HIST");
          fSampleRatioChip[il][iv][ic][is]->SetStats(0);

          gPad->Modified();
          gPad->Update();

          TString text = Form("PACE %02d(%02d) = %.3f", thisPACE, ic, fSampleRatioChip[il][iv][ic][is]->GetMean());
          Double_t xmin = h->GetXaxis()->GetBinLowEdge(1);
          Double_t xmax = h->GetXaxis()->GetBinUpEdge(h->GetXaxis()->GetNbins());
          if (this->fOperation == kLHC2022) {
            xmin = fZoomMin;
            xmax = fZoomMax;
          }
          Double_t xscale = 0.400 * (xmax - xmin);
          Double_t px = xmax - xscale;
          Double_t yscale = 0.500 * (ymax - ymin) / fNPacePerHybrid;
          Double_t py = ymax - (thisPACE + 1) * yscale;

          TLatex *ltex = new TLatex(px, py, text.Data());
          ltex->SetTextColor(fSampleRatioChip[il][iv][ic][is]->GetLineColor());
          ltex->SetTextSize(0.035);
          ltex->SetTextFont(42);
          gPad->Update();
          cc->Modified();

          ltex->Draw("SAME");

          gPad->Modified();
          gPad->Update();
        }

        cc->Print(Form("%sSample%dToSample%dRatio_Layer%d%s_Run_%d_%d.eps",  //
                       GetPathName(fOutputName).Data(), is, fSampleRef, il, fXY[iv].Data(), fFirstRun, fLastRun));
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotSubTDCSignalHisto() {
  TCanvas *cs = new TCanvas("cs", "", 1400, 700);
  cs->Divide(2, 1);

  for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
    for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
      cs->cd(1 + iv);
      Double_t ymin = 1e+6;
      Double_t ymax = 1e-6;
      for (UInt_t ib = 0; ib < fTDCSubBin; ++ib) {
        fSubTDCvsRefSignal[il][iv][ib]->Draw("HIST");
        fSubTDCvsRefSignal[il][iv][ib]->GetXaxis()->SetRangeUser(0.51, 30.49);
        Double_t lmin = fSubTDCvsRefSignal[il][iv][ib]->GetMinimum();
        Double_t lmax = fSubTDCvsRefSignal[il][iv][ib]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }
      //
      TH1D *h = (TH1D *)fSubTDCvsRefSignal[il][iv][0]->Clone();
      h->SetTitle(fXY[iv].Data());
      h->Draw("HIST");
      h->SetStats(0);
      if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
      h->SetMinimum(0.95 * ymin);
      h->SetMaximum(1.05 * ymax);
      gPad->Modified();
      gPad->Update();
      //
      for (UInt_t ib = 0; ib < fTDCSubBin; ++ib) {
        fSubTDCvsRefSignal[il][iv][ib]->Draw("SAME HIST");
        fSubTDCvsRefSignal[il][iv][ib]->SetStats(0);
      }
      gPad->Modified();
      gPad->Update();
      gPad->SetGrid();
      gPad->Modified();
      gPad->Update();
    }
    cs->Print(Form("%sSubTDCSignal_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), il, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotSubTDCRatioHisto() {
  TCanvas *cr = new TCanvas("cr", "", 1400, 700);
  cr->Divide(2, 1);

  for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
    for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
      cr->cd(1 + iv);
      Double_t ymin = 1e+6;
      Double_t ymax = 1e-6;
      for (UInt_t ib = 0; ib < fTDCSubBin; ++ib) {
        fSubTDCvsRefRatio[il][iv][ib]->Draw("HIST");
        fSubTDCvsRefRatio[il][iv][ib]->GetXaxis()->SetRangeUser(0.51, 30.49);
        Double_t lmin = fSubTDCvsRefRatio[il][iv][ib]->GetMinimum();
        Double_t lmax = fSubTDCvsRefRatio[il][iv][ib]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }
      //
      TH1D *h = (TH1D *)fSubTDCvsRefRatio[il][iv][0]->Clone();
      h->SetTitle(fXY[iv].Data());
      h->Draw("HIST");
      h->SetStats(0);
      if (this->fOperation == kLHC2022) h->GetXaxis()->SetRangeUser(fZoomMin, fZoomMax);
      h->SetMinimum(0.95 * ymin);
      h->SetMaximum(1.05 * ymax);
      gPad->Modified();
      gPad->Update();
      //
      for (UInt_t ib = 0; ib < fTDCSubBin; ++ib) {
        fSubTDCvsRefRatio[il][iv][ib]->Draw("SAME HIST");
        fSubTDCvsRefRatio[il][iv][ib]->SetStats(0);
      }
      gPad->Modified();
      gPad->Update();
      gPad->SetGrid();
      gPad->Modified();
      gPad->Update();
    }
    cr->Print(Form("%sSubTDCRatio_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), il, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotSubTDCHisto() {
  PlotSubTDCSignalHisto();
  PlotSubTDCRatioHisto();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotSignalHisto() {
  for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
    TCanvas *cc = new TCanvas(Form("ks%d", il), "", 1400, 700);
    cc->Divide(2, 1);
    for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
      cc->cd(1 + iv);

      Double_t xmin = 0.;
      Double_t xmax = 0.;
      Double_t ymin = 1e+6;
      Double_t ymax = 1e-6;
      if (this->fOperation == kSPS2022) {
        xmin = -130.;
        xmax = -100.;
      }
      for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {
        fTDCSignalForCorrection[il][iv][ic]->Draw("HIST");
        Double_t lmin = fTDCSignalForCorrection[il][iv][ic]->GetMinimum();
        Double_t lmax = fTDCSignalForCorrection[il][iv][ic]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }

      TH1D *h = (TH1D *)fTDCSignalForCorrection[il][iv][0]->Clone();
      h->Draw("HIST");
      h->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
      h->SetStats(0);
      h->GetXaxis()->SetRangeUser(xmin, xmax);
      h->SetMinimum(ymin);
      h->SetMaximum(ymax);
      gPad->Modified();
      gPad->Update();
      gPad->SetGrid();

      for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {  // This variable is on PACE3 in the software
        Int_t thisPACE = fPACEID[ic];                    // This loop is on PACE3 on the hybrid

        fTDCSignalForCorrection[il][iv][ic]->Draw("SAME HIST");
        fTDCSignalForCorrection[il][iv][ic]->SetStats(0);

        gPad->Modified();
        gPad->Update();

        TString text = Form("PACE %02d -> %02d", thisPACE, ic);
        Double_t xscale = 1.;
        Double_t px = xmin + xscale;
        Double_t yscale = 0.500 * (ymax - ymin) / fNPacePerHybrid;
        Double_t py = ymax - (thisPACE + 1) * yscale;

        TLatex *ltex = new TLatex(px, py, text.Data());
        ltex->SetTextColor(fTDCSignalForCorrection[il][iv][ic]->GetLineColor());
        ltex->SetTextSize(0.035);
        ltex->SetTextFont(42);
        gPad->Update();
        cc->Modified();

        ltex->Draw("SAME");

        gPad->Modified();
        gPad->Update();
      }
    }
    cc->Print(Form("%sSignal_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), il, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotRatioHisto() {
  for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
    TCanvas *cc = new TCanvas(Form("kr%d", il), "", 1400, 700);
    cc->Divide(2, 1);
    for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
      cc->cd(1 + iv);

      //
      Double_t xmin = 0.;
      Double_t xmax = 0.;
      Double_t ymin = 0.;
      Double_t ymax = 0.;
      if (this->fOperation == kSPS2022) {
        xmin = -130.;
        xmax = -100.;
        ymin = +0.0;
        ymax = +3.0;
      }

      TH1D *h = (TH1D *)fTDCRatioForCorrection[il][iv][0]->Clone();
      h->Draw("HIST");
      h->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
      h->SetStats(0);
      h->GetXaxis()->SetRangeUser(xmin, xmax);
      h->SetMinimum(ymin);
      h->SetMaximum(ymax);
      gPad->Modified();
      gPad->Update();
      gPad->SetGrid();

      for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {  // This variable is on PACE3 in the software
        Int_t thisPACE = fPACEID[ic];                    // This loop is on PACE3 on the hybrid

        fTDCRatioForCorrection[il][iv][ic]->Draw("SAME HIST");
        fTDCRatioForCorrection[il][iv][ic]->SetStats(0);

        gPad->Modified();
        gPad->Update();

        TString text = Form("PACE %02d -> %02d", thisPACE, ic);
        Double_t xscale = 0.290 * (xmax - xmin);
        Double_t px = xmax - xscale;
        Double_t yscale = 0.500 * (ymax - ymin) / fNPacePerHybrid;
        Double_t py = ymax - (thisPACE + 1) * yscale;

        TLatex *ltex = new TLatex(px, py, text.Data());
        ltex->SetTextColor(fTDCRatioForCorrection[il][iv][ic]->GetLineColor());
        ltex->SetTextSize(0.035);
        ltex->SetTextFont(42);
        gPad->Update();
        cc->Modified();

        ltex->Draw("SAME");

        gPad->Modified();
        gPad->Update();
      }
    }
    cc->Print(Form("%sRatio_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), il, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotCorrelationHisto() {
  for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
    TCanvas *cc = new TCanvas(Form("kc%d", il), "", 1400, 700);
    cc->Divide(2, 1);
    for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
      cc->cd(1 + iv);

      Double_t xmin = 0.;
      Double_t xmax = 0.;
      Double_t ymin = 0.;
      Double_t ymax = 0.;
      if (this->fOperation == kSPS2022) {
        xmin = +0.0;
        xmax = +3.0;
        ymin = +0.0;
        ymax = +3.0;
      }

      TMultiGraph *mg = new TMultiGraph();
      mg->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
      mg->GetXaxis()->SetTitle(fRatioVsSignal[il][iv][0]->GetXaxis()->GetTitle());
      mg->GetYaxis()->SetTitle(fRatioVsSignal[il][iv][0]->GetYaxis()->GetTitle());
      for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {
        mg->Add(fRatioVsSignal[il][iv][ic]);
      }
      mg->Draw("AP");
      gPad->SetGrid();
      gPad->Modified();
      gPad->Update();
      mg->GetXaxis()->SetLimits(xmin, xmax);
      mg->SetMinimum(ymin);
      mg->SetMaximum(ymax);
      for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {  // This variable is on PACE3 in the software
        Int_t thisPACE = fPACEID[ic];                    // This loop is on PACE3 on the hybrid

        gPad->Modified();
        gPad->Update();

        TString text = Form("PACE %02d -> %02d", thisPACE, ic);
        Double_t xscale = 0.290 * (xmax - xmin);
        Double_t px = xmax - xscale;
        Double_t yscale = 0.500 * (ymax - ymin) / fNPacePerHybrid;
        Double_t py = ymax - (thisPACE + 1) * yscale;

        TLatex *ltex = new TLatex(px, py, text.Data());
        ltex->SetTextColor(fRatioVsSignal[il][iv][ic]->GetLineColor());
        ltex->SetTextSize(0.035);
        ltex->SetTextFont(42);
        gPad->Update();
        cc->Modified();

        ltex->Draw("SAME");

        gPad->Modified();
        gPad->Update();
      }
    }
    cc->Print(Form("%sSignalVsRatio_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), il, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::PlotCorrectionHisto() {
  for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
    TCanvas *cc = new TCanvas(Form("kk%d", il), "", 1400, 700);
    cc->Divide(2, 1);
    for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
      cc->cd(1 + iv);

      Double_t xmin = 0.;
      Double_t xmax = 0.;
      Double_t ymin = 0.;
      Double_t ymax = 0.;
      if (this->fOperation == kSPS2022) {
        xmin = -130.;
        xmax = -100.;
        ymin = +0.0;
        ymax = +3.0;
      }

      TH1D *h = (TH1D *)fTDCCorrection[il][iv][0]->Clone();
      h->Draw("HIST");
      h->SetTitle(Form("Layer %d%s", il, fXY[iv].Data()));
      h->SetStats(0);
      h->GetXaxis()->SetRangeUser(xmin, xmax);
      h->SetMinimum(ymin);
      h->SetMaximum(ymax);
      gPad->Modified();
      gPad->Update();
      gPad->SetGrid();

      for (UInt_t ic = 0; ic < fNPacePerHybrid; ++ic) {  // This variable is on PACE3 in the software
        Int_t thisPACE = fPACEID[ic];                    // This loop is on PACE3 on the hybrid

        fTDCCorrection[il][iv][ic]->Draw("SAME HIST");
        fTDCCorrection[il][iv][ic]->SetStats(0);

        gPad->Modified();
        gPad->Update();

        TString text = Form("PACE %02d -> %02d", thisPACE, ic);
        Double_t xscale = 0.290 * (xmax - xmin);
        Double_t px = xmax - xscale;
        Double_t yscale = 0.500 * (ymax - ymin) / fNPacePerHybrid;
        Double_t py = ymax - (thisPACE + 1) * yscale;

        TLatex *ltex = new TLatex(px, py, text.Data());
        ltex->SetTextColor(fTDCCorrection[il][iv][ic]->GetLineColor());
        ltex->SetTextSize(0.035);
        ltex->SetTextFont(42);
        gPad->Update();
        cc->Modified();

        ltex->Draw("SAME");

        gPad->Modified();
        gPad->Update();
      }
    }
    cc->Print(Form("%sCorrection_Layer%d_Run_%d_%d.eps", GetPathName(fOutputName).Data(), il, fFirstRun, fLastRun));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();

  for (UInt_t il = 0; il < fHitmap.size(); ++il) {
    for (UInt_t iv = 0; iv < fHitmap[il].size(); ++iv) {
      fHitmap[il][iv]->Write();
    }
  }
  //
  if (fEqualize) {
    for (UInt_t il = 0; il < fPositionVsSignal.size(); ++il) {
      for (UInt_t iv = 0; iv < fPositionVsSignal[il].size(); ++iv) {
        for (UInt_t is = 0; is < fPositionVsSignal[il][iv].size(); ++is) {
          fPositionVsSignal[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fPosVsSigWeight.size(); ++il) {
      for (UInt_t iv = 0; iv < fPosVsSigWeight[il].size(); ++iv) {
        for (UInt_t is = 0; is < fPosVsSigWeight[il][iv].size(); ++is) {
          fPosVsSigWeight[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fPosVsEneWeight.size(); ++il) {
      for (UInt_t iv = 0; iv < fPosVsEneWeight[il].size(); ++iv) {
        for (UInt_t is = 0; is < fPosVsEneWeight[il][iv].size(); ++is) {
          fPosVsEneWeight[il][iv][is]->Write();
        }
      }
    }
  }
  //
  for (UInt_t is = 0; is < fSampleSignalTotal.size(); ++is) {
    fSampleSignalTotal[is]->Write();
  }
  for (UInt_t il = 0; il < fSampleSignalLayer.size(); ++il) {
    for (UInt_t iv = 0; iv < fSampleSignalLayer[il].size(); ++iv) {
      for (UInt_t is = 0; is < fSampleSignalLayer[il][iv].size(); ++is) {
        fSampleSignalLayer[il][iv][is]->Write();
      }
    }
  }
  for (UInt_t il = 0; il < fSampleSignalChip.size(); ++il) {
    for (UInt_t iv = 0; iv < fSampleSignalChip[il].size(); ++iv) {
      for (UInt_t ic = 0; ic < fSampleSignalChip[il][iv].size(); ++ic) {
        for (UInt_t is = 0; is < fSampleSignalChip[il][iv][ic].size(); ++is) {
          fSampleSignalChip[il][iv][ic][is]->Write();
        }
      }
    }
  }
  //
  if (fInputLevel == 1) {
    for (UInt_t is = 0; is < fSampleRatioTotal.size(); ++is) {
      if (is == fSampleRef) continue;
      fSampleRatioTotal[is]->Write();
    }
    for (UInt_t il = 0; il < fSampleRatioLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fSampleRatioLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fSampleRatioLayer[il][iv].size(); ++is) {
          if (is == fSampleRef) continue;
          fSampleRatioLayer[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fSampleRatioChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fSampleRatioChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fSampleRatioChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fSampleRatioChip[il][iv][ic].size(); ++is) {
            if (is == fSampleRef) continue;
            fSampleRatioChip[il][iv][ic][is]->Write();
          }
        }
      }
    }
  }
  //
  //
  //
  if (fEqualize) {
    for (UInt_t il = 0; il < fProfilePositionSignal.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfilePositionSignal[il].size(); ++iv) {
        for (UInt_t is = 0; is < fProfilePositionSignal[il][iv].size(); ++is) {
          fProfilePositionSignal[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fProfilePosSigWeight.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfilePosSigWeight[il].size(); ++iv) {
        for (UInt_t is = 0; is < fProfilePosSigWeight[il][iv].size(); ++is) {
          fProfilePosSigWeight[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fProfilePosEneWeight.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfilePosEneWeight[il].size(); ++iv) {
        for (UInt_t is = 0; is < fProfilePosEneWeight[il][iv].size(); ++is) {
          fProfilePosEneWeight[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fProfilePosSigAverage.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfilePosSigAverage[il].size(); ++iv) {
        for (UInt_t is = 0; is < fProfilePosSigAverage[il][iv].size(); ++is) {
          fProfilePosSigAverage[il][iv][is]->Write();
        }
      }
    }
  }
  //
  //
  //
  if (fTDC) {
    for (UInt_t is = 0; is < fTDCvsSignalTotal.size(); ++is) {
      fTDCvsSignalTotal[is]->Write();
    }
    for (UInt_t il = 0; il < fTDCvsSignalLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsSignalLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fTDCvsSignalLayer[il][iv].size(); ++is) {
          fTDCvsSignalLayer[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCvsSignalChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsSignalChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsSignalChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsSignalChip[il][iv][ic].size(); ++is) {
            fTDCvsSignalChip[il][iv][ic][is]->Write();
          }
        }
      }
    }
    for (UInt_t il = 0; il < fSubTDCvsRefSignal.size(); ++il) {
      for (UInt_t iv = 0; iv < fSubTDCvsRefSignal[il].size(); ++iv) {
        for (UInt_t ib = 0; ib < fSubTDCvsRefSignal[il][iv].size(); ++ib) {
          fSubTDCvsRefSignal[il][iv][ib]->Write();
        }
      }
    }
    //
    for (UInt_t is = 0; is < fTDCvsRatioTotal.size(); ++is) {
      if (is == fSampleRef) continue;
      fTDCvsRatioTotal[is]->Write();
    }
    for (UInt_t il = 0; il < fTDCvsRatioLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsRatioLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fTDCvsRatioLayer[il][iv].size(); ++is) {
          if (is == fSampleRef) continue;
          fTDCvsRatioLayer[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCvsRatioChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsRatioChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsRatioChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsRatioChip[il][iv][ic].size(); ++is) {
            if (is == fSampleRef) continue;
            fTDCvsRatioChip[il][iv][ic][is]->Write();
          }
        }
      }
    }
    for (UInt_t il = 0; il < fSubTDCvsRefRatio.size(); ++il) {
      for (UInt_t iv = 0; iv < fSubTDCvsRefRatio[il].size(); ++iv) {
        for (UInt_t ib = 0; ib < fSubTDCvsRefRatio[il][iv].size(); ++ib) {
          fSubTDCvsRefRatio[il][iv][ib]->Write();
        }
      }
    }
    //
    for (UInt_t il = 0; il < fTDCvsShaperPerSample.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsShaperPerSample[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsShaperPerSample[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fTDCvsShaperPerSample[il][iv][ic].size(); ++is) {
            fTDCvsShaperPerSample[il][iv][ic][is]->Write();
          }
        }
      }
    }
    //
    for (UInt_t il = 0; il < fTDCvsShaperGlobal.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCvsShaperGlobal[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCvsShaperGlobal[il][iv].size(); ++ic) {
          fTDCvsShaperGlobal[il][iv][ic]->Write();
        }
      }
    }
    //
    //
    //
    for (UInt_t il = 0; il < fProfileShaperGlobal.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileShaperGlobal[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fProfileShaperGlobal[il][iv].size(); ++ic) {
          fPositionDependence[il][iv][ic]->Write();
        }
      }
    }
    //
    //
    //
    for (UInt_t is = 0; is < fProfileSignalTotal.size(); ++is) {
      fProfileSignalTotal[is]->Write();
    }
    for (UInt_t il = 0; il < fProfileSignalLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileSignalLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fProfileSignalLayer[il][iv].size(); ++is) {
          fProfileSignalLayer[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fProfileSignalChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileSignalChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fProfileSignalChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fProfileSignalChip[il][iv][ic].size(); ++is) {
            fProfileSignalChip[il][iv][ic][is]->Write();
          }
        }
      }
    }
    //
    for (UInt_t is = 0; is < fTDCvsRatioTotal.size(); ++is) {
      if (is == fSampleRef) continue;
      fProfileRatioTotal[is]->Write();
    }
    for (UInt_t il = 0; il < fProfileRatioLayer.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileRatioLayer[il].size(); ++iv) {
        for (UInt_t is = 0; is < fProfileRatioLayer[il][iv].size(); ++is) {
          if (is == fSampleRef) continue;
          fProfileRatioLayer[il][iv][is]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fProfileRatioChip.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileRatioChip[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fProfileRatioChip[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fProfileRatioChip[il][iv][ic].size(); ++is) {
            if (is == fSampleRef) continue;
            fProfileRatioChip[il][iv][ic][is]->Write();
          }
        }
      }
    }
    //
    for (UInt_t il = 0; il < fProfileShaperPerSample.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileShaperPerSample[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fProfileShaperPerSample[il][iv].size(); ++ic) {
          for (UInt_t is = 0; is < fProfileShaperPerSample[il][iv][ic].size(); ++is) {
            fProfileShaperPerSample[il][iv][ic][is]->Write();
          }
        }
      }
    }
    //
    for (UInt_t il = 0; il < fProfileShaperGlobal.size(); ++il) {
      for (UInt_t iv = 0; iv < fProfileShaperGlobal[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fProfileShaperGlobal[il][iv].size(); ++ic) {
          fProfileShaperGlobal[il][iv][ic]->Write();
        }
      }
    }
    //
    for (UInt_t il = 0; il < fTDCSignalForCorrection.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCSignalForCorrection[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCSignalForCorrection[il][iv].size(); ++ic) {
          fTDCSignalForCorrection[il][iv][ic]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCRatioForCorrection.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCRatioForCorrection[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCRatioForCorrection[il][iv].size(); ++ic) {
          fTDCRatioForCorrection[il][iv][ic]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCCorrection.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCCorrection[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCCorrection[il][iv].size(); ++ic) {
          fTDCCorrection[il][iv][ic]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fRatioVsSignal.size(); ++il) {
      for (UInt_t iv = 0; iv < fRatioVsSignal[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fRatioVsSignal[il][iv].size(); ++ic) {
          fRatioVsSignal[il][iv][ic]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCSignalSpline.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCSignalSpline[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCSignalSpline[il][iv].size(); ++ic) {
          fTDCSignalSpline[il][iv][ic]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCRatioSpline.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCRatioSpline[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCRatioSpline[il][iv].size(); ++ic) {
          fTDCRatioSpline[il][iv][ic]->Write();
        }
      }
    }
    for (UInt_t il = 0; il < fTDCCorrectionSpline.size(); ++il) {
      for (UInt_t iv = 0; iv < fTDCCorrectionSpline[il].size(); ++iv) {
        for (UInt_t ic = 0; ic < fTDCCorrectionSpline[il][iv].size(); ++ic) {
          fTDCCorrectionSpline[il][iv][ic]->Write();
        }
      }
    }
    //
    //
    //
    fTDCCorrelation->Write();
    fTDCDifference->Write();
  }
  //
  for (UInt_t it = 0; it < fParticleEnergy.size(); ++it) {
    fParticleEnergy[it]->Write();
  }

  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*---------------------------*/
/*--- Auxiliary functions ---*/
/*---------------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
TString SiliconLatency<armcal, armrec, arman, armclass>::GetPathName(TString sin) {
  TString sout = "";

  string s = sin.Data();

  size_t i = s.rfind('/', s.length());
  if (i != string::npos) {
    sout = s.substr(0, i) + "/";
  }

  return sout;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
TGraph *SiliconLatency<armcal, armrec, arman, armclass>::HistoToGraph(TH1 *histo) {
  TGraph *graph = new TGraph();
  Int_t gpoint = 0;
  for (int ib = 1; ib <= histo->GetXaxis()->GetNbins(); ++ib) {
    if (histo->GetBinContent(ib) > 0.) {
      graph->SetPoint(gpoint, histo->GetBinCenter(ib), histo->GetBinContent(ib));
      ++gpoint;
    }
  }
  return graph;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
TSpline3 *SiliconLatency<armcal, armrec, arman, armclass>::HistoToSpline(TH1 *histo) {
  Int_t nv = 0;
  vector <Double_t> vx = vector <Double_t>();
  vector <Double_t> vy = vector <Double_t>();
  for (int ib = 1; ib <= histo->GetXaxis()->GetNbins(); ++ib) {
    if (histo->GetBinContent(ib) != 0.) {
	++nv;
	vx.push_back(histo->GetBinCenter(ib));
	vy.push_back(histo->GetBinContent(ib));
    }
  }
  TSpline3 *spline;
  if(nv<=0)
  	spline = new TSpline3();
  else 
  	spline = new TSpline3("", &vx[0], &vy[0], nv);
  return spline;
}

/*
template <typename armcal, typename armrec, typename arman, typename armclass>
TSpline3 *SiliconLatency<armcal, armrec, arman, armclass>::HistoToSpline(TH1 *histo) {
  TSpline3 *spline = new TSpline3("", HistoToGraph(histo));
  spline->Print("ALL");
  return spline;
}
*/

template <typename armcal, typename armrec, typename arman, typename armclass>
Int_t SiliconLatency<armcal, armrec, arman, armclass>::FromStripToTower(Int_t view, Int_t strip) {
  for (UInt_t tower = 0; tower < this->kCalNtower; ++tower)
    if (strip >= fSiliconRange[tower][view][0] && strip <= fSiliconRange[tower][view][1]) {
      return tower;
    }
  return -1;
}

/*-------------*/
/*--- Check ---*/
/*-------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconLatency<armcal, armrec, arman, armclass>::Find() {
  if (fInputLevel != 1 && fInputLevel != 2) {
    UT::Printf(UT::kPrintError, "Invalid input layer %d\n", fInputLevel);
    exit(EXIT_FAILURE);
  }

  SetInputTree();

  fOutputFile = new TFile(fOutputName, "RECREATE");

  InitSiliconRange();

  InitTDCSubBins();

  MakeHistograms();

  /* Event Loop */
  EventLoop();

  /* Get Profile */
  GetProfile();

  /* Get Correction */
  if (fTDC && fCorrection) {
    GetCorrection();
  }

  /* Save S0/S1 average value */
  SaveS0S1Ratio();

  /* Plot main histograms */
  PlotHisto();

  /* Write to output file */
  WriteToOutput();

  /* Close all files */
  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class SiliconLatency<Arm1CalPars, Arm1RecPars, Arm1AnPars, Arm1Params>;
template class SiliconLatency<Arm2CalPars, Arm2RecPars, Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
