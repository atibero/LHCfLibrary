#include <TRint.h>

#include "SiliconLatency.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/

/*--- Default values ---*/
string input_dir = "";
int first_run = -1;
int last_run = -1;
string output_file = "latency.root";
int fillnum = -1;
int subfill = -1;
string table_folder = "";
double minthr = 0;
double maxthr = 1e6;
double minene = 0.;
double maxene = 1e6;
bool maximum = false;
bool vertex = false;
bool photon = false;
bool neutron = false;
bool singlehit = false;
int iarm = -1;
int level = -1;
bool tdc = false;
bool correction = false;
bool equalization = false;
int verbose = 1;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (or input file if ending with \".root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = %d)\n", first_run);
  printf("\t-l <last>\t\tLast run to be analysed (default = %d)\n", last_run);
  printf("\t-o <output>\t\tOutput ROOT file (default = \"%s\")\n", output_file.c_str());
  printf("\t-t <tables_dir>\t\t\tSet tables directory (default = \"../Tables\")\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t-m <minthr>\t\tMinimum deposit in single string (default = %.0f)\n", minthr);
  printf("\t-M <maxthr>\t\tMaximum deposit in single string (default = %.0f)\n", maxthr);
  printf("\t-e <minene>\t\tMinimum calorimetric energy [Lvl3 required] (default = %.0f)\n", minene);
  printf("\t-E <maxene>\t\tMaximum calorimetric energy [Lvl3 required] (default = %.0f)\n", maxene);
  printf("\t--maximum\t\tSelect only maximum layer [Lvl3 required] (default = %d)\n", vertex);
  printf("\t--vertex\t\tSelect only strip along shower axis [Lvl3 required] (default = %d)\n", vertex);
  printf("\t--photon\t\tSelect only photon shower [Lvl3 required] (default = %.0f)\n", photon);
  printf("\t--neutron\t\tSelect only neutron shower [Lvl3 required] (default = %.0f)\n", neutron);
  printf("\t--singlehit\t\tSelect only photon singlehit [Lvl3 required] (default = %.0f)\n", singlehit);
  printf("\t--arm1\t\t\tArm1 input file [either \"--arm1\" or \"--arm2\" required]\n");
  printf("\t--arm2\t\t\tArm2 input file [either \"--arm1\" or \"--arm2\" required]\n");
  printf("\t--lvl1\t\t\tUse Level1 [either \"--lvl1\" or \"--lvl2\" required]\n");
  printf("\t--lvl2\t\t\tUse Level2 [either \"--lvl1\" or \"--lvl2\" required]\n");
  printf("\t--tdc\t\t\tShow correlation with TDC trigger time (default = %d)\n", tdc);
  printf("\t--correction\t\tCompute TDC correction for SPS (default = %d)\n", correction);
  printf("\t--equalization\t\tCompute variables used for equalization (default = %d)\n", equalization);
  printf("\t-v <verbose>\t\tSet verbose level (default = %d)\n", verbose);
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"output", required_argument, NULL, 'o'},
      {"tables-dir", required_argument, NULL, 't'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"minthr", required_argument, NULL, 'm'},
      {"maxthr", required_argument, NULL, 'M'},
      {"minene", required_argument, NULL, 'e'},
      {"maxene", required_argument, NULL, 'E'},
      {"maximum", no_argument, NULL, 901},
      {"vertex", no_argument, NULL, 902},
      {"photon", no_argument, NULL, 903},
      {"neutron", no_argument, NULL, 904},
      {"singlehit", no_argument, NULL, 905},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"lvl1", no_argument, NULL, 1101},
      {"lvl2", no_argument, NULL, 1102},
      {"tdc", no_argument, NULL, 1201},
      {"correction", no_argument, NULL, 1202},
      {"equalization", no_argument, NULL, 1203},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 't':
        if (optarg) table_folder = optarg;
        break;
      case 'F':
        if (optarg) fillnum = atoi(optarg);
        break;
      case 'S':
        if (optarg) subfill = atoi(optarg);
        break;
      case 'm':
        minthr = atof(optarg);
        break;
      case 'M':
        maxthr = atof(optarg);
        break;
      case 'e':
        minene = atof(optarg);
        break;
      case 'E':
        maxene = atof(optarg);
        break;
      case 901:
        maximum = true;
        break;
      case 902:
        vertex = true;
        break;
      case 903:
        photon = true;
        break;
      case 904:
        neutron = true;
        break;
      case 905:
        singlehit = true;
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 1101:
        level = 1;
        break;
      case 1102:
        level = 2;
        break;
      case 1201:
        tdc = true;
        break;
      case 1202:
        correction = true;
        break;
      case 1203:
        equalization = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "" || iarm < 0 || iarm > 2 || level < 0 || level > 2) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d -> %d\n", first_run, last_run);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  Utils::SetVerbose(verbose);

  /*--- Latency ---*/

  if (table_folder != "") {
    Arm1Params::SetTableDirectory(table_folder.c_str());
    Arm2Params::SetTableDirectory(table_folder.c_str());
  }
  Arm1AnPars::SetFill(fillnum, subfill);
  Arm2AnPars::SetFill(fillnum, subfill);

  SiliconLatency<Arm1CalPars, Arm1RecPars, Arm1AnPars, Arm1Params> lat_1;
  SiliconLatency<Arm2CalPars, Arm2RecPars, Arm2AnPars, Arm2Params> lat_2;
  if (iarm == Arm1Params::kArmIndex) {
    lat_1.SetInputDir(input_dir.c_str());
    lat_1.SetFirstRun(first_run);
    lat_1.SetLastRun(last_run);
    lat_1.SetOutputName(output_file.c_str());
    lat_1.SetInputLevel(level);
    lat_1.SetStripThreshold(minthr, maxthr);
    lat_1.SetEnergyThreshold(minene, maxene);
    if (tdc) lat_1.AddTDC();
    if (maximum) lat_1.SelectMaximumLayer();
    if (vertex) lat_1.SelectVertexStrip();
    if (photon) lat_1.SelectPhoton();
    if (neutron) lat_1.SelectNeutron();
    if (singlehit) lat_1.SelectSinglehit();
    if (correction) lat_1.ComputeCorrection();
    if (equalization) lat_1.ComputeEqualization();
    lat_1.Find();
  } else if (iarm == Arm2Params::kArmIndex) {
    lat_2.SetInputDir(input_dir.c_str());
    lat_2.SetFirstRun(first_run);
    lat_2.SetLastRun(last_run);
    lat_2.SetOutputName(output_file.c_str());
    lat_2.SetInputLevel(level);
    lat_2.SetStripThreshold(minthr, maxthr);
    lat_2.SetEnergyThreshold(minene, maxene);
    if (tdc) lat_2.AddTDC();
    if (maximum) lat_2.SelectMaximumLayer();
    if (vertex) lat_2.SelectVertexStrip();
    if (photon) lat_2.SelectPhoton();
    if (neutron) lat_2.SelectNeutron();
    if (singlehit) lat_2.SelectSinglehit();
    if (correction) lat_2.ComputeCorrection();
    if (equalization) lat_2.ComputeEqualization();
    lat_2.Find();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
