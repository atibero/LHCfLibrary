#include <TRint.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "EventViewer.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput file\n");
  printf("\t-l <level>\t\tInput level is 0, 1, or 2 (default = 2)\n");
  printf("\t--arm1\t\t\tArm1 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--scaler\t\tAdd scaler canvas (default = Off)\n");
  printf("\t-W\t\t\tCanvas Width\n");
  printf("\t-H\t\t\tCanvas Height\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_file = "";
  int level = 2;
  int iarm = -1;
  int width = 1000;
  int height = 800;
  int verbose = 1;
  bool scaler = false;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},  {"level", required_argument, NULL, 'l'},
      {"arm1", no_argument, NULL, 1001},        {"arm2", no_argument, NULL, 1002},
      {"scaler", no_argument, NULL, 1101},      {"width", required_argument, NULL, 'W'},
      {"height", required_argument, NULL, 'H'}, {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},         {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_file = optarg;
        break;
      case 'l':
        level = atoi(optarg);
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 1101:
        scaler = true;
        break;
      case 'W':
        width = atoi(optarg);
        break;
      case 'H':
        height = atoi(optarg);
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  /*--- Check main input variables ---*/
  if (input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  if (level < 0 || level > 2) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  /*--- Print software version ---*/
  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  EventViewer<Arm1Params, Arm1RecPars> viewer_1;
  EventViewer<Arm2Params, Arm2RecPars> viewer_2;
  if (iarm == Arm1Params::kArmIndex) {
    viewer_1.Open(input_file.c_str());
    viewer_1.SetLevel(level);
    viewer_1.SetCanvasSize(width, height);
    if (!scaler) viewer_1.SetDrawOption(Form("canvas1 lvl%d", level));
    viewer_1.Run();
  } else if (iarm == Arm2Params::kArmIndex) {
    viewer_2.Open(input_file.c_str());
    viewer_2.SetLevel(level);
    viewer_2.SetCanvasSize(width, height);
    if (!scaler) viewer_2.SetDrawOption(Form("canvas1 lvl%d", level));
    viewer_2.Run();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
