#include "EventViewer.hh"

#include <TROOT.h>

#include <Arm1RecPars.hh>
#include <Arm2RecPars.hh>
#include <iostream>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "Utils.hh"

using namespace nLHCf;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass, typename armrec>
EventViewer<armclass, armrec>::EventViewer() {
  fTree = NULL;
  fEvent = NULL;
  fUseLevel = 2;
  fNextEvent = 0;
  fDrawOption = Form("canvas1 canvas2 lvl%d", fUseLevel);
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass, typename armrec>
EventViewer<armclass, armrec>::~EventViewer() {
  // if (fInputFile != NULL) {
  //   fInputFile->Close();
  //   delete fInputFile;
  // }
  if (fEvent != NULL) delete fEvent;
}

/*-----------------------*/
/*--- Open input file ---*/
/*-----------------------*/
template <typename armclass, typename armrec>
void EventViewer<armclass, armrec>::Open(TString fname) {
  fInputFile = new TFile(fname, "READ");
  if (!fInputFile->IsOpen()) {
    Utils::Printf(Utils::kPrintError, "Error: input file \"%s\" not opened\n", fname.Data());
    exit(EXIT_FAILURE);
  }
  fInputFile->GetObject("LHCfEvents", fTree);
  fEvent = new LHCfEvent("event", "LHCfEvent");
  fTree->SetBranchAddress("ev.", &fEvent);
  gROOT->cd();
}

/*-----------------------*/
/*--- Set canvas size ---*/
/*-----------------------*/
template <typename armclass, typename armrec>
void EventViewer<armclass, armrec>::SetCanvasSize(Int_t x, Int_t y) {
  fEvDisplay.SetCanvasSize(x, y);
};

/*-------------------*/
/*--- Go to event ---*/
/*-------------------*/
template <typename armclass, typename armrec>
void EventViewer<armclass, armrec>::GoTo(Int_t iev) {
  if (iev < 0 || iev >= fTree->GetEntries()) {
    Utils::Printf(Utils::kPrintError, "Error: cannot find event %d (last event: %d)\n", iev, fTree->GetEntries() - 1);
    return;
  }

  fTree->GetEntry(iev);

  if (fUseLevel == 0) {
    Level0<armclass> *lvl0 = (Level0<armclass> *)fEvent->Get(Form("lvl0_a%d", this->kArmIndex + 1));
    if (!lvl0) {
      Utils::Printf(Utils::kPrintError, "Error: cannot find Level0 object for Arm%d\n", this->kArmIndex + 1);
      return;
    }
    fEvDisplay.Fill(lvl0);
  } else if (fUseLevel == 1) {
    Level1<armclass> *lvl1 = (Level1<armclass> *)fEvent->Get(Form("lvl1_a%d", this->kArmIndex + 1));
    if (!lvl1) {
      Utils::Printf(Utils::kPrintError, "Error: cannot find Level1 object for Arm%d\n", this->kArmIndex + 1);
      return;
    }
    fEvDisplay.Fill(lvl1);
  } else if (fUseLevel == 2) {
    Level2<armclass> *lvl2 = (Level2<armclass> *)fEvent->Get(Form("lvl2_a%d", this->kArmIndex + 1));
    if (!lvl2) {
      Utils::Printf(Utils::kPrintError, "Error: cannot find Level2 object for Arm%d\n", this->kArmIndex + 1);
      return;
    }
    fEvDisplay.Fill(lvl2);
  } else {
    Utils::Printf(Utils::kPrintError, "Unknown level %d\n", fUseLevel);
    return;
  }

  fEvDisplay.Draw(fDrawOption);
  fEvDisplay.Update();
}

/*---------------------*/
/*--- Start process ---*/
/*---------------------*/
template <typename armclass, typename armrec>
void EventViewer<armclass, armrec>::Run() {
  // Int_t iev=0;
  while (1) {
    Utils::Printf(Utils::kPrintError, "Type next event to be viewer or a character for the next event (-1 to exit):\n");
    Int_t iev;
    std::cin >> iev;
    if (std::cin.fail()) {
      std::cin.clear();
      std::cin.ignore();
      ++fNextEvent;
    } else
      fNextEvent = iev;
    if (fNextEvent < 0) return;
    Utils::Printf(Utils::kPrintError, "Showing event %d\n", fNextEvent);
    GoTo(fNextEvent);
    // getchar();
    // ++iev;
  }
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventViewer<Arm1Params, Arm1RecPars>;
template class EventViewer<Arm2Params, Arm2RecPars>;
}  // namespace nLHCf
