#ifndef EventViewer_HH
#define EventViewer_HH

#include <TFile.h>
#include <TString.h>
#include <TTree.h>

#include "EventDisplay.hh"
#include "LHCfEvent.hh"

using namespace std;

namespace nLHCf {

template <typename armclass, typename armrec>
class EventViewer : public armclass {
 public:
  EventViewer();
  ~EventViewer();

 private:
  TFile *fInputFile;
  TTree *fTree;
  LHCfEvent *fEvent;
  TString fDrawOption;
  Int_t fUseLevel;
  Int_t fNextEvent;
  EventDisplay<armclass, armrec> fEvDisplay;

 public:
  void Open(TString fname);
  void SetLevel(Int_t level) { fUseLevel = level; };
  void SetCanvasSize(Int_t x, Int_t y);
  void SetDrawOption(TString foption) { fDrawOption = foption; };
  void Run();

 private:
  void GoTo(Int_t iev);
};
}  // namespace nLHCf
#endif
