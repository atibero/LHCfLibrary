#include <getopt.h>
#ifndef __LHCFROOTFUNCTIONS_CPP__
#define __LHCFROOTFUNCTIONS_CPP__

#include <iomanip>
#include <iostream>

using namespace std;

#include <LHCfrootFunctions.hh>
using namespace nLHCf;

/**
 * \defgroup Functions for LHCfroot. These functions work only in LHCfroot.
 */
/*@{*/

//////////////////////////////////////
/// Print help of LHCfroot
void LHCfroot_help() {
  cout << "------------------------------------------------------------------------------\n"
       << "LHCfroot is a modified ROOT interpreter with LHCf libraries. \n"
       << "\n"
       << "It works as usual root interpreter. \n"
       << "  ./LHCfroot  or  ./LHCfroot XXX.C  or  ./LHCfroot XXXX.root \n"
       << "You can use the LHCf classes without loading LHCf libraries.\n"
       << "Note) You must remember to set namsepace to nLHCf for these classes.\n"
       << "\n"
       << "Useful functions when you open a LHCf event tree file. \n"
       << "  void     LHCfEvents_Show(#event)         : Show info of contained object of the event. \n"
       << "  Long_t   LHCfEvents_GetEntries()         : Return the number of entries. \n"
       << "  TObject* LHCfEvents_Get(#event, obj_name): Return the pointer of \"obj_name\"\n"
       << "  Level2*  LHCfEvents_GetLevel0_A1(#event) : Return the pointer of \"lvl0_a1\" \n"
       << "  Level2*  LHCfEvents_GetLevel0_A2(#event) : Return the pointer of \"lvl0_a2\" \n"
       << "  Level2*  LHCfEvents_GetLevel1_A1(#event) : Return the pointer of \"lvl1_a1\" \n"
       << "  Level2*  LHCfEvents_GetLevel1_A2(#event) : Return the pointer of \"lvl1_a2\" \n"
       << "  Level2*  LHCfEvents_GetLevel2_A1(#event) : Return the pointer of \"lvl2_a1\" \n"
       << "  Level2*  LHCfEvents_GetLevel2_A2(#event) : Return the pointer of \"lvl2_a2\" \n"
       << "  Level3*  LHCfEvents_GetLevel3_A1(#event) : Return the pointer of \"lvl3_a1\" \n"
       << "  Level3*  LHCfEvents_GetLevel3_A2(#event) : Return the pointer of \"lvl3_a2\" \n"
       << "  McEvent* LHCfEvents_GetMcEvent_A1(#event) : Return the pointer of \"true_a1\"\n"
       << "  McEvent* LHCfEvents_GetMcEvent_A2(#event) : Return the pointer of \"true_a2\"\n"
       << "  void     LHCfEvents_GetLevel2_A1_Print(#event) : Call Level2::Print()\n"
       << "  void     LHCfEvents_GetLevel2_A2_Print(#event) : Call Level2::Print()\n"
       << "  void     LHCfEvents_GetMcEvent_A1_Print(#event, opt) : Call McEvent::Print() \n"
       << "  void     LHCfEvents_GetMcEvent_A2_Print(#event, opt) : Call McEvent::Print() \n"
       << "                  opt='lhc,detector,calorimeter,collision,arm1,2'\n"
       << "  void     LHCfEvents_GetLevel0_A1_EventDisplay(#event) : Draw the event \n"
       << "  void     LHCfEvents_GetLevel0_A2_EventDisplay(#event) : Draw the event \n"
       << "  void     LHCfEvents_GetLevel1_A1_EventDisplay(#event) : Draw the event \n"
       << "  void     LHCfEvents_GetLevel1_A2_EventDisplay(#event) : Draw the event \n"
       << "  void     LHCfEvents_GetLevel2_A1_EventDisplay(#event, option) : Draw the event, option=\"level3\" \n"
       << "  void     LHCfEvents_GetLevel2_A2_EventDisplay(#event, option) : Draw the event, option=\"level3\" \n"
       << "  SPSAdamo* LHCfEvents_GetAdamo(#event)      :  Return the pointer of Adamo event \n"
       << "  void      LHCfEvents_GetAdamo_EventDisplay(#event) : Draw the even\n"
       << "------------------------------------------------------------------------------\n"
       << endl;

  return;
}

//////////////////////////////////////
/// Print help of LHCfroot
void help() { LHCfroot_help(); }

TTree *gTree = NULL;
Int_t ievent = 0;

//////////////////////////////////////////////////////////
/// Internal function
TTree *SearchLHCfEvents() {
  if (gDirectory->FindObject("LHCfEvents")) {
    gTree = (TTree *)gDirectory->FindObject("LHCfEvents");
  } else if (gFile && gFile->Get("LHCfEvents")) {
    gTree = (TTree *)gFile->Get("LHCfEvents");
  } else {
    cerr << "ERROR: no LHCfEvents tree could be found in neither in gDirectory nor in gFile." << endl;
    gTree = NULL;
  }
  return gTree;
}

///////////////////////////////////////////////////////////
/// Print the contents of LHCfEvent (calling LHCfEvent::Show) with tree pointer and #Event in the tree.
/// \param tree [in]
/// \param iev  [in]
Int_t LHCfEvents_Show(TTree *tree, Int_t iev = ievent) {
  // LHCfEvents_Show(TTree *tree, Int_tiev)
  // Show the content list of LHCfEvent in EventID:iev of tree.
  if (tree) {
    tree->Draw("ev.Show()", "", "goff", 1, iev);
    ievent = iev + 1;
  } else {
    cerr << "Error: No LHCfEvents was found. " << endl;
  }
  return 0;
}

///////////////////////////////////////////////////////////
/// Print the contents of LHCfEvent (calling LHCfEvent::Show) with the file pointer
/// and the event number.
/// \param file [in] TTree file pointer
/// \param iev  [in] #event
/// \return if no LHCfEvents tree in the file, return -1.
Int_t LHCfEvents_Show(TFile *file, Int_t iev = ievent) {
  // LHCfEvents_Show(TFile *file, Int_t iev)
  // Show the content list of LHCfEvent in EventID:iev of file.

  if (file->Get("LHCfEvents")) {
    return LHCfEvents_Show((TTree *)file->Get("LHCfEvents"), iev);
  }
  return 0;
}

///////////////////////////////////////////////////////////
/// Print the contents of LHCfEvent (calling LHCfEvent::Show) with #event \n
/// This works only if LHCfroot is executed with an argument of the root file like \n
///  ./LHCfroot run10000_cal.root
/// \param file [in] TTree file pointer
/// \param iev  [in] #event
/// \return if no LHCfEvents tree in the file, return -1.
Int_t LHCfEvents_Show(Int_t iev = ievent) {
  if (SearchLHCfEvents())
    return LHCfEvents_Show(gTree, iev);
  else
    return 0;
}

///////////////////////////////////////////////////////////
/// Return the number of entries of LHCfEvents found in gDirectory or gFile.
/// \return the number of entries.
Long_t LHCfEvents_GetEntries() {
  if (SearchLHCfEvents())
    return gTree->GetEntries();
  else
    return 0;
}

// ====              LHCfEvents_Get              ====
// These classes are for getting the object from the LHCfEvents.
// The arguments are basically EventID:iev and the object name:objname.
// The classes with the specific object name are also available.

//////////////////////////////////////////////////////////////
/// Return the object specified with tree pointer, #event and the object name. \n
/// This is a base class for the following functions.
/// \param tree [in] Tree pointer of LHCfEvents
/// \param iev [in] #Event
/// \param objname [in]  The object name specified as "lvl2_a1", "lvl2_a2"
/// \return the pointer of the found object;
TObject *LHCfEvents_Get(TTree *tree, const Int_t iev, const TString objname) {
  static LHCfEvent *ev = new LHCfEvent();
  static Int_t iev_previous = -10000;
  static TTree *tree_previous = NULL;
  TObject *obj = NULL;

  if (tree_previous != tree) {
    tree->ResetBranchAddresses();
    tree->SetBranchAddress("ev.", &ev);
  }

  if (iev != iev_previous) {
    ev->Delete();
    tree->GetEntry(iev);
    iev_previous = iev;
  }

  if (ev->Check((char *)objname.Data())) {
    obj = ev->Get((char *)objname.Data());
    ievent = iev + 1;
  } else {
    std::cerr << "No object(name:" << objname << ")" << std::endl;
  }

  return obj;
}

///////////////////////////////////////////////////
/// Return the object specified with tree pointer, #event and the object name. \n
/// \param iev [in] #event
/// \param objname [in] The object name
/// \return the pointer of the found object;
TObject *LHCfEvents_Get(const Int_t iev, const TString objname) {
  if (SearchLHCfEvents())
    return LHCfEvents_Get(gTree, iev, objname);
  else
    return NULL;
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level0 of Arm1 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level0<Arm1Params> found in the event. \n
/// if no, return -1;
Level0<Arm1Params> *LHCfEvents_GetLevel0_A1(const Int_t iev = ievent) {
  return (Level0<Arm1Params> *)LHCfEvents_Get(iev, "lvl0_a1");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level0 of Arm2 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level2<Arm2Params> found in the event. \n
/// if no, return -1;
Level0<Arm2Params> *LHCfEvents_GetLevel0_A2(const Int_t iev = ievent) {
  return (Level0<Arm2Params> *)LHCfEvents_Get(iev, "lvl0_a2");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level1 of Arm1 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level1<Arm1Params> found in the event. \n
/// if no, return -1;
Level1<Arm1Params> *LHCfEvents_GetLevel1_A1(const Int_t iev = ievent) {
  return (Level1<Arm1Params> *)LHCfEvents_Get(iev, "lvl1_a1");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level1 of Arm2 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level2<Arm2Params> found in the event. \n
/// if no, return -1;
Level1<Arm2Params> *LHCfEvents_GetLevel1_A2(const Int_t iev = ievent) {
  return (Level1<Arm2Params> *)LHCfEvents_Get(iev, "lvl1_a2");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level2 of Arm1 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level2<Arm1Params> found in the event. \n
/// if no, return -1;
Level2<Arm1Params> *LHCfEvents_GetLevel2_A1(const Int_t iev = ievent) {
  return (Level2<Arm1Params> *)LHCfEvents_Get(iev, "lvl2_a1");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level2 of Arm2 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level2<Arm2Params> found in the event. \n
/// if no, return -1;
Level2<Arm2Params> *LHCfEvents_GetLevel2_A2(const Int_t iev = ievent) {
  return (Level2<Arm2Params> *)LHCfEvents_Get(iev, "lvl2_a2");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level3 of Arm1 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level3<Arm1Params> found in the event. \n
/// if no, return -1;
Level3<Arm1RecPars> *LHCfEvents_GetLevel3_A1(const Int_t iev = ievent) {
  return (Level3<Arm1RecPars> *)LHCfEvents_Get(iev, "lvl3_a1");
}

//////////////////////////////////////////////////////////
/// Get the pointer of Level3 of Arm2 in the specified event.
/// \param iev [in] #event
/// \return the object point of Level3<Arm2Params> found in the event. \n
/// if no, return -1;
Level3<Arm2RecPars> *LHCfEvents_GetLevel3_A2(const Int_t iev = ievent) {
  return (Level3<Arm2RecPars> *)LHCfEvents_Get(iev, "lvl3_a2");
}

//////////////////////////////////////////////////////////
/// Get the pointer of McEvent of Arm1 in the specified event.
/// \param iev [in] #event
/// \return the object point of McEvent found in the event. \n
/// if no, return -1;
McEvent *LHCfEvents_GetMcEvent_A1(const Int_t iev = ievent) { return (McEvent *)LHCfEvents_Get(iev, "true_a1"); }

//////////////////////////////////////////////////////////
/// Get the pointer of McEvent of Arm2 in the specified event.
/// \param iev [in] #event
/// \return the object point of McEvent found in the event. \n
/// if no, return -1;
McEvent *LHCfEvents_GetMcEvent_A2(const Int_t iev = ievent) { return (McEvent *)LHCfEvents_Get(iev, "true_a2"); }

//////////////////////////////////////////////////////////
/// Call Level2::Print of the specified event.
/// \param iev [in] #event
/// \return the object point of Level2<Arm1Params> found in the event. \n
/// if no, return -1;
void LHCfEvents_GetLevel2_A1_Print(const Int_t iev = ievent) {
  Level2<Arm1Params> *obj = LHCfEvents_GetLevel2_A1(iev);
  if (obj) {
    obj->Print();
  }
  return;
}

//////////////////////////////////////////////////////////
/// Call Level2::Print of the specified event.
/// \param iev [in] #event
/// \return the object point of Level2<Arm2Params> found in the event. \n
/// if no, return -1;
void LHCfEvents_GetLevel2_A2_Print(const Int_t iev = ievent) {
  Level2<Arm2Params> *obj = LHCfEvents_GetLevel2_A2(iev);
  if (obj) {
    obj->Print();
  }
  return;
}

//////////////////////////////////////////////////////////
/// Call Level3::Print of the specified event.
/// \param iev [in] #event
/// \return the object point of Level3<Arm1Params> found in the event. \n
/// if no, return -1;
void LHCfEvents_GetLevel3_A1_Print(const Int_t iev = ievent) {
  Level3<Arm1RecPars> *obj = LHCfEvents_GetLevel3_A1(iev);
  if (obj) {
    obj->Print();
  }
  return;
}

//////////////////////////////////////////////////////////
/// Call Level3::Print of the specified event.
/// \param iev [in] #event
/// \return the object point of Level3<Arm2Params> found in the event. \n
/// if no, return -1;
void LHCfEvents_GetLevel3_A2_Print(const Int_t iev = ievent) {
  Level3<Arm2RecPars> *obj = LHCfEvents_GetLevel3_A2(iev);
  if (obj) {
    obj->Print();
  }
  return;
}

//////////////////////////////////////////////////////////
/// Call McEvent::Print of the specified event.
/// \param iev [in] #event
/// \return the object point of McEvent found in the event. \n
/// if no, return -1;
void LHCfEvents_GetMcEvent_A1_Print(const Int_t iev = ievent, const char *opt) {
  McEvent *obj = LHCfEvents_GetMcEvent_A1(iev);
  if (obj) {
    obj->Print(opt);
  }
  return;
}

//////////////////////////////////////////////////////////
/// Call McEvent::Print of the specified event.
/// \param iev [in] #event
/// \return the object point of McEvent found in the event. \n
/// if no, return -1;
void LHCfEvents_GetMcEvent_A2_Print(const Int_t iev = ievent, const char *opt) {
  McEvent *obj = LHCfEvents_GetMcEvent_A2(iev);
  if (obj) {
    obj->Print(opt);
  }
  return;
}

//////////////////////////////////////////////////////////
/// Event Display for Arm1 Level0
/// \param iev [in] #event
/// \return the object point of EventDisplay<Arm1Params>
/// if no, return -1;
EventDisplay<Arm1Params, Arm1RecPars> *LHCfEvents_GetLevel0_A1_EventDisplay(const Int_t iev = ievent) {
  static EventDisplay<Arm1Params, Arm1RecPars> *eventDisplay = NULL;

  Level0<Arm1Params> *obj = LHCfEvents_GetLevel0_A1(iev);
  if (obj == NULL) return NULL;

  if (eventDisplay == NULL) {
    eventDisplay = new EventDisplay<Arm1Params, Arm1RecPars>("ev_lvl0_a1", "Arm1 Level0 Event Display");
    eventDisplay->Fill(obj);
    eventDisplay->Draw();
  } else {
    eventDisplay->Fill(obj);
    eventDisplay->Update();
  }
  return eventDisplay;
}

//////////////////////////////////////////////////////////
/// Event Display for Arm2 Level0
/// \param iev [in] #event
/// \return the object point of EventDisplay<Arm2Params>
/// if no, return -1;
EventDisplay<Arm2Params, Arm2RecPars> *LHCfEvents_GetLevel0_A2_EventDisplay(const Int_t iev = ievent) {
  static EventDisplay<Arm2Params, Arm2RecPars> *eventDisplay = NULL;

  Level0<Arm2Params> *obj = LHCfEvents_GetLevel0_A2(iev);
  if (obj == NULL) return NULL;

  if (eventDisplay == NULL) {
    eventDisplay = new EventDisplay<Arm2Params, Arm2RecPars>("ev_lvl0_a2", "Arm2 Level0 Event Display");
    eventDisplay->Fill(obj);
    eventDisplay->Draw();
  } else {
    eventDisplay->Fill(obj);
    eventDisplay->Update();
  }
  return eventDisplay;
}

//////////////////////////////////////////////////////////
/// Event Display for Arm1 Level1
/// \param iev [in] #event
/// \return the object point of EventDisplay<Arm1Params>
/// if no, return -1;
EventDisplay<Arm1Params, Arm1RecPars> *LHCfEvents_GetLevel1_A1_EventDisplay(const Int_t iev = ievent) {
  static EventDisplay<Arm1Params, Arm1RecPars> *eventDisplay = NULL;

  Level1<Arm1Params> *obj = LHCfEvents_GetLevel1_A1(iev);
  if (obj == NULL) return NULL;

  if (eventDisplay == NULL) {
    eventDisplay = new EventDisplay<Arm1Params, Arm1RecPars>("ev_lvl1_a1", "Arm1 Level1 Event Display");
    eventDisplay->Fill(obj);
    eventDisplay->Draw();
  } else {
    eventDisplay->Fill(obj);
    eventDisplay->Update();
  }
  return eventDisplay;
}

//////////////////////////////////////////////////////////
/// Event Display for Arm2 Level1
/// \param iev [in] #event
/// \return the object point of EventDisplay<Arm2Params>
/// if no, return -1;
EventDisplay<Arm2Params, Arm2RecPars> *LHCfEvents_GetLevel1_A2_EventDisplay(const Int_t iev = ievent) {
  static EventDisplay<Arm2Params, Arm2RecPars> *eventDisplay = NULL;

  Level1<Arm2Params> *obj = LHCfEvents_GetLevel1_A2(iev);
  if (obj == NULL) return NULL;

  if (eventDisplay == NULL) {
    eventDisplay = new EventDisplay<Arm2Params, Arm2RecPars>("ev_lvl1_a2", "Arm2 Level1 Event Display");
    eventDisplay->Fill(obj);
    eventDisplay->Draw();
  } else {
    eventDisplay->Fill(obj);
    eventDisplay->Update();
  }
  return eventDisplay;
}

//////////////////////////////////////////////////////////
/// Event Display for Arm1 Level2
/// \param iev [in] #event
/// \return the object point of EventDisplay<Arm1Params>
/// if no, return -1;

EventDisplay<Arm1Params, Arm1RecPars> *LHCfEvents_GetLevel2_A1_EventDisplay(const Int_t iev) {
  return LHCfEvents_GetLevel2_A1_EventDisplay(iev, "");
}

EventDisplay<Arm1Params, Arm1RecPars> *LHCfEvents_GetLevel2_A1_EventDisplay(const Int_t iev, Option_t *option) {
  static EventDisplay<Arm1Params, Arm1RecPars> *eventDisplay = NULL;

  // Check the option
  Bool_t flag_lvl3 = false;
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("level3") || opt.Contains("lvl3")) flag_lvl3 = true;

  Level2<Arm1Params> *obj = LHCfEvents_GetLevel2_A1(iev);
  if (obj == NULL) return NULL;

  Level3<Arm1RecPars> *lvl3 = NULL;
  if (flag_lvl3) lvl3 = LHCfEvents_GetLevel3_A1(iev);

  if (eventDisplay == NULL) {
    eventDisplay = new EventDisplay<Arm1Params, Arm1RecPars>("ev_lvl2_a1", "Arm1 Level2 Event Display");
    eventDisplay->Fill(obj);

    if (flag_lvl3 && lvl3) {
      eventDisplay->Fill(lvl3);
      eventDisplay->Draw("canvas1 canvas2 level3");
    } else {
      eventDisplay->Draw("canvas1 canvas2");
    }
  } else {
    eventDisplay->Fill(obj);
    if (flag_lvl3 && lvl3) {
      eventDisplay->Fill(lvl3);
      eventDisplay->Update("level3");
    } else {
      eventDisplay->Update();
    }
  }
  return eventDisplay;
}

//////////////////////////////////////////////////////////
/// Event Display for Arm2 Level2
/// \param iev [in] #event
/// \return the object point of EventDisplay<Arm2Params>
/// if no, return -1;
EventDisplay<Arm2Params, Arm2RecPars> *LHCfEvents_GetLevel2_A2_EventDisplay(const Int_t iev) {
  return LHCfEvents_GetLevel2_A2_EventDisplay(iev, "");
}

EventDisplay<Arm2Params, Arm2RecPars> *LHCfEvents_GetLevel2_A2_EventDisplay(const Int_t iev, Option_t *option) {
  static EventDisplay<Arm2Params, Arm2RecPars> *eventDisplay = NULL;

  // Check the option
  Bool_t flag_lvl3 = false;
  TString opt = option;
  opt.ToLower();
  if (opt.Contains("level3") || opt.Contains("lvl3")) flag_lvl3 = true;

  Level2<Arm2Params> *obj = LHCfEvents_GetLevel2_A2(iev);
  if (obj == NULL) return NULL;

  Level3<Arm2RecPars> *lvl3 = NULL;
  if (flag_lvl3) lvl3 = LHCfEvents_GetLevel3_A2(iev);

  if (eventDisplay == NULL) {
    eventDisplay = new EventDisplay<Arm2Params, Arm2RecPars>("ev_lvl2_a2", "Arm2 Level2 Event Display");
    eventDisplay->Fill(obj);

    if (flag_lvl3 && lvl3) {
      eventDisplay->Fill(lvl3);
      eventDisplay->Draw("canvas1 canvas2 level3");
    } else {
      eventDisplay->Draw("canvas1 canvas2");
    }
  } else {
    eventDisplay->Fill(obj);
    if (flag_lvl3 && lvl3) {
      eventDisplay->Fill(lvl3);
      eventDisplay->Update("level3");
    } else {
      eventDisplay->Update();
    }
  }

  return eventDisplay;
}

////////////////////////////////////////////////////
/// Get ADAMO data (only for SPS beam test)
/// \param iev
/// \return pinter of adamo data
SPSAdamo *LHCfEvents_GetAdamo(const Int_t iev) { return (SPSAdamo *)LHCfEvents_Get(iev, "adamo"); }

///////////////////////////////////////////////////
/// Event display of Adamo
/// \param iev
/// \return
SPSAdamoHist *LHCfEvents_GetAdamo_EventDisplay(const Int_t iev) {
  static SPSAdamoHist *eventDisplay = NULL;

  SPSAdamo *obj = LHCfEvents_GetAdamo(iev);
  if (obj == NULL) return NULL;

  if (eventDisplay == NULL) {
    eventDisplay = new SPSAdamoHist();
    eventDisplay->Initialize();
    eventDisplay->DrawCanvas();
  }
  eventDisplay->Reset();
  eventDisplay->Fill(obj);
  eventDisplay->UpdateCanvas();

  return eventDisplay;
}

/*@}*/  // end of doxygen group.

#endif
