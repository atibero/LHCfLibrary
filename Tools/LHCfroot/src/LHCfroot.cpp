#include <iomanip>
#include <iostream>
#include <string>

using namespace std;

#include <TInterpreter.h>
#include <TROOT.h>
#include <TRint.h>
#include <TStyle.h>
#include <version.h>

#include <LHCfrootFunctions.hh>

using namespace nLHCf;

int main(int argc, char **argv) {
  for (int i = 1; i < argc; ++i) {
    string ss = argv[i];
    if (ss == "-h" || ss == "--help") {
      LHCfroot_help();
    }
  }

  TRint theApp("LHCfroot", &argc, argv, 0, 0, kTRUE);

  // Include Path ----------------------------------------------------
  cout << "PROJECT SOURCE PATH = " << PROJECT_SOURCE_PATH << endl;
  cout << "PROJECT BINARY PATH = " << PROJECT_BINARY_PATH << endl;
  gInterpreter->AddIncludePath(Form("%s/include", PROJECT_SOURCE_PATH));
  gInterpreter->AddIncludePath(Form("%s/Dictionary/include", PROJECT_SOURCE_PATH));
  gInterpreter->AddIncludePath(Form("%s/Reconstruction/include", PROJECT_SOURCE_PATH));
  gInterpreter->AddIncludePath(Form("%s/Analysis/include", PROJECT_SOURCE_PATH));
  gInterpreter->AddIncludePath(Form("%s/Tools/nLHCfroot/include", PROJECT_SOURCE_PATH));
  gInterpreter->AddIncludePath(Form("%s/include", PROJECT_BINARY_PATH));

  // Generic Configuration of Art works ------------------------------

  help();

  // Start ROOT Interpreter ------------------------------------------
  theApp.Run();
}
