#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ function LHCfroot_help();
#pragma link C++ function help();
#pragma link C++ function LHCfEvents_Show(TTree*, int);
#pragma link C++ function LHCfEvents_Show(TFile*, int);
#pragma link C++ function LHCfEvents_GetEntries();
#pragma link C++ function LHCfEvents_Show(int);
#pragma link C++ function LHCfEvents_Get(TTree*, const int, const TString);
#pragma link C++ function LHCfEvents_Get(const int, const TString);
#pragma link C++ function LHCfEvents_GetLevel0_A1(const int);
#pragma link C++ function LHCfEvents_GetLevel0_A2(const int);
#pragma link C++ function LHCfEvents_GetLevel1_A1(const int);
#pragma link C++ function LHCfEvents_GetLevel1_A2(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A1(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A2(const int);
#pragma link C++ function LHCfEvents_GetLevel3_A1(const int);
#pragma link C++ function LHCfEvents_GetLevel3_A2(const int);
#pragma link C++ function LHCfEvents_GetMcEvent_A1(const int);
#pragma link C++ function LHCfEvents_GetMcEvent_A2(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A1_Print(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A2_Print(const int);
#pragma link C++ function LHCfEvents_GetLevel3_A1_Print(const int);
#pragma link C++ function LHCfEvents_GetLevel3_A2_Print(const int);
#pragma link C++ function LHCfEvents_GetMcEvent_A1_Print(const int, const char*);
#pragma link C++ function LHCfEvents_GetMcEvent_A2_Print(const int, const char*);
#pragma link C++ function LHCfEvents_GetLevel0_A1_EventDisplay(const int);
#pragma link C++ function LHCfEvents_GetLevel0_A2_EventDisplay(const int);
#pragma link C++ function LHCfEvents_GetLevel1_A1_EventDisplay(const int);
#pragma link C++ function LHCfEvents_GetLevel1_A2_EventDisplay(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A1_EventDisplay(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A2_EventDisplay(const int);
#pragma link C++ function LHCfEvents_GetLevel2_A1_EventDisplay(const int, Option_t*);
#pragma link C++ function LHCfEvents_GetLevel2_A2_EventDisplay(const int, Option_t*);
#pragma link C++ function LHCfEvents_GetAdamo(const int);
#pragma link C++ function LHCfEvents_GetAdamo_EventDisplay(const int);

#endif
