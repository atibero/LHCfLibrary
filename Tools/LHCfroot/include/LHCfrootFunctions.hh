#ifndef __LHCFROOTFUNCTIONS_H__
#define __LHCFROOTFUNCTIONS_H__

#include <TDirectory.h>
#include <TFile.h>
#include <TTree.h>

#include <Arm1RecPars.hh>
#include <Arm2RecPars.hh>
#include <EventDisplay.hh>
#include <LHCfEvent.hh>
#include <Level2.hh>
#include <Level3.hh>
#include <McEvent.hh>
#include <SPSAdamo.hh>
#include <SPSAdamoHist.hh>

void LHCfroot_help();
void help();

Int_t LHCfEvents_Show(TTree *tree, Int_t iev);  // Show the content list in EventID:iev of tree.
Int_t LHCfEvents_Show(TFile *file, Int_t iev);  // Show the content list in EventID:iev of file.
Int_t LHCfEvents_Show(int iev);                 // Show the contents list in EventID::iev of the opened tree.
Long_t LHCfEvents_GetEntries();                 // Return the get entries;

TObject *LHCfEvents_Get(
    TTree *tree, const Int_t iev,
    const TString objname);  // Get the object named as objname from EventID::iev of the LHCfEvents Tree:: tree.
TObject *LHCfEvents_Get(
    const Int_t iev,
    const TString dataname);  // Get the object named as objname from EventID::iev of the opened tree.
nLHCf::Level0<nLHCf::Arm1Params> *LHCfEvents_GetLevel0_A1(const Int_t iev);
nLHCf::Level0<nLHCf::Arm2Params> *LHCfEvents_GetLevel0_A2(const Int_t iev);
nLHCf::Level1<nLHCf::Arm1Params> *LHCfEvents_GetLevel1_A1(const Int_t iev);
nLHCf::Level1<nLHCf::Arm2Params> *LHCfEvents_GetLevel1_A2(const Int_t iev);
nLHCf::Level2<nLHCf::Arm1Params> *LHCfEvents_GetLevel2_A1(const Int_t iev);
nLHCf::Level2<nLHCf::Arm2Params> *LHCfEvents_GetLevel2_A2(const Int_t iev);
nLHCf::Level3<nLHCf::Arm1RecPars> *LHCfEvents_GetLevel3_A1(const Int_t iev);
nLHCf::Level3<nLHCf::Arm2RecPars> *LHCfEvents_GetLevel3_A2(const Int_t iev);
nLHCf::McEvent *LHCfEvents_GetMcEvent_A1(const Int_t iev);
nLHCf::McEvent *LHCfEvents_GetMcEvent_A2(const Int_t iev);
void LHCfEvents_GetLevel2_A1_Print(const Int_t iev);
void LHCfEvents_GetLevel2_A2_Print(const Int_t iev);
void LHCfEvents_GetLevel3_A1_Print(const Int_t iev);
void LHCfEvents_GetLevel3_A2_Print(const Int_t iev);
void LHCfEvents_GetMcEvent_A1_Print(const Int_t iev, const char* option="lhc");
void LHCfEvents_GetMcEvent_A2_Print(const Int_t iev, const char* option="lhc");

nLHCf::EventDisplay<nLHCf::Arm1Params, nLHCf::Arm1RecPars> *LHCfEvents_GetLevel0_A1_EventDisplay(const Int_t iev);
nLHCf::EventDisplay<nLHCf::Arm2Params, nLHCf::Arm2RecPars> *LHCfEvents_GetLevel0_A2_EventDisplay(const Int_t iev);
nLHCf::EventDisplay<nLHCf::Arm1Params, nLHCf::Arm1RecPars> *LHCfEvents_GetLevel1_A1_EventDisplay(const Int_t iev);
nLHCf::EventDisplay<nLHCf::Arm2Params, nLHCf::Arm2RecPars> *LHCfEvents_GetLevel1_A2_EventDisplay(const Int_t iev);
nLHCf::EventDisplay<nLHCf::Arm1Params, nLHCf::Arm1RecPars> *LHCfEvents_GetLevel2_A1_EventDisplay(const Int_t iev);
nLHCf::EventDisplay<nLHCf::Arm2Params, nLHCf::Arm2RecPars> *LHCfEvents_GetLevel2_A2_EventDisplay(const Int_t iev);
nLHCf::EventDisplay<nLHCf::Arm1Params, nLHCf::Arm1RecPars> *LHCfEvents_GetLevel2_A1_EventDisplay(const Int_t iev, Option_t *option);
nLHCf::EventDisplay<nLHCf::Arm2Params, nLHCf::Arm2RecPars> *LHCfEvents_GetLevel2_A2_EventDisplay(const Int_t iev, Option_t *option);

// For SPS beam tests
nLHCf::SPSAdamo *LHCfEvents_GetAdamo(const Int_t iev);
nLHCf::SPSAdamoHist *LHCfEvents_GetAdamo_EventDisplay(const Int_t iev);
#endif
