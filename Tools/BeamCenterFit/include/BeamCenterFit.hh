#ifndef BeamCenterFit_HH
#define BeamCenterFit_HH

#include <Minuit2/FCNBase.h>
#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/Minuit2Minimizer.h>
#include <Minuit2/MnMigrad.h>
#include <Minuit2/MnMinos.h>
#include <Minuit2/MnPlot.h>
#include <Minuit2/MnPrint.h>
#include <Minuit2/MnScan.h>
#include <Minuit2/MnUserParameters.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TF1.h>
#include <TF2.h>
#include <TFile.h>
#include <TFitResult.h>
#include <TGaxis.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLine.h>
#include <TMarker.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TPaletteAxis.h>
#include <TPaveStats.h>
#include <TPostScript.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>
#include <TVirtualX.h>
#include <dirent.h>

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <map>

#include "Arm1AnPars.hh"
#include "Arm1Params.hh"
#include "Arm2AnPars.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"

using namespace std;

namespace ROOT {
namespace Minuit2 {
class CustomFCN : public FCNBase {
 public:
  CustomFCN(const std::vector<double> &posx, const std::vector<double> &posy, const std::vector<double> &height,
            int *npfits, const double xEdgeMin, const double xEdgeMax, const double yEdgeMin, const double yEdgeMax)
      : fPosx(posx),
        fPosy(posy),
        fHeight(height),
        np(npfits),
        xmin(xEdgeMin),
        xmax(xEdgeMax),
        ymin(yEdgeMin),
        ymax(yEdgeMax) {}

  virtual ~CustomFCN() {}
  //--- simple function ---
  virtual double operator()(const std::vector<double> &par) const {
    double fval = 0.;
    for (unsigned int i = 0; i < fPosx.size(); i++) {
      double posX = fPosx[i];
      double posY = fPosy[i];
      double meas = fHeight[i];

      double A = par[0];
      double B = par[1];
      double x0 = par[2];
      double y0 = par[3];

      if (i == 0) *np = 0;

      if (posX < xmin || posX >= xmax) continue;
      if (posY < ymin || posY >= ymax) continue;

      double func = A * exp(-B * sqrt(pow(posX - x0, 2.) + pow(posY - y0, 2.)));

      /* Gauss chi2 */
      double error = meas;  // +1 ?
      if (meas > 0.) {
        fval += pow(func - meas, 2.) / error;

        ++(*np);
      }
    }
    return fval;
  }
  virtual double Up() const { return 1.; }

 private:
  std::vector<double> fPosx;
  std::vector<double> fPosy;
  std::vector<double> fHeight;
  int *np;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
};
}  // namespace Minuit2
}  // namespace ROOT

namespace nLHCf {

template <typename arman, typename armclass>
class BeamCenterFit : public arman, public LHCfParams {
 public:
  BeamCenterFit();
  ~BeamCenterFit();

 private:
  /*--- Input/Output ---*/
  TString fInputFileName;
  TString fOutputDirName;
  Int_t fFirstRun;
  Int_t fLastRun;
  Int_t fRebin;
  Bool_t fCumulative;
  Bool_t fRunByRun;
  Bool_t fProjection;
  Bool_t fVertexXY;
  Bool_t fFitByLayer;
  Bool_t fOptimize;
  Bool_t fFixLimit;
  Bool_t f2DFrom1D;
  Bool_t fGaussian;
  Bool_t fDrawProj;
  Bool_t fMinuitXY;
  Bool_t fOfficial;

  Int_t fNRun;
  Int_t fNEne;
  TFile *fOutputFile;
  ofstream fOfstream;

  /*--- Define tower edge ---*/
  Double_t xTowerInfEdge;
  Double_t yTowerInfEdge;
  Double_t xTowerSupEdge;
  Double_t yTowerSupEdge;

  /*--- Best 1D parameter ---*/
  Double_t fFmin_1D;
  Double_t fFmax_1D;
  Double_t fFPar_1D[3];
  Double_t fNdof_1D;
  Double_t fChi2_1D;

  /*--- Best 2D parameter ---*/
  Double_t fFmin_2D[2];
  Double_t fFmax_2D[2];
  Double_t fFPar_2D[5];
  Double_t fNdof_2D;
  Double_t fChi2_2D;

  /*--- Energy threshold ---*/
  const vector<Double_t> fEnergyThr = {1000., 4000., 7000.};

  /*--- Energy threshold use as nominal value ---*/
  const Double_t fNominalThr = 4000.;

  /*--- Hitmap Histograms ---*/
  vector<vector<vector<TH1D *>>> fVertexZaverage;
  vector<vector<vector<vector<TH1D *>>>> fVertexHitmap1D;
  vector<vector<vector<vector<TH2D *>>>> fVertexHitmap2D;

  vector<vector<TH1D *>> fZaverage_Cumulative;
  vector<vector<vector<TH1D *>>> fZaverage_RunByRun;

  vector<TH2D *> fHisto2D_Cumulative;
  vector<TF2> fFit2D_Cumulative;
  vector<vector<TGraph>> fFitPro2D_Cumulative;
  vector<vector<TH1D>> fHistoPro2D_Cumulative;

  vector<vector<TH2D *>> fHisto2D_RunByRun;
  vector<vector<TF2>> fFit2D_RunByRun;
  vector<vector<vector<TGraph>>> fFitPro2D_RunByRun;
  vector<vector<vector<TH1D>>> fHistoPro2D_RunByRun;

  vector<vector<TH1D *>> fHisto1D_Cumulative;
  vector<vector<TF1>> fFit1D_Cumulative;

  vector<vector<vector<TH1D *>>> fHisto1D_RunByRun;
  vector<vector<vector<TF1>>> fFit1D_RunByRun;

  vector<vector<vector<TH1D *>>> fHisto1D_ByLayer;
  vector<vector<vector<TF1>>> fFit1D_ByLayer;

  vector<vector<TLine *>> fLine2DFit_OverAllRuns;
  vector<vector<TLine *>> fLine2DInf_OverAllRuns;
  vector<vector<TLine *>> fLine2DSup_OverAllRuns;

  vector<vector<TLine *>> fLine1DFit_OverAllRuns;
  vector<vector<TLine *>> fLine1DInf_OverAllRuns;
  vector<vector<TLine *>> fLine1DSup_OverAllRuns;

  vector<TGraphErrors *> fGraph2D_Cumulative;
  vector<TGraphErrors *> fGraph1D_Cumulative;

  vector<vector<TGraphErrors *>> fGraph2D_RunByRun;
  vector<vector<TGraphErrors *>> fGraph1D_RunByRun;

  vector<vector<TGraphErrors *>> fGraph1D_ByLayer;

  vector<vector<TMultiGraph *>> fSummary_RunByRun;
  vector<vector<TLegend *>> fLegend_RunByRun;

  vector<TMultiGraph *> fSummary_Cumulative;
  vector<TLegend *> fLegend_Cumulative;

  vector<TMultiGraph *> fSummary_ByLayer;
  vector<TLegend *> fLegend_ByLayer;

  vector<vector<TH1D *>> fHisto2D_Dispersion;
  vector<vector<TH1D *>> fHisto1D_Dispersion;

 public:
  void SetInputFile(const Char_t *name) { fInputFileName = name; }
  void SetOutputDir(const Char_t *name) { fOutputDirName = name; }
  void SetFitOption(Bool_t cumulative, Bool_t runbyrun, Bool_t projection, Bool_t xyvertex, Bool_t fitbylayer,
                    Bool_t optimize, Bool_t fixlimit, Bool_t d2fromd1, Bool_t gaussian, Bool_t xyminuit,
                    Bool_t drawproj, Bool_t official) {
    fCumulative = cumulative;
    fRunByRun = runbyrun;
    fProjection = projection;
    fVertexXY = xyvertex;
    fFitByLayer = fitbylayer;
    fOptimize = optimize;
    fFixLimit = fixlimit;
    f2DFrom1D = d2fromd1;
    fGaussian = gaussian;
    fMinuitXY = xyminuit;
    fDrawProj = drawproj;
    fOfficial = official;
  }
  void SetRunRange(Int_t first_run, Int_t last_run) {
    fFirstRun = first_run;
    fLastRun = last_run;
  }
  void SetBinFactor(Int_t rebin) { fRebin = rebin; }
  void Run();

 private:
  void GetInputHistograms();
  void SetHistoToBeFitted();
  void SetTowerRange();
  void SetUpPalette();
  void SetUpCanvasStyle();
  void ClearBestPars2D();
  void CopyToBestFit2D(TF2 f2, Double_t minEdge[2], Double_t maxEdge[2]);
  void InitializeFit2D(TF2 &f2, Double_t minEdge[2], Double_t maxEdge[2], Double_t maxPeak[2], Double_t ampPeak);
  void OptimizeRange2DFast(TH2D *h2, Double_t minEdge[2], Double_t maxEdge[2], Double_t maxPeak[2], Double_t ampPeak);
  void OptimizeRange2DFull(TH2D *h2, Double_t minEdge[2], Double_t maxEdge[2], Double_t maxPeak[2], Double_t ampPeak);
  void OptimizeRange2D(TH2D *h2, Double_t minEdge[2], Double_t maxEdge[2], Double_t maxPeak[2], Double_t ampPeak) {
    return OptimizeRange2DFast(h2, minEdge, maxEdge, maxPeak, ampPeak);
  }
  void SetUpFinalFit2D(TF2 &f2);
  void Fit2DVertex(TH2D *h2, TF2 &f2);
  void ClearBestPars1D();
  void CopyToBestFit1D(TF1 f1, Double_t minEdge, Double_t maxEdge);
  void InitializeFit1D(TF1 &f1, Int_t view, Double_t minEdge, Double_t maxEdge, Double_t maxPeak, Double_t ampPeak);
  void OptimizeRange1D(TH1D *h1, Int_t view, Double_t minEdge, Double_t maxEdge, Double_t maxPeak, Double_t ampPeak);
  void SetUpFinalFit1D(TF1 &f1, Int_t view);
  void Fit1DVertex(TH1D *h1, TF1 &f1, Int_t view);
  void Draw2DFit(TH2D *h2, TF2 &f2);
  void Draw2DFitProjection(TH2D *h2, TF2 &f2, TH1D &hp, TGraph &gr, Int_t view);
  void Draw1DFit(TH1D *h1, TF1 &f1, Int_t view);
  void CreatePaveFit(TCanvas *c2, TH2D *h2, TF2 &f2);
  void CreatePavePro(TCanvas *c2, TF2 &f2, TH1D &hp, TGraph &gr, Int_t view);
  void FitCumulative2D();
  void FitRunbyRun2D();
  void FitCumulative1D();
  void FitRunByRun1D();
  void FitByLayer();
  void ProjectCumulative2D();
  void ProjectRunbyRun2D();
  void Create2DOverEnergy();
  void Create1DOverEnergy();
  void DrawCumulativeSummary();
  void Create2DOverAllRuns();
  void Create1DOverAllRuns();
  void Create2DRunByRunTrend();
  void Create1DRunByRunTrend();
  void DrawRunByRunSummary();
  void CreateComparisonByLayer();
  void DrawFitByLayer();
  void Create2DDispersion();
  void DrawDispersionSummary2D();
  void Create1DDispersion();
  void DrawDispersionSummary1D();
  void PrintSummary();
  void WriteToOutput();
  void SaveCumulative2D();
  void SaveRunbyRun2D();
  void SaveCumulative1D();
  void SaveRunByRun1D();
  void OpenFiles();
  void CloseFiles();
};
}  // namespace nLHCf
#endif
