#include "BeamCenterFit.hh"

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*-----------------------*/
/*--- 2D Fit Function ---*/
/*-----------------------*/
Double_t exp2d1(Double_t *x, Double_t *par) {
  Double_t A = par[0];  // peak
  Double_t B = par[1];
  Double_t x0 = par[2];  // x center
  Double_t y0 = par[3];  // y center
  Double_t r = sqrt(pow(x[0] - x0, 2.) + pow(x[1] - y0, 2.));

  return A * exp(-B * r);
}

Double_t lor2d1(Double_t *x, Double_t *par) {
  Double_t A = par[0];  // peak
  Double_t B = par[1];
  Double_t x0 = par[2];  // x center
  Double_t y0 = par[3];  // y center
  Double_t r = sqrt(pow(x[0] - x0, 2.) + pow(x[1] - y0, 2.));

  return A / (B * TMath::Pi() * (1 + TMath::Power(r / B, 2.)));
}

Double_t gaus2d(Double_t *x, Double_t *par) {
  Double_t A = par[0];  // peak
  Double_t B = par[1];
  Double_t x0 = par[2];  // x center
  Double_t y0 = par[3];  // y center
  // Double_t r = sqrt(pow(x[0] - x0, 2.) + pow(x[1] - y0, 2.));

  return A * TMath::Gaus(x[0], x0, B) * TMath::Gaus(x[1], y0, B);
}

Double_t cauc2d(Double_t *x, Double_t *par) {
  Double_t A = par[0];  // peak
  Double_t B = par[1];
  Double_t x0 = par[2];  // x center
  Double_t y0 = par[3];  // y center
  // Double_t r = sqrt(pow(x[0] - x0, 2.) + pow(x[1] - y0, 2.));

  return A * TMath::CauchyDist(x[0], x0, B) * TMath::CauchyDist(x[1], y0, B);
}

Double_t gausv2(Double_t *x, Double_t *par) {
  Double_t A = par[0];   // peak
  Double_t x0 = par[1];  // x center
  Double_t y0 = par[2];  // y center
  Double_t sx = par[3];  // x stddev
  Double_t sy = par[4];  // y stddev

  return A * TMath::Gaus(x[0], x0, sx) * TMath::Gaus(x[1], y0, sy);
}

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename arman, typename armclass>
BeamCenterFit<arman, armclass>::BeamCenterFit()
    : fCumulative(false),
      fRunByRun(false),
      fProjection(false),
      fVertexXY(false),
      fFitByLayer(false),
      fOptimize(false),
      fFixLimit(false),
      f2DFrom1D(false),
      fGaussian(false),
      fMinuitXY(false),
      fDrawProj(false),
      fOfficial(false),
      fFirstRun(-1),
      fLastRun(-1),
      fRebin(1),
      fNRun(-1),
      fNEne(-1) {
  fOutputFile = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename arman, typename armclass>
BeamCenterFit<arman, armclass>::~BeamCenterFit() {
  delete fOutputFile;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::GetInputHistograms() {
  UT::Printf(UT::kPrintInfo, "Getting input histograms...");
  fflush(stdout);

  TFile file(fInputFileName.Data(), "READ");
  if (file.IsZombie()) {
    file.Close();
    UT::Printf(UT::kPrintError, "File %s does not exist: Exit...\n", fInputFileName.Data());
  }

  fNRun = fLastRun - fFirstRun + 1;
  fNEne = fEnergyThr.size();

  Utils::AllocateVector3D(fVertexZaverage, fNRun, this->kPosNview, fNEne);
  for (Int_t ir = 0; ir < fNRun; ++ir)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t ie = 0; ie < fNEne; ++ie) {
        Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
        TString NameHis = Form("HitmapZaverage_%d%s_%.0fGeV", thisRun, iv == 0 ? "x" : "y", fEnergyThr[ie]);
        fVertexZaverage[ir][iv][ie] = NULL;
        fVertexZaverage[ir][iv][ie] = (TH1D *)file.Get(NameHis.Data());
        if (fVertexZaverage[ir][iv][ie] == NULL) {
          UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Exit...\n", NameHis.Data());
          continue;
        }
        if (fVertexZaverage[ir][iv][ie]->GetEntries() == 0) {
          // UT::Printf(UT::kPrintError, "\tHistogram %s is empty: Skip it...\n", NameHis.Data());
          delete fVertexZaverage[ir][iv][ie];
          fVertexZaverage[ir][iv][ie] = NULL;
          continue;
        }
        fVertexZaverage[ir][iv][ie]->SetDirectory(0);
      }

  Utils::AllocateVector4D(fVertexHitmap1D, fNRun, this->kPosNlayer, this->kPosNview, fNEne);
  Utils::AllocateVector4D(fVertexHitmap2D, fNRun, this->kPosNlayer, this->kPosNlayer, fNEne);
  for (Int_t ir = 0; ir < fNRun; ++ir)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ie = 0; ie < fNEne; ++ie) {
          Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
          TString NameHis = Form("HitmapVertex1D_%d_%d%s_%.0fGeV", thisRun, il, iv == 0 ? "x" : "y", fEnergyThr[ie]);
          fVertexHitmap1D[ir][il][iv][ie] = NULL;
          fVertexHitmap1D[ir][il][iv][ie] = (TH1D *)file.Get(NameHis.Data());
          if (fVertexHitmap1D[ir][il][iv][ie] == NULL) {
            UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Exit...\n", NameHis.Data());
            continue;
          }
          if (fVertexHitmap1D[ir][il][iv][ie]->GetEntries() == 0) {
            // UT::Printf(UT::kPrintError, "\tHistogram %s is empty: Skip it...\n", NameHis.Data());
            delete fVertexHitmap1D[ir][il][iv][ie];
            fVertexHitmap1D[ir][il][iv][ie] = NULL;
            continue;
          }
          fVertexHitmap1D[ir][il][iv][ie]->SetDirectory(0);
        }
  for (Int_t ir = 0; ir < fNRun; ++ir)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t jl = 0; jl < this->kPosNlayer; ++jl)
        for (Int_t ie = 0; ie < fNEne; ++ie) {
          Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
          TString NameHis = Form("HitmapVertex2D_%d_%dx_%dy_%.0fGeV", thisRun, il, jl, fEnergyThr[ie]);
          fVertexHitmap2D[ir][il][jl][ie] = NULL;
          fVertexHitmap2D[ir][il][jl][ie] = (TH2D *)file.Get(NameHis.Data());
          if (fVertexHitmap2D[ir][il][jl][ie] == NULL) {
            UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Skip it...\n", NameHis.Data());
            continue;
          }
          if (fVertexHitmap2D[ir][il][jl][ie]->GetEntries() == 0) {
            // UT::Printf(UT::kPrintError, "\tHistogram %s is empty: Skip it...\n", NameHis.Data());
            delete fVertexHitmap2D[ir][il][jl][ie];
            fVertexHitmap2D[ir][il][jl][ie] = NULL;
            continue;
          }
          fVertexHitmap2D[ir][il][jl][ie]->SetDirectory(0);
        }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::PrintSummary() {
  UT::Printf(UT::kPrintInfo, "Computing best estimation...");

  Double_t center[this->kPosNview];
  Double_t uncinf[this->kPosNview];
  Double_t uncsup[this->kPosNview];
  Double_t zlayer[this->kPosNview];
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    center[iv] = 0.00;
    uncinf[iv] = +1e6;
    uncsup[iv] = -1e6;
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      const Double_t pos2d = fFit2D_Cumulative[ie].GetParameter(iv == 0 ? "x_{0}" : "y_{0}");
      const Double_t pos1d = fFit1D_Cumulative[iv][ie].GetParameter(1);
      if (fEnergyThr[ie] == fNominalThr) {
        center[iv] = pos2d;
        zlayer[iv] = fZaverage_Cumulative[iv][ie]->GetMean();
      }
      if (pos2d < uncinf[iv]) uncinf[iv] = pos2d;
      if (pos2d > uncsup[iv]) uncsup[iv] = pos2d;
      if (pos1d < uncinf[iv]) uncinf[iv] = pos1d;
      if (pos1d > uncsup[iv]) uncsup[iv] = pos1d;
    }
    uncinf[iv] = center[iv] - uncinf[iv];
    uncsup[iv] = uncsup[iv] - center[iv];
  }

  fOfstream << endl;
  fOfstream << "Final Value\tx0\t-Δx\t+Δx\t@z\ty0\t-Δy\t+Δy\t@z" << endl;
  fOfstream << "\t\t" << center[0] << "\t-" << uncinf[0] << "\t+" << uncsup[0] << "\t" << zlayer[0] <<  //
      "\t" << center[1] << "\t-" << uncinf[1] << "\t+" << uncsup[1] << "\t" << zlayer[1] << endl;

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile = new TFile(Form("%s/FitResult%d-%d.root", fOutputDirName.Data(), fFirstRun, fLastRun), "RECREATE");
  fOutputFile->cd();

  if (fVertexXY) {
    if (fCumulative) SaveCumulative2D();
    if (fRunByRun) SaveRunbyRun2D();
  }
  if (fProjection) {
    if (fCumulative) SaveCumulative1D();
    if (fRunByRun) SaveRunByRun1D();
  }

  fOutputFile->Close();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SaveCumulative2D() {
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    if (fHisto2D_Cumulative[ie] == NULL) continue;

    fHisto2D_Cumulative[ie]->Write();
    fFit2D_Cumulative[ie].Write();
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SaveRunbyRun2D() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (fHisto2D_RunByRun[ir][ie] == NULL) continue;

      fHisto2D_RunByRun[ir][ie]->Write();
      fFit2D_RunByRun[ir][ie].Write();
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SaveCumulative1D() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      if (fHisto1D_Cumulative[iv][ie] == NULL) continue;

      fHisto1D_Cumulative[iv][ie]->Write();
      fFit1D_Cumulative[iv][ie].Write();
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SaveRunByRun1D() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto1D_RunByRun[ir][iv][ie] == NULL) continue;

        fHisto1D_RunByRun[ir][iv][ie]->Write();
        fFit1D_RunByRun[ir][iv][ie].Write();
      }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::OpenFiles() {
  UT::Printf(UT::kPrintInfo, "Opening output file...");
  fflush(stdout);

  fOfstream.open(Form("%s/FitResult%d-%d.dat", fOutputDirName.Data(), fFirstRun, fLastRun));

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOfstream.close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-----------------------------------------------------*/
/*--- Combine histograms according to input options ---*/
/*-----------------------------------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SetHistoToBeFitted() {
  UT::Printf(UT::kPrintInfo, "Combining input histograms...");
  fflush(stdout);

  Utils::AllocateVector2D(fZaverage_Cumulative, this->kPosNlayer, fNEne);
  Utils::AllocateVector3D(fZaverage_RunByRun, fNRun, this->kPosNlayer, fNEne);
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;

        if (fVertexZaverage[ir][iv][ie] == NULL) {
          continue;
        }
        if (fZaverage_Cumulative[iv][ie] == NULL) {
          fZaverage_Cumulative[iv][ie] = (TH1D *)(fVertexZaverage[ir][iv][ie]->Clone());
          fZaverage_Cumulative[iv][ie]->Reset();
          fZaverage_Cumulative[iv][ie]->SetName(
              Form("ZAverage_Cumulative%s_%d_%d_%.0fGeV", iv == 0 ? "X" : "Y", fFirstRun, fLastRun, fEnergyThr[ie]));
          fZaverage_Cumulative[iv][ie]->SetTitle(
              Form("Run %d-%d - E>%.0fGeV; %s Layer", fFirstRun, fLastRun, fEnergyThr[ie], iv == 0 ? "X" : "Y"));
        }
        if (fZaverage_RunByRun[ir][iv][ie] == NULL) {
          fZaverage_RunByRun[ir][iv][ie] = (TH1D *)(fVertexZaverage[ir][iv][ie]->Clone());
          fZaverage_RunByRun[ir][iv][ie]->Reset();
          fZaverage_RunByRun[ir][iv][ie]->SetName(
              Form("ZAverage_RunByRun%s_%d_%.0fGeV", iv == 0 ? "X" : "Y", thisRun, fEnergyThr[ie]));
          fZaverage_RunByRun[ir][iv][ie]->SetTitle(
              Form("Run %d - E>%.0fGeV; %s Layer", thisRun, fEnergyThr[ie], iv == 0 ? "X" : "Y"));
        }
        fZaverage_Cumulative[iv][ie]->Add(fVertexZaverage[ir][iv][ie]);
        fZaverage_RunByRun[ir][iv][ie]->Add(fVertexZaverage[ir][iv][ie]);
      }
    }
  }

  Utils::AllocateVector1D(fHisto2D_Cumulative, fNEne);
  Utils::AllocateVector2D(fHisto2D_RunByRun, fNRun, fNEne);
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t jl = 0; jl < this->kPosNlayer; ++jl) {
          if (fVertexHitmap2D[ir][il][jl][ie] == NULL) {
            continue;
          }
          if (fHisto2D_Cumulative[ie] == NULL) {
            fHisto2D_Cumulative[ie] = (TH2D *)(fVertexHitmap2D[ir][il][jl][ie]->Clone());
            fHisto2D_Cumulative[ie]->Reset();
            fHisto2D_Cumulative[ie]->SetName(
                Form("Hitmap2D_Cumulative_%d_%d_%.0fGeV", fFirstRun, fLastRun, fEnergyThr[ie]));
            fHisto2D_Cumulative[ie]->SetTitle(
                Form("Run %d-%d - E>%.0fGeV; X [mm]; Y [mm]", fFirstRun, fLastRun, fEnergyThr[ie]));
          }
          if (fHisto2D_RunByRun[ir][ie] == NULL) {
            fHisto2D_RunByRun[ir][ie] = (TH2D *)(fVertexHitmap2D[ir][il][jl][ie]->Clone());
            fHisto2D_RunByRun[ir][ie]->Reset();
            fHisto2D_RunByRun[ir][ie]->SetName(Form("Hitmap2D_RunByRun_%d_%.0fGeV", thisRun, fEnergyThr[ie]));
            fHisto2D_RunByRun[ir][ie]->SetTitle(Form("Run %d - E>%.0fGeV; X [mm]; Y [mm]", thisRun, fEnergyThr[ie]));
          }
          fHisto2D_Cumulative[ie]->Add(fVertexHitmap2D[ir][il][jl][ie]);
          fHisto2D_RunByRun[ir][ie]->Add(fVertexHitmap2D[ir][il][jl][ie]);
        }
    }
  }

  Utils::AllocateVector2D(fHisto1D_Cumulative, this->kPosNview, fNEne);
  Utils::AllocateVector3D(fHisto1D_RunByRun, fNRun, this->kPosNview, fNEne);
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;

        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          if (fVertexHitmap1D[ir][il][iv][ie] == NULL) {
            continue;
          }
          if (fHisto1D_Cumulative[iv][ie] == NULL) {
            fHisto1D_Cumulative[iv][ie] = (TH1D *)(fVertexHitmap1D[ir][il][iv][ie]->Clone());
            fHisto1D_Cumulative[iv][ie]->Reset();
            fHisto1D_Cumulative[iv][ie]->SetName(
                Form("Hitmap1D_Cumulative%s_%d_%d_%.0fGeV", iv == 0 ? "X" : "Y", fFirstRun, fLastRun, fEnergyThr[ie]));
            fHisto1D_Cumulative[iv][ie]->SetTitle(
                Form("Run %d-%d - E>%.0fGeV; %s [mm]", fFirstRun, fLastRun, iv == 0 ? "X" : "Y", fEnergyThr[ie]));
          }
          if (fHisto1D_RunByRun[ir][iv][ie] == NULL) {
            fHisto1D_RunByRun[ir][iv][ie] = (TH1D *)(fVertexHitmap1D[ir][il][iv][ie]->Clone());
            fHisto1D_RunByRun[ir][iv][ie]->Reset();
            fHisto1D_RunByRun[ir][iv][ie]->SetName(
                Form("Hitmap1D_RunByRun%s_%d_%.0fGeV", iv == 0 ? "X" : "Y", thisRun, fEnergyThr[ie]));
            fHisto1D_RunByRun[ir][iv][ie]->SetTitle(
                Form("Run %d - E>%.0fGeV; %s [mm]", thisRun, iv == 0 ? "X" : "Y", fEnergyThr[ie]));
          }
          fHisto1D_Cumulative[iv][ie]->Add(fVertexHitmap1D[ir][il][iv][ie]);
          fHisto1D_RunByRun[ir][iv][ie]->Add(fVertexHitmap1D[ir][il][iv][ie]);
        }
      }
    }
  }
  UT::Printf(UT::kPrintInfo, " Blablo.\n");

  Utils::AllocateVector3D(fHisto1D_ByLayer, this->kPosNlayer, this->kPosNview, fNEne);
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t ir = 0; ir < fNRun; ++ir) {
          if (fVertexHitmap1D[ir][il][iv][ie] == NULL) {
            continue;
          }
          if (fHisto1D_ByLayer[il][iv][ie] == NULL) {
            fHisto1D_ByLayer[il][iv][ie] = (TH1D *)(fVertexHitmap1D[ir][il][iv][ie]->Clone());
            fHisto1D_ByLayer[il][iv][ie]->Reset();
            fHisto1D_ByLayer[il][iv][ie]->SetName(Form("Hitmap1D_ByLayer%d%s_%d_%d_%.0fGeV", il, iv == 0 ? "X" : "Y",
                                                       fFirstRun, fLastRun, fEnergyThr[ie]));
            fHisto1D_ByLayer[il][iv][ie]->SetTitle(
                Form("Run %d-%d - E>%.0fGeV; %d%s [mm]", fFirstRun, fLastRun, il, iv == 0 ? "X" : "Y", fEnergyThr[ie]));
          }
          fHisto1D_ByLayer[il][iv][ie]->Add(fVertexHitmap1D[ir][il][iv][ie]);
        }
      }
    }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-------------------------------*/
/*--- Set Tower Range for Fit ---*/
/*-------------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SetTowerRange() {
  UT::Printf(UT::kPrintInfo, "Setting small tower edges...");
  fflush(stdout);

  // TODO: Check if this procedure works for Arm1 as well
  const Double_t towerSize = this->kTowerSize[0];
  // TODO: Is it better to use fiducial edge or not?
  const Double_t fiducEdge = this->fFiducial;
  // const Double_t fiducEdge = 0;

  TVector3 posInf = CT::GetRecoDetectorCoordinates(this->kArmIndex, 0, fiducEdge, fiducEdge, 0, 0);
  if (this->kArmIndex == 1)  // Arm2 is mirrored: How about Arm1?
    xTowerSupEdge = -posInf.X();
  else
    xTowerInfEdge = posInf.X();
  yTowerInfEdge = posInf.Y();

  TVector3 posSup =
      CT::GetRecoDetectorCoordinates(this->kArmIndex, 0, towerSize - fiducEdge, towerSize - fiducEdge, 0, 0);
  if (this->kArmIndex == 1)  // Arm2 is mirrored: How about Arm1?
    xTowerInfEdge = -posSup.X();
  else
    xTowerSupEdge = posSup.X();
  yTowerSupEdge = posSup.Y();

  UT::Printf(UT::kPrintInfo, " Done.\n");

  UT::Printf(UT::kPrintInfo, "\tEdge Minimum = [%f, %f]\n", xTowerInfEdge, yTowerInfEdge);
  UT::Printf(UT::kPrintInfo, "\tEdge Maximum = [%f, %f]\n", xTowerSupEdge, yTowerSupEdge);
}

/*-----------------------------*/
/*--- Palette Configuration ---*/
/*-----------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SetUpPalette() {
  const Int_t NRGBs = 5;
  const Int_t NCont = 999;

  Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
  Double_t red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
  Double_t green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
  Double_t blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};

  Int_t MyPalette[NCont];
  Int_t FI = TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  for (int i = 0; i < NCont; i++) MyPalette[i] = FI + i;

  gStyle->SetPalette(NCont, MyPalette);
  gStyle->SetNumberContours(NCont);
}

/*----------------------------*/
/*--- Canvas Configuration ---*/
/*----------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SetUpCanvasStyle() {
  TGaxis::SetMaxDigits(3);
  gROOT->Reset();
  gROOT->SetBatch();
  SetUpPalette();
}

/*---------------------*/
/*--- Fit functions ---*/
/*---------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::ClearBestPars2D() {
  for (Int_t iv = 0; iv < 2; ++iv) {
    fFmin_2D[iv] = 0.;
    fFmax_2D[iv] = 0.;
  }
  for (Int_t ip = 0; ip < 5; ++ip) fFPar_2D[ip] = 0.;
  fNdof_2D = 1.;
  fChi2_2D = 1e12;
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::CopyToBestFit2D(TF2 f2, Double_t minEdge[2], Double_t maxEdge[2]) {
  for (Int_t iv = 0; iv < 2; ++iv) {
    fFmin_2D[iv] = minEdge[iv];
    fFmax_2D[iv] = maxEdge[iv];
  }
  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) fFPar_2D[ip] = f2.GetParameter(ip);
  fNdof_2D = f2.GetNDF();
  fChi2_2D = f2.GetChisquare();
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::InitializeFit2D(TF2 &f2, Double_t minEdge[2], Double_t maxEdge[2],
                                                     Double_t maxPeak[2], Double_t ampPeak) {
  f2.SetRange(minEdge[0], minEdge[1], maxEdge[0], maxEdge[1]);

  if (fGaussian) {
    f2.SetParName(0, "A");
    f2.SetParName(1, "x_{0}");
    f2.SetParName(2, "y_{0}");
    f2.SetParName(3, "#sigma_{x}");
    f2.SetParName(4, "#sigma_{y}");
    f2.SetParameter(0, ampPeak);
    f2.SetParameter(1, maxPeak[0]);
    f2.SetParameter(2, maxPeak[1]);
    f2.SetParameter(3, 5.);
    f2.SetParameter(4, 5.);
  } else {
    f2.SetParName(0, "A");
    f2.SetParName(1, "#sigma");
    f2.SetParName(2, "x_{0}");
    f2.SetParName(3, "y_{0}");
    //  f2.SetParameter(0, ampPeak);
    //  f2.SetParameter(1, 0.1);
    //  f2.SetParameter(2, maxPeak[0]);
    //  f2.SetParameter(3, maxPeak[1]);
    f2.SetParameter(0, ampPeak);
    f2.SetParameter(1, 5.);
    f2.SetParameter(2, maxPeak[0]);
    f2.SetParameter(3, maxPeak[1]);
  }

  UT::Printf(UT::kPrintDebug, "2D Parameters initialization:\n");
  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
    string parnam = f2.GetParName(ip);
    Double_t parval = f2.GetParameter(ip);
    Double_t parmin, parmax;
    f2.GetParLimits(ip, parmin, parmax);
    UT::Printf(UT::kPrintDebug, "\t%s : %f in [%f, %f]\n", parnam.c_str(), parval, parmin, parmax);
  }
}

// First optimize x range, then optimize y range
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::OptimizeRange2DFast(TH2D *h2, Double_t minEdge[2], Double_t maxEdge[2],
                                                         Double_t maxPeak[2], Double_t ampPeak) {
  Int_t minNbin = 6 * (fRebin / 8);
  Int_t minDist = 3 * (fRebin / 8);

  Int_t dummy;

  Int_t minBinX, minBinY;
  Int_t maxBinX, maxBinY;
  Int_t peakBinX, peakBinY;

  Int_t minBin, maxBin, peakBin;

  ClearBestPars2D();

  /*
   * Optimize x range
   */

  //	UT::Printf(UT::kPrintInfo, "\t ---> X Range\n");

  minBin = h2->FindBin(minEdge[0], minEdge[1]);
  maxBin = h2->FindBin(maxEdge[0], maxEdge[1]);
  peakBin = h2->FindBin(maxPeak[0], maxPeak[1]);

  h2->GetBinXYZ(minBin, minBinX, minBinY, dummy);
  h2->GetBinXYZ(maxBin, maxBinX, maxBinY, dummy);
  h2->GetBinXYZ(peakBin, peakBinX, peakBinY, dummy);

  while (minBinX > peakBinX - minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinX < peakBinX + minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinX - minBinX < minNbin) {
    if (minNbin <= 1) break;
    --minNbin;
  }

  while (minBinY > peakBinY - minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinY < peakBinY + minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinY - minBinY < minNbin) {
    if (minNbin <= 1) break;
    --minNbin;
  }

  for (Int_t imX = minBinX; imX <= peakBinX - minDist; ++imX)
    for (Int_t iMX = maxBinX; iMX >= peakBinX + minDist; --iMX) {
      if ((iMX - imX) < minNbin) continue;

      const Double_t xEdgeMin = h2->GetXaxis()->GetBinLowEdge(imX);
      const Double_t xEdgeMax = h2->GetXaxis()->GetBinLowEdge(iMX);  // It is correct since range is [min, max)

      const Double_t yEdgeMin = h2->GetYaxis()->GetBinLowEdge(minBinY);
      const Double_t yEdgeMax = h2->GetYaxis()->GetBinLowEdge(maxBinY);  // It is correct since range is [min, max)

      Double_t pEdgeMin[2] = {xEdgeMin, yEdgeMin};
      Double_t pEdgeMax[2] = {xEdgeMax, yEdgeMax};
      Double_t pMaxPeak[2] = {maxPeak[0], maxPeak[1]};

      TF2 f2;
      if (fGaussian) {
        f2 = TF2(Form("Tmp_%s", h2->GetName()), gausv2, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 5);
      } else {
        // f2 = TF2(Form("Tmp_%s", h2->GetName()), exp2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
        f2 = TF2(Form("Tmp_%s", h2->GetName()), gaus2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
        // f2 = TF2(Form("Tmp_%s", h2->GetName()), lor2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
        // f2 = TF2(Form("Tmp_%s", h2->GetName()), cauc2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
      }

      InitializeFit2D(f2, pEdgeMin, pEdgeMax, pMaxPeak, ampPeak);

      UT::Printf(UT::kPrintDebug, "\n\t\t Optimization in [%f, %f]-[%f, %f] (%f in [%f, %f]) \n\n", pEdgeMin[0],
                 pEdgeMax[0], pEdgeMin[1], pEdgeMax[1], ampPeak, maxPeak[0], maxPeak[1]);
      Double_t pini, pmin, pmax, qmin, qmax;
      f2.GetRange(pmin, qmin, pmax, qmax);
      UT::Printf(UT::kPrintDebug, "\t\t Fit Range [%f, %f]-[%f, %f] \n", pmin, pmax, qmin, qmax);
      for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
        pini = f2.GetParameter(ip);
        f2.GetParLimits(ip, pmin, pmax);
        UT::Printf(UT::kPrintDebug, "\t\t\t Parameter %d: %f in [%f, %f] \n", ip, pini, pmin, pmax);
      }

      h2->Fit(&f2, "NORIQ");  // E Minos for error estimation

      if (!f2.IsValid()) continue;
      if (f2.GetNDF() < TMath::Power(minNbin - 4, 2)) continue;
      if (f2.GetChisquare() / f2.GetNDF() > fChi2_2D / fNdof_2D) continue;

      // Check is a parameter is ** at limit **
      Bool_t atlimit = false;
      for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
        Double_t parval = f2.GetParameter(ip);
        Double_t parmin, parmax;
        f2.GetParLimits(ip, parmin, parmax);
        if (TMath::Abs(parval / parmin - 1) < 1e-3 || TMath::Abs(parval / parmax - 1) < 1e-3) atlimit = true;
      }
      if (atlimit) continue;

      UT::Printf(UT::kPrintDebug, "\t\t Best Range Update [%f, %f]-[%f, %f] (%.0f/%d=%.0f against %.0f/%.0f=%.0f) \n",
                 pEdgeMin[0], pEdgeMax[0], pEdgeMin[1], pEdgeMax[1], f2.GetChisquare(), f2.GetNDF(),
                 f2.GetChisquare() / f2.GetNDF(), fChi2_2D, fNdof_2D, fChi2_2D / fNdof_2D);

      CopyToBestFit2D(f2, pEdgeMin, pEdgeMax);
    }

  /*
   * Optimize y range
   */

  //	UT::Printf(UT::kPrintInfo, "\t ---> Y Range\n");

  minBin = h2->FindBin(fFmin_2D[0], minEdge[1]);
  maxBin = h2->FindBin(fFmax_2D[0], maxEdge[1]);
  peakBin = h2->FindBin(maxPeak[0], maxPeak[1]);

  h2->GetBinXYZ(minBin, minBinX, minBinY, dummy);
  h2->GetBinXYZ(maxBin, maxBinX, maxBinY, dummy);
  h2->GetBinXYZ(peakBin, peakBinX, peakBinY, dummy);

  for (Int_t imY = minBinY; imY <= peakBinY - minDist; ++imY)
    for (Int_t iMY = maxBinY; iMY >= peakBinY + minDist; --iMY) {
      if ((iMY - imY) < minNbin) continue;

      const Double_t xEdgeMin = h2->GetXaxis()->GetBinLowEdge(minBinX);
      const Double_t xEdgeMax = h2->GetXaxis()->GetBinLowEdge(maxBinX);  // It is correct since range is [min, max)

      const Double_t yEdgeMin = h2->GetYaxis()->GetBinLowEdge(imY);
      const Double_t yEdgeMax = h2->GetYaxis()->GetBinLowEdge(iMY);  // It is correct since range is [min, max)

      Double_t pEdgeMin[2] = {xEdgeMin, yEdgeMin};
      Double_t pEdgeMax[2] = {xEdgeMax, yEdgeMax};

      TF2 f2;
      if (fGaussian) {
        f2 = TF2(Form("Tmp_%s", h2->GetName()), gausv2, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 5);
      } else {
        // f2 = TF2(Form("Tmp_%s", h2->GetName()), exp2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
        f2 = TF2(Form("Tmp_%s", h2->GetName()), gaus2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
        // f2 = TF2(Form("Tmp_%s", h2->GetName()), lor2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
        // f2 = TF2(Form("Tmp_%s", h2->GetName()), cauc2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
      }

      InitializeFit2D(f2, pEdgeMin, pEdgeMax, maxPeak, ampPeak);

      UT::Printf(UT::kPrintDebug, "\n\t\t Optimization in [%f, %f]-[%f, %f] (%f in [%f, %f]) \n\n", pEdgeMin[0],
                 pEdgeMax[0], pEdgeMin[1], pEdgeMax[1], ampPeak, maxPeak[0], maxPeak[1]);
      Double_t pini, pmin, pmax, qmin, qmax;
      f2.GetRange(pmin, qmin, pmax, qmax);
      UT::Printf(UT::kPrintDebug, "\t\t Fit Range [%f, %f]-[%f, %f] \n", pmin, pmax, qmin, qmax);
      for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
        pini = f2.GetParameter(ip);
        f2.GetParLimits(ip, pmin, pmax);
        UT::Printf(UT::kPrintDebug, "\t\t\t Parameter %d: %f in [%f, %f] \n", ip, pini, pmin, pmax);
      }

      h2->Fit(&f2, "NORIQ");  // E Minos for error estimation

      if (!f2.IsValid()) continue;
      if (f2.GetNDF() < TMath::Power(minNbin - 4, 2)) continue;
      if (f2.GetChisquare() / f2.GetNDF() > fChi2_2D / fNdof_2D) continue;

      UT::Printf(UT::kPrintDebug, "\t\t Best Range Update [%f, %f]-[%f, %f] (%.0f/%d=%.0f against %.0f/%.0f=%.0f) \n",
                 pEdgeMin[0], pEdgeMax[0], pEdgeMin[1], pEdgeMax[1], f2.GetChisquare(), f2.GetNDF(),
                 f2.GetChisquare() / f2.GetNDF(), fChi2_2D, fNdof_2D, fChi2_2D / fNdof_2D);

      CopyToBestFit2D(f2, pEdgeMin, pEdgeMax);
    }
}

// Simultaneously optimize x and y range: never ending...
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::OptimizeRange2DFull(TH2D *h2, Double_t minEdge[2], Double_t maxEdge[2],
                                                         Double_t maxPeak[2], Double_t ampPeak) {
  Int_t minNbin = 6 * (fRebin / 8);
  Int_t minDist = 3 * (fRebin / 8);

  Int_t dummy;

  Int_t minBinX, minBinY;
  Int_t maxBinX, maxBinY;
  Int_t peakBinX, peakBinY;

  Int_t minBin, maxBin, peakBin;

  ClearBestPars2D();

  minBin = h2->FindBin(minEdge[0], minEdge[1]);
  maxBin = h2->FindBin(maxEdge[0], maxEdge[1]);
  peakBin = h2->FindBin(maxPeak[0], maxPeak[1]);

  h2->GetBinXYZ(minBin, minBinX, minBinY, dummy);
  h2->GetBinXYZ(maxBin, maxBinX, maxBinY, dummy);
  h2->GetBinXYZ(peakBin, peakBinX, peakBinY, dummy);

  while (minBinX > peakBinX - minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinX < peakBinX + minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinX - minBinX < minNbin) {
    if (minNbin <= 1) break;
    --minNbin;
  }

  while (minBinY > peakBinY - minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinY < peakBinY + minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBinY - minBinY < minNbin) {
    if (minNbin <= 1) break;
    --minNbin;
  }

  for (Int_t imX = minBinX; imX <= peakBinX - minDist; ++imX)
    for (Int_t iMX = maxBinX; iMX >= peakBinX + minDist; --iMX) {
      if ((iMX - imX) < minNbin) continue;

      for (Int_t imY = minBinY; imY <= peakBinY - minDist; ++imY)
        for (Int_t iMY = maxBinY; iMY >= peakBinY + minDist; --iMY) {
          if ((iMY - imY) < minNbin) continue;

          const Double_t xEdgeMin = h2->GetXaxis()->GetBinLowEdge(imX);
          const Double_t xEdgeMax = h2->GetXaxis()->GetBinLowEdge(iMX);  // It is correct since range is [min, max)

          const Double_t yEdgeMin = h2->GetYaxis()->GetBinLowEdge(imY);
          const Double_t yEdgeMax = h2->GetYaxis()->GetBinLowEdge(iMY);  // It is correct since range is [min, max)

          Double_t pEdgeMin[2] = {xEdgeMin, yEdgeMin};
          Double_t pEdgeMax[2] = {xEdgeMax, yEdgeMax};

          TF2 f2;
          if (fGaussian) {
            f2 = TF2(Form("Tmp_%s", h2->GetName()), gausv2, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 5);
          } else {
            // f2 = TF2(Form("Tmp_%s", h2->GetName()), exp2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
            f2 = TF2(Form("Tmp_%s", h2->GetName()), gaus2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
            // f2 = TF2(Form("Tmp_%s", h2->GetName()), lor2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
            // f2 = TF2(Form("Tmp_%s", h2->GetName()), cauc2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
          }

          InitializeFit2D(f2, pEdgeMin, pEdgeMax, maxPeak, ampPeak);

          UT::Printf(UT::kPrintDebug, "\n\t\t Optimization in [%f, %f]-[%f, %f] (%f in [%f, %f]) \n\n", pEdgeMin[0],
                     pEdgeMax[0], pEdgeMin[1], pEdgeMax[1], ampPeak, maxPeak[0], maxPeak[1]);
          Double_t pini, pmin, pmax, qmin, qmax;
          f2.GetRange(pmin, qmin, pmax, qmax);
          UT::Printf(UT::kPrintDebug, "\t\t Fit Range [%f, %f]-[%f, %f] \n", pmin, pmax, qmin, qmax);
          for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
            pini = f2.GetParameter(ip);
            f2.GetParLimits(ip, pmin, pmax);
            UT::Printf(UT::kPrintDebug, "\t\t\t Parameter %d: %f in [%f, %f] \n", ip, pini, pmin, pmax);
          }

          TFitResultPtr r1 = h2->Fit(&f2, "NORISQ");  // E Minos for error estimation

          if (!r1->IsValid()) continue;
          if (f2.GetNDF() < TMath::Power(minNbin - 4, 2)) continue;
          if (f2.GetChisquare() / f2.GetNDF() > fChi2_2D / fNdof_2D) continue;

          // Check is a parameter is ** at limit **
          Bool_t atlimit = false;
          for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
            Double_t parval = f2.GetParameter(ip);
            Double_t parmin, parmax;
            f2.GetParLimits(ip, parmin, parmax);
            if (TMath::Abs(parval / parmin - 1) < 1e-3 || TMath::Abs(parval / parmax - 1) < 1e-3) atlimit = true;
          }
          if (atlimit) continue;

          UT::Printf(UT::kPrintDebug,
                     "\t\t Best Range Update [%f, %f]-[%f, %f] (%.0f/%d=%.0f against %.0f/%.0f=%.0f)\n", pEdgeMin[0],
                     pEdgeMax[0], pEdgeMin[1], pEdgeMax[1], f2.GetChisquare(), f2.GetNDF(),
                     f2.GetChisquare() / f2.GetNDF(), fChi2_2D, fNdof_2D, fChi2_2D / fNdof_2D);

          CopyToBestFit2D(f2, pEdgeMin, pEdgeMax);
        }
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SetUpFinalFit2D(TF2 &f2) {
  f2.SetRange(fFmin_2D[0], fFmin_2D[1], fFmax_2D[0], fFmax_2D[1]);

  if (fGaussian) {
    f2.SetParName(0, "A");
    f2.SetParName(1, "x_{0}");
    f2.SetParName(2, "y_{0}");
    f2.SetParName(3, "#sigma_{x}");
    f2.SetParName(4, "#sigma_{y}");
  } else {
    f2.SetParName(0, "A");
    f2.SetParName(1, "#sigma");
    f2.SetParName(2, "x_{0}");
    f2.SetParName(3, "y_{0}");
  }

  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) f2.SetParameter(ip, fFPar_2D[ip]);

  //  f2.SetParLimits(0, .5 * fFPar_2D[0], 5. * fFPar_2D[0]);
  //  //f2.SetParLimits(1, 0., 1.);
  //  f2.SetParLimits(2, fFmin_2D[0], fFmax_2D[0]);
  //  f2.SetParLimits(3, fFmin_2D[1], fFmax_2D[1]);

  UT::Printf(UT::kPrintDebug, "2D Parameters final initialization:\n");
  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
    string parnam = f2.GetParName(ip);
    Double_t parval = f2.GetParameter(ip);
    Double_t parmin, parmax;
    f2.GetParLimits(ip, parmin, parmax);
    UT::Printf(UT::kPrintDebug, "\t%s : %f in [%f, %f]\n", parnam.c_str(), parval, parmin, parmax);
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Fit2DVertex(TH2D *h2, TF2 &f2) {
  UT::Printf(UT::kPrintInfo, "\n=== 2D FITTING: %s\n\n", h2->GetName());

  /*
   * Get information from histogram name
   */

  TString name = h2->GetName();

  Bool_t isRunByRun = false;
  if (name.Contains("Cumulative")) {
    isRunByRun = false;
  } else if (name.Contains("ByLayer")) {
    isRunByRun = false;
  } else if (name.Contains("RunByRun")) {
    isRunByRun = true;
  } else {
    UT::Printf(UT::kPrintError, "===             Attempting to fit nor Cumulative nor RunByRun! Exit...\n");
    exit(EXIT_FAILURE);
  }

  Int_t eneBin = -1;
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    if (name.Contains(Form("%.0fGeV", fEnergyThr[ie]))) {
      eneBin = ie;
      break;
    }
  }
  if (eneBin < 0) {
    UT::Printf(UT::kPrintError, "===             Attempting to fit an absurd Energy Threshold! Exit...\n");
    exit(EXIT_FAILURE);
  }

  /*
   * Complex weighted fRebin scheme to avoid bad counts on tower edges
   */

  TH2D *hn = (TH2D *)(h2->Clone());
  hn->Reset();
  for (Int_t ibinX = 1; ibinX < hn->GetXaxis()->GetNbins() + 1; ibinX++)
    for (Int_t ibinY = 1; ibinY < hn->GetYaxis()->GetNbins() + 1; ibinY++) {
      if (h2->GetBinContent(ibinX, ibinY) > 0) hn->SetBinContent(ibinX, ibinY, 1.);
    }
  hn->Rebin2D(fRebin, fRebin);
  for (Int_t ibinX = 1; ibinX < hn->GetXaxis()->GetNbins() + 1; ibinX++)
    for (Int_t ibinY = 1; ibinY < hn->GetYaxis()->GetNbins() + 1; ibinY++) {
      Double_t cont = hn->GetBinContent(ibinX, ibinY) / TMath::Power(fRebin, 2.);
      hn->SetBinContent(ibinX, ibinY, cont);
    }
  h2->Rebin2D(fRebin, fRebin);
  h2->Divide(hn);

  if (fOfficial) {  // For official canvas we mask edge bins
    for (Int_t ibinX = 1; ibinX < hn->GetXaxis()->GetNbins() + 1; ibinX++)
      for (Int_t ibinY = 1; ibinY < hn->GetYaxis()->GetNbins() + 1; ibinY++) {
        if (hn->GetBinContent(ibinX, ibinY) != 1.) h2->SetBinContent(ibinX, ibinY, 0.);
      }
  }

  /*
   * Fill points to a vector and compute fit range
   */

  Int_t nbinX = h2->GetXaxis()->GetNbins();
  Int_t nbinY = h2->GetYaxis()->GetNbins();

  // Position corresponding to maximum amplitude
  Double_t hAmpPeak = -1.;
  Double_t xMaxPeak = -1.;
  Double_t yMaxPeak = -1.;

  // Initial funmin-max range on small tower
  Double_t xEdgeMin = +1000;
  Double_t yEdgeMin = +1000;
  Double_t xEdgeMax = -1000;
  Double_t yEdgeMax = -1000;

  // Vector storing the information to be fitted
  vector<Double_t> posx;
  vector<Double_t> posy;
  vector<Double_t> pamp;

  posx.clear();
  posy.clear();
  pamp.clear();

  hAmpPeak = h2->GetMaximum();
  Int_t binXmax = h2->ProjectionX()->GetMaximumBin();
  xMaxPeak = h2->GetXaxis()->GetBinCenter(binXmax);
  Int_t binYmax = h2->ProjectionY()->GetMaximumBin();
  yMaxPeak = h2->GetYaxis()->GetBinCenter(binYmax);

  if (!fFixLimit || !isRunByRun) {  // If fitting cumulative or without fixed limits
    for (Int_t ibinX = 1; ibinX < nbinX + 1; ibinX++) {
      for (Int_t ibinY = 1; ibinY < nbinY + 1; ibinY++) {
        if (h2->GetBinContent(ibinX, ibinY) > 0) {
          if (fMinuitXY) {
            posx.push_back(h2->GetXaxis()->GetBinCenter(ibinX));
            posy.push_back(h2->GetYaxis()->GetBinCenter(ibinY));
            pamp.push_back(h2->GetBinContent(ibinX, ibinY));
          }

          Double_t x_inf = h2->GetXaxis()->GetBinLowEdge(ibinX);
          Double_t y_inf = h2->GetYaxis()->GetBinLowEdge(ibinY);
          Double_t x_sup = h2->GetXaxis()->GetBinLowEdge(1 + ibinX);
          Double_t y_sup = h2->GetYaxis()->GetBinLowEdge(1 + ibinY);

          if (x_inf >= xTowerInfEdge && x_inf < xEdgeMin) xEdgeMin = x_inf;
          if (x_sup <= xTowerSupEdge && x_sup > xEdgeMax) xEdgeMax = x_sup;
          if (y_inf >= yTowerInfEdge && y_inf < yEdgeMin) yEdgeMin = y_inf;
          if (y_sup <= yTowerSupEdge && y_sup > yEdgeMax) yEdgeMax = y_sup;
        }
      }
    }
  } else {  // If fitting run-by-run with fixed limits
    fFit2D_Cumulative[eneBin].GetRange(xEdgeMin, yEdgeMin, xEdgeMax, yEdgeMax);
  }

  /*
   * Fit
   */

  ClearBestPars2D();

  Int_t npfit = 1;
  Int_t nvpar = fGaussian ? 5 : 4;
  Double_t par[6];

  Int_t xminbin, xmaxbin, yminbin, ymaxbin, dummy;
  h2->GetBinXYZ(h2->FindBin(xEdgeMin, yEdgeMin), xminbin, yminbin, dummy);
  h2->GetBinXYZ(h2->FindBin(xEdgeMax, yEdgeMax), xmaxbin, ymaxbin, dummy);
  UT::Printf(UT::kPrintInfo, "===             Fit in x = [%.2f, %.2f]\n", xEdgeMin, xEdgeMax);
  UT::Printf(UT::kPrintInfo, "===                    y = [%.2f, %.2f]\n", yEdgeMin, yEdgeMax);
  UT::Printf(UT::kPrintInfo, "===                        (%.0f entries)\n",
             h2->Integral(xminbin, xmaxbin, yminbin, ymaxbin));

  Double_t pEdgeMin[2] = {xEdgeMin, yEdgeMin};
  Double_t pEdgeMax[2] = {xEdgeMax, yEdgeMax};
  Double_t pMaxPeak[2] = {xMaxPeak, yMaxPeak};

  if (f2DFrom1D) {  // Overwrite initialization using the result from 1D fit
    // Get Cumulative/RunByRun
    Bool_t isRunByRun = false;
    if (name.Contains("Cumulative")) {
      isRunByRun = false;
    } else if (name.Contains("RunByRun")) {
      isRunByRun = true;
    } else {
      UT::Printf(UT::kPrintError, "===             Attempting to fit nor Cumulative nor RunByRun! Exit...\n");
      exit(EXIT_FAILURE);
    }
    // Retrieve energy bin
    Int_t eneBin = -1;
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      if (name.Contains(Form("%.0fGeV", fEnergyThr[ie]))) {
        eneBin = ie;
        break;
      }
    }
    if (eneBin < 0) {
      UT::Printf(UT::kPrintError, "===             Attempting to fit an absurd Energy Threshold! Exit...\n");
      exit(EXIT_FAILURE);
    }
    // Retrieve run number
    Int_t runBin = -1;
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (name.Contains(Form("RunByRun_%d", fFirstRun + ir))) {
        runBin = ir;
        break;
      }
    }
    if (isRunByRun && runBin < 0) {
      UT::Printf(UT::kPrintError, "===             Attempting to fit an absurd Run number! Exit...\n");
      exit(EXIT_FAILURE);
    }
    // Initialize 2D fit from 1D fit result
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      TF1 *finit = NULL;
      if (isRunByRun)
        finit = &fFit1D_RunByRun[runBin][iv][eneBin];
      else
        finit = &fFit1D_Cumulative[iv][eneBin];
      finit->GetRange(pEdgeMin[iv], pEdgeMax[iv]);
      pMaxPeak[iv] = finit->GetParameter(1);
    }
  } else if (fOptimize) {
    if (!fFixLimit || !isRunByRun) {  // Do not perform optimization if fitting run-by-run with fixed limits
      UT::Printf(UT::kPrintInfo, "===             \t Optimization\n\n");
      OptimizeRange2D(h2, pEdgeMin, pEdgeMax, pMaxPeak, hAmpPeak);
    }
  }

  TF2 f1;
  if (fGaussian) {
    f1 = TF2(Form("Aux_%s", h2->GetName()), gausv2, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, nvpar);
  } else {
    // f1 = TF2(Form("Aux_%s", h2->GetName()), exp2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
    f1 = TF2(Form("Aux_%s", h2->GetName()), gaus2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, nvpar);
    // f1 = TF2(Form("Aux_%s", h2->GetName()), lor2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
    // f1 = TF2(Form("Aux_%s", h2->GetName()), cauc2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
  }

  if (fNdof_2D == 1. || fChi2_2D == 1e12) {  // If no optimization was performed
    UT::Printf(UT::kPrintInfo, "===             \t Fit\n");
    InitializeFit2D(f1, pEdgeMin, pEdgeMax, pMaxPeak, hAmpPeak);
  } else {  // If optimization was successfully performed
    h2->GetBinXYZ(h2->FindBin(fFmin_2D[0], fFmax_2D[0]), xminbin, yminbin, dummy);
    h2->GetBinXYZ(h2->FindBin(fFmin_2D[1], fFmax_2D[1]), xmaxbin, ymaxbin, dummy);
    UT::Printf(UT::kPrintInfo, "===             Final fit in x = [%.2f, %.2f]\n", fFmin_2D[0], fFmax_2D[0]);
    UT::Printf(UT::kPrintInfo, "===                          y = [%.2f, %.2f]\n", fFmin_2D[1], fFmax_2D[1]);
    UT::Printf(UT::kPrintInfo, "===                              (%.0f entries)\n",
               h2->Integral(xminbin, xmaxbin, yminbin, ymaxbin));
    SetUpFinalFit2D(f1);
  }

  h2->Fit(&f1, "NOERI");  // E Minos for error estimation

  // Necessary to overwrite function range with the result of the optimization
  f1.GetRange(xEdgeMin, yEdgeMin, xEdgeMax, yEdgeMax);

  if (fGaussian) {
    f2 = TF2(Form("Fit_%s", h2->GetName()), gausv2, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, nvpar);
  } else {
    // f2 = TF2(Form("Fit_%s", h2->GetName()), exp2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
    f2 = TF2(Form("Fit_%s", h2->GetName()), gaus2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, nvpar);
    // f2 = TF2(Form("Fit_%s", h2->GetName()), lor2d1, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
    // f2 = TF2(Form("Fit_%s", h2->GetName()), cauc2d, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax, 4);
  }

  f2.SetContour(6);
  f2.SetChisquare(f1.GetChisquare());
  f2.SetNDF(f1.GetNDF());
  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
    f2.SetParName(ip, f1.GetParName(ip));
    f2.SetParameter(ip, f1.GetParameter(ip));
    f2.SetParError(ip, f1.GetParError(ip));
  }

  if (fMinuitXY) {
    UT::Printf(UT::kPrintInfo, "===             \t Second Fit\n");

    // TODO: This fitting function does not use integral, is it more useful or dangerous?
    ROOT::Minuit2::CustomFCN fFCN(posx, posy, pamp, &npfit, xEdgeMin, xEdgeMax, yEdgeMin, yEdgeMax);

    ROOT::Minuit2::MnUserParameters upar;
    for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
      upar.Add(f1.GetParName(ip), f1.GetParameter(0), 1e-2);

      ROOT::Minuit2::MnMigrad migrad(fFCN, upar);
      ROOT::Minuit2::FunctionMinimum funmin = migrad();
      if (!funmin.IsValid()) {
        UT::Printf(UT::kPrintInfo, "===             \t\t Valid=NO! Exit...\n");
        exit(EXIT_FAILURE);  // TODO: Handle this case...
      }

      f2.SetChisquare(funmin.Fval());
      f2.SetNDF(npfit - nvpar);

      for (Int_t ip = 0; ip < f2.GetNpar(); ++ip) {
        f2.SetParameter(0, funmin.UserState().Value(ip));
        f2.SetParError(0, funmin.UserState().Error(ip));
      }
    }
  }

  UT::Printf(UT::kPrintInfo, "\t\t Valid=YES\n");
  UT::Printf(UT::kPrintInfo, "  ChiSquare = %f with Ndof = %d\n", f2.GetChisquare(), f2.GetNDF());
  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip)
    UT::Printf(UT::kPrintInfo, "   %s = %f ± %f\n", f2.GetParName(ip), f2.GetParameter(ip), f2.GetParError(ip));

  /*
   * Save information to output files
   */

  if (fGaussian) {
    fOfstream << endl;
    fOfstream << "2D Histo"
              << "\t\t\t"
              << "valid"
              << "\t"
              << "chi"
              << "\t"
              << "ndof"
              << "\t"
              << "A"
              << "\t"
              << "x0"
              << "\t"
              << "Δx"
              << "\t"
              << "y0"
              << "\t"
              << "Δy"
              << "\t"
              << "sx"
              << "\t"
              << "sy" << endl;
    fOfstream << h2->GetTitle() << "\t" << f2.IsValid() << "\t" << fixed << setprecision(3) << f2.GetChisquare() << "\t"
              << f2.GetNDF() << "\t" << f2.GetParameter(0) << "\t" << f2.GetParameter(1) << "\t" << f2.GetParError(1)
              << "\t" << f2.GetParameter(2) << "\t" << f2.GetParError(2) << "\t" << f2.GetParameter(3) << "\t"
              << f2.GetParameter(4) << endl;
  } else {
    fOfstream << endl;
    fOfstream << "2D Histo"
              << "\t\t\t"
              << "valid"
              << "\t"
              << "chi"
              << "\t"
              << "ndof"
              << "\t"
              << "A"
              << "\t"
              << "B"
              << "\t"
              << "x0"
              << "\t"
              << "Δx"
              << "\t"
              << "y0"
              << "\t"
              << "Δy" << endl;
    fOfstream << h2->GetTitle() << "\t" << f2.IsValid() << "\t" << fixed << setprecision(3) << f2.GetChisquare() << "\t"
              << f2.GetNDF() << "\t" << f2.GetParameter(0) << "\t" << f2.GetParameter(1) << "\t" << f2.GetParameter(2)
              << "\t" << f2.GetParError(2) << "\t" << f2.GetParameter(3) << "\t" << f2.GetParError(3) << endl;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::ClearBestPars1D() {
  fFmin_1D = 0.;
  fFmax_1D = 0.;
  for (Int_t ip = 0; ip < 3; ++ip) fFPar_1D[ip] = 0.;
  fNdof_1D = 1.;
  fChi2_1D = 1e12;
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::CopyToBestFit1D(TF1 f1, Double_t minEdge, Double_t maxEdge) {
  fFmin_1D = minEdge;
  fFmax_1D = maxEdge;
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) fFPar_1D[ip] = f1.GetParameter(ip);
  fNdof_1D = f1.GetNDF();
  fChi2_1D = f1.GetChisquare();
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::InitializeFit1D(TF1 &f1, Int_t view, Double_t minEdge, Double_t maxEdge,
                                                     Double_t maxPeak, Double_t ampPeak) {
  f1.SetRange(minEdge, maxEdge);

  f1.SetParName(0, "A");
  f1.SetParName(1, view == 0 ? "x_{0}" : "y_{0}");
  f1.SetParName(2, "#sigma");

  f1.SetParameter(0, ampPeak);
  f1.SetParameter(1, maxPeak);
  f1.SetParameter(2, 5.);

  f1.SetParLimits(0, .5 * ampPeak, 5. * ampPeak);
  f1.SetParLimits(1, minEdge, maxEdge);
  f1.SetParLimits(2, 2.5, 10.);

  UT::Printf(UT::kPrintDebug, "1D Parameters initialization:\n");
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
    string parnam = f1.GetParName(ip);
    Double_t parval = f1.GetParameter(ip);
    Double_t parmin, parmax;
    f1.GetParLimits(ip, parmin, parmax);
    UT::Printf(UT::kPrintDebug, "\t%s : %f in [%f, %f]\n", parnam.c_str(), parval, parmin, parmax);
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::OptimizeRange1D(TH1D *h1, Int_t view, Double_t minEdge, Double_t maxEdge,
                                                     Double_t maxPeak, Double_t ampPeak) {
  Int_t minNbin = 6 * (fRebin / 8);
  Int_t minDist = 3 * (fRebin / 8);

  ClearBestPars1D();

  const Int_t minBin = h1->FindBin(minEdge);
  const Int_t maxBin = h1->FindBin(maxEdge);
  const Int_t peakBin = h1->FindBin(maxPeak);

  while (minBin > peakBin - minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBin < peakBin + minDist) {
    if (minDist <= 1) break;
    --minDist;
  }
  while (maxBin - minBin < minNbin) {
    if (minNbin <= 1) break;
    --minNbin;
  }

  for (Int_t im = minBin; im <= peakBin - minDist; ++im)
    for (Int_t iM = maxBin; iM >= peakBin + minDist; --iM) {
      if ((iM - im) < minNbin) continue;

      const Double_t pEdgeMin = h1->GetXaxis()->GetBinLowEdge(im);
      const Double_t pEdgeMax = h1->GetXaxis()->GetBinLowEdge(iM);  // It is correct since range is [min, max)

      TF1 f1 = TF1(Form("Tmp_%s", h1->GetName()), "gaus", pEdgeMin, pEdgeMax);

      InitializeFit1D(f1, view, pEdgeMin, pEdgeMax, maxPeak, ampPeak);

      UT::Printf(UT::kPrintDebug, "\n\t\t Optimization in %f-%f (%f in %f) \n\n", pEdgeMin, pEdgeMax, ampPeak, maxPeak);
      Double_t pini, pmin, pmax;
      f1.GetRange(pmin, pmax);
      UT::Printf(UT::kPrintDebug, "\t\t Fit Range [%f, %f] \n", pmin, pmax);
      for (Int_t ip = 0; ip < 3; ++ip) {
        pini = f1.GetParameter(ip);
        f1.GetParLimits(ip, pmin, pmax);
        UT::Printf(UT::kPrintDebug, "\t\t\t Parameter %d: %f in [%f, %f] \n", ip, pini, pmin, pmax);
      }

      TFitResultPtr r1 = h1->Fit(&f1, "RISQ");

      if (!r1->IsValid()) continue;
      if (f1.GetNDF() < minNbin - f1.GetNpar()) continue;
      if (f1.GetChisquare() / f1.GetNDF() > fChi2_1D / fNdof_1D) continue;

      // Check is a parameter is ** at limit **
      Bool_t atlimit = false;
      for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
        Double_t parval = f1.GetParameter(ip);
        Double_t parmin, parmax;
        f1.GetParLimits(ip, parmin, parmax);
        if (TMath::Abs(parval / parmin - 1) < 1e-3 || TMath::Abs(parval / parmax - 1) < 1e-3) atlimit = true;
      }
      if (atlimit) continue;

      UT::Printf(UT::kPrintDebug, "\t\t Best Range Update %f %f (%.0f/%d=%.0f against %.0f/%.0f=%.0f) \n", pEdgeMin,
                 pEdgeMax, f1.GetChisquare(), f1.GetNDF(), f1.GetChisquare() / f1.GetNDF(), fChi2_1D, fNdof_1D,
                 fChi2_1D / fNdof_1D);

      CopyToBestFit1D(f1, pEdgeMin, pEdgeMax);
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::SetUpFinalFit1D(TF1 &f1, Int_t view) {
  f1.SetRange(fFmin_1D, fFmax_1D);

  f1.SetParName(0, "A");
  f1.SetParName(1, view == 0 ? "x_{0}" : "y_{0}");
  f1.SetParName(2, "#sigma");

  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) f1.SetParameter(ip, fFPar_1D[ip]);

  f1.SetParLimits(0, .5 * fFPar_1D[0], 5. * fFPar_1D[0]);
  f1.SetParLimits(1, fFmin_1D, fFmax_1D);
  f1.SetParLimits(2, 2.5, 10.);

  UT::Printf(UT::kPrintDebug, "1D Parameters final initialization:\n");
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
    string parnam = f1.GetParName(ip);
    Double_t parval = f1.GetParameter(ip);
    Double_t parmin, parmax;
    f1.GetParLimits(ip, parmin, parmax);
    UT::Printf(UT::kPrintDebug, "\t%s : %f in [%f, %f]\n", parnam.c_str(), parval, parmin, parmax);
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Fit1DVertex(TH1D *h1, TF1 &f1, Int_t view) {
  UT::Printf(UT::kPrintInfo, "\n=== 1D FITTING: %s\n\n", h1->GetName());

  /*
   * Get information from histogram name
   */

  TString name = h1->GetName();

  Bool_t isRunByRun = false;
  if (name.Contains("Cumulative")) {
    isRunByRun = false;
  } else if (name.Contains("ByLayer")) {
    isRunByRun = false;
  } else if (name.Contains("RunByRun")) {
    isRunByRun = true;
  } else {
    UT::Printf(UT::kPrintError, "===             Attempting to fit nor Cumulative nor RunByRun! Exit...\n");
    exit(EXIT_FAILURE);
  }

  Int_t eneBin = -1;
  for (Int_t ie = 0; ie < fNEne; ++ie) {
    if (name.Contains(Form("%.0fGeV", fEnergyThr[ie]))) {
      eneBin = ie;
      break;
    }
  }
  if (eneBin < 0) {
    UT::Printf(UT::kPrintError, "===             Attempting to fit an absurd Energy Threshold! Exit...\n");
    exit(EXIT_FAILURE);
  }

  /*
   * Complex weighted fRebin scheme to avoid bad counts on tower edges
   */

  TH1D *hn = (TH1D *)(h1->Clone());
  hn->Reset();
  for (Int_t ibin = 1; ibin < hn->GetXaxis()->GetNbins() + 1; ibin++) {
    if (h1->GetBinContent(ibin) > 0) hn->SetBinContent(ibin, 1.);
  }
  hn->Rebin(fRebin);
  for (Int_t ibin = 1; ibin < hn->GetXaxis()->GetNbins() + 1; ibin++) {
    Double_t cont = hn->GetBinContent(ibin) / fRebin;
    hn->SetBinContent(ibin, cont);
  }
  h1->Rebin(fRebin);
  h1->Divide(hn);

  if (fOfficial) {  // For official canvas we mask edge bins
    for (Int_t ibin = 1; ibin < hn->GetXaxis()->GetNbins() + 1; ibin++) {
      if (hn->GetBinContent(ibin) != 1.) {
        h1->SetBinContent(ibin, 0.);
      }
    }
  }

  /*
   * Fill points to a vector and compute fit range
   */

  Int_t nbin = h1->GetXaxis()->GetNbins();

  // Position corresponding to maximum amplitude
  Double_t hAmpPeak = -1.;
  Double_t pMaxPeak = -1.;

  // Initial funmin-max range on small tower
  Double_t pEdgeMin = +1000;
  Double_t pEdgeMax = -1000;

  // Vector storing the information to be fitted
  vector<Double_t> vpos;
  vector<Double_t> vamp;

  vpos.clear();
  vamp.clear();

  hAmpPeak = h1->GetMaximum();
  Int_t binPmax = h1->GetMaximumBin();
  pMaxPeak = h1->GetXaxis()->GetBinCenter(binPmax);

  if (!fFixLimit || !isRunByRun) {  // If fitting cumulative or without fixed limits
    for (Int_t ibin = 1; ibin < nbin + 1; ibin++) {
      if (h1->GetBinContent(ibin) > 0) {
        vpos.push_back(h1->GetXaxis()->GetBinCenter(ibin));
        vamp.push_back(h1->GetBinContent(ibin));

        Double_t p_inf = h1->GetXaxis()->GetBinLowEdge(ibin);
        Double_t p_sup = h1->GetXaxis()->GetBinLowEdge(1 + ibin);

        if (view == 0) {
          if (p_inf >= xTowerInfEdge && p_inf < pEdgeMin) pEdgeMin = p_inf;
          if (p_sup <= xTowerSupEdge && p_sup > pEdgeMax) pEdgeMax = p_sup;
        }
        if (view == 1) {
          if (p_inf >= yTowerInfEdge && p_inf < pEdgeMin) pEdgeMin = p_inf;
          if (p_sup <= yTowerSupEdge && p_sup > pEdgeMax) pEdgeMax = p_sup;
        }
      }
    }
  } else {  // If fitting run-by-run with fixed limits
    fFit1D_Cumulative[view][eneBin].GetRange(pEdgeMin, pEdgeMax);
  }

  /*
   * Define fitting range
   */

  ClearBestPars1D();

  UT::Printf(UT::kPrintInfo, "===             Fit in [%.2f, %.2f] (%.0f entries)\n", pEdgeMin, pEdgeMax,
             h1->Integral(h1->FindBin(pEdgeMin), h1->FindBin(pEdgeMax)));

  if (fOptimize) {
    if (!fFixLimit || !isRunByRun) {  // Do not perform optimization if fitting run-by-run with fixed limits
      UT::Printf(UT::kPrintInfo, "===             \t Optimization\n\n");
      OptimizeRange1D(h1, view, pEdgeMin, pEdgeMax, pMaxPeak, hAmpPeak);
    }
  }

  f1 = TF1(Form("Fit_%s", h1->GetName()), "gaus", pEdgeMin, pEdgeMax);

  if (fNdof_1D == 1. || fChi2_1D == 1e12) {  // If no optimization was performed
    UT::Printf(UT::kPrintInfo, "===             \t Fit\n");
    InitializeFit1D(f1, view, pEdgeMin, pEdgeMax, pMaxPeak, hAmpPeak);
  } else {  // If optimization was successfully performed
    UT::Printf(UT::kPrintInfo, "===             \t Final fit in [%.2f, %.2f] (%.0f entries)\n", fFmin_1D, fFmax_1D,
               h1->Integral(h1->FindBin(fFmin_1D), h1->FindBin(fFmax_1D)));
    SetUpFinalFit1D(f1, view);
  }

  /*
   * Fit
   */

  TFitResultPtr r1 = h1->Fit(&f1, "RIES");

  if (!r1->IsValid()) {
    UT::Printf(UT::kPrintInfo, "===             \t\t Valid=NO! Exit...\n");
    // exit(EXIT_FAILURE);  // TODO: Handle this case...
  }
  UT::Printf(UT::kPrintInfo, "\t\t Valid=YES\n");
  UT::Printf(UT::kPrintInfo, "  ChiSquare = %f with Ndof = %d\n", f1.GetChisquare(), f1.GetNDF());
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip)
    UT::Printf(UT::kPrintInfo, "   %s = %f ± %f\n", f1.GetParName(ip), f1.GetParameter(ip), f1.GetParError(ip));

  /*
   * Save information to output files
   */

  fOfstream << endl;
  fOfstream << "1D Histo"
            << "\t\t\t"
            << "valid"
            << "\t"
            << "chi"
            << "\t\t"
            << "ndof"
            << "\t"
            << "A"
            << "\t\t" << (view == 0 ? "x0" : "y0") << "\t" << (view == 0 ? "Δx0" : "Δy0") << "\t"
            << "sigma" << endl;
  fOfstream << h1->GetTitle() << "\t" << f1.IsValid() << "\t" << fixed << setprecision(3) << f1.GetChisquare() << "\t\t"
            << f1.GetNDF() << "\t" << f1.GetParameter(0) << "\t" << f1.GetParameter(1) << "\t" << f1.GetParError(1)
            << "\t" << f1.GetParameter(2) << endl;
}

/*--------------------------*/
/*--- Fit Draw functions ---*/
/*--------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::CreatePaveFit(TCanvas *c2, TH2D *h2, TF2 &f2) {
  TPaveStats *pave = (TPaveStats *)c2->GetPrimitive("stats");
  if (pave == NULL) return;

  pave->SetName("ThisCommandIsNecessaryToWork");
  TList *listOfLines = pave->GetListOfLines();
  TText *dummy = pave->GetLineWith("Entries");
  listOfLines->Remove(dummy);

  vector<TString> ts = {Form("#chi^{2} / ndf = %.2g / %d", f2.GetChisquare(), f2.GetNDF()),
                        Form("Prob = %.2g", f2.GetProb())};
  for (Int_t ip = 0; ip < f2.GetNpar(); ++ip)
    ts.push_back(Form("%s = %.2g #pm %.2g", f2.GetParName(ip), f2.GetParameter(ip), f2.GetParError(ip)));

  vector<TLatex *> tl = vector<TLatex *>();
  for (Int_t is = 0; is < ts.size(); ++is) {
    tl.push_back(new TLatex(0, 0, ts[is].Data()));
    tl[is]->SetTextSize(0.025);
    tl[is]->SetTextFont(42);
    listOfLines->Add(tl[is]);
  }

  h2->SetStats(0);

  if (this->kArmIndex == 0) {  // TODO: Find best choord1inates for Arm1
    pave->SetX1NDC(0.125);
    pave->SetX2NDC(0.450);
    pave->SetY1NDC(fGaussian ? 0.535 : 0.585);
    pave->SetY2NDC(0.885);
  } else {
    pave->SetX1NDC(0.125);
    pave->SetX2NDC(0.450);
    pave->SetY1NDC(fGaussian ? 0.535 : 0.585);
    pave->SetY2NDC(0.885);
  }

  c2->Modified();
  c2->Update();

  gPad->Modified();
  gPad->Update();
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::CreatePavePro(TCanvas *c2, TF2 &f2, TH1D &hp, TGraph &gr, Int_t view) {
  Double_t range[2][2];  // Axis range[x/y][min/max]
  f2.GetRange(range[0][0], range[1][0], range[0][1], range[1][1]);

  Double_t chi2 = 0.;
  Double_t ndof = 0.;
  Double_t prob = 0.;
  for (Int_t ibin = 1; ibin <= hp.GetXaxis()->GetNbins(); ++ibin) {
    Double_t center = hp.GetXaxis()->GetBinCenter(ibin);
    if (center < range[view][0] || center > range[view][1]) {
      continue;
    }
    Double_t value = hp.GetBinContent(ibin);
    Double_t error = hp.GetBinError(ibin);
    Double_t graph = gr.Eval(center);
    chi2 += TMath::Power((graph - value) / error, 2);
    ++ndof;
  }
  chi2 = chi2;
  ndof = ndof - (f2.GetNpar() - 1);  // From 2D to 1D we loose one parameter
  prob = TMath::Prob(chi2, ndof);

  gPad->Update();

  TPaveStats *pave = (TPaveStats *)gPad->GetPrimitive("stats");
  if (pave == NULL) return;

  pave->SetName("ThisCommandIsNecessaryToWork");
  TList *listOfLines = pave->GetListOfLines();
  TText *dummy = pave->GetLineWith("Entries");
  listOfLines->Remove(dummy);

  vector<TString> ts = {Form("#chi^{2} / ndf = %.2g / %.0f", chi2, ndof), Form("Prob = %.2g", prob),
                        Form("A = %.2g #pm %.2g", f2.GetParameter(0), f2.GetParError(0))};
  if (view == 0) {
    TString cname = "x_{0}";
    TString wname = fGaussian ? "#sigma_{x}" : "#sigma";
    ts.push_back(Form("%s = %.2g #pm %.2g", cname.Data(), f2.GetParameter(cname),
                      f2.GetParError(f2.GetParNumber(cname.Data()))));
    ts.push_back(Form("%s = %.2g #pm %.2g", wname.Data(), f2.GetParameter(wname.Data()),
                      f2.GetParError(f2.GetParNumber(wname.Data()))));
  } else if (view == 1) {
    TString cname = "y_{0}";
    TString wname = fGaussian ? "#sigma_{y}" : "#sigma";
    ts.push_back(Form("%s = %.2g #pm %.2g", cname.Data(), f2.GetParameter(cname.Data()),
                      f2.GetParError(f2.GetParNumber(cname.Data()))));
    ts.push_back(Form("%s = %.2g #pm %.2g", wname.Data(), f2.GetParameter(wname.Data()),
                      f2.GetParError(f2.GetParNumber(wname.Data()))));
  } else {
    UT::Printf(UT::kPrintError, "Wrong index in projection! Exit...\n");
    exit(EXIT_FAILURE);
  }

  vector<TLatex *> tl = vector<TLatex *>();
  for (Int_t is = 0; is < ts.size(); ++is) {
    tl.push_back(new TLatex(0, 0, ts[is].Data()));
    tl[is]->SetTextSize(0.029);
    tl[is]->SetTextFont(42);
    listOfLines->Add(tl[is]);
  }

  hp.SetStats(0);

  if (this->kArmIndex == 0) {  // TODO: Find best choord1inates for Arm1
    pave->SetX1NDC(0.59);
    pave->SetX2NDC(0.95);
    pave->SetY1NDC(0.55);
    pave->SetY2NDC(0.85);
  } else {
    pave->SetX1NDC(0.59);
    pave->SetX2NDC(0.95);
    pave->SetY1NDC(0.55);
    pave->SetY2NDC(0.85);
  }

  c2->Modified();
  c2->Update();

  gPad->Modified();
  gPad->Update();
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Draw2DFit(TH2D *h2, TF2 &f2) {
  /*
   * Draw the fit on the distribution
   */

  gStyle->SetOptStat("e");
  // gStyle->SetOptStat(0);
  // gStyle->SetOptFit(1111);
  gStyle->SetTitleFontSize(0.065);

  gStyle->SetStatX(0.05);
  gStyle->SetStatY(0.85);
  gStyle->SetStatH(0.2);

  gStyle->SetFitFormat("3.3g");

  h2->Draw("colz");
  h2->GetXaxis()->SetTitleOffset(1.);
  h2->GetYaxis()->SetTitleOffset(1.);
  f2.Draw("same");

  gPad->Update();

  TPaletteAxis *palette = (TPaletteAxis *)h2->GetListOfFunctions()->FindObject("palette");
  palette->SetX1NDC(0.90);
  palette->SetX2NDC(0.93);

  const Double_t xMaxPeak = h2->GetXaxis()->GetBinCenter(h2->ProjectionX()->GetMaximumBin());
  const Double_t yMaxPeak = h2->GetYaxis()->GetBinCenter(h2->ProjectionY()->GetMaximumBin());

  const Double_t xFitPeak = f2.GetParameter("x_{0}");
  const Double_t yFitPeak = f2.GetParameter("y_{0}");

  TMarker marker1;
  marker1.SetMarkerStyle(29);
  marker1.SetMarkerColor(0);
  marker1.SetMarkerSize(2);
  marker1.DrawMarker(xMaxPeak, yMaxPeak);

  TMarker marker2;
  marker2.SetMarkerStyle(33);
  marker2.SetMarkerColor(1);
  marker2.SetMarkerSize(2);
  marker2.DrawMarker(xFitPeak, yFitPeak);

  TLine *l1 = new TLine(-30, 0, 50, 0);
  TLine *ly = new TLine(0, -20, 0, 80);
  l1->SetLineStyle(9);
  l1->SetLineWidth(2);
  l1->SetLineColor(39);
  ly->SetLineStyle(9);
  ly->SetLineWidth(2);
  ly->SetLineColor(39);

  TPave *st;
  TPave *lt;
  st = new TPave(-17, -12.5, +8, +12.5);
  lt = new TPave(+8. + 1.8, +12.5 + 1.8, +8. + 1.8 + 32., +12.5 + 1.8 + 32.);
  st->SetFillStyle(0);
  lt->SetFillStyle(0);
  st->Draw("same");
  lt->Draw("same");
  gPad->Update();

  if (fOfficial) {
    gStyle->SetOptStat("");

    Double_t energy = 0.;
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      string name = h2->GetName();
      string subs = Form("%.0f", fEnergyThr[ie]);
      if (name.find(subs) != std::string::npos) {
        energy = fEnergyThr[ie];
        break;
      }
    }

    h2->SetTitle("");
    if (this->kArmIndex == Arm1Params::kArmIndex) {  // TODO for Arm1
      TLatex *tex;
      tex = new TLatex(-14, 34, "#bf{#bf{#it{LHCf-Arm1}}}");
      tex->SetTextSize(0.05);
      tex->Draw();
      if (this->fOperation == OpType::kLHC2025)
        tex = new TLatex(-16, 30, "#bf{#it{p-O} @ #sqrt{s_{NN}} = XX.X TeV}");
      else if (this->fOperation == OpType::kLHC2022)
        tex = new TLatex(-16, 30, "#bf{#it{p-p} @ #sqrt{s} = 13.6 TeV}");
      else
        tex = new TLatex(-16, 30, "");
      tex->SetTextSize(0.04);
      tex->Draw();
      tex = new TLatex(-15.75, 26, Form("#bf{Hadron-like > %.0f TeV}", energy / 1000.));
      tex->SetTextSize(0.04);
      tex->Draw();
    } else if (this->kArmIndex == Arm2Params::kArmIndex) {
      TLatex *tex;
      tex = new TLatex(-14, 34, "#bf{#bf{#it{LHCf-Arm2}}}");
      tex->SetTextSize(0.05);
      tex->Draw();
      if (this->fOperation == OpType::kLHC2025)
        tex = new TLatex(-16, 30, "#bf{#it{p-O} @ #sqrt{s_{NN}} = XX.X TeV}");
      else if (this->fOperation == OpType::kLHC2022)
        tex = new TLatex(-16, 30, "#bf{#it{p-p} @ #sqrt{s} = 13.6 TeV}");
      else
        tex = new TLatex(-16, 30, "");
      tex->SetTextSize(0.04);
      tex->Draw();
      tex = new TLatex(-15.75, 26, Form("#bf{Hadron-like > %.0f TeV}", energy / 1000.));
      tex->SetTextSize(0.04);
      tex->Draw();
    }
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Draw2DFitProjection(TH2D *h2, TF2 &f2, TH1D &hp, TGraph &gr, Int_t view) {
  /*
   * Set desired graphics options
   */

  gStyle->SetOptStat("e");
  // gStyle->SetOptFit(0);
  gStyle->SetTitleFontSize(0.065);

  gStyle->SetStatX(0.95);
  gStyle->SetStatY(0.85);
  gStyle->SetStatH(0.2);

  gStyle->SetFitFormat("3.3g");

  /*
   * Retrieve Axis and Range
   */

  Int_t weiv = 1 - view;    // Complementary axis
  Double_t epsilon = 1e-3;  // Integration step
  Double_t range[2][2];     // Axis range[x/y][min/max]
  f2.GetRange(range[0][0], range[1][0], range[0][1], range[1][1]);

  /*
   * Project histogram
   */

  if (view == 0) {
    TString name = Form("%s_projectionX", h2->GetName());
    Int_t bmin = h2->GetYaxis()->FindBin(range[1][0] + 1e-6);
    Int_t bmax = h2->GetYaxis()->FindBin(range[1][1] - 1e-6);
    hp = *((TH1D *)h2->ProjectionX(name.Data(), bmin, bmax, "e"));
  } else if (view == 1) {
    TString name = Form("%s_projectionY", h2->GetName());
    Int_t bmin = h2->GetXaxis()->FindBin(range[0][0] + 1e-6);
    Int_t bmax = h2->GetXaxis()->FindBin(range[0][1] - 1e-6);
    hp = *((TH1D *)h2->ProjectionY(name.Data(), bmin, bmax, "e"));
  } else {
    UT::Printf(UT::kPrintError, "Wrong index in projection! Exit...\n");
    exit(EXIT_FAILURE);
  }
  hp.SetMarkerColor(kRed);
  hp.SetLineColor(kRed);

  /*
   * Project function
   */

  gr = TGraph(0);
  gr.SetName(Form("%s_projection%s", f2.GetName(), view == 0 ? "X" : "Y"));
  Int_t nbin = (range[view][1] - range[view][0]) / epsilon;
  for (Int_t ibin = 0; ibin < nbin; ++ibin) {
    Double_t center = range[view][0] + (ibin + 0.5) * epsilon;
    Double_t vmin = center - 0.5 * epsilon;
    Double_t vmax = center + 0.5 * epsilon;
    Double_t wmin = range[weiv][0];
    Double_t wmax = range[weiv][1];
    Double_t value = 0.;
    if (view == 0) {
      Double_t width = h2->GetXaxis()->GetBinWidth(h2->GetXaxis()->FindBin(0.5 * (range[view][0] + range[view][1])));
      value = f2.Integral(vmin, vmax, wmin, wmax, epsilon);
      value /= width * epsilon;
    } else {
      Double_t width = h2->GetYaxis()->GetBinWidth(h2->GetYaxis()->FindBin(0.5 * (range[view][0] + range[view][1])));
      value = f2.Integral(wmin, wmax, vmin, vmax, epsilon);
      value /= width * epsilon;
    }
    gr.SetPoint(ibin, center, value);
  }
  gr.SetMarkerColor(kGreen);
  gr.SetLineColor(kGreen);

  /*
   * Draw projection
   */

  hp.Draw("P");
  hp.SetMinimum(0.);
  gr.Draw("LSAME");

  gPad->Update();

  TLine *l1 = new TLine(0, 0, 0, gPad->GetUymax());
  l1->SetLineStyle(9);
  l1->SetLineWidth(2);
  l1->SetLineColor(39);
  l1->Draw();
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Draw1DFit(TH1D *h1, TF1 &f1, Int_t view) {
  /*
   * Draw the fit on the distribution
   */

  // gStyle->SetOptStat("mr");
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1111);
  gStyle->SetTitleFontSize(0.065);

  gStyle->SetStatX(0.95);
  gStyle->SetStatY(0.85);
  gStyle->SetStatH(0.20);

  gStyle->SetFitFormat("3.3g");

  h1->Draw();
  h1->SetMinimum(0.);
  f1.Draw("SAME");

  gPad->Update();

  TLine *l1 = new TLine(0, 0, 0, gPad->GetUymax());
  l1->SetLineStyle(9);
  l1->SetLineWidth(2);
  l1->SetLineColor(39);
  l1->Draw();
}

/*--------------------------*/
/*--- Fit Draw functions ---*/
/*--------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create2DOverEnergy() {
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    fGraph2D_Cumulative[iv] = new TGraphErrors();
    fGraph2D_Cumulative[iv]->SetName(Form("Graph2D_Cumulative_%s", iv == 0 ? "x0" : "y0"));
    fGraph2D_Cumulative[iv]->SetTitle(
        Form("Run %d-%d; > Energy [GeV]; %s [mm];", fFirstRun, fLastRun, iv == 0 ? "x_{0}" : "y_{0}"));

    Int_t ip = 0;
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      const Double_t thr = fEnergyThr[ie];
      const Double_t pos = fFit2D_Cumulative[ie].GetParameter(iv == 0 ? "x_{0}" : "y_{0}");
      const Double_t err =
          fFit2D_Cumulative[ie].GetParError(fFit2D_Cumulative[ie].GetParNumber(iv == 0 ? "x_{0}" : "y_{0}"));

      fGraph2D_Cumulative[iv]->SetPoint(ip, thr, pos);
      fGraph2D_Cumulative[iv]->SetPointError(ip, 0, err);

      ++ip;
    }

    fGraph2D_Cumulative[iv]->SetMarkerStyle(kFullCircle);
    fGraph2D_Cumulative[iv]->SetMarkerColor(kBlue);
    fGraph2D_Cumulative[iv]->SetMarkerSize(2);
    fGraph2D_Cumulative[iv]->SetLineStyle(1);
    fGraph2D_Cumulative[iv]->SetLineWidth(2);
    fGraph2D_Cumulative[iv]->SetLineColor(kBlue);

    if (fSummary_Cumulative[iv] == NULL) fSummary_Cumulative[iv] = new TMultiGraph();
    fSummary_Cumulative[iv]->Add(fGraph2D_Cumulative[iv]);

    if (fLegend_Cumulative[iv] == NULL) fLegend_Cumulative[iv] = new TLegend(0.905, 0.375, 1.000, 0.625);
    fLegend_Cumulative[iv]->AddEntry(fGraph2D_Cumulative[iv], "#scale[2.0]{Fit 2D}", "lp");
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create1DOverEnergy() {
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    fGraph1D_Cumulative[iv] = new TGraphErrors();
    fGraph1D_Cumulative[iv]->SetName(Form("Graph1D_Cumulative_%s", iv == 0 ? "x0" : "y0"));
    fGraph1D_Cumulative[iv]->SetTitle(
        Form("Run %d-%d; > Energy [GeV]; %s [mm];", fFirstRun, fLastRun, iv == 0 ? "x_{0}" : "y_{0}"));

    Int_t ip = 0;
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      const Double_t thr = fEnergyThr[ie];
      const Double_t pos = fFit1D_Cumulative[iv][ie].GetParameter(1);
      const Double_t err = fFit1D_Cumulative[iv][ie].GetParError(1);

      fGraph1D_Cumulative[iv]->SetPoint(ip, thr, pos);
      fGraph1D_Cumulative[iv]->SetPointError(ip, 0, err);

      ++ip;
    }

    fGraph1D_Cumulative[iv]->SetMarkerStyle(kFullCircle);
    fGraph1D_Cumulative[iv]->SetMarkerColor(kRed);
    fGraph1D_Cumulative[iv]->SetMarkerSize(2);
    fGraph1D_Cumulative[iv]->SetLineStyle(1);
    fGraph1D_Cumulative[iv]->SetLineWidth(2);
    fGraph1D_Cumulative[iv]->SetLineColor(kRed);

    if (fSummary_Cumulative[iv] == NULL) fSummary_Cumulative[iv] = new TMultiGraph();
    fSummary_Cumulative[iv]->Add(fGraph1D_Cumulative[iv]);

    if (fLegend_Cumulative[iv] == NULL) fLegend_Cumulative[iv] = new TLegend(0.905, 0.375, 1.000, 0.625);
    fLegend_Cumulative[iv]->AddEntry(fGraph1D_Cumulative[iv], "#scale[2.0]{Fit 1D}", "lp");
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::ProjectCumulative2D() {
  Utils::AllocateVector2D(fHistoPro2D_Cumulative, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fFitPro2D_Cumulative, this->kPosNview, fNEne);

  //	  Utils::ClearVector2D(fHistoPro2D_Cumulative, NULL);
  //	  Utils::ClearVector2D(fFitPro2D_Cumulative, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    if (fHisto2D_Cumulative[ie] == NULL) continue;

    TString canvasName =
        Form("%s/Pro2D_Cumulative_%d_%d_%.0fGeV.eps", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("2D Vertex Projection", "", 1200, 600);

    c->Divide(2, 1);
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      c->cd(iv + 1);
      Draw2DFitProjection(fHisto2D_Cumulative[ie], fFit2D_Cumulative[ie], fHistoPro2D_Cumulative[iv][ie],
                          fFitPro2D_Cumulative[iv][ie], iv);
      CreatePavePro(c, fFit2D_Cumulative[ie], fHistoPro2D_Cumulative[iv][ie], fFitPro2D_Cumulative[iv][ie], iv);
    }
    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::ProjectRunbyRun2D() {
  Utils::AllocateVector3D(fHistoPro2D_RunByRun, fNRun, this->kPosNview, fNEne);
  Utils::AllocateVector3D(fFitPro2D_RunByRun, fNRun, this->kPosNview, fNEne);

  // Utils::ClearVector3D(fHistoPro2D_RunByRun, NULL);
  // Utils::ClearVector3D(fFitPro2D_RunByRun, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    TString canvasName =
        Form("%s/Pro2D_RunByRun_%d_%d_%.0fGeV.pdf", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("2D Vertex Projection", "", 1200, 600);
    c->Print(Form("%s[", canvasName.Data()));

    c->Divide(2, 1);
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (fHisto2D_RunByRun[ir][ie] == NULL) continue;

      c->cd();
      c->Update();
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        c->cd(iv + 1);
        Draw2DFitProjection(fHisto2D_RunByRun[ir][ie], fFit2D_RunByRun[ir][ie], fHistoPro2D_RunByRun[ir][iv][ie],
                            fFitPro2D_RunByRun[ir][iv][ie], iv);
        CreatePavePro(c, fFit2D_RunByRun[ir][ie], fHistoPro2D_RunByRun[ir][iv][ie], fFitPro2D_RunByRun[ir][iv][ie], iv);
      }
      c->Update();
      c->Print(Form("%s[", canvasName.Data()));
    }
    c->Print(Form("%s]", canvasName.Data()));
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::DrawCumulativeSummary() {
  Utils::AllocateVector1D(fGraph2D_Cumulative, this->kPosNview);
  Utils::AllocateVector1D(fGraph1D_Cumulative, this->kPosNview);

  Utils::AllocateVector1D(fSummary_Cumulative, this->kPosNview);
  Utils::AllocateVector1D(fLegend_Cumulative, this->kPosNview);

  // Utils::ClearVector1D(fGraph2D_Cumulative, NULL);
  // Utils::ClearVector1D(fGraph1D_Cumulative, NULL);

  // Utils::ClearVector1D(fSummary_Cumulative, NULL);
  // Utils::ClearVector1D(fLegend_Cumulative, NULL);

  if (fVertexXY) Create2DOverEnergy();
  if (fProjection) Create1DOverEnergy();

  TGaxis::SetMaxDigits(9);

  TString canvasName = Form("%s/Summary_Cumulative_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  TCanvas *c = new TCanvas("Summary", "", 1200, 600);
  c->Divide(1, 2);
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    c->cd(iv + 1);

    fLegend_Cumulative[iv]->SetTextSize(0.03);
    fSummary_Cumulative[iv]->SetTitle(
        Form("Run %d-%d; > Energy [GeV]; %s [mm];", fFirstRun, fLastRun, iv == 0 ? "x_{0}" : "y_{0}"));
    fSummary_Cumulative[iv]->Draw("AP");
    fLegend_Cumulative[iv]->Draw();

    gPad->Update();
    fSummary_Cumulative[iv]->GetXaxis()->SetTitleOffset(0.9);
    fSummary_Cumulative[iv]->GetXaxis()->SetTitleSize(0.06);
    fSummary_Cumulative[iv]->GetXaxis()->SetLabelSize(0.06);
    fSummary_Cumulative[iv]->GetYaxis()->SetTitleOffset(0.5);
    fSummary_Cumulative[iv]->GetYaxis()->SetTitleSize(0.06);
    fSummary_Cumulative[iv]->GetYaxis()->SetLabelSize(0.06);
    gPad->Update();
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->Update();
  }
  c->Print(canvasName.Data());
  c->Close();
  delete c;
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create2DOverAllRuns() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      const Double_t pos = fFit2D_Cumulative[ie].GetParameter(iv == 0 ? "x_{0}" : "y_{0}");
      const Double_t err =
          fFit2D_Cumulative[ie].GetParError(fFit2D_Cumulative[ie].GetParNumber(iv == 0 ? "x_{0}" : "y_{0}"));

      fLine2DFit_OverAllRuns[iv][ie] = new TLine();
      fLine2DInf_OverAllRuns[iv][ie] = new TLine();
      fLine2DSup_OverAllRuns[iv][ie] = new TLine();

      fLine2DFit_OverAllRuns[iv][ie]->SetX1(fFirstRun - 1);
      fLine2DFit_OverAllRuns[iv][ie]->SetX2(fLastRun + 1);
      fLine2DInf_OverAllRuns[iv][ie]->SetX1(fFirstRun - 1);
      fLine2DInf_OverAllRuns[iv][ie]->SetX2(fLastRun + 1);
      fLine2DSup_OverAllRuns[iv][ie]->SetX1(fFirstRun - 1);
      fLine2DSup_OverAllRuns[iv][ie]->SetX2(fLastRun + 1);

      fLine2DFit_OverAllRuns[iv][ie]->SetY1(pos);
      fLine2DFit_OverAllRuns[iv][ie]->SetY2(pos);
      fLine2DInf_OverAllRuns[iv][ie]->SetY1(pos - err);
      fLine2DInf_OverAllRuns[iv][ie]->SetY2(pos - err);
      fLine2DSup_OverAllRuns[iv][ie]->SetY1(pos + err);
      fLine2DSup_OverAllRuns[iv][ie]->SetY2(pos + err);

      fLine2DFit_OverAllRuns[iv][ie]->SetLineStyle(1);
      fLine2DInf_OverAllRuns[iv][ie]->SetLineStyle(2);
      fLine2DSup_OverAllRuns[iv][ie]->SetLineStyle(2);

      fLine2DFit_OverAllRuns[iv][ie]->SetLineWidth(1);
      fLine2DInf_OverAllRuns[iv][ie]->SetLineWidth(1);
      fLine2DSup_OverAllRuns[iv][ie]->SetLineWidth(1);

      fLine2DFit_OverAllRuns[iv][ie]->SetLineColor(kBlue);
      fLine2DInf_OverAllRuns[iv][ie]->SetLineColor(kBlue);
      fLine2DSup_OverAllRuns[iv][ie]->SetLineColor(kBlue);
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create1DOverAllRuns() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      const Double_t pos = fFit1D_Cumulative[iv][ie].GetParameter(1);
      const Double_t err = fFit1D_Cumulative[iv][ie].GetParError(1);

      fLine1DFit_OverAllRuns[iv][ie] = new TLine();
      fLine1DInf_OverAllRuns[iv][ie] = new TLine();
      fLine1DSup_OverAllRuns[iv][ie] = new TLine();

      fLine1DFit_OverAllRuns[iv][ie]->SetX1(fFirstRun - 1);
      fLine1DFit_OverAllRuns[iv][ie]->SetX2(fLastRun + 1);
      fLine1DInf_OverAllRuns[iv][ie]->SetX1(fFirstRun - 1);
      fLine1DInf_OverAllRuns[iv][ie]->SetX2(fLastRun + 1);
      fLine1DSup_OverAllRuns[iv][ie]->SetX1(fFirstRun - 1);
      fLine1DSup_OverAllRuns[iv][ie]->SetX2(fLastRun + 1);

      fLine1DFit_OverAllRuns[iv][ie]->SetY1(pos);
      fLine1DFit_OverAllRuns[iv][ie]->SetY2(pos);
      fLine1DInf_OverAllRuns[iv][ie]->SetY1(pos - err);
      fLine1DInf_OverAllRuns[iv][ie]->SetY2(pos - err);
      fLine1DSup_OverAllRuns[iv][ie]->SetY1(pos + err);
      fLine1DSup_OverAllRuns[iv][ie]->SetY2(pos + err);

      fLine1DFit_OverAllRuns[iv][ie]->SetLineStyle(1);
      fLine1DInf_OverAllRuns[iv][ie]->SetLineStyle(2);
      fLine1DSup_OverAllRuns[iv][ie]->SetLineStyle(2);

      fLine1DFit_OverAllRuns[iv][ie]->SetLineWidth(1);
      fLine1DInf_OverAllRuns[iv][ie]->SetLineWidth(1);
      fLine1DSup_OverAllRuns[iv][ie]->SetLineWidth(1);

      fLine1DFit_OverAllRuns[iv][ie]->SetLineColor(kRed);
      fLine1DInf_OverAllRuns[iv][ie]->SetLineColor(kRed);
      fLine1DSup_OverAllRuns[iv][ie]->SetLineColor(kRed);
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create2DRunByRunTrend() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      fGraph2D_RunByRun[iv][ie] = new TGraphErrors();
      fGraph2D_RunByRun[iv][ie]->SetName(Form("Graph2D_RunByRun_%s_%.0fGeV", iv == 0 ? "x0" : "y0", fEnergyThr[ie]));
      fGraph2D_RunByRun[iv][ie]->SetTitle(
          Form("E >%.0fGeV ; Run; %s [mm];", fEnergyThr[ie], iv == 0 ? "x_{0}" : "y_{0}"));

      Int_t ip = 0;
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto2D_RunByRun[ir][ie] == NULL) continue;

        const Double_t run = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
        const Double_t pos = fFit2D_RunByRun[ir][ie].GetParameter(iv == 0 ? "x_{0}" : "y_{0}");
        const Double_t err =
            fFit2D_RunByRun[ir][ie].GetParError(fFit2D_RunByRun[ir][ie].GetParNumber(iv == 0 ? "x_{0}" : "y_{0}"));

        fGraph2D_RunByRun[iv][ie]->SetPoint(ip, run, pos);
        fGraph2D_RunByRun[iv][ie]->SetPointError(ip, 0, err);

        ++ip;
      }

      fGraph2D_RunByRun[iv][ie]->SetMarkerStyle(23);
      fGraph2D_RunByRun[iv][ie]->SetMarkerColor(kAzure + 1);
      fGraph2D_RunByRun[iv][ie]->SetMarkerSize(2);
      fGraph2D_RunByRun[iv][ie]->SetLineStyle(1);
      fGraph2D_RunByRun[iv][ie]->SetLineWidth(2);
      fGraph2D_RunByRun[iv][ie]->SetLineColor(kAzure + 1);

      if (fSummary_RunByRun[iv][ie] == NULL) fSummary_RunByRun[iv][ie] = new TMultiGraph();
      fSummary_RunByRun[iv][ie]->Add(fGraph2D_RunByRun[iv][ie]);

      if (fLegend_RunByRun[iv][ie] == NULL) fLegend_RunByRun[iv][ie] = new TLegend(0.905, 0.375, 1.000, 0.625);
      fLegend_RunByRun[iv][ie]->AddEntry(fGraph2D_RunByRun[iv][ie], "#scale[2.0]{Fit 2D}", "lp");
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create1DRunByRunTrend() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      fGraph1D_RunByRun[iv][ie] = new TGraphErrors();
      fGraph1D_RunByRun[iv][ie]->SetName(Form("Graph1D_RunByRun_%s_%.0fGeV", iv == 0 ? "x0" : "y0", fEnergyThr[ie]));
      fGraph1D_RunByRun[iv][ie]->SetTitle(
          Form("E >%.0fGeV ; Run; %s [mm];", fEnergyThr[ie], iv == 0 ? "x_{0}" : "y_{0}"));

      Int_t ip = 0;
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto1D_RunByRun[ir][iv][ie] == NULL) continue;

        const Double_t run = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
        const Double_t pos = fFit1D_RunByRun[ir][iv][ie].GetParameter(1);
        const Double_t err = fFit1D_RunByRun[ir][iv][ie].GetParError(1);

        fGraph1D_RunByRun[iv][ie]->SetPoint(ip, run, pos);
        fGraph1D_RunByRun[iv][ie]->SetPointError(ip, 0, err);

        ++ip;
      }

      fGraph1D_RunByRun[iv][ie]->SetMarkerStyle(22);
      fGraph1D_RunByRun[iv][ie]->SetMarkerColor(kOrange + 10);
      fGraph1D_RunByRun[iv][ie]->SetMarkerSize(2);
      fGraph1D_RunByRun[iv][ie]->SetLineStyle(1);
      fGraph1D_RunByRun[iv][ie]->SetLineWidth(2);
      fGraph1D_RunByRun[iv][ie]->SetLineColor(kOrange + 10);

      if (fSummary_RunByRun[iv][ie] == NULL) fSummary_RunByRun[iv][ie] = new TMultiGraph();
      fSummary_RunByRun[iv][ie]->Add(fGraph1D_RunByRun[iv][ie]);

      if (fLegend_RunByRun[iv][ie] == NULL) fLegend_RunByRun[iv][ie] = new TLegend(0.905, 0.375, 1.000, 0.625);
      fLegend_RunByRun[iv][ie]->AddEntry(fGraph1D_RunByRun[iv][ie], "#scale[2.0]{Fit 1D}", "lp");
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::DrawRunByRunSummary() {
  Utils::AllocateVector2D(fLine2DFit_OverAllRuns, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLine2DInf_OverAllRuns, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLine2DSup_OverAllRuns, this->kPosNview, fNEne);

  Utils::AllocateVector2D(fLine1DFit_OverAllRuns, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLine1DInf_OverAllRuns, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLine1DSup_OverAllRuns, this->kPosNview, fNEne);

  Utils::AllocateVector2D(fGraph2D_RunByRun, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fGraph1D_RunByRun, this->kPosNview, fNEne);

  Utils::AllocateVector2D(fSummary_RunByRun, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLegend_RunByRun, this->kPosNview, fNEne);

  // Utils::ClearVector2D(fLine2DFit_OverAllRuns, NULL);
  // Utils::ClearVector2D(fLine2DInf_OverAllRuns, NULL);
  // Utils::ClearVector2D(fLine2DSup_OverAllRuns, NULL);

  // Utils::ClearVector2D(fLine1DFit_OverAllRuns, NULL);
  // Utils::ClearVector2D(fLine1DInf_OverAllRuns, NULL);
  // Utils::ClearVector2D(fLine1DSup_OverAllRuns, NULL);

  // Utils::ClearVector2D(fGraph2D_RunByRun, NULL);
  // Utils::ClearVector2D(fGraph1D_RunByRun, NULL);

  // Utils::ClearVector2D(fSummary_RunByRun, NULL);
  // Utils::ClearVector2D(fLegend_RunByRun, NULL);

  if (fVertexXY) {
    if (fCumulative) Create2DOverAllRuns();
    if (fRunByRun) Create2DRunByRunTrend();
  }
  if (fProjection) {
    if (fCumulative) Create1DOverAllRuns();
    if (fRunByRun) Create1DRunByRunTrend();
  }

  TGaxis::SetMaxDigits(9);

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    TString canvasName =
        Form("%s/Summary_RunByRun_%d_%d_%.0fGeV.eps", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("Summary", "", 1200, 600);
    c->Divide(1, 2);
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      c->cd(iv + 1);
      fLegend_RunByRun[iv][ie]->SetTextSize(0.03);
      fSummary_RunByRun[iv][ie]->SetTitle(
          Form("E >%.0fGeV; Run; %s [mm];", fEnergyThr[ie], iv == 0 ? "x_{0}" : "y_{0}"));
      fSummary_RunByRun[iv][ie]->Draw("AP");
      fLegend_RunByRun[iv][ie]->Draw();
      if (fCumulative) {
        if (fVertexXY) {
          fLine2DFit_OverAllRuns[iv][ie]->Draw();
          fLine2DInf_OverAllRuns[iv][ie]->Draw();
          fLine2DSup_OverAllRuns[iv][ie]->Draw();
        }
        if (fProjection) {
          fLine1DFit_OverAllRuns[iv][ie]->Draw();
          fLine1DInf_OverAllRuns[iv][ie]->Draw();
          fLine1DSup_OverAllRuns[iv][ie]->Draw();
        }
      }
      gPad->Update();

      fSummary_RunByRun[iv][ie]->GetXaxis()->SetLimits(fFirstRun - 1, fLastRun + 1);
      fSummary_RunByRun[iv][ie]->GetXaxis()->SetTitleOffset(0.9);
      fSummary_RunByRun[iv][ie]->GetXaxis()->SetTitleSize(0.06);
      fSummary_RunByRun[iv][ie]->GetXaxis()->SetLabelSize(0.06);
      fSummary_RunByRun[iv][ie]->GetYaxis()->SetTitleOffset(0.5);
      fSummary_RunByRun[iv][ie]->GetYaxis()->SetTitleSize(0.06);
      fSummary_RunByRun[iv][ie]->GetYaxis()->SetLabelSize(0.06);
      gPad->Update();
      gPad->SetGridx();
      gPad->SetGridy();
      gPad->Update();
    }
    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::CreateComparisonByLayer() {
  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      fGraph1D_ByLayer[iv][ie] = new TGraphErrors();
      fGraph1D_ByLayer[iv][ie]->SetName(Form("Graph1D_ByLayer_%s_%.0fGeV", iv == 0 ? "x0" : "y0", fEnergyThr[ie]));
      fGraph1D_ByLayer[iv][ie]->SetTitle(Form("Run %d-%d - E >%.0fGeV; Layer; %s [mm];", fFirstRun, fLastRun,
                                              fEnergyThr[ie], iv == 0 ? "x_{0}" : "y_{0}"));

      Int_t ip = 0;
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        if (fHisto1D_ByLayer[il][iv][ie] == NULL) continue;

        const Double_t lay = il;
        const Double_t pos = fFit1D_ByLayer[il][iv][ie].GetParameter(1);
        const Double_t err = fFit1D_ByLayer[il][iv][ie].GetParError(1);

        fGraph1D_ByLayer[iv][ie]->SetPoint(ip, lay, pos);
        fGraph1D_ByLayer[iv][ie]->SetPointError(ip, 0, err);

        ++ip;
      }

      fGraph1D_ByLayer[iv][ie]->SetMarkerSize(2);
      if (ie == 0) {
        fGraph1D_ByLayer[iv][ie]->SetMarkerStyle(kFullCircle);
        fGraph1D_ByLayer[iv][ie]->SetMarkerColor(kCyan);
        fGraph1D_ByLayer[iv][ie]->SetLineColor(kCyan);
      } else if (ie == 1) {
        fGraph1D_ByLayer[iv][ie]->SetMarkerStyle(kFullSquare);
        fGraph1D_ByLayer[iv][ie]->SetMarkerColor(kMagenta);
        fGraph1D_ByLayer[iv][ie]->SetLineColor(kMagenta);
      } else {
        fGraph1D_ByLayer[iv][ie]->SetMarkerStyle(kFullTriangleUp);
        fGraph1D_ByLayer[iv][ie]->SetMarkerColor(kOrange);
        fGraph1D_ByLayer[iv][ie]->SetLineColor(kOrange);
      }
      fGraph1D_ByLayer[iv][ie]->SetLineWidth(2);

      if (fSummary_ByLayer[iv] == NULL) fSummary_ByLayer[iv] = new TMultiGraph();
      fSummary_ByLayer[iv]->Add(fGraph1D_ByLayer[iv][ie]);

      TString label = Form("#scale[2.0]{> %.0f GeV}", fEnergyThr[ie]);
      if (fLegend_ByLayer[iv] == NULL) fLegend_ByLayer[iv] = new TLegend(0.905, 0.325, 1.000, 0.675);
      fLegend_ByLayer[iv]->AddEntry(fGraph1D_ByLayer[iv][ie], label.Data(), "p");
      fLegend_ByLayer[iv]->SetTextSize(0.03);
    }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::DrawFitByLayer() {
  Utils::AllocateVector2D(fGraph1D_ByLayer, this->kPosNview, fNEne);

  Utils::AllocateVector1D(fSummary_ByLayer, this->kPosNview);
  Utils::AllocateVector1D(fLegend_ByLayer, this->kPosNview);

  // Utils::ClearVector2D(fGraph1D_ByLayer, NULL);

  // Utils::ClearVector1D(fSummary_ByLayer, NULL);
  // Utils::ClearVector1D(fLegend_ByLayer, NULL);

  CreateComparisonByLayer();

  Utils::AllocateVector2D(fLine1DFit_OverAllRuns, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLine1DInf_OverAllRuns, this->kPosNview, fNEne);
  Utils::AllocateVector2D(fLine1DSup_OverAllRuns, this->kPosNview, fNEne);

  Create1DOverAllRuns();

  TGaxis::SetMaxDigits(9);

  TString canvasName = Form("%s/Summary_ByLayer_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  TCanvas *c = new TCanvas("Summary", "", 1200, 600);
  c->Divide(1, 2);
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    c->cd(iv + 1);
    fSummary_ByLayer[iv]->SetTitle(
        Form("Run %d-%d; Layer; %s [mm];", fFirstRun, fLastRun, iv == 0 ? "x_{0}" : "y_{0}"));
    fSummary_ByLayer[iv]->Draw("AP");
    if (fCumulative) {
      if (fProjection) {
        for (Int_t ie = 0; ie < fNEne; ++ie) {
          fLine1DFit_OverAllRuns[iv][ie]->SetX1(-1);
          fLine1DFit_OverAllRuns[iv][ie]->SetX2(+4);
          fLine1DInf_OverAllRuns[iv][ie]->SetX1(-1);
          fLine1DInf_OverAllRuns[iv][ie]->SetX2(+4);
          fLine1DSup_OverAllRuns[iv][ie]->SetX1(-1);
          fLine1DSup_OverAllRuns[iv][ie]->SetX2(+4);
          if (ie == 0) {
            fLine1DFit_OverAllRuns[iv][ie]->SetLineColor(kAzure + 1);
            fLine1DInf_OverAllRuns[iv][ie]->SetLineColor(kAzure + 1);
            fLine1DSup_OverAllRuns[iv][ie]->SetLineColor(kAzure + 1);
          } else if (ie == 1) {
            fLine1DFit_OverAllRuns[iv][ie]->SetLineColor(kViolet + 1);
            fLine1DInf_OverAllRuns[iv][ie]->SetLineColor(kViolet + 1);
            fLine1DSup_OverAllRuns[iv][ie]->SetLineColor(kViolet + 1);
          } else {
            fLine1DFit_OverAllRuns[iv][ie]->SetLineColor(kOrange + 1);
            fLine1DInf_OverAllRuns[iv][ie]->SetLineColor(kOrange + 1);
            fLine1DSup_OverAllRuns[iv][ie]->SetLineColor(kOrange + 1);
          }
          fLine1DFit_OverAllRuns[iv][ie]->Draw();
          fLine1DInf_OverAllRuns[iv][ie]->Draw();
          fLine1DSup_OverAllRuns[iv][ie]->Draw();
        }
      }
    }
    gPad->Update();
    fSummary_ByLayer[iv]->GetXaxis()->SetLimits(-1, +4);
    fSummary_ByLayer[iv]->GetXaxis()->SetTitleOffset(0.9);
    fSummary_ByLayer[iv]->GetXaxis()->SetTitleSize(0.06);
    fSummary_ByLayer[iv]->GetXaxis()->SetLabelSize(0.06);
    fSummary_ByLayer[iv]->GetYaxis()->SetTitleOffset(0.5);
    fSummary_ByLayer[iv]->GetYaxis()->SetTitleSize(0.06);
    fSummary_ByLayer[iv]->GetYaxis()->SetLabelSize(0.06);
    fLegend_ByLayer[iv]->Draw();
    gPad->Update();
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->Update();
  }
  c->Print(canvasName.Data());
  c->Close();
  delete c;
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create2DDispersion() {
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      // Fill temporary center vector
      vector<Double_t> vCenter;
      vCenter.clear();
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto2D_RunByRun[ir][ie] == NULL) continue;
        Double_t center = fFit2D_RunByRun[ir][ie].GetParameter(iv == 0 ? "x_{0}" : "y_{0}");
        vCenter.push_back(center);
      }

      // Compute minimum/maximum values
      auto iter = std::minmax_element(vCenter.begin(), vCenter.end());
      Double_t vmin = *iter.first;
      Double_t vmax = *iter.second;

      // Compute histogram ranges
      Int_t rbin = 50;
      Double_t rdif = TMath::Max(1., vmax - vmin);
      Double_t rmin = vmin - 0.25 * rdif;
      Double_t rmax = vmax + 0.25 * rdif;
      string name = Form("Dispersion2D%s_%d_%d_%.0fGeV", iv == 0 ? "X" : "Y", fFirstRun, fLastRun, fEnergyThr[ie]);
      string title =
          Form("Run %d-%d - E >%.0fGeV; %s [mm];", fFirstRun, fLastRun, fEnergyThr[ie], iv == 0 ? "x_{0}" : "y_{0}");

      // Create center histogram
      fHisto2D_Dispersion[iv][ie] = new TH1D(name.c_str(), title.c_str(), rbin, rmin, rmax);
      fHisto2D_Dispersion[iv][ie]->SetMarkerStyle(23);
      fHisto2D_Dispersion[iv][ie]->SetMarkerColor(kAzure + 1);
      fHisto2D_Dispersion[iv][ie]->SetLineColor(kAzure + 1);

      // Fill histogram
      for (Int_t ic = 0; ic < vCenter.size(); ++ic) {
        fHisto2D_Dispersion[iv][ie]->Fill(vCenter[ic]);
      }
    }
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::DrawDispersionSummary2D() {
  Utils::AllocateVector2D(fHisto2D_Dispersion, this->kPosNview, fNEne);

  Create2DDispersion();

  TGaxis::SetMaxDigits(3);

  gStyle->SetOptStat("emuro");
  gStyle->SetStatX(0.9500);
  gStyle->SetStatY(0.8500);
  gStyle->SetStatH(0.1250);
  gStyle->SetStatW(0.3125);

  TString canvasName = Form("%s/Dispersion2D_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  TCanvas *c = new TCanvas("Dispersion 2D", "", 1200, 800);
  c->Divide(3, 2);
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      c->cd(iv * fNEne + ie + 1);
      fHisto2D_Dispersion[iv][ie]->Draw("HIST");
      gPad->Update();
      Double_t reference = fFit2D_Cumulative[ie].GetParameter(iv == 0 ? "x_{0}" : "y_{0}");
      ;
      TLine *lCenter = new TLine(reference, gPad->GetUymin(), reference, gPad->GetUymax());
      lCenter->SetLineStyle(1);
      lCenter->SetLineWidth(1);
      lCenter->SetLineColor(kBlue);
      lCenter->Draw();
    }
  }
  c->Print(canvasName.Data());
  c->Close();
  delete c;

  gStyle->SetOptStat(0);
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Create1DDispersion() {
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      // Fill temporary center vector
      vector<Double_t> vCenter;
      vCenter.clear();
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto1D_RunByRun[ir][iv][ie] == NULL) continue;
        Double_t center = fFit1D_RunByRun[ir][iv][ie].GetParameter(1);
        vCenter.push_back(center);
      }

      // Compute minimum/maximum values
      auto iter = std::minmax_element(vCenter.begin(), vCenter.end());
      Double_t vmin = *iter.first;
      Double_t vmax = *iter.second;

      // Compute histogram ranges
      Int_t rbin = 50;
      Double_t rdif = TMath::Max(1., vmax - vmin);
      Double_t rmin = vmin - 0.25 * rdif;
      Double_t rmax = vmax + 0.25 * rdif;
      string name = Form("Dispersion1D%s_%d_%d_%.0fGeV", iv == 0 ? "X" : "Y", fFirstRun, fLastRun, fEnergyThr[ie]);
      string title =
          Form("Run %d-%d - E >%.0fGeV; %s [mm];", fFirstRun, fLastRun, fEnergyThr[ie], iv == 0 ? "x_{0}" : "y_{0}");

      // Create center histogram
      fHisto1D_Dispersion[iv][ie] = new TH1D(name.c_str(), title.c_str(), rbin, rmin, rmax);
      fHisto1D_Dispersion[iv][ie]->SetMarkerStyle(11);
      fHisto1D_Dispersion[iv][ie]->SetMarkerColor(kOrange + 10);
      fHisto1D_Dispersion[iv][ie]->SetLineColor(kOrange + 10);

      // Fill histogram
      for (Int_t ic = 0; ic < vCenter.size(); ++ic) {
        fHisto1D_Dispersion[iv][ie]->Fill(vCenter[ic]);
      }
    }
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::DrawDispersionSummary1D() {
  Utils::AllocateVector2D(fHisto1D_Dispersion, this->kPosNview, fNEne);

  Create1DDispersion();

  TGaxis::SetMaxDigits(3);

  gStyle->SetOptStat("emuro");
  gStyle->SetStatX(0.9500);
  gStyle->SetStatY(0.8500);
  gStyle->SetStatH(0.1250);
  gStyle->SetStatW(0.3125);

  TString canvasName = Form("%s/Dispersion1D_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  TCanvas *c = new TCanvas("Dispersion 1D", "", 1200, 800);
  c->Divide(3, 2);
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    for (Int_t ie = 0; ie < fNEne; ++ie) {
      c->cd(iv * fNEne + ie + 1);
      fHisto1D_Dispersion[iv][ie]->Draw();
      gPad->Update();
      Double_t reference = fFit1D_Cumulative[iv][ie].GetParameter(1);
      TLine *lCenter = new TLine(reference, gPad->GetUymin(), reference, gPad->GetUymax());
      lCenter->SetLineStyle(1);
      lCenter->SetLineWidth(1);
      lCenter->SetLineColor(kRed);
      lCenter->Draw();
    }
  }
  c->Print(canvasName.Data());
  c->Close();
  delete c;

  gStyle->SetOptStat(0);
}

/*--------------------------*/
/*--- Handling functions ---*/
/*--------------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::FitCumulative2D() {
  Utils::AllocateVector1D(fFit2D_Cumulative, fNEne);

  // Utils::ClearVector1D(fFit2D_Cumulative, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    if (fHisto2D_Cumulative[ie] == NULL) continue;
    Fit2DVertex(fHisto2D_Cumulative[ie], fFit2D_Cumulative[ie]);
  }

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    if (fHisto2D_Cumulative[ie] == NULL) continue;

    TString canvasName =
        Form("%s/Fit2D_Cumulative_%d_%d_%.0fGeV.eps", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("2D Vertex Fit", "", 600, 600);
    Draw2DFit(fHisto2D_Cumulative[ie], fFit2D_Cumulative[ie]);
    CreatePaveFit(c, fHisto2D_Cumulative[ie], fFit2D_Cumulative[ie]);
    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::FitRunbyRun2D() {
  Utils::AllocateVector2D(fFit2D_RunByRun, fNRun, fNEne);

  // Utils::ClearVector2D(fFit2D_RunByRun, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (fHisto2D_RunByRun[ir][ie] == NULL) continue;
      Fit2DVertex(fHisto2D_RunByRun[ir][ie], fFit2D_RunByRun[ir][ie]);
    }

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    TString canvasName =
        Form("%s/Fit2D_RunByRun_%d_%d_%.0fGeV.pdf", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("2D Vertex Fit", "", 600, 600);
    c->Print(Form("%s[", canvasName.Data()));
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (fHisto2D_RunByRun[ir][ie] == NULL) continue;
      c->cd();
      c->Update();
      Draw2DFit(fHisto2D_RunByRun[ir][ie], fFit2D_RunByRun[ir][ie]);
      CreatePaveFit(c, fHisto2D_RunByRun[ir][ie], fFit2D_RunByRun[ir][ie]);
      c->Update();
      c->Print(Form("%s[", canvasName.Data()));
    }
    c->Print(Form("%s]", canvasName.Data()));
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::FitCumulative1D() {
  Utils::AllocateVector2D(fFit1D_Cumulative, this->kPosNview, fNEne);

  // Utils::ClearVector2D(fFit1D_Cumulative, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      if (fHisto1D_Cumulative[iv][ie] == NULL) continue;
      Fit1DVertex(fHisto1D_Cumulative[iv][ie], fFit1D_Cumulative[iv][ie], iv);
    }

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    TString canvasName =
        Form("%s/Fit1D_Cumulative_%d_%d_%.0fGeV.eps", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("1D Vertex Fit", "", 1200, 600);
    c->Divide(2, 1);
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      if (fHisto1D_Cumulative[iv][ie] == NULL) continue;

      c->cd(iv + 1);
      Draw1DFit(fHisto1D_Cumulative[iv][ie], fFit1D_Cumulative[iv][ie], iv);
      //      if(fDrawProj) {
      //          Draw2DFitProjection(fHisto2D_Cumulative[ie], fFit2D_Cumulative[ie], fHistoPro2D_Cumulative[ie],
      //          fFitPro2D_Cumulative[ie], iv);
      //      }
    }
    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::FitRunByRun1D() {
  Utils::AllocateVector3D(fFit1D_RunByRun, fNRun, this->kPosNview, fNEne);

  // Utils::ClearVector3D(fFit1D_RunByRun, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto1D_RunByRun[ir][iv][ie] == NULL) continue;
        Fit1DVertex(fHisto1D_RunByRun[ir][iv][ie], fFit1D_RunByRun[ir][iv][ie], iv);
      }

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    TString canvasName =
        Form("%s/Fit1D_RunByRun_%d_%d_%.0fGeV.pdf", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("1D Vertex Fit", "", 1200, 600);
    c->Print(Form("%s[", canvasName.Data()));
    c->Divide(2, 1);

    for (Int_t ir = 0; ir < fNRun; ++ir) {
      c->cd();
      c->Update();
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        if (fHisto1D_RunByRun[ir][iv][ie] == NULL) continue;

        c->cd(iv + 1);
        Draw1DFit(fHisto1D_RunByRun[ir][iv][ie], fFit1D_RunByRun[ir][iv][ie], iv);
        //        if(fDrawProj) {
        //            Draw2DFitProjection(fHisto2D_RunByRun[ir][ie], fFit2D_RunByRun[ir][ie],
        //            fHistoPro2D_RunByRun[ir][ie], fFitPro2D_RunByRun[ir][ie], iv);
        //        }
      }
      c->Update();
      c->Print(Form("%s[", canvasName.Data()));
    }
    c->Print(Form("%s]", canvasName.Data()));
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::FitByLayer()  // TODO: Implementation is not finalized!
{
  Utils::AllocateVector3D(fFit1D_ByLayer, this->kPosNlayer, this->kPosNview, fNEne);

  // Utils::ClearVector3D(fFit1D_ByLayer, NULL);

  for (Int_t ie = 0; ie < fNEne; ++ie)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        if (fHisto1D_ByLayer[il][iv][ie] == NULL) continue;
        Fit1DVertex(fHisto1D_ByLayer[il][iv][ie], fFit1D_ByLayer[il][iv][ie], iv);
      }

  for (Int_t ie = 0; ie < fNEne; ++ie) {
    TString canvasName =
        Form("%s/Fit1D_ByLayer_%d_%d_%.0fGeV.pdf", fOutputDirName.Data(), fFirstRun, fLastRun, fEnergyThr[ie]);
    TCanvas *c = new TCanvas("1D Vertex Fit", "", 1200, 600);
    c->Print(Form("%s[", canvasName.Data()));
    c->Divide(2, 1);
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      c->cd();
      c->Update();
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        if (fHisto1D_ByLayer[il][iv][ie] == NULL) continue;

        c->cd(iv + 1);
        Draw1DFit(fHisto1D_ByLayer[il][iv][ie], fFit1D_ByLayer[il][iv][ie], iv);
      }
      c->Update();
      c->Print(Form("%s[", canvasName.Data()));
    }
    c->Print(Form("%s]", canvasName.Data()));
    c->Close();
    delete c;
  }
}

/*------------------*/
/*--- Class Core ---*/
/*------------------*/
template <typename arman, typename armclass>
void BeamCenterFit<arman, armclass>::Run() {
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
  ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(0);
  // gErrorIgnoreLevel = kFatal;

  GetInputHistograms();

  OpenFiles();

  SetHistoToBeFitted();
  SetUpCanvasStyle();
  SetTowerRange();

  if (fProjection) {
    if (fCumulative) FitCumulative1D();
    if (fRunByRun) FitRunByRun1D();
  }
  if (fVertexXY) {
    if (fCumulative) FitCumulative2D();
    if (fRunByRun) FitRunbyRun2D();
  }
  if (fFitByLayer) FitByLayer();

  if (fDrawProj) {
    if (fCumulative) ProjectCumulative2D();
    if (fRunByRun) ProjectRunbyRun2D();
  }

  if (fCumulative) DrawCumulativeSummary();
  if (fRunByRun) DrawRunByRunSummary();
  if (fFitByLayer) DrawFitByLayer();

  if (fCumulative && fRunByRun) {
    if (fVertexXY) DrawDispersionSummary2D();
    if (fProjection) DrawDispersionSummary1D();
  }

  if (fCumulative && fVertexXY && fProjection) PrintSummary();

  WriteToOutput();

  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class BeamCenterFit<Arm1AnPars, Arm1Params>;
template class BeamCenterFit<Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
