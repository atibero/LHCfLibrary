#include <TRint.h>

#include "BeamCenterFit.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput file\n");
  printf("\t-o <output>\t\tOutput folder\n");
  printf("\t-f <first>\t\tFirst run to be fitted (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be fitted (default = -1)\n");
  printf("\t-n <rebin>\t\tRebin factor for 1D and/or 2D histograms (default = 8)\n");
  printf("\t--arm1\t\t\tArm1 (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--cumulative\t\tPerform Fit the cumulative distribution (Default: True)\n");
  printf("\t--runbyrun\t\tPerform Fit the run-by-run distribution (Default: True)\n");
  printf("\t--projection\t\tPerform Fit on 1D distribution (Default: True)\n");
  printf("\t--xyvertex\t\tPerform Fit on 2D distribution (Default: True)\n");
  printf("\t--fitbylayer\t\tPerform 1D cumulative Fit layer by layer (Default: False)\n");
  printf("\t--optimize\t\tPerform Fit range optimization - better but slower (Default: False)\n");
  printf("\t--fixlimit\t\tPerform run-by-run with the same range of cumulative (Default: False)\n");
  printf("\t--gaussian\t\tUse 2D gaussian function with independent width (Default: False)\n");
  printf("\t--d2fromd1\t\tInitialize 2D from 1D result without 2D optimization (Default: False)\n");
  printf("\t--xyminuit\t\tAccomplish 2D fit using additional Minuit step (Default: False)\n");
  printf("\t--drawproj\t\tProject 2D distribution and fit to 1D (Default: False)\n");
  printf("\t--official\t\tFormat the canvas in the official style (Default: False)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string infile = "";
  string outdir = "";
  Int_t first_run = -1;
  Int_t last_run = -1;
  Int_t rebin = 8;
  Int_t arm = -1;
  Bool_t cumulative = false;
  Bool_t runbyrun = false;
  Bool_t projection = false;
  Bool_t xyvertex = false;
  Bool_t fitbylayer = false;
  Bool_t optimize = false;
  Bool_t fixlimit = false;
  Bool_t gaussian = false;
  Bool_t d2fromd1 = false;
  Bool_t xyminuit = false;
  Bool_t drawproj = false;
  Bool_t official = false;
  Int_t verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'}, {"output", required_argument, NULL, 'o'},
      {"first", required_argument, NULL, 'f'}, {"last", required_argument, NULL, 'l'},
      {"rebin", required_argument, NULL, 'n'}, {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},       {"cumulative", no_argument, NULL, 1003},
      {"runbyrun", no_argument, NULL, 1004},   {"projection", no_argument, NULL, 1005},
      {"xyvertex", no_argument, NULL, 1006},   {"fitbylayer", no_argument, NULL, 1007},
      {"optimize", no_argument, NULL, 1008},   {"fixlimit", no_argument, NULL, 1009},
      {"d2fromd1", no_argument, NULL, 1010},   {"gaussian", no_argument, NULL, 1011},
      {"xyminuit", no_argument, NULL, 1012},   {"drawproj", no_argument, NULL, 1013},
      {"official", no_argument, NULL, 1014},   {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},        {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) infile = optarg;
        break;
      case 'o':
        if (optarg) outdir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'n':
        if (optarg) rebin = atoi(optarg);
        break;
      case 1001:
        arm = Arm1Params::kArmIndex;
        break;
      case 1002:
        arm = Arm2Params::kArmIndex;
        break;
      case 1003:
        cumulative = true;
        break;
      case 1004:
        runbyrun = true;
        break;
      case 1005:
        projection = true;
        break;
      case 1006:
        xyvertex = true;
        break;
      case 1007:
        fitbylayer = true;
        break;
      case 1008:
        optimize = true;
        break;
      case 1009:
        fixlimit = true;
        break;
      case 1010:
        d2fromd1 = true;
        break;
      case 1011:
        gaussian = true;
        break;
      case 1012:
        xyminuit = true;
        break;
      case 1013:
        drawproj = true;
        break;
      case 1014:
        official = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (!cumulative && !runbyrun && !xyvertex && !projection) {
    cumulative = true;
    runbyrun = true;
    xyvertex = true;
    projection = true;
  }

  if (infile == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (outdir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (((cumulative || runbyrun) && !(xyvertex || projection)) ||
      (!(cumulative || runbyrun) && (xyvertex || projection))) {
    Utils::Printf(Utils::kPrintError,
                  "\nPlease specify at least "
                  "one between --cumulative and --runbyrun and one between --xyvertex and --projection\n");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (fixlimit && !cumulative) {
    Utils::Printf(Utils::kPrintError, "\nCannot fix run-by-run range limits without cumulative fit\n");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (drawproj && !xyvertex) {
    Utils::Printf(Utils::kPrintError, "\nCannot draw 2D fit projection without 2D fit request\n");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (d2fromd1 && !projection) {
    Utils::Printf(Utils::kPrintError, "\nCannot initialize 2D fit from 1D if you do not fit projection\n");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  BeamCenterFit<Arm1AnPars, Arm1Params> bcf_1;
  BeamCenterFit<Arm2AnPars, Arm2Params> bcf_2;
  if (arm == Arm1Params::kArmIndex) {
    bcf_1.SetInputFile(infile.c_str());
    bcf_1.SetOutputDir(outdir.c_str());
    bcf_1.SetBinFactor(rebin);
    bcf_1.SetRunRange(first_run, last_run);
    bcf_1.SetFitOption(cumulative, runbyrun, projection, xyvertex, fitbylayer, optimize, fixlimit, d2fromd1, gaussian,
                       xyminuit, drawproj, official);
    bcf_1.Run();
  } else if (arm == Arm2Params::kArmIndex) {
    bcf_2.SetInputFile(infile.c_str());
    bcf_2.SetOutputDir(outdir.c_str());
    bcf_2.SetBinFactor(rebin);
    bcf_2.SetRunRange(first_run, last_run);
    bcf_2.SetFitOption(cumulative, runbyrun, projection, xyvertex, fitbylayer, optimize, fixlimit, d2fromd1, gaussian,
                       xyminuit, drawproj, official);
    bcf_2.Run();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
