/*
 * CreatePedestalTree.cpp
 *
 * Description:
 * 	This code uses as input the raw root files and generate an output root file containing:
 * 	- histograms of pedestal, delayed gate and pedetal-delayed gate for the calorimeter
 * 	- histograms of pedestal for the position sensitive detector
 * 	- histograms of event, delayed gate and event-delayed gate for the calorimeter using events with L2T in ArmY and
 *not ArmX
 * 	- histograms of event, for the position sensitive detector using events with L2T in ArmY and not ArmX
 *	- tree containing events of calorimeter pedestal - delayed gate
 *	- tree containing events of position sensitive detector pedestal
 *	These output histograms must be later elaborated using ExtractPedTableAndFile to extract relevant tables
 *
 * Created on: 21 April 2021
 *      Author: Eugenio Berti
 */

#include <TRint.h>

#include "LHCfPedestal.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\tInput ROOT file\n");
  printf("\t-o <output>\tOutput ROOT file (default = \"calibrated.root\")\n");
  printf("\t-f <event>\tFirst event to be processed (default = 0)\n");
  printf("\t-l <event>\tLast event to be processed (default = all)\n");
  printf("\t--disable-arm1\tDisable Arm1 calibration (default = enabled)\n");
  printf("\t--disable-arm2\tDisable Arm2 calibration (default = enabled)\n");
  printf("\t--realign-arm2\tRealign silicon to calorimeter events (default = disabled)\n");
  printf("\t--histogram\tSave pedestal histograms for each channel (default = disabled)\n");
  printf("\t--average-pedestal\tPedestal tree without delayed gate subtraction (default = disabled)\n");
  printf("\t-v <verbose>\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_file = "";
  string output_file = "pedestal.root";
  int first_ev = 0;
  int last_ev = -1;
  int nevents = -1;
  int verbose = 1;
  bool arm1_en = true;
  bool arm2_en = true;
  bool sav_ped = false;
  bool sil_rea = false;
  bool ave_ped = false;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first-ev", required_argument, NULL, 'f'},
      {"last-ev", required_argument, NULL, 'l'},
      {"disable-arm1", no_argument, NULL, 1001},
      {"disable-arm2", no_argument, NULL, 1002},
      {"realign-arm2", no_argument, NULL, 1003},
      {"histogram", no_argument, NULL, 1004},
      {"average-pedestal", no_argument, NULL, 1005},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_file = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'f':
        if (optarg) first_ev = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_ev = atoi(optarg);
        break;
      case 1001:
        arm1_en = false;
        break;
      case 1002:
        arm2_en = false;
        break;
      case 1003:
        sil_rea = true;
        break;
      case 1004:
        sav_ped = true;
        break;
      case 1005:
        ave_ped = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Gain calibration ---*/
  Utils::SetVerbose(verbose);
  LHCfPedestal pedestal_func;
  pedestal_func.SetInputName(input_file.c_str());
  pedestal_func.SetOutputName(output_file.c_str());

  printf("\n");
  printf("===\n");
  printf("======= Pedestal\n");
  printf("===========================\n");
  printf("\tInput File: %s\n", input_file.c_str());
  printf("\tOutput File: %s\n", output_file.c_str());
  printf("\tFirst event: %d\n", first_ev);
  printf("\tLast event: %d\n", last_ev >= 0 ? last_ev : 0);
  if (!arm1_en) {
    printf("\t\tArm1 disabled\n");
    pedestal_func.DisableArm1();
  }
  if (!arm2_en) {
    printf("\t\tArm2 disabled\n");
    pedestal_func.DisableArm2();
  }
  if (sav_ped) {
    printf("\t\tSaving pedestal histogram\n");
    pedestal_func.SavePedestal();
  }
  if (sil_rea) {
    printf("\t\tRealigning silicon events\n");
    pedestal_func.RealignSilicon();
  }
  if (ave_ped) {
    printf("\t\tAvoiding delayed gate subtraction\n");
    pedestal_func.AveragePedestal();
  }

  pedestal_func.PedestalRun(first_ev, last_ev);

  // theApp.Run();

  return 0;
}
