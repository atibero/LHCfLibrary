#include <TFile.h>
#include <TRint.h>
#include <TTree.h>

#include <string>

#include "LHCfEvent.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\tInput file\n");
  printf("\t-h\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  std::string input_file = "";

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_file = optarg;
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  TFile tfile(input_file.c_str());
  TTree *tree;
  tfile.GetObject("LHCfEvents", tree);
  LHCfEvent *ev = new LHCfEvent("event", "LHCfEvent");
  tree->SetBranchAddress("ev.", &ev);

  for (int ie = 0; ie < tree->GetEntries(); ++ie) {
    printf("Event %d/%d\n", ie, tree->GetEntries());
    tree->GetEntry(ie);
    ev->Show();
    getchar();
  }

  tfile.Close();
  return 0;
}
