#include "GainRatio.hh"

#include <TF1.h>
#include <TMath.h>
#include <TProfile.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>

#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass>
GainRatio<armclass>::GainRatio()
    : fFirstRun(-1),
      fLastRun(-1),
      fOutputName("gain_ratio.root"),
      fOutputTableName(Form("a%d_cal_gain_factor.dat", this->kArmIndex + 1)) {
  fOutputFile = nullptr;
  fOutputTable = nullptr;
  fInputEv = nullptr;
  fLvl1 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass>
GainRatio<armclass>::~GainRatio() {
  delete fOutputFile;
  delete fOutputTable;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armclass>
void GainRatio<armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/cal_run%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void GainRatio<armclass>::MakeHistograms() {
  UT::AllocateVector1D(fSumdE, this->kCalNtower);
  UT::AllocateVector2D(fEdep, this->kCalNtower, this->kCalNlayer);
  UT::AllocateVector2D(fCorr, this->kCalNtower, this->kCalNlayer);

  Double_t lrange_bin = 500;
  Double_t lrange_min = 0.;
  Double_t lrange_max = 4000.;

  Double_t hrange_bin = 500;
  Double_t hrange_min = 0.;
  Double_t hrange_max = 500.;

  for (UInt_t it = 0; it < fCorr.size(); ++it)
    fSumdE[it] =
        new TH1D(Form("hsumde_%d", it), Form("Tower %d;Total energy deposit [HR ADC];Events", it), 1000, 0., 10000.);

  for (UInt_t it = 0; it < fEdep.size(); ++it) {
    for (UInt_t il = 0; il < fEdep[it].size(); ++il) {
      fEdep[it][il] =
          new TH1D(Form("hde_%d_%d", it, il), Form("Tower %d;Energy deposit [HR ADC];Events", it), 1000, 0., 10000.);
    }
  }

  for (UInt_t it = 0; it < fCorr.size(); ++it) {
    for (UInt_t il = 0; il < fCorr[it].size(); ++il) {
      fCorr[it][il] = new TH2F(Form("gain_ratio_%d_%02d", it, il),
                               Form("Tower %d, Layer %d;HR signal [ADC];LR signal [ADC]", it, il), hrange_bin,
                               hrange_min, hrange_max, lrange_bin, lrange_min, lrange_max);
    }
  }
}

template <typename armclass>
void GainRatio<armclass>::ClearEvent() {
  fLvl1 = nullptr;
  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armclass>
void GainRatio<armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->Write();
  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-----------*/
/*--- Fit ---*/
/*-----------*/
template <typename armclass>
void GainRatio<armclass>::FitGain() {
  SetInputTree();

  fOutputFile = new TFile(fOutputName, "RECREATE");
  fOutputTable = new ofstream(fOutputTableName.Data());

  MakeHistograms();

  /*--- Event Loop ---*/
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  Int_t nentries = fInputTree->GetEntries();
  for (Int_t ie = 0; ie <= nentries; ++ie) {
    if (ie % 100 == 0 || ie == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d", ie);
      fflush(stdout);
    }
    UT::Printf(UT::kPrintDebug, "  get entry\n");
    fInputTree->GetEntry(ie);

    UT::Printf(UT::kPrintDebug, "  getting level1\n");
    fLvl1 = (Level1<armclass> *)fInputEv->Get(Form("lvl1_a%d", this->kArmIndex + 1));
    UT::Printf(UT::kPrintDebug, "  got level1 %p\n", fLvl1);
    if (!fLvl1) {
      UT::Printf(UT::kPrintError, "Level1 not found in event %d\n", ie);
      continue;
    }

    /* Fill histograms */
    const Double_t threshold = 0.;

    for (UInt_t it = 0; it < this->kCalNtower; ++it) {
      // check sum-dE
      Double_t sumdE = 0.;
      UT::Printf(UT::kPrintDebug, "  sumdE\n");
      for (UInt_t il = 0; il < this->kCalNlayer; ++il) sumdE += fLvl1->fCalorimeter[it][il][1];
      fSumdE[it]->Fill(sumdE);
      for (UInt_t il = 0; il < this->kCalNlayer; ++il) fEdep[it][il]->Fill(fLvl1->fCalorimeter[it][il][1]);
      if (sumdE > threshold) {
        UT::Printf(UT::kPrintDebug, "  fill\n");
        // fill correlation hist
        for (UInt_t il = 0; il < this->kCalNlayer; ++il) {
          UT::Printf(UT::kPrintDebug, "  %lf %lf\n", fLvl1->fCalorimeter[it][il][1], fLvl1->fCalorimeter[it][il][0]);
          fCorr[it][il]->Fill(fLvl1->fCalorimeter[it][il][1], fLvl1->fCalorimeter[it][il][0]);
        }
      }
    }
    UT::Printf(UT::kPrintDebug, "  clear\n");
    ClearEvent();
  }
  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");

  fOutputFile->cd();
  vector<vector<TProfile *> > pratio;
  UT::AllocateVector2D(pratio, this->kCalNtower, this->kCalNlayer);
  for (UInt_t it = 0; it < this->kCalNtower; ++it) {
    for (UInt_t il = 0; il < this->kCalNlayer; ++il) {
      if (fCorr[it][il]->GetEntries() > 0) {
        const double bmin = 0;
        const double bmax = 300;
        pratio[it][il] = fCorr[it][il]->ProfileX(Form("pratio_%d_%02d", it, il), 1, -1, "i");
        pratio[it][il]->Fit("pol1", "EMI", "", bmin, bmax);
        TF1 *func = pratio[it][il]->GetFunction("pol1");
        if (func) {
          double offset = func->GetParameter(0);
          double ratio = func->GetParameter(1);
          double offset_err = func->GetParError(0);
          double ratio_err = func->GetParError(1);
          *fOutputTable << it << "\t" << il << "\t" << ratio << "\t" << offset << "\t" << ratio_err << "\t"
                        << offset_err << endl;
        }
      }
    }
  }
  fOutputTable->close();
  delete fOutputTable;
  fOutputTable = nullptr;

  /* Write to output file */
  WriteToOutput();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class GainRatio<Arm1Params>;
template class GainRatio<Arm2Params>;
}  // namespace nLHCf
