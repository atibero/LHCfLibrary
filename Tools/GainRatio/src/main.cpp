#include <TRint.h>

#include "GainRatio.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\tInput directory (or input file if ending with \".root\")\n");
  printf("\t-f <first>\tFirst run to be analysed (default = all runs in dir)\n");
  printf("\t-l <last>\tLast run to be analysed (default = all runs in dir)\n");
  printf("\t-o <output>\tOutput ROOT file (default = \"gain_ratio.root\")\n");
  printf("\t--arm1\t\tArm1 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\tArm2 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t-v <verbose>\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  int first_run = -1;
  int last_run = -1;
  string output_file = "gain_ratio.root";
  int iarm = -1;
  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"output", required_argument, NULL, 'o'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d -> %d\n", first_run, last_run);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  GainRatio<Arm1Params> gain_1;
  GainRatio<Arm2Params> gain_2;
  if (iarm == Arm1Params::kArmIndex) {
    gain_1.SetInputDir(input_dir.c_str());
    gain_1.SetFirstRun(first_run);
    gain_1.SetLastRun(last_run);
    gain_1.SetOutputName(output_file.c_str());
    gain_1.FitGain();
  } else if (iarm == Arm2Params::kArmIndex) {
    gain_2.SetInputDir(input_dir.c_str());
    gain_2.SetFirstRun(first_run);
    gain_2.SetLastRun(last_run);
    gain_2.SetOutputName(output_file.c_str());
    gain_2.FitGain();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
