#ifndef GainRatio_HH
#define GainRatio_HH

#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TString.h>

#include <fstream>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level1.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class GainRatio : public armclass, public LHCfParams {
 public:
  GainRatio();
  ~GainRatio();

 private:
  /*--- Input/Output ---*/
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;
  TString fOutputTableName;
  std::ofstream *fOutputTable;

  /*--- Level1 ---*/
  Level1<armclass> *fLvl1;

  /*--- Histograms ---*/
  vector<TH1D *> fSumdE;
  vector<vector<TH1D *> > fEdep;
  vector<vector<TH2F *> > fCorr;

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SetOutputTable(const Char_t *name) { fOutputTableName = name; }

  void FitGain();

 private:
  void SetInputTree();
  void MakeHistograms();
  void ClearEvent();
  void WriteToOutput();
};
}  // namespace nLHCf
#endif
