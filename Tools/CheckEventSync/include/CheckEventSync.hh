#ifndef CheckEventSync_HH
#define CheckEventSync_HH

#include <TChain.h>
#include <TFile.h>
#include <TH2F.h>
#include <TString.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class CheckEventSync : public armclass, public LHCfParams {
 public:
  CheckEventSync();
  ~CheckEventSync();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  Int_t fSubRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;

  Int_t fNevents;

  /*--- Level0/1/2 ---*/
  Level0<armclass> *fLvl0;
  Level0<armclass> *fPed0;
  Level1<armclass> *fLvl1;
  Level2<armclass> *fLvl2;

  /*--- Histograms ---*/
  vector<vector<vector<vector<TH2F *> > > > fSilCalCorr;
  vector<vector<TH2F *> > fSilCalCorrSum;

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetSubRun(Int_t subrun) { fSubRun = subrun; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SetInputFile(const Char_t *name) { fInputFile = name; }

  void SetEvents(Int_t ev) { fNevents = ev; }

  void Check();

 private:
  void SetInputTree();
  void MakeHistograms();
  void ClearEvent();
  void WriteToOutput();
  void CloseFiles();
};
}  // namespace nLHCf
#endif
