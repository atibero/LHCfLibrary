#include "CheckEventSync.hh"

#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>

#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass>
CheckEventSync<armclass>::CheckEventSync() : fFirstRun(-1), fLastRun(-1), fSubRun(-1), fNevents(-1) {
  fOutputFile = nullptr;
  fInputEv = nullptr;

  fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass>
CheckEventSync<armclass>::~CheckEventSync() {
  delete fOutputFile;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armclass>
void CheckEventSync<armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      printf("%d\n", fSubRun);
      TString iname(fSubRun >= 0 ? Form("%s/cal_run%05d_%02d.root", fInputDir.Data(), irun, fSubRun)
                                 : Form("%s/cal_run%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void CheckEventSync<armclass>::MakeHistograms() {
  UT::AllocateVector4D(fSilCalCorr, this->kCalNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  UT::AllocateVector2D(fSilCalCorrSum, this->kCalNtower, this->kPosNsample);

  TString pos_title = this->kArmIndex == Arm1Params::kArmIndex ? "GSO bars" : "Silicon";

  Double_t cal_bin = 100;
  Double_t cal_min = 0.;
  Double_t cal_max = 5.;  // 1000.;

  Double_t pos_bin = 150;
  Double_t pos_min = 0.;
  Double_t pos_max = this->kArmIndex == Arm1Params::kArmIndex ? 20000 : 30000.;

  for (UInt_t it = 0; it < fSilCalCorr.size(); ++it) {
    for (UInt_t il = 0; il < fSilCalCorr[it].size(); ++il) {
      for (UInt_t iv = 0; iv < fSilCalCorr[it][il].size(); ++iv) {
        for (UInt_t is = 0; is < fSilCalCorr[it][il][iv].size(); ++is) {
          fSilCalCorr[it][il][iv][is] = new TH2F(
              Form("sil_vs_cal_%d_%d_%d_%d", it, il, iv, is),
              // Form("%s vs Calorimeter (tower %d, layer %d, view %d, sample %d);Calorimeter dE [ADC];%s dE [ADC]",
              // pos_title.Data(), it, il, iv, is, pos_title.Data()),
              Form("%s vs Calorimeter (tower %d, layer %d, view %d, sample %d);Calorimeter dE [GeV];%s dE [GeV]",
                   pos_title.Data(), it, il, iv, is, pos_title.Data()),
              cal_bin, cal_min, cal_max, pos_bin, pos_min, pos_max);
        }
      }
    }
  }

  for (UInt_t it = 0; it < fSilCalCorrSum.size(); ++it) {
    for (UInt_t is = 0; is < fSilCalCorrSum[it].size(); ++is) {
      fSilCalCorrSum[it][is] =
          new TH2F(Form("sil_vs_cal_sum_%d_%d", it, is),
                   // Form("%s vs Calorimeter (sum) (tower %d, sample %d);Calorimeter total dE [ADC];%s total dE [ADC]",
                   // pos_title.Data(), it, is, pos_title.Data()),
                   Form("%s vs Calorimeter (sum) (tower %d, sample %d);Calorimeter total dE [GeV];%s total dE [GeV]",
                        pos_title.Data(), it, is, pos_title.Data()),
                   cal_bin, cal_min * 10., cal_max * 10., pos_bin, pos_min * 5., pos_max * 5.);
    }
  }
}

template <typename armclass>
void CheckEventSync<armclass>::ClearEvent() {
  fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armclass>
void CheckEventSync<armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void CheckEventSync<armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-------------*/
/*--- Check ---*/
/*-------------*/
template <typename armclass>
void CheckEventSync<armclass>::Check() {
  SetInputTree();

  fOutputFile = new TFile(fOutputName, "RECREATE");

  MakeHistograms();

  /*--- Event Loop ---*/
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  vector<vector<vector<Int_t> > > silran;
  Utils::AllocateVector3D(silran, this->kCalNtower, this->kPosNview, 2);
  silran[0][0][0] = 218;
  silran[0][0][1] = 375;  // ST X
  silran[0][1][0] = 219;
  silran[0][1][1] = 376;  // ST Y
  silran[1][0][0] = 6;
  silran[1][0][1] = 207;  // LT X
  silran[1][1][0] = 8;
  silran[1][1][1] = 209;  // LT Y

  Int_t nentries = fInputTree->GetEntries();
  if (fNevents > 0) nentries = TMath::Min(nentries, fNevents);
  for (Int_t ie = 0; ie <= nentries; ++ie) {
    if (ie % 100 == 0 || ie == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d", ie);
      fflush(stdout);
    }
    fInputTree->GetEntry(ie);

    // fLvl1 = (Level1<armclass> *)fInputEv->Get(Form("lvl1_a%d", this->kArmIndex+1));
    // if (!fLvl1) continue;
    fLvl2 = (Level2<armclass> *)fInputEv->Get(Form("lvl2_a%d", this->kArmIndex + 1));
    if (!fLvl2) continue;

    const Int_t ir = 1;

    /* Fill histograms */
    for (UInt_t it = 0; it < this->kCalNtower; ++it) {
      for (UInt_t is = 0; is < this->kPosNsample; ++is) {
        const Int_t pos_tower = this->kArmIndex == Arm1Params::kArmIndex ? it : 0;

        // total calorimeter energy deposit
        Double_t cal_tot_dE = 0.;
        for (UInt_t il = 0; il < this->kCalNlayer; ++il)
          // cal_tot_dE += fLvl1->fCalorimeter[it][il][ir];
          cal_tot_dE += fLvl2->fCalorimeter[it][il];

        // silicon energy deposits
        Double_t sil_tot_dE = 0.;
        for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
          for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
            // sum of all strips
            Double_t sil_dE = 0;
            for (UInt_t ic = 0; ic < this->kPosNchannel[pos_tower]; ++ic) {
              // Double_t dE = fLvl1->fPosDet[pos_tower][il][iv][ic][is];
              Double_t dE = fLvl2->fPosDet[pos_tower][il][iv][ic][is];
              // if (dE > 50.)
              if (ic >= silran[it][iv][0] && ic <= silran[it][iv][1]) sil_dE += dE;
            }
            // sum of nearest calorimeter channels
            Double_t cal_dE = 0.;
            // Arm1
            if (this->kArmIndex == Arm1Params::kArmIndex) {
              if (il == 0)
                // cal_dE =fLvl1->fCalorimeter[it][2][ir];
                cal_dE = fLvl2->fCalorimeter[it][2];
              else if (il == 1)
                // cal_dE = fLvl1->fCalorimeter[it][4][ir];
                cal_dE = fLvl2->fCalorimeter[it][4];
              else if (il == 2)
                // cal_dE = fLvl1->fCalorimeter[it][12][ir];
                cal_dE = fLvl2->fCalorimeter[it][12];
              else if (il == 3)
                // cal_dE = fLvl1->fCalorimeter[it][15][ir];
                cal_dE = fLvl2->fCalorimeter[it][15];
            }
            // Arm2
            else if (this->kArmIndex == Arm2Params::kArmIndex) {
              if (il == 0) {
                // cal_dE = fLvl1->fCalorimeter[it][2][ir];
                cal_dE = fLvl2->fCalorimeter[it][2];
              } else if (il == 1) {
                // cal_dE = fLvl1->fCalorimeter[it][5][ir];
                cal_dE = fLvl2->fCalorimeter[it][5];
              } else if (il == 2) {
                if (iv == 0)  // X
                  // cal_dE = 0.5 * (fLvl1->fCalorimeter[it][10][ir] + fLvl1->fCalorimeter[it][11][ir]);
                  cal_dE = 0.5 * (fLvl2->fCalorimeter[it][10] + fLvl2->fCalorimeter[it][11]);
                else  // Y
                  // cal_dE = fLvl1->fCalorimeter[it][8][ir];
                  cal_dE = fLvl2->fCalorimeter[it][8];
              } else if (il == 3) {
                if (iv == 0)  // X
                  // cal_dE =  0.5 * (fLvl1->fCalorimeter[it][14][ir] + fLvl1->fCalorimeter[it][15][ir]);
                  cal_dE = 0.5 * (fLvl2->fCalorimeter[it][14] + fLvl2->fCalorimeter[it][15]);
                else  // Y
                  // cal_dE =  0.5 * (fLvl1->fCalorimeter[it][12][ir] + fLvl1->fCalorimeter[it][13][ir]);
                  cal_dE = 0.5 * (fLvl2->fCalorimeter[it][12] + fLvl2->fCalorimeter[it][13]);
              }
            }
            // fill histogram
            fSilCalCorr[it][il][iv][is]->Fill(cal_dE, sil_dE);
            sil_tot_dE += sil_dE;
          }
        }
        // fill sum histogram
        fSilCalCorrSum[it][is]->Fill(cal_tot_dE, sil_tot_dE);
      }
    }

    ClearEvent();
  }
  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");

  /* Write to output file */
  WriteToOutput();
  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class CheckEventSync<Arm1Params>;
template class CheckEventSync<Arm2Params>;
}  // namespace nLHCf
