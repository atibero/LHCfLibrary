#include <TRint.h>

#include "CheckEventSync.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (or input file if ending with \".root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = -1)\n");
  printf("\t-s <subrun>\t\tSubrun to be analysed (default = -1)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"converted.root\")\n");
  printf("\t-n <nev>\t\t# of events to be analysed (default = all\n");
  printf("\t--arm1\t\t\tArm1 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  int first_run = -1;
  int last_run = -1;
  int sub_run = -1;
  string output_file = "check.root";
  int nev = -1;
  int iarm = -1;
  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"subrun", required_argument, NULL, 's'},
      {"output", required_argument, NULL, 'o'},
      {"nev", required_argument, NULL, 'n'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 's':
        if (optarg) sub_run = atoi(optarg);
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'n':
        if (optarg) nev = atoi(optarg);
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d -> %d\n", first_run, last_run);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  CheckEventSync<Arm1Params> chk_1;
  CheckEventSync<Arm2Params> chk_2;
  if (iarm == Arm1Params::kArmIndex) {
    chk_1.SetInputDir(input_dir.c_str());
    chk_1.SetFirstRun(first_run);
    chk_1.SetLastRun(last_run);
    if (sub_run >= 0) chk_1.SetSubRun(sub_run);
    chk_1.SetOutputName(output_file.c_str());
    chk_1.SetEvents(nev);
    chk_1.Check();
  } else if (iarm == Arm2Params::kArmIndex) {
    chk_2.SetInputDir(input_dir.c_str());
    chk_2.SetFirstRun(first_run);
    chk_2.SetLastRun(last_run);
    if (sub_run >= 0) chk_2.SetSubRun(sub_run);
    chk_2.SetOutputName(output_file.c_str());
    chk_2.SetEvents(nev);
    chk_2.Check();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
