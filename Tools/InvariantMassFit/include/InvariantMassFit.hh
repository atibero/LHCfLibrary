#ifndef InvariantMassFit_HH
#define InvariantMassFit_HH

#include <Minuit2/FCNBase.h>
#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/Minuit2Minimizer.h>
#include <Minuit2/MnMigrad.h>
#include <Minuit2/MnMinos.h>
#include <Minuit2/MnPlot.h>
#include <Minuit2/MnPrint.h>
#include <Minuit2/MnScan.h>
#include <Minuit2/MnUserParameters.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TF1.h>
#include <TF2.h>
#include <TFile.h>
#include <TFitResult.h>
#include <TGaxis.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLine.h>
#include <TMarker.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TPaletteAxis.h>
#include <TPaveStats.h>
#include <TPostScript.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>
#include <TVirtualX.h>
#include <dirent.h>

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <map>

#include "Arm1AnPars.hh"
#include "Arm1Params.hh"
#include "Arm2AnPars.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"

using namespace std;

namespace ROOT {
namespace Minuit2 {
class CustomFCN : public FCNBase {
 public:
  CustomFCN(const std::vector<double> &posx, const std::vector<double> &posy, const std::vector<double> &height,
            int *npfits, const double xEdgeMin, const double xEdgeMax, const double yEdgeMin, const double yEdgeMax)
      : fPosx(posx),
        fPosy(posy),
        fHeight(height),
        np(npfits),
        xmin(xEdgeMin),
        xmax(xEdgeMax),
        ymin(yEdgeMin),
        ymax(yEdgeMax) {}

  virtual ~CustomFCN() {}
  //--- simple function ---
  virtual double operator()(const std::vector<double> &par) const {
    double fval = 0.;
    for (unsigned int i = 0; i < fPosx.size(); i++) {
      double posX = fPosx[i];
      double posY = fPosy[i];
      double meas = fHeight[i];

      double A = par[0];
      double B = par[1];
      double x0 = par[2];
      double y0 = par[3];

      if (i == 0) *np = 0;

      if (posX < xmin || posX >= xmax) continue;
      if (posY < ymin || posY >= ymax) continue;

      double func = A * exp(-B * sqrt(pow(posX - x0, 2.) + pow(posY - y0, 2.)));

      /* Gauss chi2 */
      double error = meas;  // +1 ?
      if (meas > 0.) {
        fval += pow(func - meas, 2.) / error;

        ++(*np);
      }
    }
    return fval;
  }
  virtual double Up() const { return 1.; }

 private:
  std::vector<double> fPosx;
  std::vector<double> fPosy;
  std::vector<double> fHeight;
  int *np;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
};
}  // namespace Minuit2
}  // namespace ROOT

namespace nLHCf {

template <typename arman, typename armclass>
class InvariantMassFit : public arman, public LHCfParams {
 public:
  InvariantMassFit();
  ~InvariantMassFit();

 private:
  /*--- Input/Output ---*/
  TString fInputFileName;
  TString fOutputDirName;
  Int_t fFirstRun;
  Int_t fLastRun;
  Int_t fRebin;
  Bool_t fIsEta;
  Bool_t fCumulative;
  Bool_t fRunByRun;
  Bool_t fXfTower;
  Bool_t fOptimize;
  Bool_t fFixLimit;
  Bool_t fOfficial;
  TFile *fOutputFile;

  Int_t fNRun;

  /*--- Number of towers ---*/
  static const Int_t fNTower = 2;

  /*--- Energy threshold ---*/
  static const Int_t fNxFBin = 20.;

  /*--- Invariant mass type ---*/
  static const Int_t fNType = 5;
  const TString fLabelType[fNType] = {"TypeI", "TypeIITS", "TypeIITL", "TypeITS", "TypeITL"};
  const TColor *fColor[fNType] = {
      gROOT->GetColor(kBlack),  gROOT->GetColor(kRed),        gROOT->GetColor(kBlue),
      gROOT->GetColor(kOrange), gROOT->GetColor(kAzure + 10),
  };

  /*--- Number of fit parameters ---*/
  // static const Int_t fNPars = 7; //symmetric
  static const Int_t fNPars = 8;  // asymmetric

  /*--- Minimum number of entries ---*/
  const Int_t fThresholdEntries = 1000;
  const Int_t fThresholdMaximum = 250;

  /*--- Meson Constants ---*/
  const Int_t fPi0Type = 3;
  const Double_t fPi0Mass = 134.977;
  const Double_t fPi0Min = 120;
  const Double_t fPi0Max = 170;
  const Int_t fEtaType = 1;
  const Double_t fEtaMass = 547.862;
  const Double_t fEtaMin = 500;
  const Double_t fEtaMax = 600;

  /*--- Number of types considered ---*/
  Int_t fNCategory;
  Double_t fRangeMin;
  Double_t fRangeMax;
  Double_t fMassMin;
  Double_t fMassMax;
  Double_t fMass;

  /*--- Best fit parameters ---*/
  Double_t fFmin;
  Double_t fFmax;
  Double_t fFPar[fNPars];
  Double_t fNdof;
  Double_t fChi2;

  /*--- Mass Histograms ---*/
  vector<TH1D *> fHisto_InvMassCumTot;
  vector<vector<TH1D *>> fHisto_InvMassRunTot;
  vector<vector<TH1D *>> fHisto_InvMassCumXf;
  vector<vector<vector<TH1D *>>> fHisto_InvMassRunXf;

  /*--- Mass Functions ---*/
  vector<TF1> fFunc_InvMassCumTot;
  vector<vector<TF1>> fFunc_InvMassRunTot;
  vector<vector<TF1>> fFunc_InvMassCumXf;
  vector<vector<vector<TF1>>> fFunc_InvMassRunXf;

  vector<TLine *> fLine_Cumulative;
  vector<TGraphErrors *> fGraph_Cumulative;
  vector<TMultiGraph *> fSummary_Cumulative;
  //  vector<TLegend *> fLegend_Cumulative;

  vector<TLine *> fLine_RunByRun;
  vector<TGraphErrors *> fGraph_RunByRun;
  vector<TMultiGraph *> fSummary_RunByRun;
  //  vector<TLegend *> fLegend_RunByRun;

  vector<TLine *> fLine_Deviation_Cumulative;
  vector<TGraphErrors *> fGraph_Deviation_Cumulative;
  TMultiGraph *fSummary_Deviation_Cumulative;
  TLegend *fLegend_Deviation_Cumulative;

  vector<TLine *> fLine_Deviation_RunByRun;
  vector<TGraphErrors *> fGraph_Deviation_RunByRun;
  TMultiGraph *fSummary_Deviation_RunByRun;
  TLegend *fLegend_Deviation_RunByRun;

  vector<TLine *> fLine_Correction_Cumulative;
  vector<TGraphErrors *> fGraph_Correction_Cumulative;
  TMultiGraph *fSummary_Correction_Cumulative;
  TLegend *fLegend_Correction_Cumulative;

  vector<TLine *> fLine_Correction_RunByRun;
  vector<TGraphErrors *> fGraph_Correction_RunByRun;
  TMultiGraph *fSummary_Correction_RunByRun;
  TLegend *fLegend_Correction_RunByRun;

 public:
  void SetInputFile(const Char_t *name) { fInputFileName = name; }
  void SetOutputDir(const Char_t *name) { fOutputDirName = name; }
  void SetFitOption(Bool_t eta, Bool_t cumulative, Bool_t runbyrun, Bool_t xftower, Bool_t optimize, Bool_t fixlimit,
                    Bool_t official) {
    fIsEta = eta;
    fCumulative = cumulative;
    fRunByRun = runbyrun;
    fXfTower = xftower;
    fOptimize = optimize;
    fFixLimit = fixlimit;
    fOfficial = official;
  }
  void SetRunRange(Int_t first_run, Int_t last_run) {
    fFirstRun = first_run;
    fLastRun = last_run;
  }
  void SetBinFactor(Int_t rebin) { fRebin = rebin; }
  void Run();

 private:
  void GetInputHistograms();
  void SetUpCanvasStyle();
  void ClearBestPars();
  void CopyToBestFit(TF1 f1, Double_t minEdge, Double_t maxEdge);
  void LocateMaximum(TH1D *h1, Double_t minMass, Double_t maxMass, Double_t &hAmpPeak, Double_t &hMaxPeak);
  void InitializeFit(TF1 &f1, Int_t pionType, Double_t minEdge, Double_t maxEdge, Double_t maxPeak, Double_t ampPeak);
  void OptimizeRange(TH1D *h1, Int_t pionType, Double_t minEdge, Double_t maxEdge, Double_t maxPeak, Double_t ampPeak);
  void SetUpFinalFit(TF1 &f1, Int_t pionType);
  Bool_t FitMass(TH1D *h1, TF1 &f1);
  Bool_t DrawFit(TH1D *h1, TF1 &f1);
  void FitCumulative();
  void FitRunByRun();
  void CreateReference();
  void CreateCumulativeXfDependence();
  void CreateCumulativeDeviation();
  void CreateCumulativeCorrection();
  void DrawCumulativeSummary();
  void CreateRunByRunVariation();
  void CreateRunByRunDeviation();
  void CreateRunByRunCorrection();
  void DrawRunByRunSummary();
  void WriteToOutput();
  void SaveCumulative();
  void SaveRunByRun();
};
}  // namespace nLHCf
#endif
