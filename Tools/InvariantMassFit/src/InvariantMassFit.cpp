#include "InvariantMassFit.hh"

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*--------------------*/
/*--- Fit Function ---*/
/*--------------------*/
Double_t asymmetricFunction(Double_t *x, Double_t *par) {
  Double_t A = par[0];  // peak
  Double_t M = par[1];  // center
  Double_t I = par[2];  // sigma-inferior
  Double_t S = par[3];  // sigma-superior

  Double_t a = par[4];
  Double_t b = par[5];
  Double_t c = par[6];
  Double_t d = par[7];

  Double_t sigma = x[0] >= M ? S : I;

  Double_t signal = A * TMath::Exp(-0.5 * TMath::Power(x[0] - M, 2.) / TMath::Power(sigma, 2.));
  Double_t background = a + b * x[0] + c * (TMath::Power(x[0], 2.) - 1) + d * (4. * TMath::Power(x[0], 3.) - 3. * x[0]);

  return signal + background;
}

Double_t invariantFunction(Double_t *x, Double_t *par) {
  Double_t A = par[0];  // peak
  Double_t M = par[1];  // center
  Double_t S = par[2];  // sigma

  Double_t a = par[3];
  Double_t b = par[4];
  Double_t c = par[5];
  Double_t d = par[6];

  Double_t signal = A * TMath::Exp(-0.5 * TMath::Power(x[0] - M, 2.) / TMath::Power(S, 2.));
  Double_t background = a + b * x[0] + c * (TMath::Power(x[0], 2.) - 1) + d * (4. * TMath::Power(x[0], 3.) - 3. * x[0]);

  return signal + background;
}

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename arman, typename armclass>
InvariantMassFit<arman, armclass>::InvariantMassFit()
    : fIsEta(false),
      fCumulative(false),
      fRunByRun(false),
      fXfTower(false),
      fOptimize(false),
      fFixLimit(false),
      fOfficial(false),
      fFirstRun(-1),
      fLastRun(-1),
      fRebin(1),
      fNRun(-1) {
  fOutputFile = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename arman, typename armclass>
InvariantMassFit<arman, armclass>::~InvariantMassFit() {
  delete fOutputFile;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::GetInputHistograms() {
  UT::Printf(UT::kPrintInfo, "Getting input histograms...");
  fflush(stdout);

  TFile file(fInputFileName.Data(), "READ");
  if (file.IsZombie()) {
    file.Close();
    UT::Printf(UT::kPrintError, "File %s does not exist: Exit...\n", fInputFileName.Data());
  }

  fNRun = fLastRun - fFirstRun + 1;

  Utils::AllocateVector1D(fHisto_InvMassCumTot, fNCategory);
  Utils::AllocateVector2D(fHisto_InvMassRunTot, fNCategory, fNRun);
  Utils::AllocateVector2D(fHisto_InvMassCumXf, fNCategory, fNxFBin);
  Utils::AllocateVector3D(fHisto_InvMassRunXf, fNCategory, fNxFBin, fNRun);

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    TString thisPion = fLabelType[ip];
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      Double_t thisxF = ((Double_t)(ix + 0.5)) / fNxFBin;
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
        //
        TString thisName = Form("%s_%d_xF%.3f", thisPion.Data(), thisRun, thisxF);
        fHisto_InvMassRunXf[ip][ix][ir] = NULL;
        fHisto_InvMassRunXf[ip][ix][ir] = (TH1D *)file.Get(thisName.Data());
        if (fHisto_InvMassRunXf[ip][ix][ir] == NULL) {
          UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Exit...\n", thisName.Data());
          continue;
        }
        if (fHisto_InvMassRunXf[ip][ix][ir]->Integral(fHisto_InvMassRunXf[ip][ix][ir]->FindBin(fRangeMin),
                                                      fHisto_InvMassRunXf[ip][ix][ir]->FindBin(fRangeMax)) <
            fThresholdEntries / 10.) {
          delete fHisto_InvMassRunXf[ip][ix][ir];
          fHisto_InvMassRunXf[ip][ix][ir] = NULL;
          continue;
        }
        fHisto_InvMassRunXf[ip][ix][ir]->SetDirectory(0);
      }
      TString thisName = Form("%s_Cumulative_xF%.3f", thisPion.Data(), thisxF);
      fHisto_InvMassCumXf[ip][ix] = NULL;
      fHisto_InvMassCumXf[ip][ix] = (TH1D *)file.Get(thisName.Data());
      if (fHisto_InvMassCumXf[ip][ix] == NULL) {
        UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Exit...\n", thisName.Data());
        continue;
      }
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries) {
        delete fHisto_InvMassCumXf[ip][ix];
        fHisto_InvMassCumXf[ip][ix] = NULL;
        continue;
      }
      fHisto_InvMassCumXf[ip][ix]->SetDirectory(0);
    }
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
      //
      TString thisName = Form("%s_%d", thisPion.Data(), thisRun);
      fHisto_InvMassRunTot[ip][ir] = NULL;
      fHisto_InvMassRunTot[ip][ir] = (TH1D *)file.Get(thisName.Data());
      if (fHisto_InvMassRunTot[ip][ir] == NULL) {
        UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Exit...\n", thisName.Data());
        continue;
      }
      if (fHisto_InvMassRunTot[ip][ir]->Integral(fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMin),
                                                 fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMax)) <
          fThresholdEntries / 5.) {
        delete fHisto_InvMassRunTot[ip][ir];
        fHisto_InvMassRunTot[ip][ir] = NULL;
        continue;
      }
      fHisto_InvMassRunTot[ip][ir]->SetDirectory(0);
    }
    //
    TString thisName = Form("%s_Cumulative", thisPion.Data());
    fHisto_InvMassCumTot[ip] = NULL;
    fHisto_InvMassCumTot[ip] = (TH1D *)file.Get(thisName.Data());
    if (fHisto_InvMassCumTot[ip] == NULL) {
      UT::Printf(UT::kPrintError, "\tHistogram %s does not exist: Exit...\n", thisName.Data());
      continue;
    }
    if (fHisto_InvMassCumTot[ip]->Integral(fHisto_InvMassCumTot[ip]->FindBin(fRangeMin),
                                           fHisto_InvMassCumTot[ip]->FindBin(fRangeMax)) < fThresholdEntries) {
      delete fHisto_InvMassCumTot[ip];
      fHisto_InvMassCumTot[ip] = NULL;
      continue;
    }
    fHisto_InvMassCumTot[ip]->SetDirectory(0);
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile = new TFile(Form("%s/FitResult%d-%d.root", fOutputDirName.Data(), fFirstRun, fLastRun), "RECREATE");
  fOutputFile->cd();

  if (fCumulative) SaveCumulative();
  if (fRunByRun) SaveRunByRun();

  fOutputFile->Close();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::SaveCumulative() {
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      fHisto_InvMassCumXf[ip][ix]->Write();
      fFunc_InvMassCumXf[ip][ix].Write();
    }
    if (fHisto_InvMassCumTot[ip] == NULL) continue;
    fHisto_InvMassCumTot[ip]->Write();
    fFunc_InvMassCumTot[ip].Write();
    if (fGraph_Cumulative[ip] == NULL) continue;
    fGraph_Cumulative[ip]->Write();
    if (fGraph_Deviation_Cumulative[ip] == NULL) continue;
    fGraph_Deviation_Cumulative[ip]->Write();
    if (fGraph_Correction_Cumulative[ip] == NULL) continue;
    fGraph_Correction_Cumulative[ip]->Write();
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::SaveRunByRun() {
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      for (Int_t ix = 0; ix < fNxFBin; ++ix) {
        if (fHisto_InvMassRunXf[ip][ix][ir] == NULL) continue;
        fHisto_InvMassRunXf[ip][ix][ir]->Write();
        fFunc_InvMassRunXf[ip][ix][ir].Write();
      }
      if (fHisto_InvMassRunTot[ip][ir] == NULL) continue;
      fHisto_InvMassRunTot[ip][ir]->Write();
      fFunc_InvMassRunTot[ip][ir].Write();
    }
    if (fGraph_RunByRun[ip] == NULL) continue;
    fGraph_RunByRun[ip]->Write();
    if (fGraph_Deviation_RunByRun[ip] == NULL) continue;
    fGraph_Deviation_RunByRun[ip]->Write();
    if (fGraph_Correction_RunByRun[ip] == NULL) continue;
    fGraph_Correction_RunByRun[ip]->Write();
  }
}

/*----------------------------*/
/*--- Canvas Configuration ---*/
/*----------------------------*/
template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::SetUpCanvasStyle() {
  TGaxis::SetMaxDigits(3);
  gROOT->Reset();
  gROOT->SetBatch();
}

/*---------------------*/
/*--- Fit functions ---*/
/*---------------------*/
template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::ClearBestPars() {
  fFmin = 0.;
  fFmax = 0.;
  for (Int_t ip = 0; ip < fNPars; ++ip) fFPar[ip] = 0.;
  fNdof = 1.;
  fChi2 = 1e12;
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CopyToBestFit(TF1 f1, Double_t minEdge, Double_t maxEdge) {
  fFmin = minEdge;
  fFmax = maxEdge;
  for (Int_t ip = 0; ip < fNPars; ++ip) fFPar[ip] = f1.GetParameter(ip);
  fNdof = f1.GetNDF();
  fChi2 = f1.GetChisquare();
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::LocateMaximum(TH1D *h1, Double_t minMass, Double_t maxMass, Double_t &hAmpPeak,
                                                      Double_t &hMaxPeak) {
  // Search for corresponding bin range
  Int_t minBin = h1->GetXaxis()->FindBin(minMass);
  Int_t maxBin = h1->GetXaxis()->FindBin(maxMass);
  // Avoid underflow and overflow bins
  if (maxBin == h1->GetXaxis()->GetNbins() + 1) --maxBin;
  if (minBin == 0) ++minBin;
  // Look for maximum amplitude and position
  hAmpPeak = -1.;
  hMaxPeak = -1.;
  for (Int_t ib = minBin; ib <= maxBin; ++ib) {
    Double_t center = h1->GetXaxis()->GetBinCenter(ib);
    Double_t counts = h1->GetBinContent(ib);
    if (counts > hAmpPeak) {
      hAmpPeak = counts;
      hMaxPeak = center;
    }
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::InitializeFit(TF1 &f1, Int_t pionType, Double_t minEdge, Double_t maxEdge,
                                                      Double_t maxPeak, Double_t ampPeak) {
  f1.SetRange(minEdge, maxEdge);

  //	//symmetric
  //	f1.SetParName(0, "A");
  //	f1.SetParName(1, "#mu");
  //	f1.SetParName(2, "#sigma");
  //	f1.SetParName(3, "a");
  //	f1.SetParName(4, "b");
  //	f1.SetParName(5, "c");
  //	f1.SetParName(6, "d");
  // asymmetric
  f1.SetParName(0, "A");
  f1.SetParName(1, "#mu");
  f1.SetParName(2, "#sigma_{l}");
  f1.SetParName(3, "#sigma_{u}");
  f1.SetParName(4, "a");
  f1.SetParName(5, "b");
  f1.SetParName(6, "c");
  f1.SetParName(7, "d");

  //	//symmetric
  //	f1.SetParameter(0, ampPeak);
  //	f1.SetParameter(1, maxPeak);
  //	f1.SetParameter(2, 10.);
  //  //asymmetric
  f1.SetParameter(0, ampPeak);
  f1.SetParameter(1, maxPeak);
  f1.SetParameter(2, 10.);
  f1.SetParameter(3, 10.);

  if (fIsEta) {  // Eta
    f1.SetParLimits(0, 0.50 * ampPeak, 1.25 * ampPeak);
    f1.SetParLimits(1, maxPeak - 25., maxPeak + 25.);
    f1.SetParLimits(2, 2.5, 25.);
    f1.SetParLimits(3, 2.5, 25.);
  } else if (pionType == 0 || pionType == 3 || pionType == 4) {  // Type-I Pion
    f1.SetParLimits(0, 0.75 * ampPeak, 1.25 * ampPeak);
    f1.SetParLimits(1, maxPeak - 15., maxPeak + 15.);
    f1.SetParLimits(2, 2.5, 15.);
    f1.SetParLimits(3, 2.5, 15.);
  } else {  // Type-II Pion
    f1.SetParLimits(0, 0.50 * ampPeak, 1.25 * ampPeak);
    f1.SetParLimits(1, maxPeak - 15., maxPeak + 15.);
    f1.SetParLimits(2, 2.5, 15.);
    f1.SetParLimits(3, 2.5, 15.);
  }

  UT::Printf(UT::kPrintDebug, "\n\t\t\t Parameters initialization:\n");
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
    string parnam = f1.GetParName(ip);
    Double_t parval = f1.GetParameter(ip);
    Double_t parmin, parmax;
    f1.GetParLimits(ip, parmin, parmax);
    UT::Printf(UT::kPrintDebug, "\t\t\t %s : %f in [%f, %f]\n", parnam.c_str(), parval, parmin, parmax);
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::OptimizeRange(TH1D *h1, Int_t pionType, Double_t minEdge, Double_t maxEdge,
                                                      Double_t maxPeak, Double_t ampPeak) {
  ClearBestPars();

  Int_t minNbin = 5 * (fRebin / 1);
  Int_t minDist = 5 * (fRebin / 1);

  const Int_t minBin = h1->FindBin(minEdge);
  const Int_t maxBin = h1->FindBin(maxEdge);
  const Int_t ampBin = h1->FindBin(maxPeak);

  while (minBin > ampBin - minDist) {
    if (minDist <= 5) break;
    --minDist;
  }
  while (maxBin < ampBin + minDist) {
    if (minDist <= 5) break;
    --minDist;
  }
  while (maxBin - minBin < minNbin) {
    if (minNbin <= 5) break;
    --minNbin;
  }

  UT::Printf(UT::kPrintDebug, "\n\t\t Number of inferior bin for optimization %d\n", ampBin - minDist - minBin + 1);
  UT::Printf(UT::kPrintDebug, "\n\t\t Number of superior bin for optimization %d\n", maxBin - ampBin - minDist + 1);

  for (Int_t im = minBin; im <= ampBin - minDist; ++im)
    for (Int_t iM = maxBin; iM >= ampBin + minDist; --iM) {
      if ((iM - im) < minNbin) {
        continue;
      }

      const Double_t hEdgeMin = h1->GetXaxis()->GetBinLowEdge(im);
      const Double_t hEdgeMax = h1->GetXaxis()->GetBinLowEdge(iM);

      TF1 f1 = TF1(Form("Tmp_%s", h1->GetName()), asymmetricFunction, hEdgeMin, hEdgeMax, fNPars);

      InitializeFit(f1, pionType, hEdgeMin, hEdgeMax, maxPeak, ampPeak);

      UT::Printf(UT::kPrintDebug, "\n\t\t Optimization in %f-%f (%f in %f) \n\n", hEdgeMin, hEdgeMax, ampPeak, maxPeak);
      Double_t parval, parmin, parmax;
      f1.GetRange(parmin, parmax);
      UT::Printf(UT::kPrintDebug, "\t\t Fit Range [%f, %f] \n", parmin, parmax);
      for (Int_t ip = 0; ip < fNPars; ++ip) {
        parval = f1.GetParameter(ip);
        f1.GetParLimits(ip, parmin, parmax);
        UT::Printf(UT::kPrintDebug, "\t\t\t Parameter %d: %f in [%f, %f] \n", ip, parval, parmin, parmax);
      }

      // TFitResultPtr r1 = h1->Fit(&f1, Utils::fVerbose>1? "RIS" : "RISQ0");
      TFitResultPtr r1 = h1->Fit(&f1, "RISQ0");

      if (!r1->IsValid()) continue;
      if (f1.GetNDF() < minNbin - f1.GetNpar()) continue;
      if (f1.GetChisquare() / f1.GetNDF() > fChi2 / fNdof) continue;

      // Check is a parameter is ** at limit **
      Bool_t atlimit = false;
      for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
        Double_t parval = f1.GetParameter(ip);
        Double_t parmin, parmax;
        f1.GetParLimits(ip, parmin, parmax);
        if (TMath::Abs(parval / parmin - 1) < 1e-3 || TMath::Abs(parval / parmax - 1) < 1e-3) {
          UT::Printf(UT::kPrintDebug, "\n\t\t\t Limited Parameter %d = %f in [%f, %f]\n", ip, parval, parmin, parmax);
          atlimit = true;
        }
      }
      if (atlimit) continue;

      UT::Printf(UT::kPrintDebug, "\n\t\t Best Range Update %f %f (%.0f/%d=%.0f against %.0f/%.0f=%.0f) \n", hEdgeMin,
                 hEdgeMax, f1.GetChisquare(), f1.GetNDF(), f1.GetChisquare() / f1.GetNDF(), fChi2, fNdof,
                 fChi2 / fNdof);

      CopyToBestFit(f1, hEdgeMin, hEdgeMax);
    }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::SetUpFinalFit(TF1 &f1, Int_t pionType) {
  f1.SetRange(fFmin, fFmax);

  //	//symmetric
  //	f1.SetParName(0, "A");
  //	f1.SetParName(1, "#mu");
  //	f1.SetParName(2, "#sigma");
  //	f1.SetParName(3, "a");
  //	f1.SetParName(4, "b");
  //	f1.SetParName(5, "c");
  //	f1.SetParName(6, "d");
  // asymmetric
  f1.SetParName(0, "A");
  f1.SetParName(1, "#mu");
  f1.SetParName(2, "#sigma_{l}");
  f1.SetParName(3, "#sigma_{u}");
  f1.SetParName(4, "a");
  f1.SetParName(5, "b");
  f1.SetParName(6, "c");
  f1.SetParName(7, "d");

  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) f1.SetParameter(ip, fFPar[ip]);

  if (fIsEta) {  // Eta
    f1.SetParLimits(0, 0.75 * fFPar[0], 1.25 * fFPar[0]);
    f1.SetParLimits(1, fFPar[1] - 25., fFPar[1] + 25.);
    f1.SetParLimits(2, 2.5, 25.);
    f1.SetParLimits(3, 2.5, 25.);
  } else if (pionType == 0 || pionType == 3 || pionType == 4) {  // Type-I Pion
    f1.SetParLimits(0, 0.75 * fFPar[0], 1.25 * fFPar[0]);
    f1.SetParLimits(1, fFPar[1] - 15., fFPar[1] + 15.);
    f1.SetParLimits(2, 2.5, 15.);
    f1.SetParLimits(3, 2.5, 15.);
  } else {  // Type-II Pion
    f1.SetParLimits(0, 0.50 * fFPar[0], 1.25 * fFPar[0]);
    f1.SetParLimits(1, fFPar[1] - 15., fFPar[1] + 15.);
    f1.SetParLimits(2, 2.5, 15.);
    f1.SetParLimits(3, 2.5, 15.);
  }

  UT::Printf(UT::kPrintDebug, " Parameters final initialization:\n");
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip) {
    string parnam = f1.GetParName(ip);
    Double_t parval = f1.GetParameter(ip);
    Double_t parmin, parmax;
    f1.GetParLimits(ip, parmin, parmax);
    UT::Printf(UT::kPrintInfo, "\t%s : %f in [%f, %f]\n", parnam.c_str(), parval, parmin, parmax);
  }
}

template <typename arman, typename armclass>
Bool_t InvariantMassFit<arman, armclass>::FitMass(TH1D *h1, TF1 &f1) {
  UT::Printf(UT::kPrintInfo, "\n===  FITTING: %s\n\n", h1->GetName());

  /*
   * Get information from histogram name
   */

  TString name = h1->GetName();

  Bool_t isReference = false;
  if (name.Contains("Cumulative") && !name.Contains("xF")) {
    isReference = true;
  }
  Bool_t isRunbyRun = false;
  if (name.Contains("RunByRun")) {
    isRunbyRun = true;
  }
  Bool_t isxFbyxF = false;
  if (name.Contains("xF")) {
    isxFbyxF = true;
  }
  Int_t PionType = -1;
  for (Int_t ip = 0; ip < fNCategory; ++ip)
    if (name.Contains(fLabelType[ip].Data())) {
      PionType = ip;
    }
  if (PionType < 0) {
    UT::Printf(UT::kPrintError, "===             Attempting to fit an absurd Pion Type! Exit...\n");
    exit(EXIT_FAILURE);
  }

  h1->Rebin(fRebin);

  h1->GetXaxis()->SetRangeUser(fMassMin, fMassMax);

  // Position corresponding to maximum amplitude
  //	Double_t hAmpPeak = h1->GetMaximum();
  //	Double_t hMaxPeak = h1->GetXaxis()->GetBinCenter(h1->GetMaximumBin());
  Double_t hAmpPeak, hMaxPeak;
  LocateMaximum(h1, fMassMin, fMassMax, hAmpPeak, hMaxPeak);
  // Initial funmin-max range on small tower
  Double_t hEdgeMin = hMaxPeak * 0.75;
  Double_t hEdgeMax = hMaxPeak * 1.25;

  Double_t threshold = fThresholdMaximum;
  if (isRunbyRun) threshold /= 5.;
  if (isxFbyxF) threshold /= 2.;

  // Further rebin may be needed if statistics is limited
  while (hAmpPeak < threshold) {
    Int_t nbins = h1->GetXaxis()->GetNbins();

    Int_t nbinsInRange = h1->FindBin(fRangeMax) - h1->FindBin(fRangeMin);
    if (nbinsInRange < fNPars + 1) {
      UT::Printf(UT::kPrintInfo, "===             \t No additional rebinning is possible: skip it!\n");
      return kFALSE;
    }

    for (Int_t ir = 2; ir <= 10; ++ir) {
      if ((nbins % ir) == 0) {
        h1->Rebin(ir);

        // Position corresponding to maximum amplitude
        //				hAmpPeak = h1->GetMaximum();
        //				hMaxPeak = h1->GetXaxis()->GetBinCenter(h1->GetMaximumBin());
        LocateMaximum(h1, fMassMin, fMassMax, hAmpPeak, hMaxPeak);
        // Initial funmin-max range on small tower
        hEdgeMin = hMaxPeak * 0.75;
        hEdgeMax = hMaxPeak * 1.25;

        break;
      }
    }
  }

  // h1->GetXaxis()->UnZoom();

  if (fFixLimit && !isReference) {  // If fitting run-by-run with fixed limits
    fFunc_InvMassCumTot[PionType].GetRange(hEdgeMin, hEdgeMax);
  }

  /*
   * Define fitting range
   */

  ClearBestPars();

  if (fOptimize) {
    if (isReference || !fFixLimit) {  // Do not perform optimization if fitting run-by-run with fixed limits
      UT::Printf(UT::kPrintInfo, "===             \t Optimization\n\n");
      OptimizeRange(h1, PionType, hEdgeMin, hEdgeMax, hMaxPeak, hAmpPeak);
    }
  }

  UT::Printf(UT::kPrintInfo, "===             Fit in [%.2f, %.2f] (%.0f entries)\n", hEdgeMin, hEdgeMax,
             h1->Integral(h1->FindBin(hEdgeMin), h1->FindBin(hEdgeMax)));

  f1 = TF1(Form("Fit_%s", h1->GetName()), asymmetricFunction, hEdgeMin, hEdgeMax, fNPars);

  if (fNdof == 1. || fChi2 == 1e12) {  // If no optimization was performed
    UT::Printf(UT::kPrintInfo, "===             \t Fit\n");
    InitializeFit(f1, PionType, hEdgeMin, hEdgeMax, hMaxPeak, hAmpPeak);
  } else {  // If optimization was successfully performed
    UT::Printf(UT::kPrintInfo, "===             \t Final fit in [%.2f, %.2f] (%.0f entries)\n", fFmin, fFmax,
               h1->Integral(h1->FindBin(fFmin), h1->FindBin(fFmax)));
    SetUpFinalFit(f1, PionType);
  }

  /*
   * Fit
   */

  TFitResultPtr r1 = h1->Fit(&f1, "RIES");

  if (!r1->IsValid()) {
    UT::Printf(UT::kPrintInfo, "===             \t\t Valid=NO! Exit...\n");
    return kFALSE;
  }
  UT::Printf(UT::kPrintInfo, "\t\t Valid=YES\n");
  UT::Printf(UT::kPrintInfo, "  ChiSquare = %f with Ndof = %d\n", f1.GetChisquare(), f1.GetNDF());
  for (Int_t ip = 0; ip < f1.GetNpar(); ++ip)
    UT::Printf(UT::kPrintInfo, "   %s = %f ± %f\n", f1.GetParName(ip), f1.GetParameter(ip), f1.GetParError(ip));

  return kTRUE;
}

/*--------------------------*/
/*--- Fit Draw functions ---*/
/*--------------------------*/
template <typename arman, typename armclass>
Bool_t InvariantMassFit<arman, armclass>::DrawFit(TH1D *h1, TF1 &f1) {
  /*
   * Draw the fit on the distribution
   */

  // gStyle->SetOptStat("emr");
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1111);
  gStyle->SetTitleFontSize(0.065);

  gStyle->SetStatX(0.95);
  gStyle->SetStatY(0.85);
  gStyle->SetStatH(0.20);

  gStyle->SetFitFormat("3.3g");

  h1->Draw("HISTE");
  f1.Draw("LSAME");

  h1->GetXaxis()->SetRangeUser(fMassMin, fMassMax);

  gPad->Update();

  return kTRUE;
}

/*--------------------------*/
/*--- Fit Draw functions ---*/
/*--------------------------*/
template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateReference() {
  Utils::AllocateVector1D(fLine_Cumulative, fNCategory);
  Utils::AllocateVector1D(fLine_RunByRun, fNCategory);

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fHisto_InvMassCumTot[ip] == NULL) continue;
    const Double_t mass = fFunc_InvMassCumTot[ip].GetParameter(1);

    Double_t minxF = 0.;
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      minxF = ((Double_t)(ix)) / fNxFBin;
      break;
    }
    Double_t maxxF = 1.;
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      maxxF = ((Double_t)(ix + 1.0)) / fNxFBin;
      // break;
    }

    fLine_Cumulative[ip] = new TLine();

    fLine_Cumulative[ip]->SetX1(minxF);
    fLine_Cumulative[ip]->SetX2(maxxF);
    fLine_Cumulative[ip]->SetY1(mass);
    fLine_Cumulative[ip]->SetY2(mass);

    if (ip >= fPi0Type) {
      fLine_Cumulative[ip]->SetLineStyle(2);
    } else {
      fLine_Cumulative[ip]->SetLineStyle(1);
    }
    fLine_Cumulative[ip]->SetLineWidth(1);
    fLine_Cumulative[ip]->SetLineColor(fColor[ip]->GetNumber());

    fLine_RunByRun[ip] = new TLine();

    fLine_RunByRun[ip]->SetX1(fFirstRun - 1);
    fLine_RunByRun[ip]->SetX2(fLastRun + 1);
    fLine_RunByRun[ip]->SetY1(mass);
    fLine_RunByRun[ip]->SetY2(mass);

    if (ip >= fPi0Type) {
      fLine_RunByRun[ip]->SetLineStyle(2);
    } else {
      fLine_RunByRun[ip]->SetLineStyle(1);
    }
    fLine_RunByRun[ip]->SetLineWidth(1);
    fLine_RunByRun[ip]->SetLineColor(fColor[ip]->GetNumber());
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateCumulativeXfDependence() {
  Utils::AllocateVector1D(fGraph_Cumulative, fNCategory);
  Utils::AllocateVector1D(fSummary_Cumulative, fNCategory);
  //	Utils::AllocateVector1D(fLegend_Cumulative, fNCategory);

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    fGraph_Cumulative[ip] = new TGraphErrors();
    fGraph_Cumulative[ip]->SetName(Form("Graph_Cumulative_%s", fLabelType[ip].Data()));
    fGraph_Cumulative[ip]->SetTitle(
        Form("%s - Run %d-%d; x_{F}; M_{#gamma#gamma} [GeV/c^{2}]", fLabelType[ip].Data(), fFirstRun, fLastRun));

    Int_t jp = 0;
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;

      const Double_t xf = ((Double_t)(ix + 0.5)) / fNxFBin;
      const Double_t mass = fFunc_InvMassCumXf[ip][ix].GetParameter(1);
      const Double_t error = fFunc_InvMassCumXf[ip][ix].GetParError(1);

      fGraph_Cumulative[ip]->SetPoint(jp, xf, mass);
      fGraph_Cumulative[ip]->SetPointError(jp, 0, error);

      ++jp;
    }

    if (ip >= fPi0Type) {
      fGraph_Cumulative[ip]->SetMarkerStyle(kOpenCircle);
    } else {
      fGraph_Cumulative[ip]->SetMarkerStyle(kFullCircle);
    }
    fGraph_Cumulative[ip]->SetMarkerColor(fColor[ip]->GetNumber());
    fGraph_Cumulative[ip]->SetMarkerSize(2);
    fGraph_Cumulative[ip]->SetLineColor(fColor[ip]->GetNumber());
    fGraph_Cumulative[ip]->SetLineStyle(1);
    fGraph_Cumulative[ip]->SetLineWidth(1);

    if (fSummary_Cumulative[ip] == NULL) fSummary_Cumulative[ip] = new TMultiGraph();
    fSummary_Cumulative[ip]->Add(fGraph_Cumulative[ip]);

    //		if (fLegend_Cumulative[ip] == NULL) fLegend_Cumulative[ip] = new TLegend(0.905, 0.375, 1.000, 0.625);
    //		fLegend_Cumulative[ip]->AddEntry(fGraph_Cumulative[ip], "#scale[2.0]{Fit }", "lp");
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateCumulativeDeviation() {
  Utils::AllocateVector1D(fLine_Deviation_Cumulative, fNCategory);
  Utils::AllocateVector1D(fGraph_Deviation_Cumulative, fNCategory);
  fSummary_Deviation_Cumulative = new TMultiGraph();
  if (fNCategory > fPi0Type) {
    fLegend_Deviation_Cumulative = new TLegend(0.750, 0.750, 0.990, 0.990);
  } else {
    fLegend_Deviation_Cumulative = new TLegend(0.750, 0.850, 0.990, 0.990);
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fLine_Cumulative[ip] == NULL) continue;
    fLine_Deviation_Cumulative[ip] = (TLine *)fLine_Cumulative[ip]->Clone();
    Double_t mass1 = fLine_Deviation_Cumulative[ip]->GetY1();
    fLine_Deviation_Cumulative[ip]->SetY1(100. * ((mass1 / fMass) - 1.));
    Double_t mass2 = fLine_Deviation_Cumulative[ip]->GetY2();
    fLine_Deviation_Cumulative[ip]->SetY2(100. * ((mass2 / fMass) - 1.));
    if (ip >= fPi0Type) {
      fLine_Deviation_Cumulative[ip]->SetLineStyle(2);
    }
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_Cumulative[ip] == NULL) continue;
    fGraph_Deviation_Cumulative[ip] = (TGraphErrors *)fGraph_Cumulative[ip]->Clone();
    fGraph_Deviation_Cumulative[ip]->SetName(Form("Graph_Deviation_Cumulative_%s", fLabelType[ip].Data()));
    fGraph_Deviation_Cumulative[ip]->SetTitle(
        Form("%s - Run %d-%d; x_{F}; #DeltaM_{#gamma#gamma} [%]", fLabelType[ip].Data(), fFirstRun, fLastRun));
    for (Int_t jp = 0; jp < fGraph_Deviation_Cumulative[ip]->GetN(); ++jp) {
      Double_t mass = fGraph_Deviation_Cumulative[ip]->GetY()[jp];
      Double_t merr = fGraph_Deviation_Cumulative[ip]->GetEY()[jp];
      fGraph_Deviation_Cumulative[ip]->GetY()[jp] = 100. * ((mass / fMass) - 1.);
      fGraph_Deviation_Cumulative[ip]->GetEY()[jp] = 100. * (merr / fMass);
    }
    fSummary_Deviation_Cumulative->Add(fGraph_Deviation_Cumulative[ip]);
    fLegend_Deviation_Cumulative->AddEntry(fGraph_Deviation_Cumulative[ip],
                                           Form("#scale[2.0]{%s}", fLabelType[ip].Data()), "lp");
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateCumulativeCorrection() {
  Utils::AllocateVector1D(fLine_Correction_Cumulative, fNCategory);
  Utils::AllocateVector1D(fGraph_Correction_Cumulative, fNCategory);
  fSummary_Correction_Cumulative = new TMultiGraph();
  if (fNCategory > fPi0Type) {
    fLegend_Correction_Cumulative = new TLegend(0.750, 0.750, 0.990, 0.990);
  } else {
    fLegend_Correction_Cumulative = new TLegend(0.750, 0.850, 0.990, 0.990);
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fLine_Cumulative[ip] == NULL) continue;
    fLine_Correction_Cumulative[ip] = (TLine *)fLine_Cumulative[ip]->Clone();
    Double_t mass1 = fLine_Correction_Cumulative[ip]->GetY1();
    fLine_Correction_Cumulative[ip]->SetY1(fMass / mass1);
    Double_t mass2 = fLine_Correction_Cumulative[ip]->GetY2();
    fLine_Correction_Cumulative[ip]->SetY2(fMass / mass2);
    if (ip >= fPi0Type) {
      fLine_Correction_Cumulative[ip]->SetLineStyle(2);
    }
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_Cumulative[ip] == NULL) continue;
    fGraph_Correction_Cumulative[ip] = (TGraphErrors *)fGraph_Cumulative[ip]->Clone();
    fGraph_Correction_Cumulative[ip]->SetName(Form("Graph_Correction_Cumulative_%s", fLabelType[ip].Data()));
    fGraph_Correction_Cumulative[ip]->SetTitle(
        Form("%s - Run %d-%d; x_{F}; Correction", fLabelType[ip].Data(), fFirstRun, fLastRun));
    for (Int_t jp = 0; jp < fGraph_Correction_Cumulative[ip]->GetN(); ++jp) {
      Double_t mass = fGraph_Correction_Cumulative[ip]->GetY()[jp];
      Double_t merr = fGraph_Correction_Cumulative[ip]->GetEY()[jp];
      fGraph_Correction_Cumulative[ip]->GetY()[jp] = fMass / mass;
      fGraph_Correction_Cumulative[ip]->GetEY()[jp] = (merr / mass) * (fMass / mass);
    }
    fSummary_Correction_Cumulative->Add(fGraph_Correction_Cumulative[ip]);
    fLegend_Correction_Cumulative->AddEntry(fGraph_Correction_Cumulative[ip],
                                            Form("#scale[2.0]{%s}", fLabelType[ip].Data()), "lp");
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::DrawCumulativeSummary() {
  CreateCumulativeXfDependence();

  TGaxis::SetMaxDigits(9);

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fSummary_Cumulative[ip] == NULL) continue;

    TString canvasName =
        Form("%s/Summary_Cumulative_%s_%d_%d.eps", fOutputDirName.Data(), fLabelType[ip].Data(), fFirstRun, fLastRun);
    TCanvas *c = new TCanvas("Summary", "", 600, 600);
    c->cd();

    //		fLegend_Cumulative[ip]->SetTextSize(0.03);
    fSummary_Cumulative[ip]->SetTitle(
        Form("%s - Run %d-%d; x_{F}; M_{#gamma#gamma} [GeV/c^{2}]", fLabelType[ip].Data(), fFirstRun, fLastRun));
    fSummary_Cumulative[ip]->Draw("AP");
    //		fLegend_Cumulative[ip]->Draw();
    if (fLine_Cumulative[ip]) fLine_Cumulative[ip]->Draw();

    Double_t minxF = 0.;
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      minxF = ((Double_t)(ix)) / fNxFBin;
      break;
    }
    Double_t maxxF = 1.;
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      maxxF = ((Double_t)(ix + 1.0)) / fNxFBin;
      // break;
    }

    gPad->Update();
    fSummary_Cumulative[ip]->GetXaxis()->SetLimits(minxF, maxxF);
    fSummary_Cumulative[ip]->GetYaxis()->SetTitleOffset(1.40);
    gPad->Update();
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->Update();

    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }

  CreateCumulativeDeviation();

  TString canvasName = Form("%s/Summary_Cumulative_Deviation_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  TCanvas *c = new TCanvas("Deviation", "", 600, 600);
  c->cd();
  //		fSummary_Deviation_Cumulative->SetTextSize(0.03);
  fSummary_Deviation_Cumulative->SetTitle(Form("Run %d-%d; x_{F}; #DeltaM_{#gamma#gamma} [%]", fFirstRun, fLastRun));
  fSummary_Deviation_Cumulative->Draw("AP");
  fLegend_Deviation_Cumulative->Draw();

  Double_t maxxF = +0.;
  Double_t minxF = +1.;
  for (Int_t ip = 0; ip < fNCategory; ++ip)
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      if (((Double_t)(ix)) / fNxFBin < minxF) minxF = ((Double_t)(ix)) / fNxFBin;
      if (((Double_t)(ix + 1.0)) / fNxFBin > maxxF) maxxF = ((Double_t)(ix + 1.0)) / fNxFBin;
    }
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fLine_Deviation_Cumulative[ip]) {
      fLine_Deviation_Cumulative[ip]->SetX1(minxF);
      fLine_Deviation_Cumulative[ip]->SetX2(maxxF);
      fLine_Deviation_Cumulative[ip]->Draw();
    }
  }
  Double_t maxdv = -1.;
  Double_t mindv = +1.;
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_Deviation_Cumulative[ip] == NULL) continue;
    for (Int_t jp = 0; jp < fGraph_Deviation_Cumulative[ip]->GetN(); ++jp) {
      Double_t deviation = fGraph_Deviation_Cumulative[ip]->GetY()[jp];
      if (deviation < mindv) mindv = deviation;
      if (deviation > maxdv) maxdv = deviation;
    }
  }

  gPad->Update();
  fSummary_Deviation_Cumulative->GetXaxis()->SetLimits(minxF, maxxF);
  fSummary_Deviation_Cumulative->GetYaxis()->SetLimits(mindv - 5., maxdv + 5.);
  fSummary_Deviation_Cumulative->GetYaxis()->SetTitleOffset(1.40);
  gPad->Update();
  gPad->SetGridx();
  gPad->SetGridy();
  gPad->Update();

  c->Print(canvasName.Data());
  c->Close();
  delete c;

  CreateCumulativeCorrection();

  canvasName = Form("%s/Summary_Cumulative_Correction_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  c = new TCanvas("Correction", "", 600, 600);
  c->cd();
  //		fSummary_Correction_Cumulative->SetTextSize(0.03);
  fSummary_Correction_Cumulative->SetTitle(Form("Run %d-%d; x_{F}; Correction", fFirstRun, fLastRun));
  fSummary_Correction_Cumulative->Draw("AP");
  fLegend_Correction_Cumulative->Draw();

  maxxF = +0.;
  minxF = +1.;
  for (Int_t ip = 0; ip < fNCategory; ++ip)
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      if (((Double_t)(ix)) / fNxFBin < minxF) minxF = ((Double_t)(ix)) / fNxFBin;
      if (((Double_t)(ix + 1.0)) / fNxFBin > maxxF) maxxF = ((Double_t)(ix + 1.0)) / fNxFBin;
    }
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fLine_Correction_Cumulative[ip]) {
      fLine_Correction_Cumulative[ip]->SetX1(minxF);
      fLine_Correction_Cumulative[ip]->SetX2(maxxF);
      fLine_Correction_Cumulative[ip]->Draw();
    }
  }
  maxdv = -1.;
  mindv = +1.;
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_Correction_Cumulative[ip] == NULL) continue;
    for (Int_t jp = 0; jp < fGraph_Correction_Cumulative[ip]->GetN(); ++jp) {
      Double_t Correction = fGraph_Correction_Cumulative[ip]->GetY()[jp];
      if (Correction < mindv) mindv = Correction;
      if (Correction > maxdv) maxdv = Correction;
    }
  }

  gPad->Update();
  fSummary_Correction_Cumulative->GetXaxis()->SetLimits(minxF, maxxF);
  fSummary_Correction_Cumulative->GetYaxis()->SetLimits(mindv - 5., maxdv + 5.);
  fSummary_Correction_Cumulative->GetYaxis()->SetTitleOffset(1.40);
  gPad->Update();
  gPad->SetGridx();
  gPad->SetGridy();
  gPad->Update();

  c->Print(canvasName.Data());
  c->Close();
  delete c;
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateRunByRunVariation() {
  Utils::AllocateVector1D(fGraph_RunByRun, fNCategory);
  Utils::AllocateVector1D(fSummary_RunByRun, fNCategory);
  //	Utils::AllocateVector1D(fLegend_RunByRun, fNCategory);

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    fGraph_RunByRun[ip] = new TGraphErrors();
    fGraph_RunByRun[ip]->SetName(Form("Graph_RunByRun_%s", fLabelType[ip].Data()));
    fGraph_RunByRun[ip]->SetTitle(
        Form("%s - Run %d-%d; Run; M_{#gamma#gamma} [GeV/c^{2}]", fLabelType[ip].Data(), fFirstRun, fLastRun));

    Int_t jp = 0;
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (fHisto_InvMassRunTot[ip][ir] == NULL) continue;
      if (fHisto_InvMassRunTot[ip][ir]->Integral(fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMin),
                                                 fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMax)) <
          fThresholdEntries / 5.)
        continue;

      const Double_t run = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
      const Double_t mass = fFunc_InvMassRunTot[ip][ir].GetParameter(1);
      const Double_t error = fFunc_InvMassRunTot[ip][ir].GetParError(1);

      fGraph_RunByRun[ip]->SetPoint(jp, run, mass);
      fGraph_RunByRun[ip]->SetPointError(jp, 0, error);

      ++jp;
    }

    fGraph_RunByRun[ip]->SetMarkerStyle(22);
    fGraph_RunByRun[ip]->SetMarkerColor(fColor[ip]->GetNumber());
    fGraph_RunByRun[ip]->SetMarkerSize(2);
    fGraph_RunByRun[ip]->SetLineColor(fColor[ip]->GetNumber());
    fGraph_RunByRun[ip]->SetLineStyle(1);
    fGraph_RunByRun[ip]->SetLineWidth(1);

    if (fSummary_RunByRun[ip] == NULL) fSummary_RunByRun[ip] = new TMultiGraph();
    fSummary_RunByRun[ip]->Add(fGraph_RunByRun[ip]);

    //		if (fLegend_RunByRun[ip] == NULL) fLegend_RunByRun[ip] = new TLegend(0.905, 0.375, 1.000, 0.625);
    //		fLegend_RunByRun[ip]->AddEntry(fGraph_RunByRun[ip], "#scale[2.0]{Fit }", "lp");
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateRunByRunDeviation() {
  Utils::AllocateVector1D(fLine_Deviation_RunByRun, fNCategory);
  Utils::AllocateVector1D(fGraph_Deviation_RunByRun, fNCategory);
  fSummary_Deviation_RunByRun = new TMultiGraph();
  if (fNCategory > fPi0Type) {
    fLegend_Deviation_RunByRun = new TLegend(0.750, 0.750, 0.990, 0.990);
  } else {
    fLegend_Deviation_RunByRun = new TLegend(0.750, 0.850, 0.990, 0.990);
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (!fCumulative) continue;
    if (fLine_RunByRun[ip] == NULL) continue;
    fLine_Deviation_RunByRun[ip] = (TLine *)fLine_RunByRun[ip]->Clone();
    Double_t mass1 = fLine_Deviation_RunByRun[ip]->GetY1();
    fLine_Deviation_RunByRun[ip]->SetY1(100. * ((mass1 / fMass) - 1.));
    Double_t mass2 = fLine_Deviation_RunByRun[ip]->GetY2();
    fLine_Deviation_RunByRun[ip]->SetY2(100. * ((mass2 / fMass) - 1.));
    if (ip >= fPi0Type) {
      fLine_Deviation_RunByRun[ip]->SetLineStyle(2);
    }
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_RunByRun[ip] == NULL) continue;
    fGraph_Deviation_RunByRun[ip] = (TGraphErrors *)fGraph_RunByRun[ip]->Clone();
    fGraph_Deviation_RunByRun[ip]->SetName(Form("Graph_Deviation_RunByRun_%s", fLabelType[ip].Data()));
    fGraph_Deviation_RunByRun[ip]->SetTitle(
        Form("%s - Run %d-%d; x_{F}; #DeltaM_{#gamma#gamma} [%]", fLabelType[ip].Data(), fFirstRun, fLastRun));
    for (Int_t jp = 0; jp < fGraph_Deviation_RunByRun[ip]->GetN(); ++jp) {
      Double_t mass = fGraph_Deviation_RunByRun[ip]->GetY()[jp];
      Double_t merr = fGraph_Deviation_RunByRun[ip]->GetEY()[jp];
      fGraph_Deviation_RunByRun[ip]->GetY()[jp] = 100. * ((mass / fMass) - 1.);
      fGraph_Deviation_RunByRun[ip]->GetEY()[jp] = 100. * (merr / fMass);
    }
    fSummary_Deviation_RunByRun->Add(fGraph_Deviation_RunByRun[ip]);
    fLegend_Deviation_RunByRun->AddEntry(fGraph_Deviation_RunByRun[ip], Form("#scale[2.0]{%s}", fLabelType[ip].Data()),
                                         "lp");
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::CreateRunByRunCorrection() {
  Utils::AllocateVector1D(fLine_Correction_RunByRun, fNCategory);
  Utils::AllocateVector1D(fGraph_Correction_RunByRun, fNCategory);
  fSummary_Correction_RunByRun = new TMultiGraph();
  if (fNCategory > fPi0Type) {
    fLegend_Correction_RunByRun = new TLegend(0.750, 0.750, 0.990, 0.990);
  } else {
    fLegend_Correction_RunByRun = new TLegend(0.750, 0.850, 0.990, 0.990);
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (!fCumulative) continue;
    if (fLine_RunByRun[ip] == NULL) continue;
    fLine_Correction_RunByRun[ip] = (TLine *)fLine_RunByRun[ip]->Clone();
    Double_t mass1 = fLine_Correction_RunByRun[ip]->GetY1();
    fLine_Correction_RunByRun[ip]->SetY1(fMass / mass1);
    Double_t mass2 = fLine_Correction_RunByRun[ip]->GetY2();
    fLine_Correction_RunByRun[ip]->SetY2(fMass / mass2);
    if (ip >= fPi0Type) {
      fLine_Correction_RunByRun[ip]->SetLineStyle(2);
    }
  }

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_RunByRun[ip] == NULL) continue;
    fGraph_Correction_RunByRun[ip] = (TGraphErrors *)fGraph_RunByRun[ip]->Clone();
    fGraph_Correction_RunByRun[ip]->SetName(Form("Graph_Correction_RunByRun_%s", fLabelType[ip].Data()));
    fGraph_Correction_RunByRun[ip]->SetTitle(
        Form("%s - Run %d-%d; x_{F}; Correction", fLabelType[ip].Data(), fFirstRun, fLastRun));
    for (Int_t jp = 0; jp < fGraph_Correction_RunByRun[ip]->GetN(); ++jp) {
      Double_t mass = fGraph_Correction_RunByRun[ip]->GetY()[jp];
      Double_t merr = fGraph_Correction_RunByRun[ip]->GetEY()[jp];
      fGraph_Correction_RunByRun[ip]->GetY()[jp] = fMass / mass;
      fGraph_Correction_RunByRun[ip]->GetEY()[jp] = (merr / mass) * (fMass / mass);
    }
    fSummary_Correction_RunByRun->Add(fGraph_Correction_RunByRun[ip]);
    fLegend_Correction_RunByRun->AddEntry(fGraph_Correction_RunByRun[ip],
                                          Form("#scale[2.0]{%s}", fLabelType[ip].Data()), "lp");
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::DrawRunByRunSummary() {
  CreateRunByRunVariation();

  TGaxis::SetMaxDigits(9);

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fSummary_RunByRun[ip] == NULL) continue;

    TString canvasName =
        Form("%s/Summary_RunByRun_%s_%d_%d.eps", fOutputDirName.Data(), fLabelType[ip].Data(), fFirstRun, fLastRun);
    TCanvas *c = new TCanvas("Summary", "", 600, 600);
    c->cd();

    //		fLegend_RunByRun[ip]->SetTextSize(0.03);
    fSummary_RunByRun[ip]->SetTitle(
        Form("%s - Run %d-%d; Run; M_{#gamma#gamma} [GeV/c^{2}]", fLabelType[ip].Data(), fFirstRun, fLastRun));
    fSummary_RunByRun[ip]->Draw("AP");
    //		fLegend_RunByRun[ip]->Draw();
    if (fCumulative)
      if (fLine_RunByRun[ip]) fLine_RunByRun[ip]->Draw();

    gPad->Update();
    fSummary_RunByRun[ip]->GetXaxis()->SetLimits(fFirstRun - 1, fLastRun + 1);
    fSummary_RunByRun[ip]->GetYaxis()->SetTitleOffset(1.40);
    gPad->Update();
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->Update();

    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }

  CreateRunByRunDeviation();

  TString canvasName = Form("%s/Summary_RunByRun_Deviation_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  TCanvas *c = new TCanvas("Deviation", "", 600, 600);
  c->cd();
  //		fSummary_Deviation_RunByRun->SetTextSize(0.03);
  fSummary_Deviation_RunByRun->SetTitle(Form("Run %d-%d; Run; #DeltaM_{#gamma#gamma} [%]", fFirstRun, fLastRun));
  fSummary_Deviation_RunByRun->Draw("AP");
  fLegend_Deviation_RunByRun->Draw();

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fCumulative)
      if (fLine_Deviation_RunByRun[ip]) {
        fLine_Deviation_RunByRun[ip]->SetX1(fFirstRun - 1);
        fLine_Deviation_RunByRun[ip]->SetX2(fLastRun + 1);
        fLine_Deviation_RunByRun[ip]->Draw();
      }
  }
  Double_t maxdv = -1.;
  Double_t mindv = +1.;
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_Deviation_RunByRun[ip] == NULL) continue;
    for (Int_t jp = 0; jp < fGraph_Deviation_RunByRun[ip]->GetN(); ++jp) {
      Double_t deviation = fGraph_Deviation_RunByRun[ip]->GetY()[jp];
      if (deviation < mindv) mindv = deviation;
      if (deviation > maxdv) maxdv = deviation;
    }
  }

  gPad->Update();
  fSummary_Deviation_RunByRun->GetXaxis()->SetLimits(fFirstRun - 1, fLastRun + 1);
  fSummary_Deviation_RunByRun->GetYaxis()->SetLimits(mindv - 5., maxdv + 5.);
  fSummary_Deviation_RunByRun->GetYaxis()->SetTitleOffset(1.40);
  gPad->Update();
  gPad->SetGridx();
  gPad->SetGridy();
  gPad->Update();

  c->Print(canvasName.Data());
  c->Close();
  delete c;

  CreateRunByRunCorrection();

  canvasName = Form("%s/Summary_RunByRun_Correction_%d_%d.eps", fOutputDirName.Data(), fFirstRun, fLastRun);
  c = new TCanvas("Correction", "", 600, 600);
  c->cd();
  //		fSummary_Correction_RunByRun->SetTextSize(0.03);
  fSummary_Correction_RunByRun->SetTitle(Form("Run %d-%d; Run; Correction", fFirstRun, fLastRun));
  fSummary_Correction_RunByRun->Draw("AP");
  fLegend_Correction_RunByRun->Draw();

  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fCumulative)
      if (fLine_Correction_RunByRun[ip]) {
        fLine_Correction_RunByRun[ip]->SetX1(fFirstRun - 1);
        fLine_Correction_RunByRun[ip]->SetX2(fLastRun + 1);
        fLine_Correction_RunByRun[ip]->Draw();
      }
  }
  maxdv = -1.;
  mindv = +1.;
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fGraph_Correction_RunByRun[ip] == NULL) continue;
    for (Int_t jp = 0; jp < fGraph_Correction_RunByRun[ip]->GetN(); ++jp) {
      Double_t Correction = fGraph_Correction_RunByRun[ip]->GetY()[jp];
      if (Correction < mindv) mindv = Correction;
      if (Correction > maxdv) maxdv = Correction;
    }
  }

  gPad->Update();
  fSummary_Correction_RunByRun->GetXaxis()->SetLimits(fFirstRun - 1, fLastRun + 1);
  fSummary_Correction_RunByRun->GetYaxis()->SetLimits(mindv - 5., maxdv + 5.);
  fSummary_Correction_RunByRun->GetYaxis()->SetTitleOffset(1.40);
  gPad->Update();
  gPad->SetGridx();
  gPad->SetGridy();
  gPad->Update();

  c->Print(canvasName.Data());
  c->Close();
  delete c;
}

/*--------------------------*/
/*--- Handling functions ---*/
/*--------------------------*/
template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::FitCumulative() {
  Utils::AllocateVector1D(fFunc_InvMassCumTot, fNCategory);
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    if (fHisto_InvMassCumTot[ip] == NULL) continue;
    if (fHisto_InvMassCumTot[ip]->Integral(fHisto_InvMassCumTot[ip]->FindBin(fRangeMin),
                                           fHisto_InvMassCumTot[ip]->FindBin(fRangeMax)) < fThresholdEntries) {
      delete fHisto_InvMassCumTot[ip];
      fHisto_InvMassCumTot[ip] = NULL;
      continue;
    }
    if (!FitMass(fHisto_InvMassCumTot[ip], fFunc_InvMassCumTot[ip])) {
      delete fHisto_InvMassCumTot[ip];
      fHisto_InvMassCumTot[ip] = NULL;
    }
  }
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    TString canvasName =
        Form("%s/Fit_Cumulative_%s_%d_%d.eps", fOutputDirName.Data(), fLabelType[ip].Data(), fFirstRun, fLastRun);
    TCanvas *c = new TCanvas(Form("FitCumTot%s", fLabelType[ip].Data()), "", 750, 600);
    c->cd();
    if (fHisto_InvMassCumTot[ip] == NULL) continue;
    if (fHisto_InvMassCumTot[ip]->Integral(fHisto_InvMassCumTot[ip]->FindBin(fRangeMin),
                                           fHisto_InvMassCumTot[ip]->FindBin(fRangeMax)) < fThresholdEntries)
      continue;
    DrawFit(fHisto_InvMassCumTot[ip], fFunc_InvMassCumTot[ip]);
    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }

  Utils::AllocateVector2D(fFunc_InvMassCumXf, fNCategory, fNxFBin);
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries) {
        delete fHisto_InvMassCumXf[ip][ix];
        fHisto_InvMassCumXf[ip][ix] = NULL;
        continue;
      }
      if (!FitMass(fHisto_InvMassCumXf[ip][ix], fFunc_InvMassCumXf[ip][ix])) {
        delete fHisto_InvMassCumXf[ip][ix];
        fHisto_InvMassCumXf[ip][ix] = NULL;
      }
    }
  }
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    TString canvasName =
        Form("%s/Fit_Cumulative_xF_%s_%d_%d.eps", fOutputDirName.Data(), fLabelType[ip].Data(), fFirstRun, fLastRun);
    TCanvas *c = new TCanvas(Form("FitCumXf", fLabelType[ip].Data()), "", 750, 600);
    c->Divide(5, 4);
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      c->cd(1 + ix);
      Double_t thisxF = ((Double_t)(ix + 0.5)) / fNxFBin;
      if (fHisto_InvMassCumXf[ip][ix] == NULL) continue;
      if (fHisto_InvMassCumXf[ip][ix]->Integral(fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMin),
                                                fHisto_InvMassCumXf[ip][ix]->FindBin(fRangeMax)) < fThresholdEntries)
        continue;
      DrawFit(fHisto_InvMassCumXf[ip][ix], fFunc_InvMassCumXf[ip][ix]);
    }
    c->Print(canvasName.Data());
    c->Close();
    delete c;
  }
}

template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::FitRunByRun() {
  Utils::AllocateVector2D(fFunc_InvMassRunTot, fNCategory, fNRun);
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      if (fHisto_InvMassRunTot[ip][ir] == NULL) continue;
      if (fHisto_InvMassRunTot[ip][ir]->Integral(fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMin),
                                                 fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMax)) <
          fThresholdEntries / 5.) {
        delete fHisto_InvMassRunTot[ip][ir];
        fHisto_InvMassRunTot[ip][ir] = NULL;
        continue;
      }
      if (!FitMass(fHisto_InvMassRunTot[ip][ir], fFunc_InvMassRunTot[ip][ir])) {
        delete fHisto_InvMassRunTot[ip][ir];
        fHisto_InvMassRunTot[ip][ir] = NULL;
      }
    }
  }
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    TString canvasName =
        Form("%s/Fit_RunByRun_%s_%d_%d.pdf", fOutputDirName.Data(), fLabelType[ip].Data(), fFirstRun, fLastRun);
    TCanvas *c = new TCanvas(Form("FitRunTot%s", fLabelType[ip].Data()), "", 750, 600);
    c->Print(Form("%s[", canvasName.Data()));
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      c->Update();
      c->cd();
      if (fHisto_InvMassRunTot[ip][ir] == NULL) continue;
      if (fHisto_InvMassRunTot[ip][ir]->Integral(fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMin),
                                                 fHisto_InvMassRunTot[ip][ir]->FindBin(fRangeMax)) <
          fThresholdEntries / 5.)
        continue;
      DrawFit(fHisto_InvMassRunTot[ip][ir], fFunc_InvMassRunTot[ip][ir]);
      c->Update();
      c->Print(Form("%s", canvasName.Data()));
    }
    c->Print(Form("%s]", canvasName.Data()));
    c->Close();
    delete c;
  }

  Utils::AllocateVector3D(fFunc_InvMassRunXf, fNCategory, fNxFBin, fNRun);
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      for (Int_t ir = 0; ir < fNRun; ++ir) {
        if (fHisto_InvMassRunXf[ip][ix][ir] == NULL) continue;
        if (fHisto_InvMassRunXf[ip][ix][ir]->Integral(fHisto_InvMassRunXf[ip][ix][ir]->FindBin(fRangeMin),
                                                      fHisto_InvMassRunXf[ip][ix][ir]->FindBin(fRangeMax)) <
            fThresholdEntries / 10.) {
          delete fHisto_InvMassRunXf[ip][ix][ir];
          fHisto_InvMassRunXf[ip][ix][ir] = NULL;
          continue;
        }
        if (!FitMass(fHisto_InvMassRunXf[ip][ix][ir], fFunc_InvMassRunXf[ip][ix][ir])) {
          delete fHisto_InvMassRunXf[ip][ix][ir];
          fHisto_InvMassRunXf[ip][ix][ir] = NULL;
        }
      }
    }
  }
  for (Int_t ip = 0; ip < fNCategory; ++ip) {
    TString canvasName =
        Form("%s/Fit_RunByRun_xF_%s_%d_%d.pdf", fOutputDirName.Data(), fLabelType[ip].Data(), fFirstRun, fLastRun);
    TCanvas *c = new TCanvas(Form("FitRunXf%s", fLabelType[ip].Data()), "", 750, 600);
    c->Print(Form("%s[", canvasName.Data()));
    c->Divide(5, 4);
    for (Int_t ir = 0; ir < fNRun; ++ir) {
      c->Update();
      for (Int_t ix = 0; ix < fNxFBin; ++ix) {
        c->cd(1 + ix);
        gPad->Clear();
        if (fHisto_InvMassRunXf[ip][ix][ir] == NULL) continue;
        if (fHisto_InvMassRunXf[ip][ix][ir]->Integral(fHisto_InvMassRunXf[ip][ix][ir]->FindBin(fRangeMin),
                                                      fHisto_InvMassRunXf[ip][ix][ir]->FindBin(fRangeMax)) <
            fThresholdEntries / 10.)
          continue;
        DrawFit(fHisto_InvMassRunXf[ip][ix][ir], fFunc_InvMassRunXf[ip][ix][ir]);
      }
      c->Update();
      c->Print(Form("%s", canvasName.Data()));
    }
    c->Print(Form("%s]", canvasName.Data()));
    c->Close();
    delete c;
  }
}

/*------------------*/
/*--- Class Core ---*/
/*------------------*/
template <typename arman, typename armclass>
void InvariantMassFit<arman, armclass>::Run() {
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
  ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(0);

  fNCategory = fIsEta ? fEtaType : fPi0Type;
  if (fXfTower) {
    fNCategory += fNTower;
  }

  if (fIsEta) {
    if (fRebin == 1) fRebin = 5;
    fMass = fEtaMass;
    fMassMin = fEtaMin;
    fMassMax = fEtaMax;
    fRangeMin = fMassMin;
    fRangeMax = fMassMax;
    //    fRangeMin = TMath::Max(0.50*fMass, fMassMin);
    //    fRangeMax = TMath::Min(1.25*fMass, fMassMax);
  } else {
    if (fRebin == 1) fRebin = 2;
    fMass = fPi0Mass;
    fMassMin = fPi0Min;
    fMassMax = fPi0Max;
    fRangeMin = fMassMin;
    fRangeMax = fMassMax;
    //    fRangeMin = TMath::Max(0.50*fMass, fMassMin);
    //    fRangeMax = TMath::Min(1.25*fMass, fMassMax);
  }

  GetInputHistograms();
  SetUpCanvasStyle();

  if (fCumulative) FitCumulative();
  if (fRunByRun) FitRunByRun();

  if (fCumulative) {
    CreateReference();
  }

  if (fCumulative) DrawCumulativeSummary();
  if (fRunByRun) DrawRunByRunSummary();

  WriteToOutput();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class InvariantMassFit<Arm1AnPars, Arm1Params>;
template class InvariantMassFit<Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
