#include <TRint.h>

#include "InvariantMassFit.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput file\n");
  printf("\t-o <output>\t\tOutput folder\n");
  printf("\t-f <first>\t\tFirst run to be fitted (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be fitted (default = -1)\n");
  printf("\t-n <rebin>\t\tRebin factor (default = automatically optimized)\n");
  printf("\t--arm1\t\t\tArm1 (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--eta\t\t\tFit η meson instead of neutral π mass\n");
  printf("\t--cumulative\t\tFit the cumulative distribution (Default: False)\n");
  printf("\t--run-by-run\t\tFit the run-by-run distribution (Default: False)\n");
  printf("\t--xf-tower\t\tFit the tower xF distribution (Default: False)\n");
  printf("\t--optimize\t\tPerform Fit range optimization - better but slower (Default: False but Recommended)\n");
  printf("\t--fixlimit\t\tPerform run-by-run with the same range of cumulative (Default: False but Recommended)\n");
  printf("\t--official\t\tFormat the canvas in the official style (Default: False)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string infile = "";
  string outdir = "";
  Int_t first_run = -1;
  Int_t last_run = -1;
  Int_t rebin = 1;
  Int_t arm = -1;
  Bool_t eta = false;
  Bool_t cumulative = false;
  Bool_t run_by_run = false;
  Bool_t xf_tower = false;
  Bool_t optimize = false;
  Bool_t fixlimit = false;
  Bool_t official = false;
  Int_t verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"rebin", required_argument, NULL, 'n'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"eta", no_argument, NULL, 1101},
      {"cumulative", no_argument, NULL, 1102},
      {"run-by-run", no_argument, NULL, 1103},
      {"xf-tower", no_argument, NULL, 1104},
      {"optimize", no_argument, NULL, 1201},
      {"fixlimit", no_argument, NULL, 1202},
      {"official", no_argument, NULL, 1303},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) infile = optarg;
        break;
      case 'o':
        if (optarg) outdir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'n':
        if (optarg) rebin = atoi(optarg);
        break;
      case 1001:
        arm = Arm1Params::kArmIndex;
        break;
      case 1002:
        arm = Arm2Params::kArmIndex;
        break;
      case 1101:
        eta = true;
        break;
      case 1102:
        cumulative = true;
        break;
      case 1103:
        run_by_run = true;
        break;
      case 1104:
        xf_tower = true;
        break;
      case 1201:
        optimize = true;
        break;
      case 1202:
        fixlimit = true;
        break;
      case 1303:
        official = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (infile == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (outdir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (!(cumulative || run_by_run)) {
    Utils::Printf(Utils::kPrintError, "\nPlease specify at least one between --cumulative and --run_by_runn");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (fixlimit && !cumulative) {
    Utils::Printf(Utils::kPrintError, "\nCannot fix run-by-run range limits without cumulative fit\n");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (official) {
    Utils::Printf(Utils::kPrintError, "\nOption \"official\" not yet implemented! Sorry...\n");
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  InvariantMassFit<Arm1AnPars, Arm1Params> bcf_1;
  InvariantMassFit<Arm2AnPars, Arm2Params> bcf_2;
  if (arm == Arm1Params::kArmIndex) {
    bcf_1.SetInputFile(infile.c_str());
    bcf_1.SetOutputDir(outdir.c_str());
    bcf_1.SetBinFactor(rebin);
    bcf_1.SetRunRange(first_run, last_run);
    bcf_1.SetFitOption(eta, cumulative, run_by_run, xf_tower, optimize, fixlimit, official);
    bcf_1.Run();
  } else if (arm == Arm2Params::kArmIndex) {
    bcf_2.SetInputFile(infile.c_str());
    bcf_2.SetOutputDir(outdir.c_str());
    bcf_2.SetBinFactor(rebin);
    bcf_2.SetRunRange(first_run, last_run);
    bcf_2.SetFitOption(eta, cumulative, run_by_run, xf_tower, optimize, fixlimit, official);
    bcf_2.Run();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
