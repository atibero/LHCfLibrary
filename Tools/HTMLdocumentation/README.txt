Automatic Documentation for LHCf Softwares 

How to generate the html documents 
 
1. Install doxygen in your system 

2. Generate the document 
     make clean;
     make 

3. Upload the html files to lhcfs1
    make upload

Log:
 30 Nov. 2018 by Menjo 
    Create Doxyfile 
    The parameters have not been optimized yet. 


Memo for developers:
Important paramters in Doxyfile
INPUT               <-  The path to the main directory is given in Makefile
OUTPUT_DIRECTORY    <-  Given in Makefile 
RECURSIVE           -> Yes : Search sources in the subdirectires also. 
SOURCE_BROWSER      -> Yes : Include the sources into the documents

Useful webpages 
http://algo13.net/doxygen/doxyfile.html (Japanese)
