project (MCtoLevel0)
set (TARGET_NAME ConvertMCtoLvl0)

#------------------------#
#--- Set include path ---#
#------------------------#
include_directories (${PROJECT_SOURCE_DIR}/include)
include_directories (${CMAKE_BINARY_DIR}/include)
include_directories (${CMAKE_SOURCE_DIR}/include)
include_directories (${CMAKE_SOURCE_DIR}/${Dict_DIR}/include)

#------------------------#
#--- Set source files ---#
#------------------------#
set (SOURCES ${PROJECT_SOURCE_DIR}/src/main.cpp ${PROJECT_SOURCE_DIR}/src/${PROJECT_NAME}.cpp ${CMAKE_SOURCE_DIR}/src/utils.cpp)

#---------------------#
#--- Set libraries ---#
#---------------------#
set (PROJECT_LINK_LIBS ${Dict_LIB} ${ROOT_LIBRARIES})
link_directories (${LIBRARY_OUTPUT_PATH})

#----------------------#
#--- Set executable ---#
#----------------------#
add_executable(${TARGET_NAME} ${SOURCES})
target_link_libraries (${TARGET_NAME} ${PROJECT_LINK_LIBS})

#---------------#
#--- Install ---#
#---------------#
install (TARGETS ${TARGET_NAME} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
