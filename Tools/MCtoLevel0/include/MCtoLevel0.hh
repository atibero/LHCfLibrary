#ifndef MCtoLevel0_HH
#define MCtoLevel0_HH

#include <TFile.h>
#include <TRandom3.h>
#include <TString.h>
#include <TTree.h>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "McEvent.hh"
#include "ReadTableCal.hh"

using namespace std;

namespace nLHCf {

template <typename armclass, typename armcal>
class MCtoLevel0 : public armcal, public LHCfParams {
 public:
  MCtoLevel0();
  ~MCtoLevel0();

 private:
  /*--- Input/Output ---*/
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  Int_t fFileNumber;
  TString fMCName;
  TString fInputFormat;
  TString fInputFile;

  TString fOutputName;
  TFile *fOutputFile;
  TTree *fOutputTree;
  LHCfEvent *fOutputEv;

  /*--- Options ---*/
  Bool_t fSaveLevel2;
  Bool_t fDoSmearing;
  Bool_t fAveragePedestal;

  /*--- Level0/1/2 ---*/
  Level0<armclass> fLvl0;
  Level0<armclass> fPed0;
  Level1<armclass> fLvl1;
  Level2<armclass> fLvl2Tmp;
  Level2<armclass> fLvl2;

  /*--- MC truth ---*/
  McEvent fMcEvent;

  /*--- Current run ---*/
  Int_t fCurrentRun;

  /*--- Calibration data from tables ---*/
  ReadTableCal<armcal> fReadTableCal;

  /*--- Variables used for artificial sum ---*/
  TFile *list_file;
  TTree *list_tree;
  Int_t runNumber;
  Int_t eventNumber;
  Int_t nTotHits;
  Int_t nHitsSmall;
  Int_t nHitsLarge;
  vector<Int_t> *vtower;
  vector<Int_t> *vcode;
  vector<Int_t> *vcharge;
  vector<Float_t> *venergy;
  vector<Float_t> *vposx;
  vector<Float_t> *vposy;

 public:
  void Initialise();

  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetFileNumber(Int_t filenumber) { fFileNumber = filenumber; }
  void SetMCName(const Char_t *mcname) { fMCName = mcname; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SaveLevel2(Bool_t option) { fSaveLevel2 = option; }
  void DisableSmearing() { fDoSmearing = false; }
  void AveragePedestal() { fAveragePedestal = true; }
  void SetSeed(ULong_t seed) { fReadTableCal.SetSeed(seed); }
  void SetInputFile(const Char_t *name) { fInputFile = name; }
  void SetInputFormat(const Char_t *name) { fInputFormat = name; }

  void Convert();
  void ConvertSumArtificial(TString fMHListFile);
  void ConvertSumArtificialAutomatic();

 private:
  /* Input/output */
  void BuildOutputTree();

  void AddObject(TObject *obj);
  void FillEvent();
  void ClearEvent();

  void WriteToOutput();
  void CloseFiles();

  /* Set calibration parameters */
  void SetParameters();

  /* Read End2End event */
  Bool_t ReadEvent(ifstream &, McEvent *, Level2<armclass> *);
  // Int_t    CosmosToPDG(Int_t code, Int_t subcode, Int_t charge);
  Double_t GetCalSum(Level2<armclass> *);
  /* Smearing */
  void ConvertToLevel1(Level1<armclass> *, Level2<armclass> *);
  void ConvertToADC(Level2<armclass> *);
  void CrossTalkSmearing(Level2<armclass> *);
  void CopyData(Level1<armclass> *, Level2<armclass> *);

  void ConvertToLevel0(Level0<armclass> *lvl0, Level1<armclass> *lvl1, Level0<armclass> *ped);
  void SilCorrInvCorrection(Level0<armclass> *);
  void CopyData(Level0<armclass> *, Level1<armclass> *);
  void AddPedestal(Level0<armclass> *lvl0, Level0<armclass> *ped);
  void GeneratePedestal(Level0<armclass> *ped0);
  void GenerateConstantPedestal(Level0<armclass> *ped0);

  void OpenMHListFile(TString fMHListFile);
  void GetMHInterval(int fCurrentRun, int &int_inf, int &int_sup);
};
}  // namespace nLHCf
#endif
