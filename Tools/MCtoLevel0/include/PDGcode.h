const int kfpion = 211;
const int kfpi0 = 111;

const int kfeta = 221;

const int kfkaon = 321;
const int kfk0l = 130;
const int kfk0s = 310;
const int kfk0 = 311;

const int kfdmes = 411;
const int kfd0 = 421;

const int kfproton = 2212;
const int kfneutron = 2112;
const int kflambdac = 4122;
const int kfbomega = 3334;
const int kflambda = 3122;
const int kfsigma0 = 3212;
const int kfsigmap = 3222;
const int kfsigmam = 3112;
const int kfgzai0 = 3322;
const int kfgzai = 3312;

const int kfelec = 11;
const int kfmuon = 13;
const int kfneue = 12;
const int kfneumu = 14;
const int kfphoton = 22;

const int kfrho = 113;
const int kfmomega = 223;
const int kfphi = 333;
