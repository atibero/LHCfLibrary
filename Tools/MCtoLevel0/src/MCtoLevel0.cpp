#include <TMath.h>
#include <TROOT.h>
#include <TVectorD.h>

#include <CoordinateTransformation.hh>
#include <cstdlib>
#include <fstream>

#include "Utils.hh"
// #include "COSMOScode.h"
// #include "PDGcode.h"
#include "MCtoLevel0.hh"

using namespace nLHCf;

const double difference = 0.01;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass, typename armcal>
MCtoLevel0<armclass, armcal>::MCtoLevel0()
    : fPed0(Form("ped0_a%d", this->kArmIndex + 1), Form("Pedestal (Arm%d)", this->kArmIndex + 1)),
      fLvl0(Form("lvl0_a%d", this->kArmIndex + 1), Form("Level0 (Arm%d)", this->kArmIndex + 1)),
      fLvl1(Form("lvl1_a%d", this->kArmIndex + 1), Form("Level1 (Arm%d)", this->kArmIndex + 1)),
      fLvl2Tmp(Form("lvl2_a%dtmp", this->kArmIndex + 1), Form("Tmp%d", this->kArmIndex + 1)),
      fLvl2(Form("lvl2_a%d", this->kArmIndex + 1), Form("Level2 (Arm%d MC true)", this->kArmIndex + 1)),
      fMcEvent(Form("true_a%d", this->kArmIndex + 1), Form("MC truth (Arm%d)", this->kArmIndex + 1)),
      fSaveLevel2(false),
      fDoSmearing(true),
      fAveragePedestal(false),
      fFirstRun(0),
      fLastRun(0),
      fFileNumber(0),
      fMCName("") {
  fMcEvent.fParticleArray.SetOwner(kTRUE);

  fOutputFile = NULL;
  fOutputEv = NULL;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass, typename armcal>
MCtoLevel0<armclass, armcal>::~MCtoLevel0() {
  delete fOutputFile;
  delete fOutputEv;
}

/*-----------------------------*/
/*--- Allocate/clear memory ---*/
/*-----------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::Initialise() {
  BuildOutputTree();
  SetParameters();
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::BuildOutputTree() {
  Utils::Printf(Utils::kPrintInfo, "Building output list_tree...");
  fflush(stdout);

  fOutputFile = new TFile(fOutputName.Data(), "RECREATE");
  if (!fOutputFile->IsOpen()) {
    Utils::Printf(Utils::kPrintError, "Error: output file \"%s\" not opened\n", fOutputName.Data());
    exit(EXIT_FAILURE);
  }
  fOutputTree = new TTree("LHCfEvents", "LHCf events (level0)");
  fOutputEv = new LHCfEvent("event", "LHCfEvent");
  fOutputTree->Branch("ev.", "nLHCf::LHCfEvent", &fOutputEv);
  fOutputTree->SetMaxTreeSize(10000000000);
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::AddObject(TObject *obj) {
  fOutputEv->Add(obj);
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::FillEvent() {
  fOutputTree->Fill();
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::ClearEvent() {
  fOutputEv->HeaderClear();
  fOutputEv->ObjDelete();

  fLvl2.DataClear();
  fLvl1.DataClear();
  fLvl0.DataClear();
  fPed0.DataClear();

  /* Reset MC truth container */
  fMcEvent.fRun = 0;
  fMcEvent.fEvent = 0;
  fMcEvent.fReference.clear();
  fMcEvent.fParticleArray.Delete();
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::WriteToOutput() {
  Utils::Printf(Utils::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputTree->Write("", TObject::kOverwrite);
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::CloseFiles() {
  Utils::Printf(Utils::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

/*----------------------------------*/
/*--- Set calibration parameters ---*/
/*----------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::SetParameters() {
  fReadTableCal.ReadTables();
}

/*---------------------------------*/
/*--- Convert End2End to Level0 ---*/
/*---------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::Convert() {
  /*--- Event Loop ---*/
  Utils::Printf(Utils::kPrintInfo, "Start of event loop\n");

  if (fInputFormat == "") {
    if (this->fOperation == kSPS2022 || this->fOperation == kSPS2021 || this->fOperation == kSPS2015)
      fInputFormat = "end2end.%07d.out";
    else if (this->fOperation == kLHC2022) {
      TString arm = this->kArmIndex == 0 ? "a1c" : "a2c";
      fInputFormat = TString::Format("e2e_%s_%s_%%06d_%%03d.out", arm.Data(), fMCName.Data());
    } else
      fInputFormat = "end2end.%06d.out";
  }

  /*--- File Loop ---*/
  for (Int_t ifile = fFirstRun; ifile <= fLastRun; ++ifile) {
    TString filename;
    if (fInputFile == "") {
      TString fileformat = fInputDir + "/" + fInputFormat;
      if (fInputFormat.CountChar('%') == 2) {
        filename = TString::Format(fileformat.Data(), fFileNumber, ifile);
      } else
        filename = TString::Format(fileformat.Data(), ifile);
    } else {
      filename = fInputFile;
    }
    // Utils::Printf(Utils::kPrintError, "filename = %s", filename.Data());

    ifstream infile(filename.Data(), ifstream::in);
    printf("%s\n", filename.Data());
    if (!infile) {
      Utils::Printf(Utils::kPrintError, "!!!!!!can't open file!!!!!!!\n");
    }
    fCurrentRun = ifile;
    this->SetSeed(fCurrentRun);

    /*--- Event Loop ---*/
    Int_t ie = 0;
    while (ReadEvent(infile, &fMcEvent, &fLvl2)) {
      // Utils::Printf(Utils::kPrintInfo,"%lf\n",fLvl2.fPosDet[0][0][0][0][0]);
      // Utils::Printf(Utils::kPrintInfo,"%lf\n",fLvl2.fCalorimeter[0][0]);
      // Utils::Printf(Utils::kPrintInfo,"%lf\n",fLvl2.fOpenADC[0]);
      if (ie % 100 == 0) {
        Utils::Printf(Utils::kPrintInfo, "\r\trun %d, event %5d", fCurrentRun, ie);
        fflush(stdout);
      }

      Double_t true_sumde = GetCalSum(&fLvl2);
      // Utils::Printf(Utils::kPrintInfo,"%lf\n",true_sumde);
      // if (true_sumde > 0.) {

      fLvl2Tmp.DataCopy(&fLvl2);
      ConvertToLevel1(&fLvl1, &fLvl2Tmp);
      ConvertToLevel0(&fLvl0, &fLvl1, &fPed0);

      AddObject(&fLvl0);

      if (fDoSmearing)
        GeneratePedestal(&fPed0);  // with pedestal fluctuations
      else
        GenerateConstantPedestal(&fPed0);  // only pedestal mean
      AddObject(&fPed0);

      if (fSaveLevel2) AddObject(&fLvl2);
      //}
      AddObject(&fMcEvent);

      FillEvent();
      ClearEvent();

      ++ie;
    }  // end of event loop
    Utils::Printf(Utils::kPrintInfo, "\r\trun %d, event %5d", fCurrentRun, ie);
    fflush(stdout);

    infile.close();
  }  // end of file loop
  Utils::Printf(Utils::kPrintInfo, "\nEnd of event loop\n");

  /* Write to output file */
  WriteToOutput();
  CloseFiles();
}

/*-----------------------------------------------------*/
/*--- Convert End2End to Level0 with artificial sum ---*/
/*--------------------f---------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::ConvertSumArtificial(TString fMHListFile) {
  OpenMHListFile(fMHListFile);

  if (fInputFormat == "") {
    if (this->fOperation == kSPS2022 || this->fOperation == kSPS2021 || this->fOperation == kSPS2015)
      fInputFormat = "end2end.%07d.out";
    else if (this->fOperation == kLHC2022) {
      TString arm = this->kArmIndex == 0 ? "a1c" : "a2c";
      fInputFormat = TString::Format("e2e_%s_%s_%%06d_%%03d.out", arm.Data(), fMCName.Data());
    } else
      fInputFormat = "end2end.%06d.out";
  }

  /*--- Event Loop ---*/
  Utils::Printf(Utils::kPrintInfo, "Start of event loop\n");
  for (Int_t ifile = fFirstRun; ifile <= fLastRun; ++ifile) {
    TString filename;

    if (fInputFile == "") {
      TString fileformat = fInputDir + "/" + fInputFormat;
      if (fInputFormat.CountChar('%') == 2) {
        filename = TString::Format(fileformat.Data(), fFileNumber, ifile);
      } else
        filename = TString::Format(fileformat.Data(), ifile);
    } else {
      filename = fInputFile;
    }
    // Utils::Printf(Utils::kPrintError, "filename = %s", filename.Data());

    ifstream infile(filename.Data(), ifstream::in);
    printf("%s\n", filename.Data());
    if (!infile) {
      Utils::Printf(Utils::kPrintError, "!!!!!!can't open file!!!!!!!\n");
    }

    fCurrentRun = ifile;
    this->SetSeed(fCurrentRun);
    int int_inf, int_sup;
    GetMHInterval(fCurrentRun, int_inf, int_sup);

    for (int ievent = int_inf; ievent < int_sup; ++ievent) {
      list_tree->GetEntry(ievent);

      for (int itower = 0; itower < this->kCalNtower; ++itower) {
        const int nhits = itower == 0 ? nHitsSmall : nHitsLarge;
        if (nhits == 0) continue;

        fMcEvent.DataClear();

        vector<McEvent> mcAuxiliary(nhits);
        Level2<armclass> lvl2Auxiliary;

        int nevent = 0;
        for (int ihit = 0; ihit < nhits; ++ihit) {
          mcAuxiliary[ihit].DataClear();
          lvl2Auxiliary.Clear();

          // Read the event
          const int id = itower == 0 ? ihit : ihit + nHitsSmall;
          fflush(stdout);
          if (!ReadEvent(infile, &mcAuxiliary[ihit], &lvl2Auxiliary)) {
            Utils::Printf(Utils::kPrintError, "\nMC file ended before Log file\n");
            exit(EXIT_FAILURE);
          }
          if (nevent % 100 == 0) {
            Utils::Printf(Utils::kPrintInfo, "\r\trun %d, event %5d", fCurrentRun, nevent);
            fflush(stdout);
          }

          // Make check for alignment
          Int_t kcode, ksubcode, kcharge;
          UtilsMc::PdgToEpicsCode(mcAuxiliary[ihit].Get(0)->fPdgCode, kcode, ksubcode, kcharge);
          if (itower != vtower->at(id)) {
            Utils::Printf(Utils::kPrintError, "\nTowers not matched in Log file\n");
            exit(EXIT_FAILURE);
          }
          if (mcAuxiliary[ihit].GetN() != 1) {
            Utils::Printf(Utils::kPrintError, "\nFound %d particles in MC file\n", mcAuxiliary[ihit].GetN());
            exit(EXIT_FAILURE);
          }
          if (TMath::Abs(venergy->at(id) - mcAuxiliary[ihit].Get(0)->Energy()) > 1000. * difference) {
            Utils::Printf(Utils::kPrintError, "\nEnergy %f in Log file and %f in MC file\n", venergy->at(id),
                          mcAuxiliary[ihit].Get(0)->Energy());
            exit(EXIT_FAILURE);
          }
          if (TMath::Abs(vposx->at(id) - mcAuxiliary[ihit].Get(0)->X()) > difference) {
            Utils::Printf(Utils::kPrintError, "\nX %f in Log file and %f in MC file\n", vposx->at(id),
                          mcAuxiliary[ihit].Get(0)->X());
            exit(EXIT_FAILURE);
          }
          if (TMath::Abs(vposy->at(id) - mcAuxiliary[ihit].Get(0)->Y()) > difference) {
            Utils::Printf(Utils::kPrintError, "\nY %f in Log file and %f in MC file\n", vposy->at(id),
                          mcAuxiliary[ihit].Get(0)->Y());
            exit(EXIT_FAILURE);
          }
          if (vcode->at(id) != kcode) {
            Utils::Printf(Utils::kPrintError, "\nCode %d in Log file and %d in MC file\n", vcode->at(id), kcode);
            exit(EXIT_FAILURE);
          }
          if (vcharge->at(id) != kcharge) {
            Utils::Printf(Utils::kPrintError, "\nCharge %d in Log file and %d in MC file\n", vcharge->at(id), kcharge);
            exit(EXIT_FAILURE);
          }

          // Pile up events
          fMcEvent.fRun = runNumber;
          fMcEvent.fEvent = eventNumber;
          fMcEvent.Add(mcAuxiliary[ihit]);
          fLvl2.Add(&lvl2Auxiliary);
          ++nevent;
        }

        // fMcEvent.Print();
        // fLvl2.Print();

        Double_t true_sumde = GetCalSum(&fLvl2);
        if (true_sumde > 0.) {
          ConvertToLevel1(&fLvl1, &fLvl2);
          ConvertToLevel0(&fLvl0, &fLvl1, &fPed0);

          AddObject(&fLvl0);

          if (fDoSmearing)
            GeneratePedestal(&fPed0);  // with pedestal fluctuations
          else
            GenerateConstantPedestal(&fPed0);  // only pedestal mean
          AddObject(&fPed0);
        }
        AddObject(&fMcEvent);

        FillEvent();
        ClearEvent();
      }
    }

    infile.close();
  }
  Utils::Printf(Utils::kPrintInfo, "\nEnd of event loop\n");

  /* Write to output file */
  WriteToOutput();
  CloseFiles();
}

/*-----------------------------------------------------*/
/*--- Convert End2End to Level0 with artificial sum ---*/
/*--------------------f---------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::ConvertSumArtificialAutomatic() {
  Utils::Printf(Utils::kPrintInfo, "\nAutomatic Multi-hit event sum mode.\n");
  if (fInputFormat == "") {
    if (this->fOperation == kSPS2022 || this->fOperation == kSPS2021 || this->fOperation == kSPS2015)
      fInputFormat = "end2end.%07d.out";
    else if (this->fOperation == kLHC2022) {
      TString arm = this->kArmIndex == 0 ? "a1c" : "a2c";
      fInputFormat = TString::Format("e2e_%s_%s_%%06d_%%03d.out", arm.Data(), fMCName.Data());
    } else
      fInputFormat = "end2end.%06d.out";
  }

  /*--- Event Loop ---*/
  Utils::Printf(Utils::kPrintInfo, "Start of event loop\n");
  for (Int_t ifile = fFirstRun; ifile <= fLastRun; ++ifile) {
    TString filename;
    if (fInputFile == "") {
      TString fileformat = fInputDir + "/" + fInputFormat;
      if (fInputFormat.CountChar('%') == 2) {
        filename = TString::Format(fileformat.Data(), fFileNumber, ifile);
      } else
        filename = TString::Format(fileformat.Data(), ifile);
    } else {
      filename = fInputFile;
    }
    // Utils::Printf(Utils::kPrintError, "filename = %s", filename.Data());

    ifstream infile(filename.Data(), ifstream::in);
    printf("%s\n", filename.Data());
    if (!infile) {
      Utils::Printf(Utils::kPrintError, "!!!!!!can't open file!!!!!!!\n");
    }

    if (this->fOperation == kLHC2022)
      fCurrentRun = fFileNumber * 1000 + ifile;
    else
      fCurrentRun = ifile;
    this->SetSeed(fCurrentRun);

    McEvent mcAuxiliary;
    Level2<armclass> lvl2Auxiliary;

    /*--- Event Loop ---*/
    Bool_t endfile = true;
    Int_t ie = 0;
    Int_t ie2 = 0;
    Int_t npar = 0;
    while (endfile) {
      while (1) {
        endfile = ReadEvent(infile, &mcAuxiliary, &lvl2Auxiliary);

        if (!endfile) break;
        ++ie2;
        if (ie2 % 1 == 0) {
          Utils::Printf(Utils::kPrintInfo, "\r\trun %d, event %5d, sub-event %5d", fCurrentRun, ie, ie2);
          fflush(stdout);
        }
        if (mcAuxiliary.GetN() == 0) continue;
        // first event in the file
        if (npar == 0 || mcAuxiliary.Get(0)->GetEventID() == fMcEvent.Get(0)->GetEventID()) {
          if (this->fOperation == kLHC2022)
            fMcEvent.fRun = fCurrentRun;
          else
            fMcEvent.fRun = 0;  // to-do
          fMcEvent.fEvent = mcAuxiliary.Get(0)->GetEventID();
          fMcEvent.Add(mcAuxiliary);
          fLvl2.Add(&lvl2Auxiliary);
          mcAuxiliary.DataClear();
          lvl2Auxiliary.Clear();
          ++npar;
        } else {
          npar = 1;
          break;
        }
      }

      if (npar == 0) continue;

      Double_t true_sumde = GetCalSum(&fLvl2);
      // Utils::Printf(Utils::kPrintInfo,"%lf\n",true_sumde);
      // if (true_sumde > 0.) {

      fLvl2Tmp.DataCopy(&fLvl2);
      ConvertToLevel1(&fLvl1, &fLvl2Tmp);
      ConvertToLevel0(&fLvl0, &fLvl1, &fPed0);

      AddObject(&fLvl0);

      if (fDoSmearing)
        GeneratePedestal(&fPed0);  // with pedestal fluctuations
      else
        GenerateConstantPedestal(&fPed0);  // only pedestal mean
      AddObject(&fPed0);

      if (fSaveLevel2) AddObject(&fLvl2);
      //}
      AddObject(&fMcEvent);

      FillEvent();
      ClearEvent();

      if (npar == 1 && endfile) {
        // Copy the buffered event
        if (this->fOperation == kLHC2022)
          fMcEvent.fRun = fCurrentRun;
        else
          fMcEvent.fRun = 0;  // to-do
        if (mcAuxiliary.GetN() > 0) fMcEvent.fEvent = mcAuxiliary.Get(0)->GetEventID();
        fMcEvent.Add(mcAuxiliary);
        fLvl2.Add(&lvl2Auxiliary);
        mcAuxiliary.DataClear();
        lvl2Auxiliary.Clear();
      }

      ++ie;
    }  // end of event loop

    Utils::Printf(Utils::kPrintInfo, "\r\trun %d, event %5d (org. event %5d)", fCurrentRun, ie, ie2);
    fflush(stdout);

    infile.close();
  }  // end of file loop
  Utils::Printf(Utils::kPrintInfo, "\nEnd of event loop\n");

  /* Write to output file */
  WriteToOutput();
  CloseFiles();
}

/*-----------------------------------------*/
/*--- Open List File for artificial sum ---*/
/*-----------------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::OpenMHListFile(TString fMHListFile) {
  list_file = new TFile(fMHListFile.Data(), "READ");
  list_tree = (TTree *)list_file->Get("Log");

  list_tree->SetBranchAddress("runNumber", &runNumber);
  list_tree->SetBranchAddress("eventNumber", &eventNumber);
  list_tree->SetBranchAddress("nTotHits", &nTotHits);
  list_tree->SetBranchAddress("nHitsSmall", &nHitsSmall);
  list_tree->SetBranchAddress("nHitsLarge", &nHitsLarge);

  vtower = new vector<Int_t>();
  vcode = new vector<Int_t>();
  vcharge = new vector<Int_t>();
  venergy = new vector<Float_t>();
  vposx = new vector<Float_t>();
  vposy = new vector<Float_t>();

  TBranch *btower = list_tree->GetBranch("tower");
  TBranch *bcode = list_tree->GetBranch("code");
  TBranch *bcharge = list_tree->GetBranch("charge");
  TBranch *benergy = list_tree->GetBranch("energy");
  TBranch *bposx = list_tree->GetBranch("posx");
  TBranch *bposy = list_tree->GetBranch("posy");

  list_tree->SetBranchAddress("tower", &vtower, &btower);
  list_tree->SetBranchAddress("code", &vcode, &bcode);
  list_tree->SetBranchAddress("charge", &vcharge, &bcharge);
  list_tree->SetBranchAddress("energy", &venergy, &benergy);
  list_tree->SetBranchAddress("posx", &vposx, &bposx);
  list_tree->SetBranchAddress("posy", &vposy, &bposy);
}

/*---------------------------------------------*/
/*--- Get entry interval for artificial sum ---*/
/*---------------------------------------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::GetMHInterval(int fCurrentRun, int &int_inf, int &int_sup) {
  int_inf = -1;
  int_sup = -1;
  bool evfound = false;
  const int nentries = list_tree->GetEntries();
  for (int ievent = 0; ievent < nentries; ++ievent) {
    list_tree->GetEntry(ievent);
    if (evfound == false) {
      if (runNumber == fCurrentRun) {
        int_inf = ievent;
        evfound = true;
      }
    }
    if (evfound == true) {
      if (runNumber != fCurrentRun) {
        int_sup = ievent;
        break;
      } else if (ievent == nentries - 1) {
        int_sup = nentries;
        break;
      }
    }
  }
  if (int_inf < 0 || int_sup < 0) {
    Utils::Printf(Utils::kPrintError, "\nNo run number %d found in Log file\n", fCurrentRun);
    exit(EXIT_FAILURE);
  }
}

/*---------------------------*/
/*--- Read End2End output ---*/
/*---------------------------*/
template <typename armclass, typename armcal>
Bool_t MCtoLevel0<armclass, armcal>::ReadEvent(ifstream &infile, McEvent *mcev, Level2<armclass> *lvl2) {
  /* Allocate buffers */
  const Int_t buf_size = 1024;
  Char_t buf[buf_size];
  Char_t header[buf_size];
  /* ZDC sum for calculation of data with a bug*/
  Double_t zdcsum[3] = {0., 0., 0.};

  /* Find the beginning of the event */
  while (1) {
    infile.getline(buf, buf_size);
    if (!infile.good()) return false;
    if (buf[0] == '#') continue;

    sscanf(buf, "%s", header);
    if (strcmp(header, "start") == 0) {
      Int_t event = 0;
      sscanf(buf, "%*s %d", &event);

      // Run & event number
      lvl2->fRun = fCurrentRun;
      lvl2->fEvent = event - 1;
      mcev->fRun = fCurrentRun;
      mcev->fEvent = event - 1;

      // Utils::Printf(Utils::kPrintInfo, "Event %d\n", event);
      break;
    }
  }

  /* Read event until the end if found */
  Int_t count = 0;
  while (1) {
    infile.getline(buf, buf_size);
    if (!infile.good()) break;
    if (buf[0] == '#') continue;

    sscanf(buf, "%s", header);
    // MC truth
    if (strcmp(header, "TRG") == 0) {
      Double_t user = -1;  // change from Int_t to Double_t (16 May 2024)
      Int_t code = -1;
      Int_t subcode = -1;
      Int_t charge = -1;
      Double_t posx = 0.;
      Double_t posy = 0.;
      Double_t posz = 0.;
      Double_t energy = 0.;  // kinetic energy!
      Double_t px = 0.;
      Double_t py = 0.;
      Double_t pz = 0.;
      sscanf(buf, "%*s %lf %d %d %d %lf %lf %lf %lf %lf %lf %lf", &user, &code, &subcode, &charge, &posx, &posy, &posz,
             &energy, &px, &py, &pz);
      McParticle *part = new McParticle(Form("part_%d_%d_%d", mcev->fRun, mcev->fEvent, count),
                                        Form("part_%d_%d_%d", mcev->fRun, mcev->fEvent, count));
      part->fPdgCode = UtilsMc::EpicsToPdgCode(code, subcode, charge);
      if (this->fOperation == kSPS2022 || this->fOperation == kSPS2021 || this->fOperation == kSPS2015)
        part->fPosition.SetXYZ(posx * 10., posy * 10., 0.);
      else
        part->fPosition.SetXYZ(posx * 10., posy * 10., CoordinateTransformation::kEnd2EndInZpos);
      part->fMomentum.SetPxPyPzE(px, py, pz, energy);

      if (user - (int)user > 0.001) {
        // new End2End format
        if (user > 1.E7) {  // For the number of "user" with > 10^7
          user = user - ((int)(user / 1E7)) * 1.E7;
        }
        part->fUserCode = (Int_t)((user + 0.00001) * 100);
      } else {
        // old End2End format
        part->fUserCode = (Int_t)(user + 0.00001);
      }

      mcev->fParticleArray.Add(part);
      // mcev->fParticleArray.Print();
      // printf("%d\n", count);
      // getchar();
      ++count;
    }
    // Front counters
    else if (strcmp(header, "scin") == 0) {
      Int_t channel = 0;
      Double_t signal = 0.;
      sscanf(buf, "%*s %*s %*s %d %lf", &channel, &signal);
      --channel;
      if (channel >= 0 && channel < this->kFcNlayer) lvl2->fFrontCounter[channel] = signal;
    }
    // GSO layers
    else if (strcmp(header, "gsoplate") == 0) {
      Char_t tower_name[buf_size];
      Int_t layer = 0;
      Double_t signal = 0.;
      Double_t signal_nolce = 0.;
      // sscanf(buf, "%*s %s %*s %d %lf %lf", tower_name, &layer, &signal, &signal_nolce);
      sscanf(buf, "%*s %s %*s %d %lf %lf", tower_name, &layer, &signal, &signal_nolce);
      --layer;
      Int_t tower = -1;
      if (this->kArmIndex == Arm1Params::kArmIndex) {
        if (strcmp(tower_name, "t2") == 0) tower = 0;
        if (strcmp(tower_name, "t4") == 0) tower = 1;
      }
      if (this->kArmIndex == Arm2Params::kArmIndex) {
        if (strcmp(tower_name, "t25") == 0) tower = 0;
        if (strcmp(tower_name, "t32") == 0) tower = 1;
      }
      if (tower >= 0 && tower < this->kCalNtower && layer >= 0 && layer < this->kCalNlayer)
        lvl2->fCalorimeter[tower][layer] = signal;
    }
    // GSO bars
    else if ((strcmp(header, "gsobarx") == 0) || (strcmp(header, "gsobary") == 0)) {
      Char_t tower_name[buf_size];
      Int_t layer = 0;
      Int_t channel = 0;
      Double_t signal1 = 0.;  // true dE
      Double_t signal2 = 0.;  // only Att.
      Double_t signal3 = 0.;  // true dE
      // sscanf(buf, "%s %s %*s %d %d %*s %lf", header, tower_name, &layer, &channel, &signal);
      sscanf(buf, "%s %s %*s %d %d %lf %lf %lf", header, tower_name, &layer, &channel, &signal1, &signal2, &signal3);
      Int_t it = -1;
      if (strcmp(tower_name, "t2") == 0) it = 0;
      if (strcmp(tower_name, "t4") == 0) it = 1;
      // Int_t il = layer - 1;
      Int_t il = -1;
      Int_t iv = -1;
      if (strcmp(header, "gsobarx") == 0) {
        iv = 0;
        il = ((layer + 1) / 2) - 1;
      }
      if (strcmp(header, "gsobary") == 0) {
        iv = 1;
        il = layer / 2;
        il = il - 1;
      }
      Int_t ic = channel - 1;
      Int_t is = 0;
      if (it >= 0 && it < this->kPosNtower && il >= 0 && il < this->kPosNlayer && iv >= 0 && iv < this->kPosNview &&
          ic >= 0 && ic < this->kPosNchannel[it])
        lvl2->fPosDet[it][il][iv][ic][is] = signal2;
    }
    // Silicon
    else if (strcmp(header, "si") == 0) {
      Int_t layer = 0;
      Int_t channel = 0;
      Double_t signal = 0.;
      sscanf(buf, "%*s %d %d %lf", &layer, &channel, &signal);
      Int_t it = 0;
      Int_t il = (layer - 1) / 2;
      Int_t iv = layer % 2;  // odd -> Y, even -> X
      Int_t ic = channel - 1;
      if (il >= 0 && il < this->kPosNlayer && iv >= 0 && iv < this->kPosNview && ic >= 0 && ic < this->kPosNchannel[it])
        for (Int_t is = 0; is < this->kPosNsample; ++is) lvl2->fPosDet[it][il][iv][ic][is] = signal;
    }
    // ZDC layer (work only for SPS2021 and Op2022)
    /*
    // It does not work for the simulation results with a bug (data before Dec/2022)
    else if (strcmp(header, "zdcsum") == 0) {
      Int_t module = 0;
      Double_t signal = 0.;
      sscanf(buf,"%s %d %lf", header, &module, &signal);
      Int_t im = module-1;
      if (im >= 0 && im < 3 ){
        lvl2->fOpenADC[im] = signal;
             lvl2->fZdc[im] = signal;
      }
    }
    */
    else if (strcmp(header, "zdcf") == 0) {
      Int_t layer = 0, fiber = 0;
      Double_t signal = 0.;
      sscanf(buf, "%s %d %d %lf", header, &layer, &fiber, &signal);
      Int_t im = (int)((layer - 1) / 12);
      if (layer != 1 && fiber >= 5) {
        zdcsum[im] += signal;
      }
    }

    // End of event
    else if (strcmp(header, "end") == 0) {
      Int_t event = 0;
      sscanf(buf, "%*s %d", &event);
      if (event != mcev->fEvent + 1) {
        Utils::Printf(Utils::kPrintInfo, "Warning: event mismatch (run=%d: start=%d, end=%d), skipping event...\n",
                      fCurrentRun, mcev->fEvent + 1, event);
        break;
      }
      for (int im = 0; im < 3; im++) {
        lvl2->fOpenADC[im] = zdcsum[im];
        lvl2->fZdc[im] = zdcsum[im];
      }
      return true;
    }
  }

  ClearEvent();
  return false;
}

// template<typename armclass, typename armcal>
// Int_t MCtoLevel0<armclass, armcal>::CosmosToPDG(Int_t code, Int_t subcode, Int_t charge)
// {
//   if (code == kphoton && subcode == 0 && charge == 0)
//     return kfphoton; // gamma
//   if (code == kphoton && subcode == 2 && charge == 0)
//     return kfphoton; // direct gamma (?)

//   if (code == kelec) {
//     if (subcode == -1 && charge == -1)
//       return kfelec; // electron
//     if (subcode == 1 && charge == 1)
//       return -kfelec; // anti-electron
//   }

//   if (code == kmuon) {
//     if (subcode == -1 && charge == -1)
//       return kfmuon; // mu-
//     if (subcode == 1 && charge == 1)
//       return -kfmuon; // mu+
//   }

//   if (code == kneue) {
//     if (subcode == -1)
//       return kfneue; // electron neutrino
//     if (subcode == 1)
//       return -kfneue; // electron anti-neutrino
//   }

//   if (code == kneumu) {
//     if (subcode == -1)
//       return kfneumu; // muon neutrino
//     if (subcode == 1)
//       return -kfneumu; // muon anti-neutrino
//   }

//   if (code == knuc) {
//     if (subcode == kneutron && charge == 0)
//       return kfneutron; // neutron
//     if (subcode == kneutronb && charge == 0)
//       return -kfneutron; // anti-neutron
//     if (subcode == regptcl && charge == 1)
//       return kfproton; // proton
//     if (subcode == antip && charge == -1)
//       return -kfproton; // anti-proton
//   }

//   if (code == kpion) {
//     if (subcode == 0 && charge == 0)
//       return kfpi0; // neutral pion
//     if (subcode == -1 && charge == 1)
//       return kfpion; // pi+
//     if (subcode == 1 && charge == -1)
//       return -kfpion; // pi-
//   }

//   if (code == keta && subcode == 0 && charge == 0)
//     return kfeta; // eta

//   if (code == kkaon) {
//     if (subcode == -1 && charge == 1)
//       return kfkaon; // K+
//     if (subcode == 1 && charge == -1)
//       return -kfkaon; // K-
//     if (subcode == k0l && charge == 0)
//       return kfk0l; // K0l
//     if (subcode == -k0l && charge == 0)
//       return -kfk0l; // anti-K0l
//     if (subcode == k0s && charge == 0)
//       return kfk0s; // K0s
//     if (subcode == -k0s && charge == 0)
//       return -kfk0s; // anti-K0s
//   }

//   if (code == klambda) {
//     if (subcode == -1 && charge == 0)
//       return kflambda; // lambda
//     if (subcode == 1 && charge == 0)
//       return -kflambda; // anti-lambda
//   }

//   if (code == kdmes) {
//     if (subcode == -1 && charge == 0)
//       return kfd0; // D0
//     if (subcode == 1 && charge == 0)
//       return -kfd0; // anti-D0
//     if (subcode == -1 && charge == 1)
//       return kfdmes; // D+
//     if (subcode == 1 && charge == -1)
//       return -kfdmes; // D-
//   }

//   if (code == ksigma) {
//     if (subcode == -1 && charge == 0)
//       return kfsigma0; // sigma0
//     if (subcode == 1 && charge == 0)
//       return -kfsigma0; // anti-sigma0
//     if (subcode == -1 && charge == 1)
//       return kfsigmap; // sigma+
//     if (subcode == 1 && charge == -1)
//       return -kfsigmap; // anti-sigma+
//     if (subcode == -1 && charge == -1)
//       return kfsigmam; // sigma-
//     if (subcode == 1 && charge == 1)
//       return -kfsigmam; // anti-sigma-
//   }

//   if (code == kgzai) {
//     if (subcode == -1 && charge == 0)
//       return kfgzai0; // xi0
//     if (subcode == 1 && charge == 0)
//       return -kfgzai0; // anti-xi0
//     if (subcode == -1 && charge == -1)
//       return kfgzai; // xi-
//     if (subcode == 1 && charge == 1)
//       return -kfgzai; // anti-xi-
//   }

//   if (code == klambdac) {
//     if (subcode == -1 && charge == 1)
//       return kflambdac; // lambda_C
//     if (subcode == 1 && charge == -1)
//       return -kflambdac; // anti-lambda_C
//   }

//   if (code == kbomega) {
//     if (subcode == -1 && charge == -1)
//       return kfbomega; // omega (baryon)
//     if (subcode == 1 && charge == 1)
//       return -kfbomega; // anti-omega (baryon)
//   }

//   if (code == krho && subcode == 0 && charge == 0)
//     return kfrho; // rho

//   if (code == komega && subcode == 0 && charge == 0)
//     return kfmomega; // omega (meson)

//   if (code == kphi && subcode == 0 && charge == 0)
//     return kfphi; // phi

//   Utils::Printf(Utils::kPrintError, "Unknown Cosmos code (%d %d %d)\n", code, subcode, charge);
//   //exit(EXIT_FAILURE);
//   return 0;
// }

template <typename armclass, typename armcal>
Double_t MCtoLevel0<armclass, armcal>::GetCalSum(Level2<armclass> *lvl2) {
  Double_t sum = 0.;
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kCalNlayer; il++) {
      sum += lvl2->fCalorimeter[it][il];
    }
  }
  return sum;
}

/*----------------*/
/*--- Smearing ---*/
/*----------------*/
template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::ConvertToLevel1(Level1<armclass> *lvl1, Level2<armclass> *lvl2) {
  ConvertToADC(lvl2);    // convert GeV to ADC
  CopyData(lvl1, lvl2);  // copy to Level1
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::ConvertToADC(Level2<armclass> *lvl2) {
  /* Calorimeter */
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) lvl2->fCalorimeter[it][il] /= fReadTableCal.fCalConvFactor[it][il];

  /* Position Detector */
  // Bug (The CrossTalkSmearing was applied after the GainCalibration.) was corrected on 25 Oct. 2024.
  if (this->kArmIndex == 0)  // Arm1
     CrossTalkSmearing(lvl2);
  
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is)
            lvl2->fPosDet[it][il][iv][ic][is] /= fReadTableCal.fPosConvFactor[it][il][iv][ic][is];

  /* FrontCounter */  // !!! TODO !!!
                      // for (Int_t il = 0; il < this->kFcNlayer; ++il)
                      //   lvl2->fFrontCounter[il] *= fFcConvFactor[il];
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::CrossTalkSmearing(Level2<armclass> *lvl2) {
  if (this->kArmIndex != 0) return;

  Int_t nmap = this->kPosNlayer * this->kPosNview;  // it must be 8
  for (Int_t il = 0; il < this->kPosNlayer; ++il) {
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      Int_t imap = il * 2 + iv;

      // Fill to values into the TVectorD
      Int_t i = 0;
      Double_t tmp[60];
      for (Int_t it = 0; it < this->kPosNtower; ++it) {
        for (Int_t ip = 0; ip < this->kPosNchannel[it]; ++ip) {
          tmp[i++] = lvl2->fPosDet[it][il][iv][ip][0];
        }
      }
      TVectorD V(60, tmp);

      // Apply the correction
      V = fReadTableCal.fPosCrossTalkInv[imap] * V;

      // Fill back to the Level2
      i = 0;
      for (Int_t it = 0; it < this->kPosNtower; ++it) {
        for (Int_t ip = 0; ip < this->kPosNchannel[it]; ++ip) {
          lvl2->fPosDet[it][il][iv][ip][0] = V(i++);
        }
      }
    }
  }

  return;
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::SilCorrInvCorrection(Level0<armclass> *lvl0) {
  if (this->kArmIndex != 1) return;

  Double_t tmp;
  for (Int_t it = 0; it < this->kPosNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t ip = 0; ip < this->kPosNchannel[it]; ++ip) {
          if (lvl0->fPosDet[it][il][iv][ip][1] > 0) {  // apply on positive energy deposit (Sample1 only)
            // Fill to values into the Double_t tmp variable
            tmp = lvl0->fPosDet[it][il][iv][ip][1];
            // Apply the inverse correction
            tmp = fReadTableCal.fPosSilCorrInv.Eval(tmp);
            // Fill back to the Level2
            lvl0->fPosDet[it][il][iv][ip][1] = tmp;
          }
        }
      }
    }
  }
  return;
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::CopyData(Level1<armclass> *lvl1, Level2<armclass> *lvl2) {
  lvl1->fRun = lvl2->fRun;
  lvl1->fEvent = lvl2->fEvent;

  /* Calorimeter */
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      lvl1->fCalorimeter[it][il][0] = lvl2->fCalorimeter[it][il];
      lvl1->fCalorimeter[it][il][1] = lvl2->fCalorimeter[it][il] / fReadTableCal.fCalRangeFactor[it][il];
    }

  /* Position Detector */
  Utils::CopyVector5D(lvl1->fPosDet, lvl2->fPosDet);
  /* FrontCounter */
  for (Int_t il = 0; il < this->kFcNlayer; ++il) {
    lvl1->fFrontCounter[il][0] = lvl2->fFrontCounter[il];
    lvl1->fFrontCounter[il][1] = 0.;  // TODO
  }
  /* Laser */
  Utils::CopyVector1D(lvl1->fLaser, lvl2->fLaser);
  /* ZDC */
  Utils::CopyVector1D(lvl1->fOpenADC, lvl2->fOpenADC);
  for (int ic = 0; ic < this->kZdcNchannel; ++ic) {
    lvl1->fZdc[ic][0] = lvl2->fZdc[ic];
    lvl1->fZdc[ic][1] = lvl2->fZdc[ic] / 8.;
  }
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::ConvertToLevel0(Level0<armclass> *lvl0, Level1<armclass> *lvl1,
                                                   Level0<armclass> *ped0) {
  CopyData(lvl0, lvl1);
  if (this->kArmIndex == Arm2Params::kArmIndex && this->fOperation == kLHC2022) SilCorrInvCorrection(lvl0);
  GenerateConstantPedestal(ped0);  // only pedestal mean
  AddPedestal(lvl0, ped0);
  // Utils::Printf(Utils::kPrintInfo,"%d\n",lvl0);
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::CopyData(Level0<armclass> *lvl0, Level1<armclass> *lvl1) {
  lvl0->fRun = lvl1->fRun;
  lvl0->fEvent = lvl1->fEvent;

  /* Calorimeter */
  Utils::CopyVector3D(lvl0->fCalorimeter, lvl1->fCalorimeter);
  /* Position Detector */
  Utils::CopyVector5D(lvl0->fPosDet, lvl1->fPosDet);
  /* FrontCounter */
  Utils::CopyVector2D(lvl0->fFrontCounter, lvl1->fFrontCounter);
  /* Laser */
  Utils::CopyVector1D(lvl0->fLaser, lvl1->fLaser);
  /* ZDC */
  Utils::CopyVector1D(lvl0->fOpenADC, lvl1->fOpenADC);
  Utils::CopyVector2D(lvl0->fZdc, lvl1->fZdc);
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::AddPedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  Utils::AddVector3D(lvl0->fCalorimeter, ped0->fCalorimeter);
  Utils::AddVector5D(lvl0->fPosDet, ped0->fPosDet);
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::GeneratePedestal(Level0<armclass> *ped0) {
  if (fReadTableCal.fCalPedEntries != fReadTableCal.fPosPedEntries) {
    Utils::Printf(Utils::kPrintError, "Error: pedestal tree entries mismatch (cal=%d, pos=%d)\n",
                  fReadTableCal.fCalPedEntries, fReadTableCal.fPosPedEntries);
    exit(EXIT_FAILURE);
  }
  Int_t entry = fReadTableCal.fRandom.Integer(fReadTableCal.fCalPedEntries);

  /* Calorimeter */
  fReadTableCal.fCalPedTree->GetEntry(entry);
  Int_t cvec = 0;
  for (Int_t ir = 0; ir < this->kCalNrange; ++ir)
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        if (fAveragePedestal) {  // ROOT File contains only pedestal signal
          ped0->fCalorimeter[it][il][ir] = fReadTableCal.fCalPedVec->at(cvec);
        } else {  // ROOT File contains pedestal-delayed gate signal
          ped0->fCalorimeter[it][il][ir] = -fReadTableCal.fCalPedVec->at(cvec) + fReadTableCal.fCalPedMean[it][il][ir];
        }
        ++cvec;
      }

  /* Position detector */
  fReadTableCal.fPosPedTree->GetEntry(entry);
  Int_t pvec = 0;
  if (this->kArmIndex == Arm1Params::kArmIndex) {  // Arm1
    Int_t is = 0;
    for (Int_t it = 0; it < this->kPosNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
            ped0->fPosDet[it][il][iv][ic][is] = fReadTableCal.fPosPedVec->at(pvec);
            ++pvec;
          }
  } else if (this->kArmIndex == Arm2Params::kArmIndex) {  // Arm2
    Int_t it = 0;
    for (Int_t is = 0; is < this->kPosNsample; ++is)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
            ped0->fPosDet[it][il][iv][ic][is] = fReadTableCal.fPosPedVec->at(pvec);
            ++pvec;
          }
  }
}

template <typename armclass, typename armcal>
void MCtoLevel0<armclass, armcal>::GenerateConstantPedestal(Level0<armclass> *ped0) {
  Utils::CopyVector3D(ped0->fCalorimeter, fReadTableCal.fCalPedMean);
  Utils::CopyVector5D(ped0->fPosDet, fReadTableCal.fPosPedMean);
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class MCtoLevel0<Arm1Params, Arm1CalPars>;

template class MCtoLevel0<Arm2Params, Arm2CalPars>;
}  // namespace nLHCf
