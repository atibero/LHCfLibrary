#include <TRint.h>
#include <TMath.h>

#include "MCtoLevel0.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-I <Input> filepath (give this parameter if you specify only one file)\n");
  printf("\t-i <input>\t\tInput directory\n");
  printf("\t-n <filename>\t\tFormat of input filename (default= end2end.%%06d.out)\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = 1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = 1)\n");
  printf("\t-N <filenumber>\t\tFile number to be analysed for OP2022 (default = 0)\n");
  printf("\t-k <mcname>\t\tName of the MC generator for Op2022 (must be specified for Op2022\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"converted.root\")\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (default = 3855)\n");
  printf(
      "\t-S <subfill>\t\tAutomatically set parameters for specified subfill "
      "(mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> (and <subfill>) are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t--arm1\t\t\tArm1 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--savelvl2\t\t Save Level2 (MC true) (default = disable)\n");
  printf("\t--disable-smearing\tDisable smearing (default = enabled)\n");
  printf(
      "\t--average-pedestal\tAdd pedestal average instead of delayed gate (default = disabled)\n");
  printf("\t-t <tables_dir>\t\tSet tables directory (default = \"../Tables\")\n");
  printf(
      "\t-m <multihit-list>\t\tList used to combine n-events in one event (default = disabled)\n");
  printf("\t-M \t\t\tAutomatic combine multi-hit events in one event (default = disabled)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  int first_run = 0;
  int last_run = 0;
  int file_number = 0;
  string mc_name = "standard";
  string output_file = "converted.root";
  int fill = -1;
  int subfill = -1;
  int iarm = -1;
  bool savelvl2 = false;
  bool smearing = true;
  bool average_pedestal = false;
  bool auto_mhcombine = false;
  string list_file = "";
  int verbose = 1;
  string input_format = "";
  string input_file = "";

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"filenumber", required_argument, NULL, 'N'},
      {"mcname", required_argument, NULL, 'k'},
      {"output", required_argument, NULL, 'o'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"savelvl2", no_argument, NULL, 1010},
      {"disable-smearing", no_argument, NULL, 1020},
      {"average-pedestal", no_argument, NULL, 1030},
      {"tables-dir", required_argument, NULL, 't'},
      {"multihit-list", required_argument, NULL, 'm'},
      {"auto-mhcombine", no_argument, NULL, 'M'},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {"input-format", required_argument, NULL, 'n'},
      {"input-file", required_argument, NULL, 'I'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'N':
        if (optarg) file_number = atoi(optarg);
        break;
      case 'k':
        if (optarg) mc_name = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'F':
        if (optarg) fill = TMath::Abs(atoi(optarg));
        break;
      case 'S':
        if (optarg) subfill = TMath::Abs(atoi(optarg));
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 1010:
        savelvl2 = true;
        break;
      case 1020:
        smearing = false;
        break;
      case 1030:
        average_pedestal = true;
        break;
      case 't':
        if (optarg) {
          Arm1Params::SetTableDirectory(optarg);
          Arm2Params::SetTableDirectory(optarg);
        }
        break;
      case 'm':
        if (optarg) list_file = optarg;
        break;
      case 'M':
        auto_mhcombine = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      case 'n':
        if (optarg) input_format = optarg;
        break;
      case 'I':
        if (optarg) input_file = optarg;
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "" && input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d-%d\n", first_run, last_run);

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/

  printf("\n");
  printf("===\n");
  printf("======= ConvertMCtoLvl0\n");
  printf("===============================\n");
  if (input_file != "") {
    printf("\tInput file: %s\n", input_file.c_str());
  } else {
    printf("\tInput directory: %s\n", input_dir.c_str());
    printf("\tFirst run: %d\n", first_run);
    printf("\tLast run: %d\n", last_run);
    printf("\tFile number: %d\n", file_number);
    printf("\tMC name: %s\n", mc_name.c_str());
    printf("\tInput format: %s\n", input_format.c_str());
  }
  printf("\tFill number: %d [%d]\n", fill, subfill);
  printf("\tOutput file: %s\n", output_file.c_str());
  printf("\tOptions:\n");
  if (savelvl2) {
    printf("\t\tSaving lvl2\n");
  }
  if (!smearing) {
    printf("\t\tSmearing disabled\n");
  }
  if (average_pedestal) {
    printf("\t\tUsing calorimeter pedestal average\n");
  }
  Utils::SetVerbose(verbose);
  Arm1CalPars::SetFill(fill, subfill);
  Arm2CalPars::SetFill(fill, subfill);
  MCtoLevel0<Arm1Params, Arm1CalPars> mc_1;
  MCtoLevel0<Arm2Params, Arm2CalPars> mc_2;
  if (iarm == Arm1Params::kArmIndex) {
    if (input_file != "")
      mc_1.SetInputFile(input_file.c_str());
    else {
      if (mc_1.fOperation == kLHC2022 && auto_mhcombine && mc_name == "") {
        Utils::Printf(Utils::kPrintError, "Please specify MC generator name for Arm1 simulation");
        return -1;
      }
      mc_1.SetInputDir(input_dir.c_str());
      mc_1.SetFirstRun(first_run);
      mc_1.SetLastRun(last_run);
      mc_1.SetFileNumber(file_number);
      mc_1.SetMCName(mc_name.c_str());
      mc_1.SetInputFormat(input_format.c_str());
    }
    mc_1.SetOutputName(output_file.c_str());
    mc_1.SaveLevel2(savelvl2);
    if (!smearing) mc_1.DisableSmearing();
    if (average_pedestal) mc_1.AveragePedestal();
    mc_1.Initialise();
    if (list_file == "" && !auto_mhcombine)
      mc_1.Convert();
    else if (auto_mhcombine)
      mc_1.ConvertSumArtificialAutomatic();
    else
      mc_1.ConvertSumArtificial(list_file);

  } else if (iarm == Arm2Params::kArmIndex) {
    if (input_file != "")
      mc_2.SetInputFile(input_file.c_str());
    else {
      if (mc_2.fOperation == kLHC2022 && auto_mhcombine && mc_name == "") {
        Utils::Printf(Utils::kPrintError, "Please specify MC generator name for Arm2 simulation");
        return -1;
      }
      mc_2.SetInputDir(input_dir.c_str());
      mc_2.SetFirstRun(first_run);
      mc_2.SetLastRun(last_run);
      mc_2.SetFileNumber(file_number);
      mc_2.SetMCName(mc_name.c_str());
      mc_2.SetInputFormat(input_format.c_str());
    }
    mc_2.SetOutputName(output_file.c_str());
    mc_2.SaveLevel2(savelvl2);
    if (!smearing) mc_2.DisableSmearing();
    if (average_pedestal) mc_2.AveragePedestal();
    mc_2.Initialise();
    if (list_file == "" && !auto_mhcombine) 
      mc_2.Convert();
    else if (auto_mhcombine)
      mc_2.ConvertSumArtificialAutomatic();
    else
      mc_2.ConvertSumArtificial(list_file);
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
