#include <TRint.h>

#include "FitDiscriminatorEfficiency.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput file\n");
  printf("\t-o <output>\t\tOutput folder\n");
  printf("\t--arm1\t\t\tArm1 (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string infile = "";
  string outdir = "";
  Int_t arm = -1;
  Int_t verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) infile = optarg;
        break;
      case 'o':
        if (optarg) outdir = optarg;
        break;
      case 1001:
        arm = Arm1Params::kArmIndex;
        break;
      case 1002:
        arm = Arm2Params::kArmIndex;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (infile == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (outdir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  FitDiscriminatorEfficiency<Arm1AnPars, Arm1Params> fde_1;
  FitDiscriminatorEfficiency<Arm2AnPars, Arm2Params> fde_2;
  if (arm == Arm1Params::kArmIndex) {
    fde_1.SetInputFile(infile.c_str());
    fde_1.SetOutputDir(outdir.c_str());
    fde_1.Run();
  } else if (arm == Arm2Params::kArmIndex) {
    fde_2.SetInputFile(infile.c_str());
    fde_2.SetOutputDir(outdir.c_str());
    fde_2.Run();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
