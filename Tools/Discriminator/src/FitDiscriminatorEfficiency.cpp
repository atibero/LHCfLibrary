#include "FitDiscriminatorEfficiency.hh"

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*--------------------*/
/*--- Fit Function ---*/
/*--------------------*/
Double_t sigmoid(Double_t *x, Double_t *par) {
  Double_t func = 1. / (1. + TMath::Exp(-par[0] * (x[0] - par[1])));
  return func;
}

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename arman, typename armclass>
FitDiscriminatorEfficiency<arman, armclass>::FitDiscriminatorEfficiency() {
  fOutputFile = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename arman, typename armclass>
FitDiscriminatorEfficiency<arman, armclass>::~FitDiscriminatorEfficiency() {
  delete fOutputFile;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::GetInputHistograms() {
  UT::Printf(UT::kPrintInfo, "Getting input histograms...");
  fflush(stdout);

  TFile file(fInputFileName.Data(), "READ");
  if (file.IsZombie()) {
    file.Close();
    UT::Printf(UT::kPrintError, "File %s does not exist: Exit...\n", fInputFileName.Data());
  }

  Utils::AllocateVector2D(fDiscrimEff, this->kCalNtower, this->kCalNlayer);
  Utils::AllocateVector2D(fDiscrimFun, this->kCalNtower, this->kCalNlayer);
  Utils::AllocateVector1D(fDiscrimThr, this->kCalNtower);

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      TString NameHis = Form("DiscriminatorEff_Tower%d_Layer%d", it, il);
      fDiscrimEff[it][il] = NULL;
      fDiscrimFun[it][il] = NULL;
      fDiscrimEff[it][il] = (TGraphAsymmErrors *)file.Get(NameHis.Data());
      if (fDiscrimEff[it][il] == NULL) {
        UT::Printf(UT::kPrintError, "Histogram %s does not exist: Exit...\n", NameHis.Data());
        continue;
      }
      fDiscrimFun[it][il] = new TF1(Form("Fit_%s", fDiscrimEff[it][il]->GetName()), sigmoid, 0., 10., 2);
    }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile = new TFile(Form("%s/FitDiscriminatorEfficiency.root", fOutputDirName.Data()), "RECREATE");
  fOutputFile->cd();

  SaveFitResult();

  fOutputFile->Close();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::SaveFitResult() {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      fDiscrimEff[it][il]->Write();
      fDiscrimFun[it][il]->Write();
    }
    fDiscrimThr[it]->Write();
  }
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::OpenFiles() {
  UT::Printf(UT::kPrintInfo, "Opening output file...");
  fflush(stdout);

  fOfstream.open(Form("%s/FitDiscriminatorEfficiency.dat", fOutputDirName.Data()));
  fOfstream << "#Tower\tLayer\tvalid\tchi2\tndof\ta\t\tΔa\t\tb\t\tΔb";
  for (Int_t iv = 0; iv < efficiency.size(); ++iv) fOfstream << "\t\tE(" << 100. * efficiency[iv] << "%)";
  fOfstream << endl;

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOfstream.close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*----------------------------*/
/*--- Fit & Draw functions ---*/
/*----------------------------*/
template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::ComputeValue(Double_t a, Double_t b, Double_t eff, Double_t &thr) {
  thr = ((TMath::Log(eff / (1 - eff))) / a) + b;
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::Fit(Int_t tower, Int_t layer, TGraphAsymmErrors *g, TF1 *f) {
  UT::Printf(UT::kPrintInfo, "\n=== FITTING: %s\n\n", g->GetName());

  Double_t minval = g->GetX()[0] - g->GetEXlow()[0];
  Double_t maxval = g->GetX()[g->GetN() - 1] - g->GetEXlow()[g->GetN() - 1];

  f->SetRange(minval, maxval);

  f->SetParName(0, "a");
  f->SetParName(1, "b");
  // TODO: Check Parameter initialization with real data
  // f->SetParameter(0, 0.);
  // f->SetParameter(1, 0.);
  f->SetParLimits(0, 0., 50.);
  f->SetParLimits(1, 0., 10.);

  g->Fit(f, "RIME");

  if (!f->IsValid()) {
    UT::Printf(UT::kPrintInfo, "===             \t\t Valid=NO\n");
    exit(EXIT_FAILURE);  // TODO: Handle this case...
  }
  UT::Printf(UT::kPrintInfo, "\t\t Valid=YES\n");
  UT::Printf(UT::kPrintInfo, "  ChiSquare = %f with Ndof = %d\n", f->GetChisquare(), f->GetNDF());
  UT::Printf(UT::kPrintInfo, "   a = %f ± %f\n", f->GetParameter(0), f->GetParError(0));
  UT::Printf(UT::kPrintInfo, "   b = %f ± %f\n", f->GetParameter(1), f->GetParError(1));
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::Draw(Int_t tower, Int_t layer, TGraphAsymmErrors *g, TF1 *f) {
  TGaxis::SetMaxDigits(3);

  g->Draw("AP");

  f->Draw("SAME");

  gPad->Modified();
  gPad->Update();
  Double_t aval = f->GetParameter(0);
  Double_t bval = f->GetParameter(1);
  Double_t ethreshold = 0;
  Double_t efficiency = 0.95;
  ComputeValue(aval, bval, efficiency, ethreshold);
  g->GetXaxis()->SetLimits(ethreshold - 1.0, ethreshold + 1.0);
  gPad->Modified();
  gPad->Update();
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::Print(Int_t tower, Int_t layer, TGraphAsymmErrors *g, TF1 *f) {
  Double_t aval = f->GetParameter(0);
  Double_t bval = f->GetParameter(1);
  Double_t aerr = f->GetParError(0);
  Double_t berr = f->GetParError(1);

  vector<Double_t> threshold(efficiency.size(), 0.);
  for (Int_t iv = 0; iv < efficiency.size(); ++iv) {
    ComputeValue(aval, bval, efficiency[iv], threshold[iv]);
    TLine *l = new TLine(threshold[iv], gPad->GetUymin(), threshold[iv], gPad->GetUymax());
    l->SetLineStyle(3);
    l->Draw();
  }

  gPad->Modified();
  gPad->Update();
  TPaveStats *stats = (TPaveStats *)g->GetListOfFunctions()->FindObject("stats");
  Double_t statsize = 0.5 * (stats->GetY2NDC() - stats->GetY1NDC());
  stats->SetY1NDC(0.5 - statsize);
  stats->SetY2NDC(0.5 + statsize);
  stats->Draw();
  gPad->Modified();
  gPad->Update();

  streamsize ss = std::cout.precision();
  fOfstream << tower << "\t" << layer << "\t" << f->IsValid() << "\t" << f->GetChisquare() << "\t" << f->GetNDF();
  fOfstream << setprecision(3);
  fOfstream << std::scientific;
  fOfstream << "\t" << aval << "\t" << aerr << "\t" << bval << "\t" << berr;
  for (Int_t iv = 0; iv < threshold.size(); ++iv) fOfstream << "\t" << threshold[iv];
  fOfstream.unsetf(ios_base::floatfield);
  fOfstream << setprecision(ss);
  fOfstream << endl;
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::FitEfficiency() {
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1111);
  gStyle->SetFitFormat("3.3g");
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    TString canvasName = Form("%s/Discriminator_Efficiency_%s.eps", fOutputDirName.Data(), (it == 0 ? "TS" : "TL"));
    TCanvas *c = new TCanvas(Form("canvas%d", it), "", 600, 600);
    c->Divide(4, 4);
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      if (fDiscrimEff[it][il] == NULL || fDiscrimFun[it][il] == NULL) continue;

      c->cd(il + 1);
      Fit(it, il, fDiscrimEff[it][il], fDiscrimFun[it][il]);
      Draw(it, il, fDiscrimEff[it][il], fDiscrimFun[it][il]);
      Print(it, il, fDiscrimEff[it][il], fDiscrimFun[it][il]);
    }
    c->Print(canvasName.Data());
  }
}

template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::PlotDiscriminatorThreshold() {
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  gStyle->SetFitFormat("3.3g");

  TString canvasName = Form("%s/Discriminator_Threshold.eps", fOutputDirName.Data());
  TCanvas *c = new TCanvas("canvas", "", 1200, 600);
  c->Divide(2, 1);

  Double_t ethreshold = 0;
  Double_t efficiency = 0.99;
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    c->cd(it + 1);
    TString name = Form("DiscriminatorThresholdTower%d", it);
    TString title = Form("; Layer; %.0f%% Efficiency Threshold [GeV]", it, 100. * efficiency);
    fDiscrimThr[it] = new TH1D(name.Data(), title.Data(), this->kCalNlayer, -0.5, this->kCalNlayer - 0.5);
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      Double_t aval = fDiscrimFun[it][il]->GetParameter(0);
      Double_t bval = fDiscrimFun[it][il]->GetParameter(1);
      ComputeValue(aval, bval, efficiency, ethreshold);
      fDiscrimThr[it]->Fill(il, ethreshold);
    }
    fDiscrimThr[it]->Draw("HIST");
  }
  c->Print(canvasName.Data());
}

/*------------------*/
/*--- Class Core ---*/
/*------------------*/
template <typename arman, typename armclass>
void FitDiscriminatorEfficiency<arman, armclass>::Run() {
  GetInputHistograms();

  OpenFiles();

  FitEfficiency();

  PlotDiscriminatorThreshold();

  WriteToOutput();

  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class FitDiscriminatorEfficiency<Arm1AnPars, Arm1Params>;
template class FitDiscriminatorEfficiency<Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
