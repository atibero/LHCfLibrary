#ifndef FitDiscriminatorEfficiency_HH
#define FitDiscriminatorEfficiency_HH

#include <Minuit2/FCNBase.h>
#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/MnMigrad.h>
#include <Minuit2/MnMinos.h>
#include <Minuit2/MnPlot.h>
#include <Minuit2/MnPrint.h>
#include <Minuit2/MnScan.h>
#include <Minuit2/MnUserParameters.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TEfficiency.h>
#include <TF1.h>
#include <TF2.h>
#include <TFile.h>
#include <TFitResult.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLine.h>
#include <TMarker.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TPaletteAxis.h>
#include <TPaveStats.h>
#include <TPostScript.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>
#include <TVirtualX.h>
#include <dirent.h>

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <map>

#include "Arm1AnPars.hh"
#include "Arm1Params.hh"
#include "Arm2AnPars.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"

using namespace std;

namespace nLHCf {

template <typename arman, typename armclass>
class FitDiscriminatorEfficiency : public arman, public LHCfParams {
 public:
  FitDiscriminatorEfficiency();
  ~FitDiscriminatorEfficiency();

 private:
  /*--- Input/Output ---*/
  TString fInputFileName;
  TString fOutputDirName;

  TFile *fOutputFile;
  ofstream fOfstream;

  /*--- Efficiency Graphs ---*/
  vector<vector<TGraphAsymmErrors *>> fDiscrimEff;
  vector<vector<TF1 *>> fDiscrimFun;
  vector<TH1D *> fDiscrimThr;

  /*--- Reference Efficiency ---*/
  vector<Double_t> efficiency = {0.5000, 0.9900, 0.9999};

 public:
  void SetInputFile(const Char_t *name) { fInputFileName = name; }
  void SetOutputDir(const Char_t *name) { fOutputDirName = name; }
  void Run();

 private:
  void GetInputHistograms();
  void WriteToOutput();
  void SaveFitResult();
  void OpenFiles();
  void CloseFiles();
  void ComputeValue(Double_t a, Double_t b, Double_t eff, Double_t &thr);
  void Fit(Int_t tower, Int_t layer, TGraphAsymmErrors *g, TF1 *f);
  void Draw(Int_t tower, Int_t layer, TGraphAsymmErrors *g, TF1 *f);
  void Print(Int_t tower, Int_t layer, TGraphAsymmErrors *g, TF1 *f);
  void FitEfficiency();
  void PlotDiscriminatorThreshold();
};

}  // namespace nLHCf
#endif
