#ifndef CheckRunIIIData_HH
#define CheckRunIIIData_HH

#include <TCanvas.h>
#include <TChain.h>
#include <TEfficiency.h>
#include <TFile.h>
#include <TGraphAsymmErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TLine.h>
#include <TString.h>

#include "Arm1AnPars.hh"
#include "Arm1CalPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2CalPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "DataModification.hh"
#include "EventCut.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"
#include "PionRec.hh"

using namespace std;

namespace nLHCf {

template <typename armcal, typename armrec, typename arman, typename armclass>
class CheckRunIIIData : public armcal, public armrec, public arman, public LHCfParams {
 public:
  CheckRunIIIData();
  ~CheckRunIIIData();

 private:
  /*--- Input/Output ---*/
  static const Int_t fN = 2;
  vector<McParticle *> fParticleArray[fN];

  Bool_t fUseRapidity;

  Bool_t fSinglehit;
  Bool_t fMultihit;
  Bool_t fSWTrgEff;
  Bool_t fAlignment;
  Bool_t fHitmap;
  Bool_t fDiscrimin;
  Bool_t fPileUp;
  Bool_t fSilicon;
  Bool_t fMass;
  Bool_t fSPS;
  Bool_t fLHC;
  Bool_t fGSOCalib;
  Bool_t fPosDet;
  Bool_t fSilEn;
  Bool_t fSilLeak;

  TString fInputFile;
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  Int_t fSubRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;

  Int_t fEntry;
  Int_t fNevents;
  Double_t fFiducialEdge;
  Bool_t fRotDet;
  Bool_t fHadEle;
  Bool_t fModify;

  /*--- Level0/1/2 ---*/
  McEvent *fMC;
  Level1<armclass> *fLvl1;
  Level2<armclass> *fLvl2;
  Level3<armrec> *fLvl3;

  /*--- Event Cut ---*/
  EventCut<arman, armrec> fEveCut;

  /*--- ENUM structures ---*/
  enum TRIGGER {
    kShowerTrg = 0,
    kPi0Trg = 1,
    kHighEMTrg = 2,
    kHadronTrg = 3,
    kZdcTrg = 4,
    kFcTrg = 5,
    kL1tTrg = 6,
    kLaserTrg = 7,
    kPedestalTrg = 8,
    nTRIGGER = kPedestalTrg + 1
  };

  enum PILEUP { kST = 0, kLT = 1, kTotal = 2, kCrossing = 3, kPedestal = 4, nPILEUP = kPedestal + 1 };

  /*--- Corresponding labels ---*/
  string LabelTRIGGER[nTRIGGER]{"Shower",
                                "Pi0"
                                "HighEM",
                                "Hadron",
                                "Zdc",
                                "Fc",
                                "L1t",
                                "Laser",
                                "Pedestal"};

  string LabelPILEUP[nPILEUP]{"ST Trigger", "LT Trigger", "Total Trigger", "Crossing", "Pedestal"};

  /*--- Trigger Histograms ---*/
  TH1D *fTriggerCount;

  /*--- Singlehit Histograms ---*/
  vector<vector<Double_t>> fPhotonEbins;

  /*--- Singlehit Histograms ---*/
  vector<vector<TH1D *>> fCaldE[fN];
  vector<vector<TH2D *>> fCaldEmap[fN];
  vector<TH1D *> fRecoEnergy[fN];
  TH2D *fRecoHitmap[fN];
  vector<TH2D *> fRecoTower[fN];
  vector<TH1D *> fTrueEnergy[fN];
  TH2D *fTrueHitmap[fN];
  vector<TH2D *> fTrueTower[fN];
  vector<TH1D *> fEneRes[fN];
  vector<vector<TH1D *>> fPosRes[fN];

  /*--- Multihit Histograms ---*/
  vector<vector<TH2D *>> fMHmigration;
  vector<vector<TH2D *>> fEfficiency;
  vector<vector<TH2D *>> fPurity;
  vector<vector<TH2D *>> fF1;
  vector<vector<TH1D *>> fEfficiency1D;
  vector<vector<TH1D *>> fPurity1D;
  vector<vector<TH1D *>> fF11D;
  vector<vector<TH2D *>> fNotEfficiency;
  vector<vector<TH2D *>> fNotPurity;

  /*--- Software Trigger Efficiency Histograms ---*/
  vector<TH1D *> fSWTriggerAll;
  vector<TH1D *> fSWTriggerYes;
  vector<TGraphAsymmErrors *> fSWTriggerEff;

  /*--- PosDet Histograms ---*/
  vector<vector<vector<vector<TH1D *>>>> fRawPosDet;
  /*--- PosDet MAX signal Histograms ---*/
  vector<vector<vector<vector<TH1D *>>>> fRawPosDetMAX;

  /*--- Alignment Histograms ---*/
  vector<vector<vector<TH1D *>>> fFitAmplitude;
  vector<vector<vector<TH1D *>>> fRedChiSquare;
  vector<vector<vector<vector<TH1D *>>>> fRelAlignment;
  vector<vector<vector<vector<TH1D *>>>> fRelAlignmentMaskPACE;
  vector<vector<vector<TH2D *>>> fRedChiVsFitAmp;
  vector<vector<vector<TH2D *>>> fRecPosVsRelAli;
  vector<vector<vector<TH2D *>>> fRecPosVsRelAliMaskPACE;

  /*--- Hitmap Histograms ---*/
  vector<vector<vector<TH1D *>>> fVertexZaverage;
  vector<vector<vector<vector<TH1D *>>>> fVertexHitmap1D;
  vector<vector<vector<vector<TH2D *>>>> fVertexHitmap2D;
  vector<vector<vector<vector<TH2D *>>>> fVertexPDChan2D;

  /*--- Invariant Mass Histograms ---*/
  vector<TH1D *> fInvMassTot;
  vector<vector<TH1D *>> fInvMassRun;
  vector<vector<TH1D *>> fInvMassTotXf;
  vector<vector<vector<TH1D *>>> fInvMassRunXf;

  /*--- Discriminator Histograms ---*/
  vector<vector<TH1D *>> fDiscrimYes;
  vector<vector<TH1D *>> fDiscrimAll;
  vector<vector<TGraphAsymmErrors *>> fDiscrimEff;

  /*--- Pile-Up Histograms ---*/
  TH1D *fPileUpCount;
  vector<TH1D *> fPileUpL90;
  vector<TH1D *> fPileUpEnergy;
  vector<vector<TH1D *>> fPileUpL90Particle;
  vector<vector<TH1D *>> fPileUpEnergyParticle;

  /*--- Silicon Histograms ---*/
  TH2D *fSiRecoHitmap;
  vector<TH2D *> fSiRecoTower;
  vector<TH1D *> fSiRecoEnergy;
  vector<TH1D *> fSiRecoSiliconEnergy;
  vector<TH2D *> fSiRecoEnergyCorrelation;
  TH2D *fSiTrueHitmap;
  vector<TH2D *> fSiTrueTower;
  vector<TH1D *> fSiTrueEnergy;
  vector<TH1D *> fSiEneRes;
  vector<vector<TH1D *>> fSiPosRes;
  vector<TH1D *> fSiSiliconEneRes;

  /*--- SPS Histograms ---*/
  vector<vector<TH1D *>> fSPSCaldE;
  vector<vector<TH2D *>> fSPSCaldEmap;
  vector<TH1D *> fSPSAvedE;
  vector<TH1D *> fSPSL20;
  vector<TH1D *> fSPSL90;
  vector<TH1D *> fSPSL2D;
  vector<TH1D *> fSPSRecoEnergy;
  TH2D *fSPSRecoHitmap;
  vector<TH2D *> fSPSRecoTower;
  vector<TH1D *> fSPSTrueEnergy;
  TH2D *fSPSTrueHitmap;
  vector<TH2D *> fSPSTrueTower;
  vector<TH1D *> fSPSEneRes;
  vector<vector<TH1D *>> fSPSPosRes;

  /*--- LHC Histograms ---*/
  vector<vector<TH1D *>> fLHCCaldE;
  vector<vector<TH2D *>> fLHCCaldEmap;
  vector<TH1D *> fLHCAvedE;
  vector<TH1D *> fLHCSumdE;
  vector<TH1D *> fLHCL20;
  vector<TH1D *> fLHCL90;
  vector<TH1D *> fLHCL2D;
  vector<TH1D *> fLHCRecoEnergy;
  TH2D *fLHCRecoHitmap;
  vector<TH2D *> fLHCRecoTower;
  vector<TH1D *> fLHCTrueEnergy;
  TH2D *fLHCTrueHitmap;
  vector<TH2D *> fLHCTrueTower;
  vector<TH1D *> fLHCEneRes;
  vector<vector<TH1D *>> fLHCPosRes;

  /*--- GSO Calibration Histograms ---*/
  vector<vector<TH2D *>> fGSOCalibTower;
  vector<vector<vector<TH1D *>>> fGSOCalibCaldE;
  vector<vector<vector<vector<TH1D *>>>> fGSOCalibCalMC;
  vector<vector<TProfile *>> fGSOCalibAvedE;

  /*--- Silicon Study Histograms ---*/
  vector<vector<vector<TH1D *>>> fSilDep;
  vector<vector<TH2D *>> fSilDepXYCorr;
  vector<TH1D *> fSilDepTot;
  vector<TH1D *> fSilDepTotXYMean;
  vector<TH1D *> fSilDepTotYviews;
  vector<TH1D *> fSilDepTot01layers;
  vector<TH1D *> fSilDepTot2;
  vector<TH1D *> fSilDepTotXYMean2;
  vector<TH1D *> fSilDepTotYviews2;
  //vector<TH1D *> fSilDepTot_onebin;
  //vector<TH1D *> fSilDepTotXYMean_onebin;
  //vector<TH1D *> fSilDepTotYviews_onebin;
  //vector<TH1D *> fSilDepTot01layers_onebin;
  vector<vector<vector<vector<TH1D *>>>> fPhoSilArea;
  vector<vector<TH1D *>> fPhoSilAreaTot;
  vector<vector<vector<vector<TH2D *>>>> fCorrelSilEn;
  vector<vector<TH2D *>> fCorrelSilEnTot;
  vector<vector<TH2D *>> fCorrelRecoEnPhoSilArea;
  vector<vector<TH2D *>> fCorrelRecoXPhoSilArea;
  vector<vector<TH2D *>> fCorrelRecoYPhoSilArea;
  vector<TH2D *> fCorrelRecoEnSilDep;
  vector<TH2D *> fCorrelRecoXSilDep;
  vector<TH2D *> fCorrelRecoYSilDep;
  vector<TH2D *> fCorrelSil1YCalo4;
  vector<TH2D *> fCorrelSil1YCalo5;
  vector<vector<TH1D *>> fCaloDep;
  vector<TH2D *> fSilDepMap;
  vector<TH2D *> fSilDepMapCounts;
  vector<vector<vector<TH2D *>>> fSilDepMapLayer;
  vector<vector<vector<TH2D *>>> fSilDepMapLayerCounts;
  vector<TH2D *> fSilDepMapSum;
  vector<vector<vector<TH2D *>>> fSilDepMapLayerSum;
  vector<TH2D *> fSilDepMapLeakIn;
  vector<TH2D *> fSilDepMapLeakInCounts;
  vector<vector<vector<TH2D *>>> fSilDepMapLeakInLayer;
  vector<vector<vector<TH2D *>>> fSilDepMapLeakInLayerCounts;
  vector<TH2D *> fSilDepMapLeakInSum;
  vector<vector<vector<TH2D *>>> fSilDepMapLeakInLayerSum;
  vector<TH2D *> fSilRecoTower;
  vector<TH1D *> fSilRecoEnergy;
  vector<TH1D *> fPhotonSilEn;
  vector<TH2D *> fPhotonSilEnergyMap;
  vector<TH2D *> fPhotonSilCountsMap;
  vector<TH2D *> fPhotonSilAverageMap;

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetSubRun(Int_t subrun) { fSubRun = subrun; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SetInputFile(const Char_t *name) { fInputFile = name; }
  void SetEvents(Int_t ev) { fNevents = ev; }
  void SetUseRapidity(Bool_t urap) { fUseRapidity = urap; }
  void SetFiducialEdge(Double_t edge) { fFiducialEdge = edge; }
  void SetRotatedDetector(Bool_t rotdet) { fRotDet = rotdet; }
  void SetHadronOrElemag(Bool_t hadele) { fHadEle = hadele; }
  void SetModificationFlag(Bool_t modify) { fModify = modify; }
  void SetToFillHisto(Bool_t singlehit, Bool_t multihit, Bool_t swtrgeff, Bool_t alignment,
                      Bool_t hitmap, Bool_t mass, Bool_t discrimin, Bool_t pileup, Bool_t silicon, Bool_t sps, Bool_t lhc, Bool_t gsocalib, Bool_t posdet, Bool_t silen, Bool_t silleak) {
    fSinglehit = singlehit;
    fMultihit = multihit;
    fSWTrgEff = swtrgeff;
    fAlignment = alignment;
    fHitmap = hitmap;
    fMass = mass;
    fDiscrimin = discrimin;
    fPileUp = pileup;
    fSilicon = silicon;
    fSPS = sps;
    fLHC = lhc;
    fGSOCalib = gsocalib;
    fPosDet = posdet;
    fSilEn = silen;
    fSilLeak = silleak;
  }
  Bool_t TrueParticleList();
  Bool_t SelectTrueHadron(Int_t tower, Int_t code, Double_t eTrue, Double_t xTrue, Double_t yTrue,
                          TString cuts = "PID");
  Bool_t SelectTruePhoton(Int_t tower, Int_t code, Double_t eTrue, Double_t xTrue, Double_t yReco,
                          TString cuts = "PID");
  Bool_t SelectRecoHadron(Int_t tower, Bool_t swTrigger, Double_t eReco, Double_t xReco, Double_t yReco, Double_t L2D,
                          TString cuts = "SWT ENE POS PID");
  Bool_t SelectRecoPhoton(Int_t tower, Bool_t swTrigger, Double_t eReco, Double_t xReco, Double_t yReco, Double_t L90,
                          TString cuts = "ENE POS PID");
  Bool_t SelectStrictPhoton(Int_t tower, Bool_t swTrigger, Double_t mhReco, Double_t xReco, Double_t yReco, Double_t L90, Double_t eReco,
                          TString cuts = "HIT SWT ENE POS PID");
  Int_t RapidityCut(Int_t index, Double_t x, Double_t y);
  Double_t GetPeakArea(Int_t tower, Int_t layer, Int_t view);
  void FillTriggerHisto();
  void FillSingleHitHisto();
  void FillMultiHitHisto();
  void FillSWTrgEffHisto();
  void FillAlignmentHisto();
  void FillHitmapHisto();
  void FillMassHisto();
  void FillDiscriminHisto();
  void FillPileUpHisto();
  void FillSPSHisto();
  void FillLHCHisto();
  void FillGSOCalibHisto();
  void FillSiliconHisto();
  void FillPosDetHisto();
  void FillPosDetMaximum();
  void FillSiliconDepositHisto();
  void FillPhotonSiliconAreaHisto();
  void FillCorrelationSilEnHisto();
  void FillSilDepMapHisto();
  void FillPhotonSiliconEnergyHisto();
  void FillRecoPosEnHisto();
  void FillPhotonSiliconMapHisto();
  void NormalizeTriggerHisto();
  void NormalizeSingleHitHisto();
  void NormalizeMultiHitHisto();
  void NormalizeSWTrgEffHisto();
  void NormalizeDiscriminHisto();
  void NormalizePileUpHisto();
  void NormalizeSPSHisto();
  void NormalizeLHCHisto();
  void MeanSilDepMapHisto();
  void MeanSilEneMapHisto();
  void DrawPosDetHisto();
  void CalculateSumdE(vector<vector<Double_t>> &cal, vector<Double_t> &sumdE, Bool_t isProEle, Bool_t isRotated);
  Double_t GetPhotonSumdE(Int_t it, vector<vector<Double_t>> &cal);
  Double_t GetNeutronSumdE(Int_t it, vector<vector<Double_t>> &cal);
  Double_t GetPhotonSumdEFromBackSide(Int_t it, vector<vector<Double_t>> &cal);
  Double_t GetNeutronSumdEFromBackSide(Int_t it, vector<vector<Double_t>> &cal);
  void CalculateL20L90L2D(vector<vector<Double_t>> &cal, vector<Double_t> &L20, vector<Double_t> &L90,
                          vector<Double_t> &L2D, Bool_t isRotated);
  void CalculateL20L90(vector<vector<Double_t>> &cal, vector<Double_t> &l20, vector<Double_t> &l90);
  Double_t GetLayerDepth(Int_t it, Int_t il);
  void CalculateL20L90FromBackSide(vector<vector<Double_t>> &cal, vector<Double_t> &l20, vector<Double_t> &l90);
  Double_t GetLayerDepthFromBackSide(Int_t it, Int_t il);
  Double_t L90Boundary(Int_t tower, Double_t energy, Int_t type, Int_t mode);
  void Run();

 private:
  /*--- Particle ---*/
  const vector<TString> fParticle = {"Neutron", "Photon"};

  /*--- Multihit identification ---*/
  const vector<Double_t> migrationEne = {0., 2000., 4000., 6000., 20000.};
  const map<UInt_t, UInt_t> migrationMap = {
      {0, 0},                                                                         // NoHit
      {1, 1},   {2, 1},                                                               // SingleHit
      {22, 2},  {21, 3},  {12, 4},  {11, 5},                                          // DoubleHit
      {222, 6}, {221, 6}, {212, 6}, {211, 6}, {122, 6}, {121, 6}, {112, 6}, {111, 6}  // TripleHit
  };
  const map<UInt_t, string> migrationLab = {
      {0, "0"},                                    // NoHit
      {1, "SH"},                                   // SingleHit
      {2, "HH"}, {3, "HG"}, {4, "GH"}, {5, "GG"},  // DoubleHit
      {6, "TH"}                                    // TripleHit
  };

  /*--- Hitmap energy threshold ---*/
  const vector<Double_t> hitmapEne = {1000., 4000., 7000.};  // GeV

  /*--- Tower alignment table ---*/
  const vector<vector<vector<Double_t>>> towerEdge = {
      {{218, 375}, {219, 376}},  // TS x and y
      {{6, 207}, {8, 209}}       // TL x and y
  };

  /*--- Discriminator energy threshold ---*/
  const vector<vector<Double_t>> discriminThreshold = {
      {0.53242, 0.565503, 0.588973, 0.621501, 1.18885, 6.58392e-1, 1.65133, 0.514235, 0.681353, 1.24568, 0.497217,
       1.12938, 0.75516, 1.13262, 1.15236, 0.573391},  // Small Tower
      {0.69968, 0.675582, 0.690171, 0.703855, 0.82589, 6.02084e-1, 0.650602, 0.73455, 0.845967, 0.81544, 0.519089,
       0.69916, 0.38498, 0.51875, 0.67539, 0.552278}  // Large Tower
  };                                                  // GeV

  /*--- Invariant mass reconstruction ---*/
  const Int_t fNType = 5;
  const Int_t fNxFBin = 20;
  const vector<TString> fLabelType = {"TypeI", "TypeIITS", "TypeIITL", "TypeITS", "TypeITL"};

  /*--- Energy bin range ---*/
  const vector<Double_t> energyBinning = {
		  0., 500., 550., 600., 650., 700., 750., 800., 850., 900, 950., 1000.,
		  1050., 1100., 1150., 1200., 1250., 1300., 1350., 1400., 1450., 1500.,
		  1600., 1700., 1800., 1900., 2000.,
		  2100., 2200., 2300., 2400., 2500.,
		  2600., 2700., 2800., 2900., 3000.,
		  10000.
  };

  void SetEnergyBinning();
  void SetInputTree();
  void MakeHisto();
  void MakeTriggerHisto();
  void MakeSinglehitHisto();
  void MakeMultihitHisto();
  void MakeSWTrgEffHisto();
  void MakeAlignmentHisto();
  void MakeHitmapHisto();
  void MakeMassHisto();
  void MakeDiscriminHisto();
  void MakePileUpHisto();
  void MakeSPSHisto();
  void MakeLHCHisto();
  void MakeGSOCalibHisto();
  void MakeSiliconHisto();
  void MakePosDetHisto();
  void MakePosDetMaximum();
  void MakeSiliconDepositHisto();
  void MakePhotonSiliconAreaHisto();
  void MakeCorrelationSilEnHisto();
  void MakeRecoPosEnHisto();
  void MakePhotonSiliconEnergyHisto();
  void MakeSilDepMapHisto();
  void MakePhotonSilEneMapHisto();
  void ClearEvent();
  void WriteToOutput();
  void CloseFiles();

  TVector3 GetTrueColCoord(TVector3 posLHC);
  TVector3 GetTrueCalCoord(Int_t tower, TVector3 posCol);
  TVector3 GetRecoColCoord(Int_t tower, Double_t posXarm, Double_t posYarm, Int_t maxXlay, Int_t maxYlay);
};
}  // namespace nLHCf
#endif
