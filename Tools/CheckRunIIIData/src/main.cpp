#include <TRint.h>

#include "CheckRunIIIData.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (or input file if ending with \".root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = -1)\n");
  printf("\t-s <subrun>\t\tSubrun to be analysed (default = -1)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"converted.root\")\n");
  printf("\t-n <nev>\t\t# of events to be analysed (default = all\n");
  printf("\t-t <tables_dir>\t\t\tSet tables directory (default = \"../Tables\")\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> and <subfill> are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t-E <edge>\t\tFiducial edge cut (default = 2 mm)\n");
  printf("\t-X <fill>\t\tSpecify non zero MC Beam X offset (default = 0)\n");
  printf("\t-Y <fill>\t\tSpecify non zero MC Beam Y offset (default = 0)\n");
  printf("\t--arm1\t\t\tArm1 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--is-rapidity\t\tUse rapidity-based instead of tower-based plot\n");
  printf("\t--is-rotated\t\tData refers to SPS rotated instead of standard detector\n");
  printf("\t--is-hadron\t\tData refers to SPS/LHC hadron instead of photon\n");
  printf("\t--modify\t\tCorrect absolute energy scale (default = 0)\n");
  printf("\t--mc-singlehit\t\tFill true singlehit histos (MC only)\n");
  printf("\t--mc-multihit\t\tFill true multihit histos (MC only)\n");
  printf("\t--sw-trigger\t\tFill software trigger efficiency histos (MC only)\n");
  printf("\t--discriminator\t\tFill discriminator efficiency histos (Data only)\n");
  printf("\t--pile-up\t\tFill pile-up[pedestal] x acceptance histos (Data only)\n");
  printf("\t--alignment\t\tFill posdet alignment histos(Data only)\n");
  printf("\t--hitmap\t\tFill reco hitmap histos\n");
  printf("\t--mass\t\tFill invariant map histos\n");
  printf("\t--silicon\t\tFill silicon energy histos\n");
  printf("\t--sps\t\t\tFill distributions for SPS configuration\n");
  printf("\t--lhc\t\t\tFill distributions for LHC configuration\n");
  printf("\t--gsocalib\t\t\tFill distributions for GSO calibration\n");
  printf("\t--posdet\t\tFill silicon raw histos\n");
  printf("\t--silen\t\tFill silicon deposit histos and photon silicon area histos\n");
  printf("\t--silleak\t\tFill silicon deposit maps for leakage study\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  int first_run = -1;
  int last_run = -1;
  int sub_run = -1;
  string output_file = "check.root";
  int nev = -1;
  string table_folder = "";
  int fill = -1;
  int subfill = -1;
  double edge = -1.;
  int iarm = -1;
  int verbose = 1;
  bool userap = false;
  bool rotdet = false;
  bool hadele = false;
  bool modify = false;
  bool singlehit = false;
  bool multihit = false;
  bool swtrgeff = false;
  bool discrimin = false;
  bool pileup = false;
  bool alignment = false;
  bool hitmap = false;
  bool mass = false;
  bool silicon = false;
  bool sps = false;
  bool lhc = false;
  bool gsocalib = false;
  bool posdet = false;
  bool silen = false;
  bool silleak = false;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"subrun", required_argument, NULL, 's'},
      {"output", required_argument, NULL, 'o'},
      {"nev", required_argument, NULL, 'n'},
      {"tables-dir", required_argument, NULL, 't'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"fiducial-edge", required_argument, NULL, 'E'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"is-rapidity", no_argument, NULL, 1003},
      {"is-rotated", no_argument, NULL, 1004},
      {"is-hadron", no_argument, NULL, 1005},
      {"modify", no_argument, NULL, 1006},
      {"mc-singlehit", no_argument, NULL, 1101},
      {"mc-multihit", no_argument, NULL, 1102},
      {"sw-trigger", no_argument, NULL, 1103},
      {"discriminator", no_argument, NULL, 1201},
      {"pile-up", no_argument, NULL, 1202},
      {"alignment", no_argument, NULL, 1203},
      {"hitmap", no_argument, NULL, 1204},
      {"mass", no_argument, NULL, 1205},
      {"silicon", no_argument, NULL, 1206},
      {"sps", no_argument, NULL, 1301},
      {"lhc", no_argument, NULL, 1302},
      {"gsocalib", no_argument, NULL, 1303},
      {"posdet", no_argument, NULL, 1401},
      {"silen", no_argument, NULL, 1402},
      {"silleak", no_argument, NULL, 1403},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 's':
        if (optarg) sub_run = atoi(optarg);
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'n':
        if (optarg) nev = atoi(optarg);
        break;
      case 't':
        if (optarg) table_folder = optarg;
        break;
      case 'F':
        if (optarg) fill = atoi(optarg);
        break;
      case 'S':
        if (optarg) subfill = atoi(optarg);
        break;
      case 'E':
        if (optarg) edge = atof(optarg);
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 1003:
        userap = true;
        break;
      case 1004:
        rotdet = true;
        break;
      case 1005:
        hadele = true;
        break;
      case 1006:
        modify = true;
        break;
      case 1101:
        singlehit = true;
        break;
      case 1102:
        multihit = true;
        break;
      case 1103:
        swtrgeff = true;
        break;
      case 1201:
        discrimin = true;
        break;
      case 1202:
        pileup = true;
        break;
      case 1203:
        alignment = true;
        break;
      case 1204:
        hitmap = true;
        break;
      case 1205:
        mass = true;
        break;
      case 1206:
        silicon = true;
        break;
      case 1301:
        sps = true;
        break;
      case 1302:
        lhc = true;
        break;
      case 1303:
        gsocalib = true;
        break;
      case 1401:
        posdet = true;
        break;
      case 1402:
        silen = true;
        break;
      case 1403:
        silleak = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d -> %d\n", first_run, last_run);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  Utils::SetVerbose(verbose);

  /*--- Check ---*/

  if (table_folder != "") {
    Arm1Params::SetTableDirectory(table_folder.c_str());
    Arm2Params::SetTableDirectory(table_folder.c_str());
  }
  Arm1AnPars::SetFill(fill, subfill);
  Arm2AnPars::SetFill(fill, subfill);

  CheckRunIIIData<Arm1CalPars, Arm1RecPars, Arm1AnPars, Arm1Params> chk_1;
  CheckRunIIIData<Arm2CalPars, Arm2RecPars, Arm2AnPars, Arm2Params> chk_2;
  if (iarm == Arm1Params::kArmIndex) {
    printf("\tArm1 table folder: %s\n", Arm1Params::GetTableDirectory().Data());
    chk_1.SetInputDir(input_dir.c_str());
    chk_1.SetFirstRun(first_run);
    chk_1.SetLastRun(last_run);
    if (sub_run >= 0) chk_1.SetSubRun(sub_run);
    chk_1.SetOutputName(output_file.c_str());
    chk_1.SetEvents(nev);
    chk_1.SetUseRapidity(userap);
    chk_1.SetRotatedDetector(rotdet);
    chk_1.SetHadronOrElemag(hadele);
    chk_1.SetModificationFlag(modify);
    if (edge >= 0) chk_1.SetFiducialEdge(edge);
    chk_1.SetToFillHisto(singlehit, multihit, swtrgeff, alignment, hitmap, mass, discrimin, pileup, silicon, sps, lhc,
                         gsocalib, posdet, silen, silleak);
    chk_1.Run();
  } else if (iarm == Arm2Params::kArmIndex) {
    printf("\tArm2 table folder: %s\n", Arm2Params::GetTableDirectory().Data());
    chk_2.SetInputDir(input_dir.c_str());
    chk_2.SetFirstRun(first_run);
    chk_2.SetLastRun(last_run);
    if (sub_run >= 0) chk_2.SetSubRun(sub_run);
    chk_2.SetOutputName(output_file.c_str());
    chk_2.SetEvents(nev);
    chk_2.SetUseRapidity(userap);
    chk_2.SetRotatedDetector(rotdet);
    chk_2.SetHadronOrElemag(hadele);
    chk_2.SetModificationFlag(modify);
    if (edge >= 0) chk_2.SetFiducialEdge(edge);
    chk_2.SetToFillHisto(singlehit, multihit, swtrgeff, alignment, hitmap, mass, discrimin, pileup, silicon, sps, lhc,
                         gsocalib, posdet, silen, silleak);
    chk_2.Run();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
