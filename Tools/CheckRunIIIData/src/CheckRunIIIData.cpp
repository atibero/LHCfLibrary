#include "CheckRunIIIData.hh"

#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>
#include <map>

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
CheckRunIIIData<armcal, armrec, arman, armclass>::CheckRunIIIData()
    : fFirstRun(-1),
      fLastRun(-1),
      fSubRun(-1),
      fNevents(-1),
      fFiducialEdge(2.),
      fUseRapidity(false),
      fRotDet(false),
      fHadEle(false),
      fModify(false),
      fSinglehit(false),
      fMultihit(false),
      fAlignment(false),
      fHitmap(false),
      fMass(false),
      fDiscrimin(false),
      fPileUp(false),
      fSilicon(false),
      fSPS(false),
      fLHC(false),
      fGSOCalib(false),
      fPosDet(false),
      fSilEn(false),
      fSilLeak(false) {
  fEntry = 0;

  fOutputFile = nullptr;
  fInputEv = nullptr;

  fMC = nullptr;
  fLvl1 = nullptr;
  fLvl2 = nullptr;
  fLvl3 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
CheckRunIIIData<armcal, armrec, arman, armclass>::~CheckRunIIIData() {
  delete fOutputFile;
  delete fInputEv;
}

/*--------------------------*/
/*--- Set energy binning ---*/
/*--------------------------*/

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::SetEnergyBinning() {
  Utils::Printf(Utils::kPrintDebug, "Setting energy binning...");
  fflush(stdout);

  Int_t AreaSize = fUseRapidity ? (fHadEle ? this->kNeutronNrap : this->kPhotonNrap) : this->kCalNtower;

  Utils::AllocateVector1D(fPhotonEbins, this->kPhotonNrap);
  for (Int_t ir = 0; ir < fPhotonEbins.size(); ++ir) {
    Int_t it = fPhotonEbins.size() == this->kCalNtower ? ir : this->RapToTower(ir);
    if (it == 0) {
      // small tower
      for (Int_t ie = 1; ie <= 20; ++ie) fPhotonEbins[ir].push_back((Double_t)ie * 100.);  // eugenio
      for (Int_t ie = 1; ie <= 10; ++ie) fPhotonEbins[ir].push_back(2000. + (Double_t)ie * 200.);
      for (Int_t ie = 1; ie <= 2; ++ie) fPhotonEbins[ir].push_back(4000. + (Double_t)ie * 500.);
      for (Int_t ie = 1; ie <= 2; ++ie) fPhotonEbins[ir].push_back(5000. + (Double_t)ie * 1000.);
    } else {
      // large tower
      for (Int_t ie = 1; ie <= 20; ++ie) fPhotonEbins[ir].push_back((Double_t)ie * 100.);  // eugenio
      for (Int_t ie = 1; ie <= 5; ++ie) fPhotonEbins[ir].push_back(2000. + (Double_t)ie * 200.);
      for (Int_t ie = 1; ie <= 2; ++ie) fPhotonEbins[ir].push_back(3000. + (Double_t)ie * 500.);
      for (Int_t ie = 1; ie <= 3; ++ie) fPhotonEbins[ir].push_back(4000. + (Double_t)ie * 1000.);
    }
  }

  // Rescale energy to use the same Feynman binning
  if (this->fOperation == kLHC2022) {
    for (Int_t ir = 0; ir < fPhotonEbins.size(); ++ir) {
      for (Int_t ie = 0; ie < fPhotonEbins[ir].size(); ++ie) {
        fPhotonEbins[ir][ie] *= 6.8 / 6.5;
      }
    }
  }

  Utils::Printf(Utils::kPrintDebug, " Done.\n");
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(fSubRun >= 0 ? Form("%s/rec_run%05d_%02d.root", fInputDir.Data(), irun, fSubRun)
                                 : Form("%s/rec_run%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
        TString jname(fSubRun >= 0 ? Form("%s/cal_run%05d_%02d.root", fInputDir.Data(), irun, fSubRun)
                                   : Form("%s/cal_run%05d.root", fInputDir.Data(), irun));
        TFile jfile(jname.Data(), "READ");
        if (jfile.IsZombie()) {
          jfile.Close();
          UT::Printf(UT::kPrintInfo, "skipping file %s\n", jname.Data());
        } else {
          jfile.Close();
          fInputTree->Add(jname.Data());
        }
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeHisto() {
  TH1::SetDefaultSumw2();

  SetEnergyBinning();

  MakeTriggerHisto();

  if (fSinglehit) MakeSinglehitHisto();
  if (fMultihit) MakeMultihitHisto();
  if (fSWTrgEff) MakeSWTrgEffHisto();
  if (fAlignment) MakeAlignmentHisto();
  if (fHitmap) MakeHitmapHisto();
  if (fMass) MakeMassHisto();
  if (fDiscrimin) MakeDiscriminHisto();
  if (fPileUp) MakePileUpHisto();
  if (fSPS) MakeSPSHisto();
  if (fLHC) MakeLHCHisto();
  if (fGSOCalib) MakeGSOCalibHisto();
  if (fSilicon) MakeSiliconHisto();

  // Silicon Study
  if (fPosDet) {
    MakePosDetHisto();
    MakePosDetMaximum();
  }
  if (fSilEn) {
    MakeSiliconDepositHisto();
    MakePhotonSiliconAreaHisto();
    MakeCorrelationSilEnHisto();
    MakeRecoPosEnHisto();
    MakePhotonSiliconEnergyHisto();
  }
  if (fSilLeak) {
    MakeSilDepMapHisto();
    MakePhotonSilEneMapHisto();
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeTriggerHisto() {
  Int_t nBin = nTRIGGER;
  Double_t nMin = -0.5;
  Double_t nMax = nMin + nTRIGGER;

  fTriggerCount = new TH1D("TriggerCount", ";;Counts", nBin, nMin, nMax);
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeSinglehitHisto() {
  TString tag = fUseRapidity ? "Region" : "Tower";

  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t mapBin[] = {this->kArmIndex == 0 ? 384 : 384, this->kArmIndex == 0 ? 384 : 384};
  Double_t mapMin[] = {this->kArmIndex == 0 ? -18.56 : -18.56, this->kArmIndex == 0 ? -13.88 : -13.88};
  Double_t mapMax[] = {this->kArmIndex == 0 ? 42.88 : 42.88, this->kArmIndex == 0 ? 47.56 : 47.56};

  Int_t depBin = 500;
  Double_t depMin = 0.;
  Double_t depMax = 10.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    depBin = 500;
    depMin = 0.;
    depMax = 2.5;
  }

  Int_t eneBin = 14;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    eneBin = 100.;
    eneMin = 0.;
    eneMax = 500.;
  }

  Int_t ereBin = 400;
  Double_t ereMin = -50.;
  Double_t ereMax = +50.;

  Int_t preBin = 1000;
  Double_t preMin = -10.;
  Double_t preMax = +10.;

  if (fUseRapidity) {
    Utils::AllocateVector2D(fCaldE[0], this->kNeutronNrap, this->kCalNlayer);
    Utils::AllocateVector2D(fCaldEmap[0], this->kNeutronNrap, this->kCalNlayer);
    Utils::AllocateVector1D(fRecoEnergy[0], this->kNeutronNrap);
    Utils::AllocateVector1D(fTrueEnergy[0], this->kNeutronNrap);
    Utils::AllocateVector1D(fEneRes[0], this->kNeutronNrap);
    Utils::AllocateVector2D(fPosRes[0], this->kNeutronNrap, this->kPosNview);

    Utils::AllocateVector2D(fCaldE[1], this->kPhotonNrap, this->kCalNlayer);
    Utils::AllocateVector2D(fCaldEmap[1], this->kPhotonNrap, this->kCalNlayer);
    Utils::AllocateVector1D(fRecoEnergy[1], this->kPhotonNrap);
    Utils::AllocateVector1D(fTrueEnergy[1], this->kPhotonNrap);
    Utils::AllocateVector1D(fEneRes[1], this->kPhotonNrap);
    Utils::AllocateVector2D(fPosRes[1], this->kPhotonNrap, this->kPosNview);
  } else {
    Utils::AllocateVector2D(fCaldE[0], this->kCalNtower, this->kCalNlayer);
    Utils::AllocateVector2D(fCaldEmap[0], this->kCalNtower, this->kCalNlayer);
    Utils::AllocateVector1D(fRecoEnergy[0], this->kCalNtower);
    Utils::AllocateVector1D(fTrueEnergy[0], this->kCalNtower);
    Utils::AllocateVector1D(fEneRes[0], this->kCalNtower);
    Utils::AllocateVector2D(fPosRes[0], this->kCalNtower, this->kPosNview);

    Utils::AllocateVector2D(fCaldE[1], this->kCalNtower, this->kCalNlayer);
    Utils::AllocateVector2D(fCaldEmap[1], this->kCalNtower, this->kCalNlayer);
    Utils::AllocateVector1D(fRecoEnergy[1], this->kCalNtower);
    Utils::AllocateVector1D(fTrueEnergy[1], this->kCalNtower);
    Utils::AllocateVector1D(fEneRes[1], this->kCalNtower);
    Utils::AllocateVector2D(fPosRes[1], this->kCalNtower, this->kPosNview);
  }

  Utils::AllocateVector1D(fRecoTower[0], this->kCalNtower);
  Utils::AllocateVector1D(fTrueTower[0], this->kCalNtower);

  Utils::AllocateVector1D(fRecoTower[1], this->kCalNtower);
  Utils::AllocateVector1D(fTrueTower[1], this->kCalNtower);

  for (Int_t ip = 0; ip < this->fN; ++ip) {
    fRecoHitmap[ip] = new TH2D(Form("%sRecoHitmap", fParticle[ip].Data()), "Reco Hitmap; x [mm]; y [mm]", mapBin[0],
                               mapMin[0], mapMax[0], mapBin[1], mapMin[1], mapMax[1]);
    fTrueHitmap[ip] = new TH2D(Form("%sTrueHitmap", fParticle[ip].Data()), "True Hitmap; x [mm]; y [mm]", mapBin[0],
                               mapMin[0], mapMax[0], mapBin[1], mapMin[1], mapMax[1]);
    for (Int_t it = 0; it < fCaldE[ip].size(); ++it) {
      fRecoTower[ip][it] = new TH2D(Form("%sRecoTower_%s%d", fParticle[ip].Data(), tag.Data(), it),
                                    Form("%s %d; x [mm]; y [mm];", tag.Data(), it), towBin[it], towMin[it], towMax[it],
                                    towBin[it], towMin[it], towMax[it]);
      fTrueTower[ip][it] = new TH2D(Form("%sTrueTower_%s%d", fParticle[ip].Data(), tag.Data(), it),
                                    Form("%s %d; x [mm]; y [mm];", tag.Data(), it), towBin[it], towMin[it], towMax[it],
                                    towBin[it], towMin[it], towMax[it]);
      for (Int_t il = 0; il < fCaldE[ip][0].size(); ++il) {
        fCaldE[ip][it][il] = new TH1D(Form("%sCaldE_%s%d_Layer%d", fParticle[ip].Data(), tag.Data(), it, il),
                                      Form("%s %d - Layer %d; dE [GeV]", tag.Data(), it, il), depBin, depMin, depMax);
        fCaldEmap[ip][it][il] = new TH2D(Form("%sCaldEmap_%s%d_Layer%d", fParticle[ip].Data(), tag.Data(), it, il),
                                         Form("%s %d - Layer %d; x [mm]; y [mm]; dE [GeV]", tag.Data(), it, il),
                                         towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
      }
      fRecoEnergy[ip][it] =
          new TH1D(Form("%sRecoEnergy_%s%d", fParticle[ip].Data(), tag.Data(), it),
                   Form("%s %d; E [GeV]", tag.Data(), it), eneBin, eneMin, ip == 0 ? 2 * eneMax : eneMax);
      fTrueEnergy[ip][it] = new TH1D(Form("%sTrueEnergy_%s%d", fParticle[ip].Data(), tag.Data(), it),
                                     Form("%s %d; E [GeV]", tag.Data(), it), eneBin, eneMin, eneMax);
      fEneRes[ip][it] = new TH1D(Form("%sEneRes_%s%d", fParticle[ip].Data(), tag.Data(), it),
                                 Form("%s %d; E_{Reco}/E_{True} - 1 [%]", tag.Data(), it), ereBin,
                                 ip == 0 ? 2 * ereMin : ereMin, ip == 0 ? 2 * ereMax : ereMax);
      for (Int_t iv = 0; iv < fPosRes[ip][0].size(); ++iv) {
        TString xoy = iv ? "X" : "Y";
        fPosRes[ip][it][iv] = new TH1D(Form("%sPosRes_%s%d%s", fParticle[ip].Data(), tag.Data(), it, xoy.Data()),
                                       Form("%s %d; %s_{Reco} - %s_{True}", tag.Data(), it, xoy.Data(), xoy.Data()),
                                       preBin, preMin, preMax);
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeMultihitHisto() {
  TString tag = fUseRapidity ? "Region" : "Tower";

  Int_t migBin = 6;
  Double_t migMin = 1;
  Double_t migMax = (Double_t)(migBin);

  if (fUseRapidity) {
    Utils::AllocateVector2D(fMHmigration, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fEfficiency, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fPurity, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fF1, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fNotEfficiency, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fNotPurity, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fEfficiency1D, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fPurity1D, this->kNeutronNrap, migrationEne.size());
    Utils::AllocateVector2D(fF11D, this->kNeutronNrap, migrationEne.size());
  } else {
    Utils::AllocateVector2D(fMHmigration, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fEfficiency, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fPurity, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fF1, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fNotEfficiency, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fNotPurity, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fEfficiency1D, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fPurity1D, this->kCalNtower, migrationEne.size());
    Utils::AllocateVector2D(fF11D, this->kCalNtower, migrationEne.size());
  }
  for (Int_t it = 0; it < fMHmigration.size(); ++it) {
    for (Int_t ie = 0; ie < fMHmigration[it].size() - 1; ++ie) {
      fMHmigration[it][ie] =
          new TH2D(Form("MHMigrationMap_%s%d_%.0fTo%.0fGeV", tag.Data(), it, migrationEne[ie], migrationEne[ie + 1]),
                   Form("%s%d [%.0f, %.0f] GeV; RECO; TRUE", tag.Data(), it, migrationEne[ie], migrationEne[ie + 1]),
                   migBin, migMin, migMax, migBin, migMin, migMax);
    }
    Int_t il = migrationEne.size() - 1;
    fMHmigration[it][il] = new TH2D(Form("MHMigrationMap_%s%d_%.0fTo%.0fGeV", tag.Data(), it, migrationEne[0],
                                         migrationEne[migrationEne.size() - 1]),
                                    Form("%s%d [%.0f, %.0f] GeV; RECO; TRUE", tag.Data(), it, migrationEne[0],
                                         migrationEne[migrationEne.size() - 1]),
                                    migBin, migMin, migMax, migBin, migMin, migMax);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeSWTrgEffHisto() {
  Int_t AreaSize = fUseRapidity ? (fHadEle ? this->kNeutronNrap : this->kPhotonNrap) : this->kCalNtower;
  TString AreaName = fUseRapidity ? "Region" : "Tower";

  Utils::AllocateVector1D(fSWTriggerAll, AreaSize);
  Utils::AllocateVector1D(fSWTriggerYes, AreaSize);

  for (Int_t ir = 0; ir < fSWTriggerAll.size(); ++ir) {
    fSWTriggerAll[ir] =
        new TH1D(Form("LHCSoftwareTriggerAll_%s%d", AreaName.Data(), ir), Form("%s %d; E [GeV]", AreaName.Data(), ir),
                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
    fSWTriggerYes[ir] =
        new TH1D(Form("LHCSoftwareTriggerYes_%s%d", AreaName.Data(), ir), Form("%s %d; E [GeV]", AreaName.Data(), ir),
                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakePosDetHisto() {
  Utils::AllocateVector4D(fRawPosDet, this->kPosNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);

  for (Int_t it = 0; it < this->kPosNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t is = 0; is < this->kPosNsample; ++is) {
          fRawPosDet[it][il][iv][is] =
              new TH1D(Form("RawPosDet_%d_%d_%d_%d", it, il, iv, is),
                       Form("%s %d - Layer %d%s; Channel; <dE> [ADC]", (this->kArmIndex == 0 ? "Tower" : "Sample"),
                            (this->kArmIndex == 0 ? it : is), il, (iv == 0 ? "x" : "y")),
                       this->kPosNchannel[it], 0, this->kPosNchannel[it]);
        }
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakePosDetMaximum() {
  Utils::AllocateVector4D(fRawPosDetMAX, this->kPosNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);

  for (Int_t it = 0; it < this->kPosNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t is = 0; is < this->kPosNsample; ++is) {
          fRawPosDetMAX[it][il][iv][is] = new TH1D(Form("RawPosDetMAX_%d_%d_%d_%d", it, il, iv, is),
                                                   Form("%s %d - Layer %d%s; <dE> [ADC] max (per channel); Counts",
                                                        (this->kArmIndex == 0 ? "Tower" : "Sample"),
                                                        (this->kArmIndex == 0 ? it : is), il, (iv == 0 ? "x" : "y")),
                                                   4000, 0, 10000);
        }
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeSiliconDepositHisto() {
  Utils::AllocateVector3D(fSilDep, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector2D(fSilDepXYCorr, this->kCalNtower, this->kPosNlayer);
  Utils::AllocateVector1D(fSilDepTot, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepTotXYMean, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepTotYviews, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepTot01layers, this->kCalNtower);

  Utils::AllocateVector1D(fSilDepTot2, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepTotXYMean2, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepTotYviews2, this->kCalNtower);

  // Utils::AllocateVector1D(fSilDepTot_onebin, this->kCalNtower);
  // Utils::AllocateVector1D(fSilDepTotXYMean_onebin, this->kCalNtower);
  // Utils::AllocateVector1D(fSilDepTotYviews_onebin, this->kCalNtower);
  // Utils::AllocateVector1D(fSilDepTot01layers_onebin, this->kCalNtower);

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fSilDep[it][il][iv] = new TH1D(
            Form("SilDep_%d_%d_%d", it, il, iv),
            Form("%s %d - Layer %d%s; Sum of energy deposit [GeV]; Counts", "Tower", it, il, (iv == 0 ? "x" : "y")),
            5000, -0.2, 60);
      }
      fSilDepXYCorr[it][il] = new TH2D(
          Form("SilDepXYCorr_%d_%d", it, il),
          Form("%s %d - Layer %d; Sum of energy deposit x view [GeV]; Sum of energy deposit y view [GeV]; Counts",
               "Tower", it, il),
          1000, -0.2, 30, 1000, -0.2, 30);
    }
    fSilDepTot[it] = new TH1D(Form("SilDepTot_%d", it), Form("%s %d; Sum of energy deposit [GeV]; Counts", "Tower", it),
                              5000, -0.2, 60);
    fSilDepTotXYMean[it] =
        new TH1D(Form("SilDepTotXYMean_%d", it),
                 Form("%s %d; Sum of energy deposit (xy-view mean)[GeV]; Counts", "Tower", it), 5000, -0.2, 60);
    fSilDepTotYviews[it] =
        new TH1D(Form("SilDepTotYviews_%d", it),
                 Form("%s %d; Sum of energy deposit (on y views) [GeV]; Counts", "Tower", it), 5000, -0.2, 60);
    fSilDepTot01layers[it] =
        new TH1D(Form("SilDepTot01layers_%d", it),
                 Form("%s %d; Sum of energy deposit (on layers 0 and 1) [GeV]; Counts", "Tower", it), 5000, -0.2, 60);
    fSilDepTot2[it] = new TH1D(Form("SilDepTotWith2X_%d", it),
                               Form("%s %d; Sum of energy deposit [GeV]; Counts", "Tower", it), 5000, -0.2, 60);
    fSilDepTotXYMean2[it] =
        new TH1D(Form("SilDepTotXYMeanWith2X_%d", it),
                 Form("%s %d; Sum of energy deposit (xy-view mean)[GeV]; Counts", "Tower", it), 5000, -0.2, 60);
    fSilDepTotYviews2[it] =
        new TH1D(Form("SilDepTotYviewsWith2X_%d", it),
                 Form("%s %d; Sum of energy deposit (on y views and 2x) [GeV]; Counts", "Tower", it), 5000, -0.2, 60);

    // fSilDepTot_onebin[it] = new TH1D(Form("SilDepTot_onebin_%d", it), Form("%s %d; Sum of energy deposit [GeV];
    // Counts", "Tower", it),
    //                         1, -0.2, 100000);
    // fSilDepTotXYMean_onebin[it] = new TH1D(Form("SilDepTotXYMean_onebin_%d", it), Form("%s %d; Sum of energy deposit
    // (xy-view mean)[GeV]; Counts", "Tower", it),
    //                         1, -0.2, 100000);
    // fSilDepTotYviews_onebin[it] = new TH1D(Form("SilDepTotYviews_onebin_%d", it), Form("%s %d; Sum of energy deposit
    // (on y views) [GeV]; Counts", "Tower", it),
    //                         1, -0.2, 100000);
    // fSilDepTot01layers_onebin[it] = new TH1D(Form("SilDepTot01layers_onebin_%d", it), Form("%s %d; Sum of energy
    // deposit (on layers 0 and 1) [GeV]; Counts", "Tower", it),
    //                       1, -0.2, 100000);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakePhotonSiliconAreaHisto() {
  Utils::AllocateVector4D(fPhoSilArea, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector2D(fPhoSilAreaTot, this->kCalNtower, this->kMaxNparticle);

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          fPhoSilArea[it][ip][il][iv] =
              new TH1D(Form("PhoSilArea_%d_%d_%d_%d", it, ip, il, iv),
                       Form("%s %d Particle %d - Layer %d%s; Area under fit of particle peak [GeV]; Counts", "Tower",
                            it, ip, il, (iv == 0 ? "x" : "y")),
                       1000, -0.2, 60);
        }
      }
      fPhoSilAreaTot[it][ip] = new TH1D(
          Form("PhoSilAreaTot_%d_%d", it, ip),
          Form("%s %d Particle %d; Area under fit of particle peak [GeV]; Counts", "Tower", it, ip), 1000, -0.2, 60);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeCorrelationSilEnHisto() {
  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t eneBin = 1400;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;

  Utils::AllocateVector4D(fCorrelSilEn, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector2D(fCorrelSilEnTot, this->kCalNtower, this->kMaxNparticle);
  Utils::AllocateVector2D(fCorrelRecoEnPhoSilArea, this->kCalNtower, this->kMaxNparticle);
  Utils::AllocateVector1D(fCorrelRecoEnSilDep, this->kCalNtower);
  Utils::AllocateVector2D(fCorrelRecoYPhoSilArea, this->kCalNtower, this->kMaxNparticle);
  Utils::AllocateVector2D(fCorrelRecoXPhoSilArea, this->kCalNtower, this->kMaxNparticle);
  Utils::AllocateVector1D(fCorrelRecoYSilDep, this->kCalNtower);
  Utils::AllocateVector1D(fCorrelRecoXSilDep, this->kCalNtower);
  Utils::AllocateVector1D(fCorrelSil1YCalo4, this->kCalNtower);
  Utils::AllocateVector1D(fCorrelSil1YCalo5, this->kCalNtower);
  Utils::AllocateVector2D(fCaloDep, this->kCalNtower, this->kCalNlayer);

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          fCorrelSilEn[it][ip][il][iv] = new TH2D(Form("CorrelSilEn_%d_%d_%d_%d", it, ip, il, iv),
                                                  Form("%s %d Particle %d - Layer %d%s; Sum of energy deposit [GeV]; "
                                                       "Area under fit of particle peak [GeV]; Counts",
                                                       "Tower", it, ip, il, (iv == 0 ? "x" : "y")),
                                                  5000, -0.2, 60, 1000, -0.2, 60);
        }
      }
      fCorrelSilEnTot[it][ip] =
          new TH2D(Form("CorrelSilEnTot_%d_%d", it, ip),
                   Form("%s %d Particle %d; Sum of energy deposit [GeV]; Area under fit of particle peak [GeV]; Counts",
                        "Tower", it, ip),
                   5000, -0.2, 60, 5000, -0.2, 60);
      fCorrelRecoEnPhoSilArea[it][ip] =
          new TH2D(Form("CorrelRecoEnPhoSilArea_%d_%d", it, ip),
                   Form("%s %d Particle %d; Reconstructed Energy [GeV]; Area under fit of particle peak [GeV]; Counts",
                        "Tower", it, ip),
                   eneBin, eneMin, eneMax, 1000, -0.2, 60);
      fCorrelRecoYPhoSilArea[it][ip] =
          new TH2D(Form("CorrelRecoYPhoSilArea_%d_%d", it, ip),
                   Form("%s %d Particle %d; Reconstructed Y [mm]; Area under fit of particle peak [GeV]; Counts",
                        "Tower", it, ip),
                   towBin[it], towMin[it], towMax[it], 1000, -0.2, 60);
      fCorrelRecoXPhoSilArea[it][ip] =
          new TH2D(Form("CorrelRecoXPhoSilArea_%d_%d", it, ip),
                   Form("%s %d Particle %d; Reconstructed X [mm]; Area under fit of particle peak [GeV]; Counts",
                        "Tower", it, ip),
                   towBin[it], towMin[it], towMax[it], 1000, -0.2, 60);
    }
    fCorrelRecoEnSilDep[it] =
        new TH2D(Form("CorrelRecoEnSilDep_%d", it),
                 Form("%s %d; Reconstructed Energy [GeV]; Sum of energy deposit [GeV]; Counts", "Tower", it), eneBin,
                 eneMin, eneMax, 1000, -0.2, 60);
    fCorrelRecoYSilDep[it] =
        new TH2D(Form("CorrelRecoYSilDep_%d", it),
                 Form("%s %d; Reconstructed Y [mm]; Sum of energy deposit [GeV]; Counts", "Tower", it), towBin[it],
                 towMin[it], towMax[it], 1000, -0.2, 60);
    fCorrelRecoXSilDep[it] =
        new TH2D(Form("CorrelRecoXSilDep_%d", it),
                 Form("%s %d; Reconstructed X [mm]; Sum of energy deposit [GeV]; Counts", "Tower", it), towBin[it],
                 towMin[it], towMax[it], 1000, -0.2, 60);
    fCorrelSil1YCalo4[it] = new TH2D(
        Form("CorrelSil1YCalo4_%d", it),
        Form("%s %d; Sum of energy deposit on silicon 1Y [GeV]; Energy deposit in Calorimeter layer 4 [GeV]; Counts",
             "Tower", it),
        1000, -0.2, 60, 1000, -0.2, 60);
    fCorrelSil1YCalo5[it] = new TH2D(
        Form("CorrelSil1YCalo5_%d", it),
        Form("%s %d; Sum of energy deposit on silicon 1Y [GeV]; Energy deposit in Calorimeter layer 5 [GeV]; Counts",
             "Tower", it),
        1000, -0.2, 60, 1000, -0.2, 60);
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      fCaloDep[it][il] =
          new TH1D(Form("CaloDep_%d_%d", it, il),
                   Form("%s %d - Layer %d; Energy deposit [GeV]; Counts", "Tower", it, il), 1000, -0.2, 60);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeSilDepMapHisto() {
  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  // Int_t towBin[] = {(Int_t)(this->kTowerSize[0]), (Int_t)(this->kTowerSize[1])};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Utils::AllocateVector1D(fSilDepMap, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepMapCounts, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepMapSum, this->kCalNtower);

  Utils::AllocateVector3D(fSilDepMapLayer, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fSilDepMapLayerCounts, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fSilDepMapLayerSum, this->kCalNtower, this->kPosNlayer, this->kPosNview);

  Utils::AllocateVector1D(fSilDepMapLeakIn, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepMapLeakInCounts, this->kCalNtower);
  Utils::AllocateVector1D(fSilDepMapLeakInSum, this->kCalNtower);

  Utils::AllocateVector3D(fSilDepMapLeakInLayer, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fSilDepMapLeakInLayerCounts, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fSilDepMapLeakInLayerSum, this->kCalNtower, this->kPosNlayer, this->kPosNview);

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    fSilDepMap[it] =
        new TH2D(Form("SilDepMap_%d", it), Form("%s %d; X [mm]; Y [mm]; Mean Energy Deposit [GeV]", "Tower", it),
                 towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    fSilDepMapCounts[it] = new TH2D(Form("SilDepMapCounts_%d", it), Form("%s %d; X [mm]; Y [mm]; Counts", "Tower", it),
                                    towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    fSilDepMapLeakIn[it] = new TH2D(Form("SilDepMapLeakIn_%d", it),
                                    Form("Leak In from %s %d; X [mm]; Y [mm]; Mean Energy Deposit [GeV]", "Tower", it),
                                    towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    fSilDepMapLeakInCounts[it] =
        new TH2D(Form("SilDepMapLeakInCounts_%d", it), Form("Leak In from %s %d; X [mm]; Y [mm]; Counts", "Tower", it),
                 towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);

    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fSilDepMapLayer[it][il][iv] = new TH2D(Form("SilDepMap_%d_%d_%d", it, il, iv),
                                               Form("%s %d - Layer %d%s; X [mm]; Y [mm]; Mean Energy Deposit [GeV]",
                                                    "Tower", it, il, (iv == 0) ? "x" : "y"),
                                               towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
        fSilDepMapLayerCounts[it][il][iv] =
            new TH2D(Form("SilDepMapCounts_%d_%d_%d", it, il, iv),
                     Form("%s %d - Layer %d%s; X [mm]; Y [mm]; Counts", "Tower", it, il, (iv == 0) ? "x" : "y"),
                     towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
        fSilDepMapLeakInLayer[it][il][iv] =
            new TH2D(Form("SilDepMapLeakIn_%d_%d_%d", it, il, iv),
                     Form("Leak In from %s %d - Layer %d%s; X [mm]; Y [mm]; Mean Energy Deposit [GeV]", "Tower", it, il,
                          (iv == 0) ? "x" : "y"),
                     towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
        fSilDepMapLeakInLayerCounts[it][il][iv] = new TH2D(
            Form("SilDepMapLeakInCounts_%d_%d_%d", it, il, iv),
            Form("Leak In from %s %d - Layer %d%s; X [mm]; Y [mm]; Counts", "Tower", it, il, (iv == 0) ? "x" : "y"),
            towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeRecoPosEnHisto() {
  // TString tag = fUseRapidity ? "Region" : "Tower";

  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t eneBin = 1400;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    eneBin = 100.;
    eneMin = 0.;
    eneMax = 500.;
  }

  // Int_t AreaSize = fUseRapidity ? (fHadEle ? this->kNeutronNrap : this->kPhotonNrap) : this->kCalNtower;
  // TString AreaName = fUseRapidity ? "Region" : "Tower";
  Int_t AreaSize = this->kCalNtower;
  TString AreaName = "Tower";

  Utils::AllocateVector1D(fSilRecoEnergy, AreaSize);
  Utils::AllocateVector1D(fSilRecoTower, AreaSize);

  for (Int_t ir = 0; ir < AreaSize; ++ir) {
    Int_t etaBin = fUseRapidity ? (ir < 3 ? towBin[0] : towBin[1]) : towBin[ir];
    Int_t etaMin = fUseRapidity ? (ir < 3 ? towMin[0] : towMin[1]) : towMin[ir];
    Int_t etaMax = fUseRapidity ? (ir < 3 ? towMax[0] : towMax[1]) : towMax[ir];
    fSilRecoTower[ir] =
        new TH2D(Form("RecoTower_%s%d", AreaName.Data(), ir), Form("%s %d; x [mm]; y [mm];", AreaName.Data(), ir),
                 etaBin, etaMin, etaMax, etaBin, etaMin, etaMax);

    fSilRecoEnergy[ir] = new TH1D(Form("RecoEnergy_%s%d", AreaName.Data(), ir),
                                  Form("%s %d; E [GeV]", AreaName.Data(), ir), eneBin, eneMin, eneMax);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakePhotonSiliconEnergyHisto() {
  Utils::AllocateVector1D(fPhotonSilEn, this->kCalNtower);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    fPhotonSilEn[it] =
        new TH1D(Form("PhotonSiliconEnergy_%d", it),
                 Form("%s %d; Reconstructed Photon Energy [GeV]; Counts", "Tower", it), 7000, -100, 7000);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakePhotonSilEneMapHisto() {
  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Utils::AllocateVector1D(fPhotonSilEnergyMap, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonSilCountsMap, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonSilAverageMap, this->kCalNtower);

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    fPhotonSilEnergyMap[it] = new TH2D(Form("PhotonSiliconEnergyMap_%d", it),
                                       Form("%s %d; X [mm]; Y [mm]; Mean Energy Deposit [GeV]", "Tower", it),
                                       towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    fPhotonSilCountsMap[it] =
        new TH2D(Form("PhotonSiliconCountsMap_%d", it), Form("%s %d; X [mm]; Y [mm]; Counts", "Tower", it), towBin[it],
                 towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeAlignmentHisto() {
  Utils::AllocateVector3D(fFitAmplitude, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fRedChiSquare, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector4D(fRelAlignment, this->kCalNtower, this->kPosNview, this->kPosNlayer, this->kPosNlayer);
  Utils::AllocateVector4D(fRelAlignmentMaskPACE, this->kCalNtower, this->kPosNview, this->kPosNlayer, this->kPosNlayer);

  Utils::AllocateVector3D(fRedChiVsFitAmp, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fRecPosVsRelAli, this->kPosNview, this->kPosNlayer, this->kPosNlayer);
  Utils::AllocateVector3D(fRecPosVsRelAliMaskPACE, this->kPosNview, this->kPosNlayer, this->kPosNlayer);

  Int_t ampBin = 1000;
  Double_t ampMin = 0.;
  Double_t ampMax = 100000.;

  Int_t chiBin = 1000;
  Double_t chiMin = 0.;
  Double_t chiMax = 100.;

  Int_t aliBin = 1000;
  Double_t aliMin = -10.;
  Double_t aliMax = +10.;

  Int_t mapBin = this->kArmIndex == 0 ? this->kPosNchannel[0] + this->kPosNchannel[1] : this->kPosNchannel[0];
  Double_t mapMin = 0.;
  Double_t mapMax = (Double_t)mapBin;

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fFitAmplitude[it][il][iv] =
            new TH1D(Form("LorentzAmplitude_%d_%d%s", it, il, iv == 0 ? "x" : "y"),
                     Form("Layer %d%s; Area [ADC]", il, iv == 0 ? "x" : "y"), ampBin, ampMin, ampMax);
        fRedChiSquare[it][il][iv] =
            new TH1D(Form("ReducedChiSquare_%d_%d%s", it, il, iv == 0 ? "x" : "y"),
                     Form("Layer %d%s; #chi^2/ndof", il, iv == 0 ? "x" : "y"), chiBin, chiMin, chiMax);

        fRedChiVsFitAmp[it][il][iv] = new TH2D(Form("RedChiVsFitAmp_%d_%d%s", it, il, iv == 0 ? "x" : "y"),
                                               Form("Layer %d%s; #chi^2/ndof; Area [ADC]", il, iv == 0 ? "x" : "y"),
                                               chiBin, chiMin, chiMax, ampBin, ampMin, ampMax);
      }

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t jl = 0; jl < this->kPosNlayer; ++jl) {
          fRelAlignment[it][iv][il][jl] = new TH1D(
              Form("RelativeAlignment%s_%d_%dvs%d", iv == 0 ? "x" : "y", it, il, jl),
              Form("%s : Layer %d - Layer %d; #Delta%s [strip]", iv == 0 ? "x" : "y", il, jl, iv == 0 ? "x" : "y"),
              aliBin, aliMin, aliMax);
          fRelAlignmentMaskPACE[it][iv][il][jl] = new TH1D(
              Form("RelativeAlignmentMaskPACE%s_%d_%dvs%d", iv == 0 ? "x" : "y", it, il, jl),
              Form("%s : Layer %d - Layer %d; #Delta%s [strip]", iv == 0 ? "x" : "y", il, jl, iv == 0 ? "x" : "y"),
              aliBin, aliMin, aliMax);
        }
  for (Int_t iv = 0; iv < this->kPosNview; ++iv)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t jl = 0; jl < this->kPosNlayer; ++jl) {
        fRecPosVsRelAli[iv][il][jl] = new TH2D(
            Form("RecPosVsRelAli%s_%dvs%d", iv == 0 ? "x" : "y", il, jl),
            Form("%s : Layer %d - Layer %d; #Delta%s [strip]", iv == 0 ? "x" : "y", il, jl, iv == 0 ? "x" : "y"),
            mapBin, mapMin, mapMax, aliBin, aliMin, aliMax);
        fRecPosVsRelAliMaskPACE[iv][il][jl] = new TH2D(
            Form("RecPosVsRelAliMaskPACE%s_%dvs%d", iv == 0 ? "x" : "y", il, jl),
            Form("%s : Layer %d - Layer %d; #Delta%s [strip]", iv == 0 ? "x" : "y", il, jl, iv == 0 ? "x" : "y"),
            mapBin, mapMin, mapMax, aliBin, aliMin, aliMax);
      }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeHitmapHisto() {
  Int_t mapBin[] = {this->kArmIndex == 0 ? 384 : 384, this->kArmIndex == 0 ? 384 : 384};
  Double_t mapMin[] = {this->kArmIndex == 0 ? -18.56 : -18.56, this->kArmIndex == 0 ? -13.88 : -13.88};
  Double_t mapMax[] = {this->kArmIndex == 0 ? 42.88 : 42.88, this->kArmIndex == 0 ? 47.56 : 47.56};

  Int_t zedBin = 6;
  Double_t zedMin = -1;
  Double_t zedMax = +5;

  Int_t nRun = (fFirstRun >= 0 && fLastRun >= 0) ? fLastRun - fFirstRun + 1 : 1;
  Int_t nEne = hitmapEne.size();

  Utils::AllocateVector3D(fVertexZaverage, nRun, this->kPosNview, nEne);
  for (Int_t ir = 0; ir < nRun; ++ir)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t ie = 0; ie < nEne; ++ie) {
        Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
        fVertexZaverage[ir][iv][ie] = new TH1D(
            Form("HitmapZaverage_%d%s_%.0fGeV", thisRun, iv == 0 ? "x" : "y", hitmapEne[ie]),
            Form("Run %d - E>%.0fGeV; %s Layer", thisRun, hitmapEne[ie], iv == 0 ? "x" : "y"), zedBin, zedMin, zedMax);
      }

  Utils::AllocateVector4D(fVertexHitmap1D, nRun, this->kPosNlayer, this->kPosNview, nEne);
  Utils::AllocateVector4D(fVertexHitmap2D, nRun, this->kPosNlayer, this->kPosNlayer, nEne);
  Utils::AllocateVector4D(fVertexPDChan2D, nRun, this->kPosNlayer, this->kPosNlayer, nEne);
  for (Int_t ir = 0; ir < nRun; ++ir)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ie = 0; ie < nEne; ++ie) {
          Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
          fVertexHitmap1D[ir][il][iv][ie] =
              new TH1D(Form("HitmapVertex1D_%d_%d%s_%.0fGeV", thisRun, il, iv == 0 ? "x" : "y", hitmapEne[ie]),
                       Form("Run %d - E>%.0fGeV; %d%s [mm]", thisRun, hitmapEne[ie], il, iv == 0 ? "x" : "y"),
                       mapBin[iv], mapMin[iv], mapMax[iv]);
        }
  for (Int_t ir = 0; ir < nRun; ++ir)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t jl = 0; jl < this->kPosNlayer; ++jl)
        for (Int_t ie = 0; ie < nEne; ++ie) {
          Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
          fVertexHitmap2D[ir][il][jl][ie] =
              new TH2D(Form("HitmapVertex2D_%d_%dx_%dy_%.0fGeV", thisRun, il, jl, hitmapEne[ie]),
                       Form("Run %d - E>%.0fGeV; %dx [mm]; %dy [mm]", thisRun, hitmapEne[ie], il, jl), mapBin[0],
                       mapMin[0], mapMax[0], mapBin[1], mapMin[1], mapMax[1]);
        }
  for (Int_t ir = 0; ir < nRun; ++ir)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t jl = 0; jl < this->kPosNlayer; ++jl)
        for (Int_t ie = 0; ie < nEne; ++ie) {
          Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
          if (this->kArmIndex == 0)
            fVertexPDChan2D[ir][il][jl][ie] = new TH2D(
                Form("HitmapPDChan2D_%d_%dx_%dy_%.0fGeV", thisRun, il, jl, hitmapEne[ie]),
                Form("Run %d - E>%.0fGeV; %dx [strip]; %dy [strip]", thisRun, hitmapEne[ie], il, jl),
                this->kPosNchannel[0] + this->kPosNchannel[1], -0.5,
                this->kPosNchannel[0] + this->kPosNchannel[1] - 0.5, this->kPosNchannel[0] + this->kPosNchannel[1],
                -0.5, this->kPosNchannel[0] + this->kPosNchannel[1] - 0.5);
          else
            fVertexPDChan2D[ir][il][jl][ie] =
                new TH2D(Form("HitmapPDChan2D_%d_%dx_%dy_%.0fGeV", thisRun, il, jl, hitmapEne[ie]),
                         Form("Run %d - E>%.0fGeV; %dx [strip]; %dy [strip]", thisRun, hitmapEne[ie], il, jl),
                         this->kPosNchannel[0], -0.5, this->kPosNchannel[0] - 0.5, this->kPosNchannel[0], -0.5,
                         this->kPosNchannel[0] - 0.5);
        }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeMassHisto() {
  fEveCut.SetPhotonCuts("BPTX L2T DISCRIMINATOR");

  Int_t invBin = 1200.;
  Double_t invMin = 0.;
  Double_t invMax = 600.;

  Int_t nRun = (fFirstRun >= 0 && fLastRun >= 0) ? fLastRun - fFirstRun + 1 : 1;

  Utils::AllocateVector1D(fInvMassTot, fNType);
  Utils::AllocateVector2D(fInvMassRun, fNType, nRun);
  Utils::AllocateVector2D(fInvMassTotXf, fNType, fNxFBin);
  Utils::AllocateVector3D(fInvMassRunXf, fNType, fNxFBin, nRun);

  for (Int_t ip = 0; ip < fNType; ++ip) {
    TString thisPion = fLabelType[ip];
    for (Int_t ix = 0; ix < fNxFBin; ++ix) {
      Double_t minxF = ((Double_t)(ix)) / fNxFBin;
      Double_t maxxF = ((Double_t)(ix + 1.0)) / fNxFBin;
      Double_t thisxF = ((Double_t)(ix + 0.5)) / fNxFBin;

      for (Int_t ir = 0; ir < nRun; ++ir) {
        Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
        //
        TString thisName = Form("%s_%d_xF%.3f", thisPion.Data(), thisRun, thisxF);
        TString thisTitle = Form("Run %d - %0.3f < x_{F} < %0.3f - %s; M_{#gamma#gamma} [MeV/c^{2}]", thisRun, minxF,
                                 maxxF, thisPion.Data());
        fInvMassRunXf[ip][ix][ir] = new TH1D(thisName.Data(), thisTitle.Data(), invBin, invMin, invMax);
      }
      TString thisName = Form("%s_Cumulative_xF%.3f", thisPion.Data(), thisxF);
      TString thisTitle =
          Form("Cumulative - %0.3f < x_{F} < %0.3f - %s; M_{#gamma#gamma} [MeV/c^{2}]", minxF, maxxF, thisPion.Data());
      fInvMassTotXf[ip][ix] = new TH1D(thisName.Data(), thisTitle.Data(), invBin, invMin, invMax);
    }
    for (Int_t ir = 0; ir < nRun; ++ir) {
      Int_t thisRun = (fFirstRun >= 0 && fLastRun >= 0) ? fFirstRun + ir : 0;
      //
      TString thisName = Form("%s_%d", thisPion.Data(), thisRun);
      TString thisTitle = Form("Run %d - %s; M_{#gamma#gamma} [MeV/c^{2}]", thisRun, thisPion.Data());
      fInvMassRun[ip][ir] = new TH1D(thisName.Data(), thisTitle.Data(), invBin, invMin, invMax);
    }
    TString thisName = Form("%s_Cumulative", thisPion.Data());
    TString thisTitle = Form("Total - %s; M_{#gamma#gamma} [MeV/c^{2}]", thisPion.Data());
    fInvMassTot[ip] = new TH1D(thisName.Data(), thisTitle.Data(), invBin, invMin, invMax);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeDiscriminHisto() {
  Int_t depBin = 2500;
  Double_t depMin = 0.;
  Double_t depMax = 10.;

  Utils::AllocateVector2D(fDiscrimYes, this->kCalNtower, this->kCalNlayer);
  Utils::AllocateVector2D(fDiscrimAll, this->kCalNtower, this->kCalNlayer);

  Utils::AllocateVector2D(fDiscrimEff, this->kCalNtower, this->kCalNlayer);

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      fDiscrimYes[it][il] =
          new TH1D(Form("DiscriminatorYes_Tower%d_Layer%d", it, il),
                   Form("%s Layer %d; dE [GeV]", (it == 0 ? "TS" : "TL"), il), depBin, depMin, depMax);
      fDiscrimAll[it][il] =
          new TH1D(Form("DiscriminatorAll_Tower%d_Layer%d", it, il),
                   Form("%s Layer %d; dE [GeV]", (it == 0 ? "TS" : "TL"), il), depBin, depMin, depMax);
    }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakePileUpHisto() {
  Int_t nBin = nPILEUP;
  Double_t nMin = -0.5;
  Double_t nMax = nMin + nPILEUP;

  Int_t lenBin = 44;
  Double_t lenMin = 0;
  Double_t lenMax = 44;

  Int_t eneBin = 56;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;

  Int_t nParticle = 2;

  Utils::AllocateVector1D(fPileUpL90, this->kCalNtower);
  Utils::AllocateVector1D(fPileUpEnergy, this->kCalNtower);

  Utils::AllocateVector2D(fPileUpL90Particle, this->kCalNtower, nParticle);
  Utils::AllocateVector2D(fPileUpEnergyParticle, this->kCalNtower, nParticle);

  fPileUpCount = new TH1D("PileUpCount", ";;Counts", nBin, nMin, nMax);

  for (Int_t it = 0; it < fPileUpL90.size(); ++it) {
    fPileUpL90[it] =
        new TH1D(Form("PileUpL90_Tower%d", it), Form("Pile Up on Tower %d; L_{90} [X_0]", it), lenBin, lenMin, lenMax);
    fPileUpEnergy[it] =
        new TH1D(Form("PileUpEnergy_Tower%d", it), Form("Pile Up on Tower %d; Equivalent Photon Energy [GeV]", it),
                 eneBin, eneMin, eneMax);
    for (Int_t ip = 0; ip < fPileUpL90Particle[it].size(); ++ip) {
      TString particle = ip == 0 ? "Photon" : "Neutron";
      fPileUpL90Particle[it][ip] =
          new TH1D(Form("PileUpL90%s_Tower%d", particle.Data(), it),
                   Form("%s Pile Up on Tower %d; L_{90} [X_0]", particle.Data(), it), lenBin, lenMin, lenMax);
      fPileUpEnergyParticle[it][ip] = new TH1D(
          Form("PileUpEnergy%s_Tower%d", particle.Data(), it),
          Form("%s Pile Up on Tower %d; Equivalent Photon Energy [GeV]", particle.Data(), it), eneBin, eneMin, eneMax);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeSPSHisto() {
  // TString tag = fUseRapidity ? "Region" : "Tower";

  // TString units = "ADC"; //lvl1
  TString units = "GeV";  // lvl2

  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t mapBin[] = {this->kArmIndex == 0 ? 384 : 384, this->kArmIndex == 0 ? 384 : 384};
  Double_t mapMin[] = {this->kArmIndex == 0 ? -18.56 : -18.56, this->kArmIndex == 0 ? -13.88 : -13.88};
  Double_t mapMax[] = {this->kArmIndex == 0 ? 42.88 : 42.88, this->kArmIndex == 0 ? 47.56 : 47.56};

  Int_t aveBin = this->kCalNlayer;
  Double_t aveMin = 0;
  Double_t aveMax = this->kCalNlayer;

  Int_t lenBin = 44;
  Double_t lenMin = 0;
  Double_t lenMax = 44;

  Int_t depBin = 500;
  Double_t depMin = 0.;
  // Double_t depMax = 1500.; //lvl1
  Double_t depMax = 10.;  // lvl2
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    depBin = 500;
    depMin = 0.;
    depMax = 2.5;
  }

  Int_t eneBin = 14;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    eneBin = 100.;
    eneMin = 0.;
    eneMax = 500.;
  }

  Int_t ereBin = 400;
  Double_t ereMin = -50.;
  Double_t ereMax = +50.;

  Int_t preBin = 1000;
  Double_t preMin = -10.;
  Double_t preMax = +10.;

  Utils::AllocateVector2D(fSPSCaldE, this->kCalNtower, this->kCalNlayer);
  Utils::AllocateVector2D(fSPSCaldEmap, this->kCalNtower, this->kCalNlayer);
  Utils::AllocateVector1D(fSPSAvedE, this->kCalNtower);
  Utils::AllocateVector1D(fSPSL20, this->kCalNtower);
  Utils::AllocateVector1D(fSPSL90, this->kCalNtower);
  Utils::AllocateVector1D(fSPSL2D, this->kCalNtower);
  Utils::AllocateVector1D(fSPSRecoEnergy, this->kCalNtower);
  Utils::AllocateVector1D(fSPSTrueEnergy, this->kCalNtower);
  Utils::AllocateVector1D(fSPSEneRes, this->kCalNtower);
  Utils::AllocateVector2D(fSPSPosRes, this->kCalNtower, this->kPosNview);

  Utils::AllocateVector1D(fSPSRecoTower, this->kCalNtower);
  Utils::AllocateVector1D(fSPSTrueTower, this->kCalNtower);

  fSPSRecoHitmap = new TH2D("SPSRecoHitmap", "Reco Hitmap; x [mm]; y [mm]", mapBin[0], mapMin[0], mapMax[0], mapBin[1],
                            mapMin[1], mapMax[1]);
  fSPSTrueHitmap = new TH2D("SPSTrueHitmap", "True Hitmap; x [mm]; y [mm]", mapBin[0], mapMin[0], mapMax[0], mapBin[1],
                            mapMin[1], mapMax[1]);
  for (Int_t it = 0; it < fSPSCaldE.size(); ++it) {
    fSPSRecoTower[it] = new TH2D(Form("SPSRecoTower_%s%d", "Tower", it), Form("%s %d; x [mm]; y [mm];", "Tower", it),
                                 towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    fSPSTrueTower[it] = new TH2D(Form("SPSTrueTower_%s%d", "Tower", it), Form("%s %d; x [mm]; y [mm];", "Tower", it),
                                 towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    for (Int_t il = 0; il < fSPSCaldE[0].size(); ++il) {
      fSPSCaldE[it][il] =
          new TH1D(Form("SPSCaldE_%s%d_Layer%d", "Tower", it, il),
                   Form("%s %d - Layer %d; dE [%s]", "Tower", it, il, units.Data()), depBin, depMin, depMax);
      fSPSCaldEmap[it][il] = new TH2D(Form("SPSCaldEmap_%s%d_Layer%d", "Tower", it, il),
                                      Form("%s %d - Layer %d; x [mm]; y [mm]; dE [%s]", "Tower", it, il, units.Data()),
                                      towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
    }
    fSPSAvedE[it] = new TH1D(Form("SPSAvedE_%s%d", "Tower", it),
                             Form("%s %d; Layer; dE [%s];", "Tower", it, units.Data()), aveBin, aveMin, aveMax);
    fSPSL20[it] =
        new TH1D(Form("SPSL20_%s%d", "Tower", it), Form("%s %d; L_{20} [X_0]", "Tower", it), lenBin, lenMin, lenMax);
    fSPSL90[it] =
        new TH1D(Form("SPSL90_%s%d", "Tower", it), Form("%s %d; L_{90} [X_0]", "Tower", it), lenBin, lenMin, lenMax);
    fSPSL2D[it] =
        new TH1D(Form("SPSL2D_%s%d", "Tower", it), Form("%s %d; L_{2D} [X_0]", "Tower", it), lenBin, lenMin, lenMax);
    fSPSRecoEnergy[it] = new TH1D(Form("SPSRecoEnergy_%s%d", "Tower", it),
                                  Form("%s %d; E [%s]", "Tower", it, units.Data()), depBin, depMin, 3. * depMax);
    fSPSTrueEnergy[it] = new TH1D(Form("SPSTrueEnergy_%s%d", "Tower", it),
                                  Form("%s %d; E [%s]", "Tower", it, units.Data()), eneBin, eneMin, eneMax);
    fSPSEneRes[it] = new TH1D(Form("SPSEneRes_%s%d", "Tower", it),
                              Form("%s %d; E_{Reco}/E_{True} - 1 [%]", "Tower", it), ereBin, ereMin, ereMax);
    for (Int_t iv = 0; iv < fSPSPosRes[0].size(); ++iv) {
      TString xoy = iv ? "X" : "Y";
      fSPSPosRes[it][iv] =
          new TH1D(Form("SPSPosRes_%s%d%s", "Tower", it, xoy.Data()),
                   Form("%s %d; %s_{Reco} - %s_{True}", "Tower", it, xoy.Data(), xoy.Data()), preBin, preMin, preMax);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeLHCHisto() {
  // TString tag = fUseRapidity ? "Region" : "Tower";

  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t mapBin[] = {this->kArmIndex == 0 ? 384 : 384, this->kArmIndex == 0 ? 384 : 384};
  Double_t mapMin[] = {this->kArmIndex == 0 ? -18.56 : -18.56, this->kArmIndex == 0 ? -13.88 : -13.88};
  Double_t mapMax[] = {this->kArmIndex == 0 ? 42.88 : 42.88, this->kArmIndex == 0 ? 47.56 : 47.56};

  Int_t aveBin = this->kCalNlayer;
  Double_t aveMin = 0;
  Double_t aveMax = this->kCalNlayer;

  Int_t lenBin = 44;
  Double_t lenMin = 0;
  Double_t lenMax = 44;

  Int_t depBin = 500;
  Double_t depMin = 0.;
  Double_t depMax = 10.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    depBin = 500;
    depMin = 0.;
    depMax = 2.5;
  }

  Int_t eneBin = 56;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    eneBin = 100.;
    eneMin = 0.;
    eneMax = 500.;
  }

  Int_t ereBin = 400;
  Double_t ereMin = -50.;
  Double_t ereMax = +50.;

  Int_t preBin = 1000;
  Double_t preMin = -10.;
  Double_t preMax = +10.;

  Int_t AreaSize = fUseRapidity ? (fHadEle ? this->kNeutronNrap : this->kPhotonNrap) : this->kCalNtower;
  TString AreaName = fUseRapidity ? "Region" : "Tower";

  Utils::AllocateVector2D(fLHCCaldE, AreaSize, this->kCalNlayer);
  Utils::AllocateVector2D(fLHCCaldEmap, AreaSize, this->kCalNlayer);
  Utils::AllocateVector1D(fLHCAvedE, AreaSize);
  Utils::AllocateVector1D(fLHCL20, AreaSize);
  Utils::AllocateVector1D(fLHCL90, AreaSize);
  Utils::AllocateVector1D(fLHCL2D, AreaSize);
  Utils::AllocateVector1D(fLHCSumdE, AreaSize);
  Utils::AllocateVector1D(fLHCRecoEnergy, AreaSize);
  Utils::AllocateVector1D(fLHCTrueEnergy, AreaSize);
  Utils::AllocateVector1D(fLHCEneRes, AreaSize);
  Utils::AllocateVector2D(fLHCPosRes, AreaSize, this->kPosNview);

  Utils::AllocateVector1D(fLHCRecoTower, AreaSize);
  Utils::AllocateVector1D(fLHCTrueTower, AreaSize);

  fLHCRecoHitmap = new TH2D("LHCRecoHitmap", "Reco Hitmap; x [mm]; y [mm]", mapBin[0], mapMin[0], mapMax[0], mapBin[1],
                            mapMin[1], mapMax[1]);
  fLHCTrueHitmap = new TH2D("LHCTrueHitmap", "True Hitmap; x [mm]; y [mm]", mapBin[0], mapMin[0], mapMax[0], mapBin[1],
                            mapMin[1], mapMax[1]);
  for (Int_t ir = 0; ir < AreaSize; ++ir) {
    Int_t etaBin = fUseRapidity ? (ir < 3 ? towBin[0] : towBin[1]) : towBin[ir];
    Int_t etaMin = fUseRapidity ? (ir < 3 ? towMin[0] : towMin[1]) : towMin[ir];
    Int_t etaMax = fUseRapidity ? (ir < 3 ? towMax[0] : towMax[1]) : towMax[ir];
    fLHCRecoTower[ir] =
        new TH2D(Form("LHCRecoTower_%s%d", AreaName.Data(), ir), Form("%s %d; x [mm]; y [mm];", AreaName.Data(), ir),
                 etaBin, etaMin, etaMax, etaBin, etaMin, etaMax);
    fLHCTrueTower[ir] =
        new TH2D(Form("LHCTrueTower_%s%d", AreaName.Data(), ir), Form("%s %d; x [mm]; y [mm];", AreaName.Data(), ir),
                 etaBin, etaMin, etaMax, etaBin, etaMin, etaMax);
    for (Int_t il = 0; il < fLHCCaldE[0].size(); ++il) {
      fLHCCaldE[ir][il] = new TH1D(Form("LHCCaldE_%s%d_Layer%d", AreaName.Data(), ir, il),
                                   Form("%s %d - Layer %d; dE [GeV]", AreaName.Data(), ir, il), depBin, depMin, depMax);
      fLHCCaldEmap[ir][il] = new TH2D(Form("LHCCaldEmap_%s%d_Layer%d", AreaName.Data(), ir, il),
                                      Form("%s %d - Layer %d; x [mm]; y [mm]; dE [GeV]", AreaName.Data(), ir, il),
                                      etaBin, etaMin, etaMax, etaBin, etaMin, etaMax);
    }
    fLHCAvedE[ir] = new TH1D(Form("LHCAvedE_%s%d", AreaName.Data(), ir),
                             Form("%s %d; Layer; dE [GeV];", AreaName.Data(), ir), aveBin, aveMin, aveMax);
    fLHCL20[ir] = new TH1D(Form("LHCL20_%s%d", AreaName.Data(), ir), Form("%s %d; L_{20} [X_0]", AreaName.Data(), ir),
                           lenBin, lenMin, lenMax);
    fLHCL90[ir] = new TH1D(Form("LHCL90_%s%d", AreaName.Data(), ir), Form("%s %d; L_{90} [X_0]", AreaName.Data(), ir),
                           lenBin, lenMin, lenMax);
    fLHCL2D[ir] = new TH1D(Form("LHCL2D_%s%d", AreaName.Data(), ir), Form("%s %d; L_{2D} [X_0]", AreaName.Data(), ir),
                           lenBin, lenMin, lenMax);
    fLHCSumdE[ir] = new TH1D(Form("LHCRecoSumdE_%s%d", AreaName.Data(), ir),
                             Form("%s %d; sumdE [GeV]", AreaName.Data(), ir), 10 * depBin, 10 * depMin, 10 * depMax);
    fLHCRecoEnergy[ir] = new TH1D(Form("LHCRecoEnergy_%s%d", AreaName.Data(), ir),
                                  Form("%s %d; E [GeV]", AreaName.Data(), ir), eneBin, eneMin, eneMax);
    fLHCTrueEnergy[ir] = new TH1D(Form("LHCTrueEnergy_%s%d", AreaName.Data(), ir),
                                  Form("%s %d; E [GeV]", AreaName.Data(), ir), eneBin, eneMin, eneMax);
    fLHCEneRes[ir] = new TH1D(Form("LHCEneRes_%s%d", AreaName.Data(), ir),
                              Form("%s %d; E_{Reco}/E_{True} - 1 [%]", AreaName.Data(), ir), ereBin, ereMin, ereMax);
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      TString xoy = iv == 0 ? "X" : "Y";
      fLHCPosRes[ir][iv] = new TH1D(Form("LHCPosRes_%s%d%s", AreaName.Data(), ir, xoy.Data()),
                                    Form("%s %d; %s_{Reco} - %s_{True}", AreaName.Data(), ir, xoy.Data(), xoy.Data()),
                                    preBin, preMin, preMax);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeGSOCalibHisto() {
  Int_t nBinEnergy = energyBinning.size() - 1;
  Int_t nBinPID = 2;

  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t mapBin[] = {this->kArmIndex == 0 ? 384 : 384, this->kArmIndex == 0 ? 384 : 384};
  Double_t mapMin[] = {this->kArmIndex == 0 ? -18.56 : -18.56, this->kArmIndex == 0 ? -13.88 : -13.88};
  Double_t mapMax[] = {this->kArmIndex == 0 ? 42.88 : 42.88, this->kArmIndex == 0 ? 47.56 : 47.56};

  Int_t aveBin = this->kCalNlayer;
  Double_t aveMin = 0;
  Double_t aveMax = this->kCalNlayer;

  Int_t depBin = 2500;
  Double_t depMin = 0.;
  Double_t depMax = 50.;

  Utils::AllocateVector2D(fGSOCalibTower, this->kCalNtower, nBinEnergy);
  Utils::AllocateVector3D(fGSOCalibCaldE, this->kCalNtower, this->kCalNlayer, nBinEnergy);
  Utils::AllocateVector4D(fGSOCalibCalMC, this->kCalNtower, this->kCalNlayer, nBinEnergy, nBinPID);
  Utils::AllocateVector2D(fGSOCalibAvedE, this->kCalNtower, nBinEnergy);

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t ib = 0; ib < nBinEnergy; ++ib) {
      fGSOCalibTower[it][ib] = new TH2D(
          Form("GSOCalibMap_Tower%d_Bin%d", it, ib),
          Form("Tower %d - Energy %.0f-%.0f GeV; x [mm]; y [mm];", it, energyBinning[ib], energyBinning[ib + 1]),
          towBin[it], towMin[it], towMax[it], towBin[it], towMin[it], towMax[it]);
      fGSOCalibAvedE[it][ib] = new TProfile(
          Form("GSOCalibProfile_Tower%d_Bin%d", it, ib),
          Form("Tower %d - Energy %.0f-%.0f GeV; Layer; dE [GeV];", it, energyBinning[ib], energyBinning[ib + 1]),
          aveBin, aveMin, aveMax);
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        fGSOCalibCaldE[it][il][ib] = new TH1D(Form("GSOCalibProfile_Tower%dLayer%d_Bin%d", it, il, ib),
                                              Form("Tower %d - Layer %d - Energy %.0f-%.0f GeV; dE [GeV]; Counts;", it,
                                                   il, energyBinning[ib], energyBinning[ib + 1]),
                                              depBin, depMin, depMax);
        for (Int_t ip = 0; ip < nBinPID; ++ip) {
          fGSOCalibCalMC[it][il][ib][ip] =
              new TH1D(Form("GSOCalibProfile_%s_Tower%dLayer%d_Bin%d", ip == 0 ? "Hadron" : "Photon", it, il, ib),
                       Form("Tower %d - Layer %d - Energy %.0f-%.0f GeV; dE [GeV]; Counts;", it, il, energyBinning[ib],
                            energyBinning[ib + 1]),
                       depBin, depMin, depMax);
        }
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MakeSiliconHisto() {
  // TString tag = fUseRapidity ? "Region" : "Tower";

  Int_t towBin[] = {(Int_t)(this->kTowerSize[0] / 0.160), (Int_t)(this->kTowerSize[1] / 0.160)};
  Double_t towMin[] = {0., 0.};
  Double_t towMax[] = {this->kTowerSize[0], this->kTowerSize[1]};

  Int_t mapBin[] = {this->kArmIndex == 0 ? 384 : 384, this->kArmIndex == 0 ? 384 : 384};
  Double_t mapMin[] = {this->kArmIndex == 0 ? -18.56 : -18.56, this->kArmIndex == 0 ? -13.88 : -13.88};
  Double_t mapMax[] = {this->kArmIndex == 0 ? 42.88 : 42.88, this->kArmIndex == 0 ? 47.56 : 47.56};

  Int_t eneBin = 56;
  Double_t eneMin = 0.;
  Double_t eneMax = 7000.;
  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    eneBin = 100.;
    eneMin = 0.;
    eneMax = 500.;
  }

  Int_t ereBin = 400;
  Double_t ereMin = -50.;
  Double_t ereMax = +50.;

  Int_t preBin = 1000;
  Double_t preMin = -10.;
  Double_t preMax = +10.;

  Int_t AreaSize = fUseRapidity ? (fHadEle ? this->kNeutronNrap : this->kPhotonNrap) : this->kCalNtower;
  TString AreaName = fUseRapidity ? "Region" : "Tower";

  Utils::AllocateVector1D(fSiRecoEnergy, AreaSize);
  Utils::AllocateVector1D(fSiRecoSiliconEnergy, AreaSize);
  Utils::AllocateVector1D(fSiRecoEnergyCorrelation, AreaSize);
  Utils::AllocateVector1D(fSiTrueEnergy, AreaSize);
  Utils::AllocateVector1D(fSiEneRes, AreaSize);
  Utils::AllocateVector1D(fSiSiliconEneRes, AreaSize);
  Utils::AllocateVector2D(fSiPosRes, AreaSize, this->kPosNview);

  Utils::AllocateVector1D(fSiRecoTower, AreaSize);
  Utils::AllocateVector1D(fSiTrueTower, AreaSize);

  fSiRecoHitmap = new TH2D("SiRecoHitmap", "Reco Hitmap; x [mm]; y [mm]", mapBin[0], mapMin[0], mapMax[0], mapBin[1],
                           mapMin[1], mapMax[1]);
  fSiTrueHitmap = new TH2D("SiTrueHitmap", "True Hitmap; x [mm]; y [mm]", mapBin[0], mapMin[0], mapMax[0], mapBin[1],
                           mapMin[1], mapMax[1]);
  for (Int_t ir = 0; ir < AreaSize; ++ir) {
    Int_t etaBin = fUseRapidity ? (ir < 3 ? towBin[0] : towBin[1]) : towBin[ir];
    Int_t etaMin = fUseRapidity ? (ir < 3 ? towMin[0] : towMin[1]) : towMin[ir];
    Int_t etaMax = fUseRapidity ? (ir < 3 ? towMax[0] : towMax[1]) : towMax[ir];
    fSiRecoTower[ir] =
        new TH2D(Form("SiRecoTower_%s%d", AreaName.Data(), ir), Form("%s %d; x [mm]; y [mm];", AreaName.Data(), ir),
                 etaBin, etaMin, etaMax, etaBin, etaMin, etaMax);
    fSiTrueTower[ir] =
        new TH2D(Form("SiTrueTower_%s%d", AreaName.Data(), ir), Form("%s %d; x [mm]; y [mm];", AreaName.Data(), ir),
                 etaBin, etaMin, etaMax, etaBin, etaMin, etaMax);
    fSiRecoEnergy[ir] = new TH1D(Form("SiRecoEnergy_%s%d", AreaName.Data(), ir),
                                 Form("%s %d; E [GeV]", AreaName.Data(), ir), eneBin, eneMin, eneMax);
    fSiRecoSiliconEnergy[ir] = new TH1D(Form("SiRecoSiliconEnergy_%s%d", AreaName.Data(), ir),
                                        Form("%s %d; E [GeV]", AreaName.Data(), ir), eneBin, eneMin, eneMax);
    fSiRecoEnergyCorrelation[ir] = new TH2D(Form("SiRecoEnergyCorrelation_%s%d", AreaName.Data(), ir),
                                            Form("%s %d; E_{GSO} [GeV];  E_{Si} [GeV]", AreaName.Data(), ir), eneBin,
                                            eneMin, eneMax, eneBin, eneMin, eneMax);
    fSiTrueEnergy[ir] = new TH1D(Form("SiTrueEnergy_%s%d", AreaName.Data(), ir),
                                 Form("%s %d; E [GeV]", AreaName.Data(), ir), eneBin, eneMin, eneMax);
    fSiEneRes[ir] = new TH1D(Form("SiEneRes_%s%d", AreaName.Data(), ir),
                             Form("%s %d; E_{Reco}/E_{True} - 1 [%]", AreaName.Data(), ir), ereBin, ereMin, ereMax);
    fSiSiliconEneRes[ir] =
        new TH1D(Form("SiSiliconRes_%s%d", AreaName.Data(), ir),
                 Form("%s %d; E_{Reco}/E_{True} - 1 [%]", AreaName.Data(), ir), ereBin, ereMin, ereMax);
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      TString xoy = iv == 0 ? "X" : "Y";
      fSiPosRes[ir][iv] = new TH1D(Form("SiPosRes_%s%d%s", AreaName.Data(), ir, xoy.Data()),
                                   Form("%s %d; %s_{Reco} - %s_{True}", AreaName.Data(), ir, xoy.Data(), xoy.Data()),
                                   preBin, preMin, preMax);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::ClearEvent() {
  fMC = nullptr;
  fLvl2 = nullptr;
  fLvl3 = nullptr;

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

// This function returns TRUE coordinates in "Collision" r.s. for LHC and "Detector" r.s for SPS (and Flat)
template <typename armcal, typename armrec, typename arman, typename armclass>
TVector3 CheckRunIIIData<armcal, armrec, arman, armclass>::GetTrueColCoord(TVector3 posLHC) {
  TVector3 posCol = CT::GetTrueCollisionCoordinates(this->kArmIndex, posLHC);
  return posCol;
}

// This function returns TRUE coordinates in "Calorimeter" r.s.
template <typename armcal, typename armrec, typename arman, typename armclass>
TVector3 CheckRunIIIData<armcal, armrec, arman, armclass>::GetTrueCalCoord(Int_t tower, TVector3 posLHC) {
  TVector3 posCal = CT::GetTrueCalorimeterCoordinates(this->kArmIndex, tower, posLHC);
  return posCal;
}

// This function returns RECO coordinates in "Collision" r.s. for LHC and "Detector" r.s for SPS (and Flat)
template <typename armcal, typename armrec, typename arman, typename armclass>
TVector3 CheckRunIIIData<armcal, armrec, arman, armclass>::GetRecoColCoord(Int_t tower, Double_t posXarm,
                                                                           Double_t posYarm, Int_t maxXlay,
                                                                           Int_t maxYlay) {
  TVector3 posCol = CT::GetRecoCollisionCoordinates(this->kArmIndex, tower, posXarm, posYarm, maxXlay, maxYlay);
  return posCol;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t CheckRunIIIData<armcal, armrec, arman, armclass>::TrueParticleList() {
  fParticleArray[0].clear();
  fParticleArray[1].clear();

  // fMC->Print();
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    fMC->CheckParticleInTower(this->kArmIndex, it, 10., 0.);
    for (Int_t ip = 0; ip < fMC->fReference.size(); ++ip) {
      TVector3 posCal = GetTrueCalCoord(it, fMC->fReference[ip]->Position());
      const Double_t energy = fMC->fReference[ip]->Energy();
      const Double_t x = posCal.X();
      const Double_t y = posCal.Y();
      const Double_t infEdge = 0.;
      const Double_t supEdge = this->kTowerSize[it];
      // printf("############\n");
      // posLHC.Print();
      // posCol.Print();
      // posCal.Print();
      if (energy > 10. && x > infEdge && x < supEdge && y > infEdge && y < supEdge) {
        // printf("Eccoci\n");
        fParticleArray[it].push_back(fMC->fReference[ip]);
      }
    }
  }

  if (fParticleArray[0].size() == 0 && fParticleArray[1].size() == 0) return false;

  return true;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t CheckRunIIIData<armcal, armrec, arman, armclass>::SelectTrueHadron(Int_t tower, Int_t code, Double_t eTrue,
                                                                          Double_t xTrue, Double_t yTrue,
                                                                          TString cuts) {
  const Double_t infEdge = fFiducialEdge;
  const Double_t supEdge = this->kTowerSize[tower] - fFiducialEdge;
  if (cuts.Contains("PID", TString::kIgnoreCase)) {
    if (TMath::Abs(code) == 11 || TMath::Abs(code) == 22)  // electron or gamma
      return false;
  }
  if (cuts.Contains("ENE", TString::kIgnoreCase)) {
    if (eTrue < 250.) return false;
  }
  if (cuts.Contains("POS", TString::kIgnoreCase)) {
    if (!(xTrue > infEdge && xTrue < supEdge && yTrue > infEdge && yTrue < supEdge)) return false;
  }
  if (cuts.Contains("ETA", TString::kIgnoreCase)) {
    if (RapidityCut(0, xTrue, yTrue) < 0) return false;
  }
  return true;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t CheckRunIIIData<armcal, armrec, arman, armclass>::SelectTruePhoton(Int_t tower, Int_t code, Double_t eTrue,
                                                                          Double_t xTrue, Double_t yTrue,
                                                                          TString cuts) {
  const Double_t infEdge = fFiducialEdge;
  const Double_t supEdge = this->kTowerSize[tower] - fFiducialEdge;
  if (cuts.Contains("PID", TString::kIgnoreCase)) {
    if (!(TMath::Abs(code) == 11 || TMath::Abs(code) == 22))  // neutron or baryon
      return false;
  }
  if (cuts.Contains("ENE", TString::kIgnoreCase)) {
    if (eTrue < 250.) return false;
  }
  if (cuts.Contains("POS", TString::kIgnoreCase)) {
    if (!(xTrue > infEdge && xTrue < supEdge && yTrue > infEdge && yTrue < supEdge)) return false;
  }
  if (cuts.Contains("ETA", TString::kIgnoreCase)) {
    if (RapidityCut(1, xTrue, yTrue) < 0) return false;
  }
  return true;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t CheckRunIIIData<armcal, armrec, arman, armclass>::SelectRecoHadron(Int_t tower, Bool_t swTrigger, Double_t eReco,
                                                                          Double_t xReco, Double_t yReco, Double_t L2D,
                                                                          TString cuts) {
  const Double_t infEdge = fFiducialEdge;
  const Double_t supEdge = this->kTowerSize[tower] - fFiducialEdge;
  if (cuts.Contains("SWT", TString::kIgnoreCase)) {
    if (!swTrigger) return false;
  }
  if (cuts.Contains("ENE", TString::kIgnoreCase)) {
    if (eReco < 250.) return false;
  }
  if (cuts.Contains("POS", TString::kIgnoreCase)) {
    if (!(xReco > infEdge && xReco < supEdge && yReco > infEdge && yReco < supEdge)) return false;
  }
  if (cuts.Contains("ETA", TString::kIgnoreCase)) {
    if (RapidityCut(0, xReco, yReco) < 0) return false;
  }
  if (cuts.Contains("PID", TString::kIgnoreCase)) {
    if (fLvl2) {
      if (L2D < 20.)  // electron or gamma
        return false;
    } else {
      if (!fLvl3->fIsNeutron[tower])  // electron or gamma
        return false;
    }
  }
  return true;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t CheckRunIIIData<armcal, armrec, arman, armclass>::SelectRecoPhoton(Int_t tower, Bool_t swTrigger, Double_t eReco,
                                                                          Double_t xReco, Double_t yReco, Double_t L90,
                                                                          TString cuts) {
  const Double_t infEdge = fFiducialEdge;
  const Double_t supEdge = this->kTowerSize[tower] - fFiducialEdge;
  if (cuts.Contains("SWT", TString::kIgnoreCase)) {
    if (!swTrigger) return false;
  }
  if (cuts.Contains("ENE", TString::kIgnoreCase)) {
    if (eReco < 250.) return false;
  }
  if (cuts.Contains("POS", TString::kIgnoreCase)) {
    if (!(xReco > infEdge && xReco < supEdge && yReco > infEdge && yReco < supEdge)) return false;
  }
  if (cuts.Contains("ETA", TString::kIgnoreCase)) {
    if (RapidityCut(1, xReco, yReco) < 0) return false;
  }
  if (cuts.Contains("PID", TString::kIgnoreCase)) {
    if (fLvl2) {
      if (L90 > 20.)  // neutron or baryon
        return false;
    } else {
      if (!fLvl3->fIsPhoton[tower])  // neutron or baryon
        return false;
    }
  }
  return true;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t CheckRunIIIData<armcal, armrec, arman, armclass>::SelectStrictPhoton(Int_t tower, Bool_t swTrigger,
                                                                            Double_t mhReco, Double_t xReco,
                                                                            Double_t yReco, Double_t L90,
                                                                            Double_t eReco, TString cuts) {
  const Double_t infEdge = 5.;
  const Double_t supEdge = this->kTowerSize[tower] - 5.;
  if (cuts.Contains("SWT", TString::kIgnoreCase)) {
    if (!swTrigger) return false;
  }
  if (cuts.Contains("HIT", TString::kIgnoreCase)) {
    if (mhReco) return false;
  }
  if (cuts.Contains("ENE", TString::kIgnoreCase)) {
    if (eReco < 250.) return false;
  }
  if (cuts.Contains("POS", TString::kIgnoreCase)) {
    if (!(xReco > infEdge && xReco < supEdge && yReco > infEdge && yReco < supEdge)) return false;
  }
  if (cuts.Contains("PID", TString::kIgnoreCase)) {
    if (L90 > 18.) return false;
  }
  return true;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Int_t CheckRunIIIData<armcal, armrec, arman, armclass>::RapidityCut(Int_t index, Double_t x, Double_t y) {
  Double_t r = TMath::Sqrt(x * x + y * y);
  Double_t phi = TMath::ATan2(y, x) / TMath::Pi() * 180.;  // deg
  if (phi < 0.) phi += 360.;

  if (index == 0) {
    if (r < 6. && phi > 90. && phi < 270.)
      return 0;
    else if (r > 6. && r < 12. && phi > 135. && phi < 215.)
      return 1;
    else if (r > 12. && r < 18. && phi > 150. && phi < 200.)
      return 2;
    else if (r > 28. && r < 35. && phi > 45. && phi < 70.)
      return 3;
    else if (r > 35. && r < 42. && phi > 45. && phi < 70.)
      return 4;
    else if (r > 42. && r < 49. && phi > 45. && phi < 70.)
      return 5;
    else
      return -1;
  }

  if (index == 1) {
    if (r < 5. && phi > 90. && phi < 270.)
      return 0;
    else if (r > 5. && r < 10. && phi > 135. && phi < 215.)
      return 1;
    else if (r > 10. && r < 15. && phi > 150 && phi < 200)
      return 2;
    else if (r > 28. && r < 35. && phi > 45. && phi < 70.)
      return 3;
    else if (r > 35. && r < 42. && phi > 45. && phi < 70.)
      return 4;
    else if (r > 42. && r < 49. && phi > 45. && phi < 70.)
      return 5;
    else
      return -1;
  }

  return -2;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetPeakArea(Int_t tower, Int_t layer, Int_t view) {
  // NB: This is the correct integral approach for the signal in the whole space
  //	  It would be better to use the integral in the tower but is complicated
  Double_t amplitude = fLvl3->fNeutronPosFitPar[tower][layer][view][0];
  Double_t fraction1 = fLvl3->fNeutronPosFitPar[tower][layer][view][2];
  Double_t fraction2 = fLvl3->fNeutronPosFitPar[tower][layer][view][4];
  Double_t fraction3 = 1 - fraction1 - fraction2;
  if (fLvl3->fNeutronPosFitPar[tower][layer][view][3] < 0) fraction1 = -fraction1;
  if (fLvl3->fNeutronPosFitPar[tower][layer][view][5] < 0) fraction2 = -fraction2;
  if (fLvl3->fNeutronPosFitPar[tower][layer][view][6] < 0) fraction3 = -fraction3;
  amplitude *= TMath::Pi() * (fraction1 + fraction2 + fraction3);
  if (fLvl3->fSilSaturated[tower][layer][view][1]) amplitude = 0.;
  // printf("%f %f\n", fLvl3->fNeutronPosFitPar[tower][layer][view][0], amplitude);
  return amplitude;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillTriggerHisto() {
  if (fLvl3) {
    if (fLvl3->IsShowerTrg()) fTriggerCount->Fill(kShowerTrg);
    if (fLvl3->IsPi0Trg()) fTriggerCount->Fill(kPi0Trg);
    if (fLvl3->IsHighEMTrg()) fTriggerCount->Fill(kHighEMTrg);
    if (fLvl3->IsHadronTrg()) fTriggerCount->Fill(kHadronTrg);
    if (fLvl3->IsZdcTrg()) fTriggerCount->Fill(kZdcTrg);
    if (fLvl3->IsFcTrg()) fTriggerCount->Fill(kFcTrg);
    if (fLvl3->IsL1tTrg()) fTriggerCount->Fill(kL1tTrg);
    if (fLvl3->IsLaserTrg()) fTriggerCount->Fill(kLaserTrg);
    if (fLvl3->IsPedestalTrg()) fTriggerCount->Fill(kPedestalTrg);
  } else if (fLvl2) {
    if (fLvl2->IsShowerTrg()) fTriggerCount->Fill(kShowerTrg);
    if (fLvl2->IsPi0Trg()) fTriggerCount->Fill(kPi0Trg);
    if (fLvl2->IsHighEMTrg()) fTriggerCount->Fill(kHighEMTrg);
    if (fLvl2->IsHadronTrg()) fTriggerCount->Fill(kHadronTrg);
    if (fLvl2->IsZdcTrg()) fTriggerCount->Fill(kZdcTrg);
    if (fLvl2->IsFcTrg()) fTriggerCount->Fill(kFcTrg);
    if (fLvl2->IsL1tTrg()) fTriggerCount->Fill(kL1tTrg);
    if (fLvl2->IsLaserTrg()) fTriggerCount->Fill(kLaserTrg);
    if (fLvl2->IsPedestalTrg()) fTriggerCount->Fill(kPedestalTrg);
  } else if (fLvl1) {
    if (fLvl1->IsShowerTrg()) fTriggerCount->Fill(kShowerTrg);
    if (fLvl1->IsPi0Trg()) fTriggerCount->Fill(kPi0Trg);
    if (fLvl1->IsHighEMTrg()) fTriggerCount->Fill(kHighEMTrg);
    if (fLvl1->IsHadronTrg()) fTriggerCount->Fill(kHadronTrg);
    if (fLvl1->IsZdcTrg()) fTriggerCount->Fill(kZdcTrg);
    if (fLvl1->IsFcTrg()) fTriggerCount->Fill(kFcTrg);
    if (fLvl1->IsL1tTrg()) fTriggerCount->Fill(kL1tTrg);
    if (fLvl1->IsLaserTrg()) fTriggerCount->Fill(kLaserTrg);
    if (fLvl1->IsPedestalTrg()) fTriggerCount->Fill(kPedestalTrg);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillSingleHitHisto() {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    if (fParticleArray[it].size() != 1)  // Consider only singlehit event
      continue;

    const Int_t pdg = TMath::Abs(fParticleArray[it][0]->PdgCode());
    Int_t index = -1;
    if (pdg == 11 || pdg == 22)  // electron or gamma
      index = 1;
    else  // proton or neutron, hadrons here and there
      index = 0;

    //		if(index < 0) //Consider only neutron/proton or gamma/electron
    //			continue;

    /* Reco Trigger */
    const Bool_t swTrigger = fLvl3->fSoftwareTrigger[it];
    /* Reco Position */
    const Int_t xMaxReco = fLvl3->fPosMaxLayer[it][0];
    const Int_t yMaxReco = fLvl3->fPosMaxLayer[it][1];
    const Double_t xCalReco = fLvl3->fNeutronPosition[it][0];
    const Double_t yCalReco = fLvl3->fNeutronPosition[it][1];
    TVector3 posColReco = GetRecoColCoord(it, xCalReco, yCalReco, xMaxReco, yMaxReco);
    Double_t xColReco = -posColReco.X();
    Double_t yColReco = posColReco.Y();
    /* Reco Energy */
    Double_t eReco = index == 0 ? fLvl3->fNeutronEnergy[it] : fLvl3->fPhotonEnergy[it];
    /* True Position */
    TVector3 posLHCTrue = fParticleArray[it][0]->Position();
    TVector3 posColTrue = GetTrueColCoord(posLHCTrue);
    const Double_t xColTrue = -posColTrue.X();
    const Double_t yColTrue = posColTrue.Y();
    TVector3 posCalTrue = GetTrueCalCoord(it, posLHCTrue);
    const Double_t xCalTrue = posCalTrue.X();
    const Double_t yCalTrue = posCalTrue.Y();
    /* True Energy */
    Double_t eTrue = fParticleArray[it][0]->Energy();
    /* Reco PID */
    const Double_t L2D = fLvl3->fNeutronL90[it] - 0.25 * fLvl3->fNeutronL20[it];

    ////This selection must be removed to check SPS simulation
    //		if(!SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D))
    //			continue;
    Int_t ir = it;
    if (fUseRapidity) {
      // This is the only selection applied, that applies only in case of rapidity
      ir = RapidityCut(index, xColTrue, yColTrue);
      if (ir < 0) continue;
    }

    if (fLvl2)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        fCaldE[index][ir][il]->Fill(fLvl2->fCalorimeter[ir][il]);
        fCaldEmap[index][ir][il]->Fill(xCalReco, yCalReco, fLvl2->fCalorimeter[ir][il]);
      }
    fRecoEnergy[index][ir]->Fill(eReco);
    fTrueEnergy[index][ir]->Fill(eTrue);

    fRecoHitmap[index]->Fill(xColReco, yColReco);
    fTrueHitmap[index]->Fill(xColTrue, yColTrue);

    fEneRes[index][ir]->Fill(100. * (eReco / eTrue - 1.));
    fPosRes[index][ir][0]->Fill(xColReco - xColTrue);
    fPosRes[index][ir][1]->Fill(yColReco - yColTrue);

    fRecoTower[index][it]->Fill(xCalReco, yCalReco);  // This is done for each tower, not for each region
    fTrueTower[index][it]->Fill(xCalTrue, yCalTrue);  // This is done for each tower, not for each region

  }  // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillMultiHitHisto() {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /*
     * At first let's apply standard selection on RECO variables as in the analysis
     * (necessary to understand algorithm performances for interesting events)
     */

    const Double_t infEdge = this->fFiducial;
    const Double_t supEdge = this->kTowerSize[it] - this->fFiducial;
    /* Reco Trigger */
    const Bool_t swTrigger = fLvl3->fSoftwareTrigger[it];
    /* Reco Energy */
    const Double_t eReco = fLvl3->fNeutronEnergy[it];
    /* Reco Position */
    const Int_t xMaxReco = fLvl3->fPosMaxLayer[it][0];
    const Int_t yMaxReco = fLvl3->fPosMaxLayer[it][1];
    const Double_t xCalReco = fLvl3->fNeutronPosition[it][0];
    const Double_t yCalReco = fLvl3->fNeutronPosition[it][1];
    TVector3 posColReco = GetRecoColCoord(it, xCalReco, yCalReco, xMaxReco, yMaxReco);
    const Double_t xColReco = -posColReco.X();
    const Double_t yColReco = posColReco.Y();
    /* Reco PID */
    const Double_t L2D = fLvl3->fNeutronL90[it] - 0.25 * fLvl3->fNeutronL20[it];
    if (!SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D)) continue;
    // TODO: That's a critical point, if PID in MH works well, this L2D cut must be modified
    //		- SH: it must be kept since it is very superior
    //		- MH: the new MH cut must efficiently reject 2g!

    /*
     * Compute a tag corresponding to the multihit configuration and fill the migration histograms
     * 1*ID[first-particle] + 10*ID[second-particle] + 100*ID[third-particle]
     */
    UInt_t tagRECO = 0;
    for (Int_t ip = 0; ip < 3; ++ip) tagRECO += (UInt_t)(fLvl3->fMultiParticleSiID[it][ip] * TMath::Power(10, ip));
    //		printf("RECO-TaG %u %u %u -> %u\n",
    //				fLvl3->fMultiParticleSiID[it][0],
    //				fLvl3->fMultiParticleSiID[it][1],
    //				fLvl3->fMultiParticleSiID[it][2],
    //				tagRECO);
    // TODO: Consider if it is correct to include gamma-SH, gg-MH,... contamination in the event count
    UInt_t tagTRUE = 0;
    UInt_t code[3] = {0, 0, 0};
    Int_t np = (Int_t)(TMath::Min(3, (Int_t)fParticleArray[it].size()));
    for (Int_t ip = 0; ip < np; ++ip) {
      const Int_t PDGcode = TMath::Abs(fParticleArray[it][ip]->PdgCode());
      code[ip] = (PDGcode == 11 || PDGcode == 22) ? 1 : 2;
      tagTRUE += (UInt_t)(code[ip] * TMath::Power(10, ip));
    }
    //		printf("TRUE-TaG %u %u %u -> %u\n",
    //				code[0],
    //				code[1],
    //				code[2],
    //				tagTRUE);

    // TODO: Consider it it is correct to consider different fiducial margin for RECO and TRUE
    auto matchingBin = std::upper_bound(migrationEne.begin(), migrationEne.end(), eReco);
    Int_t ie = std::distance(migrationEne.begin(), matchingBin - 1);
    Int_t il = migrationEne.size() - 1;
    //		printf("RECO-TaG %u->%d - TRUE-TaG %u->%d\n", tagRECO, migrationMap.at(tagRECO), tagTRUE,
    // migrationMap.at(tagTRUE)); 		printf("ENERGY-BiN %f -> %d\n", eReco, ie);
    fMHmigration[it][ie]->Fill(migrationMap.at(tagRECO), migrationMap.at(tagTRUE));  // Bin by bin information
    fMHmigration[it][il]->Fill(migrationMap.at(tagRECO), migrationMap.at(tagTRUE));  // Whole range information
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillSWTrgEffHisto() {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /*
     * True information
     */

    if (fParticleArray[it].size() != 1) continue;

    /* True Identity */
    const Int_t code = TMath::Abs(fParticleArray[it][0]->PdgCode());
    /* True Position */
    TVector3 posLHCTrue = fParticleArray[it][0]->Position();
    TVector3 posColTrue = GetTrueColCoord(posLHCTrue);
    const Double_t xColTrue = -posColTrue.X();
    const Double_t yColTrue = posColTrue.Y();
    TVector3 posCalTrue = GetTrueCalCoord(it, posLHCTrue);
    const Double_t xCalTrue = posCalTrue.X();
    const Double_t yCalTrue = posCalTrue.Y();
    /* True Energy */
    Double_t eTrue = fParticleArray[it][0]->Energy();

    Bool_t cutTrue = false;

    // Photon detection efficiency
    if (fUseRapidity) {
      if (fHadEle)
        cutTrue = SelectTrueHadron(it, code, eTrue, xColTrue, yColTrue, "PID ETA");
      else
        cutTrue = SelectTruePhoton(it, code, eTrue, xColTrue, yColTrue, "PID ETA");
    } else {
      if (fHadEle)
        cutTrue = SelectTrueHadron(it, code, eTrue, xCalTrue, yCalTrue, "PID POS");
      else
        cutTrue = SelectTruePhoton(it, code, eTrue, xCalTrue, yCalTrue, "PID POS");
    }

    if (cutTrue) {
      /*
       * Reco information
       */

      /* Software Trigger */
      Bool_t triggerConfirmation = fLvl3->fSoftwareTrigger[it];
      /*Bool_t triggerConfirmation = true;
for (Int_t il = 0; il <= this->kCalNlayer - 3; ++il) {
triggerConfirmation = true;
for (Int_t jl = il; jl < il + 3; ++jl)
triggerConfirmation = triggerConfirmation && fLvl2->fCalorimeter[it][jl] > discriminThreshold[it][jl];
if (triggerConfirmation) break;
}*/

      Int_t ir = fUseRapidity ? RapidityCut(fHadEle ? 1 : 0, xColTrue, yColTrue) : it;

      fSWTriggerAll[ir]->Fill(eTrue);
      if (triggerConfirmation) {
        fSWTriggerYes[ir]->Fill(eTrue);
      }
    }

  }  // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillPosDetHisto() {
  if (fLvl1) {
    if (!fLvl1->IsBeam()) return;
    for (Int_t it = 0; it < this->kPosNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t is = 0; is < this->kPosNsample; ++is)
            for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
              fRawPosDet[it][il][iv][is]->Fill(ic, fLvl1->fPosDet[it][il][iv][ic][is]);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillPosDetMaximum() {
  if (fLvl1) {
    if (!fLvl1->IsBeam()) return;
    for (Int_t it = 0; it < this->kPosNtower; ++it) {
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            Double_t max = -1.0;
            for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
              if (fLvl1->fPosDet[it][il][iv][ic][is] > max) max = fLvl1->fPosDet[it][il][iv][ic][is];
            }
            fRawPosDetMAX[it][il][iv][is]->Fill(max);
            // if (max>8000 && il==3 && iv==1) UT::Printf(UT::kPrintInfo, "\n Event %d \n ", fEntry);
          }
        }
      }
    }
  }
}

// Elena
template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillSiliconDepositHisto() {
  //  if (fLvl3) {
  //    // if (!fLvl1->IsBeam()) return;
  //    Double_t totdep;
  //    Double_t totdep2;
  //    Double_t totdepmean;
  //    Double_t totdepmean2;
  //    Double_t totdepy;
  //    Double_t totdepy2;
  //    Double_t totdepfirst;
  //    for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //      // if (!fLvl3->fIsPhoton[it]) continue;  // skip if it is not photon
  //      totdep = 0.;
  //      totdep2 = 0.;
  //      totdepmean = 0.;
  //      totdepmean2 = 0.;
  //      totdepy = 0.;
  //      totdepy2 = 0.;
  //      totdepfirst = 0.;
  //      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //          fSilDep[it][il][iv]->StatOverflows();
  //          fSilDep[it][il][iv]->Fill(fLvl3->fSiliconDeposit[it][il][iv]);
  //          // layer 2X excluded (except in totdep)
  //          /*
  //          if (il < 3) {
  //            totdep += fLvl3->fSiliconDeposit[it][il][iv];
  //            if (iv == 1) totdepy += fLvl3->fSiliconDeposit[it][il][iv];
  //            if (il < 2) {
  //              totdepfirst += fLvl3->fSiliconDeposit[it][il][iv];
  //              totdepmean += fLvl3->fSiliconDeposit[it][il][iv];
  //            }
  //            if (il == 2 && iv == 1) totdepmean += 2 * fLvl3->fSiliconDeposit[it][il][iv];
  //          }
  //          */
  //          if (il < 3) {
  //            totdep2 += fLvl3->fSiliconDeposit[it][il][iv];
  //            if ((iv == 1) || (il == 2 && iv == 0)) totdepy2 += fLvl3->fSiliconDeposit[it][il][iv];
  //            if (iv == 1) totdepy += fLvl3->fSiliconDeposit[it][il][iv];
  //            if (il < 2) {
  //              totdep += fLvl3->fSiliconDeposit[it][il][iv];
  //              totdepfirst += fLvl3->fSiliconDeposit[it][il][iv];
  //              totdepmean += fLvl3->fSiliconDeposit[it][il][iv];
  //              totdepmean2 += fLvl3->fSiliconDeposit[it][il][iv];
  //            }
  //            if (il == 2) {
  //              totdepmean2 += 2 * fLvl3->fSiliconDeposit[it][il][iv];
  //              if (iv == 1) {
  //                totdep += fLvl3->fSiliconDeposit[it][il][iv];
  //                totdepmean += 2 * fLvl3->fSiliconDeposit[it][il][iv];
  //              }
  //            }
  //          }
  //        }  // view
  //        fSilDepXYCorr[it][il]->StatOverflows();
  //        fSilDepXYCorr[it][il]->Fill(fLvl3->fSiliconDeposit[it][il][0], fLvl3->fSiliconDeposit[it][il][1]);
  //      }  // layer
  //
  //      fSilDepTot[it]->StatOverflows();
  //      fSilDepTotXYMean[it]->StatOverflows();
  //      fSilDepTotYviews[it]->StatOverflows();
  //      fSilDepTot01layers[it]->StatOverflows();
  //
  //      fSilDepTot2[it]->StatOverflows();
  //      fSilDepTotXYMean2[it]->StatOverflows();
  //      fSilDepTotYviews2[it]->StatOverflows();
  //
  //      fSilDepTot[it]->Fill(totdep);               // Total dep= Dep_0y + Dep_0x + Dep_1y + Dep_1x + Dep_2y
  //      fSilDepTotXYMean[it]->Fill(totdepmean);     // Total dep= Dep_0y + Dep_0x + Dep_1y + Dep_1x + 2 * Dep_2y
  //      fSilDepTotYviews[it]->Fill(totdepy);        // Total dep= Dep_0y + Dep_1y + Dep_2y
  //      fSilDepTot01layers[it]->Fill(totdepfirst);  // Total dep= Dep_0y + Dep_0x + Dep_1y + Dep_1x
  //
  //      fSilDepTot2[it]->Fill(totdep2);            // Total dep= Dep_0y + Dep_0x + Dep_1y + Dep_1x + Dep_2y + Dep_2x
  //      fSilDepTotXYMean2[it]->Fill(totdepmean2);  // Total dep= Dep_0y + Dep_0x + Dep_1y + Dep_1x + 2 *
  //      (Dep_2y+Dep_2y) fSilDepTotYviews2[it]->Fill(totdepy2);     // Total dep= Dep_0y + Dep_1y + Dep_2y + Dep_2x
  //
  //      // fSilDepTot_onebin[it]->Fill(totdep);                 //Total deposit= Dep_0y + Dep_0x + Dep_1y + Dep_1x +
  //      // Dep_2y fSilDepTotXYMean_onebin[it]->Fill(totdepmean);       //Total deposit= Dep_0y + Dep_0x + Dep_1y +
  //      Dep_1x
  //      // + 2 * Dep_2y fSilDepTotYviews_onebin[it]->Fill(totdepy);          //Total deposit= Dep_0y + Dep_1y + Dep_2y
  //      // fSilDepTot01layers_onebin[it]->Fill(totdepfirst);    //Total deposit= Dep_0y + Dep_0x + Dep_1y + Dep_1x
  //      // if (fLvl3->fSiliconDeposit[it][1][1]>30) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //    }  // tower
  //  }    // lvl3
}

// Elena
template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillPhotonSiliconAreaHisto() {
  //  if (fLvl3) {
  //    // if (!fLvl1->IsBeam()) return;
  //    Double_t totarea;
  //    for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //      // if (!fLvl3->fIsPhoton[it]) continue;  // skip if it is not a photon
  //      for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
  //        totarea = 0.;
  //        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //          for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //            if (fLvl3->fPhotonPosFitPar[it][il][iv][0 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][1 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][2 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][3 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][4 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][5 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][6 + ip * 7] > 0 ||
  //                true) {
  //              fPhoSilArea[it][ip][il][iv]->Fill(fLvl3->fPhotonSiliconArea[it][ip][il][iv]);
  //              if (fLvl3->fPhotonSiliconArea[it][ip][il][iv] != -1) {
  //                totarea += fLvl3->fPhotonSiliconArea[it][ip][il][iv];
  //              }
  //            }
  //          }
  //        }
  //        fPhoSilAreaTot[it][ip]->Fill(totarea);
  //      }
  //    }  // tower loop
  //  }
}

// Elena
template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillCorrelationSilEnHisto() {
  //  if (fLvl3) {
  //    // if (!fLvl1->IsBeam()) return;
  //    Double_t totarea;
  //    Double_t totdep;
  //    for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //      // if (!fLvl3->fIsPhoton[it]) continue;
  //      // totdep = 0.;
  //      if (fLvl2) {
  //        for (Int_t il = 0; il < this->kCalNlayer; ++il) {
  //          fCaloDep[it][il]->Fill(fLvl2->fCalorimeter[it][il]);
  //        }
  //      }
  //      for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
  //        totarea = 0.;
  //        totdep = 0.;
  //        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //          for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //            if (fLvl3->fPhotonPosFitPar[it][il][iv][0 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][1 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][2 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][3 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][4 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][5 + ip * 7] > 0 &&
  //                    fLvl3->fPhotonPosFitPar[it][il][iv][6 + ip * 7] > 0 ||
  //                true) {
  //              fCorrelSilEn[it][ip][il][iv]->Fill(fLvl3->fSiliconDeposit[it][il][iv],
  //                                                 fLvl3->fPhotonSiliconArea[it][ip][il][iv]);
  //              if (fLvl3->fPhotonSiliconArea[it][ip][il][iv] != -1) {
  //                totarea += fLvl3->fPhotonSiliconArea[it][ip][il][iv];
  //              }
  //            }
  //            totdep += fLvl3->fSiliconDeposit[it][il][iv];
  //
  //          }  // view
  //        }    // layer
  //        fCorrelSilEnTot[it][ip]->Fill(totdep, totarea);
  //        fCorrelRecoEnPhoSilArea[it][ip]->Fill(fLvl3->fPhotonEnergy[it], totarea);
  //        fCorrelRecoXPhoSilArea[it][ip]->Fill(fLvl3->fPhotonPosition[it][0], totarea);
  //        fCorrelRecoYPhoSilArea[it][ip]->Fill(fLvl3->fPhotonPosition[it][1], totarea);
  //        if (ip == 0) {
  //          fCorrelRecoEnSilDep[it]->Fill(fLvl3->fPhotonEnergy[it], totdep);
  //          fCorrelRecoXSilDep[it]->Fill(fLvl3->fPhotonPosition[it][0], totdep);
  //          fCorrelRecoYSilDep[it]->Fill(fLvl3->fPhotonPosition[it][1], totdep);
  //        }
  //      }  // particle
  //         // UT::Printf(UT::kPrintInfo, "\n fLvl2->fCalorimeter[%d][4] %f\n ", it, fLvl2->fCalorimeter[it][4]);
  //      if (fLvl2) {
  //        // UT::Printf(UT::kPrintInfo, "\n fLvl2 %d\n ", fLvl2);
  //        fCorrelSil1YCalo4[it]->Fill(fLvl3->fSiliconDeposit[it][1][1], fLvl2->fCalorimeter[it][4]);
  //        fCorrelSil1YCalo5[it]->Fill(fLvl3->fSiliconDeposit[it][1][1], fLvl2->fCalorimeter[it][5]);
  //      }
  //    }  // tower
  //  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillSilDepMapHisto() {
  //  Double_t sumdep;
  //  Double_t sumdepmean;
  //  Double_t sumdepy;
  //  Double_t sumdep_leakin;
  //  Double_t sumdepmean_leakin;
  //  Double_t sumdepy_leakin;
  //
  //  // Map using Reconstructed X and Y
  //  /*
  //  for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //    // ****Reco Position
  //    const Double_t xCalReco = fHadEle ? fLvl3->fNeutronPosition[it][0] : fLvl3->fPhotonPosition[it][0];
  //    const Double_t yCalReco = fHadEle ? fLvl3->fNeutronPosition[it][1] : fLvl3->fPhotonPosition[it][1];
  //    // ****Reco Energy
  //    // Double_t eReco = fHadEle ? fLvl3->fNeutronEnergy[it] : fLvl3->fPhotonEnergy[it];
  //    sumdep = 0.;
  //    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //        sumdep += fLvl3->fSiliconDeposit[it][il][iv];
  //        fSilDepMapLayer[it][il][iv]->Fill(xCalReco, yCalReco, fLvl3->fSiliconDeposit[it][il][iv]);
  //        fSilDepMapLayerCounts[it][il][iv]->Fill(xCalReco, yCalReco, 1.);
  //      }
  //    }
  //    fSilDepMap[it]->Fill(xCalReco, yCalReco, sumdep);
  //    fSilDepMapCounts[it]->Fill(xCalReco, yCalReco, 1.);
  //  }
  //  */
  //
  //  // Map using True X and Y
  //
  //  for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //    // if (!fLvl3->fIsPhoton[it]) continue ;
  //    fMC->CheckParticleInTower(this->kArmIndex, it, 10., 0.);
  //    for (Int_t ip = 0; ip < fMC->fReference.size(); ++ip) {
  //      TVector3 tmpposCal = fMC->fReference[ip]->Position();
  //      const Double_t tmp_y = tmpposCal.Y();
  //      tmpposCal.SetY(tmp_y + 0);
  //      // TVector3 posCal = GetTrueCalCoord(it, fMC->fReference[ip]->Position());
  //      TVector3 posCal = GetTrueCalCoord(it, tmpposCal);
  //      // const Double_t energy = fMC->fReference[ip]->Energy();
  //      const Double_t x = posCal.X();
  //      const Double_t y = posCal.Y();
  //
  //      for (Int_t jt = 0; jt < this->kCalNtower; ++jt) {
  //        if (jt == it) {
  //          sumdep = 0.;
  //          sumdepmean = 0.;
  //          sumdepy = 0;
  //          for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //            for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //              // Comment the algorithms that you do not want to use
  //              // ALGORITHM ONE: 0X + 0Y + 1X + 1Y + 2X + 2Y + 3X + 3Y
  //              sumdep += fLvl3->fSiliconDeposit[jt][il][iv];
  //              // ALGORITHM TWO: 0X + 0Y + 1X + 1Y + 2x(2X) + 2x(2Y)
  //              if (il < 2)
  //                sumdepmean += fLvl3->fSiliconDeposit[jt][il][iv];
  //              else if (il == 2)
  //                sumdepmean += 2 * (fLvl3->fSiliconDeposit[jt][il][iv]);
  //              // ALGORITHM THREE: 0Y + 1Y + 2Y + 2X
  //              if (il < 3) {
  //                if ((iv == 1) || (il == 2 && iv == 0)) sumdepy += fLvl3->fSiliconDeposit[jt][il][iv];
  //              }
  //              fSilDepMapLayer[jt][il][iv]->Fill(x, y, fLvl3->fSiliconDeposit[jt][il][iv]);
  //              fSilDepMapLayerCounts[jt][il][iv]->Fill(x, y, 1.);
  //            }  // view loop
  //          }    // layer loop
  //          // Fill fSilDepMap with sumdep, sumdepmean or sumdepy according to the algorithm chosen
  //          fSilDepMap[jt]->Fill(x, y, sumdepy);
  //          fSilDepMapCounts[jt]->Fill(x, y, 1.);
  //          // if(x>30 && (y>14 && y<16)) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //          // if(y<2 && (x>14 && x<16)) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //          // if((x>14 && x<16) && (y>14 && y<16)) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //          //
  //          //
  //
  //        }  // if
  //
  //        else if (jt != it) {
  //          sumdep_leakin = 0.;
  //          sumdepmean_leakin = 0.;
  //          sumdepy_leakin = 0;
  //          for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //            for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //              // Comment the algorithms that you do not want to use
  //              // ALGORITHM ONE: 0X + 0Y + 1X + 1Y + 2X + 2Y + 3X + 3Y
  //              sumdep_leakin += fLvl3->fSiliconDeposit[jt][il][iv];
  //              // ALGORITHM TWO: 0X + 0Y + 1X + 1Y + 2x(2X) + 2x(2Y)
  //              if (il < 2)
  //                sumdepmean_leakin += fLvl3->fSiliconDeposit[jt][il][iv];
  //              else if (il == 2)
  //                sumdepmean_leakin += 2 * (fLvl3->fSiliconDeposit[jt][il][iv]);
  //              // ALGORITHM THREE: 0Y + 1Y + 2Y + 2X
  //              if (il < 3) {
  //                if ((iv == 1) || (il == 2 && iv == 0)) sumdepy_leakin += fLvl3->fSiliconDeposit[jt][il][iv];
  //              }
  //              fSilDepMapLeakInLayer[it][il][iv]->Fill(x, y, fLvl3->fSiliconDeposit[jt][il][iv]);
  //              fSilDepMapLeakInLayerCounts[it][il][iv]->Fill(x, y, 1.);
  //            }  // view loop
  //          }    // layer loop
  //          // Fill fSilDepMapLeakIn with sumdep_leakin, sumdepmean_leakin or sumdepy_leakin according to the
  //          algorithm
  //          // chosen
  //          fSilDepMapLeakIn[it]->Fill(x, y, sumdepy_leakin);
  //          fSilDepMapLeakInCounts[it]->Fill(x, y, 1.);
  //          // if(x>30 && (y>14 && y<16)) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //          // if(y<2 && (x>14 && x<16)) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //          // if((x>14 && x<16) && (y>14 && y<16)) UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //
  //        }  // else if
  //
  //      }  // tower loop jt
  //
  //    }  // particle loop
  //  }    // tower loop it
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillRecoPosEnHisto() {
  //  /*
  //   * PID and sumdE are reconstructed outside the loop because it is based on a cut&paste from EventRec
  //   *     We want to keep this simple structure in case of future changes in EventRec
  //   */
  //
  //  for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //    /* Reco Position */
  //    const Double_t xCalReco = fHadEle ? fLvl3->fNeutronPosition[it][0] : fLvl3->fPhotonPosition[it][0];
  //    const Double_t yCalReco = fHadEle ? fLvl3->fNeutronPosition[it][1] : fLvl3->fPhotonPosition[it][1];
  //    /* Reco Energy */
  //    Double_t eReco = fHadEle ? fLvl3->fNeutronEnergy[it] : fLvl3->fPhotonEnergy[it];
  //
  //    fSilRecoTower[it]->Fill(xCalReco, yCalReco);  // This is done for each tower, not for each region
  //    fSilRecoEnergy[it]->Fill(eReco);
  //  }  // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillPhotonSiliconEnergyHisto() {
  //  if (fLvl3) {
  //    for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //      if (it == 0 && fLvl3->fPhotonSiliconEnergy[it] < 500) {
  //        UT::Printf(UT::kPrintInfo, "Event %d\n", fEntry);
  //        printf("%f %f\n", fLvl3->fPhotonEnergy[it], fLvl3->fPhotonSiliconEnergy[it]);
  //      }
  //      fPhotonSilEn[it]->StatOverflows();
  //      fPhotonSilEn[it]->Fill(fLvl3->fPhotonSiliconEnergy[it]);
  //    }
  //  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillPhotonSiliconMapHisto() {
  //  if (fLvl3) {
  //    for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //      fMC->CheckParticleInTower(this->kArmIndex, it, 10., 0.);
  //      for (Int_t ip = 0; ip < fMC->fReference.size(); ++ip) {
  //        TVector3 posCal = GetTrueCalCoord(it, fMC->fReference[ip]->Position());
  //        // const Double_t energy = fMC->fReference[ip]->Energy();
  //        const Double_t x = posCal.X();
  //        const Double_t y = posCal.Y();
  //        //
  //        fPhotonSilCountsMap[it]->Fill(x, y);
  //        fPhotonSilEnergyMap[it]->Fill(x, y, fLvl3->fPhotonSiliconEnergy[it]);
  //      }
  //    }
  //  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillAlignmentHisto() {
  if (fLvl3) {
    if (!fLvl3->IsBeam()) return;  // For LHC
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          Double_t amp = GetPeakArea(it, il, iv);                  // Amplitude [GeV] (if table is present)
          Double_t pos = fLvl3->fNeutronPosFitPar[it][il][iv][1];  // Position [strip]
          Double_t chi = fLvl3->fNeutronPosFitChi2[it][il][iv];
          Double_t ndf = fLvl3->fNeutronPosFitNDF[it][il][iv];
          Double_t rcs = chi / ndf;
          if (this->kArmIndex == 0 && it == 1) pos += this->kPosNchannel[0];
          fFitAmplitude[it][il][iv]->Fill(amp);
          fRedChiSquare[it][il][iv]->Fill(rcs);
          fRedChiVsFitAmp[it][il][iv]->Fill(rcs, amp);
        }

    const Double_t amp_thr = 10000;  // ADC //For LHC
    // const Double_t amp_thr = 1000;  // ADC //For SPS

    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      Int_t max_ref[2] = {0, 0};
      Double_t amp_ref[2] = {0., 0.};
      Double_t pos_ref[2] = {0., 0.};
      Double_t chi_ref[2] = {0., 0.};
      Double_t ndf_ref[2] = {0., 0.};
      Double_t rcs_ref[2] = {0., 0.};
      Bool_t ins_ref[2] = {false, false};
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        max_ref[iv] = fLvl3->fPosMaxLayer[it][iv];
        amp_ref[iv] = GetPeakArea(it, max_ref[iv], iv);                  // Amplitude [GeV] (if table is present)
        pos_ref[iv] = fLvl3->fNeutronPosFitPar[it][max_ref[iv]][iv][1];  // Position [strip]
        chi_ref[iv] = fLvl3->fNeutronPosFitChi2[it][max_ref[iv]][iv];
        ndf_ref[iv] = fLvl3->fNeutronPosFitNDF[it][max_ref[iv]][iv];
        rcs_ref[iv] = chi_ref[iv] / ndf_ref[iv];
        if (this->kArmIndex == 0) {  // Arm1
          if (pos_ref[iv] >= fFiducialEdge && pos_ref[iv] < this->kPosNchannel[it] - fFiducialEdge) ins_ref[iv] = true;
        } else {  // Arm2
          if (pos_ref[iv] >= towerEdge[it][iv][0] + (fFiducialEdge / 0.16) &&
              pos_ref[iv] < towerEdge[it][iv][1] - (fFiducialEdge / 0.16))
            ins_ref[iv] = true;
        }
        if (this->kArmIndex == 0 && it == 1) pos_ref[iv] += this->kPosNchannel[0];
      }
      // NB: It is dangerous to use redchi since bad fits have redchi near one
      if (ins_ref[0] && ins_ref[1])                          // Check it the event is inside fiducial edges
        if (amp_ref[0] > amp_thr && amp_ref[1] > amp_thr) {  // Check if the amplitude is high enough
          for (Int_t iv = 0; iv < this->kPosNview; ++iv)
            for (Int_t il = 0; il < this->kPosNlayer; ++il) {
              Double_t amp_cmp = GetPeakArea(it, il, iv);                  // Amplitude [GeV] (if table is present)
              Double_t pos_cmp = fLvl3->fNeutronPosFitPar[it][il][iv][1];  // Position [strip]
              Double_t chi_cmp = fLvl3->fNeutronPosFitChi2[it][il][iv];
              Double_t ndf_cmp = fLvl3->fNeutronPosFitNDF[it][il][iv];
              Double_t rcs_cmp = chi_cmp / ndf_cmp;
              if (this->kArmIndex == 0 && it == 1) pos_cmp += this->kPosNchannel[0];
              if (amp_cmp > amp_thr) {
                //                // Just for Debug...
                //                if (iv == 1 && max_ref[iv] == 1 && il == 0 && pos_ref[iv] >= 289 && pos_ref[iv] < 290
                //                &&
                //                    (pos_cmp - pos_ref[iv]) > 1.0)
                //                  UT::Printf(UT::kPrintError, "x[%d]=%f vs %f at entry %d\n", max_ref[iv],
                //                  pos_ref[iv], pos_cmp,
                //                             fEntry);
                fRelAlignment[it][iv][max_ref[iv]][il]->Fill(pos_cmp - pos_ref[iv]);
                fRecPosVsRelAli[iv][max_ref[iv]][il]->Fill(pos_ref[iv], pos_cmp - pos_ref[iv]);
                Int_t chn_ref = (Int_t)pos_ref[iv] % 32;
                Int_t chn_cmp = (Int_t)pos_cmp % 32;
                Int_t pacedge = 5;
                if (chn_ref >= pacedge && chn_ref < 32 - pacedge)
                  if (chn_cmp > pacedge && chn_cmp < 32 - pacedge) {
                    fRelAlignmentMaskPACE[it][iv][max_ref[iv]][il]->Fill(pos_cmp - pos_ref[iv]);
                    fRecPosVsRelAliMaskPACE[iv][max_ref[iv]][il]->Fill(pos_ref[iv], pos_cmp - pos_ref[iv]);
                  }
              }
            }
        }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillHitmapHisto() {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    const Double_t infEdge = this->fFiducial;
    const Double_t supEdge = this->kTowerSize[it] - this->fFiducial;
    /* Reco Trigger */
    const Bool_t swTrigger = fLvl3->fSoftwareTrigger[it];
    /* Reco Energy */
    const Double_t eReco = fLvl3->fNeutronEnergy[it];
    /* Reco Position */
    const Int_t xMaxReco = fLvl3->fPosMaxLayer[it][0];
    const Int_t yMaxReco = fLvl3->fPosMaxLayer[it][1];
    const Double_t xCalReco = fLvl3->fNeutronPosition[it][0];
    const Double_t yCalReco = fLvl3->fNeutronPosition[it][1];
    TVector3 posCalChan = CT::CalorimeterToChannel(this->kArmIndex, it, xMaxReco, yMaxReco, xCalReco, yCalReco);
    Double_t xArmChan = posCalChan.X();
    Double_t yArmChan = posCalChan.Y();
    if (this->kArmIndex == 0 && it == 1) {
      xArmChan += this->kPosNchannel[0];
      yArmChan += this->kPosNchannel[0];
    }
    TVector3 posColReco = GetRecoColCoord(it, xCalReco, yCalReco, xMaxReco, yMaxReco);
    const Double_t xColReco = -posColReco.X();
    const Double_t yColReco = posColReco.Y();
    /* Reco PID */
    const Double_t L2D = fLvl3->fNeutronL90[it] - 0.25 * fLvl3->fNeutronL20[it];

    if (!SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D)) continue;

    Int_t ix = xMaxReco;
    Int_t iy = yMaxReco;
    auto matchingBin = std::upper_bound(hitmapEne.begin(), hitmapEne.end(), eReco);
    Int_t ib = std::distance(hitmapEne.begin(), matchingBin - 1);
    if (ib < 0) continue;

    // Just for Debug...
    // if(ix==3 && xArmChan>=255-0.5 && xArmChan<255+0.5)
    // UT::Printf(UT::kPrintError, "x[%d]=%.0f at entry %d\n", ix, xArmChan, fEntry);
    // if(iy==3 && yArmChan>=287-0.5 && yArmChan<287+0.5)
    // UT::Printf(UT::kPrintError, "y[%d]=%.0f at entry %d\n", iy, yArmChan, fEntry);

    Int_t ir = 0;
    if (fFirstRun >= 0 && fLastRun >= 0) ir = fLvl3->fRun - fFirstRun;

    for (Int_t ie = 0; ie <= ib; ++ie) {
      fVertexZaverage[ir][0][ie]->Fill(xMaxReco);
      fVertexZaverage[ir][1][ie]->Fill(yMaxReco);
      fVertexHitmap1D[ir][ix][0][ie]->Fill(xColReco);
      fVertexHitmap1D[ir][iy][1][ie]->Fill(yColReco);
      fVertexHitmap2D[ir][ix][iy][ie]->Fill(xColReco, yColReco);
      fVertexPDChan2D[ir][ix][iy][ie]->Fill(xArmChan, yArmChan);
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillMassHisto() {
  Int_t ir = 0;
  if (fFirstRun >= 0 && fLastRun >= 0) ir = fLvl3->fRun - fFirstRun;

  PionRec pionRec;
  pionRec.Reconstruct<Level3<armrec>, EventCut<arman, armrec>>(fLvl3, &fEveCut);

  for (int ip = 0; ip < PionRec::kNtypes; ++ip) {
    UInt_t pionFlag = pionRec.fResult[ip].fFlag;
    if (pionFlag & 0x1) {
      PionType pionType = pionRec.fResult[ip].GetType();
      if (pionType >= 0) {
        Double_t collision_energy = this->fOperation == kLHC2022 ? 13.6 * 1e3 : 13 * 1e3;  // GeV
        Double_t pionEnergy = pionRec.fResult[ip].GetEnergy();
        Double_t pionxF = 2. * pionEnergy / collision_energy;
        Double_t pionMass = pionRec.fResult[ip].GetMass();
        Int_t pionBin = pionxF * fNxFBin;
        if (pionBin < 0) pionBin = 0;
        if (pionBin >= fNxFBin) pionBin = fNxFBin - 1;
        fInvMassRunXf[pionType][pionBin][ir]->Fill(pionMass);
        fInvMassTotXf[pionType][pionBin]->Fill(pionMass);
        fInvMassRun[pionType][ir]->Fill(pionMass);
        fInvMassTot[pionType]->Fill(pionMass);
        if (pionType == 0) {                // Type-I only
          for (int ig = 0; ig < 2; ++ig) {  // Loop over the two photons
            Double_t photonTower = pionRec.fResult[ip].fPhotonTower[ig];
            Double_t photonEnergy = pionRec.fResult[ip].fPhotonEnergy[ig];
            Double_t photonxF = 2. * photonEnergy / collision_energy;
            Int_t photonBin = photonxF * fNxFBin;
            if (photonBin >= fNxFBin) photonBin = fNxFBin - 1;
            Int_t photonType = 3 + photonTower;
            fInvMassRunXf[photonType][photonBin][ir]->Fill(pionMass);
            fInvMassTotXf[photonType][photonBin]->Fill(pionMass);
            fInvMassRun[photonType][ir]->Fill(pionMass);
            fInvMassTot[photonType]->Fill(pionMass);
          }
        }
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillDiscriminHisto() {
  if (fLvl2) {
    // Select only standard shower events
    if (!fLvl2->IsShowerTrg()) return;
    // Fill discriminator distributions
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        if (fLvl2->IsDsc(it, il)) fDiscrimYes[it][il]->Fill(fLvl2->fCalorimeter[it][il]);
        fDiscrimAll[it][il]->Fill(fLvl2->fCalorimeter[it][il]);
      }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillPileUpHisto() {
  if (fLvl3->IsPedestal() || fMC) {
    fPileUpCount->Fill(kPedestal);
    if ((fLvl3->IsBeam1() && fLvl3->IsBeam2()) || fMC) {
      fPileUpCount->Fill(kCrossing);

      vector<Double_t> trigger = {false, false};
      for (Int_t it = 0; it < this->kCalNtower; ++it) {
        for (Int_t il = 0; il <= this->kCalNlayer - 3; ++il) {
          trigger[it] = true;
          for (Int_t jl = il; jl < il + 3; ++jl) {
            // trigger[it] = trigger[it] && fLvl2->fCalorimeter[it][jl] > discriminThreshold[it][jl];
            trigger[it] = trigger[it] && fLvl2->IsDsc(it, jl);
          }
          if (trigger[it]) {
            break;
          }
        }
      }

      if (trigger[0] || trigger[1]) {
        fPileUpCount->Fill(kTotal);
        if (trigger[0]) {
          fPileUpCount->Fill(kST);
        }
        if (trigger[1]) {
          fPileUpCount->Fill(kLT);
        }
        for (Int_t it = 0; it < this->kCalNtower; ++it) {
          if (trigger[it]) {
            fPileUpL90[it]->Fill(fLvl3->fPhotonL90[it]);
            fPileUpEnergy[it]->Fill(fLvl3->fPhotonEnergy[it]);
            if (fMC) {
              Int_t PDG_code = TMath::Abs(fParticleArray[it][0]->PdgCode());
              Int_t Particle = (PDG_code == 11 || PDG_code == 22) ? 0 : 1;
              fPileUpL90Particle[it][Particle]->Fill(fLvl3->fPhotonL90[it]);
              fPileUpEnergyParticle[it][Particle]->Fill(fLvl3->fPhotonEnergy[it]);
            }
          }
        }
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillSPSHisto() {
  /*
   * Fill calorimeter vector according to lvl1 or lvl2
   */
  vector<vector<Double_t>> calorimeter;
  Utils::AllocateVector2D(calorimeter, this->kCalNtower, this->kCalNlayer);
  if (fLvl2) {
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        // calorimeter[it][il] = fLvl1->fCalorimeter[it][il][0]; //lvl1
        calorimeter[it][il] = fLvl2->fCalorimeter[it][il];  // lvl2
      }
  }

  /*
   * PID and sumdE are reconstructed outside the loop because it is based on a cut&paste from EventRec
   *     We want to keep this simple structure in case of future changes in EventRec
   */
  vector<Double_t> L20 = vector<Double_t>(this->kCalNtower, 0.);
  vector<Double_t> L90 = vector<Double_t>(this->kCalNtower, 0.);
  vector<Double_t> L2D = vector<Double_t>(this->kCalNtower, 0.);
  vector<Double_t> sumdE = vector<Double_t>(this->kCalNtower, 0.);
  if (fLvl2) {
    /* Reco PID */
    CalculateL20L90L2D(calorimeter, L20, L90, L2D, fRotDet);
    /* Reco sumdE */
    CalculateSumdE(calorimeter, sumdE, fHadEle, fRotDet);
  }

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    //		    ////This selection must be removed to check SPS simulation
    //		    //		if(!SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D))
    //		    //			continue;
    //		    Int_t ir = it;
    //		    if (fUseRapidity) {
    //		      // This is the only selection applied, that applies only in case of rapidity
    //		      ir = RapidityCut(index, xColTrue, yColTrue);
    //		      if (ir < 0) continue;
    //		    }

    /*
     * Reco information
     */

    /* Reco Trigger */
    const Bool_t swTrigger = fLvl3->fSoftwareTrigger[it];
    /* Reco Position */
    const Int_t xMaxReco = fLvl3->fPosMaxLayer[it][0];
    const Int_t yMaxReco = fLvl3->fPosMaxLayer[it][1];
    const Double_t xCalReco = fHadEle ? fLvl3->fNeutronPosition[it][0] : fLvl3->fPhotonPosition[it][0];
    const Double_t yCalReco = fHadEle ? fLvl3->fNeutronPosition[it][1] : fLvl3->fPhotonPosition[it][1];
    TVector3 posColReco = GetRecoColCoord(it, xCalReco, yCalReco, xMaxReco, yMaxReco);
    Double_t xColReco = -posColReco.X();
    Double_t yColReco = posColReco.Y();
    /* Reco Energy */
    Double_t eReco = fHadEle ? fLvl3->fNeutronEnergy[it] : fLvl3->fPhotonEnergy[it];

    Bool_t cutReco = false;
    if (fHadEle)
      cutReco = SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D[it], "PID POS");
    else
      cutReco = SelectRecoPhoton(it, swTrigger, eReco, xCalReco, yCalReco, L90[it], "PID POS");

    if (cutReco) {
      fSPSRecoHitmap->Fill(xColReco, yColReco);
      fSPSRecoTower[it]->Fill(xCalReco, yCalReco);  // This is done for each tower, not for each region
      fSPSRecoEnergy[it]->Fill(sumdE[it]);

      if (fLvl2) {
        for (Int_t il = 0; il < this->kCalNlayer; ++il) {
          fSPSCaldE[it][il]->Fill(calorimeter[it][il]);
          fSPSCaldEmap[it][il]->Fill(xCalReco, yCalReco, calorimeter[it][il]);
          fSPSAvedE[it]->Fill(il, calorimeter[it][il]);
        }
        fSPSL20[it]->Fill(L20[it]);
        fSPSL90[it]->Fill(L90[it]);
        fSPSL2D[it]->Fill(L2D[it]);
      }
    }

    /*
     * True information
     */

    if (fParticleArray[it].size() != 1) continue;

    /* True Identity */
    const Int_t code = TMath::Abs(fParticleArray[it][0]->PdgCode());
    /* True Position */
    TVector3 posSPSTrue = fParticleArray[it][0]->Position();
    TVector3 posColTrue = GetTrueColCoord(posSPSTrue);
    const Double_t xColTrue = -posColTrue.X();
    const Double_t yColTrue = posColTrue.Y();
    TVector3 posCalTrue = GetTrueCalCoord(it, posSPSTrue);
    const Double_t xCalTrue = posCalTrue.X();
    const Double_t yCalTrue = posCalTrue.Y();
    /* True Energy */
    Double_t eTrue = fParticleArray[it][0]->Energy();

    Bool_t cutTrue;
    if (fHadEle)
      cutTrue = SelectTrueHadron(it, code, eTrue, xCalTrue, yCalTrue, "PID POS");
    else
      cutTrue = SelectTruePhoton(it, code, eTrue, xCalTrue, yCalTrue, "PID POS");

    if (cutTrue) {
      fSPSTrueHitmap->Fill(xColTrue, yColTrue);
      fSPSTrueTower[it]->Fill(xCalTrue, yCalTrue);
      fSPSTrueEnergy[it]->Fill(eTrue);

      fSPSEneRes[it]->Fill(100. * (eReco / eTrue - 1.));
      fSPSPosRes[it][0]->Fill(xColReco - xColTrue);
      fSPSPosRes[it][1]->Fill(yColReco - yColTrue);
    }

  }  // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillLHCHisto() {
  /*
   * PID and sumdE are reconstructed outside the loop because it is based on a cut&paste from EventRec
   *     We want to keep this simple structure in case of future changes in EventRec
   */
  vector<Double_t> L20 = vector<Double_t>(this->kCalNtower, 0.);
  vector<Double_t> L90 = vector<Double_t>(this->kCalNtower, 0.);
  vector<Double_t> L2D = vector<Double_t>(this->kCalNtower, 0.);
  vector<Double_t> sumdE = vector<Double_t>(this->kCalNtower, 0.);
  if (fLvl2) {
    /* Reco PID */
    CalculateL20L90L2D(fLvl2->fCalorimeter, L20, L90, L2D, fRotDet);
    /* Reco sumdE */
    CalculateSumdE(fLvl2->fCalorimeter, sumdE, fHadEle, fRotDet);
  }

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /*
     * Reco information
     */

    /* Reco Trigger */
    const Bool_t swTrigger = fLvl3->fSoftwareTrigger[it];
    /* Reco Position */
    const Int_t xMaxReco = fLvl3->fPosMaxLayer[it][0];
    const Int_t yMaxReco = fLvl3->fPosMaxLayer[it][1];
    const Double_t xCalReco = fHadEle ? fLvl3->fNeutronPosition[it][0] : fLvl3->fPhotonPosition[it][0];
    const Double_t yCalReco = fHadEle ? fLvl3->fNeutronPosition[it][1] : fLvl3->fPhotonPosition[it][1];
    TVector3 posColReco = GetRecoColCoord(it, xCalReco, yCalReco, xMaxReco, yMaxReco);
    Double_t xColReco = -posColReco.X();
    Double_t yColReco = posColReco.Y();
    /* Reco Energy */
    Double_t eReco = fHadEle ? fLvl3->fNeutronEnergy[it] : fLvl3->fPhotonEnergy[it];

    // Raw preliminary selection to check the absolute energy scale shift
    // if (!fLvl3->IsShowerTrg()) continue;
    // if (eReco < 500 || eReco > 550) continue;

    Bool_t cutReco = false;
    if (fUseRapidity) {
      if (fHadEle)
        cutReco = SelectRecoHadron(it, swTrigger, eReco, xColReco, yColReco, L2D[it], "SWT ENE PID ETA");
      else
        cutReco = SelectRecoPhoton(it, swTrigger, eReco, xColReco, yColReco, L90[it], "ENE PID ETA");
    } else {
      if (fHadEle)
        cutReco = SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D[it], "SWT ENE PID POS");
      else
        cutReco = SelectRecoPhoton(it, swTrigger, eReco, xCalReco, yCalReco, L90[it], "ENE PID POS");
    }

    if (cutReco) {
      Int_t ir = fUseRapidity ? RapidityCut(fHadEle ? 0 : 1, xColReco, yColReco) : it;

      fLHCRecoHitmap->Fill(xColReco, yColReco);
      fLHCRecoTower[ir]->Fill(xCalReco, yCalReco);  // This is done for each tower, not for each region
      fLHCRecoEnergy[ir]->Fill(eReco);

      if (fLvl2) {
        for (Int_t il = 0; il < this->kCalNlayer; ++il) {
          fLHCCaldE[ir][il]->Fill(fLvl2->fCalorimeter[it][il]);
          fLHCCaldEmap[ir][il]->Fill(xCalReco, yCalReco, fLvl2->fCalorimeter[it][il]);
          fLHCAvedE[ir]->Fill(il, fLvl2->fCalorimeter[it][il]);
        }
        fLHCL20[ir]->Fill(L20[it]);
        fLHCL90[ir]->Fill(L90[it]);
        fLHCL2D[ir]->Fill(L2D[it]);
        fLHCSumdE[ir]->Fill(sumdE[it]);
      }
    }

    /*
     * True information
     */

    if (fParticleArray[it].size() != 1) continue;

    /* True Identity */
    const Int_t code = TMath::Abs(fParticleArray[it][0]->PdgCode());
    /* True Position */
    TVector3 posLHCTrue = fParticleArray[it][0]->Position();
    TVector3 posColTrue = GetTrueColCoord(posLHCTrue);
    const Double_t xColTrue = -posColTrue.X();
    const Double_t yColTrue = posColTrue.Y();
    TVector3 posCalTrue = GetTrueCalCoord(it, posLHCTrue);
    const Double_t xCalTrue = posCalTrue.X();
    const Double_t yCalTrue = posCalTrue.Y();
    /* True Energy */
    Double_t eTrue = fParticleArray[it][0]->Energy();

    Bool_t cutTrue = false;
    if (fUseRapidity) {
      if (fHadEle)
        cutTrue = SelectTrueHadron(it, code, eTrue, xColTrue, yColTrue, "ENE PID ETA");
      else
        cutTrue = SelectTruePhoton(it, code, eTrue, xColTrue, yColTrue, "ENE PID ETA");
    } else {
      if (fHadEle)
        cutTrue = SelectTrueHadron(it, code, eTrue, xCalTrue, yCalTrue, "ENE PID POS");
      else
        cutTrue = SelectTruePhoton(it, code, eTrue, xCalTrue, yCalTrue, "ENE PID POS");
    }

    if (cutTrue) {
      Int_t ir = fUseRapidity ? RapidityCut(fHadEle ? 0 : 1, xColTrue, yColTrue) : it;

      fLHCTrueHitmap->Fill(xColTrue, yColTrue);
      fLHCTrueTower[ir]->Fill(xCalTrue, yCalTrue);
      fLHCTrueEnergy[ir]->Fill(eTrue);

      fLHCEneRes[ir]->Fill(100. * (eReco / eTrue - 1.));
      fLHCPosRes[ir][0]->Fill(xColReco - xColTrue);
      fLHCPosRes[ir][1]->Fill(yColReco - yColTrue);
    }

  }  // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillGSOCalibHisto() {
  // Select single L2T events
  if (!fMC && !fLvl3->IsShowerTrg()) return;

  Double_t swTrigger[this->kCalNtower];
  Double_t L90[this->kCalNtower];
  Double_t mhReco[this->kCalNtower];
  Double_t xCalReco[this->kCalNtower];
  Double_t yCalReco[this->kCalNtower];
  Double_t eReco[this->kCalNtower];
  Double_t cutReco[this->kCalNtower];

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /* Reco Trigger */
    swTrigger[it] = fLvl3->fSoftwareTrigger[it];
    /* Reco MH */
    mhReco[it] = fLvl3->fPhotonMultiHit[it];
    /* Reco Position */
    xCalReco[it] = fLvl3->fPhotonPosition[it][0];
    yCalReco[it] = fLvl3->fPhotonPosition[it][1];
    /* Reco PID */
    L90[it] = fLvl3->fPhotonL90[it];
    /* Reco Energy */
    eReco[it] = fLvl3->fPhotonEnergy[it];

    /* Cut */
    cutReco[it] = SelectStrictPhoton(it, swTrigger[it], mhReco[it], xCalReco[it], yCalReco[it], L90[it], eReco[it]);
  }

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    if (swTrigger[this->kCalNtower - it - 1])  // Remove multitower events
      continue;
    if (!cutReco[it])  // Remove multihit events
      continue;
    Int_t ib = -1;
    for (Int_t iv = 0; iv < energyBinning.size() - 1; ++iv) {
      if (eReco[it] >= energyBinning[iv] && eReco[it] < energyBinning[iv + 1]) {
        ib = iv;
        break;
      }
    }
    if (ib < 0) continue;
    fGSOCalibTower[it][ib]->Fill(xCalReco[it], yCalReco[it]);
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      fGSOCalibCaldE[it][il][ib]->Fill(fLvl2->fCalorimeter[it][il]);
      fGSOCalibAvedE[it][ib]->Fill(il, fLvl2->fCalorimeter[it][il]);
      if (fMC) {
        if (fParticleArray[it].size() != 1)  // Consider only singlehit event
          continue;

        const Int_t pdg = TMath::Abs(fParticleArray[it][0]->PdgCode());
        Int_t index = -1;
        if (pdg == 11 || pdg == 22)  // electron or gamma
          index = 1;
        else  // proton or neutron, hadrons here and there
          index = 0;

        if (index == -1) continue;

        fGSOCalibCalMC[it][il][ib][index]->Fill(fLvl2->fCalorimeter[it][il]);
      }
    }  // layer loop
  }    // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::FillSiliconHisto() {
  //  /*
  //   * PID and sumdE are reconstructed outside the loop because it is based on a cut&paste from EventRec
  //   *     We want to keep this simple structure in case of future changes in EventRec
  //   */
  //  vector<Double_t> L20 = vector<Double_t>(this->kCalNtower, 0.);
  //  vector<Double_t> L90 = vector<Double_t>(this->kCalNtower, 0.);
  //  vector<Double_t> L2D = vector<Double_t>(this->kCalNtower, 0.);
  //  if (fLvl2) {
  //    /* Reco PID */
  //    CalculateL20L90L2D(fLvl2->fCalorimeter, L20, L90, L2D, fRotDet);
  //  }
  //
  //  for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //    /*
  //     * Preliminary selection: Select only single particle events
  //     */
  //    // Consider only single hit (tower-level) events
  //    const Bool_t mHitFlag = fHadEle ? fLvl3->fNeutronMultiHit[it] : fLvl3->fPhotonMultiHit[it];
  //    if (mHitFlag) continue;
  //    // Consider only single hit (detector-level) events
  //    const Bool_t towerFlag = fLvl3->fSoftwareTrigger[this->kCalNtower - it - 1];
  //    if (towerFlag) continue;
  //
  //    /*
  //     * Reco information
  //     */
  //
  //    /* Reco Trigger */
  //    const Bool_t swTrigger = fLvl3->fSoftwareTrigger[it];
  //    /* Reco Position */
  //    const Int_t xMaxReco = fLvl3->fPosMaxLayer[it][0];
  //    const Int_t yMaxReco = fLvl3->fPosMaxLayer[it][1];
  //    const Double_t xCalReco = fHadEle ? fLvl3->fNeutronPosition[it][0] : fLvl3->fPhotonPosition[it][0];
  //    const Double_t yCalReco = fHadEle ? fLvl3->fNeutronPosition[it][1] : fLvl3->fPhotonPosition[it][1];
  //    TVector3 posColReco = GetRecoColCoord(it, xCalReco, yCalReco, xMaxReco, yMaxReco);
  //    Double_t xColReco = -posColReco.X();
  //    Double_t yColReco = posColReco.Y();
  //    /* Reco Energy */
  //    Double_t eReco = fHadEle ? fLvl3->fNeutronEnergy[it] : fLvl3->fPhotonEnergy[it];
  //    Double_t sReco = fLvl3->fPhotonSiliconEnergy[it];
  //
  //    // Raw preliminary selection to check the absolute energy scale shift
  //    // if (!fLvl3->IsShowerTrg()) continue;
  //    // if (eReco < 500 || eReco > 550) continue;
  //
  //    Bool_t cutReco = false;
  //
  //    if (fUseRapidity) {
  //      if (fHadEle)
  //        cutReco = SelectRecoHadron(it, swTrigger, eReco, xColReco, yColReco, L2D[it], "SWT ENE PID ETA");
  //      else
  //        cutReco = SelectRecoPhoton(it, swTrigger, eReco, xColReco, yColReco, L90[it], "ENE PID ETA");
  //    } else {
  //      if (fHadEle)
  //        cutReco = SelectRecoHadron(it, swTrigger, eReco, xCalReco, yCalReco, L2D[it], "SWT ENE PID POS");
  //      else
  //        cutReco = SelectRecoPhoton(it, swTrigger, eReco, xCalReco, yCalReco, L90[it], "ENE PID POS");
  //    }
  //
  //    if (cutReco) {
  //      Int_t ir = fUseRapidity ? RapidityCut(fHadEle ? 0 : 1, xColReco, yColReco) : it;
  //
  //      fSiRecoHitmap->Fill(xColReco, yColReco);
  //      fSiRecoTower[ir]->Fill(xCalReco, yCalReco);  // This is done for each tower, not for each region
  //      fSiRecoEnergy[ir]->Fill(eReco);
  //      fSiRecoSiliconEnergy[ir]->Fill(sReco);
  //      fSiRecoEnergyCorrelation[ir]->Fill(eReco, sReco);
  //    }
  //
  //    /*
  //     * True information
  //     */
  //
  //    if (fParticleArray[it].size() != 1) continue;
  //
  //    /* True Identity */
  //    const Int_t code = TMath::Abs(fParticleArray[it][0]->PdgCode());
  //    /* True Position */
  //    TVector3 posLHCTrue = fParticleArray[it][0]->Position();
  //    TVector3 posColTrue = GetTrueColCoord(posLHCTrue);
  //    const Double_t xColTrue = -posColTrue.X();
  //    const Double_t yColTrue = posColTrue.Y();
  //    TVector3 posCalTrue = GetTrueCalCoord(it, posLHCTrue);
  //    const Double_t xCalTrue = posCalTrue.X();
  //    const Double_t yCalTrue = posCalTrue.Y();
  //    /* True Energy */
  //    Double_t eTrue = fParticleArray[it][0]->Energy();
  //
  //    Bool_t cutTrue = false;
  //    if (fUseRapidity) {
  //      if (fHadEle)
  //        cutTrue = SelectTrueHadron(it, code, eTrue, xColTrue, yColTrue, "ENE PID ETA");
  //      else
  //        cutTrue = SelectTruePhoton(it, code, eTrue, xColTrue, yColTrue, "ENE PID ETA");
  //    } else {
  //      if (fHadEle)
  //        cutTrue = SelectTrueHadron(it, code, eTrue, xCalTrue, yCalTrue, "ENE PID POS");
  //      else
  //        cutTrue = SelectTruePhoton(it, code, eTrue, xCalTrue, yCalTrue, "ENE PID POS");
  //    }
  //
  //    if (cutTrue) {
  //      Int_t ir = fUseRapidity ? RapidityCut(fHadEle ? 0 : 1, xColTrue, yColTrue) : it;
  //
  //      fSiTrueHitmap->Fill(xColTrue, yColTrue);
  //      fSiTrueTower[ir]->Fill(xCalTrue, yCalTrue);
  //      fSiTrueEnergy[ir]->Fill(eTrue);
  //
  //      fSiEneRes[ir]->Fill(100. * (eReco / eTrue - 1.));
  //      fSiPosRes[ir][0]->Fill(xColReco - xColTrue);
  //      fSiPosRes[ir][1]->Fill(yColReco - yColTrue);
  //
  //      fSiSiliconEneRes[ir]->Fill(100. * (sReco / eTrue - 1.));
  //    }
  //
  //  }  // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeTriggerHisto() {
  for (Int_t ib = 1; ib <= nTRIGGER; ++ib) {
    fTriggerCount->GetXaxis()->SetBinLabel(ib, LabelTRIGGER[ib - 1].c_str());
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeSingleHitHisto() {
  // Compute average dE vx (x,y)
  for (Int_t ip = 0; ip < this->fN; ++ip)
    for (Int_t ir = 0; ir < fCaldEmap[ip].size(); ++ir)
      for (Int_t il = 0; il < fCaldEmap[ip][ir].size(); ++il) fCaldEmap[ip][ir][il]->Divide(fRecoTower[ip][ir]);
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeMultiHitHisto() {
  TString tag = fUseRapidity ? "Region" : "Tower";

  // Set Readable Bin Labels
  for (Int_t it = 0; it < fMHmigration.size(); ++it)
    for (Int_t ie = 0; ie < fMHmigration[it].size(); ++ie) {
      for (Int_t ix = 1; ix <= fMHmigration[it][ie]->GetXaxis()->GetNbins(); ++ix)
        fMHmigration[it][ie]->GetXaxis()->SetBinLabel(ix, migrationLab.at(ix).c_str());
      for (Int_t iy = 1; iy <= fMHmigration[it][ie]->GetYaxis()->GetNbins(); ++iy)
        fMHmigration[it][ie]->GetYaxis()->SetBinLabel(iy, migrationLab.at(iy).c_str());
      // fMHmigration[it][ie]->Draw("TEXT COLZ GOFF");
    }

  // Efficiency Matrix
  for (Int_t it = 0; it < fMHmigration.size(); ++it)
    for (Int_t ie = 0; ie < fMHmigration[it].size(); ++ie) {
      string rep_str = "MHMigrationMap";
      string aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHEfficiencyMap");
      fEfficiency[it][ie] = (TH2D *)fMHmigration[it][ie]->Clone();
      fEfficiency[it][ie]->SetName(aux_str.c_str());
      fEfficiency[it][ie]->Reset();
      for (Int_t iy = 0; iy < fEfficiency[it][ie]->GetYaxis()->GetNbins() + 1; ++iy) {
        // Get Normalization Factor
        Double_t normFact = 0.;
        for (Int_t ix = 0; ix < fEfficiency[it][ie]->GetXaxis()->GetNbins() + 1; ++ix)
          normFact += fMHmigration[it][ie]->GetBinContent(ix, iy);
        if (normFact == 0) continue;
        // Scale Content for That Factor
        for (Int_t ix = 0; ix < fEfficiency[it][ie]->GetXaxis()->GetNbins() + 1; ++ix) {
          Double_t mapsCont = fMHmigration[it][ie]->GetBinContent(ix, iy);
          Double_t mapsErr = fMHmigration[it][ie]->GetBinError(ix, iy);
          mapsCont /= normFact;
          mapsErr /= normFact;
          if (mapsCont > 0) {
            fEfficiency[it][ie]->SetBinContent(ix, iy, mapsCont);
            fEfficiency[it][ie]->SetBinError(ix, iy, mapsErr);
          }
        }
      }
      rep_str = "MHMigrationMap";
      aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHNotEfficiencyMap");
      fNotEfficiency[it][ie] = (TH2D *)fEfficiency[it][ie]->Clone();
      fNotEfficiency[it][ie]->SetName(aux_str.c_str());
      for (Int_t iy = 0; iy < fEfficiency[it][ie]->GetYaxis()->GetNbins() + 1; ++iy) {
        // Remove Non-Diagonal Terms
        for (Int_t ix = 0; ix < fEfficiency[it][ie]->GetXaxis()->GetNbins() + 1; ++ix) {
          if (ix != iy) fEfficiency[it][ie]->SetBinContent(ix, iy, 0);
        }
        // Remove Diagonal Terms
        for (Int_t ix = 0; ix < fNotEfficiency[it][ie]->GetXaxis()->GetNbins() + 1; ++ix) {
          if (ix == iy) fNotEfficiency[it][ie]->SetBinContent(ix, iy, 0);
        }
      }
      aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHEfficiency");
      fEfficiency1D[it][ie] =
          new TH1D(aux_str.c_str(), Form("%s; ; Efficiency", fMHmigration[it][ie]->GetTitle()),
                   fMHmigration[it][ie]->GetXaxis()->GetNbins(), fMHmigration[it][ie]->GetXaxis()->GetXmin(),
                   fMHmigration[it][ie]->GetXaxis()->GetXmax());
      for (Int_t ib = 1; ib <= fEfficiency1D[it][ie]->GetXaxis()->GetNbins(); ++ib) {
        fEfficiency1D[it][ie]->GetXaxis()->SetBinLabel(ib, migrationLab.at(ib).c_str());
        fEfficiency1D[it][ie]->SetBinContent(ib, fEfficiency[it][ie]->GetBinContent(ib, ib));
        fEfficiency1D[it][ie]->SetBinError(ib, fEfficiency[it][ie]->GetBinError(ib, ib));
      }
    }

  // Purity Matrix
  for (Int_t it = 0; it < fMHmigration.size(); ++it)
    for (Int_t ie = 0; ie < fMHmigration[it].size(); ++ie) {
      string rep_str = "MHMigrationMap";
      string aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHPurityMap");
      fPurity[it][ie] = (TH2D *)fMHmigration[it][ie]->Clone();
      fPurity[it][ie]->SetName(aux_str.c_str());
      fPurity[it][ie]->Reset();
      for (Int_t ix = 0; ix < fPurity[it][ie]->GetXaxis()->GetNbins() + 1; ++ix) {
        // Get Normalization Factor
        Double_t normFact = 0.;
        for (Int_t iy = 0; iy < fPurity[it][ie]->GetYaxis()->GetNbins() + 1; ++iy)
          normFact += fMHmigration[it][ie]->GetBinContent(ix, iy);
        if (normFact == 0) continue;
        // Scale Content for That Factor
        for (Int_t iy = 0; iy < fPurity[it][ie]->GetYaxis()->GetNbins() + 1; ++iy) {
          Double_t mapsCont = fMHmigration[it][ie]->GetBinContent(ix, iy);
          Double_t mapsErr = fMHmigration[it][ie]->GetBinError(ix, iy);
          mapsCont /= normFact;
          mapsErr /= normFact;
          if (mapsCont > 0) {
            fPurity[it][ie]->SetBinContent(ix, iy, mapsCont);
            fPurity[it][ie]->SetBinError(ix, iy, mapsErr);
          }
        }
      }
      rep_str = "MHMigrationMap";
      aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHNotPurityMap");
      fNotPurity[it][ie] = (TH2D *)fPurity[it][ie]->Clone();
      fNotPurity[it][ie]->SetName(aux_str.c_str());
      for (Int_t ix = 0; ix < fPurity[it][ie]->GetXaxis()->GetNbins() + 1; ++ix) {
        // Remove Non-Diagonal Terms
        for (Int_t iy = 0; iy < fPurity[it][ie]->GetYaxis()->GetNbins() + 1; ++iy) {
          if (ix != iy) fPurity[it][ie]->SetBinContent(ix, iy, 0);
        }
        // Remove Diagonal Terms
        for (Int_t iy = 0; iy < fNotPurity[it][ie]->GetYaxis()->GetNbins() + 1; ++iy) {
          if (ix == iy) fNotPurity[it][ie]->SetBinContent(ix, iy, 0);
        }
      }
      aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHPurity");
      fPurity1D[it][ie] =
          new TH1D(aux_str.c_str(), Form("%s; ; Purity", fMHmigration[it][ie]->GetTitle()),
                   fMHmigration[it][ie]->GetXaxis()->GetNbins(), fMHmigration[it][ie]->GetXaxis()->GetXmin(),
                   fMHmigration[it][ie]->GetXaxis()->GetXmax());
      for (Int_t ib = 1; ib <= fPurity1D[it][ie]->GetXaxis()->GetNbins(); ++ib) {
        fPurity1D[it][ie]->GetXaxis()->SetBinLabel(ib, migrationLab.at(ib).c_str());
        fPurity1D[it][ie]->SetBinContent(ib, fPurity[it][ie]->GetBinContent(ib, ib));
        fPurity1D[it][ie]->SetBinError(ib, fPurity[it][ie]->GetBinError(ib, ib));
      }
    }

  //	//F1 Matrix
  for (Int_t it = 0; it < fMHmigration.size(); ++it) {
    for (Int_t ie = 0; ie < fMHmigration[it].size(); ++ie) {
      string rep_str = "MHMigrationMap";
      string aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHF1Map");
      fF1[it][ie] = (TH2D *)fMHmigration[it][ie]->Clone();
      fF1[it][ie]->SetName(aux_str.c_str());
      fF1[it][ie]->Reset();
      for (Int_t ix = 0; ix < fF1[it][ie]->GetXaxis()->GetNbins() + 1; ++ix)
        for (Int_t iy = 0; iy < fF1[it][ie]->GetYaxis()->GetNbins() + 1; ++iy) {
          if (ix != iy) continue;
          Double_t eff = fEfficiency[it][ie]->GetBinContent(ix, iy);
          Double_t pur = fPurity[it][ie]->GetBinContent(ix, iy);
          if (eff == 0. || pur == 0.) continue;
          Double_t f1 = 2 * (eff * pur) / (eff + pur);
          if (f1 > 0) fF1[it][ie]->SetBinContent(ix, iy, f1);
        }
      aux_str = fMHmigration[it][ie]->GetName();
      aux_str.replace(aux_str.find(rep_str), rep_str.length(), "MHF1");
      fF11D[it][ie] =
          new TH1D(aux_str.c_str(), Form("%s; ; F1", fMHmigration[it][ie]->GetTitle()),
                   fMHmigration[it][ie]->GetXaxis()->GetNbins(), fMHmigration[it][ie]->GetXaxis()->GetXmin(),
                   fMHmigration[it][ie]->GetXaxis()->GetXmax());
      for (Int_t ib = 1; ib <= fF11D[it][ie]->GetXaxis()->GetNbins(); ++ib) {
        fF11D[it][ie]->GetXaxis()->SetBinLabel(ib, migrationLab.at(ib).c_str());
        fF11D[it][ie]->SetBinContent(ib, fF1[it][ie]->GetBinContent(ib, ib));
        fF11D[it][ie]->SetBinError(ib, fF1[it][ie]->GetBinError(ib, ib));
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeSWTrgEffHisto() {
  // Extract trigger efficiency
  for (Int_t ir = 0; ir < fSWTriggerAll.size(); ++ir) {
    TEfficiency efficiency(*fSWTriggerYes[ir], *fSWTriggerAll[ir]);
    efficiency.SetStatisticOption(TEfficiency::kFWilson);
    fSWTriggerEff[ir] = efficiency.CreateGraph();
    fSWTriggerEff[ir]->SetName(Form("SoftwareTriggerEff_Tower%d", ir));
    fSWTriggerEff[ir]->SetTitle(Form("%s; E [GeV]; Efficiency [%]", (ir == 0 ? "TS" : "TL")));
    gROOT->Remove(fSWTriggerEff[ir]);
    fOutputFile->Append(fSWTriggerEff[ir]);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeDiscriminHisto() {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      TEfficiency efficiency(*fDiscrimYes[it][il], *fDiscrimAll[it][il]);
      efficiency.SetStatisticOption(TEfficiency::kFWilson);
      fDiscrimEff[it][il] = efficiency.CreateGraph();
      fDiscrimEff[it][il]->SetName(Form("DiscriminatorEff_Tower%d_Layer%d", it, il));
      fDiscrimEff[it][il]->SetTitle(Form("%s Layer %d; dE [GeV]; Efficiency [%]", (it == 0 ? "TS" : "TL"), il));
      gROOT->Remove(fDiscrimEff[it][il]);
      fOutputFile->Append(fDiscrimEff[it][il]);
    }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizePileUpHisto() {
  for (Int_t ib = 1; ib <= nPILEUP; ++ib) {
    fPileUpCount->GetXaxis()->SetBinLabel(ib, LabelPILEUP[ib - 1].c_str());
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeSPSHisto() {
  // Compute average dE vx (x,y)
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    if (fSPSRecoTower[it]->GetEntries() > 0.)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) fSPSCaldEmap[it][il]->Divide(fSPSRecoTower[it]);
  // Compute average dE vx layer
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    if (fSPSRecoEnergy[it]->GetEntries() > 0.) fSPSAvedE[it]->Scale(1. / fSPSRecoEnergy[it]->GetEntries());
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::NormalizeLHCHisto() {
  // Compute average dE vx (x,y)
  for (Int_t ir = 0; ir < fLHCRecoTower.size(); ++ir)
    if (fLHCRecoTower[ir]->GetEntries() > 0.)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) fLHCCaldEmap[ir][il]->Divide(fLHCRecoTower[ir]);
  // Compute average dE vx layer
  for (Int_t ir = 0; ir < fLHCCaldE.size(); ++ir)
    for (Int_t il = 0; il < fLHCCaldE[ir].size(); ++il)
      if (fLHCCaldE[ir][il]->Integral() > 0.) {
        Int_t bin = fLHCAvedE[ir]->FindBin(il);
        Double_t con = fLHCAvedE[ir]->GetBinContent(bin);
        Double_t err = fLHCAvedE[ir]->GetBinError(bin);
        Double_t nor = fLHCCaldE[ir][il]->Integral();
        fLHCAvedE[ir]->SetBinContent(bin, con / nor);
        fLHCAvedE[ir]->SetBinError(bin, err / nor);
      }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MeanSilDepMapHisto() {
  // Utils::AllocateVector1D(fSilDepMapSum, this->kCalNtower);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    fSilDepMapSum[it] = (TH2D *)fSilDepMap[it]->Clone(Form("SilDepMapSum_%d", it));
    fSilDepMap[it]->Divide(fSilDepMapCounts[it]);

    fSilDepMapLeakInSum[it] = (TH2D *)fSilDepMapLeakIn[it]->Clone(Form("SilDepMapLeakInSum_%d", it));
    fSilDepMapLeakIn[it]->Divide(fSilDepMapLeakInCounts[it]);
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fSilDepMapLayerSum[it][il][iv] =
            (TH2D *)fSilDepMapLayer[it][il][iv]->Clone(Form("SilDepMapSum_%d_%d_%d", it, il, iv));
        fSilDepMapLayer[it][il][iv]->Divide(fSilDepMapLayerCounts[it][il][iv]);

        fSilDepMapLeakInLayerSum[it][il][iv] =
            (TH2D *)fSilDepMapLeakInLayer[it][il][iv]->Clone(Form("SilDepMapLeakInSum_%d_%d_%d", it, il, iv));
        fSilDepMapLeakInLayer[it][il][iv]->Divide(fSilDepMapLeakInLayerCounts[it][il][iv]);
      }
    }
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::MeanSilEneMapHisto() {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    fPhotonSilAverageMap[it] = (TH2D *)fPhotonSilEnergyMap[it]->Clone(Form("PhotonSiliconAverageMap_%d", it));
    fPhotonSilAverageMap[it]->Divide(fPhotonSilCountsMap[it]);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::DrawPosDetHisto() {
  TCanvas *crpd = new TCanvas("crpd", "", 1200, 600);
  crpd->Divide(4, 2);
  for (Int_t it = 0; it < this->kPosNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        crpd->cd(il * this->kPosNview + iv + 1);
        for (Int_t is = 0; is < this->kPosNsample; ++is) {
          const Double_t norm = this->kPosNchannel[it] / fRawPosDet[it][il][iv][is]->GetEntries();
          fRawPosDet[it][il][iv][is]->SetStats(0);
          fRawPosDet[it][il][iv][is]->Scale(norm);
          fRawPosDet[it][il][iv][is]->SetLineColor(is + 1);
          fRawPosDet[it][il][iv][is]->Draw(is == 0 ? "HIST" : "HISTSAME");
        }
        if (this->kArmIndex == 1) {
          Utils::OptimizeHistYRange((TPad *)crpd->GetPad(il * this->kPosNview + iv + 1));
          crpd->Modified();
          crpd->Update();
          for (Int_t ichip = 0; ichip < 12; ++ichip) {
            TLine *line = new TLine();
            line->SetLineColor(kBlack);
            line->SetLineStyle(3);
            Double_t xmin = 32 * ichip;
            Double_t xmax = 32 * ichip;
            line->SetX1(xmin);
            line->SetX2(xmax);
            Double_t ymin = fRawPosDet[it][il][iv][0]->GetMinimum();
            Double_t ymax = fRawPosDet[it][il][iv][0]->GetMaximum();
            line->SetY1(ymin);
            line->SetY2(ymax);
            line->Draw("SAME");
          }
        }
      }
    }
    if (this->kArmIndex == 0)
      crpd->Print(Form("GSOBarTower%d.eps", it));
    else
      crpd->Print("Silicon.eps");
  }
}

/*-------------*/
/*--- sumdE ---*/
/*-------------*/

// NB: Direct copy from EventRec + adaption to rotated detector

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::CalculateSumdE(vector<vector<Double_t>> &cal,
                                                                      vector<Double_t> &sumdE, Bool_t isProEle,
                                                                      Bool_t isRotated) {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    if (isProEle) {
      sumdE[it] = isRotated ? GetNeutronSumdEFromBackSide(it, cal) : GetNeutronSumdE(it, cal);
    } else {
      sumdE[it] = isRotated ? GetPhotonSumdEFromBackSide(it, cal) : GetPhotonSumdE(it, cal);
    }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetPhotonSumdE(Int_t it, vector<vector<Double_t>> &cal) {
  Double_t sumde = 0.;
  for (Int_t il = this->kPhotonFirstLayer; il <= this->kPhotonLastLayer; ++il) {  // Layer 1-12
    if (il < this->kDoubleStepLayer)
      sumde += cal[it][il];
    else
      sumde += 2 * cal[it][il];
  }
  return sumde;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetNeutronSumdE(Int_t it, vector<vector<Double_t>> &cal) {
  Double_t sumde = 0.;
  for (Int_t il = this->kNeutronFirstLayer; il <= this->kNeutronLastLayer; ++il) {  // Layer 2-15
    if (il < this->kDoubleStepLayer)
      sumde += cal[it][il];
    else
      sumde += 2 * cal[it][il];
  }
  return sumde;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetPhotonSumdEFromBackSide(Int_t it,
                                                                                      vector<vector<Double_t>> &cal) {
  Double_t sumde = 0.;
  for (Int_t il = 15; il >= 8; --il) {  // Layer 14-8
    if (il < this->kDoubleStepLayer - 1 || il == this->kCalNlayer - 1)
      sumde += cal[it][il];
    else
      sumde += 2 * cal[it][il];
  }
  return sumde;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetNeutronSumdEFromBackSide(Int_t it,
                                                                                       vector<vector<Double_t>> &cal) {
  Double_t sumde = 0.;
  for (Int_t il = 15; il >= 0; --il) {
    if (il < this->kDoubleStepLayer - 1 || il == this->kCalNlayer - 1)
      sumde += cal[it][il];
    else
      sumde += 2 * cal[it][il];
  }
  return sumde;
}

/*-------------------*/
/*--- L20,L90,L2D ---*/
/*-------------------*/

// NB: Direct copy from EventRec + adaption to rotated detector

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::CalculateL20L90L2D(vector<vector<Double_t>> &cal,
                                                                          vector<Double_t> &L20, vector<Double_t> &L90,
                                                                          vector<Double_t> &L2D, Bool_t isRotated) {
  isRotated ? CalculateL20L90FromBackSide(cal, L20, L90) : CalculateL20L90(cal, L20, L90);
  for (Int_t it = 0; it < this->kCalNtower; ++it) L2D[it] = L90[it] - 0.25 * L20[it];
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::CalculateL20L90(vector<vector<Double_t>> &cal,
                                                                       vector<Double_t> &l20, vector<Double_t> &l90) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /* calculate SumdE */
    Double_t sumde = 0.;
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      if (il < this->kDoubleStepLayer)
        sumde += cal[it][il];
      else
        sumde += 2 * cal[it][il];
    }

    /* Get L20% and L90% */
    Double_t tmp_sum = 0.;
    Double_t old_sum = 0.;
    Double_t tmp_x0 = 0.;
    Double_t old_x0 = 0.;
    Bool_t got_l20 = false;
    Bool_t got_l90 = false;
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      old_sum = tmp_sum;
      old_x0 = tmp_x0;
      tmp_x0 = GetLayerDepth(it, il);

      if (il < this->kDoubleStepLayer)
        tmp_sum += cal[it][il] / sumde;
      else
        tmp_sum += 2 * cal[it][il] / sumde;

      // get L20%
      if (!got_l20 && tmp_sum > this->kL20Thr) {
        if (il > 0)
          l20[it] = Utils::LinearXinterpolation(old_x0, tmp_x0, old_sum, tmp_sum, this->kL20Thr);
        else
          l20[it] = tmp_x0;
        got_l20 = true;
      }

      // get L90%
      if (!got_l90 && tmp_sum > this->kL90Thr) {
        if (il > 0)
          l90[it] = Utils::LinearXinterpolation(old_x0, tmp_x0, old_sum, tmp_sum, this->kL90Thr);
        else
          l90[it] = tmp_x0;
        got_l90 = true;
      }

      if (got_l90 && got_l20) break;
    }  // layer loop
  }    // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetLayerDepth(Int_t it, Int_t il) {
  /* tower dependences kept in case of a more accurate implementation... */
  if (il < this->kDoubleStepLayer)
    return Double_t(il + 1) * this->kSampleStep;
  else
    return Double_t(this->kDoubleStepLayer) * this->kSampleStep +
           Double_t(il + 1 - this->kDoubleStepLayer) * 2 * this->kSampleStep;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::CalculateL20L90FromBackSide(vector<vector<Double_t>> &cal,
                                                                                   vector<Double_t> &l20,
                                                                                   vector<Double_t> &l90) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /* calculate SumdE */
    Double_t sumde = 0.;
    for (Int_t il = this->kCalNlayer - 1; il >= 0; --il) {
      if (il < this->kDoubleStepLayer - 1 || il == this->kCalNlayer - 1)
        sumde += cal[it][il];
      else
        sumde += 2 * cal[it][il];
    }

    /* Get L20% and L90% */
    Double_t tmp_sum = 0.;
    Double_t old_sum = 0.;
    Double_t tmp_x0 = 0.;
    Double_t old_x0 = 0.;
    Bool_t got_l20 = false;
    Bool_t got_l90 = false;
    for (Int_t il = this->kCalNlayer - 1; il >= 0; --il) {
      old_sum = tmp_sum;
      old_x0 = tmp_x0;
      tmp_x0 = GetLayerDepthFromBackSide(it, il);

      if (il < this->kDoubleStepLayer - 1 || il == this->kCalNlayer - 1)
        tmp_sum += cal[it][il] / sumde;
      else
        tmp_sum += 2 * cal[it][il] / sumde;

      // get L20%
      if (!got_l20 && tmp_sum > this->kL20Thr) {
        if (il < this->kCalNlayer - 1)
          l20[it] = Utils::LinearXinterpolation(old_x0, tmp_x0, old_sum, tmp_sum, this->kL20Thr);
        else
          l20[it] = tmp_x0;
        got_l20 = true;
      }

      // get L90%
      if (!got_l90 && tmp_sum > this->kL90Thr) {
        if (il < this->kCalNlayer - 1)
          l90[it] = Utils::LinearXinterpolation(old_x0, tmp_x0, old_sum, tmp_sum, this->kL90Thr);
        else
          l90[it] = tmp_x0;
        got_l90 = true;
      }

      if (got_l90 && got_l20) break;
    }  // layer loop
  }    // tower loop
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::GetLayerDepthFromBackSide(Int_t it, Int_t il) {
  /* swap front with back layers becore computing layer depth */
  Int_t layer = (this->kCalNlayer - 1) - il;

  /* tower dependences kept in case of a more accurate implementation... */
  if (layer <= this->kCalNlayer - this->kDoubleStepLayer)  // 16-11=5
    return this->kSampleStep * Double_t(1 + (2 * layer));
  else
    return this->kSampleStep * Double_t(1 + (this->kCalNlayer - this->kDoubleStepLayer) + layer);
}

/*----------------------------*/
/*--- L90Boundary for Pion ---*/
/*----------------------------*/
// Type 1->I, 2->II
// Tower 0->TS, 1->TL
// Mode 0->90%, 1->85%, 2->95%
template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t CheckRunIIIData<armcal, armrec, arman, armclass>::L90Boundary(Int_t tower, Double_t energy, Int_t type,
                                                                       Int_t mode) {
  Double_t *p0;
  Double_t *p1;
  Double_t *p2;
  Double_t *p3;
  Double_t *p4;

  if (type == 1) {
    /* Type I pi0 */

    // Values for 85% selection efficiency threshold
    Double_t p0_85[] = {4.56482, 0.772699};
    Double_t p1_85[] = {0.00013645, 0.00206078};
    Double_t p2_85[] = {18.9017, 18.747};
    Double_t p3_85[] = {51.7907, 32.7345};
    Double_t p4_85[] = {-0.0102358, -0.0106355};

    // Values for 90% selection efficiency threshold
    Double_t p0_90[] = {3.52502, 77.3101};
    Double_t p1_90[] = {0.000253718, 7.96561e-06};
    Double_t p2_90[] = {19.2238, 19.4391};
    Double_t p3_90[] = {46.1364, 26.3802};
    Double_t p4_90[] = {-0.00910986, -0.00929002};

    // Values for 95% selection efficiency threshold
    Double_t p0_95[] = {0.618053, 0.643958};
    Double_t p1_95[] = {0.0557198, 0.041083};
    Double_t p2_95[] = {18.7894, 18.7704};
    Double_t p3_95[] = {58.3132, 51.1602};
    Double_t p4_95[] = {-0.00956966, -0.0104987};

    switch (mode) {
      case 1:
        p0 = p0_85;
        p1 = p1_85;
        p2 = p2_85;
        p3 = p3_85;
        p4 = p4_85;
        break;
      case 2:
        p0 = p0_95;
        p1 = p1_95;
        p2 = p2_95;
        p3 = p3_95;
        p4 = p4_95;
        break;
      default:
        p0 = p0_90;
        p1 = p1_90;
        p2 = p2_90;
        p3 = p3_90;
        p4 = p4_90;
    }

    return p0[tower] * log(1. + p1[tower] * energy) + p2[tower] + p3[tower] * exp(p4[tower] * energy);
  } else if (type == 2) {
    /* Type II pi0 */

    // Values for 85% selection efficiency threshold
    Double_t p0_85[] = {2.17912, 2.21911};
    Double_t p1_85[] = {1.81191, 1.75501};
    Double_t p2_85[] = {1809.46, 1722.06};

    // Values for 90% selection efficiency threshold
    Double_t p0_90[] = {2.00599, 2.19519};
    Double_t p1_90[] = {5.10412, 2.34534};
    Double_t p2_90[] = {4441.86, 2441.19};

    // Values for 95% selection efficiency threshold
    Double_t p0_95[] = {2.10612, 2.65565};
    Double_t p1_95[] = {5.32962, 0.55918};
    Double_t p2_95[] = {2548.74, 906.015};

    switch (mode) {
      case 1:
        p0 = p0_85;
        p1 = p1_85;
        p2 = p2_85;
        break;
      case 2:
        p0 = p0_95;
        p1 = p1_95;
        p2 = p2_95;
        break;
      default:
        p0 = p0_90;
        p1 = p1_90;
        p2 = p2_90;
    }

    return p0[tower] * log(p1[tower] * energy + p2[tower]);
  }

  UT::Printf(UT::kPrintInfo, "L90Boundary: invalid \"type\" specified\n");
  exit(EXIT_FAILURE);
}

/*-------------------*/
/*--- Main Method ---*/
/*-------------------*/

template <typename armcal, typename armrec, typename arman, typename armclass>
void CheckRunIIIData<armcal, armrec, arman, armclass>::Run() {
  CT::SetRotatedArm(fRotDet);

  SetInputTree();

  fOutputFile = new TFile(fOutputName, "RECREATE");

  MakeHisto();

  /* Level3 Modification */
  DataModification<arman, armrec, armclass> arm_modify;

  /*--- Event Loop ---*/
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  Int_t nentries = fInputTree->GetEntries();
  // UT::Printf(UT::kPrintInfo, "Entries %d \n", nentries);
  if (fNevents > 0) nentries = TMath::Min(nentries, fNevents);
  for (fEntry = 0; fEntry < nentries; ++fEntry) {
    if (fEntry % 1000 == 0 || fEntry == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d ", fEntry);
      fflush(stdout);
    }
    fInputTree->GetEntry(fEntry);
    // UT::Printf(UT::kPrintInfo, "%d ", fEntry);
    fMC = (McEvent *)fInputEv->Get(Form("true_a%d", this->kArmIndex + 1));
    /*if (fSinglehit || fMultihit || fSPS) {
if (!fMC) continue;
TrueParticleList();
}*/
    if (fMC) TrueParticleList();

    fLvl1 = (Level1<armclass> *)fInputEv->Get(Form("lvl1_a%d", this->kArmIndex + 1));
    fLvl2 = (Level2<armclass> *)fInputEv->Get(Form("lvl2_a%d", this->kArmIndex + 1));
    fLvl3 = (Level3<armrec> *)fInputEv->Get(Form("lvl3_a%d", this->kArmIndex + 1));

    // if (!fLvl3) continue;

    if (fModify) arm_modify.Modify(fLvl3);

    FillTriggerHisto();

    if (fSinglehit) FillSingleHitHisto();
    if (fMultihit) FillMultiHitHisto();
    if (fSWTrgEff) FillSWTrgEffHisto();
    if (fAlignment) FillAlignmentHisto();
    if (fHitmap) FillHitmapHisto();
    if (fMass) FillMassHisto();
    if (fDiscrimin) FillDiscriminHisto();
    if (fPileUp) FillPileUpHisto();
    if (fSPS) FillSPSHisto();
    if (fLHC) FillLHCHisto();
    if (fGSOCalib) FillGSOCalibHisto();
    if (fSilicon) FillSiliconHisto();

    // Silicon Study
    if (fPosDet) {
      FillPosDetHisto();
      FillPosDetMaximum();
    }
    if (fSilEn) {
      FillSiliconDepositHisto();
      FillPhotonSiliconAreaHisto();
      FillCorrelationSilEnHisto();
      FillRecoPosEnHisto();
      FillPhotonSiliconEnergyHisto();
    }
    if (fSilLeak) {
      FillSilDepMapHisto();
      FillPhotonSiliconMapHisto();
    }

    ClearEvent();

    // UT::Printf(UT::kPrintInfo, "End Event %d \n", fEntry);
    // fflush(stdout);
  }
  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");

  NormalizeTriggerHisto();

  if (fSinglehit) NormalizeSingleHitHisto();
  if (fMultihit) NormalizeMultiHitHisto();
  if (fSWTrgEff) NormalizeSWTrgEffHisto();
  if (fDiscrimin) NormalizeDiscriminHisto();
  if (fPileUp) NormalizePileUpHisto();
  if (fSPS) NormalizeSPSHisto();
  if (fLHC) NormalizeLHCHisto();

  if (fSilLeak) {
    MeanSilDepMapHisto();
    MeanSilEneMapHisto();
  }

  if (fPosDet) DrawPosDetHisto();

  /* Write to output file */
  WriteToOutput();
  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class CheckRunIIIData<Arm1CalPars, Arm1RecPars, Arm1AnPars, Arm1Params>;
template class CheckRunIIIData<Arm2CalPars, Arm2RecPars, Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
