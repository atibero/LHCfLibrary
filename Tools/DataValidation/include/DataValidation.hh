#ifndef DataValidation_HH
#define DataValidation_HH

#include <TCanvas.h>
#include <TChain.h>
#include <TColor.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TProfile.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class DataValidation : public armclass, public LHCfParams {
 public:
  DataValidation();
  ~DataValidation();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;
  Int_t fInputLevel;

  TString fOutputName;
  TFile *fOutputFile;

  Bool_t fSkewness;

  /*--- Level0/1/2 ---*/
  Level0<armclass> *fLvl0;
  Level0<armclass> *fPed0;
  Level1<armclass> *fLvl1;
  Level2<armclass> *fLvl2;

  /*--- Histograms ---*/
  // Sample signal
  vector<TH1D *> fSample;
  // Sample skewness
  vector<vector<vector<TH1D *> > > fAuxiliary;
  vector<vector<vector<TH1D *> > > fSampleSkew;

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SetInputFile(const Char_t *name) { fInputFile = name; }
  void SetInputLevel(const Int_t level) { fInputLevel = level; }
  void AddSkewness() { fSkewness = true; }

  void Validate();

 private:
  void SetInputTree();
  void MakeHistograms();
  void ClearEvent();
  void EventLoop();
  void PlotHisto();
  void WriteToOutput();
  void CloseFiles();
};
}  // namespace nLHCf
#endif
