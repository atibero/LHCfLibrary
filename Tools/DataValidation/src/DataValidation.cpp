#include "DataValidation.hh"

#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>

#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass>
DataValidation<armclass>::DataValidation() : fFirstRun(-1), fLastRun(-1), fInputLevel(-1), fSkewness(false) {
  fOutputFile = nullptr;
  fInputEv = nullptr;

  fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass>
DataValidation<armclass>::~DataValidation() {
  delete fOutputFile;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armclass>
void DataValidation<armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/cal_run%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void DataValidation<armclass>::MakeHistograms() {
  UT::AllocateVector1D(fSample, this->kPosNsample);

  if (fSkewness) {
    UT::AllocateVector3D(fSampleSkew, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  }

  TString pos_title = this->kArmIndex == Arm1Params::kArmIndex ? "GSO bars" : "Silicon";

  Double_t dep_bin = 500;
  Double_t dep_min = 0.;
  Double_t dep_max = 10000.;

  Double_t skw_bin = 500;
  Double_t skw_min = -10.;
  Double_t skw_max = +10.;

  for (UInt_t is = 0; is < fSample.size(); ++is) {
    fSample[is] =
        new TH1D(Form("Signal_Sample%d", is), Form("Sample %d; Deposit[ADC]; Counts", is), dep_bin, dep_min, dep_max);
  }
  //
  if (fSkewness) {
    Utils::AllocateVector3D(fAuxiliary, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    Utils::AllocateVector3D(fSampleSkew, this->kPosNlayer, this->kPosNview, this->kPosNsample);
    for (UInt_t il = 0; il < fSampleSkew.size(); ++il) {
      for (UInt_t iv = 0; iv < fSampleSkew[il].size(); ++iv) {
        for (UInt_t is = 0; is < fSampleSkew[il][iv].size(); ++is) {
          fAuxiliary[il][iv][is] = new TH1D(Form("Auxiliary_Layer%d_View%d_Sample%d", il, iv, is),
                                            Form("Sample %d - Layer %d - View %d;Strip;Deposit", is, il, iv),
                                            this->kPosNchannel[0], 0, this->kPosNchannel[0]);
          fSampleSkew[il][iv][is] =
              new TH1D(Form("Skewness_Layer%d_View%d_Sample%d", il, iv, is),
                       Form("Sample %d - Layer %d - View %d;Skewness;Counts", is, il, iv), skw_bin, skw_min, skw_max);
        }
      }
    }
  }
}

template <typename armclass>
void DataValidation<armclass>::ClearEvent() {
  fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armclass>
void DataValidation<armclass>::EventLoop() {
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  Int_t nentries = fInputTree->GetEntries();
  for (Int_t ie = 0; ie <= nentries; ++ie) {
    if (ie % 100 == 0 || ie == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d", ie);
      fflush(stdout);
    }
    fInputTree->GetEntry(ie);

    if (fInputLevel == 1) {
      fLvl1 = (Level1<armclass> *)fInputEv->Get(Form("lvl1_a%d", this->kArmIndex + 1));
      if (!fLvl1) {
        UT::Printf(UT::kPrintError, "Level1 not found in event %d\n", ie);
        continue;
      }
      if (!fLvl1->IsBeam()) continue;
    } else {
      fLvl2 = (Level2<armclass> *)fInputEv->Get(Form("lvl2_a%d", this->kArmIndex + 1));
      if (!fLvl2) {
        UT::Printf(UT::kPrintError, "Level2 not found in event %d\n", ie);
        continue;
      }
      if (!fLvl2->IsBeam()) continue;
    }

    Double_t fMinValue = 50.;
    Double_t fMaxValue = 1e6;

    /* Fill standard histograms */
    for (UInt_t it = 0; it < this->kPosNtower; ++it) {
      for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
        for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
          for (UInt_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
            if (it == 0 && il == 2 && iv == 0 && ic == 96) continue;
            for (UInt_t is = 0; is < this->kPosNsample; ++is) {
              Double_t dep =
                  (fInputLevel == 1) ? fLvl1->fPosDet[it][il][iv][ic][is] : fLvl2->fPosDet[it][il][iv][ic][is];
              if (dep > fMinValue && dep < fMaxValue) {
                // printf("%d %d %d %d %d %f\n", il, iv, ic, is, dep);
                fSample[is]->Fill(dep);
              }
            }
          }
        }
      }
    }

    /* Fill additional histograms */
    if (fSkewness) {
      for (UInt_t it = 0; it < this->kPosNtower; ++it) {
        for (UInt_t il = 0; il < this->kPosNlayer; ++il) {
          for (UInt_t iv = 0; iv < this->kPosNview; ++iv) {
            for (UInt_t is = 0; is < this->kPosNsample; ++is) {
              fAuxiliary[il][iv][is]->Reset();
              for (UInt_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
                if (il == 2 && iv == 0 && ic == 96) continue;
                Double_t val =
                    (fInputLevel == 1) ? fLvl1->fPosDet[it][il][iv][ic][is] : fLvl2->fPosDet[it][il][iv][ic][is];
                if (val > fMinValue) fAuxiliary[il][iv][is]->SetBinContent(ic + 1, val);
              }
              Double_t skewness = fAuxiliary[il][iv][is]->GetSkewness();
              fSampleSkew[il][iv][is]->Fill(skewness);
            }
          }
        }
      }
    }

    ClearEvent();
  }

  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");
}

template <typename armclass>
void DataValidation<armclass>::PlotHisto() {
  //
}

template <typename armclass>
void DataValidation<armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void DataValidation<armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-------------*/
/*--- Check ---*/
/*-------------*/
template <typename armclass>
void DataValidation<armclass>::Validate() {
  if (fInputLevel != 1 && fInputLevel != 2) {
    UT::Printf(UT::kPrintError, "Invalid input layer %d\n", fInputLevel);
    exit(EXIT_FAILURE);
  }

  SetInputTree();

  fOutputFile = new TFile(fOutputName, "RECREATE");

  MakeHistograms();

  /* Event Loop */
  EventLoop();

  /* Plot main histograms */
  PlotHisto();

  /* Write to output file */
  WriteToOutput();

  /* Close all files */
  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class DataValidation<Arm1Params>;
template class DataValidation<Arm2Params>;
}  // namespace nLHCf
