#ifndef SiliconAlignment_HH
#define SiliconAlignment_HH

#include <TCanvas.h>
#include <TChain.h>
#include <TEfficiency.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLine.h>
#include <TMarker.h>
#include <TString.h>
#include <TStyle.h>

#include <fstream>

#include "Arm1AnPars.hh"
#include "Arm1CalPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2CalPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "DataModification.hh"
#include "EventCut.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"
#include "PionRec.hh"

using namespace std;

namespace nLHCf {

template <typename armcal, typename armrec, typename arman, typename armclass>
class SiliconAlignment : public armcal, public armrec, public arman, public LHCfParams {
 public:
  SiliconAlignment();
  ~SiliconAlignment();

 private:
  /*--- Input/Output ---*/
  Bool_t fFillVector;
  Bool_t fAlignSilicon;

  TString fInputFile;
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;

  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;
  TTree *fOutputTree;
  ofstream fOfstream;

  Int_t fEntry;
  Int_t fNevents;

  /*--- Level0/1/2 ---*/
  Level3<armrec> *fLvl3;

  /*--- Event Cut ---*/
  EventCut<arman, armrec> fEveCut;

  /*--- Position Variables ---*/
  Short_t fView;             // View
  Short_t fTower;            // Tower array with fields [tower]
  Double_t fPosition[4][2];  // Position array with fields x/y=[view][layer][0], z = [view][layer][1]

  /*--- Fit quality variables ---*/
  Double_t fRedChiMin[2][4][2];  // Minimum chi2/ndof accepted [tower][layer][view]
  Double_t fRedChiMax[2][4][2];  // Maximum chi2/ndof accepted [tower][layer][view]

  /*--- Alignment Variables ---*/
  Double_t fOffset[2][4][2];  // Offset array with fields [tower][layer][view]

  /*--- Alignment Histograms ---*/
  // TODO
  vector<vector<vector<TH1D *>>> fFitAmplitude;
  vector<vector<vector<TH1D *>>> fRedChiSquare;
  vector<vector<vector<TH2D *>>> fRedChiVsFitAmp;
  vector<vector<vector<vector<TH1D *>>>> fRedChiHisto;
  vector<vector<vector<vector<TH1D *>>>> fResidualHisto;
  vector<vector<vector<TH1D *>>> fIterMeanHisto;
  vector<vector<vector<TH1D *>>> fIterRMSHisto;

  /*--- Selection thresholds ---*/
  Int_t fNlayerThr = 4;    // Number of layer
  Double_t fAmpThr = 0.1;  // MeV

  /*--- Fit Parameters ---*/
  Int_t fNIter = 20;            // Total number of iterations
  Double_t fMinChi = 0.05;      // Minimum accepted chi2/ndof
  Double_t fMaxChi = 10.0;      // Maximum accepted chi2/ndof
  Double_t fXYUncFinal = 0.50;  // Final value X/Y uncertainty
  Double_t fXYUncStep = 0.25;   // Decremental step X/Y uncertainty
  Double_t fZUnc = 1.00;        // Decremental step X/Y uncertainty

 public:
  TString GetPathName(TString stringIn);

  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetAlignOption(Bool_t fill, Bool_t iter) {
    fFillVector = fill;
    fAlignSilicon = iter;
  }
  void SetOutputName(const Char_t *name) { fOutputName = name; }

  void Run();

 private:
  void SetInputFile();
  void SetInputTree();
  void GetInputTree();
  void MakeHistoAndTree();
  void MakeHisto();
  void MakeTree();
  void FillVector();
  void AlignSilicon();
  void DrawHisto();

  void SimpleTrackLoop(Int_t iter, Int_t tower, Int_t layer, Int_t view);
  void DoubleTrackLoop(Int_t iter, Int_t tower, Int_t layer, Int_t view, Bool_t cut);

  void ClearEvent();
  void WriteToOutput();
  void CloseFiles();

  Double_t GetPeakArea(Int_t tower, Int_t layer, Int_t view);
};

}  // namespace nLHCf
#endif
