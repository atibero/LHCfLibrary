#include "SiliconAlignment.hh"

#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>
#include <map>

#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
SiliconAlignment<armcal, armrec, arman, armclass>::SiliconAlignment()
    : fFirstRun(-1), fLastRun(-1), fFillVector(false), fAlignSilicon(false) {
  fEntry = 0;

  fOutputFile = nullptr;
  fInputEv = nullptr;

  fInputTree = nullptr;
  fOutputTree = nullptr;

  fLvl3 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
SiliconAlignment<armcal, armrec, arman, armclass>::~SiliconAlignment() {
  delete fOutputFile;
  delete fInputEv;
}

/*---------------------------*/
/*--- Auxiliary functions ---*/
/*---------------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
TString SiliconAlignment<armcal, armrec, arman, armclass>::GetPathName(TString stringIn) {
  TString stringOut = "";

  string s = stringIn.Data();

  size_t i = s.rfind('/', s.length());
  if (i != string::npos) {
    stringOut = s.substr(0, i) + "/";
  }

  return stringOut;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree for LHCfEvents...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname = Form("%s/rec_run%05d.root", fInputDir.Data(), irun);
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::GetInputTree() {
  UT::Printf(UT::kPrintInfo, "Getting input tree for silicon data...");
  fflush(stdout);

  fInputTree = new TChain("SiliconData");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      UT::Printf(UT::kPrintInfo, " File not found %s\n", fInputDir.Data());
      exit(EXIT_FAILURE);
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else {
    /* could not open directory */
    UT::Printf(UT::kPrintError, " Cannot open input file!\n");
    exit(EXIT_FAILURE);
  }

  gROOT->cd();

  fInputTree->SetBranchAddress("View", &fView);
  fInputTree->SetBranchAddress("Tower", &fTower);
  fInputTree->SetBranchAddress("Position", &fPosition[0][0]);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::MakeHistoAndTree() {
  MakeHisto();
  MakeTree();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::MakeHisto() {
  TH1::SetDefaultSumw2();

  Utils::AllocateVector3D(fFitAmplitude, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fRedChiSquare, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fRedChiVsFitAmp, this->kCalNtower, this->kPosNlayer, this->kPosNview);

  Int_t ampBin = 100;
  Double_t ampMin = 0.;
  Double_t ampMax = 10.;

  Int_t chiBin = 100;
  Double_t chiMin = 0.;
  Double_t chiMax = 500.;

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fFitAmplitude[it][il][iv] =
            new TH1D(Form("LorentzAmplitude_%d_%d%s", it, il, iv == 0 ? "x" : "y"),
                     Form("Layer %d%s; Area [GeV]", il, iv == 0 ? "x" : "y"), ampBin, ampMin, ampMax);
        fRedChiSquare[it][il][iv] =
            new TH1D(Form("ReducedChiSquare_%d_%d%s", it, il, iv == 0 ? "x" : "y"),
                     Form("Layer %d%s; #chi^2/ndof", il, iv == 0 ? "x" : "y"), chiBin, chiMin, chiMax);

        fRedChiVsFitAmp[it][il][iv] = new TH2D(Form("RedChiVsFitAmp_%d_%d%s", it, il, iv == 0 ? "x" : "y"),
                                               Form("Layer %d%s; #chi^2/ndof; Area [ADC]", il, iv == 0 ? "x" : "y"),
                                               chiBin, chiMin, chiMax, ampBin, ampMin, ampMax);
      }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::MakeTree() {
  fOutputTree = new TTree("SiliconData", "Data for silicon alignment");
  fOutputTree->Branch("View", &fView, Form("View/S"));
  fOutputTree->Branch("Tower", &fTower, Form("Tower/S"));
  fOutputTree->Branch("Position", &fPosition, Form("Position[%d][%d]/D", this->kPosNlayer, this->kPosNview));
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::FillVector() {
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  arman::SetFiducialBorder(5.);

  fEveCut.SetPhotonCuts("BPTX L2T DISCRIMINATOR POSITION MULTI-HIT");

  Int_t nentries = fInputTree->GetEntries();
  for (fEntry = 0; fEntry < nentries; ++fEntry) {
    if (fEntry % 1000 == 0 || fEntry == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d", fEntry);
      fflush(stdout);
    }
    fInputTree->GetEntry(fEntry);

    fLvl3 = (Level3<armrec> *)fInputEv->Get(Form("lvl3_a%d", this->kArmIndex + 1));

    if (!fLvl3->IsShowerTrg()) {  // Select only shower trigger
      ClearEvent();
      continue;
    }
    if (fLvl3->IsHighEMTrg()) {  // Remove special Type-II trigger
      ClearEvent();
      continue;
    }

    Bool_t isGoodEvent = false;
    Bool_t isGoodTower[this->kCalNtower] = {false};
    Bool_t isGoodView[this->kCalNtower][this->kPosNlayer] = {{false}};
    isGoodEvent = false;
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      isGoodTower[it] = false;
      // Select well contained singlehit events
      if (!fEveCut.CutPhotonEventTower(fLvl3, it))  // General reconstruction quality selection (no PID!)
        continue;

      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        isGoodView[it][iv] = false;
        // Remove events with signal in few layers
        Int_t nlayer = 0;
        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          Double_t amplitude = GetPeakArea(it, il, iv);
          if (amplitude > fAmpThr)  // TODO: Adjust this condition
            ++nlayer;
        }  // Layer Loop
        if (nlayer < fNlayerThr) continue;

        isGoodView[it][iv] = true;
        isGoodTower[it] = true;
        isGoodEvent = true;
      }  // View Loop
    }    // Tower Loop

    if (!isGoodEvent) {
      ClearEvent();
      continue;
    }

    // Fill Tree
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      // Good candidate tower
      // //4 hits in both arm towers
      if (!(isGoodView[it][0] && isGoodView[it][1])) continue;
      // 4 hits in at least one tower
      //        if (!(isGoodView[it][0] || isGoodView[it][1])) continue;
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        // Good candidate layer
        if (!isGoodView[it][iv]) continue;
        // Initialization
        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          fPosition[il][0] = -1e3;
          fPosition[il][1] = -1e3;
        }
        // Tower - View
        fView = iv;
        fTower = it;
        // Consider good layers
        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          Double_t amplitude = GetPeakArea(it, il, iv);  // Amplitude [GeV] (if table is present)
          if (amplitude > fAmpThr) {
            fPosition[il][0] = fLvl3->fPhotonPosFitPar[it][il][iv][1];
            fPosition[il][1] = CT::kDetectorZpos + this->kPosLayerDepth[il][iv];
            Double_t chi = fLvl3->fPhotonPosFitChi2[it][il][iv];
            Double_t ndf = fLvl3->fPhotonPosFitNDF[it][il][iv];
            Double_t redchi = chi / ndf;
            fFitAmplitude[it][il][iv]->Fill(amplitude);
            fRedChiSquare[it][il][iv]->Fill(redchi);
            fRedChiVsFitAmp[it][il][iv]->Fill(redchi, amplitude);
          }
        }  // Layer Loop
        // Fill variables to tree
        fOutputTree->Fill();
      }  // View Loop
    }    // Tower Loop
    ClearEvent();
  }

  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::AlignSilicon() {
  UT::Printf(UT::kPrintInfo, "Start of silicon alignment\n");

  TH1::SetDefaultSumw2();

  // Build residual histogram
  Utils::AllocateVector4D(fRedChiHisto, this->kCalNtower, this->kPosNlayer, this->kPosNview, fNIter);
  Utils::AllocateVector4D(fResidualHisto, this->kCalNtower, this->kPosNlayer, this->kPosNview, fNIter);
  Utils::AllocateVector3D(fIterMeanHisto, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fIterRMSHisto, this->kCalNtower, this->kPosNlayer, this->kPosNview);

  Int_t chiBin = 500;
  Double_t chiMin = 0.;
  Double_t chiMax = 2. * fMaxChi;

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ii = 0; ii < fNIter; ++ii) {
          fRedChiHisto[it][il][iv][ii] = new TH1D(
              Form("RedChi%s_%d%s_Iter%d", it == 0 ? "TS" : "TL", il, iv == 0 ? "X" : "Y", ii),
              Form("%s - Layer %d - Iter %d; {#chi}^2/ndof", it == 0 ? "TS" : "TL", il, ii, iv == 0 ? "X" : "Y"),
              chiBin, chiMin, chiMax);
        }

  Int_t aliBin = 600;
  Double_t aliMin = -30.;
  Double_t aliMax = +30.;

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ii = 0; ii < fNIter; ++ii) {
          fResidualHisto[it][il][iv][ii] = new TH1D(
              Form("Residual%s_%d%s_Iter%d", it == 0 ? "TS" : "TL", il, iv == 0 ? "X" : "Y", ii),
              Form("%s - Layer %d - Iter %d; #Delta%s [strip]", it == 0 ? "TS" : "TL", il, ii, iv == 0 ? "X" : "Y"),
              aliBin, aliMin, aliMax);
        }

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fIterMeanHisto[it][il][iv] =
            new TH1D(Form("IterMean%s_%d%s", it == 0 ? "TS" : "TL", il, iv == 0 ? "X" : "Y"),
                     Form("%s - Layer %d; Iteration; <Residual> [strip]", it == 0 ? "TS" : "TL", il), fNIter, -0.5,
                     fNIter - 0.5);
        fIterRMSHisto[it][il][iv] =
            new TH1D(Form("IterRMS%s_%d%s", it == 0 ? "TS" : "TL", il, iv == 0 ? "X" : "Y"),
                     Form("%s - Layer %d; Iteration; #sigma_{Residual} [strip]", it == 0 ? "TS" : "TL", il), fNIter,
                     -0.5, fNIter - 0.5);
      }

  // Initialize offset variables
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        fOffset[it][il][iv] = 0.;
      }

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        fRedChiMin[it][il][iv] = fMinChi;
        fRedChiMax[it][il][iv] = fMaxChi;
      }

  fOfstream.open(Form("%sAlignmentResult%d-%d.dat", GetPathName(fOutputName).Data(), fFirstRun, fLastRun));
  fOfstream << "#Iteration\tTower\tLayer\tView\tOffset" << endl;

  /*
   * Separate the two towers
   */
  // Simple procedure but with better performances
  for (Int_t ii = 0; ii < fNIter; ++ii) {
    // UT::Printf(UT::kPrintInfo, "\tIteration %d\n", ii);
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      // UT::Printf(UT::kPrintInfo, "\t\tTower %d\n", it);
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        // UT::Printf(UT::kPrintInfo, "\t\t\tLayer %d\n", il);
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          // UT::Printf(UT::kPrintInfo, "\t\t\t\tView %d\n", iv);
          SimpleTrackLoop(ii, it, il, iv);
        }
      }
    }
  }
  //  // Standard procedure but with worse performances
  //  for (Int_t ii = 0; ii < fNIter; ++ii) {
  //    // UT::Printf(UT::kPrintInfo, "\tIteration %d\n", ii);
  //    for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //      // UT::Printf(UT::kPrintInfo, "\t\tTower %d\n", it);
  //      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //        // UT::Printf(UT::kPrintInfo, "\t\t\tLayer %d\n", il);
  //        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //          if (ii == fNIter - 1) {
  //            fRedChiMin[it][il][iv] = fMinChi;
  //            fRedChiMax[it][il][iv] = fMaxChi;
  //          }
  //          // UT::Printf(UT::kPrintInfo, "\t\t\t\tView %d\n", iv);
  //          if (ii < fNIter - 1)                       // Do not perform this on last iteration
  //            DoubleTrackLoop(ii, it, il, iv, false);  // Compute chi2/ndof limits
  //          DoubleTrackLoop(ii, it, il, iv, true);     // Compute alignment offsets
  //        }
  //      }
  //    }
  //  }

  //  /*
  //   * Consider the two towers together
  //   */
  //  // Consider the two towers together
  //  Int_t it = -1e3;  // Dummy tower variable
  //  for (Int_t ii = 0; ii < fNIter; ++ii) {
  //    // UT::Printf(UT::kPrintInfo, "\tIteration %d\n", ii);
  //    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
  //      // UT::Printf(UT::kPrintInfo, "\t\t\tLayer %d\n", il);
  //      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  //        // UT::Printf(UT::kPrintInfo, "\t\t\t\tView %d\n", iv);
  //        SimpleTrackLoop(ii, it, il, iv);
  //      }
  //    }
  //  }

  UT::Printf(UT::kPrintInfo, "\nEnd of silicon alignment\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::DrawHisto() {
  gStyle->SetOptStat(0);

  // Residual mean vs iteration
  TCanvas *cp = new TCanvas("cp", "", 1200, 300);
  cp->Divide(4, 2);
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      cp->cd(1 + il + iv * this->kPosNlayer);
      // Draw reference histogram
      Double_t ymin = 1e+6;
      Double_t ymax = 1e-6;
      for (Int_t it = 0; it < this->kCalNtower; ++it) {
        fIterMeanHisto[it][il][iv]->Draw("HIST");
        Double_t lmin = fIterMeanHisto[it][il][iv]->GetMinimum();
        Double_t lmax = fIterMeanHisto[it][il][iv]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }

      TH1D *h = (TH1D *)fIterMeanHisto[0][il][iv]->Clone();
      h->Draw("HIST");
      h->SetTitle(Form("Layer %d%s", il, iv == 0 ? "X" : "Y"));
      h->SetStats(0);
      h->SetMinimum(0.995 * ymin);
      h->SetMaximum(1.005 * ymax);
      gPad->Modified();
      gPad->Update();

      // Draw Histogram
      for (Int_t it = 0; it < this->kCalNtower; ++it) {
        fIterMeanHisto[it][il][iv]->SetLineColor(1 + it);
        fIterMeanHisto[it][il][iv]->Draw("HISTSAME");
      }
    }
  }
  cp->Print(Form("%sIterativeMean.eps", GetPathName(fOutputName).Data()));

  // Residual rms vs iteration
  TCanvas *ce = new TCanvas("ce", "", 1200, 300);
  ce->Divide(4, 2);
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      ce->cd(1 + il + iv * this->kPosNlayer);
      // Draw reference histogram
      Double_t ymin = 1e+6;
      Double_t ymax = 1e-6;
      for (Int_t it = 0; it < this->kCalNtower; ++it) {
        fIterRMSHisto[it][il][iv]->Draw("HIST");
        Double_t lmin = fIterRMSHisto[it][il][iv]->GetMinimum();
        Double_t lmax = fIterRMSHisto[it][il][iv]->GetMaximum();
        if (lmin < ymin) ymin = lmin;
        if (lmax > ymax) ymax = lmax;
      }

      TH1D *h = (TH1D *)fIterRMSHisto[0][il][iv]->Clone();
      h->Draw("HIST");
      h->SetTitle(Form("Layer %d%s", il, iv == 0 ? "X" : "Y"));
      h->SetStats(0);
      h->SetMinimum(0.995 * ymin);
      h->SetMaximum(1.005 * ymax);
      gPad->Modified();
      gPad->Update();

      // Draw Histogram
      for (Int_t it = 0; it < this->kCalNtower; ++it) {
        fIterRMSHisto[it][il][iv]->SetLineColor(1 + it);
        fIterRMSHisto[it][il][iv]->Draw("HISTSAME");
      }
    }
  }
  ce->Print(Form("%sIterativeRMS.eps", GetPathName(fOutputName).Data()));

  // Residual distribution
  TCanvas *cr = new TCanvas("cr", "", 1200, 300);
  cr->Divide(4, 2);
  for (Int_t ii = 0; ii < fNIter; ++ii) {
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        cr->cd(1 + il + iv * this->kPosNlayer);

        // Draw reference histogram
        Double_t ymin = 1e+6;
        Double_t ymax = 1e-6;
        for (Int_t it = 0; it < this->kCalNtower; ++it) {
          fResidualHisto[it][il][iv][ii]->Draw("HIST");
          Double_t lmin = fResidualHisto[it][il][iv][ii]->GetMinimum();
          Double_t lmax = fResidualHisto[it][il][iv][ii]->GetMaximum();
          if (lmin < ymin) ymin = lmin;
          if (lmax > ymax) ymax = lmax;
        }

        TH1D *h = (TH1D *)fResidualHisto[0][il][iv][ii]->Clone();
        h->Draw("HIST");
        h->SetTitle(Form("Layer %d%s", il, iv == 0 ? "X" : "Y"));
        h->SetStats(0);
        h->SetMinimum(0.995 * ymin);
        h->SetMaximum(1.005 * ymax);
        gPad->Modified();
        gPad->Update();

        // Draw Histogram
        for (Int_t it = 0; it < this->kCalNtower; ++it) {
          fResidualHisto[it][il][iv][ii]->SetLineColor(1 + it);
          fResidualHisto[it][il][iv][ii]->Draw("HISTSAME");
        }

        // Draw Mean
        Double_t xmin = h->GetXaxis()->GetBinLowEdge(1);
        Double_t xmax = h->GetXaxis()->GetBinUpEdge(h->GetXaxis()->GetNbins());
        for (Int_t it = 0; it < this->kCalNtower; ++it) {
          TString text = Form("#splitline{Mean = %.3f}{RMS = %.3f}",  //
                              fResidualHisto[it][il][iv][ii]->GetMean(), fResidualHisto[it][il][iv][ii]->GetRMS());

          Double_t xscale = 0.275 * (xmax - xmin);
          Double_t px = xmax - xscale;
          Double_t yscale = 0.350 * (ymax - ymin) / this->kCalNtower;
          Double_t py = ymax - (it + 1) * yscale;

          TLatex *ltex = new TLatex(px, py, text.Data());
          ltex->SetTextColor(fResidualHisto[it][il][iv][ii]->GetLineColor());
          ltex->SetTextSize(0.075);
          ltex->SetTextFont(42);
          gPad->Update();
          cr->Modified();

          ltex->Draw("SAME");
        }
      }
    }
    cr->Print(Form("%sResidualAtIteration%d.eps", GetPathName(fOutputName).Data(), ii));
  }

  // Reduce chi-square distribution
  TCanvas *cs = new TCanvas("cs", "", 1200, 300);
  cs->Divide(4, 2);
  for (Int_t ii = 0; ii < fNIter; ++ii) {
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {
        cs->cd(1 + il + iv * this->kPosNlayer);

        // Draw reference histogram
        Double_t ymin = 1e+6;
        Double_t ymax = 1e-6;
        for (Int_t it = 0; it < this->kCalNtower; ++it) {
          fRedChiHisto[it][il][iv][ii]->Draw("HIST");
          Double_t lmin = fRedChiHisto[it][il][iv][ii]->GetMinimum();
          Double_t lmax = fRedChiHisto[it][il][iv][ii]->GetMaximum();
          if (lmin < ymin) ymin = lmin;
          if (lmax > ymax) ymax = lmax;
        }

        TH1D *h = (TH1D *)fRedChiHisto[0][il][iv][ii]->Clone();
        h->Draw("HIST");
        h->SetTitle(Form("Layer %d%s", il, iv == 0 ? "X" : "Y"));
        h->SetStats(0);
        h->SetMinimum(0.995 * ymin);
        h->SetMaximum(1.005 * ymax);
        gPad->Modified();
        gPad->Update();

        // Draw Histogram
        for (Int_t it = 0; it < this->kCalNtower; ++it) {
          fRedChiHisto[it][il][iv][ii]->SetLineColor(1 + it);
          fRedChiHisto[it][il][iv][ii]->Draw("HISTSAME");
        }
      }
    }
    cs->Print(Form("%sChisquareAtIteration%d.eps", GetPathName(fOutputName).Data(), ii));
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::SimpleTrackLoop(  //
    Int_t iter, Int_t tower, Int_t layer, Int_t view) {
  if (tower < 0) tower = 0;

  for (Int_t ie = 0; ie < fInputTree->GetEntries(); ++ie) {
    fInputTree->GetEntry(ie);

    // View selection
    if (fView != view) continue;
    // Tower selection
    if (fTower != tower) continue;
    // Layer selection
    if (fPosition[layer][0] < 0 && fPosition[layer][1] < 0) continue;

    Int_t ip = 0;
    TGraphErrors g = TGraphErrors(0);
    TF1 f("f", "[0]+[1]*x", CT::kDetectorZpos, CT::kDetectorZpos + 250.000);
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      // if (il == layer) continue; //Standard way but it does not work here!

      if (fPosition[il][0] >= 0 && fPosition[il][1] >= 0) {
        Double_t r = fPosition[il][0] - fOffset[tower][il][view];
        Double_t z = fPosition[il][1];

        Double_t e = iter >= fNIter / 2 ? fXYUncFinal : fXYUncFinal + fXYUncStep * (fNIter / 2 - iter - 1);  // strip;
        Double_t m = fZUnc;                                                                                  // mm

        g.SetPoint(ip, z, r);
        g.SetPointError(ip, m, e);

        ++ip;
      }
    }
    g.Fit(&f, "QR");

    Double_t redchi = f.GetChisquare() / f.GetNDF();
    fRedChiHisto[tower][layer][view][iter]->Fill(redchi);

    if (redchi < fRedChiMin[tower][layer][view] || redchi > fRedChiMax[tower][layer][view]) {  // Reject bad tracking
      continue;
    }

    Double_t r = fPosition[layer][0];
    Double_t z = fPosition[layer][1];
    Double_t o = fOffset[tower][layer][view];

    Double_t r_fit = f.Eval(z);
    Double_t r_cor = r - o;
    Double_t r_res = r_cor - r_fit;

    fResidualHisto[tower][layer][view][iter]->Fill(r_res);

    // For debug
    /*
        TCanvas cd("cd", "",  1200, 600);
        cd.cd();
        gPad->SetGrid();
        g.SetTitle(Form("At %f: Fit=%f vs Reco=%f-%f=%f -> %f; Z [mm]; Strip", z, r_fit, r, o, r_cor, r_res));
        g.SetMarkerStyle(kFullCircle);
        g.SetMarkerColor(kBlack);
        g.Draw("AP");
        gPad->Modified();
        gPad->Update();
        g.GetXaxis()->SetLimits(141090., 141275.);
        gPad->Modified();
        gPad->Update();
        TMarker m;
        m.SetMarkerStyle(kFullCircle);
        m.SetMarkerColor(kGreen);
        m.SetMarkerSize(2.5);
        m.DrawMarker(fPosition[layer][1], fPosition[layer][0]);
        gPad->Update();
        cd.WaitPrimitive();
        cd.Update();
    */
  }

  Double_t mean = fRedChiHisto[tower][layer][view][iter]->GetMean();
  Double_t rms = fRedChiHisto[tower][layer][view][iter]->GetRMS();

  fRedChiMin[tower][layer][view] = TMath::Max(mean - rms, fMinChi);
  fRedChiMax[tower][layer][view] = TMath::Min(mean + rms, fMaxChi);

  UT::Printf(UT::kPrintInfo, "Update chi2/ndof range[%d][%d][%d] to [%.3f, %.3f]\n",  //
             tower, layer, view, fRedChiMin[tower][layer][view], fRedChiMax[tower][layer][view]);

  fIterMeanHisto[tower][layer][view]->Fill(iter, fResidualHisto[tower][layer][view][iter]->GetMean());
  fIterRMSHisto[tower][layer][view]->Fill(iter, fResidualHisto[tower][layer][view][iter]->GetRMS());

  fOffset[tower][layer][view] += fResidualHisto[tower][layer][view][iter]->GetMean();
  fOfstream << Form("%d\t\t%d\t%d\t%d\t%.3f\t", iter, tower, layer, view, fOffset[tower][layer][view]) << endl;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::DoubleTrackLoop(  //
    Int_t iter, Int_t tower, Int_t layer, Int_t view, Bool_t cut) {
  if (tower < 0) tower = 0;

  for (Int_t ie = 0; ie < fInputTree->GetEntries(); ++ie) {
    fInputTree->GetEntry(ie);

    // View selection
    if (fView != view) continue;
    // Tower selection
    if (fTower != tower) continue;
    // Layer selection
    if (fPosition[layer][0] < 0 && fPosition[layer][1] < 0) continue;

    Int_t ip = 0;
    TGraphErrors g = TGraphErrors(0);
    TF1 f("f", "[0]+[1]*x", CT::kDetectorZpos, CT::kDetectorZpos + 250.000);
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      if (il == layer) continue;  // Standard way but it does not work here!

      if (fPosition[il][0] >= 0 && fPosition[il][1] >= 0) {
        Double_t r = fPosition[il][0] - fOffset[tower][il][view];
        Double_t z = fPosition[il][1];

        Double_t e = iter == fNIter - 1 ? fXYUncFinal : fXYUncFinal + fXYUncStep * (fNIter - 1 - iter - 1);  // strip;
        Double_t m = fZUnc;                                                                                  // mm

        g.SetPoint(ip, z, r);
        g.SetPointError(ip, m, e);

        ++ip;
      }
    }
    g.Fit(&f, "QR");

    Double_t redchi = f.GetChisquare() / f.GetNDF();
    if (!cut) {
      fRedChiHisto[tower][layer][view][iter]->Fill(redchi);
      continue;
    }
    if (redchi < fRedChiMin[tower][layer][view] || redchi > fRedChiMax[tower][layer][view]) continue;

    Double_t r = fPosition[layer][0];
    Double_t z = fPosition[layer][1];
    Double_t o = fOffset[tower][layer][view];

    Double_t r_fit = f.Eval(z);
    Double_t r_cor = r - o;
    Double_t r_res = r_cor - r_fit;

    fResidualHisto[tower][layer][view][iter]->Fill(r_res);

    // For debug
    /*
        TCanvas cd("cd", "",  1200, 600);
        cd.cd();
        gPad->SetGrid();
        g.SetTitle(Form("At %f: Fit=%f vs Reco=%f-%f=%f -> %f; Z [mm]; Strip", z, r_fit, r, o, r_cor, r_res));
        g.SetMarkerStyle(kFullCircle);
        g.SetMarkerColor(kBlack);
        g.Draw("AP");
        gPad->Modified();
        gPad->Update();
        g.GetXaxis()->SetLimits(141090., 141275.);
        gPad->Modified();
        gPad->Update();
        TMarker m;
        m.SetMarkerStyle(kFullCircle);
        m.SetMarkerColor(kGreen);
        m.SetMarkerSize(2.5);
        m.DrawMarker(fPosition[layer][1], fPosition[layer][0]);
        gPad->Update();
        cd.WaitPrimitive();
        cd.Update();
    */
  }

  if (!cut) {
    Double_t mean = fRedChiHisto[tower][layer][view][iter]->GetMean();
    Double_t rms = fRedChiHisto[tower][layer][view][iter]->GetRMS();

    fRedChiMin[tower][layer][view] = TMath::Max(mean - 0.75 * rms, fMinChi);
    fRedChiMax[tower][layer][view] = TMath::Min(mean + 0.75 * rms, fMaxChi);

    UT::Printf(UT::kPrintInfo, "Update chi2/ndof range[%d][%d][%d] to [%.3f, %.3f]\n",  //
               tower, layer, view, fRedChiMin[tower][layer][view], fRedChiMax[tower][layer][view]);

    return;
  }

  fIterMeanHisto[tower][layer][view]->Fill(iter, fResidualHisto[tower][layer][view][iter]->GetMean());
  fIterRMSHisto[tower][layer][view]->Fill(iter, fResidualHisto[tower][layer][view][iter]->GetRMS());
  if (iter < fNIter - 1) {
    fOffset[tower][layer][view] = fResidualHisto[tower][layer][view][iter]->GetMean();
    fOfstream << Form("%d\t\t%d\t%d\t%d\t%.3f\t", iter, tower, layer, view, fOffset[tower][layer][view]) << endl;
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::ClearEvent() {
  fLvl3 = nullptr;
  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();

  fOutputFile->Write();
  if (fFillVector) {
    if (fOutputTree) fOutputTree->Write();
    if (fOfstream.is_open()) fOfstream.close();
  }

  if (fFillVector) {
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          fFitAmplitude[it][il][iv]->Write();
          fRedChiSquare[it][il][iv]->Write();
          fRedChiVsFitAmp[it][il][iv]->Write();
        }
  }

  if (fAlignSilicon) {
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          fIterMeanHisto[it][il][iv]->Write();
          fIterRMSHisto[it][il][iv]->Write();
          for (Int_t ii = 0; ii < fNIter; ++ii) {
            fResidualHisto[it][il][iv][ii]->Write();
            fRedChiHisto[it][il][iv][ii]->Write();
          }
        }
  }

  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Double_t SiliconAlignment<armcal, armrec, arman, armclass>::GetPeakArea(Int_t tower, Int_t layer, Int_t view) {
  // NB: This is the correct integral approach for the signal in the whole space
  //	  It would be better to use the integral in the tower but is complicated
  // Exclude saturate events
  if (fLvl3->fSilSaturated[tower][layer][view][1]) return 0.;
  Double_t amplitude = fLvl3->fPhotonPosFitPar[tower][layer][view][0];
  if (amplitude < 0)  // Exclude bad-fit events
    return 0.;

  Double_t fraction1 = fLvl3->fPhotonPosFitPar[tower][layer][view][2];
  Double_t fraction2 = fLvl3->fPhotonPosFitPar[tower][layer][view][4];
  Double_t fraction3 = 1 - fraction1 - fraction2;
  if (fLvl3->fPhotonPosFitPar[tower][layer][view][3] < 0) fraction1 = -fraction1;
  if (fLvl3->fPhotonPosFitPar[tower][layer][view][5] < 0) fraction2 = -fraction2;
  if (fLvl3->fPhotonPosFitPar[tower][layer][view][6] < 0) fraction3 = -fraction3;

  amplitude *= TMath::Pi() * (fraction1 + fraction2 + fraction3);
  return amplitude;
}

/*-------------------*/
/*--- Main Method ---*/
/*-------------------*/

template <typename armcal, typename armrec, typename arman, typename armclass>
void SiliconAlignment<armcal, armrec, arman, armclass>::Run() {
  fOutputFile = new TFile(fOutputName, "RECREATE");

  if (fFillVector) {  //
    SetInputTree();
    MakeHistoAndTree();
    FillVector();
  }
  if (fAlignSilicon) {  //
    GetInputTree();
    AlignSilicon();
    DrawHisto();
  }

  WriteToOutput();

  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class SiliconAlignment<Arm1CalPars, Arm1RecPars, Arm1AnPars, Arm1Params>;
template class SiliconAlignment<Arm2CalPars, Arm2RecPars, Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
