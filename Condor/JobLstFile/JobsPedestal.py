#!/usr/bin/env python3

import collections

from GlobalFile.GlobVars import *
from GlobalFile.GlobRuns import *

#Condor Collection:
# - projob : Path to the ".sub" file to launch
# - dataid : Tag for the data set to launch on
# - infile : Name of the input file  (alternative to indire)
# - oufile : Name of the output file (alternative to oudire)
# - inname : Name of the input file in     indire
# - ouname : Name of the output file in oudire
# - indire : Name of the input directory  (alternative to inname)
# - oudire : Name of the output directory (alternative to ouname)
# - runlst : List of the group of [first, last] runs to launch on (required by indire)
# - runstp : Step of further subdivision of each group of [first, last] (default null)
# - opttag : String that contains additional options for the executable to be launched 
# - tabdir : Path to Tables directory used in LHCfLibrary 
# - lograd : Radix that composes the name of the log file
# - endtag : Keyword to grep in log file to check if execution successfully completed
#
#INPUT/OUTPUT HANDLING:
#
#    - If you have just an input file simply use BOTH "infile" and "oufile" (NOT YET IMPLEMENTED!)
#        + "runlst" is meaningless in such a case
#    - If you have multiple input files you need to use "indire" (+ eventually "inname")
#        + "runlst" MUST be specified
#            * "runlst" is a bidimensional list, made of multiple [first, last] runs intervals 
#        + Specify "inname" if you want to separately process each SINGLE run in each [first, last] run interval
#          as it is for example the case when you execute Calibrate, Reconstruct [MULTIPLE OUTPUT]
#            * In such a case, it is MANDATORY to specify both "oudire" and "ouname" for the output
#        + Do not specify "inname" if you want to precess TOGHETER each GROUP of [first, last] run intervals
#          as it is for example the case when you execute ConvertMCtoLvl0, Analysis [SINGLE OUTPUT]
#            * In such a case, you can use "runstp" to request additional run group subdivision or not,
#           (i.e. use [first, first+runstp], ... [last-runstp, runstp] instead of [first, last]),
#           as it is for example the case of ConvertMCtoLvl0 and Analysis, respectively, but,
#              in both cases, it is MANDATORY to specify only ONE among "oudire" and "oufile":
#                % Specify "oufile" if you have only a group [first, last] run interval (i.e. len(runlst)=1)
#                  (otherwise for each group of [first, last] run intervals you overwrite the same file!)
#                % Specify "oudire" if you have more than a group [first, last] run intervals (i.e. len(runlst)>1),
#                  eventually accompained by "ouname" if the output is a single file
#                    - If output = single file, it means that the command line option refers to an output file
#                      In such a case, you MUST specify "ouname" accordingly [see later]
#                    - If output = multiple files, it means that the command line option refers to an output directory
#                      In such a case, you must NOT specify "ouname" (it will be placed in "oudire/first-last")
#
#NB: "inname" and "ouname" must have the following format:
#    - If it refers to a single input/output file they must constain "{r}" which will be substited by run number
#      e.g. "run{r:05d}.root" will be considered by the submission script as "runXXXXX.root"
#    - If it refers to a multiple input/output files, it depends on it "runstp" was specified or not
#       + if NOT specified, they must constain "{f}" and "{l}" which will be substited by run number
#            e.g. "run{f:05d}-{l:05d}.root" will be considered by the submission script as "runXXXXX-YYYYY.root"
#       + if YES specified, "inname" must constain "{f}" and "{l}", "ouname" must contain "{t}"
#            e.g. "run{t:05d}.root" will be considered by the submission script as "runZZZZZ.root" with ZZZZZ=last/runstp

joblst_pedestal = []

#CreatePedestalTree

joblst_pedestal.append(Condor(projob = CONDIR + "CreatePedestalTree.sub", \
                     dataid = "LHC2022", \
                     inname = "run{r:05d}.root", \
                     ouname = "ped_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/DATA/LHC2022/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Pedestal/", \
                     runlst = [ \
                               run8181_nominal_groupA, \
                               run8178_nominal_groupB, \
                               run8178_5higher_groupA, \
                               run8178_5higher_groupB, \
                               run8178_5higher_groupC, \
                               run8181_nominal_groupA, \
                     ], \
                     opttag = "--disable-arm1 --histogram --average-pedestal", \
                     lograd = "ped_run", \
		     endtag = "Closing files... Done."))

joblst_pedestal.append(Condor(projob = CONDIR + "CreatePedestalTree.sub", \
                     dataid = "SPS2022", \
                     inname = "run{r:05d}.root", \
                     ouname = "ped_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/DATA/SPS2022/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/Pedestal/", \
                     runlst = [ \
#				sps2022_ele_st_center_150gev, sps2022_ele_lt_center_150gev, \
#				sps2022_ele_st_center_200gev, sps2022_ele_lt_center_200gev, \
#				sps2022_ele_st_center_250gev, sps2022_ele_lt_center_250gev, \
#				sps2022_pro_st_center_150gev, sps2022_pro_lt_center_150gev, \
#				sps2022_pro_st_center_350gev, sps2022_pro_lt_center_350gev, \
				sps2022_ele_st_center_200gev_rotated_1, \
				sps2022_ele_st_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_1, \
				sps2022_ele_lt_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_3, \
		     ], \
                     opttag = "--disable-arm1 --histogram", \
                     lograd = "ped_run", \
		     endtag = "Closing files... Done."))

#ExtractPedestal

joblst_pedestal.append(Condor(projob = CONDIR + "ExtractPedestal.sub", \
                     dataid = "LHC2022Cal", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Pedestal/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PedestalExtracted/", \
                     runlst = [ \
                               run8181_nominal_groupA, \
                               run8178_nominal_groupB, \
                               run8178_5higher_groupA, \
                               run8178_5higher_groupB, \
                               run8178_5higher_groupC, \
                               run8181_nominal_groupA, \
                     ], \
                     opttag = "-a -c -f -d", \
                     lograd = "ped_ext_cal_", \
		     endtag = "Job successfully completed."))

joblst_pedestal.append(Condor(projob = CONDIR + "ExtractPedestal.sub", \
                     dataid = "LHC2022Pos", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Pedestal/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PedestalExtracted/", \
                     runlst = [ \
                               run8181_nominal_groupA, \
                               run8178_nominal_groupB, \
                               run8178_5higher_groupA, \
                               run8178_5higher_groupB, \
                               run8178_5higher_groupC, \
                               run8181_nominal_groupA, \
                     ], \
                     opttag = "-a -p -d", \
                     lograd = "ped_ext_pos_", \
		     endtag = "Job successfully completed."))

joblst_pedestal.append(Condor(projob = CONDIR + "ExtractPedestal.sub", \
                     dataid = "SPS2022Cal", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/Pedestal/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/PedestalExtracted/", \
                     runlst = [ \
				#sps2022_ele_st_center_150gev, sps2022_ele_lt_center_150gev, \
				#sps2022_ele_st_center_200gev, sps2022_ele_lt_center_200gev, \
				#sps2022_ele_st_center_250gev, sps2022_ele_lt_center_250gev, \
				#sps2022_pro_st_center_150gev, sps2022_pro_lt_center_150gev, \
				#sps2022_pro_st_center_350gev, sps2022_pro_lt_center_350gev, \
				sps2022_ele_st_center_200gev_rotated_1, \
				sps2022_ele_st_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_1, \
				sps2022_ele_lt_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_3, \
		     ], \
                     opttag = "-a -c", \
                     lograd = "ped_ext_cal_", \
		     endtag = "Job successfully completed."))

joblst_pedestal.append(Condor(projob = CONDIR + "ExtractPedestal.sub", \
                     dataid = "SPS2022Pos", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/Pedestal/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/PedestalExtracted/", \
                     runlst = [ \
				#sps2022_ele_st_center_150gev, sps2022_ele_lt_center_150gev, \
				#sps2022_ele_st_center_200gev, sps2022_ele_lt_center_200gev, \
				#sps2022_ele_st_center_250gev, sps2022_ele_lt_center_250gev, \
				#sps2022_pro_st_center_150gev, sps2022_pro_lt_center_150gev, \
				#sps2022_pro_st_center_350gev, sps2022_pro_lt_center_350gev, \
				sps2022_ele_st_center_200gev_rotated_1, \
				sps2022_ele_st_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_1, \
				sps2022_ele_lt_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_3, \
		     ], \
                     opttag = "-a -p", \
                     lograd = "ped_ext_pos_", \
		     endtag = "Job successfully completed."))

#CheckPedHistoCal

joblst_pedestal.append(Condor(projob = CONDIR + "CheckPedHistoCal.sub", \
                     dataid = "LHC2022Cal", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PedestalExtracted/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PedestalCheck/", \
                     runlst = [ \
                               run8181_nominal_groupA, \
                               run8178_nominal_groupB, \
                               run8178_5higher_groupA, \
                               run8178_5higher_groupB, \
                               run8178_5higher_groupC, \
                               run8181_nominal_groupA, \
                     ], \
                     opttag = "-t ped_tree -a -d", \
                     lograd = "ped_chk_cal_", \
		     endtag = "Job successfully completed."))

joblst_pedestal.append(Condor(projob = CONDIR + "CheckPedHistoPos.sub", \
                     dataid = "LHC2022Pos", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PedestalExtracted/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PedestalCheck/", \
                     runlst = [ \
                               run8181_nominal_groupA, \
                               run8178_nominal_groupB, \
                               run8178_5higher_groupA, \
                               run8178_5higher_groupB, \
                               run8178_5higher_groupC, \
                               run8181_nominal_groupA, \
                     ], \
                     opttag = "-t ped_tree -a -s -d", \
                     lograd = "ped_chk_pos_", \
		     endtag = "Job successfully completed."))

joblst_pedestal.append(Condor(projob = CONDIR + "CheckPedHistoCal.sub", \
                     dataid = "SPS2022Cal", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/PedestalExtracted/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/PedestalCheck/", \
                     runlst = [ \
				#sps2022_ele_st_center_150gev, sps2022_ele_lt_center_150gev, \
				#sps2022_ele_st_center_200gev, sps2022_ele_lt_center_200gev, \
				#sps2022_ele_st_center_250gev, sps2022_ele_lt_center_250gev, \
				#sps2022_pro_st_center_150gev, sps2022_pro_lt_center_150gev, \
				#sps2022_pro_st_center_350gev, sps2022_pro_lt_center_350gev, \
				sps2022_ele_st_center_200gev_rotated_1, \
				sps2022_ele_st_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_1, \
				sps2022_ele_lt_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_3, \
                     ], \
                     opttag = "-t ped_tree -a", \
                     lograd = "ped_chk_cal_", \
		     endtag = "Job successfully completed."))

joblst_pedestal.append(Condor(projob = CONDIR + "CheckPedHistoPos.sub", \
                     dataid = "SPS2022Pos", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/PedestalExtracted/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/SPS2022/Data/PedestalCheck/", \
                     runlst = [ \
				#sps2022_ele_st_center_150gev, sps2022_ele_lt_center_150gev, \
				#sps2022_ele_st_center_200gev, sps2022_ele_lt_center_200gev, \
				#sps2022_ele_st_center_250gev, sps2022_ele_lt_center_250gev, \
				#sps2022_pro_st_center_150gev, sps2022_pro_lt_center_150gev, \
				#sps2022_pro_st_center_350gev, sps2022_pro_lt_center_350gev, \
				sps2022_ele_st_center_200gev_rotated_1, \
				sps2022_ele_st_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_1, \
				sps2022_ele_lt_center_200gev_rotated_2, \
				sps2022_ele_lt_center_200gev_rotated_3, \
                     ], \
                     opttag = "-t ped_tree -a -s", \
                     lograd = "ped_chk_pos_", \
		     endtag = "Job successfully completed."))

