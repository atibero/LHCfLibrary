#!/usr/bin/env python3

import collections

from GlobalFile.GlobVars import *
from GlobalFile.GlobRuns import *

#Condor Collection:
# - projob : Path to the ".sub" file to launch
# - dataid : Tag for the data set to launch on
# - infile : Name of the input file  (alternative to indire)
# - oufile : Name of the output file (alternative to oudire)
# - inname : Name of the input file in     indire
# - ouname : Name of the output file in oudire
# - indire : Name of the input directory  (alternative to inname)
# - oudire : Name of the output directory (alternative to ouname)
# - runlst : List of the group of [first, last] runs to launch on (required by indire)
# - runstp : Step of further subdivision of each group of [first, last] (default null)
# - opttag : String that contains additional options for the executable to be launched 
# - tabdir : Path to Tables directory used in LHCfLibrary 
# - lograd : Radix that composes the name of the log file
# - endtag : Keyword to grep in log file to check if execution successfully completed
#
#INPUT/OUTPUT HANDLING:
#
#    - If you have just an input file simply use BOTH "infile" and "oufile" (NOT YET IMPLEMENTED!)
#        + "runlst" is meaningless in such a case
#    - If you have multiple input files you need to use "indire" (+ eventually "inname")
#        + "runlst" MUST be specified
#            * "runlst" is a bidimensional list, made of multiple [first, last] runs intervals 
#        + Specify "inname" if you want to separately process each SINGLE run in each [first, last] run interval
#          as it is for example the case when you execute Calibrate, Reconstruct [MULTIPLE OUTPUT]
#            * In such a case, it is MANDATORY to specify both "oudire" and "ouname" for the output
#        + Do not specify "inname" if you want to precess TOGHETER each GROUP of [first, last] run intervals
#          as it is for example the case when you execute ConvertMCtoLvl0, Analysis [SINGLE OUTPUT]
#            * In such a case, you can use "runstp" to request additional run group subdivision or not,
#           (i.e. use [first, first+runstp], ... [last-runstp, runstp] instead of [first, last]),
#           as it is for example the case of ConvertMCtoLvl0 and Analysis, respectively, but,
#              in both cases, it is MANDATORY to specify only ONE among "oudire" and "oufile":
#                % Specify "oufile" if you have only a group [first, last] run interval (i.e. len(runlst)=1)
#                  (otherwise for each group of [first, last] run intervals you overwrite the same file!)
#                % Specify "oudire" if you have more than a group [first, last] run intervals (i.e. len(runlst)>1),
#                  eventually accompained by "ouname" if the output is a single file
#                    - If output = single file, it means that the command line option refers to an output file
#                      In such a case, you MUST specify "ouname" accordingly [see later]
#                    - If output = multiple files, it means that the command line option refers to an output directory
#                      In such a case, you must NOT specify "ouname" (it will be placed in "oudire/first-last")
#
#NB: "inname" and "ouname" must have the following format:
#    - If it refers to a single input/output file they must constain "{r}" which will be substited by run number
#      e.g. "run{r:05d}.root" will be considered by the submission script as "runXXXXX.root"
#    - If it refers to a multiple input/output files, it depends on it "runstp" was specified or not
#       + if NOT specified, they must constain "{f}" and "{l}" which will be substited by run number
#            e.g. "run{f:05d}-{l:05d}.root" will be considered by the submission script as "runXXXXX-YYYYY.root"
#       + if YES specified, "inname" must constain "{f}" and "{l}", "ouname" must contain "{t}"
#            e.g. "run{t:05d}.root" will be considered by the submission script as "runZZZZZ.root" with ZZZZZ=last/runstp

joblst_crdsimu = []

#LHC2022 - Full

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "LHC2022-QGSJet-Nominal-Full", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/QGSJet/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/QGSJet/CalAndRecAndDel/", \
                     runlst = [ \
		     			[   1, 600], 
                     ], \
		     #For Giuseppe machine learning study
                     #opttag = " -c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     #	" -r --disable-arm1 -r --disable-neutron -r --level2 -r -pfull -r -F-8178 -r -S-1", \
		     #Intermediate information reconstruction
                     opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     	" -r --level2cal -r --disable-arm1 -r -pfull -r -F-8178 -r -S-1", \
		     #Minimal information reconstruction
                     #opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     #	" -r --disable-arm1 -r -pfull -r -F-8178 -r -S-1", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "LHC2022-EPOSLHC-Nominal-Full", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC/CalAndRecAndDel/", \
                     runlst = [ \
	                	[   1, 100], \
                     ], \
                      opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     	" -r --disable-arm1 -r -pfull -r -F-8178 -r -S-1", \
					 tabdir = TABDIR + "/Op2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))


#LHC2022 - Fast

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "LHC2022-QGSJet-Nominal-Fast", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/QGSJet/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/QGSJet/CalAndRecAndDel/", \
                     runlst = [ \
				     	[   1, 600], 
                     ], \
		     #For Giuseppe machine learning study
                     #opttag = " -c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     #	" -r --disable-arm1 -r --disable-neutron -r --level2 -r -pfull -r -F-8178 -r -S-1", \
		     #Intermediate information reconstruction
                     #opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     #	" -r --level2cal -r --disable-arm1 -r -pfast -r -F-8178 -r -S-1", \
		     #Minimal information reconstruction
                     opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     	" -r --disable-arm1 -r -pfast -r -F-8178 -r -S-1", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "LHC2022-EPOSLHC-Nominal-Fast", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC/CalAndRecAndDel/", \
                     runlst = [ \
	    	            [   1, 100], \
                     ], \
    		     #Minimal information reconstruction
                     opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     	" -r --disable-arm1 -r -pfast -r -F-8178 -r -S-1", \
					 tabdir = TABDIR + "/Op2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))

#LHC2022 - 4 Hits - Fast

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "LHC2022-EPOSLHC-4Hits-Nominal-Fast", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     #indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC4Hits/Conversion/", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC20224Hits/EPOSLHC4Hits/Conversion/", \
                     #oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC4Hits/CalAndRecAndDel/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC20224Hits/EPOSLHC4Hits/CalAndRecAndDel/", \
                     runlst = [ \
	    	            [   20000, 30000], \
                     ], \
		     #For Giuseppe machine learning study
                     opttag = "-c --average-pedestal -c --iterate-pedestal -c --disable-arm1 -c -F-8178 -c -S-1" + \
                     	" -r --disable-arm1 -r --disable-neutron -r --level2 -r -pfast -r -F-8178 -r -S-1", \
					 tabdir = TABDIR + "/Op2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))

#SPS2022

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "SPS2022-MC", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/Check/SPS2022/DPMJet/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Check/SPS2022/DPMJet/CalAndRecAndDel/", \
                     runlst = [ \
		     	[ 1001, 1005], \
		     	[ 3001, 3005], \
                     ], \
                     opttag = " -c --disable-arm1 -c -F0" + \
                      	" -r --disable-arm1 -r --disable-neutron -r -pfast -r -F0", \
                     #aaaaaaaaopttag = " -c --disable-arm1 -c -F0 -c --average-pedestal" + \
                     #	" -r --disable-arm1 -r --disable-neutron -r --level2cal -r -pfast -r -F0", \
                     #opttag = " -c --disable-arm1 -c -F0 -c --level1cal -c --average-pedestal" + \
                     #	" -r --disable-arm1 -r --disable-neutron -r --level1cal -r --level2cal -r -pfast -r -F0", \
                     tabdir = TABDIR + "/SPS2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))

#Silicon Study

joblst_crdsimu.append(Condor(projob = CONDIR + "CalAndRecAndDel.sub", \
                     dataid = "SPS2022-MC", \
                     inname = "run{r:05d}.root", \
                     ouname = "rec_run{r:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/Study/sumdEToE/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Study/sumdEToE/CalAndRecAndDel/", \
                     runlst = [ \
				# small tower center @  100 GeV
				[   1,    1], \
 				# small tower center @  200 GeV
				[ 101,  101], \
 				# small tower center @  500 GeV
				[ 201,  201], \
 				# small tower center @ 1000 GeV
				[ 301,  301], \
 				# small tower center @ 2000 GeV
				[ 351,  351], \
 				# small tower center @ 3000 GeV
				[ 401,  411], \
				# small tower center @ 6000 GeV
				[ 501,  511], \
				# large tower center @  100 GeV
				[5001, 5001], \
 				# large tower center @  200 GeV
				[5101, 5101], \
 				# large tower center @  500 GeV
				[5201, 5201], \
 				# large tower center @ 1000 GeV
				[5301, 5301], \
 				# large tower center @ 2000 GeV
				[5351, 5351], \
 				# large tower center @ 3000 GeV
				[5401, 5411], \
 				# large tower center @ 6000 GeV
				[5501, 5511] \
                     ], \
                     opttag = " -c --disable-arm1 -c -F0" + \
                      	" -r --disable-arm1 -r --disable-neutron -r -pfast -r -F0", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "crd_run", \
       	      	     endtag = "Closing files... Done."))
