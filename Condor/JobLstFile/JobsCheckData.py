#!/usr/bin/env python3

import collections

from GlobalFile.GlobVars import *
from GlobalFile.GlobRuns import *

#Condor Collection:
# - projob : Path to the ".sub" file to launch
# - dataid : Tag for the data set to launch on
# - infile : Name of the input file  (alternative to indire)
# - oufile : Name of the output file (alternative to oudire)
# - inname : Name of the input file in     indire
# - ouname : Name of the output file in oudire
# - indire : Name of the input directory  (alternative to inname)
# - oudire : Name of the output directory (alternative to ouname)
# - runlst : List of the group of [first, last] runs to launch on (required by indire)
# - runstp : Step of further subdivision of each group of [first, last] (default null)
# - opttag : String that contains additional options for the executable to be launched 
# - tabdir : Path to Tables directory used in LHCfLibrary 
# - lograd : Radix that composes the name of the log file
# - endtag : Keyword to grep in log file to check if execution successfully completed
#
#INPUT/OUTPUT HANDLING:
#
#    - If you have just an input file simply use BOTH "infile" and "oufile" (NOT YET IMPLEMENTED!)
#        + "runlst" is meaningless in such a case
#    - If you have multiple input files you need to use "indire" (+ eventually "inname")
#        + "runlst" MUST be specified
#            * "runlst" is a bidimensional list, made of multiple [first, last] runs intervals 
#        + Specify "inname" if you want to separately process each SINGLE run in each [first, last] run interval
#          as it is for example the case when you execute Calibrate, Reconstruct [MULTIPLE OUTPUT]
#            * In such a case, it is MANDATORY to specify both "oudire" and "ouname" for the output
#        + Do not specify "innae" if you want to precess TOGHETER each GROUP of [first, last] run intervals
#          as it is for example the case when you execute ConvertMCtoLvl0, Analysis [SINGLE OUTPUT]
#            * In such a case, you can use "runstp" to request additional run group subdivision or not,
#           (i.e. use [first, first+runstp], ... [last-runstp, runstp] instead of [first, last]),
#           as it is for example the case of ConvertMCtoLvl0 and Analysis, respectively, but,
#              in both cases, it is MANDATORY to specify only ONE among "oudire" and "oufile":
#                % Specify "oufile" if you have only a group [first, last] run interval (i.e. len(runlst)=1)
#                  (otherwise for each group of [first, last] run intervals you overwrite the same file!)
#                % Specify "oudire" if you have more than a group [first, last] run intervals (i.e. len(runlst)>1),
#                  eventually accompained by "ouname" if the output is a single file
#                    - If output = single file, it means that the command line option refers to an output file
#                      In such a case, you MUST specify "ouname" accordingly [see later]
#                    - If output = multiple files, it means that the command line option refers to an output directory
#                      In such a case, you must NOT specify "ouname" (it will be placed in "oudire/first-last")
#
#NB: "inname" and "ouname" must have the following format:
#    - If it refers to a single input/output file they must constain "{r}" which will be substited by run number
#      e.g. "run{r:05d}.root" will be considered by the submission script as "runXXXXX.root"
#    - If it refers to a multiple input/output files, it depends on it "runstp" was specified or not
#       + if NOT specified, they must constain "{f}" and "{l}" which will be substited by run number
#            e.g. "run{f:05d}-{l:05d}.root" will be considered by the submission script as "runXXXXX-YYYYY.root"
#       + if YES specified, "inname" must constain "{f}" and "{l}", "ouname" must contain "{t}"
#            e.g. "run{t:05d}.root" will be considered by the submission script as "runZZZZZ.root" with ZZZZZ=last/runstp

joblst_chkdata = []

#LHC2022 - Check

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022Check", \
                     ouname = "Check{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Photon/", \
                     #oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Neutron/", \
                     runlst = [ \
                               run8178_nominal_groupA, \
                               #run8178_nominal_groupB, \
                               #run8178_5higher_groupA, \
                               #run8178_5higher_groupB, \
                               #run8178_5higher_groupC, \
                               #run8181_nominal_groupA, \
                     ], \
                     opttag = "--lhc --modify --arm2 -F 8178 -S 1", \
                     #opttag = "--lhc --modify --is-hadron --arm2 -F 8178 -S 1", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "check_run"))

#LHC2022 - GSO Calibration

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022GSOCalibration", \
                     ouname = "DataGSOCalibration{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/GSOCalibration/", \
                     runlst = [ \
                               run8178_nominal_groupA, \
                               #run8178_nominal_groupB, \
                               #run8178_5higher_groupA, \
                               #run8178_5higher_groupB, \
                               #run8178_5higher_groupC, \
                               #run8181_nominal_groupA, \
                     ], \
                     #opttag = "--gsocalib --arm2 -F 8178 -S 1", \
                     opttag = "--modify --gsocalib --arm2 -F 8178 -S 1", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "gso_calib"))

#LHC2022 - Vertex Reconstruction

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022Vertex", \
                     ouname = "Vertex{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Vertex/", \
                     runlst = [ \
                               run8178_nominal_groupA, \
                               #run8178_nominal_groupB, \
                               #run8178_5higher_groupA, \
                               #run8178_5higher_groupB, \
                               #run8178_5higher_groupC, \
                               #run8181_nominal_groupA, \
                     ], \
                     opttag = "--hitmap --arm2 -F 0", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "vertex_run"))

#LHC2022 - Discriminator Reconstruction

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022Discriminator", \
                     ouname = "Discriminator{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Discriminator/", \
                     runlst = [ \
                               run8178_nominal_groupA, \
                               #run8178_nominal_groupB, \
                               #run8178_5higher_groupA, \
                               #run8178_5higher_groupB, \
                               #run8178_5higher_groupC, \
                               #run8181_nominal_groupA, \
                     ], \
                     opttag = "--discriminator --arm2 -F 0", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "discriminator_run"))

#LHC2022 - Discriminator Reconstruction

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022PileUp", \
                     ouname = "PileUp{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/PileUp/", \
                     runlst = [ \
                               run8178_nominal_groupA, \
                               run8178_nominal_groupB, \
                               run8178_5higher_groupA, \
                               run8178_5higher_groupB, \
                               run8178_5higher_groupC, \
                               run8181_nominal_groupA, \
                     ], \
                     opttag = "--pile-up --arm2 -F 0", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "photon_run"))

#LHC2022 - Invariant Mass Shift WITHOUT energy scale correction

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022InvariantMass", \
                     ouname = "InvariantMass{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/InvariantMass/Raw/", \
                     runlst = [ \
                               run8178_nominal_groupA, \
                               run8178_nominal_groupB, \
#                               run8178_5higher_groupA, \
#                               run8178_5higher_groupB, \
#                               run8178_5higher_groupC, \
#                               run8181_nominal_groupA, \
                     ], \
                     opttag = "--mass --arm2 -F 8178 -S 1", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "mass_run"))

#LHC2022 - Invariant Mass Shift WITH energy scale correction

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2022InvariantMassCorrection", \
                     ouname = "InvariantMass{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/Data/InvariantMass/Cor", \
                     runlst = [ \
                               run8178_nominal_groupA, \
#                               run8178_nominal_groupB, \
#                               run8178_5higher_groupA, \
#                               run8178_5higher_groupB, \
#                               run8178_5higher_groupC, \
#                               run8181_nominal_groupA, \
                     ], \
                     opttag = "--mass --modify --arm2 -F 8178 -S 1", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "mass_run"))

#LHC2015 - Vertex Reconstruction

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2015Vertex", \
                     ouname = "Vertex{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/Check/LHC2015/Data/Reconstruction/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Check/LHC2015/Data/Vertex/", \
                     runlst = [ \
			run3855_nominal_group1, \
                     ], \
                     #opttag = "--hitmap --arm2 -F 0", \
                     opttag = "--hitmap --arm2 -F 3855 -S 1", \
                     tabdir = TABDIR + "/Op2015", \
                     lograd = "vertex_run"))

#LHC2015 - Trigger Count

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2015TriggerCountGroup1", \
                     ouname = "Trigger{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2015/data_unified/full/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2015/data_unified/trigger/", \
                     runlst = [ \
						run3855_nominal_group1, \
                     ], \
                     #opttag = "--hitmap --arm2 -F 0", \
                     opttag = "--arm2 -F 3855 -S 1", \
                     tabdir = TABDIR + "/Op2015", \
                     lograd = "vertex_run"))

joblst_chkdata.append(Condor(projob = CONDIR + "CheckRunIIIData.sub", \
                     dataid = "LHC2015TriggerCountGroup2", \
                     ouname = "Trigger{f:05d}-{l:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/reconstructed/LHC2015/data_unified/full/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2015/data_unified/trigger/", \
                     runlst = [ \
						run3855_nominal_group2, \
                     ], \
                     #opttag = "--hitmap --arm2 -F 0", \
                     opttag = "--arm2 -F 3855 -S 2", \
                     tabdir = TABDIR + "/Op2015", \
                     lograd = "vertex_run"))

