#!/usr/bin/env python3

import collections

from GlobalFile.GlobVars import *
from GlobalFile.GlobRuns import *

#Condor Collection:
# - projob : Path to the ".sub" file to launch
# - dataid : Tag for the data set to launch on
# - infile : Name of the input file  (alternative to indire)
# - oufile : Name of the output file (alternative to oudire)
# - inname : Name of the input file in     indire
# - ouname : Name of the output file in oudire
# - indire : Name of the input directory  (alternative to inname)
# - oudire : Name of the output directory (alternative to ouname)
# - runlst : List of the group of [first, last] runs to launch on (required by indire)
# - runstp : Step of further subdivision of each group of [first, last] (default null)
# - opttag : String that contains additional options for the executable to be launched 
# - tabdir : Path to Tables directory used in LHCfLibrary 
# - lograd : Radix that composes the name of the log file
# - endtag : Keyword to grep in log file to check if execution successfully completed
#
#INPUT/OUTPUT HANDLING:
#
#    - If you have just an input file simply use BOTH "infile" and "oufile" (NOT YET IMPLEMENTED!)
#        + "runlst" is meaningless in such a case
#    - If you have multiple input files you need to use "indire" (+ eventually "inname")
#        + "runlst" MUST be specified
#            * "runlst" is a bidimensional list, made of multiple [first, last] runs intervals 
#        + Specify "inname" if you want to separately process each SINGLE run in each [first, last] run interval
#          as it is for example the case when you execute Calibrate, Reconstruct [MULTIPLE OUTPUT]
#            * In such a case, it is MANDATORY to specify both "oudire" and "ouname" for the output
#        + Do not specify "inname" if you want to precess TOGHETER each GROUP of [first, last] run intervals
#          as it is for example the case when you execute ConvertMCtoLvl0, Analysis [SINGLE OUTPUT]
#            * In such a case, you can use "runstp" to request additional run group subdivision or not,
#           (i.e. use [first, first+runstp], ... [last-runstp, runstp] instead of [first, last]),
#           as it is for example the case of ConvertMCtoLvl0 and Analysis, respectively, but,
#              in both cases, it is MANDATORY to specify only ONE among "oudire" and "oufile":
#                % Specify "oufile" if you have only a group [first, last] run interval (i.e. len(runlst)=1)
#                  (otherwise for each group of [first, last] run intervals you overwrite the same file!)
#                % Specify "oudire" if you have more than a group [first, last] run intervals (i.e. len(runlst)>1),
#                  eventually accompained by "ouname" if the output is a single file
#                    - If output = single file, it means that the command line option refers to an output file
#                      In such a case, you MUST specify "ouname" accordingly [see later]
#                    - If output = multiple files, it means that the command line option refers to an output directory
#                      In such a case, you must NOT specify "ouname" (it will be placed in "oudire/first-last")
#
#NB: "inname" and "ouname" must have the following format:
#    - If it refers to a single input/output file they must constain "{r}" which will be substited by run number
#      e.g. "run{r:05d}.root" will be considered by the submission script as "runXXXXX.root"
#    - If it refers to a multiple input/output files, it depends on it "runstp" was specified or not
#       + if NOT specified, they must constain "{f}" and "{l}" which will be substited by run number
#            e.g. "run{f:05d}-{l:05d}.root" will be considered by the submission script as "runXXXXX-YYYYY.root"
#       + if YES specified, "inname" must constain "{f}" and "{l}", "ouname" must contain "{t}"
#            e.g. "run{t:05d}.root" will be considered by the submission script as "runZZZZZ.root" with ZZZZZ=last/runstp

joblst_cnvlvl0 = []

#LHC2022

joblst_cnvlvl0.append(Condor(projob = CONDIR + "ConvertMCtoLvl0WithSubRun.sub", \
                     dataid = "LHC2022-QGSJet-Nominal", \
                     ouname = "run{t:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/simulation/LHC2022/End2End/v1_20240614/Arm2_Center/QGSJET2_04/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/QGSJet/Conversion/", \
                     runlst = [ \
		     	[     1,    600], \
                     ], \
		     ##WARNING: Changing this number may result in strange behavior
		     runstp = 1, \
                     opttag = "--average-pedestal --arm2 -F -8178 -S -1 -k QGSJET2_04 -M", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "cnv_run", \
		     endtag = "Closing output file... Done."))

#LHC2022 - 4 Hits

joblst_cnvlvl0.append(Condor(projob = CONDIR + "ConvertMCtoLvl0WithSubRun.sub", \
                     dataid = "LHC2022-EPOSLHC-4Hits-Nominal", \
                     ouname = "run{t:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/simulation/LHC2022/End2End/v1_mass_4hits/EPOSLHC/link/", \
                     #oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC2022/EPOSLHC4Hits/Conversion/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/LHC20224Hits/EPOSLHC4Hits/Conversion/", \
                     runlst = [ \
		     	[     20000,    30000], \
                     ], \
		     ##WARNING: Changing this number may result in strange behavior
		     runstp = 1, \
                     opttag = "--average-pedestal --arm2 -F -8178 -S -1 -k EPOSLHC -M", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "cnv_run", \
		     endtag = "Closing output file... Done."))

#LHC2015

joblst_cnvlvl0.append(Condor(projob = CONDIR + "ConvertMCtoLvl0.sub", \
                     dataid = "LHC2015-QGSJet", \
                     ouname = "run{t:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/simulation/LHC2015/End2End/qgsjetII04/Arm2/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Check/LHC2015/QGSJet/Conversion/", \
                     runlst = [ \
#		     	[ 10001, 20000], \
#		     	[ 20001, 30000], \
#		     	[ 30001, 40000], \
#		     	[ 40001, 50000], \
#		     	[ 50001, 60000], \
#		     	[ 60001, 70000], \
		     	[ 70001, 80000], \
#		     	[ 80001, 90000], \
#		     	[ 90001,100000], \
#		     	[100001,110000], \
                     ], \
		     #WARNING: Changing this number may result in strange behavior
		     runstp = 100, \
                     opttag = "--arm2 -F 3855 -S 1", \
                     tabdir = TABDIR + "/Op2015", \
                     lograd = "cnv_run", \
		     endtag = "Closing output file... Done."))

#Flat (LHC2015)

joblst_cnvlvl0.append(Condor(projob = CONDIR + "ConvertMCtoLvl0.sub", \
                     dataid = "LHC2015-Flat", \
                     ouname = "run{t:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/simulation/FlatSpectra/DPM/Arm2/Total/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Check/LHC2015/Flat/Conversion/", \
                     runlst = [ \
		     	[    1, 5000], \
		     	#[    1, 16000], \
		     	#[50001, 66000], \
                     ], \
		     #WARNING: Changing this number may result in strange behavior
		     runstp = 100, \
                     opttag = "--arm2 -F 3855 -S -2", \
                     tabdir = TABDIR + "/Op2015", \
                     lograd = "cnv_run", \
		     endtag = "Closing output file... Done."))

joblst_cnvlvl0.append(Condor(projob = CONDIR + "ConvertMCtoLvl0.sub", \
                     dataid = "SPS2022-MC", \
                     ouname = "run{t:06d}.root", \
                     indire = "/storage/gpfs_data/lhcf/simulation/SPS2022/electron/arm2/197.32GeV/", \
                     #indire = "/storage/gpfs_data/lhcf/simulation/SPS2022/electron/arm2/VeG23.791/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Check/SPS2022/DPMJet/Conversion/", \
                     runlst = [ \
			     	[100001,100500], \
			     	#[300001,300500], \
                     ], \
		     #WARNING: Changing this number may result in strange behavior
		     runstp = 100, \
                     #opttag = "--arm2 -F 0 -n end2end.%07d.out --disable-smearing", \
                     #opttag = "--arm2 -F 0 -n end2end.%07d.out", \
                     opttag = "--arm2 -F 0 -n end2end.%07d.out", \
                     tabdir = TABDIR + "/SPS2022", \
                     lograd = "cnv_run", \
		     endtag = "Closing output file... Done."))

#Silicon Study

joblst_cnvlvl0.append(Condor(projob = CONDIR + "ConvertMCtoLvl0.sub", \
                     dataid = "sumdEToE", \
                     ouname = "run{t:05d}.root", \
                     indire = "/storage/gpfs_data/lhcf/simulation/sumdE_to_E/photon/arm2/", \
                     oudire = "/storage/gpfs_data/lhcf/reconstructed/Study/sumdEToE/Conversion/", \
                     runlst = [ \
 				# small tower center @  100 GeV
				[     1,    100], \
 				# small tower center @  200 GeV
				[ 10001,  10100], \
 				# small tower center @  500 GeV
				[ 20001,  20100], \
 				# small tower center @ 1000 GeV
				[ 30001,  30100], \
 				# small tower center @ 2000 GeV
				[ 35001,  35100], \
 				# small tower center @ 3000 GeV
				[ 40001,  41000], \
 				# small tower center @ 6000 GeV
				[ 50001,  51000], \
				# large tower center @  100 GeV
				[500001, 500100], \
 				# large tower center @  200 GeV
				[510001, 510100], \
 				# large tower center @  500 GeV
				[520001, 520100], \
 				# large tower center @ 1000 GeV
				[530001, 530100], \
 				# large tower center @ 2000 GeV
				[535001, 535100], \
 				# large tower center @ 3000 GeV
				[540001, 541000], \
 				# large tower center @ 6000 GeV
				[550001, 551000] \
                     ], \
		     #WARNING: Changing this number may result in strange behavior
		     runstp = 100, \
                     #opttag = "--arm2 -F 0 -n end2end.%07d.out --disable-smearing", \
                     #opttag = "--arm2 -F 0 -n end2end.%07d.out", \
                     opttag = "--arm2 --disable-smearing -n end2end.%06d.out -F 0", \
                     tabdir = TABDIR + "/Op2022", \
                     lograd = "cnv_run", \
		     endtag = "Closing output file... Done."))
