#!/usr/bin/env python3

import collections

from GlobalFile.GlobVars import *
from GlobalFile.GlobRuns import *

from JobLstFile.JobsConvertMCtoLvl0 import *

from JobLstFile.JobsCalibrationMC import *
from JobLstFile.JobsCalibrationData import *

from JobLstFile.JobsReconstructionMC import *
from JobLstFile.JobsReconstructionData import *

from JobLstFile.JobsCalAndRecAndDelMC import *
from JobLstFile.JobsCalAndRecAndDelData import *

from JobLstFile.JobsAnalysisMC import *
from JobLstFile.JobsAnalysisData import *

from JobLstFile.JobsReductionData import *
from JobLstFile.JobsReductionMC import *

from JobLstFile.JobsNeutronMC import *
from JobLstFile.JobsNeutronData import *

from JobLstFile.JobsCheckMC import *
from JobLstFile.JobsCheckData import *

from JobLstFile.JobsPedestal import *

joblst = []

joblst = joblst_cnvlvl0 + \
	 joblst_calsimu + joblst_caldata + \
	 joblst_recsimu + joblst_recdata + \
	 joblst_crdsimu + joblst_crddata + \
	 joblst_anasimu + joblst_anadata + \
	 joblst_reddata + joblst_redsimu + \
	 joblst_neusimu + joblst_neudata + \
	 joblst_chkdata + joblst_chksimu + \
	 joblst_pedestal
