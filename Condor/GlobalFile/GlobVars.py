#!/usr/bin/env python3

import os
import sys
import collections

#Global Variables for internal use
#<These are the only global variables that can be changed by the user>

USERID = os.getlogin() 

BINDIR=""
if USERID=="egensini":
	BINDIR = "/opt/exp_software/lhcf/egensini/LHCfLibrary/build/bin/"
elif USERID=="berti":
	BINDIR = "/opt/exp_software/lhcf/berti/LHCfLibrary/build/bin/"
if BINDIR=="":
	print("Please specify a valid BINDIR path!")
	sys.exit()

TABDIR = "/opt/exp_software/lhcf/LHCfLibrary/Tables/"
CONDIR = "CondorFile/"
LOGHTC = "{d}/log/htc/"
LOGOUT = "{d}/log/out/"
LOGERR = "{d}/log/err/"
#SUCTAG = "Job terminated of its own accord"
SUCTAG = "Normal termination"
EXETAG = "executable = "
ARGTAG = "arguments = "

#Global Variables for .sub file interface

VarBinDir  = "VarBinDir" #Directory where binary executable is
VarInFile  = "VarInFile" #Input file
VarOutFile = "VarOutFile" #Output file
VarInDire  = "VarInDire" #Input directory
VarOutDire = "VarOutDire" #Output directory
VarMinRun  = "VarMinRun" #First run
VarMaxRun  = "VarMaxRun" #Last run
VarOptions = "VarOptions" #Executable options
VarTables  = "VarTables" #Table directory
VarLogHtc  = "VarLogHtc" #Log HTC directory
VarLogOut  = "VarLogOut" #Log out directory
VarLogErr  = "VarLogErr" #Log err directory
VarLogTag  = "VarLogTag" #Log file name

##python <3.7
##Condor Collection
#Condor = collections.namedtuple('Condor', \
#        'projob dataid infile oufile inname ouname indire oudire runlst runstp opttag tabdir lograd endtag')
##Initialization to None of all fields
#Condor.__new__.func_defaults = (None,) * len(Condor._fields)

#python >3.7
#Condor Collection
fields = ('projob', 'dataid', 'infile', 'oufile', 'inname', 'ouname', 'indire', 'oudire', \
	'runlst', 'runstp', 'opttag', 'tabdir', 'lograd', 'endtag')
#Initialization to None of all fields
Condor = collections.namedtuple('Condor', fields, defaults=(None,) * len(fields))

#Condor Collection:
# - projob : Path to the ".sub" file to launch
# - dataid : Tag for the data set to launch on
# - infile : Name of the input file  (alternative to indire)
# - oufile : Name of the output file (alternative to oudire)
# - inname : Name of the input file in 	indire
# - ouname : Name of the output file in oudire
# - indire : Name of the input directory  (alternative to inname)
# - oudire : Name of the output directory (alternative to ouname)
# - runlst : List of the group of [first, last] runs to launch on (required by indire)
# - runstp : Step of further subdivision of each group of [first, last] (default null)
# - opttag : String that contains additional options for the executable to be launched 
# - tabdir : Path to Tables directory used in LHCfLibrary 
# - lograd : Radix that composes the name of the log file
# - endtag : Keyword to grep in log file to check if execution successfully completed
#
#INPUT/OUTPUT HANDLING:
#
#    - If you have just an input file simply use BOTH "infile" and "oufile" (NOT YET IMPLEMENTED!)
#        + "runlst" is meaningless in such a case
#    - If you have multiple input files you need to use "indire" (+ eventually "inname")
#        + "runlst" MUST be specified
#            * "runlst" is a bidimensional list, made of multiple [first, last] runs intervals 
#        + Specify "inname" if you want to separately process each SINGLE run in each [first, last] run interval
#          as it is for example the case when you execute Calibrate, Reconstruct [MULTIPLE OUTPUT]
#            * In such a case, it is MANDATORY to specify both "oudire" and "ouname" for the output
#        + Do not specify "inname" if you want to precess TOGHETER each GROUP of [first, last] run intervals
#          as it is for example the case when you execute ConvertMCtoLvl0, Analysis [SINGLE OUTPUT]
#            * In such a case, you can use "runstp" to request additional run group subdivision or not,
#	       (i.e. use [first, first+runstp], ... [last-runstp, runstp] instead of [first, last]),
#	       as it is for example the case of ConvertMCtoLvl0 and Analysis, respectively, but,
#              in both cases, it is MANDATORY to specify only ONE among "oudire" and "oufile":
#                % Specify "oufile" if you have only a group [first, last] run interval (i.e. len(runlst)=1)
#                  (otherwise for each group of [first, last] run intervals you overwrite the same file!)
#                % Specify "oudire" if you have more than a group [first, last] run intervals (i.e. len(runlst)>1),
#                  eventually accompained by "ouname" if the output is a single file
#                    - If output = single file, it means that the command line option refers to an output file
#                      In such a case, you MUST specify "ouname" accordingly [see later]
#                    - If output = multiple files, it means that the command line option refers to an output directory
#                      In such a case, you must NOT specify "ouname" (it will be placed in "oudire/first-last")
#
#NB: "inname" and "ouname" must have the following format:
#    - If it refers to a single input/output file they must constain "{r}" which will be substited by run number
#      e.g. "run{r:05d}.root" will be considered by the submission script as "runXXXXX.root"
#    - If it refers to a multiple input/output files, it depends on it "runstp" was specified or not
#       + if NOT specified, they must constain "{f}" and "{l}" which will be substited by run number
#     	   e.g. "run{f:05d}-{l:05d}.root" will be considered by the submission script as "runXXXXX-YYYYY.root"
#       + if YES specified, "inname" must constain "{f}" and "{l}", "ouname" must contain "{t}"
#     	   e.g. "run{t:05d}.root" will be considered by the submission script as "runZZZZZ.root" with ZZZZZ=last/runstp
