#!/usr/bin/env python3

##########################################
### Operation 2015
#########################################

#List of analysis runs (including bad runs)

run3851_5higher_groupX=[43321, 43598]
run3855_nominal_group1=[44299, 44472]
run3855_nominal_group2=[44482, 45106]

#List of bad runs to be excluded from the analysis

slaserlst = [	42539, 42652, 42700, 42815, 43011, 43054, 43197, 43316, 43317, 43318, 43319, \
		43345, 43366, 43380, 43393, 43410, 43424, 43440, 43457, 43486, 43518, 43570, \
		43758, 43768, 43779, 43793, 43804, 44356, 44413, 44473, 44536, 44592, 44656, \
		44716, 44777, 44830, 44892, 44947, 45010, 45069, 45090, 45091	]

##########################################
### SPS 2022
#########################################
sps2022_ele_st_center_150gev=[90060, 90063]
sps2022_ele_lt_center_150gev=[90065, 90068]
sps2022_ele_st_center_200gev=[90054, 90057]
sps2022_ele_lt_center_200gev=[90046, 90048]
sps2022_ele_st_center_250gev=[90115, 90117]
sps2022_ele_lt_center_250gev=[90079, 90082]
sps2022_pro_st_center_150gev=[90205, 90205]
sps2022_pro_lt_center_150gev=[90203, 90203]
sps2022_pro_st_center_350gev=[90095, 90098]
sps2022_pro_lt_center_350gev=[90104, 90108]
sps2022_ele_st_center_200gev_rotated_1=[90606, 90606]
sps2022_ele_st_center_200gev_rotated_2=[90613, 90614]
sps2022_ele_lt_center_200gev_rotated_1=[90607, 90607]
sps2022_ele_lt_center_200gev_rotated_2=[90611, 90611]
sps2022_ele_lt_center_200gev_rotated_3=[90633, 90634]

##########################################
### Operation 2022
#########################################

#List of analysis runs (including bad runs)

run8181_nominal_groupA=[80635, 80646]
run8178_nominal_groupA=[80262, 80346]
run8178_nominal_groupB=[80434, 80520]
run8178_5higher_groupA=[80351, 80372]
run8178_5higher_groupB=[80377, 80431]
run8178_5higher_groupC=[80523, 80626]

#List of runs that require silicon realignment

silrealst = [	80392, 80393, 80394, 80395, 80396, 80397, \
		80505, 80506, 80507, \
		80549	]

#List of bad runs to be excluded from the analysis

armonelst = [80398, 80399, 80400, 80508, 80550, 80551]
lowstalst = [80397, 80465, 80506, 80507, 80509, 80549, 80612]
unstablst = [	#During Fill 8178 Nominal Group 1
		80267, 80342, 80343, 80347, 80348, 80349, 80350, \
		#During Fill 8178 5mm Higher Group 1
                80373, 80374, 80375, 80376, 80432, 80433,  \
		#During Fill 8178 Nominal Group 2
                80466, 80467, 80468, 80469, 80470, 80521, 80522, \
		#During Fill 8178 5mm Higher Group 2
                80531, 80532, 80533, 80534, 80535, 80613, 80614, 80615, 80616	]
		
baddaylst = []
baddaylst = baddaylst + slaserlst + armonelst + lowstalst + unstablst

