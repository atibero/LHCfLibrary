#!/usr/bin/env python3

import os
import sys
import fileinput
import random
import time
import subprocess
import shlex
import shutil
import glob
import getopt
import re

from GlobalFile.GlobCondor import *
	
#command line options
proc   = ""
data   = ""
skip   = False

#global variables
condor 	 = None
runlen   = None	
runstp   = None	
isinfile = False
isindire = False
isoufile = False
isoudire = False

def PrnList():
	
	cond_list = "List of available condor submission list:\n"
	for ic in range(0, len(joblst)):
		cond_list += "\t - " + joblst[ic].projob + " on " + joblst[ic].dataid + "\n"
	cond_list + "\n" 
	print(cond_list)
	sys.exit()

def PrnHelp():
	
	help_list = "\n" \
	+ "==========================================================\n" \
	+ "===================== SubmitToCondor =====================\n" \
	+ "==========================================================\n" \
	+ "\n" \
	+ "-p job process that you want to run\n" \
	+ "-d data set on which you want to run\n" \
	+ "-s simply skip non existing runs\n"
	print(help_list)
	PrnList()

def GetArgs():
	
	global proc, data, skip
	
	try: 
		opts, args = getopt.getopt(sys.argv[1:],"p:d:sh",['help'])
	except getopt.GetoptError as e:
		PrnHelp()

	for o, a in opts:
		if o == '--help' or o == "-h":
			PrnHelp()
		elif o == '-p':
		   	proc=a
		elif o == '-d':
		   	data=a
		elif o == '-s':
		   	skip=True
		else:
			PrnHelp()
			
	return proc, data, skip
			
def SetVars():
	
	global proc, data, skip, \
		condor

	prnt_list = "\n" \
		+ "==========================================================\n" \
		+ "===================== SubmitToCondor =====================\n" \
		+ "==========================================================\n" \
		+ "\n" \
		+ "Process: " + proc + " \n" \
		+ "Dataset: " + data + " \n" \
		+ "Skippin: " + str(skip)
		
	print(prnt_list)
	
	njobs = 0
	lcond = []
	for ic in range(0, len(joblst)):
		if proc != "" and proc in joblst[ic].projob :
			if data == joblst[ic].dataid :
				njobs += 1
				lcond.append(ic)
				condor = ic
    	
	# Check if at least a job was found
	if njobs <= 0:
		print("Job \"" + proc + "\" on \"" + data + "\" not found in submission list")
		PrnList()

	# Check if multiple jobs were found
	if njobs >= 2:
		print("Multiple jobs matching \"" + proc + "\" on \"" + data + "\" in submission list:")
		for ic in lcond:
			print(" - " + str(joblst[ic].projob) + " on " + str(joblst[ic].dataid))
		print("Please be more explicit or change the job list files where they are defined")
		PrnList()
		
	prnt_list = "\n=================== Running with the following parameters:\n" \
		+ "\n" \
		+ "Submission script: " + str(joblst[condor].projob) + " \n" \
		+ "Submission set ID: " + str(joblst[condor].dataid) + " \n" \
		+ "Data in  file: " + str(joblst[condor].infile) + " \n" \
		+ "Data out file: " + str(joblst[condor].oufile) + " \n" \
		+ "Data in  name: " + str(joblst[condor].inname) + " \n" \
		+ "Data out name: " + str(joblst[condor].ouname) + " \n" \
		+ "Data in  folder: " + str(joblst[condor].indire) + " \n" \
		+ "Data out folder: " + str(joblst[condor].oudire) + " \n" \
		+ "Run range list: " + str(joblst[condor].runlst) + " \n" \
		+ "Run inter-step: " + str(joblst[condor].runstp) + " \n" \
		+ "Binary options: " + str(joblst[condor].opttag) + " \n" \
		+ "Table folder: " + str(joblst[condor].tabdir) + " \n" \
		+ "Log tag name: " + str(joblst[condor].lograd) + "\n" \
		+ "End tag word: " + str(joblst[condor].endtag) + "\n"

	print(prnt_list)
	
	return proc, data, skip, \
		condor
		
def ChkVars():
	
	global proc, data, skip, \
		condor, \
		runlen, runstp, isinfile, isindire, isoufile, isoudire

	if joblst[condor].runlst == None:
		runlen = 0
	else:
		runlen = len(joblst[condor].runlst)
	
	runstp = joblst[condor].runstp
		
	isinfile = False
	isindire = False
	isoufile = False
	isoudire = False
	
	#Check input
	if joblst[condor].infile == None and joblst[condor].indire == None:
		print("Either Input File or Input Directory must be specified: Exit...")
		sys.exit()		
	if joblst[condor].infile != None and joblst[condor].indire != None:
		print("Cannot simoultaneously specify Input File and Directory: Exit...")
		sys.exit()					
	if joblst[condor].infile != None: #Case 1 : Input File
		isinfile = True
		if joblst[condor].runlst != None:
			print("Cannot simoultaneously specify Input File and Run List: Exit...")
			sys.exit()		
	if joblst[condor].indire != None: #Case 2 : Input Directory
		isindire = True
		if joblst[condor].runlst == None:
			print("Run List must be specified if using Input Directory: Exit...")
			sys.exit()		
		
	#Check output
	if joblst[condor].oufile == None and joblst[condor].oudire == None:
		print("Either Ouput File or Ouput Directory must be specified: Exit...")
		sys.exit()		
	if joblst[condor].oufile != None and joblst[condor].oudire != None:
		print("Cannot simoultaneously specify Ouput File and Directory: Exit...")
		sys.exit()		
	if joblst[condor].oufile != None: #Case 1 : Output File
		isoufile = True
		if runlen>1:
			print("Cannot specify Output File and Run List larger than single group: Exit...")
			sys.exit()		
	if joblst[condor].oudire != None: #Case 2 : Output Directory
		isoudire = True
		if joblst[condor].runlst == None:
			print("Run List must be specified if using Output Directory: Exit...")
			sys.exit()		
		
	if not isindire:
		print("Not implemented yet: Please do not blame me...")
		sys.exit()
		
	return proc, data, skip, \
		condor, \
		runlen, runstp, isinfile, isindire, isoufile, isoudire

class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("zorro.log", "w")
   
    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        # this flush method is needed for python3 compatibility.
        pass    

def BlockPrint():
	sys.stdout = open(os.devnull, 'w')

def AllowPrint():
	sys.stdout = sys.__stdout__

def Replace(thisFile,textOld,textNew):
	tempFile = open(thisFile+"_tmp", 'wt')
	count = 0
	for line in fileinput.input(thisFile):
		if textOld in line:
			count = count + 1
		tempFile.write( line.replace( textOld, str(textNew) ) )
	tempFile.close()
	shutil.move(thisFile+"_tmp",thisFile)
	return count

def isGoodRun(run):
	if run in slaserlst:
		print("Run \"" + str(run) + "\" is present in the Special LASER Run List")
		return False
	if run in armonelst:
		print("Run \"" + str(run) + "\" is present in the Arm1 Alone Run List")
		return False
	if run in lowstalst:
		print("Run \"" + str(run) + "\" is present in the Low Statistics Run List")
		return False
	if run in unstablst:
		print("Run \"" + str(run) + "\" is present in the Unstable Condition Run List")
		return False
	if run in baddaylst:
		print("Run \"" + str(run) + "\" is present in the General Bad Run List")
		return False
	return True

def isSiliconOk(run):
	if run in silrealst:
		print("Run \"" + str(run) + "\" is present in the Silicon To Realign List")
		return False
	return True

def isSuccessful(htcfile, logfile, endtag, verbose):
	if htcfile == "":
		return True

	#Chcek if htc file exists
	if os.path.exists(htcfile):
		if verbose == -1:
			print("Log File \"" + htcfile + "\" already exists!")
	else:
		if verbose == +1:
			print("...but the htc file \"" + htcfile + "\" does not exist: Reprocess it!")

	#The htc file does not exist
	if not os.path.exists(htcfile):
		return False

	#Search termination string in htc file
	isWellDone = False
	file = open(htcfile, "r")
	for line in file:
		if re.search(SUCTAG, line):
			isWellDone = True
			break
	#The htc file indicates abnormal termination
	if not isWellDone:
		print("...but it had abnormal termination: Reprocess it!")
		return False
	
	#The out file does not exist
	if not os.path.exists(logfile):
		print("...but the log file \"" + logfile + "\" does not exist: Reprocess it!")
		return False
	
	if endtag != None:
		#Search job specific termination string in out file
		isWellDone = False
		file = open(logfile, "r")
		for line in file:
			if re.search(endtag, line):
				isWellDone = True
				break
		#The iout file indicates abnormal termination
		if not isWellDone:
			print("...but termination \"" + endtag + "\" is missing: Reprocess it!")
			return False

	return True

def isProcessed(outfile, htcfile, logfile, endtag):
	if outfile != "" and os.path.exists(outfile):
		print("File \"" + outfile + "\" already exists!")
		return isSuccessful(htcfile, logfile, endtag, +1)
	return False

def isJobRunning(jobfile):
	#Extract executable and arguments variables
	exetag = None
	argtag = None
	file = open(jobfile, "r")
	for line in file:
		if re.search(EXETAG, line):
			if exetag is not None:
				print("Multiple executable matches in jobsub file: Exit...")
				sys.exit()
			exetag = line.replace(EXETAG, '')
		if re.search(ARGTAG, line):
			if argtag is not None:
				print("Multiple arguments matches in jobsub file: Exit...")
				sys.exit()
			argtag = line.replace(ARGTAG, '')	
	if (exetag is None) or (argtag is None):
		print("No match for executable or arguments in jobsub file: Exit...")
		sys.exit()
	#Replace variables in executable and arguments
	var_lst = []
	val_lst = []
	file = open(jobfile, "r")
	for line in file:
	   	if re.search("=", line):
	   		var = line.split("=")[0] #variable
	   		val = line.split("=")[1] #value
	   		var = var.replace(' ', '') #remove empty spaces
	   		if var.isupper(): #consider uppercase variable
	   			var_lst.append("$(" + var + ")")
	   			val_lst.append(val)
	if len(var_lst) != len(val_lst):
		print("Error while replacing constants to compose the command: Exit...")
		sys.exit()
	exetag = exetag.replace('\'', '')  		
	exetag = exetag.replace('\"', '')  		
	argtag = argtag.replace('\'', '')
	argtag = argtag.replace('\"', '')
	comtag = os.path.basename(exetag) + " " + argtag
	for iv in range(0, len(var_lst)):
		comtag = comtag.replace(var_lst[iv], val_lst[iv])
	comtag = comtag.replace('\n', '')  		

	#Check if the command is associated to a job already running
	#command  = 'condor_q -name sn-02.cr.cnaf.infn.it -nobatch -wide | grep -c \"' + comtag + '\"'
	command  = 'condor_q -nobatch -wide | grep -c \"' + comtag + '\"'

	subpipe  = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)	
	out, err = subpipe.communicate()
	if err != None:
		print("Error while checking if the job is already running: Exit...")
		sys.exit()
	grptag = int(out)

	if grptag != 0:
		print("...but job in " + jobfile + " is running: Skip it!\n")
		return True
	
	return False

def main():
	
	global proc, data, skip, \
		condor, \
		runlen, isinfile, isindire, isoufile, isoudire
	
	sys.stdout = Logger()

	GetArgs()
	SetVars()
	ChkVars()
	
	if isindire: #Input name folder was specified
		
		for ig in range(runlen) : #Loop over group of runs
			InFile = joblst[condor].infile
			OutFile = joblst[condor].oufile
			InName = joblst[condor].inname
			OutName = joblst[condor].ouname
			InDire = joblst[condor].indire
			OutDire = joblst[condor].oudire
			MinRun = joblst[condor].runlst[ig][0]
			MaxRun = joblst[condor].runlst[ig][1]
			OptTag = joblst[condor].opttag
			Tables = joblst[condor].tabdir
			LogRad = joblst[condor].lograd
			EndTag = joblst[condor].endtag
			LogHtc = LOGHTC.format(d=joblst[condor].oudire)
			LogOut = LOGOUT.format(d=joblst[condor].oudire)
			LogErr = LOGERR.format(d=joblst[condor].oudire)

			#Check if input/output directories exist
			if InDire != None and not os.path.exists(InDire):
				print("Input folder \"" + InDire + "\" does not exist: Exit...")
				sys.exit()
			if OutDire != None and not os.path.exists(OutDire):
				print("Output folder \"" + OutDire + "\" does not exist: Create it!")
				os.makedirs(OutDire)
			if not os.path.exists(LogHtc):
				os.makedirs(LogHtc)
			if not os.path.exists(LogOut):
				os.makedirs(LogOut)
			if not os.path.exists(LogErr):
				os.makedirs(LogErr)

			if InName!=None: #Input name format was specified
				
				for ir in range(MinRun, MaxRun+1) : #Loop over single runs
				
					Run = ir
					InFile = InDire + "/" + InName.format(r=Run)
					OutFile = OutDire + "/" + OutName.format(r=Run)
					Options = OptTag
					LogTag = LogRad + str(Run)
					HtcFile = LogHtc + "/" + LogTag + ".htc"	
					LogFile = LogOut + "/" + LogTag + ".out"	

					#Check if it is a good acquired run
					if not isGoodRun(Run):
						print("Skipping run \"" + str(Run) + "\" since it is in a bad list\n")
						continue
					if "Calibration" in joblst[condor].projob or "CreatePedestalTree" in joblst[condor].projob:
						if not isSiliconOk(Run):
							print("Correcting run \"" + str(Run) + "\" since it is in silicon list")
							Options = Options + " --realign-arm2"

					#Check if input/output files exist
					if not os.path.exists(InFile):
						print("Input file \"" + InFile + "\" does not exist: Exit...")
						if skip:
							continue
						else:
							sys.exit()
					if isProcessed(OutFile, HtcFile, LogFile, EndTag):
						print("Avoid reprocessing Run " + str(Run) + "\n")
						continue
					
					joborg = joblst[condor].projob
					jobsub = joborg + "." + str(Run)
		
					shutil.copyfile(joborg, jobsub)
			
					Replace(jobsub, VarBinDir, BINDIR) #Directory where binary executable is
					Replace(jobsub, VarInFile, InFile) #Input file	
					Replace(jobsub, VarOutFile, OutFile) #Output file
					Replace(jobsub, VarInDire, InDire) #Input directory	
					Replace(jobsub, VarOutDire, OutDire) #Output directory	
					Replace(jobsub, VarMinRun, str(Run)) #First run	
					Replace(jobsub, VarMaxRun, str(Run)) #Last run	
					Replace(jobsub, VarOptions, Options) #Executable options	
					Replace(jobsub, VarTables, Tables) #Table directory
					Replace(jobsub, VarLogHtc, LogHtc) #Log HTC directory
					Replace(jobsub, VarLogOut, LogOut) #Log out directory
					Replace(jobsub, VarLogErr, LogErr) #Log err directory
					Replace(jobsub, VarLogTag, LogTag) #Log file name
				
					#Check if the job is already running
					if isJobRunning(jobsub):
						os.remove(jobsub)
						time.sleep(1)
						continue

					informa = "Submitting " + joblst[condor].projob \
						   + " on " + joblst[condor].dataid + ":" \
						   + " run " + str(Run) 
					#command = "condor_submit -name sn-02.cr.cnaf.infn.it -spool " + jobsub;
					command = "condor_submit -spool " + jobsub; 
					
					print("\n" + "\t" + informa)
					print("\t" + command + "\n")
					
					subprocess.call(command.split())
					os.remove(jobsub)
		        		
					time.sleep(1)

			else: #Input name format was not specified
				
				if runstp != None: #Intermediate run subdivision is required
				
					ir = MinRun
 
					while ir <= MaxRun:

						InfRun = ir
						SupRun = ir + runstp -1
						TagRun = SupRun // runstp
						ir = SupRun + 1 #Increment here for better visibility

						Options = OptTag
						LogTag = LogRad + str(TagRun)
						HtcFile = LogHtc + "/" + LogTag + ".htc"
						LogFile = LogOut + "/" + LogTag + ".out"	
						SubDire = None

						#Check if input/output files exist
						#if OutFile != None: #File as Output
						#	if isProcessed(OutFile, HtcFile, LogFile, EndTag):
						#		print("Skipping existing Run " + str(InfRun) + "-" + str(SupRun)
						#		continue
						if OutDire != None: #Directory as Output
							if OutName != None: #Single Output: Run-Run File Output
								OutFile = OutDire + "/" + OutName.format(t=TagRun)
								if isProcessed(OutFile, HtcFile, LogFile, EndTag):
									print("Avoid reprocessing Run " + str(InfRun) + "-"+ str(SupRun) + "\n")
									continue						
							else: #Multiple Output: True Directory Output
								SubDire = OutDire + "/" + str(InfRun) + "-" + str(SupRun)
								if not os.path.exists(SubDire):
									print("Output folder \"" + SubDire + "\" does not exist: Create it!")
									os.makedirs(SubDire)
							
						joborg = joblst[condor].projob
						jobsub = joborg + "." + str(TagRun)
						
						shutil.copyfile(joborg, jobsub)
					
						Replace(jobsub, VarBinDir, BINDIR) #Directory where binary executable is
						Replace(jobsub, VarInFile, InFile) #Input file	
						Replace(jobsub, VarOutFile, OutFile) #Output file
						Replace(jobsub, VarInDire, InDire) #Input directory	
						Replace(jobsub, VarOutDire, SubDire) #Output directory	
						Replace(jobsub, VarMinRun, str(InfRun)) #First run	
						Replace(jobsub, VarMaxRun, str(SupRun)) #Last run	
						Replace(jobsub, VarOptions, Options) #Executable options	
						Replace(jobsub, VarTables, Tables) #Table directory
						Replace(jobsub, VarLogHtc, LogHtc) #Log HTC directory
						Replace(jobsub, VarLogOut, LogOut) #Log out directory
						Replace(jobsub, VarLogErr, LogErr) #Log err directory
						Replace(jobsub, VarLogTag, LogTag) #Log file name
					
						#Check if the job is already running
						if isJobRunning(jobsub):
							os.remove(jobsub)
							time.sleep(1)
							continue

						informa = "Submitting " + joblst[condor].projob \
							   + " on " + joblst[condor].dataid + ":" \
							   + " run " + str(InfRun) + "-" + str(SupRun) 
						#command = "condor_submit -name sn-02.cr.cnaf.infn.it -spool " + jobsub; 
						command = "condor_submit -spool " + jobsub; 
							
						print("\n" + "\t" + informa)
						print("\t" + command + "\n")

						subprocess.call(command.split())
						os.remove(jobsub)
						
						time.sleep(1)

				else: #No intermediate run subdivision is required
				
					Options = OptTag
					LogTag = LogRad + str(MinRun) + "_" + str(MaxRun)
					HtcFile = LogHtc + "/" + LogTag + ".htc"	
					LogFile = LogOut + "/" + LogTag + ".out"	
					SubDire = None
					
					#Check if input/output files exist
					if OutFile != None: #File as Output
						if isProcessed(OutFile, HtcFile, LogFile, EndTag):
							print("Skipping existing Run " + str(MinRun) + "-" + str(MaxRun))
							continue
					if OutDire != None: #Directory as Output
						if OutName != None: #Single Output: Run-Run File Output
							OutFile = OutDire + "/" + OutName.format(f=MinRun, l=MaxRun)
							if isProcessed(OutFile, HtcFile, LogFile, EndTag):
								print("Avoid reprocessing Run " + str(MinRun) + "-" + str(MaxRun) + "\n")
								continue						
						else: #Multiple Output: True Directory Output
							SubDire = OutDire + "/" + str(MinRun) + "-" + str(MaxRun)
							if not os.path.exists(SubDire):
								print("Output folder \"" + SubDire + "\" does not exist: Create it!")
								os.makedirs(SubDire)
							if isSuccessful(HtcFile, LogFile, EndTag, -1):
								print("Avoid reprocessing Run " + str(MinRun) + "-" + str(MaxRun) + "\n")
								continue						

					joborg = joblst[condor].projob
					jobsub = joborg + "." + str(MinRun) + "-" + str(MaxRun)
					
					shutil.copyfile(joborg, jobsub)
				
					Replace(jobsub, VarBinDir, BINDIR) #Directory where binary executable is
					Replace(jobsub, VarInFile, InFile) #Input file	
					Replace(jobsub, VarOutFile, OutFile) #Output file
					Replace(jobsub, VarInDire, InDire) #Input directory	
					Replace(jobsub, VarOutDire, SubDire) #Output directory	
					Replace(jobsub, VarMinRun, str(MinRun)) #First run	
					Replace(jobsub, VarMaxRun, str(MaxRun)) #Last run	
					Replace(jobsub, VarOptions, Options) #Executable options	
					Replace(jobsub, VarTables, Tables) #Table directory
					Replace(jobsub, VarLogHtc, LogHtc) #Log HTC directory
					Replace(jobsub, VarLogOut, LogOut) #Log out directory
					Replace(jobsub, VarLogErr, LogErr) #Log err directory
					Replace(jobsub, VarLogTag, LogTag) #Log file name
	
					#Check if the job is already running
					if isJobRunning(jobsub):
						os.remove(jobsub)
						time.sleep(1)
						continue
	
					informa = "Submitting " + joblst[condor].projob \
						   + " on " + joblst[condor].dataid + ":" \
						   + " run " + str(MinRun) + "-" + str(MaxRun) 
					#command = "condor_submit -name sn-02.cr.cnaf.infn.it -spool " + jobsub; 
					command = "condor_submit -spool " + jobsub; 

					print("\n" + "\t" + informa)
					print("\t" + command + "\n")

					subprocess.call(command.split())
					os.remove(jobsub)
					
					time.sleep(1)

if __name__ == '__main__':
        main()

