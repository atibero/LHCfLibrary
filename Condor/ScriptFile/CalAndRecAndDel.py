#!/usr/bin/env python3

import os
import sys
import fileinput
import random
import time
import subprocess
import gzip
import shlex
import shutil
import glob
import getopt
import re

#command line options
ifil = ""
ofil = ""
tlib = ""
copt = ""
ropt = ""
bexe = ""

def BlockPrint():
	sys.stdout = open(os.devnull, 'w')

def AllowPrint():
	sys.stdout = sys.__stdout__

def PrnHelp():

	help_list = "\n" \
		+ "==========================================================\n" \
		+ "===================== CalAndRecAndDel ====================\n" \
		+ "==========================================================\n" \
		+ "\n" \
		+ "-i input file\n" \
		+ "-o output file\n" \
		+ "-t library table\n" \
		+ "-c cal options\n" \
		+ "-r rec options\n"
	print(help_list)
	sys.exit()
	
def GetArgs():
	
	global ifil, ofil, tlib, copt, ropt, bexe
	
	try: 
		opts, args = getopt.getopt(sys.argv[1:],"b:i:o:t:c:r:h",['help'])
	except getopt.getopterror as e:
		PrnHelp()
	
	for o, a in opts:
		if o == '--help' or o == "-h":
			PrnHelp()
		elif o == '-b':
			bexe=a
		elif o == '-i':
		   	ifil=a
		elif o == '-o':
		   	ofil=a
		elif o == '-t':
		   	tlib=a
		elif o == '-c':
		   	copt=copt + " " + a
		elif o == '-r':
		   	ropt=ropt + " " + a
		else:
			PrnHelp()

	prnt_list = "\n" \
		+ "==========================================================\n" \
		+ "===================== ConvertMCtoLvl0 ====================\n" \
		+ "==========================================================\n" \
		+ "\n" \
		+ "Path to executable: " + bexe + " \n" \
		+ "Input file: " + ifil + " \n" \
		+ "Output file: " + ofil + " \n" \
		+ "Library table: " + tlib + " \n" \
		+ "Cal option: " + copt + " \n" \
		+ "Rec option: " + ropt + " \n"
				
	print(prnt_list)
	
	return ifil, ofil, tlib, copt, ropt, bexe

def main():
	
	global ifil, ofil, tlib, copt, ropt, bexe

	BlockPrint()

	GetArgs()

	#Check if input files exist
	if not os.path.exists(ifil):
		print("No such file " + ifil + " found: Exit...")
		sys.exit()					
	#Check if the ouput file already exists
	if os.path.exists(ofil):
		print("File \"" + ofil + "\" already exists: Exit...")
		sys.exit()	
		
	rawname = ifil
	calname = os.path.dirname(ofil) + "/" + os.path.basename(ofil).replace("rec", "cal")
	recname = ofil

	command = ""

	AllowPrint()
	command += bexe + "/Calibrate -i " + rawname + " -o " + calname + " -t " + tlib + " " + copt + "; "
	subprocess.call(command, shell=True)		        					        	
	command += bexe + "/Reconstruct -i " + calname + " -o " + recname + " -t " + tlib + " " + ropt + "; "
	subprocess.call(command, shell=True)		        					        	
	command += "rm -f " + calname
	subprocess.call(command, shell=True)		        					        	

if __name__ == '__main__':
        main()
