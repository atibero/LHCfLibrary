#!/usr/bin/env python3

import os
import sys
import fileinput
import random
import time
import subprocess
import gzip
import shlex
import shutil
import glob
import getopt
import re

#command line options
idir = None
igen = None
ofil = None
frun = None
lrun = None
grun = None
bexe = None
bopt = None

#global constants
innamestd = [ \
		"{dir}/end2end.{run:07d}.out.gz", "{dir}/end2end.arm1.{run:07d}.out.gz", "{dir}/end2end.arm2.{run:07d}.out.gz", \
		"{dir}/end2end.{run:06d}.out.gz", "{dir}/end2end.arm1.{run:06d}.out.gz", "{dir}/end2end.arm2.{run:06d}.out.gz", \
		"{dir}/end2end.{run:05d}.out.gz", "{dir}/end2end.arm1.{run:05d}.out.gz", "{dir}/end2end.arm2.{run:05d}.out.gz" \
		]

fsub = 1
lsub = 100
models = [ "QGSJET2_04", "EPOSLHC" ]
innamealt = [ "{dir}/e2e_a1c_{mod}_{run:06d}_{sub:03d}.out.gz", "{dir}/e2e_a2c_{mod}_{run:06d}_{sub:03d}.out.gz" ]

def BlockPrint():
	sys.stdout = open(os.devnull, 'w')

def AllowPrint():
	sys.stdout = sys.__stdout__

def PrnHelp():

	help_list = "\n" \
		+ "==========================================================\n" \
		+ "===================== ConvertMCtoLvl0 ====================\n" \
		+ "==========================================================\n" \
		+ "\n" \
		+ "-i input directory\n" \
		+ "-o output file\n" \
		+ "-N global run (optional)\n" \
		+ "-f first run\n" \
		+ "-l last run\n" \
		+ "\n" \
		+ "Other options inherited from \"ConvertMCtoLvl0\"\n" 
	print(help_list)
	sys.exit()
	
def GetArgs():
	
	global idir, igen, ofil, frun, lrun, grun, fsub, lsub, bexe, bopt

	try: 
		opts, args = getopt.getopt(sys.argv[1:],"b:i:o:f:l:h:I:F:S:k:n:N:t:m:Mv", \
			['help', 'arm1', 'arm2', 'savelvl2', 'disable-smearing', 'average-pedestal'])
	except getopt.GetoptError as e:
		PrnHelp()
	
	#opts, args = getopt.getopt(sys.argv[1:],"b:i:o:f:l:h:I:F:S:k:n:N:t:mM:v", \
	#		['help', 'arm1', 'arm2', 'savelvl2', 'disable-smearing', 'average-pedestal'])
	bopt = ""
	for o, a in opts:
		if o == '--help' or o == "-h":
			PrnHelp()
		elif o == '-b':
			bexe = a + "/ConvertMCtoLvl0"
		elif o == '-i':
		   	idir=a
		elif o == '-o':
		   	ofil=a
		elif o == '-f':
		   	frun=int(a)
		elif o == '-l':
		   	lrun=int(a)
		elif o == '-N':
		   	grun=int(a)
		if o != '-b' and o != "-f" and o != "-l" and o != "-N":
			if a == "":
				bopt += " " + o
			else:
				bopt += " " + o + " " + a
				if o == '-k':
				   	igen=str(a)
				
	print(str(bopt))
	print(frun)
	print(lrun)
	print(grun)

	prnt_list = "\n" \
		+ "==========================================================\n" \
		+ "===================== ConvertMCtoLvl0 ====================\n" \
		+ "==========================================================\n" \
		+ "\n"
	if bexe is not None:
		prnt_list += "Path to executable: " + bexe + " \n"
	if idir is not None:
		prnt_list += "Input directory: " + idir + " \n"
	if igen is not None:
		prnt_list += "Model name: " + igen + " \n"
	if ofil is not None:
		prnt_list += "Output file: " + ofil + " \n"
	if grun is not None:
		prnt_list += "Global run: " + str(grun) + " \n"
	if frun is not None:
		prnt_list += "First run: " + str(frun) + " \n"
	if lrun is not None:
		prnt_list += "Last run: " + str(lrun) + " \n"
	if bopt is not None:
		prnt_list += "Options: " + str(bopt) + " \n"
				
	print(prnt_list)
	
	return idir, igen, ofil, frun, lrun, grun, fsub, lsub, bexe, bopt

def main():
	
	global idir, igen, ofil, frun, lrun, grun, fsub, lsub, bexe, bopt

	#BlockPrint()

	GetArgs()

	#Check run range list
	if frun is not None and lrun is not None:
		if lrun<frun:
			print("Run range [" + str(frun) + ", " + str(lrun) + "] is in wrong ordering: Exit...")
			sys.exit()					

	#Save the list of input files to be considered
	todolist = []

	if igen is None: 
		#Case 1) Pre-LHC2022 simulations
		bopt += " -f " + str(frun) + " -l " + str(lrun)
		for irun in range(frun, lrun+1):
			isFound = False
			for inam in range(0, len(innamestd)) :
				if isFound:
					break
				filename = innamestd[inam].format(dir=str(idir), run=int(irun))
				if os.path.exists(filename):
					todolist.append(filename)
					isFound = True
	else:
		#Case 2) Post-LHC2022 simulations
		#
	    	#In this case we have two numbers:
	    	#Run: X (we require frun=lrun)
	    	#SubRun: 1-100 (automatically given here)
	    	#
		bopt += " -N " + str(grun)  + " -f " + str(fsub) + " -l " + str(lsub)
		print(bopt)
		for irun in range(fsub, lsub+1):
			isFound = False
			for inam in range(0, len(innamealt)) :
				if isFound:
					break
				filename = innamealt[inam].format(dir=str(idir), mod=str(igen), run=int(grun), sub=int(irun))
				if os.path.exists(filename):
					todolist.append(filename)
					isFound = True

	#Check if input files have been found
	if len(todolist) < 1:
		print("No such run [" + str(frun) + ", " + str(lrun) + "] found in \"" + idir + "\": Exit...")
		sys.exit()					
	#Check if the ouput file already exists
	if os.path.exists(ofil):
		print("File \"" + ofil + "\" already exists: Exit...")
		sys.exit()					
	#Create ouput temporary folder
	tdir = os.path.dirname(ofil) + "/tmp/"
	if not os.path.exists(tdir):
		os.makedirs(tdir)
	
	#Save the list of temporary files to be considered
	templist = []
	for todoname in todolist :
		basename = os.path.splitext(os.path.basename(todoname))[0]
		filename = tdir + "/" + basename
		templist.append(filename)

	#Check if the ouput file already exists
	if len(todolist) != len(templist):
		print("Something were wrong with size mismatch: Exit...")
		sys.exit()			
			
	#Extract compressed files
	for irun in range(0, len(todolist)) :
		with gzip.open(todolist[irun], 'rb') as f_org:
    			with open(templist[irun], 'wb') as f_ext:
            			shutil.copyfileobj(f_org, f_ext)
			        	
	#Convert from MC to Lvl0
	bopt = bopt.replace(idir,tdir)
	command = bexe + bopt
	print(command)
	AllowPrint()
	subprocess.call(command.split())		        					        	
	#BlockPrint()

	#Remove extracted files
	for irun in range(0, len(templist)) :
		command = "rm -f " + templist[irun]
		print(command)
		subprocess.call(command.split())
		
	AllowPrint()

if __name__ == '__main__':
        main()
