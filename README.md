# LHCf Library {#mainpage}

Online documentation is available on [here](http://lhcfs1.isee.nagoya-u.ac.jp/LHCfNewSoftware/doc/index.html)

 ---
## Installation 

How-to in each system like MacOS is available in [hear](doc/INSTALL.md).

### Requirement
- cmake (> ver 2.8.10)
- ROOT either 5 or 6 (6 is recommended)
- git (for developers)

### Get source codes 
- via git  
Firstly you need to register and setup CERN GitLab (https://gitlab.cern.ch/) and then ask to managers, Alessio, Eugenio and Menjo to have a access permission to the repository. 
```
git clone ssh://git@gitlab.cern.ch:7999/atibero/LHCfLibrary.git
git pull
```

- download directly
  https://gitlab.cern.ch/atibero/LHCfLibrary/-/archive/master/LHCfLibrary-master.zip

### How to compile
```
cd build
cmake .. -DLHC2022=true
make
```
- binaries and libraries are generated in "build/bin" and "build/lib", respectively.
- The data format flag must be specified as -D"DATATYPE"=true when you do cmake. The DATATYPES, LHC2015, LHC2016, LHC2022, SPS2015, SPS2021, and SPS2022 are available.
- To use the library in Nagoya cluster (w/ old SLC5), you must add the option -DNO_RCOMPRESSION=true.
- Job scripts for a CPU claster is also available. The cluster name must be specified as -DCLUSTER="CLUSTER NAME". Available CLUSTER NAMEs are ITALY (default), and NAGOYA.


<!-- Commented out the following old descriptions 

### About FindROOT.cmake

FindROOT5.cmake: original from ROOT v5.34.36 (\$ROOTSYS/etc/root/cmake/FindROOT.cmake)
FindROOT6.cmake: original from ROOT v6.08.06 (\$ROOTSYS/etc/cmake/FindROOT.cmake)
FindROOT6_mod.cmake: modified version of FindROOT6.cmake for ROOT 5.xx.xx compatibility



### Make ROOT dictionaries
(this is a workaround, need to implement it in CMake)  
**NOT NEEDED ANYMORE!!!**


```
cd LHCfDict/include
rootcint -f dict.cpp -c LHCfEvent.hh Utils.hh Arm1Params.hh Arm2Params.hh Level2.hh LinkDef.h
mv dict.cpp ../src/

cd LHCfRec/include
rootcint -f dict_rec.cpp -c -I../../LHCfDict/include LHCfCalib.hh LHCfRec.hh LinkDef.h
mv dict_rec.cpp ../src/
```
-->

## Clang formatting
- [Clang](doc/Clang.md)
Before committing it is mandatory to install clang pre-processing routines

## Versions 



## Applications

- [Calibrate](doc/Calibrate.md)  
Gain Calibration and format conversion from raw to Level 2
- [Reconstruct](doc/Reconstruct.md)  
Event Reconstruction
- [Reduction](doc/Reduction.md)  
Convert lvl3 to simple TTree format.


## Tips 

- [How to use LHCf library in Python](doc/PyROOT.md) 
