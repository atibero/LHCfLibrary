#ifndef QuickAnalysis_HH
#define QuickAnalysis_HH

#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TString.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfParams.hh"
#include "Level3.hh"
#include "TGraph.h"
#include "TGraphErrors.h"
//#include "Level1.hh"
//#include "Level2.hh"
#include "LHCfEvent.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class QuickAnalysis : public armclass, public LHCfParams {
 public:
  QuickAnalysis();
  ~QuickAnalysis();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;   // input file name
  TString fInputDir;    // input file directory
  Int_t fRunNumber;     // run number
  Int_t fSubRunNumber;  // subrun number
  TChain *fInputTree;   // input TChain
  LHCfEvent *fInputEv;  // input LHCfEvent
  TString fOutputName;  // output file name
  TFile *fOutputFile;   // output file

  /*--- Level3 ---*/
  Level3<armclass> *fLvl3;  // LvL3 container

  /*--- Histograms ---*/      // 0 small tower, 1 large tower
                              // photon
  TH1D *h_ene_photon[2][7];   // single photon energy
  TH1D *h_pt_photon[2][7];    // single photon pt
  TH2D *h_pos_photon[7];      // single photon hitmap
  TH2D *h_pos_photon_thr[7];  // single photon hitmap (E>1 TeV)
  // neutron
  TH1D *h_ene_neutron[2][7];   // neutron energy
  TH1D *h_pt_neutron[2][7];    // neutron pt
  TH2D *h_pos_neutron[7];      // neutron hitmap
  TH2D *h_pos_neutron_thr[7];  // neutron hitmap (E>1 TeV)
  // pion and Eta
  TH1D *h_mgg[7];         // diphoton invariant mass
  TH2D *h_pos_pi_eta[7];  // type I photon pair hitmap
  TH1D *h_ene_pi[7];      // pi0 (type I) energy
  TH1D *h_ene_eta[7];     // eta (type I) energy
  TH1D *h_pt_pi[7];       // pi0 (type I) pt
  TH1D *h_pt_eta[7];      // eta (type I) pt

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }  // Set input directory
  void SetRunNumber(Int_t nrun) { fRunNumber = nrun; }        // set number of rec_run*****.root
  void SetRunNumber(Int_t nrun, Int_t nsub) {
    fRunNumber = nrun;
    fSubRunNumber = nsub;
  }                                                               // set number of rec_run*****.root
  void SetOutputName(const Char_t *name) { fOutputName = name; }  // set output file name
  void SetInputFile(const Char_t *name) { fInputFile = name; }    // set input file name
  void SetInputTree();                                            // set input tree
  void RunLoop();                                                 // run loop of tree reading and information storing
 private:
  // void SetInputTree();
  void MakeHistograms();              // initialize histograms
  void ClearEvent();                  // empty event containers
  void WriteToOutput();               // write histograms into output file
  void CloseFiles();                  // close files
  Bool_t SelectPhoton(Int_t tower);   // apply photon selection cut
  Bool_t SelectNeutron(Int_t tower);  // apply neutron selection cut
  void FillPiEtaHist();               // Fill diphoton histograms
  void FillPhotonHist(Int_t tower);   // Fill single photon histograms
  void FillNeutronHist(Int_t tower);  // Fill neutron histograms
};
}  // namespace nLHCf
#endif
