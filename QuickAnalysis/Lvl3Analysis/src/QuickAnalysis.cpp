#include "QuickAnalysis.hh"

#include <TH1F.h>
#include <TH2D.h>
#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <chrono>
#include <cstdlib>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "CoordinateTransformation.hh"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "Utils.hh"

using namespace nLHCf;
using namespace std::chrono;
typedef CoordinateTransformation CT;
typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass>
QuickAnalysis<armclass>::QuickAnalysis() : fRunNumber(-1), fSubRunNumber(-1) {
  fOutputFile = nullptr;
  fInputEv = nullptr;
  fLvl3 = nullptr;
  /*fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;*/
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass>
QuickAnalysis<armclass>::~QuickAnalysis() {
  delete fOutputFile;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armclass>
void QuickAnalysis<armclass>::SetInputTree() {
  Int_t itree = 0;
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fRunNumber >= 0) {  // get file n. fRunNumber
    TString iname;
    if (fSubRunNumber >= 0)
      iname = Form("%s/rec_run%05d_%02d.root", fInputDir.Data(), fRunNumber, fSubRunNumber);
    else
      iname = Form("%s/rec_run%05d.root", fInputDir.Data(), fRunNumber);
    TFile ifile(iname.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
    } else {
      ifile.Close();
      fInputTree->Add(iname.Data());
    }
  } else {  // Error allert
    UT::Printf(UT::kPrintError, "Cannot find .root file!\n");
    exit(EXIT_FAILURE);
  }

  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void QuickAnalysis<armclass>::ClearEvent() {
  fLvl3 = nullptr;
  /*fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;*/

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armclass>
void QuickAnalysis<armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "\nSaving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void QuickAnalysis<armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
Bool_t QuickAnalysis<armclass>::SelectPhoton(Int_t tower) {
  // position cut
  const Double_t x = fLvl3->fPhotonPosition[tower][0];
  const Double_t y = fLvl3->fPhotonPosition[tower][1];
  const Double_t edge_low = 2;  // mm
  const Double_t edge_high = fLvl3->kTowerSize[tower] - 2;
  // single-hit cut
  const Int_t nhits = (fLvl3->fSoftwareTrigger[tower] ? 1 : 0) * (fLvl3->fPhotonMultiHit[tower] ? 2 : 1);
  // energy cut
  const Double_t e = fLvl3->fPhotonEnergyLin[tower];
  // PID
  const Double_t l90 = fLvl3->fPhotonL90[tower];

  // return x>edge_low && x<edge_high && y>edge_low && y<edge_high && nhits==1 && e>200 && l90<20; //For LHC
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high && e > 30;  // for SPS
}

template <typename armclass>
Bool_t QuickAnalysis<armclass>::SelectNeutron(Int_t tower) {
  // position cut
  const Double_t x = fLvl3->fNeutronPosition[tower][0];
  const Double_t y = fLvl3->fNeutronPosition[tower][1];
  const Double_t edge_low = 2;  // mm
  const Double_t edge_high = fLvl3->kTowerSize[tower] - 2;
  // single-hit cut
  const Int_t nhits = (fLvl3->fSoftwareTrigger[tower] ? 1 : 0) * (fLvl3->fNeutronMultiHit[tower] ? 2 : 1);
  // energy cut
  const Double_t e = fLvl3->fNeutronEnergy[tower];
  // PID
  const Double_t l90 = fLvl3->fNeutronL90[tower];

  // return x>edge_low && x<edge_high && y>edge_low && y<edge_high && nhits==1 && e>500 && l90>20; //for LHC
  return x > edge_low && x < edge_high && y > edge_low && y < edge_high && e > 30;  // for SPS
}

template <typename armclass>
void QuickAnalysis<armclass>::FillPiEtaHist() {
  typedef Arm1AnPars PAR;
  // if(fLvl3->kArmIndex==0){typedef Arm1AnPars PAR;}
  if (fLvl3->kArmIndex == 1) {
    typedef Arm2AnPars PAR;
  } else if (fLvl3->kArmIndex > 1) {
    UT::Printf(UT::kPrintInfo, "\nInvalid arm index\n");
  }

  const Double_t impact_distance = CT::kDetectorZpos;
  // ST
  // TODO [TrackProjection]: Check this change
  Int_t xlay_1 = fLvl3->fPosMaxLayer[0][0];
  Int_t ylay_1 = fLvl3->fPosMaxLayer[0][1];
  Double_t xpos_1 = fLvl3->fPhotonPosition[0][0];
  Double_t ypos_1 = fLvl3->fPhotonPosition[0][1];
  TVector3 coll_pos_1 = CT::GetRecoCollisionCoordinates(fLvl3->kArmIndex, 0, xpos_1, ypos_1, xlay_1, ylay_1);
  //
  Double_t x1 = coll_pos_1.X();
  Double_t y1 = coll_pos_1.Y();
  // LT
  // TODO [TrackProjection]: Check this change
  Int_t xlay_2 = fLvl3->fPosMaxLayer[1][0];
  Int_t ylay_2 = fLvl3->fPosMaxLayer[1][1];
  Double_t xpos_2 = fLvl3->fPhotonPosition[1][0];
  Double_t ypos_2 = fLvl3->fPhotonPosition[1][1];
  TVector3 coll_pos_2 = CT::GetRecoCollisionCoordinates(fLvl3->kArmIndex, 1, xpos_2, ypos_2, xlay_2, ylay_2);
  //
  Double_t x2 = coll_pos_2.X();
  Double_t y2 = coll_pos_2.Y();
  // Calculate Mgg
  Double_t r = TMath::Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  Double_t theta = r / CT::kDetectorZpos * 1000.;  // [mrad]

  Double_t e1 = fLvl3->fPhotonEnergyLin[0];
  Double_t e2 = fLvl3->fPhotonEnergyLin[1];

  Double_t mgg = TMath::Sqrt(e1 * e2) * theta;  // [MeV]

  Double_t e = e1 + e2;

  Double_t len1 = sqrt(x1 * x1 + y1 * y1 + impact_distance * impact_distance);
  Double_t len2 = sqrt(x2 * x2 + y2 * y2 + impact_distance * impact_distance);

  Double_t px = e1 * x1 / len1 + e2 * x2 / len2;
  Double_t py = e1 * y1 / len1 + e2 * y2 / len2;
  Double_t pz = e1 * impact_distance / len1 + e2 * impact_distance / len2;
  Double_t pt = sqrt(px * px + py * py);

  // fill trigger containers
  bool trigger[6] = {fLvl3->IsShowerTrg(), fLvl3->IsSpecialTrg(),  fLvl3->IsHighEMTrg(),
                     fLvl3->IsHadronTrg(), fLvl3->IsPedestalTrg(), fLvl3->IsZdcTrg()};

  for (int i = 0; i < 6; i++) {
    if (trigger[i]) {
      // fill invariant mass histo
      h_mgg[i]->Fill(mgg);
      // fill hitmap
      h_pos_pi_eta[i]->Fill(x1, y1);
      h_pos_pi_eta[i]->Fill(x2, y2);

      // fill energy and pt
      if (mgg > 120 && mgg < 145) {
        h_ene_pi[i]->Fill(e);
        h_pt_pi[i]->Fill(pt);
      }
      if (mgg > 510 && mgg < 580) {
        h_ene_eta[i]->Fill(e);
        h_pt_eta[i]->Fill(pt);
      }
    }
  }
  // fill no trigger containers

  // fill invariant mass histo
  h_mgg[6]->Fill(mgg);
  // fill hitmap
  h_pos_pi_eta[6]->Fill(x1, y1);
  h_pos_pi_eta[6]->Fill(x2, y2);

  // fill energy and pt
  if (mgg > 120 && mgg < 145) {
    h_ene_pi[6]->Fill(e);
    h_pt_pi[6]->Fill(pt);
  }
  if (mgg > 510 && mgg < 580) {
    h_ene_eta[6]->Fill(e);
    h_pt_eta[6]->Fill(pt);
  }
}

template <typename armclass>
void QuickAnalysis<armclass>::FillPhotonHist(Int_t tower) {
  typedef Arm1AnPars PAR;
  // if(fLvl3->kArmIndex==0){typedef Arm1AnPars PAR;}
  if (fLvl3->kArmIndex == 1) {
    typedef Arm2AnPars PAR;
  } else if (fLvl3->kArmIndex > 1) {
    UT::Printf(UT::kPrintInfo, "\nInvalid arm index\n");
  }

  // tower= 0 ST, 1 LT
  const Double_t impact_distance = 141050;
  // TODO [TrackProjection]: Check this change
  Int_t xlay = fLvl3->fPosMaxLayer[tower][0];
  Int_t ylay = fLvl3->fPosMaxLayer[tower][1];
  Double_t xpos = fLvl3->fPhotonPosition[tower][0];
  Double_t ypos = fLvl3->fPhotonPosition[tower][1];
  TVector3 coll_pos = CT::GetRecoCollisionCoordinates(fLvl3->kArmIndex, tower, xpos, ypos, xlay, ylay);
  //
  Double_t x = coll_pos.X();
  Double_t y = coll_pos.Y();
  Double_t ene = fLvl3->fPhotonEnergyLin[tower];

  Double_t len = sqrt(x * x + y * y + impact_distance * impact_distance);

  Double_t px = ene * x / len;
  Double_t py = ene * y / len;
  Double_t pz = ene * impact_distance / len;
  Double_t pt = sqrt(px * px + py * py);

  // fill trigger containers
  bool trigger[6] = {fLvl3->IsShowerTrg(), fLvl3->IsSpecialTrg(),  fLvl3->IsHighEMTrg(),
                     fLvl3->IsHadronTrg(), fLvl3->IsPedestalTrg(), fLvl3->IsZdcTrg()};

  for (int i = 0; i < 6; i++) {
    if (trigger[i]) {
      // fill energy
      h_ene_photon[tower][i]->Fill(ene);
      // fill pt
      h_pt_photon[tower][i]->Fill(pt);
      // fill hitmap
      h_pos_photon[i]->Fill(x, y);
      // fill hitmap >1TeV
      if (ene > 1000) h_pos_photon_thr[i]->Fill(x, y);
    }
  }
  // fill no trigger containers

  // fill energy
  h_ene_photon[tower][6]->Fill(ene);
  // fill pt
  h_pt_photon[tower][6]->Fill(pt);
  // fill hitmap
  h_pos_photon[6]->Fill(x, y);
  // fill hitmap >1TeV
  if (ene > 1000) h_pos_photon_thr[6]->Fill(x, y);
}

template <typename armclass>
void QuickAnalysis<armclass>::FillNeutronHist(Int_t tower) {
  typedef Arm1AnPars PAR;
  // if(fLvl3->kArmIndex==0){typedef Arm1AnPars PAR;}
  if (fLvl3->kArmIndex == 1) {
    typedef Arm2AnPars PAR;
  } else if (fLvl3->kArmIndex > 1) {
    UT::Printf(UT::kPrintInfo, "\nInvalid arm index\n");
  }

  // tower= 0 ST, 1 LT
  const Double_t impact_distance = CT::kDetectorZpos;

  // TODO [TrackProjection]: Check this change
  Int_t xlay = fLvl3->fPosMaxLayer[tower][0];
  Int_t ylay = fLvl3->fPosMaxLayer[tower][1];
  Double_t xpos = fLvl3->fNeutronPosition[tower][0];
  Double_t ypos = fLvl3->fNeutronPosition[tower][1];
  TVector3 coll_pos = CT::GetRecoCollisionCoordinates(fLvl3->kArmIndex, tower, xpos, ypos, xlay, ylay);
  //
  Double_t x = coll_pos.X();
  Double_t y = coll_pos.Y();
  Double_t ene = fLvl3->fNeutronEnergy[tower];

  Double_t len = sqrt(x * x + y * y + impact_distance * impact_distance);

  Double_t px = ene * x / len;
  Double_t py = ene * y / len;
  Double_t pz = ene * impact_distance / len;
  Double_t pt = sqrt(px * px + py * py);

  // fill trigger containers
  bool trigger[6] = {fLvl3->IsShowerTrg(), fLvl3->IsSpecialTrg(),  fLvl3->IsHighEMTrg(),
                     fLvl3->IsHadronTrg(), fLvl3->IsPedestalTrg(), fLvl3->IsZdcTrg()};

  for (int i = 0; i < 6; i++) {
    if (trigger[i]) {
      // fill energy
      h_ene_neutron[tower][i]->Fill(ene);
      // fill pt
      h_pt_neutron[tower][i]->Fill(pt);
      // fill hitmap
      h_pos_neutron[i]->Fill(x, y);
      // fill hitmap >1TeV
      if (ene > 1000) h_pos_neutron_thr[i]->Fill(x, y);
    }
  }

  // fill no trigger container
  // fill energy
  h_ene_neutron[tower][6]->Fill(ene);
  // fill pt
  h_pt_neutron[tower][6]->Fill(pt);
  // fill hitmap
  h_pos_neutron[6]->Fill(x, y);
  // fill hitmap >1TeV
  if (ene > 1000) h_pos_neutron_thr[6]->Fill(x, y);
}

template <typename armclass>
void QuickAnalysis<armclass>::MakeHistograms() {
  // NB: This must not be altered in case of subrun otherwise the title is wrong after hadd
  string trigger[] = {"_shower_trigger",
                      "_pi0_trigger",
                      "_high_em_trigger",
                      "_hadron_trigger",
                      "_pedestal_trigger",
                      "_zdc_trigger",
                      ""};
  string trigger_name[] = {"shower",   "#pi^{0}", "high energy EM shower", "hadron",
                           "pedestal", "ZDC",     "without selection"};
  Double_t x_inf[2] = {-30, -50}, x_sup[2] = {40, 30}, y_inf[2] = {-20, -20}, y_sup[2] = {80, 60};

  for (int i = 0; i < 7; i++) {
    // photon
    // h_ene_photon[0][i]= new TH1D(Form("h_energy_st_photon%s", trigger[i].c_str()), Form("Photon energy small tower
    // Arm%d run %05d %s trigger;E_{#gamma} [GeV];Counts ",this->kArmIndex+1, fRunNumber, trigger_name[i].c_str()), 50,
    // 0,6500); h_ene_photon[1][i]= new TH1D(Form("h_energy_lt_photon%s", trigger[i].c_str()), Form("Photon energy large
    // tower Arm%d run %05d %s trigger;E_{#gamma} [GeV];Counts ",this->kArmIndex+1, fRunNumber,
    // trigger_name[i].c_str()), 50, 0,6500);

    h_ene_photon[0][i] = new TH1D(Form("h_energy_st_photon%s", trigger[i].c_str()),
                                  Form("Photon energy small tower Arm%d run %05d %s trigger;E_{#gamma} [GeV];Counts ",
                                       this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                                  50, 0, 600);
    h_ene_photon[1][i] = new TH1D(Form("h_energy_lt_photon%s", trigger[i].c_str()),
                                  Form("Photon energy large tower Arm%d run %05d %s trigger;E_{#gamma} [GeV];Counts ",
                                       this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                                  50, 0, 600);

    h_pt_photon[0][i] = new TH1D(Form("h_pt_st_photon%s", trigger[i].c_str()),
                                 Form("Photon p_{T} small tower Arm%d run %05d %s trigger;p_{t,#gamma} [GeV];Counts ",
                                      this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                                 50, 0, 1.5);
    h_pt_photon[1][i] = new TH1D(Form("h_pt_lt_photon%s", trigger[i].c_str()),
                                 Form("Photon p_{T} large tower Arm%d run %05d %s trigger;p_{t,#gamma} [GeV];Counts ",
                                      this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                                 50, 0, 1.5);
    h_pos_photon[i] = new TH2D(Form("h_pos_photon%s", trigger[i].c_str()),
                               Form("Hit-map photons Arm%d run %05d %s trigger;x[mm];y[mm] ", this->kArmIndex + 1,
                                    fRunNumber, trigger_name[i].c_str()),
                               50, x_inf[this->kArmIndex], x_sup[this->kArmIndex], 50, y_inf[this->kArmIndex],
                               y_sup[this->kArmIndex]);
    h_pos_photon_thr[i] = new TH2D(Form("h_pos_photon_thr%s", trigger[i].c_str()),
                                   Form("Hit-map photons Arm%d run %05d E>1 TeV %s trigger;x[mm];y[mm] ",
                                        this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                                   50, x_inf[this->kArmIndex], x_sup[this->kArmIndex], 50, y_inf[this->kArmIndex],
                                   y_sup[this->kArmIndex]);
    // neutron
    // h_ene_neutron[0][i]= new TH1D(Form("h_energy_st_neutron%s", trigger[i].c_str()), Form("Neutron energy small tower
    // Arm%d run %05d %s trigger;E_{neutron} [GeV];Counts ",this->kArmIndex+1, fRunNumber, trigger_name[i].c_str()), 50,
    // 0,14000); h_ene_neutron[1][i]= new TH1D(Form("h_energy_lt_neutron%s", trigger[i].c_str()), Form("Neutron energy
    // large tower Arm%d run %05d %s trigger;E_{neutron} [GeV];Counts ",this->kArmIndex+1, fRunNumber,
    // trigger_name[i].c_str()), 50, 0,14000);

    h_ene_neutron[0][i] =
        new TH1D(Form("h_energy_st_neutron%s", trigger[i].c_str()),
                 Form("Neutron energy small tower Arm%d run %05d %s trigger;E_{neutron} [GeV];Counts ",
                      this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                 50, 0, 700);
    h_ene_neutron[1][i] =
        new TH1D(Form("h_energy_lt_neutron%s", trigger[i].c_str()),
                 Form("Neutron energy large tower Arm%d run %05d %s trigger;E_{neutron} [GeV];Counts ",
                      this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                 50, 0, 700);

    h_pt_neutron[0][i] =
        new TH1D(Form("h_pt_st_neutron%s", trigger[i].c_str()),
                 Form("Neutron p_{T} small tower Arm%d run %05d %s trigger;p_{t,neutron} [GeV];Counts ",
                      this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                 50, 0, 3);
    h_pt_neutron[1][i] =
        new TH1D(Form("h_pt_lt_neutron%s", trigger[i].c_str()),
                 Form("Neutron p_{T} large tower Arm%d run %05d %s trigger;p_{t,neutron} [GeV];Counts ",
                      this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                 50, 0, 3);
    h_pos_neutron[i] = new TH2D(Form("h_pos_neutron%s", trigger[i].c_str()),
                                Form("Hit-map neutron Arm%d run %05d %s trigger;x[mm];y[mm] ", this->kArmIndex + 1,
                                     fRunNumber, trigger_name[i].c_str()),
                                50, x_inf[this->kArmIndex], x_sup[this->kArmIndex], 50, y_inf[this->kArmIndex],
                                y_sup[this->kArmIndex]);
    h_pos_neutron_thr[i] = new TH2D(Form("h_pos_neutron_thr%s", trigger[i].c_str()),
                                    Form("Hit-map neutron Arm%d run %05d E>1 TeV %s trigger;x[mm];y[mm] ",
                                         this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                                    50, x_inf[this->kArmIndex], x_sup[this->kArmIndex], 50, y_inf[this->kArmIndex],
                                    y_sup[this->kArmIndex]);
    // pi and eta
    h_mgg[i] = new TH1D(Form("h_mgg%s", trigger[i].c_str()),
                        Form("Invariant mass Arm%d run %05d %s trigger;M_{#gamma#gamma} [MeV];Counts ",
                             this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                        50, 0, 700);
    h_pos_pi_eta[i] = new TH2D(Form("h_pos_pi_eta%s", trigger[i].c_str()),
                               Form("Hit-map #pi/#eta Arm%d run %05d %s trigger;x[mm];y[mm] ", this->kArmIndex + 1,
                                    fRunNumber, trigger_name[i].c_str()),
                               50, x_inf[this->kArmIndex], x_sup[this->kArmIndex], 50, y_inf[this->kArmIndex],
                               y_sup[this->kArmIndex]);
    h_ene_pi[i] = new TH1D(Form("h_energy_pion%s", trigger[i].c_str()),
                           Form("#pi^{0} energy Arm%d run %05d %s trigger;E_{#pi^{0}} [GeV];Counts ",
                                this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                           50, 0, 6500);
    h_ene_eta[i] = new TH1D(Form("h_energy_eta%s", trigger[i].c_str()),
                            Form("#eta energy Arm%d run %05d %s trigger;E_{#eta} [GeV];Counts ", this->kArmIndex + 1,
                                 fRunNumber, trigger_name[i].c_str()),
                            10, 0, 6500);
    h_pt_pi[i] = new TH1D(Form("h_pt_pion%s", trigger[i].c_str()),
                          Form("#pi^{0} p_{T} Arm%d run %05d %s trigger;p_{t,#pi^{0}} [GeV];Counts ",
                               this->kArmIndex + 1, fRunNumber, trigger_name[i].c_str()),
                          50, 0, 1.5);
    h_pt_eta[i] = new TH1D(Form("h_pt_eta%s", trigger[i].c_str()),
                           Form("#eta p_{T} Arm%d run %05d %s trigger;p_{t,#eta} [GeV];Counts ", this->kArmIndex + 1,
                                fRunNumber, trigger_name[i].c_str()),
                           10, 0, 1.5);
  }
}

template <typename armclass>
void QuickAnalysis<armclass>::RunLoop() {
  fOutputFile = new TFile(fOutputName, "RECREATE");
  MakeHistograms();
  SetInputTree();

  /*--- Event Loop ---*/
  if (fSubRunNumber >= 0)
    UT::Printf(UT::kPrintInfo, Form("Start of event loop run %d subrun %d\n", fRunNumber, fSubRunNumber));
  else
    UT::Printf(UT::kPrintInfo, Form("Start of event loop run %d\n", fRunNumber));
  const Int_t print_interval = 10000;
  auto start = high_resolution_clock::now();
  auto start_ev = start;
  Int_t nentries = fInputTree->GetEntries();
  for (Int_t ie = 0; ie < nentries; ++ie) {
    if (ie % 100 == 0 || ie == nentries - 1) {
      auto stop_ev = high_resolution_clock::now();
      auto duration = duration_cast<nanoseconds>(stop_ev - start_ev);
      Double_t diff = 1E9 * 100 / (Double_t)duration.count();
      UT::Printf(UT::kPrintInfo, "\rEvent %6d\t%5.0lf ev/s", ie, diff);
      fflush(stdout);
      start_ev = high_resolution_clock::now();
    }

    fInputTree->GetEntry(ie);
    fLvl3 = (Level3<armclass> *)fInputEv->Get(Form("lvl3_a%d", this->kArmIndex + 1));
    if (!fLvl3) {
      UT::Printf(UT::kPrintDebug, "Level3 not found in event %d\n", ie);
      continue;
    }

    // tower loop for photons and neutrons
    for (Int_t tower = 0; tower < 2; tower++) {
      // photons
      if (SelectPhoton(tower)) {
        FillPhotonHist(tower);
      }
      // neutrons
      if (SelectNeutron(tower)) {
        FillNeutronHist(tower);
      }
    }

    if (SelectPhoton(0) && SelectPhoton(1)) {
      FillPiEtaHist();
    }

    ClearEvent();
  }

  WriteToOutput();
  CloseFiles();
}

namespace nLHCf {
template class QuickAnalysis<Arm1Params>;
template class QuickAnalysis<Arm2Params>;
}  // namespace nLHCf
