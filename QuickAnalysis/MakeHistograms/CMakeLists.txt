project(MakeHistograms)
set (TARGET_NAME MakeHist)

add_subdirectory (BeamConfig)
add_subdirectory (Counter)

#-------------------------------#
#--- Set include directories ---#
#-------------------------------#
include_directories (${PROJECT_SOURCE_DIR}/include)
include_directories (${CMAKE_BINARY_DIR}/include)
include_directories (${CMAKE_SOURCE_DIR}/include)
include_directories (${CMAKE_SOURCE_DIR}/${Dict_DIR}/include)

#---------------------#
#--- Set libraries ---#
#---------------------#
set(LIBS ${Dict_DIR} ${ROOT_LIBRARIES})
link_directories (${LIBRARY_OUTPUT_PATH})

#----------------------#
#--- Set executable ---#
#----------------------#
add_executable (${TARGET_NAME} src/makehist.cpp)
target_link_libraries (${TARGET_NAME} ${LIBS})

#---------------#
#--- Install ---#
#---------------#
install (TARGETS ${TARGET_NAME} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
