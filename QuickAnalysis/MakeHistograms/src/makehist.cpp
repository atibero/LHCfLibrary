#include <iostream>
//#include <fstream>
#include <iomanip>
using namespace std;

#include <TApplication.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TROOT.h>
#include <TString.h>

#include <Arm1Params.hh>
#include <Arm2Params.hh>
#include <LHCfEvent.hh>
#include <Level0.hh>
#include <Level1.hh>
#include <Level2.hh>
using namespace nLHCf;

int main(int argc, char** argv) {
  TString filename = "";  // calibrated data
  TString ofilename = "hist.root";
  bool arm1_flag, arm2_flag;

  for (int i = 1; i < argc; i++) {
    TString ss = argv[i];
    if (ss == "-i" || ss == "--input") {
      filename = argv[++i];
    }
    if (ss == "-o" || ss == "--output") {
      ofilename = argv[++i];
    }
    if (ss == "-a1" || ss == "--arm1") {
      arm1_flag = true;
    }
    if (ss == "-a2" || ss == "--arm2") {
      arm2_flag = true;
    }
  }

  if (!arm1_flag && !arm2_flag) {
    cerr << "Error: Give an arm flag." << endl;
    exit(-1);
  }

  cout << "INTPUT FILENAME: " << filename << endl;
  cout << "OUTPUT FILENAME: " << ofilename << endl;

  // Open input file (calibrate file)
  TChain* tree = new TChain("LHCfEvents");
  tree->AddFile(filename);
  tree->SetCacheSize(10000000);
  LHCfEvent* ev = new LHCfEvent("event", "LHCfEvent");
  tree->SetBranchAddress("ev.", &ev);
  tree->AddBranchToCache("*");

  // Open output file and memory allocation of histograms
  TFile* ofile = new TFile(ofilename, "RECREATE");

  ofile->cd();

  TH1F* h1[2][16][2];  // calorimeter
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        if (il == 5) {
          h1[it][il][ir] = new TH1F(Form("A1Cal_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir),
                                    100, -1000, 4000);
        } else {
          h1[it][il][ir] = new TH1F(Form("A1Cal_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir),
                                    60, -100, 500);
        }
      }
    }
  }

  TH1F* h2[2][16][2];  // cal_pedestal
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        h2[it][il][ir] = new TH1F(Form("A1CalPed_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir),
                                  400, 0, 4000);
      }
    }
  }

  TH1F* h3[2][3];  // flag[ev=0/ped=0][fFlag[]]
  for (int iev = 0; iev < 2; iev++) {
    for (int i = 0; i < 3; i++) {
      h3[iev][i] = new TH1F(Form("A1Flag_%s_%d", iev == 0 ? "ev" : "ped", i),
                            Form("Flag_%s_%d", iev == 0 ? "ev" : "ped", i), 32, 0, 32);
    }
  }

  TH1F* h4[2][4][2];  // GSObar
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 4; il++) {
      for (int iv = 0; iv < 2; iv++) {
        h4[it][il][iv] = new TH1F(Form("A1Pos_%d_%d_%d", it, il, iv), Form("Tower:%d_Layer:%d_XY:%d", it, il, iv),
                                  ((it == 0) ? 20 : 40), 0, ((it == 0) ? 20 : 40));
      }
    }
  }

  TH1F* h5[2][16][2];  // lvl0 calorimeter(Arm1) WRADC, NRADC
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        h5[it][il][ir] =
            new TH1F(Form("A1Cal0_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir), 400, 0, 4000);
      }
    }
  }

  TH1F* h6[2][16][2];  // lvl0 calorimeter(Arm2) WRADC, NRADC
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        h6[it][il][ir] =
            new TH1F(Form("A2Cal0_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir), 400, 0, 4000);
      }
    }
  }

  TH1F* h7[2][4][2][3];  // silicon
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 4; il++) {
      for (int iv = 0; iv < 2; iv++) {
        for (int is = 0; is < 3; is++) {
          h7[it][il][iv][is] = new TH1F(Form("A2Pos_%d_%d_%d_%d", it, il, iv, is),
                                        Form("Tower:%d_Layer:%d_XY:%d_Sample:%d", it, il, iv, is), 384, 0, 384);
        }
      }
    }
  }

  TH1F* h8[2][16][2];  // cal_pedestal(Arm2)
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        h8[it][il][ir] = new TH1F(Form("A2CalPed_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir),
                                  400, 0, 4000);
      }
    }
  }

  TH1F* h9[2][3];  // flag[ev=0/ped=0][fFlag[]](Arm2)
  for (int iev = 0; iev < 2; iev++) {
    for (int i = 0; i < 3; i++) {
      h9[iev][i] = new TH1F(Form("A2Flag_%s_%d", iev == 0 ? "ev" : "ped", i),
                            Form("Flag_%s_%d", iev == 0 ? "ev" : "ped", i), 32, 0, 32);
    }
  }

  // Beam Configuration
  TH1F* h10 = new TH1F("hbconf_a1_all", "hbconf_a1_all", 4000, 0, 4000);
  // TH1F* h11 = new TH1F("hbconf_a1_l2ta","hbconf_a1_l2ta",4000,0,4000);
  TH1F* h12 = new TH1F("hbconf_a2_all", "hbconf_a2_all", 4000, 0, 4000);
  // TH1F* h13 = new TH1F("hbconf_a2_l2ta","hbconf_a2_l2ta",4000,0,4000);
  TH1F* h14 = new TH1F("hbconf_a1_bptx1", "hbconf_a1_bptx1", 4000, 0, 4000);
  TH1F* h15 = new TH1F("hbconf_a1_bptx2", "hbconf_a1_bptx2", 4000, 0, 4000);
  TH1F* h16 = new TH1F("hbconf_a2_bptx1", "hbconf_a2_bptx1", 4000, 0, 4000);
  TH1F* h17 = new TH1F("hbconf_a2_bptx2", "hbconf_a2_bptx2", 4000, 0, 4000);

  // TDC (arm1)
  TH1F* h18[7];
  for (int i = 0; i < 2; ++i) {
    h18[i] = new TH1F(Form("A1_TDC_BPTX%d", (i + 1)), Form("A1_TDC_BPTX%d", (i + 1)), 100, -1000, -830);
  }
  h18[2] = new TH1F("A1_TDC_Shower_Trigger", "A1_TDC_Shower_Trigger", 100, -350, -50);
  for (int i = 0; i < 2; ++i) {
    h18[i + 3] = new TH1F(Form("TDC_%smm_OR", (i == 0 ? "20" : "40")), Form("TDC_%smm_OR", (i == 0 ? "20" : "40")), 100,
                          -350, -50);
  }
  for (int i = 0; i < 2; ++i) {
    h18[i + 5] = new TH1F(Form("A1_TDC_Arm%d_FC", (i + 1)), Form("A1_TDC_Arm%d_FC", (i + 1)), 100, -350, -50);
  }

  // TDC (arm2)
  TH1F* h19[7];
  for (int i = 0; i < 2; ++i) {
    h19[i] = new TH1F(Form("A2_TDC_BPTX%d", (i + 1)), Form("A2_TDC_BPTX%d", (i + 1)), 100, -1000, -830);
  }
  h19[2] = new TH1F("A2_TDC_Shower_Trigger", "A2_TDC_Shower_Trigger", 100, -350, -50);
  for (int i = 0; i < 2; ++i) {
    h19[i + 3] = new TH1F(Form("TDC_%smm_OR", (i == 0 ? "25" : "32")), Form("TDC_%smm_OR", (i == 0 ? "25" : "32")), 100,
                          -350, -50);
  }
  for (int i = 0; i < 2; ++i) {
    h19[i + 5] = new TH1F(Form("A2_TDC_Arm%d_FC", (i + 1)), Form("A2_TDC_Arm%d_FC", (i + 1)), 100, -350, -50);
  }

  // DSC (arm1,lvl1)
  TH1F* h20[16][2];
  TH1F* h21[16][2];
  for (int i = 0; i < 16; ++i) {
    for (int ir = 0; ir < 2; ir++) {
      if (i == 5) {
        h20[i][ir] = new TH1F(Form("DSC_Arm1-Large_layer%d_Range%d", i, ir),
                              Form("Arm1_Tower:1_Layer:%d_Range:%d", i, ir), 100, -1000, 4000);
        h21[i][ir] = new TH1F(Form("DSC_Arm1-Small_layer%d_Range%d", i, ir),
                              Form("Arm1_Tower:0_Layer:%d_Range:%d", i, ir), 100, -1000, 4000);
      } else {
        h20[i][ir] = new TH1F(Form("DSC_Arm1-Large_layer%d_Range%d", i, ir),
                              Form("Arm1_Tower:1_Layer:%d_Range:%d", i, ir), 60, -100, 500);
        h21[i][ir] = new TH1F(Form("DSC_Arm1-Small_layer%d_Range%d", i, ir),
                              Form("Arm1_Tower:0_Layer:%d_Range:%d", i, ir), 60, -100, 500);
      }
    }
  }

  // DSC (arm1, lvl2)
  TH1F* h45[16];
  TH1F* h46[16];
  for (int i = 0; i < 16; ++i) {
    h45[i] = new TH1F(Form("DSC_lvl2_Arm1-Large_layer%d", i), Form("Arm1_lvl2_Tower:1_Layer:%d", i), 100, -10, 50);
    h46[i] = new TH1F(Form("DSC_lvl2_Arm1-Small_layer%d", i), Form("Arm1_lvl2_Tower:0_Layer:%d", i), 100, -10, 50);
  }

  // DSC (arm2,lvl1)
  TH1F* h22[16][2];
  TH1F* h23[16][2];
  for (int i = 0; i < 16; ++i) {
    for (int ir = 0; ir < 2; ir++) {
      if (i == 5) {
        h22[i][ir] = new TH1F(Form("DSC_Arm2-Large_layer%d_Range%d", i, ir),
                              Form("Arm2_Tower:1_Layer:%d_Range:%d", i, ir), 100, -1000, 4000);
        h23[i][ir] = new TH1F(Form("DSC_Arm2-Small_layer%d_Range%d", i, ir),
                              Form("Arm2_Tower:0_Layer:%d_Range:%d", i, ir), 100, -1000, 4000);
      } else {
        h22[i][ir] = new TH1F(Form("DSC_Arm2-Large_layer%d_Range%d", i, ir),
                              Form("Arm2_Tower:1_Layer:%d_Range:%d", i, ir), 60, -100, 500);
        h23[i][ir] = new TH1F(Form("DSC_Arm2-Small_layer%d_Range%d", i, ir),
                              Form("Arm2_Tower:0_Layer:%d_Range:%d", i, ir), 60, -100, 500);
      }
    }
  }

  // DSC (arm2, lvl2)
  TH1F* h47[16];
  TH1F* h48[16];
  for (int i = 0; i < 16; ++i) {
    h47[i] = new TH1F(Form("DSC_lvl2_Arm2-Large_layer%d", i), Form("Arm2_lvl2_Tower:1_Layer:%d", i), 100, -10, 50);
    h48[i] = new TH1F(Form("DSC_lvl2_Arm2-Small_layer%d", i), Form("Arm2_lvl2_Tower:0_Layer:%d", i), 100, -10, 50);
  }

  // DSC FC's
  TH1F* h24[4];
  TH1F* h25[4];
  for (int i = 0; i < 4; ++i) {
    h24[i] = new TH1F(Form("DSC_FCs_Arm1_channel%d", i), Form("Arm1_FC_Channel:%d", i), 100, -1000, 4000);
    h25[i] = new TH1F(Form("DSC_FCs_Arm2_channel%d", i), Form("Arm2_FC_Channel:%d", i), 100, -1000, 4000);
  }

  // DSC FC's lvl2
  TH1F* h49[4];
  TH1F* h50[4];
  for (int i = 0; i < 4; ++i) {
    h49[i] = new TH1F(Form("DSC_FCs_Arm1_lvl2_channel%d", i), Form("Arm1_lvl2_FC_Channel:%d", i), 100, -1000, 7000);
    h50[i] = new TH1F(Form("DSC_FCs_Arm2_lvl2_channel%d", i), Form("Arm2_lvl2_FC_Channel:%d", i), 100, -1000, 7000);
  }

  // DSC FC's lvl1
  TH1F* h57[4];
  TH1F* h58[4];
  for (int i = 0; i < 4; i++) {
    h57[i] = new TH1F(Form("DSC_FCs_Arm1_lvl1_channel%d", i), Form("Arm1_lvl1_FC_Channel:%d", i), 100, -1000, 4000);
    h58[i] = new TH1F(Form("DSC_FCs_Arm2_lvl1_channel%d", i), Form("Arm2_lvl1_FC_Channel:%d", i), 100, -1000, 4000);
  }

  // FrontCounter lvl0
  TH1F* h26[4][2];
  TH1F* h27[4][2];
  cout << h26 << endl;
  for (int i = 0; i < 4; ++i) {
    for (int ir = 0; ir < 2; ir++) {
      h26[i][ir] =
          new TH1F(Form("FC_Arm1_channel%d_%d", i, ir), Form("Arm1_FC_Channel:%d_%d", i, ir), 100, -1000, 4000);
      h27[i][ir] =
          new TH1F(Form("FC_Arm2_channel%d_%d", i, ir), Form("Arm2_FC_Channel:%d_%d", i, ir), 100, -1000, 4000);
    }
  }

  // FrontCounter lvl2
  TH1F* h51[4];
  TH1F* h52[4];
  for (int i = 0; i < 4; ++i) {
    h51[i] = new TH1F(Form("FC_Arm1_lvl2_channel%d", i), Form("Arm1_lvl2_FC_Channel:%d", i), 100, -1000, 7000);
    h52[i] = new TH1F(Form("FC_Arm2_lvl2_channel%d", i), Form("Arm2_lvl2_FC_Channel:%d", i), 100, -1000, 7000);
  }

  // FrontCounter lvl1
  TH1F* h55[4];
  TH1F* h56[4];
  for (int i = 0; i < 4; i++) {
    h55[i] = new TH1F(Form("FC_Arm1_lvl1_channel%d", i), Form("Arm1_lvl1_FC_Channel:%d", i), 100, -1000, 4000);
    h56[i] = new TH1F(Form("FC_Arm2_lvl1_channel%d", i), Form("Arm2_lvl1_FC_Channel:%d", i), 100, -1000, 4000);
  }

  TH1F* h28[2][16][2];  // calorimeter(arm2)
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        if (il == 5) {
          h28[it][il][ir] = new TH1F(Form("A2Cal_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir),
                                     100, -1000, 4000);
        } else {
          h28[it][il][ir] = new TH1F(Form("A2Cal_%d_%d_%d", it, il, ir), Form("Tower:%d_Layer:%d_Range:%d", it, il, ir),
                                     60, -100, 500);
        }
      }
    }
  }

  // calorimeter lvl2
  TH1F* h53[2][16];
  TH1F* h54[2][16];
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; ++il) {
      h53[it][il] = new TH1F(Form("A1Cal_lvl2_Tower%d_layer%d", it, il), Form("A1Cal_lvl2_Tower:%d_Layer:%d", it, il),
                             100, -10, 50);
      h54[it][il] = new TH1F(Form("A2Cal_lvl2_Tower%d_layer%d", it, il), Form("A2Cal_lvl2_Tower:%d_Layer:%d", it, il),
                             100, -10, 50);
    }
  }

  TH1F* h29[2];  // laser(Arm1)
  for (int it = 0; it < 2; it++) {
    h29[it] = new TH1F(Form("Laser_Arm1_%d", it), Form("Laser_Arm1_%d", it), 400, 0, 4000);
  }

  TH1F* h30[2];  // laser(Arm2)
  for (int it = 0; it < 2; it++) {
    h30[it] = new TH1F(Form("Laser_Arm2_%d", it), Form("Laser_Arm2_%d", it), 400, 0, 4000);
  }

  // Front Counter Pedestal
  TH1F* h31[4][2];
  TH1F* h32[4][2];
  TH1F* h41[4][2];
  TH1F* h42[4][2];
  for (int i = 0; i < 4; ++i) {
    for (int ir = 0; ir < 2; ir++) {
      h31[i][ir] =
          new TH1F(Form("FCped_Arm1_channel%d_%d", i, ir), Form("Arm1ped_FC_Channel:%d_%d", i, ir), 100, -1000, 4000);
      h32[i][ir] =
          new TH1F(Form("FCped_Arm2_channel%d_%d", i, ir), Form("Arm2ped_FC_Channel:%d_%d", i, ir), 100, -1000, 4000);
      h41[i][ir] = new TH1F(Form("FCoffped_Arm1_channel%d_%d", i, ir), Form("Arm1offped_FC_Channel:%d_%d", i, ir), 100,
                            -1000, 4000);
      h42[i][ir] = new TH1F(Form("FCoffped_Arm2_channel%d_%d", i, ir), Form("Arm2offped_FC_Channel:%d_%d", i, ir), 100,
                            -1000, 4000);
    }
  }

  // Laser Pedestal
  TH1F* h33[2];
  TH1F* h34[2];
  TH1F* h43[2];
  TH1F* h44[2];
  for (int it = 0; it < 2; it++) {
    h33[it] = new TH1F(Form("Laserped_Arm1_%d", it), Form("Laserped_Arm1_%d", it), 400, 0, 4000);
    h34[it] = new TH1F(Form("Laserped_Arm2_%d", it), Form("Laserped_Arm2_%d", it), 400, 0, 4000);
    h43[it] = new TH1F(Form("Laseroffped_Arm1_%d", it), Form("Laseroffped_Arm1_%d", it), 400, 0, 4000);
    h44[it] = new TH1F(Form("Laseroffped_Arm2_%d", it), Form("Laseroffped_Arm2_%d", it), 400, 0, 4000);
  }

  // ZDC
  TH1F* h35[3][2];
  TH1F* h36[3][2];
  for (int im = 0; im < 3; im++) {
    for (int ir = 0; ir < 2; ir++) {
      h35[im][ir] = new TH1F(Form("ZDC_Arm1_%d_%d", im, ir), Form("ZDC_Arm1_%d_%d", im, ir), 100, -1000, 4000);
      h36[im][ir] = new TH1F(Form("ZDC_Arm2_%d_%d", im, ir), Form("ZDC_Arm2_%d_%d", im, ir), 100, -1000, 4000);
    }
  }

  // Arm1 cal Pedestal (update 20220910)
  TH1F* h37[2][16][2];  //[tower][layer][range]
  TH1F* h38[2][16][2];
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        h37[it][il][ir] = new TH1F(Form("a1_calped_%d_%d_%d", it, il, ir),
                                   Form("Arm1_CalPed_Tower:%d_Layer:%d_Range:%d", it, il, ir), 400, 0, 4000);
        h38[it][il][ir] = new TH1F(Form("a1_caloffped_%d_%d_%d", it, il, ir),
                                   Form("Arm1_CalPed_Offset_Tower:%d_Layer:%d_Range:%d", it, il, ir), 400, 0, 4000);
      }
    }
  }

  // Arm2 cal Pedestal (update 20220910)
  TH1F* h39[2][16][2];  //[tower][layer][range]
  TH1F* h40[2][16][2];
  for (int it = 0; it < 2; it++) {
    for (int il = 0; il < 16; il++) {
      for (int ir = 0; ir < 2; ir++) {
        h39[it][il][ir] = new TH1F(Form("a2_calped_%d_%d_%d", it, il, ir),
                                   Form("Arm2_CalPed_Tower:%d_Layer:%d_Range:%d", it, il, ir), 400, 0, 4000);
        h40[it][il][ir] = new TH1F(Form("a2_caloffped_%d_%d_%d", it, il, ir),
                                   Form("Arm2_CalPed_Offset_Tower:%d_Layer:%d_Range:%d", it, il, ir), 400, 0, 4000);
      }
    }
  }

  // calorimeter (level0,each trigger) trigger = [Shower, Pi0, HighEM, Hadron, ZDC]
  TH1F* h59[5][2][16][2];  // Arm1 calorimeter[trigger][tower][layer][range]
  TH1F* h60[5][2][16][2];  // Arm2 calorimeter[trigger][tower][layer][range]
  for (int itrg = 0; itrg < 5; itrg++) {
    for (int it = 0; it < 2; it++) {
      for (int il = 0; il < 16; il++) {
        for (int ir = 0; ir < 2; ir++) {
          h59[itrg][it][il][ir] = new TH1F(Form("Arm1_cal_lvl0_eachtrg_%d_%d_%d_%d", itrg, it, il, ir),
                                           Form("Arm1_cal_lvl0_eachtrg_%d_%d_%d_%d", itrg, it, il, ir), 400, 0, 4000);
          h60[itrg][it][il][ir] = new TH1F(Form("Arm2_cal_lvl0_eachtrg_%d_%d_%d_%d", itrg, it, il, ir),
                                           Form("Arm2_cal_lvl0_eachtrg_%d_%d_%d_%d", itrg, it, il, ir), 400, 0, 4000);
        }
      }
    }
  }

  gROOT->cd();

  int nentries = tree->GetEntries();
  cout << "Event number is   " << nentries << endl;
  int nev = 0, nev_a1 = 0, nev_a2 = 0;
  cout << "Event Loop..." << endl;
  for (int ie = 0; ie < nentries; ie++) {
    tree->GetEntry(ie);
    // ev->Show();
    if (ev->Check("lvl2_a1") == 1 && arm1_flag) {
      Level0<Arm1Params>* lvl0_a1 = (Level0<Arm1Params>*)ev->Get("lvl0_a1");
      Level1<Arm1Params>* lvl1_a1 = (Level1<Arm1Params>*)ev->Get("lvl1_a1");
      Level2<Arm1Params>* lvl2_a1 = (Level2<Arm1Params>*)ev->Get("lvl2_a1");
      Level0<Arm1Params>* ped0_a1 = (Level0<Arm1Params>*)ev->Get("ped0_a1");

      // trial
      // SPSAdamo *d = (SPSAdamo*)ev ->Get("adamo");

      if (lvl2_a1->fEvent % 1000 == 0) {
        cout << "----------- Run " << lvl2_a1->fRun << "  "
             << "Event " << lvl2_a1->fEvent << endl;
      }
      // Calorimeter
      for (int it = 0; it < Arm1Params::kCalNtower; it++) {
        for (int il = 0; il < Arm1Params::kCalNlayer; il++) {
          if (lvl0_a1->fEventFlag[0]) {
            h53[it][il]->Fill(lvl2_a1->fCalorimeter[it][il]);
            for (int ir = 0; ir < 2; ir++) {
              h1[it][il][ir]->Fill(lvl1_a1->fCalorimeter[it][il][ir]);
              h5[it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              if (lvl0_a1->IsShowerTrg()) {
                h59[0][it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              } else if (lvl0_a1->IsPi0Trg()) {
                h59[1][it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              } else if (lvl0_a1->IsHighEMTrg()) {
                h59[2][it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              } else if (lvl0_a1->IsHadronTrg()) {
                h59[3][it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              } else if (lvl0_a1->IsZdcTrg()) {
                h59[4][it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              }
            }
          } else if (lvl0_a1->fEventFlag[1]) {
            // h2[it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
            for (int ir = 0; ir < 2; ir++) {
              h37[it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir]);
              h38[it][il][ir]->Fill(lvl0_a1->fCalorimeter[it][il][ir] - ped0_a1->fCalorimeter[it][il][ir]);
            }
          }
        }
      }

      // Flag
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 32; j++) {
          if (lvl0_a1->fEventFlag[0]) {
            if ((lvl0_a1->fFlag[i] >> j) & 0x1) {
              h3[0][i]->Fill(j);
            }
          } else if (lvl0_a1->fEventFlag[1]) {
            if ((lvl0_a1->fFlag[i] >> j) & 0x1) {
              h3[1][i]->Fill(j);
            }
          }
        }
      }

      // GSO bar
      for (int it = 0; it < Arm1Params::kCalNtower; it++) {
        for (int il = 0; il < Arm1Params::kPosNlayer; il++) {
          for (int iv = 0; iv < Arm1Params::kPosNview; iv++) {
            for (int cha = 0; cha < Arm1Params::kPosNchannel[it]; cha++) {
              h4[it][il][iv]->Fill(cha, lvl2_a1->fPosDet[it][il][iv][cha][0]);
            }
          }
        }
      }

      // Bunch configration
      h10->Fill(lvl0_a1->BunchID());
      // bool IsL2TA = lvl0_a1->IsShowerTrg();
      // bool IsL2TSpecial = lvl0_a1->IsSpecialTrg();
      // if(lvl0_a1->fEventFlag[0] && (IsL2TA || IsL2TSpecial)){
      // h11->Fill(lvl0_a1->fCounter[22]);
      //}
      bool bptx1 = lvl0_a1->IsBeam1();
      bool bptx2 = lvl0_a1->IsBeam2();
      if (lvl0_a1->fEventFlag[0] && bptx1) {
        h14->Fill(lvl0_a1->BunchID());
      }
      if (lvl0_a1->fEventFlag[0] && bptx2) {
        h15->Fill(lvl0_a1->BunchID());
      }

      // TDC
      int ich = 0;
      for (int i = 0; i < 7; ++i) {
        if (i == 0)
          ich = 2;  // BPTX1
        else if (i == 1)
          ich = 3;  // BPTX2
        else if (i == 2)
          ich = 4;  // ShowerTrigger
        else if (i == 3)
          ich = 8;  // 20mm Discriminator
        else if (i == 4)
          ich = 9;  // 40mm Discriminator
        else if (i == 5)
          ich = 6;  // Arm1 FC
        else if (i == 6)
          ich = 7;  // Arm2 FC
        for (int ihit = 0; ihit < 16; ++ihit) {
          // cout << "TDCFlag   : " << lvl0_a1->fTdcFlag[ich][ihit] << endl;
          // cout << "TDC  : " << lvl0_a1->fTdc[ich][ihit] << endl;
          if (lvl0_a1->fTdcFlag[ich][ihit] < 0) {
            break;
          }
          h18[i]->Fill(lvl2_a1->fTdc[ich][ihit]);
          // cout << "a1 Fill : ch " << ich << ", hit" << ihit << endl;
        }
      }

      // DSC
      for (int il = 0; il < 16; ++il) {
        if (lvl0_a1->IsDsc(0, il)) {
          h46[il]->Fill(lvl2_a1->fCalorimeter[0][il]);
          for (int ir = 0; ir < 2; ir++) {
            h21[il][ir]->Fill(lvl1_a1->fCalorimeter[0][il][ir]);
          }
        }
        if (lvl0_a1->IsDsc(1, il)) {
          h45[il]->Fill(lvl2_a1->fCalorimeter[1][il]);
          for (int ir = 0; ir < 2; ir++) {
            h20[il][ir]->Fill(lvl1_a1->fCalorimeter[1][il][ir]);
          }
        }
      }
      for (int i = 0; i < 4; ++i) {
        if (lvl0_a1->IsFcDsc(i)) {
          h49[i]->Fill(lvl2_a1->fFrontCounter[i]);
          h57[i]->Fill(lvl1_a1->fFrontCounter[i][0]);
          h24[i]->Fill(lvl0_a1->fFrontCounter[i][0]);
        }
      }

      // FC
      for (int i = 0; i < 4; ++i) {
        if (lvl0_a1->fEventFlag[0]) {
          for (int ir = 0; ir < 2; ir++) {
            h26[i][ir]->Fill(lvl0_a1->fFrontCounter[i][ir]);
          }
          h55[i]->Fill(lvl1_a1->fFrontCounter[i][0]);
          h51[i]->Fill(lvl2_a1->fFrontCounter[i]);
        } else if (lvl0_a1->fEventFlag[1]) {
          for (int ir = 0; ir < 2; ir++) {
            h31[i][ir]->Fill(lvl0_a1->fFrontCounter[i][ir]);
            h41[i][ir]->Fill(lvl0_a1->fFrontCounter[i][ir] - ped0_a1->fFrontCounter[i][ir]);
          }
        }
      }

      // Laser
      for (int it = 0; it < 2; it++) {
        if (lvl0_a1->fEventFlag[0]) {
          h29[it]->Fill(lvl0_a1->fLaser[it]);
        } else if (lvl0_a1->fEventFlag[1]) {
          h33[it]->Fill(lvl0_a1->fLaser[it]);
          h43[it]->Fill(lvl0_a1->fLaser[it] - ped0_a1->fLaser[it]);
        }
      }

      // ZDC
      for (int im = 0; im < 3; im++) {
        for (int ir = 0; ir < 2; ir++) {
          h35[im][ir]->Fill(lvl0_a1->fZdc[im][ir]);
        }
      }

      nev_a1++;
    }

    if (ev->Check("lvl2_a2") == 1 && arm2_flag) {
      Level0<Arm2Params>* lvl0_a2 = (Level0<Arm2Params>*)ev->Get("lvl0_a2");
      Level1<Arm2Params>* lvl1_a2 = (Level1<Arm2Params>*)ev->Get("lvl1_a2");
      Level2<Arm2Params>* lvl2_a2 = (Level2<Arm2Params>*)ev->Get("lvl2_a2");
      Level0<Arm2Params>* ped0_a2 = (Level0<Arm2Params>*)ev->Get("ped0_a2");

      // cout << "Arm2\n" << endl;

      // Calorimeter
      for (int it = 0; it < Arm2Params::kCalNtower; it++) {
        for (int il = 0; il < Arm2Params::kCalNlayer; il++) {
          if (lvl0_a2->fEventFlag[0]) {
            h54[it][il]->Fill(lvl2_a2->fCalorimeter[it][il]);
            for (int ir = 0; ir < 2; ir++) {
              h6[it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              h28[it][il][ir]->Fill(lvl1_a2->fCalorimeter[it][il][ir]);
              if (lvl0_a2->IsShowerTrg()) {
                h60[0][it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              } else if (lvl0_a2->IsPi0Trg()) {
                h60[1][it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              } else if (lvl0_a2->IsHighEMTrg()) {
                h60[2][it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              } else if (lvl0_a2->IsHadronTrg()) {
                h60[3][it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              } else if (lvl0_a2->IsZdcTrg()) {
                h60[4][it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              }
            }
          } else if (lvl0_a2->fEventFlag[1]) {
            for (int ir = 0; ir < 2; ir++) {
              h8[it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              h39[it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir]);
              h40[it][il][ir]->Fill(lvl0_a2->fCalorimeter[it][il][ir] - ped0_a2->fCalorimeter[it][il][ir]);
            }
          }
        }
      }
      // cout << "end A2cal\n" << endl;
      // Silicon
      for (int il = 0; il < Arm2Params::kPosNlayer; il++) {
        for (int iv = 0; iv < Arm2Params::kPosNview; iv++) {
          for (int is = 0; is < Arm2Params::kPosNsample; is++) {
            for (int cha = 0; cha < Arm2Params::kPosNchannel[0]; cha++) {
              h7[0][il][iv][is]->Fill(cha, lvl2_a2->fPosDet[0][il][iv][cha][is]);
            }
          }
        }
      }

      // Flag
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 32; j++) {
          if (lvl0_a2->fEventFlag[0]) {
            if ((lvl0_a2->fFlag[i] >> j) & 0x1) {
              h9[0][i]->Fill(j);
            }
          } else if (lvl0_a2->fEventFlag[1]) {
            if ((lvl0_a2->fFlag[i] >> j) & 0x1) {
              h9[1][i]->Fill(j);
            }
          }
        }
      }

      // Bunch configration
      // bool IsL2TA = lvl0_a2->IsShowerTrg();
      // bool IsL2TSpecial = lvl0_a2->IsSpecialTrg();
      h12->Fill(lvl0_a2->BunchID());
      // if(lvl0_a2->fEventFlag[0] && (IsL2TA || IsL2TSpecial)){
      // h13->Fill(lvl0_a2->fCounter[22]);
      //}
      bool bptx1 = lvl0_a2->IsBeam1();
      bool bptx2 = lvl0_a2->IsBeam2();
      if (lvl0_a2->fEventFlag[0] && bptx1) {
        h16->Fill(lvl0_a2->BunchID());
      }
      if (lvl0_a2->fEventFlag[0] && bptx2) {
        h17->Fill(lvl0_a2->BunchID());
      }

      // TDC
      int ich = 0;
      for (int i = 0; i < 7; ++i) {
        if (i == 0)
          ich = 2;  // BPTX1
        else if (i == 1)
          ich = 3;  // BPTX2
        else if (i == 2)
          ich = 4;  // ShowerTrigger
        else if (i == 3)
          ich = 8;  // 20mm Discriminator
        else if (i == 4)
          ich = 9;  // 40mm Discriminator
        else if (i == 5)
          ich = 6;  // Arm1 FC
        else if (i == 6)
          ich = 7;  // Arm2 FC
        for (int ihit = 0; ihit < 16; ++ihit) {
          if (lvl0_a2->fTdcFlag[ich][ihit] < 0) {
            break;
          }
          h19[i]->Fill(lvl2_a2->fTdc[ich][ihit]);
          // cout << "a2 Fill : ch " << ich << ", hit" << ihit << endl;
        }
      }

      // DSC
      for (int il = 0; il < 16; ++il) {
        if (lvl0_a2->IsDsc(0, il)) {
          h48[il]->Fill(lvl2_a2->fCalorimeter[0][il]);
          for (int ir = 0; ir < 2; ir++) {
            h23[il][ir]->Fill(lvl1_a2->fCalorimeter[0][il][ir]);
          }
        }
        if (lvl0_a2->IsDsc(1, il)) {
          h47[il]->Fill(lvl2_a2->fCalorimeter[1][il]);
          for (int ir = 0; ir < 2; ir++) {
            h22[il][ir]->Fill(lvl1_a2->fCalorimeter[1][il][ir]);
          }
        }
      }
      for (int i = 0; i < 4; ++i) {
        if (lvl0_a2->IsFcDsc(i)) {
          h25[i]->Fill(lvl0_a2->fFrontCounter[i][0]);
          h58[i]->Fill(lvl1_a2->fFrontCounter[i][0]);
          h50[i]->Fill(lvl2_a2->fFrontCounter[i]);
        }
      }

      // FC
      for (int i = 0; i < 4; ++i) {
        if (lvl0_a2->fEventFlag[0]) {
          for (int ir = 0; ir < 2; ir++) {
            h27[i][ir]->Fill(lvl0_a2->fFrontCounter[i][ir]);
          }
          h56[i]->Fill(lvl1_a2->fFrontCounter[i][0]);
          h52[i]->Fill(lvl2_a2->fFrontCounter[i]);
        } else if (lvl0_a2->fEventFlag[1]) {
          for (int ir = 0; ir < 2; ir++) {
            h32[i][ir]->Fill(lvl0_a2->fFrontCounter[i][ir]);
            h42[i][ir]->Fill(lvl0_a2->fFrontCounter[i][ir] - ped0_a2->fFrontCounter[i][ir]);
          }
        }
      }

      // Laser
      for (int it = 0; it < 2; it++) {
        if (lvl0_a2->fEventFlag[0]) {
          h30[it]->Fill(lvl0_a2->fLaser[it]);
        } else if (lvl0_a2->fEventFlag[1]) {
          h34[it]->Fill(lvl0_a2->fLaser[it]);
          h44[it]->Fill(lvl0_a2->fLaser[it] - ped0_a2->fLaser[it]);
        }
      }

      // ZDC
      for (int im = 0; im < 3; im++) {
        for (int ir = 0; ir < 2; ir++) {
          h36[im][ir]->Fill(lvl0_a2->fZdc[im][ir]);
        }
      }

      // cout << "end Arm2\n" <<endl;
      nev_a2++;
    }
    /* Clear event */
    ev->HeaderClear();
    ev->ObjDelete();
    nev++;
  }
  cout << "Number of analyzed events = " << nev << endl;
  cout << "Number of analyzed a1 events = " << nev_a1 << endl;
  cout << "Number of analyzed a2 events = " << nev_a2 << endl;

  // Write the histograms to the output file
  ofile->Write();
  ofile->Close();

  return 0;
}
