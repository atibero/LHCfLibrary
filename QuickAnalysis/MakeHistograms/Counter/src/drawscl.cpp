#include <LHCfRunInfoTable.h>
#include <LHCfRunInfoTable_add.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TRint.h>
#include <TStyle.h>
#include <gsetup.h>

#include <Arm1Params.hh>
#include <Arm2Params.hh>
#include <LHCfEvent.hh>
#include <Level0.hh>
#include <Level1.hh>
#include <Level2.hh>
#include <Level2Hist.hh>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>

using namespace std;

using namespace nLHCf;

char *GetChar(const char *format, int val);
char *GetChar(const char *format, double val);

int main(int argc, char **argv) {
  gsetup();

  // ++++++++ Manage optioons +++++++++++++++++++++++++++++

  char datafile[256] = "";
  char runinfofile[256] = "";
  char printpath[256] = "graphs/";
  bool savecanvas = false;
  int run = -1;
  int subrun = -1;
  TString calibfile = "";

  for (int ic = 0; ic < argc; ic++) {
    if (strcmp(argv[ic], "-i") == 0) {
      strcpy(datafile, argv[++ic]);
      strcpy(argv[ic], "");
    }
    if (strcmp(argv[ic], "-nodraw") == 0) {
      strcpy(argv[ic], "-b");
    }
    if (strcmp(argv[ic], "-print") == 0) {
      savecanvas = true;
      if ((ic + 1) == argc || *(argv[ic + 1]) == '-') {
        strcpy(printpath, "./graphs");
      } else {
        strcpy(printpath, argv[++ic]);
        strcpy(argv[ic], "");
      }
    }
    if (strcmp(argv[ic], "-runinfo") == 0) {
      strcpy(runinfofile, argv[++ic]);
      strcpy(argv[ic], "");
    }
    if (strcmp(argv[ic], "-run") == 0) {
      run = atoi(argv[++ic]);
    }
    if (strcmp(argv[ic], "-subrun") == 0) {
      subrun = atoi(argv[++ic]);
    }
    if (strcmp(argv[ic], "-calib") == 0) {
      calibfile = argv[++ic];
    }
    if (strcmp(argv[ic], "-h") == 0 || strcmp(argv[ic], "-help") == 0 || argc == 1) {
      cout << endl
           << " drawscl ---------------------------------------------------------- \n"
           << "  -run            :  run number\n"
           << "  -i       [file] :  run data file\n"
           << "  -runinfo [file] :  runinfo file\n"
           << "  -nodraw         :  Not draw any histograms. \n"
           << "  -print [path]   :  Save Canvases\n"
           << "  -q,-b,          :  ROOT options. \n"
           << endl;
      return 0;
    }
  }

  if (strcmp(datafile, "") == 0 || run < 0) {
    cerr << "Error: Give a filename." << endl;
    exit(-1);
  }

  TRint app("drawruninfo", &argc, argv, 0, 0, kTRUE);

  // ++++++ OPEN FILE +++++++++++++++++++++++++++++++++++++++
  TFile *file = new TFile(datafile);
  if (file->IsZombie()) {
    cerr << "Cannot Open " << datafile << endl;
    return -1;
  }

  Level2<Arm1Params> *a1callast = NULL;
  Level2<Arm2Params> *a2callast = NULL;

  a1callast = (Level2<Arm1Params> *)file->Get("a1cal1last");
  a2callast = (Level2<Arm2Params> *)file->Get("a2cal1last");

  // +++++ OPEN CALIBRATED FILE ++++++++++++++++++++++++++++++
  TChain *tree = new TChain("LHCfEvents");
  tree->AddFile(calibfile);
  tree->SetCacheSize(10000000);
  LHCfEvent *ev = new LHCfEvent("event", "LHCfEvent");
  tree->SetBranchAddress("ev.", &ev);
  tree->AddBranchToCache("*");

  gROOT->cd();

  int nentries = tree->GetEntries();
  int lastentry = nentries - 1;
  Level2<Arm1Params> *lvl2_a1 = NULL;
  Level2<Arm2Params> *lvl2_a2 = NULL;
  tree->GetEntry(lastentry);
  if (ev->Check("lvl2_a1") == 1) lvl2_a1 = (Level2<Arm1Params> *)ev->Get("lvl2_a1");
  if (ev->Check("lvl2_a2") == 1) lvl2_a2 = (Level2<Arm2Params> *)ev->Get("lvl2_a2");

  // +++++ OPEN INFO +++++++++++++++++++++++++++++++++++++++++
  LHCfRunInfo *info = NULL;
  LHCfRunInfoTable *infotable = new LHCfRunInfoTable();
  if (strcmp(runinfofile, "") != 0 && infotable->ReadTable(runinfofile) == OK) {
    if (infotable->GetRunInfo(run, subrun)) {
      info = infotable->GetRunInfo(run, subrun);
    }
  }

  // +++++++ DRAW +++++++++++++++++++++++++++++++++++++++++++++
  TCanvas *c = new TCanvas("c", "c", 1000, 650);
  c->cd();
  TPad *p_a1 = new TPad("p1", "", 0.0, 0.65, 1.0, 0.95);
  p_a1->Draw();
  c->cd();
  TPad *p_a2 = new TPad("p2", "", 0.0, 0.30, 1.0, 0.60);
  p_a2->Draw();
  c->cd();

  c->cd();
  TText *text = new TText();
  text->DrawTextNDC(0.01, 0.90, "ARM1 COUNTERS");
  text->DrawTextNDC(0.01, 0.55, "ARM2 COUNTERS");

  Level2Hist<Arm1Params> *A1Hist = new Level2Hist<Arm1Params>();
  A1Hist->Initialize();
  Level2Hist<Arm2Params> *A2Hist = new Level2Hist<Arm2Params>();
  A2Hist->Initialize();

  A1Hist->c2pad2 = p_a1;
  A2Hist->c2pad2 = p_a2;

  if (lvl2_a1) {
    cout << "draw arm1" << endl;
    A1Hist->DrawCanvas2();
    A1Hist->SetDisplayVersion_pp2022();
    A1Hist->Fill(lvl2_a1);
    A1Hist->UpdateCanvas2();
    p_a1->Modified();
    p_a1->Update();
  }
  if (lvl2_a2) {
    cout << "draw arm2" << endl;
    A2Hist->DrawCanvas2();
    A2Hist->SetDisplayVersion_pp2022();
    A2Hist->Fill(lvl2_a2);
    A2Hist->UpdateCanvas2();
    p_a2->Modified();
    p_a2->Update();
  }

  // ============= END OF COUNTER 1 ==============

  // ************** DRAW DAQ LIVE TIME ***********

  // double deltat = 1.;
  // double x,y,offx,offy;
  // char tmp[256];
  // int icounter1, icounter2, icounter3, icounter4;
  // TText *texc = new TText();
  // text->SetTextSize(0.05);
  // texc->SetTextSize(0.03);
  // texc->SetTextFont(102);
  // TCanvas *c3 = new TCanvas("c3","c",1000,600);
  // int ic=0;

  //// Arm1
  // text->DrawTextNDC(0.02,0.92,"ARM1");
  // x = 0.01;
  // y = 0.85;
  // offx = 0.09;
  // offy = 0.04;
  // ic = 0;
  // if(lvl2_a2){
  //  icounter3 = (int)(lvl2_a2->fCounter[12]);
  //}
  // if(lvl2_a1){
  //  texc->DrawTextNDC(x,y-offy*ic,"COUNTERS ON LOGIC");
  //  ic++;
  //  texc->DrawTextNDC(x,y-offy*ic,"L1T_E/L1T");
  //  texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",(int)lvl2_a1->fCounter[6]));
  // texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //  texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",(int)lvl2_a1->fCounter[5]));
  //  texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(lvl2_a1->fCounter[6])/lvl2_a1->fCounter[5]));
  //  ic++;
  //  texc->DrawTextNDC(x,y-offy*ic,"SHW_L3T/SHW_TRG");
  //    icounter1 = (int)(lvl2_a1->fCounter[15]);
  //    icounter2 = (int)(lvl2_a1->fCounter[11]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SPE_L3T/SPE_TRG");
  //    icounter1 = (int)(lvl2_a1->fCounter[16]);
  //    icounter2 = (int)(lvl2_a1->fCounter[13]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"STRG/SLOGIC");
  //    icounter1 = (int)(lvl2_a1->fCounter[7]);
  //    icounter2 = (int)(lvl2_a1->fCounter[9]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"LTRG/LLOGIC");
  //    icounter1 = (int)(lvl2_a1->fCounter[8]);
  //    icounter2 = (int)(lvl2_a1->fCounter[10]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SHW_TRG/SHW_BPTXX");
  //    icounter1 = (int)(lvl2_a1->fCounter[11]);
  //    icounter2 = (int)(lvl2_a1->fCounter[12]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SPE_TRG/SPE_BPTXX");
  //    icounter1 = (int)(lvl2_a1->fCounter[13]);
  //    icounter2 = (int)(lvl2_a1->fCounter[14]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_SHOWER/L3T");
  //    icounter1 = (int)(lvl2_a1->fCounter[15]);
  //    icounter2 = (int)(lvl2_a1->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_SPECIAL/L3T");
  //    icounter1 = (int)(lvl2_a1->fCounter[16]);
  //    icounter2 = (int)(lvl2_a1->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_PEDE/L3T");
  //    icounter1 = (int)(lvl2_a1->fCounter[17]);
  //    icounter2 = (int)(lvl2_a1->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_L1T/L3T");
  //    icounter1 = (int)(lvl2_a1->fCounter[18]);
  //    icounter2 = (int)(lvl2_a1->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"A1_L3T/L3T_OR");
  //    icounter1 = (int)(lvl2_a1->fCounter[19]);
  //    icounter2 = (int)(lvl2_a1->fCounter[20]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"COIN_SHW/A1_SHW");
  //    icounter1 = (int)(lvl2_a1->fCounter[21]& 0xFFFF);
  //    icounter2 = (int)(lvl2_a1->fCounter[12]+icounter3 - icounter1);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",(int)lvl2_a1->fCounter[12]));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter2)/lvl2_a1->fCounter[12]));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"ATLAS_LHCF/L3T_OR");
  //    icounter1 = (int)(lvl2_a1->fCounter[21]>>16);
  //    icounter2 = (int)(lvl2_a1->fCounter[20]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"FC_TRG/FC_LOGIC");
  //    icounter1 = (int)(lvl2_a1->fCounter[32]);
  //    icounter2 = (int)(lvl2_a1->fCounter[31]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SHW_AND_FC/COIN_FC");
  //    icounter1 = (int)(lvl2_a1->fCounter[33]);
  //    icounter2 = (int)(lvl2_a1->fCounter[34]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",(int)lvl2_a1->fCounter[12]));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter2)/lvl2_a1->fCounter[12]));
  //  }
  //  ic++;
  //
  //  // Arm2
  //  text->DrawTextNDC(0.52,0.92,"ARM2");
  //  x = 0.51;
  //  y = 0.85;
  //  offx = 0.09;
  //  offy = 0.04;
  //  ic = 0;
  //  if(lvl2_a1){
  //    icounter4 = (int)(lvl2_a1->fCounter[12]);
  //  }
  //  if(lvl2_a2){
  //    texc->DrawTextNDC(x,y-offy*ic,"COUNTERS ON LOGIC");
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"L1T_E/L1T");
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",(int)lvl2_a2->fCounter[6]));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",(int)lvl2_a2->fCounter[5]));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(lvl2_a2->fCounter[6])/lvl2_a2->fCounter[5]));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SHW_L3T/SHW_TRG");
  //    icounter1 = (int)(lvl2_a2->fCounter[15]);
  //    icounter2 = (int)(lvl2_a2->fCounter[11]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SPE_L3T/SPE_TRG");
  //    icounter1 = (int)(lvl2_a2->fCounter[16]);
  //    icounter2 = (int)(lvl2_a2->fCounter[13]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"STRG/SLOGIC");
  //    icounter1 = (int)(lvl2_a2->fCounter[7]);
  //    icounter2 = (int)(lvl2_a2->fCounter[9]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"LTRG/LLOGIC");
  //    icounter1 = (int)(lvl2_a2->fCounter[8]);
  //    icounter2 = (int)(lvl2_a2->fCounter[10]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SHW_TRG/SHW_BPTXX");
  //    icounter1 = (int)(lvl2_a2->fCounter[11]);
  //    icounter2 = (int)(lvl2_a2->fCounter[12]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SPE_TRG/SPE_BPTXX");
  //    icounter1 = (int)(lvl2_a2->fCounter[13]);
  //    icounter2 = (int)(lvl2_a2->fCounter[14]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_SHOWER/L3T");
  //    icounter1 = (int)(lvl2_a2->fCounter[15]);
  //    icounter2 = (int)(lvl2_a2->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_SPECIAL/L3T");
  //    icounter1 = (int)(lvl2_a2->fCounter[16]);
  //    icounter2 = (int)(lvl2_a2->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_PEDE/L3T");
  //    icounter1 = (int)(lvl2_a2->fCounter[17]);
  //    icounter2 = (int)(lvl2_a2->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"TRG_L1T/L3T");
  //    icounter1 = (int)(lvl2_a2->fCounter[18]);
  //    icounter2 = (int)(lvl2_a2->fCounter[19]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"A2_L3T/L3T_OR");
  //    icounter1 = (int)(lvl2_a2->fCounter[19]);
  //    icounter2 = (int)(lvl2_a2->fCounter[20]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"COIN_SHW/A2_SHW");
  //    icounter1 = (int)(lvl2_a2->fCounter[21]& 0xFFFF);
  //    icounter2 = (int)(lvl2_a2->fCounter[12]+icounter4 - icounter1);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",(int)lvl2_a2->fCounter[12]));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter2)/lvl2_a2->fCounter[12]));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"ATLAS_LHCF/L3T_OR");
  //    icounter1 = (int)(lvl2_a2->fCounter[21]>>16);
  //    icounter2 = (int)(lvl2_a2->fCounter[20]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"FC_TRG/FC_LOGIC");
  //    icounter1 = (int)(lvl2_a2->fCounter[32]);
  //    icounter2 = (int)(lvl2_a2->fCounter[31]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter1));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter1)/icounter2));
  //    ic++;
  //    texc->DrawTextNDC(x,y-offy*ic,"SHW_AND_FC/COIN_FC");
  //    icounter1 = (int)(lvl2_a2->fCounter[33]);
  //    icounter2 = (int)(lvl2_a2->fCounter[34]);
  //    texc->DrawTextNDC(x+offx*2.0,y-offy*ic,GetChar("%08d",icounter2));
  //    texc->DrawTextNDC(x+offx*3.1,y-offy*ic,"/");
  //    texc->DrawTextNDC(x+offx*3.2,y-offy*ic,GetChar("%08d",(int)lvl2_a2->fCounter[12]));
  //    texc->DrawTextNDC(x+offx*4.2,y-offy*ic,GetChar("%8.4lf",(double)(icounter2)/lvl2_a2->fCounter[12]));
  //  }
  //  ic++;
  //
  //  c3->Update();

  // ++++++++++ SAVE CANVASES ++++++++++++++++++
  char ch_run[20] = "";
  char printname[256];
  if (savecanvas) {
    if (run > 0) {
      sprintf(ch_run, "run%05d_", run);
    }
    sprintf(printname, "%s/Counter1.gif", printpath);
    c->Print(printname);
    // sprintf(printname,"%s/Counter2.gif",printpath);
    // c3->Print(printname);
  }
  app.Run();
  return 0;
}

char *GetChar(const char *format, int val) {
  static char buf[256];
  sprintf(buf, format, val);
  return buf;
}

char *GetChar(const char *format, double val) {
  static char buf[256];
  sprintf(buf, format, val);
  return buf;
}
