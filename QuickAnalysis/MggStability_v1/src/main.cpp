#include <TRint.h>

#include "MggStability.hh"
#include "TApplication.h"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (or input file if ending with \".root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = -1)\n");
  printf("\t-n <niteration>\t\tIteration number (default = 1)\n");
  printf("\t-p <particle>\t\tparticle to analysze (pion or eta) (default = pion)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"stability.root\")\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  int first_run = -1;
  int last_run = -1;
  int n_iteration = 1;
  string particle = "pion";
  string output_file = "stability.root";

  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"niteration", required_argument, NULL, 'n'},
      {"particle", required_argument, NULL, 'p'},
      {"output", required_argument, NULL, 'o'},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'n':
        if (optarg) n_iteration = atoi(optarg);
        break;
      case 'p':
        if (optarg) particle = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d -> %d\n", first_run, last_run);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  MggStability stability;
  stability.SetInputDir(input_dir.c_str());
  stability.SetFirstRun(first_run);
  stability.SetLastRun(last_run);
  stability.SetIterationNumber(n_iteration);
  stability.SetParticle(particle.c_str());
  stability.SetOutputName(output_file.c_str());
  stability.StabilityCalculation();

  return 0;
}
