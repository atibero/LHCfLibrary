#include "MggStability.hh"

#include <TH1F.h>
#include <TH2D.h>
#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <chrono>
#include <cstdlib>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "CoordinateTransformation.hh"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "Utils.hh"

using namespace nLHCf;
using namespace std::chrono;
typedef CoordinateTransformation CT;
typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
MggStability::MggStability() : fFirstRun(-1), fLastRun(-1) { fOutputFile = nullptr; }

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
MggStability::~MggStability() { delete fOutputFile; }

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/

void MggStability::SetInputHisto() {
  UT::Printf(UT::kPrintInfo, "Setting input Mgg histogram...");
  fflush(stdout);

  // photon
  // h_ene_photon[0]= new TH1D("h_energy_st_photon", Form("Photon energy small tower run %d-%d;E_{#gamma} [GeV];Counts
  // ",fFirstRun, fLastRun), 50, 0,6500); h_ene_photon[1]= new TH1D("h_energy_lt_photon", Form("Photon energy large
  // tower run %d-%d;E_{#gamma} [GeV];Counts ",fFirstRun, fLastRun), 50, 0,6500); h_pt_photon[0]= new
  // TH1D("h_pt_st_photon", Form("Photon p_{T} small tower run %d-%d;;p_{t,#gamma} [GeV];Counts ",fFirstRun, fLastRun),
  // 50, 0,1.5); h_pt_photon[1]= new TH1D("h_pt_lt_photon", Form("Photon p_{T} large tower run %d-%d;;p_{t,#gamma} [GeV]
  // [GeV];Counts ",fFirstRun, fLastRun), 50, 0,1.5); h_pos_photon= new TH2D("h_pos_photon", Form("Hit-map photons run
  // %d-%d;x[mm];y[mm] ",fFirstRun, fLastRun), 50,-50, 30,50, -20, 60); h_pos_photon_thr= new TH2D("h_pos_photon_thr",
  // Form("Hit-map photons run %d-%d E>1 TeV;x[mm];y[mm] ",fFirstRun, fLastRun), 50,-50, 30,50, -20, 60); neutron
  // h_ene_neutron[0]= new TH1D("h_energy_st_neutron", Form("Neutron energy small tower run %d-%d;E_{neutron}
  // [GeV];Counts ",fFirstRun, fLastRun), 50, 0,14000); h_ene_neutron[1]= new TH1D("h_energy_lt_neutron", Form("Neutron
  // energy large tower run %d-%d;E_{neutron} [GeV];Counts ",fFirstRun, fLastRun), 50, 0,14000); h_pt_neutron[0]= new
  // TH1D("h_pt_st_neutron", Form("Neutron p_{T} small towe run %d-%d;p_{t,neutron} [GeV];Counts ",fFirstRun, fLastRun),
  // 50, 0,3); h_pt_neutron[1]= new TH1D("h_pt_lt_neutron", Form("Neutron p_{T} large tower run %d-%d;p_{t,neutron}
  // [GeV];Counts ",fFirstRun, fLastRun), 50, 0,3); h_pos_neutron= new TH2D("h_pos_neutron", Form("Hit-map neutron run
  // %d-%d;x[mm];y[mm] ",fFirstRun, fLastRun), 50,-50, 30,50, -20, 60); h_pos_neutron_thr= new TH2D("h_pos_neutron_thr",
  // Form("Hit-map neutron run %d-%d E>1 TeV;x[mm];y[mm] ",fFirstRun, fLastRun), 50,-50, 30,50, -20, 60); pi and eta
  h_mgg = new TH1D("h_mgg", Form("Invariant mass run %d-%d;M_{#gamma#gamma} [MeV];Counts ", fFirstRun, fLastRun), 50, 0,
                   700);
  // h_pos_pi_eta = new TH2D("h_pos_pi_eta", Form("Hit-map #pi/#eta run %d-%d;x[mm];y[mm] ",fFirstRun, fLastRun),
  // 50,-50, 30,50, -20, 60); h_ene_pi= new TH1D("h_energy_pion", Form("#pi^{0} energy run %d-%d;E_{#pi^{0}}
  // [GeV];Counts
  // ",fFirstRun, fLastRun), 50, 0,6500); h_ene_eta= new TH1D("h_energy_eta", Form("#eta energy run %d-%d;E_{#eta}
  // [GeV];Counts ",fFirstRun, fLastRun), 10, 0,6500); h_pt_pi= new TH1D("h_pt_pion", Form("#pi^{0} p_{T} run
  // %d-%d;p_{t, #pi^{0}} [GeV];Counts ",fFirstRun, fLastRun), 50, 0,1.5); h_pt_eta= new TH1D("h_pt_eta", Form("#eta
  // p_{T} run %d-%d;p_{t, #eta} [GeV];Counts ",fFirstRun, fLastRun), 10, 0,1.5);

  if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        /*h_ene_photon[0]->Add((TH1D*)ifile.Get("h_energy_st_photon"));
    h_ene_photon[1]->Add((TH1D*)ifile.Get("h_energy_lt_photon"));
        h_pt_photon[0]->Add((TH1D*)ifile.Get("h_pt_st_photon"));
    h_pt_photon[1]->Add((TH1D*)ifile.Get("h_pt_lt_photon"));
    h_pos_photon->Add((TH2D*)ifile.Get("h_pos_photon"));
    h_pos_photon_thr->Add((TH2D*)ifile.Get("h_pos_photon_thr"));

        h_ene_neutron[0]->Add((TH1D*)ifile.Get("h_energy_st_neutron"));
    h_ene_neutron[1]->Add((TH1D*)ifile.Get("h_energy_lt_neutron"));
    h_pt_neutron[0]->Add((TH1D*)ifile.Get("h_pt_st_neutron"));
    h_pt_neutron[1]->Add((TH1D*)ifile.Get("h_pt_lt_neutron"));
    h_pos_neutron->Add((TH2D*)ifile.Get("h_pos_neutron"));
    h_pos_neutron_thr->Add((TH2D*)ifile.Get("h_pos_neutron_thr"));*/

        h_mgg->Add((TH1D *)ifile.Get("h_mgg"));
        /*h_pos_pi_eta->Add((TH2D*)ifile.Get("h_pos_pi_eta"));
        h_ene_pi->Add((TH1D*)ifile.Get("h_energy_pion"));
    h_ene_eta->Add((TH1D*)ifile.Get("h_energy_eta"));
        h_pt_pi->Add((TH1D*)ifile.Get("h_pt_pion"));
    h_pt_eta->Add((TH1D*)ifile.Get("h_pt_eta"));*/

        ifile.Close();
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            /*h_ene_photon[0]->Add((TH1D*)ifile.Get("h_energy_st_photon"));
        h_ene_photon[1]->Add((TH1D*)ifile.Get("h_energy_lt_photon"));
            h_pt_photon[0]->Add((TH1D*)ifile.Get("h_pt_st_photon"));
        h_pt_photon[1]->Add((TH1D*)ifile.Get("h_pt_lt_photon"));
        h_pos_photon->Add((TH2D*)ifile.Get("h_pos_photon"));
        h_pos_photon_thr->Add((TH2D*)ifile.Get("h_pos_photon_thr"));

            h_ene_neutron[0]->Add((TH1D*)ifile.Get("h_energy_st_neutron"));
        h_ene_neutron[1]->Add((TH1D*)ifile.Get("h_energy_lt_neutron"));
        h_pt_neutron[0]->Add((TH1D*)ifile.Get("h_pt_st_neutron"));
        h_pt_neutron[1]->Add((TH1D*)ifile.Get("h_pt_lt_neutron"));
        h_pos_neutron->Add((TH2D*)ifile.Get("h_pos_neutron"));
        h_pos_neutron_thr->Add((TH2D*)ifile.Get("h_pos_neutron_thr"));*/

            h_mgg->Add((TH1D *)ifile.Get("h_mgg"));
            /*h_pos_pi_eta->Add((TH2D*)ifile.Get("h_pos_pi_eta"));
            h_ene_pi->Add((TH1D*)ifile.Get("h_energy_pion"));
        h_ene_eta->Add((TH1D*)ifile.Get("h_energy_eta"));
            h_pt_pi->Add((TH1D*)ifile.Get("h_pt_pion"));
        h_pt_eta->Add((TH1D*)ifile.Get("h_pt_eta"));*/
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory or find file!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::SetOutputGraphs() {
  if (fParticle == "pion") {
    g_stability = new TGraphErrors();
    g_stability->SetName("g_stability_pion");
    g_stability->SetTitle("Stability plot #pi^{0} peak");
    g_stability->SetMarkerStyle(kFullCircle);
    g_stability->SetMarkerColor(kBlack);
    g_stability->SetLineColor(kBlack);
    g_stability->SetLineWidth(3);
    g_stability->GetXaxis()->SetTitle("Run number");
    g_stability->GetYaxis()->SetTitle("pi0 peak [MeV]");

    fOutputFile->cd();
    g_stability->Write();
  }
  if (fParticle == "eta") {
    g_stability = new TGraphErrors();
    g_stability->SetName("g_stability_eta");
    g_stability->SetTitle("Stability plot  #eta");
    g_stability->SetMarkerStyle(kFullCircle);
    g_stability->SetMarkerColor(kBlack);
    g_stability->SetLineColor(kBlack);
    g_stability->SetLineWidth(3);
    g_stability->GetXaxis()->SetTitle("Run number");
    g_stability->GetYaxis()->SetTitle("Eta peak [MeV]");

    fOutputFile->cd();
    g_stability->Write();
  }
}

void MggStability::GetOutputGraphs() {
  if (fParticle == "pion") {
    g_stability = (TGraphErrors *)fOutputFile->Get("g_stability_pion");
  }

  if (fParticle == "eta") {
    g_stability = (TGraphErrors *)fOutputFile->Get("g_stability_eta");
  }
}

void MggStability::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  g_stability->Write();

  /*h_ene_photon[0]->Write();
  h_ene_photon[1]->Write();
  h_pt_photon[0]->Write();
  h_pt_photon[1]->Write();
  h_pos_photon->Write();
  h_pos_photon_thr->Write();

  h_ene_neutron[0]->Write();
  h_ene_neutron[1]->Write();
  h_pt_neutron[0]->Write();
  h_pt_neutron[1]->Write();
  h_pos_neutron->Write();
  h_pos_neutron_thr->Write();*/

  h_mgg->Write();
  /*h_pos_pi_eta->Write();
  h_ene_pi->Write();
  h_ene_eta->Write();
  h_pt_pi->Write();
  h_pt_eta->Write();*/

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::FastFit(Double_t inf, Double_t sup) {
  fFit = new TF1("fFit", "[0]*TMath::Exp(-(x-[1])^2/(2*[2]^2))+[3]+[4]*x+[5]*x^2");
  fFit->SetParameter(0, 1000);
  fFit->SetParameter(1, (sup + inf) / 2);
  fFit->SetParameter(2, (sup - inf) / 2);
  h_mgg->Fit(fFit, "Q+", "", inf, sup);
}

void MggStability::GoodFit(Double_t inf, Double_t sup) {
  fFit = new TF1("fFit", "[0]*TMath::Exp(-(x-[1])^2/(2*[2]^2))+[3]+[4]*x+[5]*x^2");
  fFit->SetParameter(0, 1000);
  fFit->SetParameter(1, (sup + inf) / 2);
  fFit->SetParameter(2, (sup - inf) / 2);
  h_mgg->Fit(fFit, "Q+", "", inf, sup);
}

void MggStability::StabilityCalculation() {
  SetInputHisto();
  if (fIterationNumber == 0) {
    fOutputFile = new TFile(fOutputName, "RECREATE");
    SetOutputGraphs();
  } else {
    fOutputFile = new TFile(fOutputName, "READ");
    GetOutputGraphs();
    fOutputFile->Close();
    fOutputFile = new TFile(fOutputName, "RECREATE");
  }

  if (fParticle == "pion") {
    FastFit(100, 160);
  }
  if (fParticle == "eta") {
    FastFit(510, 570);
  }

  Double_t run_number = (fFirstRun + fLastRun) / 2;
  Double_t peak = fFit->GetParameter(1);
  Double_t run_width = (fLastRun - fFirstRun) / 2;
  Double_t peak_err = fFit->GetParError(1);

  for (Int_t i = 0; i < fIterationNumber; i++) {
    // g_stability->SetPoint(i,g_stability->GetPointX(i), g_stability->GetPointY(i));
    g_stability->SetPoint(i, g_stability->GetX()[i], g_stability->GetY()[i]);
    g_stability->SetPointError(i, run_width, g_stability->GetErrorY(i));
  }

  g_stability->SetPoint(fIterationNumber, run_number, peak);
  g_stability->SetPointError(fIterationNumber, run_width, peak_err);

  WriteToOutput();
  CloseFiles();
}

namespace nLHCf {
class MggStability;
}
