#ifndef MggStability_HH
#define MggStability_HH

#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TString.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfParams.hh"
#include "TGraph.h"
#include "TGraphErrors.h"

using namespace std;

namespace nLHCf {

class MggStability {
 public:
  MggStability();
  ~MggStability();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;      // input file name
  TString fInputDir;       // input file directory
  Int_t fFirstRun;         // first run number
  Int_t fLastRun;          // last run number
  Int_t fIterationNumber;  // Number of iteration, or number of Graph's point
  // TFile            *fInputFile;
  TString fParticle;  // particle to be analized, pi or eta

  TString fOutputName;  // output file name
  TFile *fOutputFile;   // output file

  /*--- Input histogram ---*/  // 0 small tower, 1 large tower
  // photon
  // TH1D* h_ene_photon[2]; //single photon energy
  // TH1D* h_pt_photon[2]; //single photon pt
  // TH2D* h_pos_photon;    //single photon hitmap
  // TH2D* h_pos_photon_thr;    //single photon hitmap (E>1 TeV)
  // neutron
  // TH1D* h_ene_neutron[2]; //neutron energy
  // TH1D* h_pt_neutron[2]; //neutron pt
  // TH2D* h_pos_neutron;    //neutron hitmap
  // TH2D* h_pos_neutron_thr;    //neutron hitmap (E>1 TeV)
  // pion and Eta
  TH1D *h_mgg;  // diphoton invariant mass
  // TH2D *h_pos_pi_eta;     //type I photon pair hitmap
  // TH1D *h_ene_pi;         //pi0 (type I) energy
  // TH1D *h_ene_eta;         //eta (type I) energy
  // TH1D *h_pt_pi;         //pi0 (type I) pt
  // TH1D *h_pt_eta;         //eta (type I) pt

  /*--- Output Graph ---*/
  TGraphErrors *g_stability;  // Mgg stability graph
  /*--- Fit function ---*/
  TF1 *fFit;  // Fit function

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }                  // Set input directory
  void SetFirstRun(Int_t first) { fFirstRun = first; }                        // Set first run
  void SetLastRun(Int_t last) { fLastRun = last; }                            // Set last run
  void SetOutputName(const Char_t *name) { fOutputName = name; }              // set output file name
  void SetInputFile(const Char_t *name) { fInputFile = name; }                // set input file name
  void SetIterationNumber(Int_t iteration) { fIterationNumber = iteration; }  // set iteration number
  void SetParticle(const Char_t *particle) { fParticle = particle; }          // set particle, pi or eta

  void StabilityCalculation();  // stability plot calculation

 private:
  void SetInputHisto();                      // Make input istograms
  void SetOutputGraphs();                    // set graph, only for the first iteration
  void GetOutputGraphs();                    // get output graph
  void WriteToOutput();                      // write histos and graphs to output
  void CloseFiles();                         // close files
  void FastFit(Double_t inf, Double_t sup);  // Mgg fast fit, gaussian+2nd order polynomial
  void GoodFit(Double_t inf, Double_t sup);  // Mgg good fit, slower but much precise
};
}  // namespace nLHCf
#endif
