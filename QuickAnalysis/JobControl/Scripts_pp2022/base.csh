
set LIBRARY_BASE="/home/lhcf/LHCf/Analysis/SPS2022/LHCfLibrary/"
set BUILD_BASE="${LIBRARY_BASE}/build"
set JOB_BASE="${LIBRARY_BASE}/QuickAnalysis/JobControl/"
# Raw data and directory for QuickAnalysis
set SERVER_DATA="/nfs/lhcfds4/data5/SPS2022/Data/"
set SERVER_OUTPUT="/nfs/lhcfds4/data5/SPS2022/QuickAnalysis/"
# Working directory
if( $HOSTNAME == "lhcfds4" ) then
    set DIR_DATA=${SERVER_DATA}
    set DIR_OUTPUT=${SERVER_OUTPUT}
else if ( $HOSTNAME == "hep" ) then
    set DIR_DATA="/data/SPS2022/Data/"
    set DIR_OUTPUT="/data/SPS2022/QuickAnalysis/"      
else 
    set DIR_DATA="/data/SPS2022/Data/"
     set DIR_OUTPUT="/data/SPS2022/QuickAnalysis/" 
endif
