#!/bin/csh

set RUN=$argv[1]        #  RUN
set COMFILE=$argv[2]    #  COMMENT FILE
set OPTION=$argv[3]     #  FORCE OR NONE


set TMP=`printf '%05d' $RUN`;
set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
set QUICKANALDIR="${ANALBASEDIR}/run${TMP}"
set RECFILE="${QUICKANALDIR}/rec_run${TMP}.root"
set OUTPUTDIRARM1="${QUICKANALDIR}/lvl3_hists_arm1_run${TMP}"
set OUTPUTDIRARM2="${QUICKANALDIR}/lvl3_hists_arm2_run${TMP}"
set HISTFILEARM1="${QUICKANALDIR}/lvl3_histo_arm1_run${TMP}.root"
set HISTFILEARM2="${QUICKANALDIR}/lvl3_histo_arm2_run${TMP}.root"

set WORKDIR=/home/lhcf/LHCfLibrary/QuickAnalysis/MakeHistograms/build
set PYDIR=/home/lhcf/LHCfLibrary/QuickAnalysis/JobControl/python

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE


# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
if( ! -d $QUICKANALDIR ) then
    mkdir -p $QUICKANALDIR
endif

# MAKE DIRECTORY FOR HISTOGRAMS
if( ! -d $OUTPUTDIRARM1 ) then
    mkdir -p $OUTPUTDIRARM1
endif

if( ! -d $OUTPUTDIRARM2 ) then
    mkdir -p $OUTPUTDIRARM2
endif


#lvl3 analysis
if( (! -e $HISTFILEARM1 && ! -e $HISTFILEARM2) || $OPTION == "FORCE" )then
	/bin/rm -f $COMFILE; echo "Lvl3 histograms" > $COMFILE
	./bin/QuickAnalysise -i $RECFILE -r ${TMP} -o $HISTFILEARM1 --arm1 -v 1
	./bin/QuickAnalysise -i $RECFILE -r ${TMP} -o $HISTFILEARM2 --arm2 -v 1

    # Draw histogram
    cd $PYDIR
    mkdir $RUN
    /bin/rm -f $COMFILE; echo "Drawing hists." > $COMFILE
    python draw_lvl3.py "${HISTFILEARM1}" "${RUN}" -b -q
    mv ./${RUN}/photon_energy_st.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/photon_energy_lt.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/photon_hit_map.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/neutron_energy_st.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/neutron_energy_lt.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/neutron_hit_map.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/invariant_mass.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/pion_energy.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/eta_energy.gif ${OUTPUTDIRARM1}
    mv ./${RUN}/diphoton_hit_map.gif ${OUTPUTDIRARM1}
	
	python draw_lvl3.py "${HISTFILEARM2}" "${RUN}" -b -q
    mv ./${RUN}/photon_energy_st.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/photon_energy_lt.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/photon_hit_map.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/neutron_energy_st.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/neutron_energy_lt.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/neutron_hit_map.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/invariant_mass.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/pion_energy.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/eta_energy.gif ${OUTPUTDIRARM2}
    mv ./${RUN}/diphoton_hit_map.gif ${OUTPUTDIRARM2}
endif


