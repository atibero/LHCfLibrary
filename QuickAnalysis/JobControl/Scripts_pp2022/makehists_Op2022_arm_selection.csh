#!/bin/csh

if ($#argv == 5) then #CALLED for each SUBRUN
        set RUN=$argv[1]        #  RUN
        set SUBRUN=$argv[2]     #  SUBRUN
	set ARM=$argv[3]        #  ARM
        set COMFILE=$argv[4]    #  COMMENT FILE
        set OPTION=$argv[5]     #  FORCE OR NONE
        set TMPRUN=`printf '%05d' $RUN`;
        set TMPSUBRUN=`printf '%02d' $SUBRUN`;
        set TMPRUNTAG="${TMPRUN}_${TMPSUBRUN}";
else #CALLED for the flobal RUN
        set RUN=$argv[1]        #  RUN
	set ARM=$argv[2]
        set COMFILE=$argv[3]    #  COMMENT FILE
        set OPTION=$argv[4]     #  FORCE OR NONE
        set TMPRUN=`printf '%05d' $RUN`;
        set TMPRUNTAG="${TMPRUN}";
endif

echo "========================== makehist.csh ===================="
echo ${PWD}
date
source Scripts/base.csh

set WORKDIR=${BUILD_BASE}
#set WORKDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build
#set WORKDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build_Op2015
#set WORKDIR=/home/lhcf/LHCfLibrary/LHCfLibrary/build_Op2015 # for test on daq11
#set WORKDIR=/crhome/kondo.moe/lhcflibrary/Op2022/LHCfLibrary/LHCfLibrary/QuickAnalysis/MakeHistograms/build
#set BINDIR=${WORKDIR}/bin

set PYDIR=${WORKDIR}/../QuickAnalysis/JobControl/python
#set PYDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/QuickAnalysis/JobControl/python
#set PYDIR=/home/lhcf/LHCfLibrary/LHCfLibrary/QuickAnalysis/JobControl/python # for test on daq11

set RUNINFODIR=${WORKDIR}
#set RUNINFODIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build
#set RUNINFODIR=/home/lhcf/LHCfLibrary/LHCfLibrary/build_Op2015

set COUNTERDIR=${WORKDIR}

set BEAMCONFIGDIR=${WORKDIR}
#set BEAMCONFIGDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build
#set BEAMCONFIGDIR=/home/lhcf/LHCfLibrary/LHCfLibrary/build_Op2015

#set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
#set ANALBASEDIR="/nfs/lhcfds4/data5/Op2022/QuickAnalysis"
set ANALBASEDIR=${DIR_OUTPUT}
set INITRUNFILE="${ANALBASEDIR}/.initrun.out"
set QUICKANALDIR="${ANALBASEDIR}/run${TMPRUNTAG}"
set OUTPUTDIR="${QUICKANALDIR}/hists_run${TMPRUNTAG}"
set HISTFILE="${QUICKANALDIR}/hists_run${TMPRUNTAG}.root"
set CALIBFILE="${QUICKANALDIR}/cal_run${TMPRUNTAG}.root"
set PEDEFILE="${QUICKANALDIR}/pede_run${TMPRUNTAG}.root"
set RECFILE="${QUICKANALDIR}/rec_run${TMPRUNTAG}.root"
set RUNINFOFILE="${QUICKANALDIR}/runinfo_run${TMPRUNTAG}.dat"
set RUNDATAFILE="${QUICKANALDIR}/rundata_run${TMPRUNTAG}.root"
set HISTFILE_SERVER="${SERVER_OUTPUT}/run${TMPRUNTAG}/hists_run${TMPRUNTAG}.root"

#set A1FELOG="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test/logs/LogFrontend1/frontend_arm1_log1"
#set A2FELOG="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test/logs/LogFrontend2/frontend_arm2_log2"
set A1FELOG="/nfs/lhcfds4/data5/Op2022/DAQlogs/LogFrontend1/frontend_arm1_log2" #To be changed
set A2FELOG="/nfs/lhcfds4/data5/Op2022/DAQlogs/LogFrontend2/frontend_arm2_log2" #To be changed

set Nsub=0
set scalerflag=0

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE


# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
if( ! -d $QUICKANALDIR ) then
    mkdir -p $QUICKANALDIR
endif

# MAKE DIRECTORY FOR HISTOGRAMS
if( ! -d $OUTPUTDIR ) then
    mkdir -p $OUTPUTDIR
endif

set DO_HIST="NO"

if( $OPTION == "FORCE" ) then
    set DO_HIST="YES"
else if( ! -e $HISTFILE &&  ! -e $HISTFILE_SERVER ) then
    set DO_HIST="YES"
endif
 



if( ${DO_HIST} == "YES" )then
	if ( $#argv == 5 ) then
		# fillrundata 
		cd $RUNINFODIR
		/bin/rm -f $COMFILE; echo "Fill run information" > $COMFILE
    		echo "FILLRUNDATA"
    		./bin/FillRunData -i $CALIBFILE -rd $RUNDATAFILE -ri $RUNINFOFILE -run $RUN -subrun $SUBRUN #Need change from level0 to level3

    		# filldaqinfo
    		echo "FILLDAQINFO"
    		./bin/FillDaqInfo -run ${RUN} -subrun ${SUBRUN} -runinfo $RUNINFOFILE -rundata $RUNDATAFILE -a1log $A1FELOG -a2log $A2FELOG -tmprun ${TMPRUN}
    
   		# drawruninfo
    		echo "DRAWRUNINFO"
    		./bin/DrawRunInfo -run $RUN -subrun ${SUBRUN} -runinfo $RUNINFOFILE -print $OUTPUTDIR -q -b

    		# Make histogram
    		cd $WORKDIR
    		/bin/rm -f $COMFILE; echo "Making hists." > $COMFILE
    		echo "MAKEHIST"
		date
		    if($ARM == "arm1") then
				./bin/MakeHist -i $CALIBFILE -o $HISTFILE -a1
	        else if ($ARM == "arm2") then 
				./bin/MakeHist -i $CALIBFILE -o $HISTFILE -a2
    		endif

    	else
		# add runinfo
		cd $RUNINFODIR
		/bin/rm -f $COMFILE; echo "add run information." > $COMFILE
		echo "ADDRUNINFO"
		./bin/DrawRunInfo -run ${RUN} -add -runinfodir ${SERVER_OUTPUT} -runinfo $RUNINFOFILE -print $OUTPUTDIR -q -b

	        # hadd histograms
		/bin/rm -f $COMFILE; echo "Hadd hists." > $COMFILE
		echo "HADDHISTS"
        	rm -f ${HISTFILE}
       	        #hadd -T -k -f ${HISTFILE} ${ANALBASEDIR}/run${RUN}_*/hists_run${RUN}_*.root
		#hadd -T ${PEDEFILE} ${ANALBASEDIR}/run${RUN}_*/pede_run${RUN}_*.root  # !!!!!must be removed!!!!!
        	#hadd -T -k -f ${CALIBFILE} ${ANALBASEDIR}/run${RUN}_*/cal_run${RUN}_*.root
	
		set list=()
		set i=0
		while ( $i <= 500 )
			set num=`printf '%02d' $i`;
			echo $num
			@ i++
			if ( -e ${SERVER_OUTPUT}/run${RUN}_$num/hists_run${RUN}_$num.root ) then
				echo $num
				set list=($list $num)
			endif
		end
		set Nsub=${#list}
		echo "Nsub="$Nsub

		set jnum=$list[1]
		echo $jnum
		if ( -e ${SERVER_OUTPUT}/run${RUN}_$jnum/cal_run${RUN}_$jnum.root ) then
		    hadd -T -k -f ${ANALBASEDIR}/run${RUN}/tmphist_${RUN}_$jnum.root ${SERVER_OUTPUT}/run${RUN}_$jnum/hists_run${RUN}_$jnum.root
                    hadd -T -k -f ${ANALBASEDIR}/run${RUN}/tmpcalib_${RUN}_$jnum.root ${SERVER_OUTPUT}/run${RUN}_$jnum/cal_run${RUN}_$jnum.root
		endif
		
		if ( $Nsub == 1 ) then
			@ efn = $Nsub
			set scalerflag=1
		else
			@ efn = $Nsub - 1
			echo "endfilenumber="$efn
			set j=1
			while ( $j <= $efn )
				set jnum=$list[$j]
				echo $jnum
				@ j++
				if ( -e ${SERVER_OUTPUT}/run${RUN}_$jnum/hists_run${RUN}_$jnum.root ) then
					hadd -T -k -f ${ANALBASEDIR}/run${RUN}/tmphist_${RUN}_$jnum.root ${SERVER_OUTPUT}/run${RUN}_$jnum/hists_run${RUN}_$jnum.root
					hadd -T -k -f ${ANALBASEDIR}/run${RUN}/tmpcalib_${RUN}_$jnum.root ${ANALBASEDIR}/run${RUN}_$jnum/cal_run${RUN}_$jnum.root
				else
					continue
				endif
			end
			set scalerflag=0
		endif
		hadd -T -k -f ${HISTFILE} ${ANALBASEDIR}/run${RUN}/tmphist_${RUN}_*.root
		hadd -T -k -f ${CALIBFILE} ${ANALBASEDIR}/run${RUN}/tmpcalib_${RUN}_*.root
		rm -f ${ANALBASEDIR}/run${RUN}/tmphist_${RUN}_*.root
		rm -f ${ANALBASEDIR}/run${RUN}/tmpcalib_${RUN}_*.root
	endif

	# Counter
	cd $COUNTERDIR
	echo "COUNTER"
	if ($#argv == 5) then
		./bin/DrawCounter -run ${RUN} -runinfo $RUNINFOFILE -i $RUNDATAFILE -calib ${CALIBFILE} -print $OUTPUTDIR -subrun ${SUBRUN} -q -b
	else
		cd ${OUTPUTDIR}
		cp ${QUICKANALDIR}_${jnum}/hists_run${RUN}_${jnum}/Counter1.gif ./
	endif

	# Beam Configuration
	cd $BEAMCONFIGDIR
	echo "DRAWBEAMCONFIG"
	./bin/DrawBeamConfig -i $HISTFILE -print $OUTPUTDIR -run ${TMPRUNTAG} -q -b
	
	# Scaler
	if ( $#argv == 4) then
		#if ( $scalerflag == 1 ) then
			cd ${JOB_BASE}
			./Scripts/scaler.csh ${RUN} ${COMFILE} FORCE
		#endif
	endif

    	# Draw histogram
    	cd $PYDIR
   	echo "PYTHON"
    	mkdir -p ${TMPRUNTAG}
    	/bin/rm -f $COMFILE; echo "Drawing hists." > $COMFILE
	#python2 draw.py "${HISTFILE}" "${CALIBFILE}" "${TMPRUNTAG}" -b -q

	if ($#argv == 5) then
    		python2 draw.py "${HISTFILE}" "${CALIBFILE}" "${TMPRUNTAG}" "0" "$Nsub" -b -q
	else
		python2 draw.py "${HISTFILE}" "${CALIBFILE}" "${TMPRUNTAG}" "1" "$efn" -b -q
	endif
	mv ./${TMPRUNTAG}/*.gif ${OUTPUTDIR}
	echo "Completed "
	date
endif

			
