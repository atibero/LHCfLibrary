#!/usr/bin/csh

set RUN=$argv[1]        #  RUN
set COMFILE=$argv[2]    #  COMMENT FILE
set OPTION=$argv[3]     #  FORCE OR NONE

source Scripts/base.csh

set TMPRUN=`printf '%05d' $RUN`;
set INPUTFILE="${DIR_DATA}/run${TMPRUN}.root"
set OUTPUTDIR="${SERVER_OUTPUT}/run${TMPRUN}/scl"
set OUTPUTFILE="${OUTPUTDIR}/scaler.gif"
set DUMPFILE="${OUTPUTDIR}/scaler.txt"

echo "========== scaler.csh ============"
echo ${PWD}

mkdir -p ${OUTPUTDIR}

if(! -e ${INPUTFILE} ) then
    echo "NO INPUT FILE : ${INPUTFILE}"
    exit
endif

if(! -e ${OUTPUTFILE} || ${OPTION} == "FORCE" ) then

    cd ${JOB_BASE}/python
    python3 drawscl.py -i ${INPUTFILE} -o ${OUTPUTFILE} -d ${DUMPFILE}

endif
