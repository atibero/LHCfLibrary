#!/bin/csh

set RUN=$argv[1]        #  RUN
set SUBRUN=$argv[2]     #  SUBRUN
set FIRST=$argv[3]      #  FIRST EVENT
set LAST=$argv[4]       #  LAST EVENT
set COMFILE=$argv[5]    #  COMMENT FILE
set OPTION=$argv[6]     #  FORCE OR NONE

echo " ================  calrec.csh ===================="
echo $PWD
date
source Scripts/base.csh
#set CALIBDIR=/home/lhcf/LHCfLibrary/LHCfLibrary/build_Op2015
#set CALIBDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build_Op2015
#set CALIBDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build
set CALIBDIR=${BUILD_BASE}

set TMPRUN=`printf '%05d' $RUN`;
set TMPSUBRUN=`printf '%02d' $SUBRUN`;
#set INPUTFILE="/nfs/lhcfds4/data3/Op2015/conv_files/runraw${TMPRUN}.root" # for test
#set INPUTFILE="/nfs/lhcfds4/data5/Op2022/Data/run${TMPRUN}.root"
set INPUTFILE="${DIR_DATA}/run${TMPRUN}.root"

#set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
#set ANALBASEDIR="/nfs/lhcfds4/data5/Op2022/QuickAnalysis/"
set ANALBASEDIR=${DIR_OUTPUT}
set INITRUNFILE="${ANALBASEDIR}/.initrun.out"
set QUICKANALDIR="${ANALBASEDIR}/run${TMPRUN}_${TMPSUBRUN}"
set OUTPUTDIR="${QUICKANALDIR}/hists_run${TMPRUN}_${TMPSUBRUN}"
set HISTFILE="${QUICKANALDIR}/hists_run${TMPRUN}_${TMPSUBRUN}.root"
set CALIBFILE="${QUICKANALDIR}/cal_run${TMPRUN}_${TMPSUBRUN}.root"
set PEDEFILE="${QUICKANALDIR}/pede_run${TMPRUN}_${TMPSUBRUN}.root"
set RECFILE="${QUICKANALDIR}/rec_run${TMPRUN}_${TMPSUBRUN}.root"
set CALIBFILE_SERVER="${SERVER_OUTPUT}/run${TMPRUN}_${TMPSUBRUN}/cal_run${TMPRUN}_${TMPSUBRUN}.root"
set RECFILE_SERVER="${SERVER_OUTPUT}/run${TMPRUN}_${TMPSUBRUN}/rec_run${TMPRUN}_${TMPSUBRUN}.root"

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

# CHIECK EXISTANCE OF DATA FILE
if(! -e $INPUTFILE )then
    echo  $INPUTFILE  " do not exist."
    exit
endif

# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
if( ! -d $QUICKANALDIR ) then
    mkdir -p $QUICKANALDIR
endif

# MAKE DIRECTORY FOR HISTOGRAMS
if( ! -d $OUTPUTDIR ) then
    mkdir -p $OUTPUTDIR
endif

cd $CALIBDIR

# Calibrate ---------------------
set DO_CALIB="NO"
if( $OPTION == "FORCE" ) then
    set DO_CALIB="YES"
else if ( ! -e $CALIBFILE &&  ! -e $CALIBFILE_SERVER ) then
    set DO_CALIB="YES"
else if ( ! -e $CALIBFILE && -e $CALIBFILE_SERVER ) then
    /bin/rm -f $COMFILE; echo "Copy cal from server" > $COMFILE
    echo "Copy cal file from the server" 
    rsync -av $CALIBFILE_SERVER $CALIBFILE
endif

if( ${DO_CALIB} == "YES" ) then
    /bin/rm -f $COMFILE; echo "Calibrating" > $COMFILE
    echo "-- Start Calibrate -- "
    date
    ./bin/Calibrate -f $FIRST -l $LAST -i $INPUTFILE -o $CALIBFILE --level0cal --level1cal --ped --hist
    #./bin/Calibrate -f $FIRST -l $LAST -i $INPUTFILE -o $CALIBFILE
endif 

#    # Create pedestal tree
    #/bin/rm -f $COMFILE; echo "Creating pedestal trees." > $COMFILE
    #./bin/CreatePedestalTree -i $INPUTFILE -o $PEDEFILE --histogram

# Reconstruct --------------------
set DO_REC="NO"
if( $OPTION == "FORCE" ) then
    set DO_REC="YES"
else if ( ! -e $RECFILE &&  ! -e $RECFILE_SERVER ) then
    set DO_REC="YES"
else if ( ! -e $RECFILE && -e $RECFILE_SERVER ) then
    bin/rm -f $COMFILE; echo "Copy rec from server" > $COMFILE
    echo "Copy rec file from the server"
    rsync -av $RECFILE_SERVER $RECFILE
endif

if( ${DO_REC} == "YES"  ) then
    /bin/rm -f $COMFILE; echo "Reconstructing" > $COMFILE
    echo "-- Start Reconstruct -- "
    date
    ./bin/Reconstruct -i $CALIBFILE -o $RECFILE -p fast --disable-fastpeaksearch
endif
