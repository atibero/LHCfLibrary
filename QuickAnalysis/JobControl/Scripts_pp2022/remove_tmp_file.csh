#!/usr/bin/csh

if ($#argv == 3) then #CALLED for each SUBRUN
        set RUN=$argv[1]        #  RUN
        set SUBRUN=$argv[2]     #  SUBRUN
        set COMFILE=$argv[3]    #  COMMENT FILE
        set TMPRUN=`printf '%05d' $RUN`;
        set TMPSUBRUN=`printf '%02d' $SUBRUN`;
        set TMPRUNTAG="${TMPRUN}_${TMPSUBRUN}";
else
    	set RUN=$argv[1]
        set COMFILE=$argv[2]
        set TMPRUN=`printf '%05d' $RUN`;
        set TMPRUNTAG="${TMPRUN}";
endif




source Scripts/base.csh




set ANALBASEDIR=${DIR_OUTPUT}
set QUICKANALDIR="${ANALBASEDIR}/run${TMPRUN}*"
set DATADIR=${DIR_DATA}
set DATAFILE="${DIR_DATA}/run${TMPRUN}.root"



echo "========== remove_tmp_file.csh ============"
echo ${PWD}

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

if ( ${HOSTNAME} == "hep"   ||  ${HOSTNAME} == "lhcfdaq17" || ${HOSTNAME} == "lhcfdaq18"  ) then
   rm -r ${QUICKANALDIR}
   
   rm -r ${DATAFILE}
   echo "temporary files deleted"
else 
   echo "deleting mode avalaible only for lhcfdaq17, lhcfdaq18  and lhcfcpu1. If you want to add others modify the Script/remove_tmp_file.csh"
endif
