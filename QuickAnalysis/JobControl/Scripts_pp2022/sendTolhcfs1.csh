#!/bin/csh

if ($#argv == 3) then #CALLED for each SUBRUN
        set RUN=$argv[1]        #  RUN
        set SUBRUN=$argv[2]     #  SUBRUN
        set COMFILE=$argv[3]    #  COMMENT FILE
        set TMPRUN=`printf '%05d' $RUN`;
        set TMPSUBRUN=`printf '%02d' $SUBRUN`;
        set TMPRUNTAG="${TMPRUN}_${TMPSUBRUN}";
else
        set RUN=$argv[1]
        set COMFILE=$argv[2]
        set TMPRUN=`printf '%05d' $RUN`;
        set TMPRUNTAG="${TMPRUN}";
endif


source Scripts/base.csh

echo "Sending to Nagoya" > $COMFILE
#rsync -a --exclude={'*.root','*.dat'} ${SERVER_OUTPUT}/run$RUN* lhcf@lhcfs1.isee.nagoya-u.ac.jp:/data1/Operation2022/QuickAnalysis/
rsync -a --exclude={'*.root','*.dat'} ${SERVER_OUTPUT}/run$RUN* lhcf@lhcfs1.isee.nagoya-u.ac.jp:/data2/SPS2022/QuickAnalysis/
