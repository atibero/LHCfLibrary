#!/usr/bin/csh

set RUN=$argv[1]        #  RUN
set COMFILE=$argv[2]    #  COMMENT FILE

source Scripts/base.csh

echo "========== copy2local.csh ============"
echo ${PWD}

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

echo ${DIR_DATA}
echo ${SERVER_DATA}

mkdir -p ${DIR_DATA}

if( ${DIR_DATA} != ${SERVER_DATA} ) then
    echo "Copying to Local" > $COMFILE
    echo "start copy"
    date 
    python3 ${JOB_BASE}/python/copyData.py -r $argv[1] -s ${SERVER_DATA} -d ${DIR_DATA}
    echo "end copy"
    date
endif
