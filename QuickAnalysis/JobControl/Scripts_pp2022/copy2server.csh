#!/usr/bin/csh

if ($#argv == 3) then #CALLED for each SUBRUN
	set RUN=$argv[1]        #  RUN
	set SUBRUN=$argv[2]     #  SUBRUN
	set COMFILE=$argv[3]    #  COMMENT FILE
	set TMPRUN=`printf '%05d' $RUN`;
        set TMPSUBRUN=`printf '%02d' $SUBRUN`;
        set TMPRUNTAG="${TMPRUN}_${TMPSUBRUN}";
else
	set RUN=$argv[1]
	set COMFILE=$argv[2]
	set TMPRUN=`printf '%05d' $RUN`;
        set TMPRUNTAG="${TMPRUN}";
endif

source Scripts/base.csh

set ANALBASEDIR=${DIR_OUTPUT}
set QUICKANALDIR="${ANALBASEDIR}/run${TMPRUNTAG}"
set STABDIR="${ANALBASEDIR}/stability_analysis" 

echo "========== copy2server.csh ============"
echo ${PWD}

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

if( ${DIR_OUTPUT} != ${SERVER_OUTPUT} ) then
    echo "Copying to Server" > $COMFILE
    # copy all figure files 
    rsync -a --exclude={'*/cal_run*.root','*/rec_run*.root'} ${QUICKANALDIR} "${SERVER_OUTPUT}/"
    rsync -a --include={'*/stability*.root',} ${STABDIR} "${SERVER_OUTPUT}/"
    if ($#argv == 3) then
	if ( ${SUBRUN} == 0 ) then
    		# copy rec file 
    		rsync -a --include='*/rec_run*.root' ${QUICKANALDIR} "${SERVER_OUTPUT}/"
    		# copy cal file 
    		rsync -a --include='*/cal_run*.root' ${QUICKANALDIR} "${SERVER_OUTPUT}/"	
	endif
    endif
endif
 
