#ifndef __JOBINFO_H__
#define __JOBINFO_H__

#include <TFile.h>
#include <TSystem.h>
#include <TThread.h>
#include <TTree.h>

#include <cstring>
#include <ctime>
#include <vector>

using namespace std;

class JobInfo {
 public:
  int id;
  int run;
  int subrun;
  int firstev;
  int lastev;
  bool isgoing;
  time_t starttime;
  string going_job;
  TThread* thread;

  // bool arm1_enable;
  // bool arm2_enable;
  char arm_enabled[256];

  char scriptfile[256];
  char joblogfile[256];
  char commentfile[256];
  string comment;

 public:
  JobInfo() { Clear(); }
  void Clear();
};
class JobList {
 public:
  vector<int> vhaddlist;
  // string quickcheck;
  bool skip1st;
  int ncpu;
  int nsub;
  int rescale;
  int subrunoffset;
  int entryperfile;
  int entry1stfile;
  string nrunfolder;
  JobInfo jobs[100];

  bool arm1_enable;
  bool arm2_enable;

  char scriptfile[256];
  char auxiptfile[256];
  char joblogdirpath[256];
  char commentpath[256];

 public:
  JobList(string rd, int nc, int ns, int sf, int rs, int so, int ef, int ff, string qd, bool a1, bool a2);
  ~JobList();

  int SetScriptFile(const char file[]);
  int SetAuxiptFile(const char file[]);
  int SetJobLogDir(const char path[]);
  int SetCommentDir(const char path[]);
  int ClearCommentFiles();
  int GetNCPUs() { return ncpu; }
  int GetNGoingJobs();
  int LaunchJob(int run);
  int LaunchHadd();
  int Show();

  void KillJobs();

 public:
  static string Transrate(string com, JobInfo* job);
  static string ShowCommand(string ss);
  static string GetTime(time_t ct);
};

void thread_function(void* arg);

#endif
