#!/bin/csh

set RUN=$argv[1]        #  RUN
set COMFILE=$argv[2]    #  COMMENT FILE
set OPTION=$argv[3]     #  FORCE OR NONE

#set CALIBDIR=/home/lhcf/LHCfLibrary/LHCfLibrary/build
set CALIBDIR=/crhome/kondo.moe/lhcflibrary/develop6/LHCfLibrary/build_Op2015_4771274

set TMP=`printf '%05d' $RUN`;
#set INPUTFILE="/mnt/lhcfs4/data3/Op2015/conv_files/runraw${TMP}.root"
#set INPUTFILE="/mnt/lhcfs3/data3/Op2015/conv/run${TMP}.root"
set INPUTFILE="~/lhcflibrary/Op2022/LHCfLibrary/data/run${TMP}.root"

#set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
#set ANALBASEDIR="/mnt/lhcfs4/data/kondo/LHCf/Op2015/QuickAnalysis_test"
set ANALBASEDIR="~/lhcflibrary/Op2022/LHCfLibrary/data"
set INITRUNFILE="${ANALBASEDIR}/.initrun.out"
set QUICKANALDIR="${ANALBASEDIR}/run${TMP}"
set OUTPUTDIR="${QUICKANALDIR}/hists_run${TMP}"
set HISTFILE="${QUICKANALDIR}/hists_run${TMP}.root"
set CALIBFILE="${QUICKANALDIR}/cal_run${TMP}.root"
set PEDEFILE="${QUICKANALDIR}/pede_run${TMP}.root"
set RECFILE="${QUICKANALDIR}/rec_run${TMP}.root"

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

# CHIECK EXISTANCE OF DATA FILE
if(! -e $INPUTFILE )then
    echo  $INPUTFILE  " do not exist."
    exit
endif

# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
if( ! -d $QUICKANALDIR ) then
    mkdir -p $QUICKANALDIR
endif

# MAKE DIRECTORY FOR HISTOGRAMS
if( ! -d $OUTPUTDIR ) then
    mkdir -p $OUTPUTDIR
endif

if( ! -e $HISTFILE || $OPTION == "FORCE" )then
    # Calibrate
    cd $CALIBDIR
    /bin/rm -f $COMFILE; echo "Calibrating" > $COMFILE
    ./bin/Calibrate -i $INPUTFILE -o $CALIBFILE -v 0 --level0 --level1
   
    # Create pedestal tree
    /bin/rm -f $COMFILE; echo "Creating pedestal trees." > $COMFILE
    ./bin/CreatePedestalTree -i $INPUTFILE -o $PEDEFILE --histogram

    # Reconstruct
    /bin/rm -f $COMFILE; echo "Reconstructing" > $COMFILE
    ./bin/Reconstruct -i $CALIBFILE -o $RECFILE -p fast -v 0

endif
