#!/bin/csh

set RUN=$argv[1]        #  RUN
set SUBRUN=$argv[2]     #  SUBRUN
set FIRST=$argv[3]      #  FIRST EVENT
set LAST=$argv[4]       #  LAST EVENT
set COMFILE=$argv[5]    #  COMMENT FILE
set OPTION=$argv[6]     #  FORCE OR NONE

set CALIBDIR=/home/lhcf/LHCfLibrary/LHCfLibrary/build_Op2015
#set CALIBDIR=/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build

set TMPRUN=`printf '%05d' $RUN`;
set TMPSUBRUN=`printf '%02d' $SUBRUN`;
set INPUTFILE="/nfs/lhcfds4/data3/Op2015/conv_files/runraw${TMPRUN}.root"

#set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
#set ANALBASEDIR="/nfs/lhcfds4/data5/Op2022/QuickAnalysis/"
set ANALBASEDIR="~/tmp/"
set INITRUNFILE="${ANALBASEDIR}/.initrun.out"
set QUICKANALDIR="${ANALBASEDIR}/run${TMPRUN}_${TMPSUBRUN}"
set OUTPUTDIR="${QUICKANALDIR}/hists_run${TMPRUN}_${TMPSUBRUN}"
set HISTFILE="${QUICKANALDIR}/hists_run${TMPRUN}_${TMPSUBRUN}.root"
set CALIBFILE="${QUICKANALDIR}/cal_run${TMPRUN}_${TMPSUBRUN}.root"
set PEDEFILE="${QUICKANALDIR}/pede_run${TMPRUN}_${TMPSUBRUN}.root"
set RECFILE="${QUICKANALDIR}/rec_run${TMPRUN}_${TMPSUBRUN}.root"

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

# CHIECK EXISTANCE OF DATA FILE
if(! -e $INPUTFILE )then
    echo  $INPUTFILE  " do not exist."
    exit
endif

# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
if( ! -d $QUICKANALDIR ) then
    mkdir -p $QUICKANALDIR
endif

# MAKE DIRECTORY FOR HISTOGRAMS
if( ! -d $OUTPUTDIR ) then
    mkdir -p $OUTPUTDIR
endif

if( ! -e $HISTFILE || $OPTION == "FORCE" )then
    # Calibrate
    cd $CALIBDIR
    /bin/rm -f $COMFILE; echo "Calibrating" > $COMFILE
    ./bin/Calibrate -f $FIRST -l $LAST -i $INPUTFILE -o $CALIBFILE --level0cal --level1cal
    #./bin/Calibrate -f $FIRST -l $LAST -i $INPUTFILE -o $CALIBFILE
   
#    # Create pedestal tree
#    /bin/rm -f $COMFILE; echo "Creating pedestal trees." > $COMFILE
#    ./bin/CreatePedestalTree -i $INPUTFILE -o $PEDEFILE --histogram

#    # Reconstruct
#    /bin/rm -f $COMFILE; echo "Reconstructing" > $COMFILE
#    ./bin/Reconstruct -i $CALIBFILE -o $RECFILE -p fast --disable-fastpeaksearch

endif
