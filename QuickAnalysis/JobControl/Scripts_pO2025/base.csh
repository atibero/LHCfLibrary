
set LIBRARY_BASE="/home/lhcf/LHCf/Analysis/LHC2025/LHCfLibrary/"
set BUILD_BASE="${LIBRARY_BASE}/build"
set JOB_BASE="${LIBRARY_BASE}/QuickAnalysis/JobControl/"
# Raw data and directory for QuickAnalysis
#set SERVER_DATA="/nfs/lhcfds4/data5/LHC2025/Data/"
#set SERVER_OUTPUT="/nfs/lhcfds4/data5/LHC2025/QuickAnalysis/"
#set SERVER_DATA="/nfs/lhcfds4/data4/Op2025/Data/"
#set SERVER_OUTPUT="/nfs/lhcfds4/data4/Op2025/QuickAnalysis/"
#set SERVER_DATA="/nfs/lhcfds4/data5/Op2025/Data/"
#set SERVER_OUTPUT="/nfs/lhcfds4/data5/Op2025/QuickAnalysis/"
set SERVER_DATA="/home/lhcf/LHCf/data/LHC2025/Data/"
set SERVER_OUTPUT="/home/lhcf/LHCf/data/LHC2025/QuickAnalysis/"

# Working directory
if( $HOSTNAME == "lhcfds4" ) then
    set DIR_DATA=${SERVER_DATA}
    set DIR_OUTPUT=${SERVER_OUTPUT}
else if ( $HOSTNAME == "lhcfserverfi2024" ) then
    set DIR_DATA="/home/lhcf/LHCf/data/LHC2025/Data/"
    set DIR_OUTPUT="/home/lhcf/LHCf/data/LHC2025/QuickAnalysis/"      
else if ( $HOSTNAME == "hep" ) then
    set DIR_DATA="/home/lhcf/LHCf/data/LHC2025/Data/"
    set DIR_OUTPUT="/home/lhcf/LHCf/data/LHC2025/QuickAnalysis/"      
else 
    set DIR_DATA="/home/lhcf/LHCf/data/LHC2025/Data/"
    set DIR_OUTPUT="/home/lhcf/LHCf/data/LHC2025/QuickAnalysis/" 
endif
