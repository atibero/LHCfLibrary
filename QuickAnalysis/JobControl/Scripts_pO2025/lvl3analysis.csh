#!/bin/csh

if ($#argv == 4) then #CALLED for each SUBRUN
	set RUN=$argv[1]        #  RUN
	set SUBRUN=$argv[2]     #  SUBRUN
	set COMFILE=$argv[3]    #  COMMENT FILE
	set OPTION=$argv[4]     #  FORCE OR NONE
	set TMPRUN=`printf '%05d' $RUN`;
	set TMPSUBRUN=`printf '%02d' $SUBRUN`;
	set TMPRUNTAG="${TMPRUN}_${TMPSUBRUN}";
else #CALLED for the flobal RUN
	set RUN=$argv[1]        #  RUN
	set COMFILE=$argv[2]    #  COMMENT FILE
	set OPTION=$argv[3]     #  FORCE OR NONE
	set TMPRUN=`printf '%05d' $RUN`;
	set TMPRUNTAG="${TMPRUN}";
endif

echo "===================== lvl3analysis.csh ==================="
echo ${PWD}
date
source Scripts/base.csh

#set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
#set ANALBASEDIR="/nfs/lhcfds4/data5/Op2022/QuickAnalysis"
set ANALBASEDIR=${DIR_OUTPUT}
set QUICKANALDIR="${ANALBASEDIR}/run${TMPRUNTAG}"
set RECFILE="${QUICKANALDIR}/rec_run${TMPRUNTAG}.root"
set OUTPUTDIRARM1="${QUICKANALDIR}/a1_analysis"
set OUTPUTDIRARM2="${QUICKANALDIR}/a2_analysis"
set HISTFILEARM1="${QUICKANALDIR}/lvl3_histo_arm1_run${TMPRUNTAG}.root"
set HISTFILEARM2="${QUICKANALDIR}/lvl3_histo_arm2_run${TMPRUNTAG}.root"
set HISTFILEARM1_SERVER="${SERVER_OUTPUT}/run${TMPRUNTAG}/lvl3_histo_arm1_run${TMPRUNTAG}.root"
set HISTFILEARM2_SERVER="${SERVER_OUTPUT}/run${TMPRUNTAG}/lvl3_histo_arm2_run${TMPRUNTAG}.root"
set SYNCFILEARM2="${QUICKANALDIR}/sync_histo_arm2_run${TMPRUNTAG}.root"
#set STABDIR="${ANALBASEDIR}/stability_analysis" 
#set STABFILEPIONARM1="${STABDIR}/stability_plot_pion_arm1.root"
#set STABFILEPIONARM2="${STABDIR}/stability_plot_pion_arm2.root"
#set STABFILEETAARM1="${STABDIR}/stability_plot_eta_arm1.root"
#set STABFILEETAARM2="${STABDIR}/stability_plot_eta_arm2.root"

#set WORKDIR="/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build"
set WORKDIR=${BUILD_BASE}
#set WORKDIR="/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/build_Op2015"
#set WORKDIR="~/LHCfLibrary/LHCfLibrary/build_Op2015" # for test on daq11
set PYDIR=${WORKDIR}/../QuickAnalysis/JobControl/python
#set PYDIR="/home/lhcf/LHCf/Analysis/LHC/LHCfLibrary/QuickAnalysis/JobControl/python"
#set PYDIR="~/LHCfLibrary/LHCfLibrary/QuickAnalysis/JobControl/python" # for test on daq11

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE


# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
if( ! -d $QUICKANALDIR ) then
    mkdir -p $QUICKANALDIR
endif

# MAKE DIRECTORY FOR HISTOGRAMS
if( ! -d $OUTPUTDIRARM1 ) then
    mkdir -p $OUTPUTDIRARM1
endif

if( ! -d $OUTPUTDIRARM2 ) then
    mkdir -p $OUTPUTDIRARM2
endif

#MAKE DIRECTORY FOR STABILITY ANALYSIS
#if( ! -d $STABDIR ) then
#    mkdir -p $STABDIR
#endif


set DO_ANA="NO"

if( $OPTION == "FORCE" ) then
    set DO_ANA="YES"
else if( ! -e $HISTFILEARM1 && ! -e $HISTFILEARM1_SERVER ) then
    set DO_ANA="YES"
else if( ! -e $HISTFILEARM2 && ! -e $HISTFILEARM2_SERVER ) then
    set DO_ANA="YES"
endif


#lvl3 analysis
if( ${DO_ANA} == "YES" )then
    if ($#argv == 4) then
	cd $WORKDIR
	/bin/rm -f $COMFILE; echo "Lvl3 histograms" > $COMFILE
	#./bin/QuickAnalysis -i $RECFILE -r ${TMPRUN} -u ${TMPSUBRUN} -o $HISTFILEARM1 --arm1 -v 1
	./bin/QuickAnalysis -i $RECFILE -r ${TMPRUN} -u ${TMPSUBRUN} -o $HISTFILEARM2 --arm2 -v 1

	./bin/CheckEventSync -i ${QUICKANALDIR} -f ${TMPRUN} -l ${TMPRUN} -s ${TMPSUBRUN} -o $SYNCFILEARM2 --arm2
    else
	/bin/rm -f $COMFILE; echo "Hadd files" > $COMFILE
 	rm -f ${HISTFILEARM1}
 	rm -f ${HISTFILEARM2}
	#hadd -T ${HISTFILEARM1} ${ANALBASEDIR}/run${TMPRUN}_*/lvl3_histo_arm1_run${TMPRUN}_*.root
	#hadd -T ${HISTFILEARM2} ${ANALBASEDIR}/run${TMPRUN}_*/lvl3_histo_arm2_run${TMPRUN}_*.root
	#hadd -T ${HISTFILEARM1} ${SERVER_OUTPUT}/run${TMPRUN}_*/lvl3_histo_arm1_run${TMPRUN}_*.root
	hadd -T ${HISTFILEARM2} ${SERVER_OUTPUT}/run${TMPRUN}_*/lvl3_histo_arm2_run${TMPRUN}_*.root	

	hadd -T ${SYNCFILEARM2} ${SERVER_OUTPUT}/run${TMPRUN}_*/sync_histo_arm2_run${TMPRUN}_*.root	
	
	#cd $WORKDIR

	#./bin/MggStability -i ${HISTFILEARM1} -r ${TMPRUN} -p pion -o ${STABFILEPIONARM1} 
	#./bin/MggStability -i ${HISTFILEARM2} -r ${TMPRUN} -p pion -o ${STABFILEPIONARM2}
	
	#./bin/MggStability -i ${HISTFILEARM1} -r ${TMPRUN} -p eta -o ${STABFILEETAARM1} 
	#./bin/MggStability -i ${HISTFILEARM2} -r ${TMPRUN} -p eta -o ${STABFILEETAARM2}
	
    endif
	
	

    # Draw histogram
    cd $PYDIR
    mkdir $TMPRUNTAG
    /bin/rm -f $COMFILE; echo "Drawing hists." > $COMFILE
#    python2 draw_lvl3_arm1.py "${HISTFILEARM1}" "${TMPRUNTAG}" -b -q
##	if ($#argv != 4) then
##		python2 print_mgg_stability_arm1_pion.py "${STABFILEPIONARM1}" "${TMPRUNTAG}" -b -q
##    	python2 print_mgg_stability_arm1_eta.py "${STABFILEETAARM1}" "${TMPRUNTAG}" -b -q
##    endif
#    mv ./${TMPRUNTAG}/Arm1-*.gif ${OUTPUTDIRARM1}
	
    #python2 draw_lvl3_arm2.py "${HISTFILEARM2}" "${TMPRUNTAG}" -b -q
    python draw_lvl3_arm2.py "${HISTFILEARM2}" "${TMPRUNTAG}" -b -q
#	if ($#argv != 4) then
#	   python2 print_mgg_stability_arm2_pion.py "${STABFILEPIONARM2}" "${TMPRUNTAG}" -b -q
#    	   python2 print_mgg_stability_arm2_eta.py "${STABFILEETAARM2}" "${TMPRUNTAG}" -b -q
#	endif
    mv -f ./${TMPRUNTAG}/Arm2-*.gif ${OUTPUTDIRARM2}

    #python2 draw_sync_hist.py "${SYNCFILEARM2}" "${TMPRUNTAG}" -b -q
    python draw_sync_hist.py "${SYNCFILEARM2}" "${TMPRUNTAG}" -b -q
    mv -f ./${TMPRUNTAG}/silicon*.gif ${OUTPUTDIRARM2}

    rm -rf $TMPRUNTAG
endif


