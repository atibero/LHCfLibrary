#!/bin/csh

set RUN=$argv[1]        #  RUN
set COMFILE=$argv[2]    #  COMMENT FILE
set OPTION=$argv[3]     #  FORCE OR NONE

set WORKDIR=/crhome/kondo.moe/lhcflibrary/Op2022/LHCfLibrary/LHCfLibrary/QuickAnalysis/MakeHistograms/build
set BINDIR=${WORKDIR}/bin

set CALIBDIR=/crhome/kondo.moe/lhcflibrary/develop6/LHCfLibrary/build_Op2015_4771274

set PYDIR=/crhome/kondo.moe/lhcflibrary/Op2022/LHCfLibrary/LHCfLibrary/QuickAnalysis/JobControl/python

set RUNINFODIR=/crhome/kondo.moe/lhcflibrary/Op2022/LHCfLibrary/LHCfLibrary/QuickAnalysis/RunInformation/build

set BEAMCONFIGDIR=/crhome/kondo.moe/lhcflibrary/Op2022/LHCfLibrary/LHCfLibrary/QuickAnalysis/MakeHistograms/BeamConfig/build

set TMP=`printf '%05d' $RUN`;
#set INPUTFILE="/nfs/lhcfds4/data3/Op2015/conv_files/runraw${TMP}.root"
#set INPUTFILE="/mnt/lhcfs3/data3/Op2015/conv/run${TMP}.root"
set INPUTFILE="~/lhcflibrary/Op2022/LHCfLibrary/data/run${TMP}.root"
#set ANALBASEDIR="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test"
#set ANALBASEDIR="/mnt/lhcfs4/data/kondo/LHCf/Op2015/QuickAnalysis_test"
set ANALBASEDIR="~/lhcflibrary/Op2022/LHCfLibrary/data"
set INITRUNFILE="${ANALBASEDIR}/.initrun.out"
set QUICKANALDIR="${ANALBASEDIR}/run${TMP}"
set OUTPUTDIR="${QUICKANALDIR}/hists_run${TMP}"
set HISTFILE="${QUICKANALDIR}/hists_run${TMP}.root"
set CALIBFILE="${QUICKANALDIR}/cal_run${TMP}.root"
set PEDEFILE="${QUICKANALDIR}/pede_run${TMP}.root"
set RECFILE="${QUICKANALDIR}/rec_run${TMP}.root"
set RUNINFOFILE="${QUICKANALDIR}/runinfo_run${TMP}.dat"
set RUNDATAFILE="${QUICKANALDIR}/rundata_run${TMP}.root"

#set A1FELOG="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test/logs/LogFrontend1/frontend_arm1_log1"
#set A2FELOG="/nfs/lhcfds4/data3/Op2015/QuickAnalysis_test/logs/LogFrontend2/frontend_arm2_log2"
set A1FELOG="/mnt/lhcfs5/data/Op2022/DAQ_Backup/DAQ_Backup/lhcfdaq15/Data/LogFrontend1/frontend_arm1_log"
set A2FELOG="/mnt/lhcfs5/data/Op2022/DAQ_Backup/DAQ_Backup/lhcfdaq15/Data/LogFrontend2/frontend_arm2_log"

# Clear Comment
/bin/rm -f $COMFILE; echo "" > $COMFILE

# CHIECK EXISTANCE OF DATA FILE
if(! -e $RECFILE )then
    echo  $RECFILE  " do not exist."
    exit
endif

# MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
#if( ! -d $QUICKANALDIR ) then
#    mkdir -p $QUICKANALDIR
#endif

# MAKE DIRECTORY FOR HISTOGRAMS
#if( ! -d $OUTPUTDIR ) then
#    mkdir -p $OUTPUTDIR
#endif


if( ! -e $HISTFILE || $OPTION == "FORCE" )then
  
    # Calibrate
    #cd $CALIBDIR
    #/bin/rm -f $COMFILE; echo "Calibrating" > $COMFILE
    #./bin/Calibrate -i $INPUTFILE -o $CALIBFILE --level0 --level1
   
    # Create pedestal tree
    #/bin/rm -f $COMFILE; echo "Creating pedestal trees." > $COMFILE
    #./bin/CreatePedestalTree -i $INPUTFILE -o $PEDEFILE --histogram

    # Reconstruct
    #/bin/rm -f $COMFILE; echo "Reconstructing" > $COMFILE
    #./bin/Reconstruct -i $CALIBFILE -o $RECFILE -p fast --level0 --level1 --level2

    # fillrundata 
    cd $RUNINFODIR
    /bin/rm -f $COMFILE; echo "Fill run information" > $COMFILE
    ./bin/FillRunData -i $CALIBFILE -rd $RUNDATAFILE -ri $RUNINFOFILE

    # filldaqinfo
    ./bin/FillDaqInfo -run $RUN -runinfo $RUNINFOFILE -rundata $RUNDATAFILE -a1log $A1FELOG -a2log $A2FELOG
    
    # drawruninfo
    ./bin/DrawRunInfo -run $RUN -runinfo $RUNINFOFILE -print $OUTPUTDIR -q -b

    # Make histogram
    cd $WORKDIR
    /bin/rm -f $COMFILE; echo "Making hists." > $COMFILE
    ./bin/MakeHist -i $CALIBFILE -o $HISTFILE

    # Beam Configuration
    cd $BEAMCONFIGDIR
    ./bin/DrawBeamConfig -i $HISTFILE -print $OUTPUTDIR -run $RUN -q -b

    # Draw histogram
    cd $PYDIR
    mkdir $RUN
    /bin/rm -f $COMFILE; echo "Drawing hists." > $COMFILE
    python draw.py "${HISTFILE}" "${PEDEFILE}" "${RUN}" -b -q
    mv ./${RUN}/Hist20mmCal.gif ${OUTPUTDIR}
    mv ./${RUN}/Hist40mmCal.gif ${OUTPUTDIR}
    mv ./${RUN}/HistArm1Flag.gif ${OUTPUTDIR}
    mv ./${RUN}/GSObarAccumlate.gif ${OUTPUTDIR}
    mv ./${RUN}/CalPedestalAv.gif ${OUTPUTDIR}
    mv ./${RUN}/CalPedestalRMS.gif ${OUTPUTDIR}
    mv ./${RUN}/GSObarPedestalAv.gif ${OUTPUTDIR}
    mv ./${RUN}/GSObarPedestalRMS.gif ${OUTPUTDIR}
    mv ./${RUN}/Hist25mmCal.gif ${OUTPUTDIR}
    mv ./${RUN}/Hist32mmCal.gif ${OUTPUTDIR}
    mv ./${RUN}/SiliconAccumlate.gif ${OUTPUTDIR}
    mv ./${RUN}/SiliconPedestalAv.gif ${OUTPUTDIR}
    mv ./${RUN}/SiliconPedestalRMS.gif ${OUTPUTDIR}
    mv ./${RUN}/HistArm2Flag.gif ${OUTPUTDIR}
    mv ./${RUN}/HistArm1TDC.gif ${OUTPUTDIR}
    mv ./${RUN}/HistArm2TDC.gif ${OUTPUTDIR}
    mv ./${RUN}/DSC_Arm1-Small.gif ${OUTPUTDIR}
    mv ./${RUN}/DSC_Arm1-Large.gif ${OUTPUTDIR}
    mv ./${RUN}/DSC_Arm2-Small.gif ${OUTPUTDIR}
    mv ./${RUN}/DSC_Arm2-Large.gif ${OUTPUTDIR}
    mv ./${RUN}/DSC_FCs.gif ${OUTPUTDIR}
    rm -rf ./$RUN
    
endif


