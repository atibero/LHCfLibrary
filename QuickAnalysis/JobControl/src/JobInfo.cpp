#ifndef __JOBINFO_CPP__
#define __JOBINFO_CPP__

#include "JobInfo.h"

#include <signal.h>

#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

string quickcheck = "";

void JobInfo::Clear() {
  id = 0;
  run = 0;
  subrun = 0;
  firstev = 0;
  lastev = 0;
  isgoing = false;
  starttime = 0;
  going_job = "";
  thread = NULL;

  // arm1_enable= false;
  // arm2_enable=false;

  strcpy(arm_enabled, "");

  strcpy(scriptfile, "");
  strcpy(joblogfile, "");
  strcpy(commentfile, "");
  comment = "";
}

// ---------------------------------------------------

JobList::JobList(string rd, int nc, int ns, int sf, int rs, int so, int ef, int ff, string qd, bool a1, bool a2) {
  nrunfolder = rd;
  ncpu = nc;
  nsub = ns;
  skip1st = sf;
  rescale = rs;
  subrunoffset = so;
  entryperfile = ef;
  entry1stfile = ff;
  quickcheck = qd;
  vhaddlist.clear();
  arm1_enable = a1;
  arm2_enable = a2;
  strcpy(scriptfile, "./");
  strcpy(auxiptfile, "./");
  strcpy(joblogdirpath, "./");
  strcpy(commentpath, "./");

  // Set id
  for (int i = 0; i < ncpu; i++) {
    jobs[i].id = i;
  }
}

JobList::~JobList() { ; }

int JobList::SetScriptFile(const char file[]) {
  strcpy(scriptfile, file);
  return 0;
}

int JobList::SetAuxiptFile(const char file[]) {
  strcpy(auxiptfile, file);
  return 0;
}

int JobList::SetJobLogDir(const char path[]) {
  strcpy(joblogdirpath, path);
  return 0;
}

int JobList::SetCommentDir(const char path[]) {
  strcpy(commentpath, path);
  ClearCommentFiles();
  return 0;
}

int JobList::ClearCommentFiles() {
  // Set comment files
  char filename[1024];
  for (int i = 0; i < ncpu; i++) {
    sprintf(filename, "%s/.comment_job%d.txt", commentpath, i);
    sprintf(jobs[i].commentfile, filename);
  }
  // Clear old comments
  ofstream fout;
  for (int i = 0; i < ncpu; i++) {
    fout.open(jobs[i].commentfile);
    fout << "" << endl;
    fout.close();
    fout.clear();
  }
  return 0;
}

int JobList::GetNGoingJobs() {
  int njob = 0;
  for (int i = 0; i < ncpu; i++) {
    if (jobs[i].isgoing == true) {
      njob++;
    }
  }
  return njob;
}

int JobList::LaunchJob(int run) {
  char filename[1024];
  // sprintf(filename, "%s/runraw%05d.root",nrunfolder.c_str(),run);
  sprintf(filename, "%s/run%05d.root", nrunfolder.c_str(), run);
  TFile myfile(filename, "READ");
  int nentries = 0;
  // Skip empty files
  if (arm1_enable && !arm2_enable) {
    if (!myfile.GetListOfKeys()->Contains("Trigger1")) {
      return 0;
    }
    TTree *arm1 = (TTree *)myfile.Get("Trigger1");
    nentries = arm1->GetEntries();
    myfile.Close();
  } else if (arm2_enable && !arm1_enable) {
    if (!myfile.GetListOfKeys()->Contains("Trigger2")) {
      return 0;
    }
    TTree *arm2 = (TTree *)myfile.Get("Trigger2");
    nentries = arm2->GetEntries();
    myfile.Close();

  } else {
    if (!myfile.GetListOfKeys()->Contains("Trigger1") || !myfile.GetListOfKeys()->Contains("Trigger2")) {
      return 0;
    }
    TTree *arm1 = (TTree *)myfile.Get("Trigger1");
    TTree *arm2 = (TTree *)myfile.Get("Trigger2");
    nentries = TMath::Max(arm1->GetEntries(), arm2->GetEntries());
    myfile.Close();
  }

  // Skip empty files
  if (nentries == 0) return 0;

  int srun = 1;  // First file
  if (nentries >= entry1stfile) srun += ((nentries - entry1stfile) / entryperfile);
  if (((nentries - entry1stfile) % entryperfile) != 0) srun += 1;  // Last file
  if (srun > nsub) srun = nsub;

  // cout << "DATA: " << entry1stfile << " " << entryperfile << " " << nentries << " -> SUBRUN: " << srun << endl;

  int isub = 0;
  while (isub < srun) {
    // Search for an available CPU
    int icpu = -1;
    for (int i = 0; i < ncpu; i++) {
      if (jobs[i].isgoing == false) {
        icpu = i;
        break;
      }
    }

    // If no CPU is available update monitor and wait
    if (icpu < 0) {
      cout << "\x1b[0;0H" << flush;
      cout << endl
           << "          -------  JOB CONTROLLER  -------     \n"
           << "                   QUIT : Ctrl-C               \n"
           << "             " << endl
           << endl;
      cout << "               NEXT RUN NUMBER : " << run << endl << endl;
      Show();
      cout << endl;
      sleep(5);
      continue;
    }

    // Search for a run to hadd
    bool ishaddjob = vhaddlist.size() > 0 ? true : false;
    if (ishaddjob) {
      for (int i = 0; i < ncpu; i++) {
        if (jobs[i].isgoing == true) {
          if (jobs[i].run == vhaddlist[0]) {
            ishaddjob = false;
            break;
          }
        }
      }
    }
    // if(skip1st) ishaddjob=false; // comment out for lhcfdaq17 and cpu1

    // if(ishaddjob)
    //	  cout << "HADD " << vhaddlist[0] << " @ " << icpu << endl;
    // else
    //	  cout << "PROCESS " << run << "(" << isub << ") " << " @ " << icpu << endl;

    // Submit job
    if (ishaddjob) {  // Hadd previous run
      LaunchHadd();
    } else {  // Process this subrun
      // cout << "STATUS: " << skip1st << " " << isub << " " << subrunoffset << " " << rescale << " " <<
      // ((isub-subrunoffset)%rescale) << endl;

      if (((!skip1st) && (isub == 0)) || ((isub >= subrunoffset) && (((isub - subrunoffset) % rescale) == 0))) {
        if (find(vhaddlist.begin(), vhaddlist.end(), run) == vhaddlist.end()) vhaddlist.push_back(run);

        // cout << "SUBRUN: " << isub << endl;

        jobs[icpu].run = run;
        jobs[icpu].subrun = isub;
        jobs[icpu].firstev = isub == 0 ? 0. : entry1stfile + ((isub - 1) * entryperfile);
        jobs[icpu].lastev = isub == 0 ? entry1stfile - 1 : entry1stfile + (isub * entryperfile) - 1;
        strcpy(jobs[icpu].scriptfile, scriptfile);

        if (arm1_enable) {
          strcpy(jobs[icpu].arm_enabled, "arm1");
        } else if (arm2_enable) {
          strcpy(jobs[icpu].arm_enabled, "arm2");
        }

        time_t ctime;
        time(&ctime);
        jobs[icpu].starttime = ctime;
        sprintf(jobs[icpu].joblogfile, "%s/joblog_run%05d_%02d_%d.out", joblogdirpath, jobs[icpu].run,
                jobs[icpu].subrun, (int)ctime);

        jobs[icpu].thread = new TThread(thread_function, (void *)(&jobs[icpu]));
        jobs[icpu].thread->Run();

        sleep(5);
      }

      isub += (isub == 0) ? subrunoffset : rescale;
    }
    // pthread_t tid;
    // pthread_create(&tid,NULL,thread_function,&jobs[icpu]);
    // pthread_detach(tid);
  }

  return 0;
}

int JobList::LaunchHadd() {
  // for(unsigned int i=0; i<vhaddlist.size(); ++i)
  //	  cout << i << " " << vhaddlist[i] << endl;

  if (vhaddlist.size() == 0) return 0;

  bool ishaddjob = true;
  int icpu = -1;

  while (1) {
    // Search for a run to hadd
    ishaddjob = true;
    for (int i = 0; i < ncpu; i++) {
      if (jobs[i].isgoing == true) {
        if (jobs[i].run == vhaddlist[0]) {
          ishaddjob = false;
          break;
        }
      }
    }

    // Search for an available CPU
    icpu = -1;
    for (int i = 0; i < ncpu; i++) {
      if (jobs[i].isgoing == false) {
        icpu = i;
        break;
      }
    }

    // If no CPU is available or we need to wait a job to finish
    if (!ishaddjob || icpu < 0) {
      cout << "\x1b[0;0H" << flush;
      cout << endl
           << "          -------  JOB CONTROLLER  -------     \n"
           << "                   QUIT : Ctrl-C               \n"
           << "             " << endl
           << endl;
      cout << "               NEXT RUN NUMBER : ENDED" << endl << endl;
      Show();
      cout << endl;
      sleep(5);
      continue;
    }

    break;
  }

  // Submit job
  jobs[icpu].run = vhaddlist[0];
  jobs[icpu].subrun = 0;
  jobs[icpu].firstev = 0;
  jobs[icpu].lastev = 0;
  strcpy(jobs[icpu].scriptfile, auxiptfile);

  time_t ctime;
  time(&ctime);
  jobs[icpu].starttime = ctime;
  sprintf(jobs[icpu].joblogfile, "%s/joblog_aux%05d_%d.out", joblogdirpath, jobs[icpu].run, (int)ctime);

  jobs[icpu].thread = new TThread(thread_function, (void *)(&jobs[icpu]));
  jobs[icpu].thread->Run();

  vhaddlist.erase(vhaddlist.begin());

  sleep(5);

  return 0;
}

extern void sig_quit(int pippo);
extern void sig_quitforce(int pippo);

void thread_function(void *arg) {
  JobInfo *job = (JobInfo *)arg;

  signal(SIGINT, sig_quitforce);  // Ctrl+C

  ifstream fin(job->scriptfile);
  if (!fin) {
    cerr << "Cannot open script file" << endl;
  }

  job->isgoing = true;
  job->comment = "START";

  // int iloop=0;
  string ss;
  while (getline(fin, ss)) {
    if (ss[0] == '#') continue;
    ss = JobList::Transrate(ss, job);
    job->going_job = ss;
    ss.append(" >> ");
    ss.append(job->joblogfile);
    ss.append(" 2>&1 ");
    // system(ss.c_str());
    gSystem->Exec(ss.c_str());
    sleep(1);
    // cout << job->run <<"  "<< iloop++ << endl;
  }

  job->isgoing = false;
  job->comment = "END";

  return;
}

string JobList::Transrate(string com, JobInfo *job) {
  int nsize = com.size();
  string s = com;
  // remove space
  for (int i = 0; i < nsize; i++) {
    if (s[0] == ' ') {
      s = s.substr(1);
    } else {
      break;
    }
  }
  nsize = s.size();
  if (nsize == 0) {
    return s;
  }
  unsigned long pos;

  // Replace $RUN -> run number
  char ch_run[20];
  sprintf(ch_run, "%d", job->run);
  while (1) {
    pos = s.find("$RUN");
    if (pos == string::npos) break;
    s.erase(pos, 4);
    s.insert(pos, ch_run);
  }

  // Replace $SUBRUN -> subrun number
  char ch_sub[20];
  sprintf(ch_sub, "%d", job->subrun);
  while (1) {
    pos = s.find("$SUBRUN");
    if (pos == string::npos) break;
    s.erase(pos, 7);
    s.insert(pos, ch_sub);
  }

  // Replace $FIRST -> first event number
  char ch_first[20];
  sprintf(ch_first, "%d", job->firstev);
  while (1) {
    pos = s.find("$FIRST");
    if (pos == string::npos) break;
    s.erase(pos, 6);
    s.insert(pos, ch_first);
  }

  // Replace $LAST -> first event number
  char ch_last[20];
  sprintf(ch_last, "%d", job->lastev);
  while (1) {
    pos = s.find("$LAST");
    if (pos == string::npos) break;
    s.erase(pos, 5);
    s.insert(pos, ch_last);
  }

  // Replace $ARM -> arm number
  while (1) {
    pos = s.find("$ARM");
    if (pos == string::npos) break;
    s.erase(pos, 4);
    s.insert(pos, job->arm_enabled);
  }

  // Replace $COMFILE -> commentfile
  while (1) {
    pos = s.find("$COMFILE");
    if (pos == string::npos) break;
    s.erase(pos, 8);
    s.insert(pos, job->commentfile);
  }

  // Replace $CPUID -> i
  char ch_id[5];
  sprintf(ch_id, "%d", job->id);
  while (1) {
    pos = s.find("$CPUID");
    if (pos == string::npos) break;
    s.erase(pos, 6);
    s.insert(pos, ch_id);
  }

  return s;
}

int JobList::Show() {
  // Check commentfiles
  ifstream fin;
  for (int icpu = 0; icpu < ncpu; icpu++) {
    fin.open(jobs[icpu].commentfile);
    if (fin.good()) {
      getline(fin, jobs[icpu].comment);
    }
    fin.close();
    fin.clear();
  }

  // Print
  cout << "  JOBS GOING TIME   RUN   SUBRUN    COMMENT" << endl;
  for (int icpu = 0; icpu < ncpu; icpu++) {
    if (jobs[icpu].isgoing) cout << "\x1b[32m" << flush;  // Green
    cout << "  " << setw(3) << icpu << "   " << (jobs[icpu].isgoing ? "YES" : "END") << "  "
         << GetTime(jobs[icpu].starttime) << " " << setw(5) << right << jobs[icpu].run << "   " << setw(3) << right
         << jobs[icpu].subrun
         << "   "
         //<< setw(17) << left << ShowCommand( jobs[icpu].going_job) <<" "
         << setw(20) << left << jobs[icpu].comment << right << endl;
    cout << "\x1b[39m" << flush;  // to Default
  }
  return 0;
}

string JobList::ShowCommand(string ss) {
  unsigned long pos;
  pos = ss.find(" ");
  if (pos != string::npos) {
    ss = ss.substr(0, pos);
  }

  pos = ss.rfind("/");
  if (pos != string::npos) {
    ss = ss.substr(pos + 1, ss.size() - (pos + 1));
  }
  return ss;
}

string JobList::GetTime(time_t ct) {
  struct tm *lt = localtime(&ct);
  char tmp[256];
  sprintf(tmp, "%02d:%02d", lt->tm_hour, lt->tm_min);
  string ss = tmp;
  if (ct < 10) ss = "    ";
  return ss;
}

void JobList::KillJobs() {
  for (int icpu = 0; icpu < ncpu; icpu++) {
    if (jobs[icpu].isgoing) {
      jobs[icpu].thread->Kill();
    }
  }
}

#endif
