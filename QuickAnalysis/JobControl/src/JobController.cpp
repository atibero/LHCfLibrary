#include <TApplication.h>
#include <TROOT.h>
#include <TThread.h>
#include <signal.h>
#include <sys/stat.h>

#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

#include "JobInfo.h"

//// ++++++ DEFINITIONS for 2022 +++++
//#define NCPU 10
//#define NSUB 500
//#define RESCALE 2
//#define ENTRYXFILE 25000
//#define ENTRYFIRST 10000
//#define SCRIPTFILE "./Scripts/main.job"
//#define ELIFTPIRCS "./Scripts/auxi.job"
//#define JOBLOGDIR "./joblog"
////#define RAWDIR      "/nfs/lhcfds4/data3/Op2015/conv_files/"
//#define RAWDIR "/nfs/lhcfds4/data5/SPS2022/Data"
//#define QCKDIR "/nfs/lhcfds4/data5/SPS2022/QuickAnalysis/"

// ++++++ DEFINITIONS for 2025 +++++
#define NCPU 96
#define NSUB 100
#define RESCALE 1
#define ENTRYXFILE 25000
#define ENTRYFIRST 25000
#define SCRIPTFILE "./Scripts/main.job"
#define ELIFTPIRCS "./Scripts/auxi.job"
#define JOBLOGDIR "./joblog"
#define COMMENTDIR "./"
//#define RAWDIR "/nfs/lhcfds4/data3/Op2015/conv_files/"
//#define RAWDIR "/nfs/lhcfds4/data4/Op2025/Data"
//#define QCKDIR "/nfs/lhcfds4/data4/Op2025/QuickAnalysis/"
#define RAWDIR "/home/lhcf/LHCf/data/LHC2025/Data"
#define QCKDIR "/home/lhcf/LHCf/data/LHC2025/QuickAnalysis/"

// ++++++ FUNCTIONS +++++++
int search_rootfile(int run, char dir[]);
char* printtime();
void sig_quit(int pippo);
void sig_quitforce(int pippo);

JobList* joblist;
bool rq_quit = false;

// ++++++++++++++++++++++++++++++++++++
// +++++++        MAIN         ++++++++
// ++++++++++++++++++++++++++++++++++++
int main(int argc, char** argv) {
  int ncpu = NCPU;
  int nsub = NSUB;
  int rescale = RESCALE;
  int subrunoffset = 1;
  int entry1stfile = ENTRYFIRST;
  int entryperfile = ENTRYXFILE;
  int startrun = 0;
  int endrun = 99999;
  bool arm1 = false;
  bool arm2 = false;
  char scriptfile[256] = SCRIPTFILE;
  char auxiptfile[256] = ELIFTPIRCS;
  bool skip1st = false;
  char rawdir[256] = RAWDIR;
  char qckdir[256] = QCKDIR;
  for (int i = 0; i < argc; i++) {
    if (strcmp(argv[i], "-n") == 0 || strcmp(argv[i], "--ncpu") == 0) {
      ncpu = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-u") == 0 || strcmp(argv[i], "--nsub") == 0) {
      nsub = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-o") == 0 || strcmp(argv[i], "--off-1st") == 0) {
      skip1st = true;
    }
    if (strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--rescale") == 0) {
      rescale = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--subrunoffset") == 0) {
      subrunoffset = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--entry1stfile") == 0) {
      entry1stfile = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--entryperfile") == 0) {
      entryperfile = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "--srun") == 0) {
      startrun = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-e") == 0 || strcmp(argv[i], "--erun") == 0) {
      endrun = atoi(argv[++i]);
    }
    if (strcmp(argv[i], "-j") == 0 || strcmp(argv[i], "--job") == 0) {
      strcpy(scriptfile, argv[++i]);
    }
    if (strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "--aux") == 0) {
      strcpy(auxiptfile, argv[++i]);
    }
    if (strcmp(argv[i], "-arm1") == 0) {
      arm1 = true;
    }
    if (strcmp(argv[i], "-arm2") == 0) {
      arm2 = true;
    }
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "--help") == 0) {
      cout << " JobController                          \n"
           << "                                        \n"
           << " Options)\n"
           << "   -n/--ncpu [number]          :  Maximum number of jobs\n"
           << "   -u/--nsub [number]          :  Maximum number of subruns\n"
           << "   -o/--off-1st [number]       :  Do not process first subrun file\n"
           << "   -r/--rescale [number]       :  Process only a subrun each X\n"
           << "   -p/--subrunoffset [number]  :  Position of the first subrun [default 1]\n"
           << "   -f/--entry1stfile [number]  :  Entries of first subrun file\n"
           << "   -t/--entryperfile [number]  :  Entries for each subrun file\n"
           << "   -s/--srun [number]          :  Start run number\n"
           << "   -e/--erun [number]          :  End run number\n"
           << "   -j/--job  [file]            :  Main script file [Scripts/main.job]\n"
           << "   -a/--aux  [file]            :  Auxiliary script file [Scripts/auxi.job]"
           << "   -arm1                       :  ENABLE ARM1"
           << "   -arm2                       :  ENABLE ARM2" << endl;
      exit(0);
    }
  }

  // ++++ INITILIZE ++++

  joblist =
      new JobList(rawdir, ncpu, nsub, skip1st, rescale, subrunoffset, entryperfile, entry1stfile, qckdir, arm1, arm2);
  joblist->SetScriptFile(scriptfile);
  joblist->SetAuxiptFile(auxiptfile);
  joblist->SetJobLogDir(JOBLOGDIR);
  joblist->SetCommentDir(COMMENTDIR);

  // ++++ SET SIGNALS ++++
  signal(SIGINT, sig_quitforce);  // Ctrl+C

  cout << " WAIT FOR 10 SECONDS : STARTING >>>>>>>   " << endl;

  int run = startrun;
  int iloop = 0;
  while (1) {
    // ----- SKIP SOME RUNS ---------
    // No midas files for mistakes
    if (run >= 41764 && run <= 41768) {
      run++;
      continue;
    }
    if (run >= 41895 && run <= 41984) {
      run++;
      continue;
    }

    if (iloop == 0) cout << "\x1b[2J" << flush;  // terminal clear

    // ----- LAUNCH JOB -------------
    if (!rq_quit && joblist->GetNGoingJobs() < joblist->GetNCPUs() && (run >= startrun && run <= endrun)) {
      if (search_rootfile(run, rawdir) == 1) {
        joblist->LaunchJob(run++);
      } else {
        while (joblist->LaunchHadd()){
	       	continue;
	}
      }
      /*else{
        joblist->LaunchJob(run++);
      }*/ //alessio and eugenio commented it
    }
    if (run > endrun) {
      rq_quit = true;
      // cout << "Job Process ended, looking for hadd" << endl;
      while (joblist->LaunchHadd()) continue;
    }

    // ----- WAIT FOR 5 SECONDS -----
    sleep(5);

    // ----- PRINT STATUS -----------
    cout << "\x1b[2J" << flush;  // terminal clear
    cout << "\x1b[0;0H" << flush;
    cout << endl
         << "          -------  JOB CONTROLLER  -------     \n"
         << "                   QUIT : Ctrl-C               \n"
         << "             " << printtime() << endl;
    if (!rq_quit) {
      cout << "               NEXT RUN NUMBER : " << run << endl << endl;
    } else {
      cout << "               WAITING FINISH OF JOBS  " << endl << endl;
    }
    joblist->Show();
    cout << endl;

    // ----- WAIT FOR 5 SECONDS -----
    sleep(5);

    // cout << "LOOP " << iloop << " " << rq_quit << " " << joblist->GetNGoingJobs() << endl;

    if (rq_quit && joblist->GetNGoingJobs() == 0) break;
    iloop++;
  }

  return 0;
}

// ++++++ FUNCTIONS +++++++++

int search_rootfile(int run, char dir[]) {
  struct stat fstatus;
  char filename[256];

  // sprintf(filename, "%s/runraw%05d.root", dir,run);
  sprintf(filename, "%s/run%05d.root", dir, run);

  cout << filename << endl;
  if (stat(filename, &fstatus) == -1) {
	  cout << "File doesn't exist" << endl;
    return -1;
  }

  time_t ctime;
  time(&ctime);
  cout <<ctime<<endl;
  cout <<fstatus.st_ctime<<endl;


  if (ctime - fstatus.st_ctime > 20) {
	  cout<<"file copied"<<endl;
    return 1;
  } else {
	  cout<<"still copying"<<endl;
    return 0;
  }
}

char* printtime() {
  time_t ctime;
  time(&ctime);
  return asctime(localtime(&ctime));
}

void sig_quitforce(int pippo) {
  cout << "Exiting....." << endl;
  joblist->KillJobs();
  exit(-1);
}

void sig_quit(int pippo) {
  cerr << "Requested Quit" << endl;
  rq_quit = true;
}
