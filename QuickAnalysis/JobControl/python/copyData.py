#!/usr/bin/python3

# Copy the raw data file from the server
# The target and destination directories must be specified
# using enviromental variables.
#
# if the copy is blocked (continuously see waiting),
# remove  

import datetime
import os
import time
import argparse 
import shutil 

# Parameters 
TIME_STEP = 3
TIME_WAIT = 3
DIR_SRC = ""
DIR_DST = ""
FILE_COPYING = "./joblog/.copying"


def check_file_dst(run):
    f = DIR_DST + "run" + str(run) + ".root"
    return os.path.isfile(f)
    
def check_completed(run):
    ifile = DIR_SRC + "run" + str(run) + ".root"
    ofile = DIR_DST + "run" + str(run) + ".root"
    if not os.path.isfile(ifile) or not os.path.isfile(ofile):
        return -1

    t_i = os.path.getmtime(ifile)
    t_o = os.path.getmtime(ofile)
    if(t_i - t_o < 1):
        return 1
    else:
        return 0
    
def copy_file(run):
    # create copying file
    f = open(FILE_COPYING+str(run), 'w')
    f.write(str(run))
    f.close()

    # copy the file
    ifile = DIR_SRC + "run" + str(run) + ".root"
    shutil.copy2(ifile, DIR_DST)

    # remove the copying file
    os.remove(FILE_COPYING+str(run))

def is_copying(run):
    if not os.path.isfile(FILE_COPYING+str(run)):
        return -1
    else :
        with open(FILE_COPYING+str(run)) as f:
            s = f.readline()
        return int(s)
    

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser();
    parser.add_argument("-r","--run", help="run number", type=int)
    parser.add_argument("-s","--source",  help="Source dir.", default=DIR_SRC)
    parser.add_argument("-d","--dest",  help="Destination dir.", default=DIR_DST)
    args = parser.parse_args()

    run = args.run
    DIR_SRC = args.source
    DIR_DST = args.dest

    while True: 
        # case: already the file is present
        if check_file_dst(run):
            if is_copying (run) != run :
                break

        # case: file is not yet 
        if is_copying(run) < 0 :
            print('copying {}'.format(run))
            copy_file(run)
            break
        
        print('waiting....')
        time.sleep(TIME_WAIT)
        
