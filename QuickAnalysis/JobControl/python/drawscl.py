import argparse
import ROOT
from scldata import SclData
from scldata import DrawScl
ROOT.gROOT.SetBatch(True)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser();
    parser.add_argument("-i","--input", help="input file")
    parser.add_argument("-o","--output", help="output file", default='')
    parser.add_argument('-d','--dump', help='text dump file', default='')
    args = parser.parse_args()

    inputfile = args.input
    outputfile = args.output
    dumpfile = args.dump
    
    # Open data
    scl = SclData()
    scl.Calculate(inputfile)

    # Dump the result
    if dumpfile != '':
        scl.DumpResult(dumpfile);
    
    # Draw data
    if outputfile != '':
        d = DrawScl()
        d.Draw(scl)
        d.Print(outputfile)
