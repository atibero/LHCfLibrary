import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TH1
from ROOT import TH2
import math
import sys

def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run
    
    
    
def DrawPhotonEnergyST():
    global c1, h1
    h1 = file.Get("h_energy_st_photon")
    c1 = ROOT.TCanvas("c1", "c1", 1000, 800)
    c1.cd()
    h1.Draw()
   
    
def DrawPhotonEnergyLT():
    global c2, h2
    h2 = file.Get("h_energy_lt_photon")
    c2 = ROOT.TCanvas("c2", "c2", 1000, 800)
    c2.cd()
    h2.Draw()
     
   
def DrawPhotonHitMap():
    global c3, h3
    h3 = file.Get("h_pos_photon")
    c3 = ROOT.TCanvas("c3", "c3", 1000, 800)
    c3.cd()
    h3.Draw("coltz")

def DrawNeutronEnergyST():
    global c4, h4
    h4 = file.Get("h_energy_st_neutron")
    c4 = ROOT.TCanvas("c4", "c4", 1000, 800)
    c4.cd()
    h4.Draw()
   
    
def DrawNeutronEnergyLT():
    global c5, h5
    h5 = file.Get("h_energy_lt_neutron")
    c5 = ROOT.TCanvas("c5", "c5", 1000, 800)
    c5.cd()
    h5.Draw()
     
   
def DrawNeutronHitMap():
    global c6, h6
    h6 = file.Get("h_pos_neutron")
    c6 = ROOT.TCanvas("c6", "c6", 1000, 800)
    c6.cd()
    h6.Draw("coltz")

def DrawDiPhotonMgg():
    global c7, h7
    h7 = file.Get("h_mgg")
    c7 = ROOT.TCanvas("c7", "c7", 1000, 800)
    c7.cd()
    h7.Draw()
   
    
def DrawPionEnergy():
    global c8, h8
    h8 = file.Get("h_energy_pion")
    c8 = ROOT.TCanvas("c8", "c8", 1000, 800)
    c8.cd()
    h8.Draw()
   
    
def DrawEtaEnergy():
    global c9, h9
    h9 = file.Get("h_energy_eta")
    c9 = ROOT.TCanvas("c9", "c9", 1000, 800)
    c9.cd()
    h9.Draw()   
   
def DrawDiPhotonHitMap():
    global c10, h10
    h10 = file.Get("h_pos_pi_eta")
    c10 = ROOT.TCanvas("c10", "c10", 1000, 800)
    c10.cd()
    h10.Draw("coltz")
