import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TH1
from ROOT import TH2
import math
import sys

ROOT.gStyle.SetStatY(0.9);                
ROOT.gStyle.SetStatX(0.9);                
ROOT.gStyle.SetStatW(0.2);                
ROOT.gStyle.SetStatH(0.1);   


def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run

def DrawMgg(name):
    global c1, h1
    h1 = file.Get("h_mgg%s" %name)
    c1 = ROOT.TCanvas("c1", "c1", 1000, 800)
    #c1.cd().SetLogx
    c1.cd().SetLogy()
    h1.Draw()

def DrawEnergySpectra(name):
    global c2, h2, h3, h4, h5

    h2 = file.Get("h_energy_st_photon%s" %name)
    h3 = file.Get("h_energy_lt_photon%s" %name)
    h4 = file.Get("h_energy_st_neutron%s" %name)
    h5 = file.Get("h_energy_lt_neutron%s" %name)

    c2 = ROOT.TCanvas("c2", "c2", 1000, 800)
    c2.Divide(2,2)
   # c2.cd(1).SetLogx()
    c2.cd(1).SetLogy()
    h2.Draw()
   # c2.cd(2).SetLogx()
    c2.cd(2).SetLogy()
    h3.Draw()
  #  c2.cd(3).SetLogx()
    c2.cd(3).SetLogy()
    h4.Draw()
  #  c2.cd(4).SetLogx()
    c2.cd(4).SetLogy()
    h5.Draw()
    
def DrawEnergySpectraPiEta(name):
    global c3, h6, h7

    h6 = file.Get("h_energy_pion%s" %name)
    h7 = file.Get("h_energy_eta%s" %name)
  

    c3 = ROOT.TCanvas("c3", "c3", 1000, 800)
    c3.Divide(2,2)
    c3.cd(1).SetLogx()
    c3.cd(1).SetLogy()
    h6.Draw()
    c3.cd(2).SetLogx()
    c3.cd(2).SetLogy()
    h7.Draw()
    
def DrawPtSpectra(name):
    global c4, h8, h9, h10, h11

    h8 = file.Get("h_pt_st_photon%s" %name)
    h9 = file.Get("h_pt_lt_photon%s" %name)
    h10 = file.Get("h_pt_st_neutron%s" %name)
    h11 = file.Get("h_pt_lt_neutron%s" %name)

    c4 = ROOT.TCanvas("c4", "c4", 1000, 800)
    c4.Divide(2,2)
    c4.cd(1).SetLogx()
    c4.cd(1).SetLogy()
    h8.Draw()
    c4.cd(2).SetLogx()
    c4.cd(2).SetLogy()
    h9.Draw()
    c4.cd(3).SetLogx()
    c4.cd(3).SetLogy()
    h10.Draw()
    c4.cd(4).SetLogx()
    c4.cd(4).SetLogy()
    h11.Draw()
    
def DrawPtSpectraPiEta(name):
    global c5, h12, h13
    
    
    h12 = file.Get("h_pt_pion%s" %name)
    h13 = file.Get("h_pt_eta%s" %name)
  

    c5 = ROOT.TCanvas("c5", "c5", 1000, 800)
    c5.Divide(2,2)
    
    c5.cd(1).SetLogx()
    c5.cd(1).SetLogy()
    h12.Draw()
    c5.cd(2).SetLogx()
    c5.cd(2).SetLogy()
    h13.Draw()
    
def DrawPhotonHitMap(name):
    global c6, h14
    

    
    h14 = file.Get("h_pos_photon%s" %name)
    h14.GetZaxis().SetLabelSize(0.025)
    c6 = ROOT.TCanvas("c6", "c6", 1000, 800)
    #c6.cd().SetLogx()
    #c6.cd().SetLogy()
    c6.cd().SetLogz()
    
    h14.Draw("coltz")
       

def DrawPhotonHitMapThr(name):
    global c7, h15
    
 
    
    h15 = file.Get("h_pos_photon_thr%s" %name)
    h15.GetZaxis().SetLabelSize(0.025)
    c7 = ROOT.TCanvas("c7", "c7", 1000, 800)
    #c7.cd().SetLogx()
    #c7.cd().SetLogy()
    c7.cd().SetLogz()
    
    h15.Draw("coltz")
    
def DrawNeutronHitMap(name):
    global c8, h16
    
   
    
    h16 = file.Get("h_pos_neutron%s" %name)
    h16.GetZaxis().SetLabelSize(0.025)
    c8 = ROOT.TCanvas("c8", "c8", 1000, 800)
    #c8.cd().SetLogx()
    #c8.cd().SetLogy()
    c8.cd().SetLogz()
    
    h16.Draw("coltz")
    
    
def DrawNeutronHitMapThr(name):
    global c9, h17
    


    h17 = file.Get("h_pos_neutron_thr%s" %name)
    h17.GetZaxis().SetLabelSize(0.025)
    c9 = ROOT.TCanvas("c9", "c9", 1000, 800)
   # c9.cd().SetLogx()
   # c9.cd().SetLogy()
    c9.cd().SetLogz()
    
    h17.Draw("coltz")
    
def DrawPiEtaHitMap(name):
    global c10, h18
    
    

    h18 = file.Get("h_pos_pi_eta%s" %name)
    
    h18.GetZaxis().SetLabelSize(0.025)

    c10 = ROOT.TCanvas("c10", "c10", 1000, 800)
    #c10.cd().SetLogx()
   # c10.cd().SetLogy()
    c10.cd().SetLogz()
    
    h18.Draw("coltz")



  
