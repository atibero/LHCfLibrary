import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TF2
from ROOT import TH1
from ROOT import TH2
from ROOT import TPaveText
import math
import sys

ROOT.gStyle.SetStatY(0.9);                
ROOT.gStyle.SetStatX(0.9);                
ROOT.gStyle.SetStatW(0.2);                
ROOT.gStyle.SetStatH(0.1);   

ROOT.gStyle.SetOptStat(11);
ROOT.gStyle.SetOptFit(111);

#only hadron and shower events

def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run
    
 
    
def position_fit(name):
    global c1, h1, fit1
    
    h1 = file.Get("h_pos_neutron%s" %name)
    h1.GetZaxis().SetLabelSize(0.025)
    c1 = ROOT.TCanvas("c1", "c1", 1000, 800)
    #c1.cd().SetLogx()
    #c1.cd().SetLogy()
    c1.cd().SetLogz()
    
    fit1 = ROOT.TF2("fit1","[0]*TMath::Exp(-[1]*TMath::Sqrt(pow(x-[2],2)+pow(y-[3],2)))", -10, 10, -10, 10)
    fit1.SetParameter(0, 100)
    fit1.SetParName(0, "A")
    fit1.SetParameter(1, 0.01)
    fit1.SetParName(1, "B")
    fit1.SetParameter(2, 0)
    fit1.SetParName(2, "x_{0}")
    fit1.SetParLimits(2, -10, 10)
    fit1.SetParameter(3, 0)
    fit1.SetParName(3, "y_{0}")
    fit1.SetParLimits(3, -10, 10)
    
    h1.Fit(fit1)
    
    
    h1.Draw("coltz")
    h1.SetStats(1)
    #fit1.Draw("same")
  