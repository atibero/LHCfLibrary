import ROOT
import drawhist_lvl3
import drawhist_lvl3_all_trigger
import make_position_fit
import sys
ROOT.gROOT.SetBatch(True)
args = sys.argv

file,Run = drawhist_lvl3.file(args[1],args[2])


trigger_name= ["" ,"_shower_trigger", "_pi0_trigger", "_high_em_trigger","_hadron_trigger", "_pedestal_trigger", "_zdc_trigger"];
trigger_title= [ "without selection trigger","Shower trigger", "#pi^{0} trigger", "high energy EM shower trigger", "hadron trigger", "pedestal trigger", "ZDC trigger"];
for name in trigger_name:
    drawhist_lvl3.DrawMgg(name)
    drawhist_lvl3.c1.Print("%s/Arm1-Pi0mass%s.gif" % (Run, name))
    drawhist_lvl3.DrawEnergySpectra(name)
    drawhist_lvl3.c2.Print("%s/Arm1-Spectrum%s.gif" % (Run, name))
    drawhist_lvl3.DrawEnergySpectraPiEta(name)
    drawhist_lvl3.c3.Print("%s/Arm1-Spectrum_Pi_Eta%s.gif" % (Run, name))
    drawhist_lvl3.DrawPtSpectra(name)
    drawhist_lvl3.c4.Print("%s/Arm1-Spectrum_Pt%s.gif" % (Run, name))
    drawhist_lvl3.DrawPtSpectraPiEta(name)
    drawhist_lvl3.c5.Print("%s/Arm1-Spectrum_Pt_Pi_Eta%s.gif" % (Run, name)) 
    drawhist_lvl3.DrawPhotonHitMap(name)
    drawhist_lvl3.c6.Print("%s/Arm1-Hitmap_Elemag%s.gif" % (Run, name))
    drawhist_lvl3.DrawPhotonHitMapThr(name)
    drawhist_lvl3.c7.Print("%s/Arm1-Hitmap_Elemag_1TeV%s.gif" % (Run, name))
    drawhist_lvl3.DrawNeutronHitMap(name)
    drawhist_lvl3.c8.Print("%s/Arm1-Hitmap_Hadron%s.gif" % (Run, name))
    drawhist_lvl3.DrawNeutronHitMapThr(name)
    drawhist_lvl3.c9.Print("%s/Arm1-Hitmap_Hadron_1TeV%s.gif" % (Run, name))
    drawhist_lvl3.DrawPiEtaHitMap(name)
    drawhist_lvl3.c10.Print("%s/Arm1-Hitmap_Pi_Eta%s.gif" % (Run, name))
    

file,Run = drawhist_lvl3_all_trigger.file(args[1],args[2])

drawhist_lvl3_all_trigger.DrawMgg(trigger_name, trigger_title)
drawhist_lvl3_all_trigger.c1.Print("%s/Arm1-Pi0mass_all_trigger.gif" %Run)    
drawhist_lvl3_all_trigger.DrawEnergySpectra(trigger_name, trigger_title)
drawhist_lvl3_all_trigger.c2.Print("%s/Arm1-Spectrum_all_trigger.gif" %Run)
drawhist_lvl3_all_trigger.DrawEnergySpectraPiEta(trigger_name, trigger_title)
drawhist_lvl3_all_trigger.c3.Print("%s/Arm1-Spectrum_Pi_Eta_all_trigger.gif" %Run)
drawhist_lvl3_all_trigger.DrawPtSpectra(trigger_name, trigger_title)
drawhist_lvl3_all_trigger.c4.Print("%s/Arm1-Spectrum_Pt_all_trigger.gif" %Run)
drawhist_lvl3_all_trigger.DrawPtSpectraPiEta(trigger_name, trigger_title)
drawhist_lvl3_all_trigger.c5.Print("%s/Arm1-Spectrum_Pt_Pi_Eta_all_trigger.gif" %Run)

file,Run = make_position_fit.file(args[1],args[2])


trigger_name= ["_shower_trigger","_hadron_trigger"];
trigger_title= ["Shower trigger", "hadron trigger"];

for name in trigger_name:
    make_position_fit.position_fit(name)
    make_position_fit.c1.Print("%s/Arm1-Position_fit%s.gif" % (Run, name))
