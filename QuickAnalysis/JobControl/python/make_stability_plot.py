import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TF2
from ROOT import TH1
from ROOT import TH2
from ROOT import TPaveText
from ROOT import TGraph
from ROOT import TGraphErrors

import math
import sys

ROOT.gStyle.SetStatY(0.9);                
ROOT.gStyle.SetStatX(0.9);                
ROOT.gStyle.SetStatW(0.2);                
ROOT.gStyle.SetStatH(0.1);   


#only hadron and shower events

def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run
    
def stability_plot_pion():
    global c1, h1
    h1 = file.Get("g_stability_pion")
    c1 = ROOT.TCanvas("c1", "c1", 1000, 800)
    h1.Draw("ap")
    
def stability_plot_eta():
    global c2, h2
    h2 = file.Get("g_stability_eta")
    c2 = ROOT.TCanvas("c2", "c2", 1000, 800)
    h2.Draw("ap")
    
def mgg_fit():
    global c3, h3
    h3 = file.Get("h_mgg")
    c3 = ROOT.TCanvas("c3", "c3", 1000, 800)
    c3.cd().SetLogy
    h3.Draw()


    

  