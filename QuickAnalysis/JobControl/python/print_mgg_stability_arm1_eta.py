import ROOT
import make_stability_plot
import sys
ROOT.gROOT.SetBatch(True)
args = sys.argv

file,Run = make_stability_plot.file(args[1],args[2])


make_stability_plot.stability_plot_eta()
make_stability_plot.c2.Print("%s/Arm1-Stability_eta.gif" % Run)
make_stability_plot.mgg_fit()
make_stability_plot.c3.Print("%s/Arm1-Etamass_fit.gif" % Run)