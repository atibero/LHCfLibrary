import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TH1
from ROOT import TH2
import math
import sys

ROOT.gStyle.SetStatY(0.9);                
ROOT.gStyle.SetStatX(0.9);                
ROOT.gStyle.SetStatW(0.2);                
ROOT.gStyle.SetStatH(0.1);   


def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run
    

def DrawHitMap():
    global c1, h1
    h1 = file.Get("hitmap_adamo")
    c1 = ROOT.TCanvas("c1", "c1", 1000, 800)
    #c1.cd().SetLogx
    #c1.cd().SetLogy()
    h1.Draw("coltz")    
    
def DrawHitMapCalRel():
    global c2, h2
    h2 = file.Get("hitmap_adamo_cal_release")
    c2 = ROOT.TCanvas("c2", "c2", 1000, 800)
    #c1.cd().SetLogx
    #c1.cd().SetLogy()
    h2.Draw("coltz")    
        
    





  
