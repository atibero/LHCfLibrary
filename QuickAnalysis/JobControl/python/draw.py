import ROOT
import drawhist
import sys
ROOT.gROOT.SetBatch(True)

args = sys.argv
#file = [ROOT.TFile(args[1]), ROOT.TFile(args[2])]

#file = [ROOT.TFile("./a1hist2.root"),ROOT.TFile("../../LHCfLibrary/build/pedestal/ped_run44299_10.root")]

file,Run,Add,Nsub = drawhist.file(args[1],args[2],args[3],args[4],args[5])

#file,Run = drawhist.file(args[1],args[2],args[3])

drawhist.drawcalhist(0,0)
if drawhist.c1:
    drawhist.c1.Print("%s/Hist20mmCal_WRADC.gif" % Run)

drawhist.drawcalhist(1,0)
if drawhist.c1:
    drawhist.c1.Print("%s/Hist40mmCal_WRADC.gif" % Run)

drawhist.drawcalhist(0,1)
if drawhist.c1:
    drawhist.c1.Print("%s/Hist20mmCal_NRADC.gif" % Run)

drawhist.drawcalhist(1,1)
if drawhist.c1:
    drawhist.c1.Print("%s/Hist40mmCal_NRADC.gif" % Run)

drawhist.drawflaghist()
if drawhist.c2:
    drawhist.c2.Print("%s/HistArm1Flag.gif" % Run)

drawhist.drawposhist()
if drawhist.c3:
    drawhist.c3.Print("%s/GSObarAccumlate.gif" % Run)

drawhist.calpedav()
if drawhist.c4:
    drawhist.c4.Print("%s/CalPedestalAv.gif" % Run)

drawhist.calpedrms()
if drawhist.c5:
    drawhist.c5.Print("%s/CalPedestalRMS.gif" % Run)

drawhist.pospedav(Add,Nsub)
#drawhist.pospedav()
if drawhist.c6:
    drawhist.c6.Print("%s/GSObarPedestalAv.gif" % Run)

drawhist.pospedrms(Add,Nsub)
#drawhist.pospedrms()
if drawhist.c7:
    drawhist.c7.Print("%s/GSObarPedestalRMS.gif" % Run)

drawhist.drawa2calhist(0,0)
if drawhist.c8:
    drawhist.c8.Print("%s/Hist25mmCal_WRADC.gif" % Run)

drawhist.drawa2calhist(1,0)
if drawhist.c8:
    drawhist.c8.Print("%s/Hist32mmCal_WRADC.gif" % Run)

drawhist.drawa2calhist(0,1)
if drawhist.c8:
    drawhist.c8.Print("%s/Hist25mmCal_NRADC.gif" % Run)

drawhist.drawa2calhist(1,1)
if drawhist.c8:
    drawhist.c8.Print("%s/Hist32mmCal_NRADC.gif" % Run)

drawhist.drawa2poshist()
if drawhist.c9:
    drawhist.c9.Print("%s/SiliconAccumlate.gif" % Run)

drawhist.a2pospedav(Add,Nsub)
#drawhist.a2pospedav()
if drawhist.c10:
    drawhist.c10.Print("%s/SiliconPedestalAv.gif" % Run)

drawhist.a2pospedrms(Add,Nsub)
#drawhist.a2pospedrms()
if drawhist.c11:
    drawhist.c11.Print("%s/SiliconPedestalRMS.gif" % Run)

drawhist.drawa2flaghist()
if drawhist.c12:
    drawhist.c12.Print("%s/HistArm2Flag.gif" % Run)

drawhist.drawTDChist(0)
if drawhist.c13:
    drawhist.c13.Print("%s/HistArm1TDC.gif" % Run)

drawhist.drawTDChist(1)
if drawhist.c13:
    drawhist.c13.Print("%s/HistArm2TDC.gif" % Run)

drawhist.drawDSChist(0,0)
if drawhist.c14:
    drawhist.c14.Print("%s/DSC_Arm1-Small.gif" % Run)

drawhist.drawDSChist(0,1)
if drawhist.c14:
    drawhist.c14.Print("%s/DSC_Arm1-Large.gif" % Run)

drawhist.drawDSChist(1,0)
if drawhist.c14:
    drawhist.c14.Print("%s/DSC_Arm2-Small.gif" % Run)

drawhist.drawDSChist(1,1)
if drawhist.c14:
    drawhist.c14.Print("%s/DSC_Arm2-Large.gif" % Run)

#Not at SPS!
drawhist.drawDSCFChist()
if drawhist.c15:
   drawhist.c15.Print("%s/DSC_FCs.gif" % Run)

drawhist.drawotherhist(0)
if drawhist.c16:
    drawhist.c16.Print("%s/HistArm1OtherADCs.gif" % Run)

drawhist.drawotherhist(1)
if drawhist.c16:
    drawhist.c16.Print("%s/HistArm2OtherADCs.gif" % Run) 

#Not at SPS!
drawhist.drawzdchist()
if drawhist.c17:
   drawhist.c17.Print("%s/ZDC.gif" % Run)

drawhist.drawDSChist_lvl2(0,0)
if drawhist.c19:
    drawhist.c19.Print("%s/DSC_lvl2_Arm1-Small.gif" % Run)

drawhist.drawDSChist_lvl2(0,1)
if drawhist.c19:
    drawhist.c19.Print("%s/DSC_lvl2_Arm1-Large.gif" % Run)

drawhist.drawDSChist_lvl2(1,0)
if drawhist.c19:
    drawhist.c19.Print("%s/DSC_lvl2_Arm2-Small.gif" % Run)

drawhist.drawDSChist_lvl2(1,1)
if drawhist.c19:
    drawhist.c19.Print("%s/DSC_lvl2_Arm2-Large.gif" % Run)

#Not at SPS!
drawhist.drawDSCFChist_lvl2()
if drawhist.c18:
   drawhist.c18.Print("%s/DSC_FCs_lvl2.gif" % Run)
