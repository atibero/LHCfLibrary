import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TH1
from ROOT import TH2
from ROOT import TLegend
import math
import sys

#string_remove = "shower trigger"
string_remove = "without selection trigger"

def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run

def DrawMgg(name_list, title_list):
    global c1, h1, leg1
    c1 = ROOT.TCanvas("c1", "c1", 1000, 800)
    #c1.cd().SetLogx
    c1.cd().SetLogy()
    
    leg1 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    
    
    for i in range(7):
        h1 = file.Get("h_mgg%s" %name_list[i])
        h1.SetLineColor(1+i)
        h1.SetStats(0)
        title_old=h1.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h1.SetTitle(title_new)
        leg1.AddEntry(h1, "%s" %title_list[i])
        h1.Draw("same")
        
    leg1.Draw("same")

def DrawEnergySpectra(name_list, title_list):
    global c2, h2, h3, h4, h5, leg2, leg3, leg4, leg5

    c2 = ROOT.TCanvas("c2", "c2", 1000, 800)
    c2.Divide(2,2)
    
    leg2 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg3 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg4 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg5 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    
    
    for i in range(7):
        h2 = file.Get("h_energy_st_photon%s" %name_list[i])
        h3 = file.Get("h_energy_lt_photon%s" %name_list[i])
        h4 = file.Get("h_energy_st_neutron%s" %name_list[i])
        h5 = file.Get("h_energy_lt_neutron%s" %name_list[i])
        
        h2.SetLineColor(1+i)
        h2.SetStats(0)
        title_old=h2.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h2.SetTitle(title_new)
        leg2.AddEntry(h2, "%s" %title_list[i])
        
        h3.SetLineColor(1+i)
        h3.SetStats(0)
        title_old=h3.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h3.SetTitle(title_new)
        leg3.AddEntry(h3, "%s" %title_list[i])
        
        h4.SetLineColor(1+i)
        h4.SetStats(0)
        title_old=h4.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h4.SetTitle(title_new)
        leg4.AddEntry(h4, "%s" %title_list[i])
        
        h5.SetLineColor(1+i)
        h5.SetStats(0)
        title_old=h5.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h5.SetTitle(title_new)
        leg5.AddEntry(h5, "%s" %title_list[i])

       
        #c2.cd(1).SetLogx()
        c2.cd(1).SetLogy()
        h2.Draw("same")
        leg2.Draw("same")
       # c2.cd(2).SetLogx()
        c2.cd(2).SetLogy()
        h3.Draw("same")
        leg3.Draw("same")
       # c2.cd(3).SetLogx()
        c2.cd(3).SetLogy()
        h4.Draw("same")
        leg4.Draw("same")
       # c2.cd(4).SetLogx()
        c2.cd(4).SetLogy()
        h5.Draw("same")
        leg5.Draw("same")
    
def DrawEnergySpectraPiEta(name_list, title_list):
    global c3, h6, h7, leg6, leg7

    c3 = ROOT.TCanvas("c3", "c3", 1000, 800)
    c3.Divide(2,2)
    
    leg6 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg7 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    
    
    for i in range(7):
        h6 = file.Get("h_energy_pion%s" %name_list[i])
        h7 = file.Get("h_energy_eta%s" %name_list[i])
        
        h6.SetLineColor(1+i)
        h6.SetStats(0)
        title_old=h6.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h6.SetTitle(title_new)
        leg6.AddEntry(h6, "%s" %title_list[i])
        
        h7.SetLineColor(1+i)
        h7.SetStats(0)
        title_old=h7.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h7.SetTitle(title_new)
        leg7.AddEntry(h7, "%s" %title_list[i])
      
        c3.cd(1).SetLogx()
        c3.cd(1).SetLogy()
        h6.Draw("same")
        leg6.Draw("same")
        c3.cd(2).SetLogx()
        c3.cd(2).SetLogy()
        h7.Draw("same")
        leg7.Draw("same")
        
        
    
def DrawPtSpectra(name_list, title_list):
    global c4, h8, h9, h10, h11, leg8, leg9, leg10, leg11

    c4 = ROOT.TCanvas("c4", "c4", 1000, 800)
    c4.Divide(2,2)
    
    leg8 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg9 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg10 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg11 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    
    
    for i in range(7):
        h8 = file.Get("h_pt_st_photon%s" %name_list[i])
        h9 = file.Get("h_pt_lt_photon%s" %name_list[i])
        h10 = file.Get("h_pt_st_neutron%s" %name_list[i])
        h11 = file.Get("h_pt_lt_neutron%s" %name_list[i])
        
        h8.SetLineColor(1+i)
        h8.SetStats(0)
        title_old=h8.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h8.SetTitle(title_new)
        leg8.AddEntry(h8, "%s" %title_list[i])
        
        h9.SetLineColor(1+i)
        h9.SetStats(0)
        title_old=h9.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h9.SetTitle(title_new)
        leg9.AddEntry(h9, "%s" %title_list[i])
        
        h10.SetLineColor(1+i)
        h10.SetStats(0)
        title_old=h10.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h10.SetTitle(title_new)
        leg10.AddEntry(h10, "%s" %title_list[i])
        
        h11.SetLineColor(1+i)
        h11.SetStats(0)
        title_old=h11.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h11.SetTitle(title_new)
        leg11.AddEntry(h11, "%s" %title_list[i])

       
        c4.cd(1).SetLogx()
        c4.cd(1).SetLogy()
        h8.Draw("same")
        leg8.Draw("same")
        c4.cd(2).SetLogx()
        c4.cd(2).SetLogy()
        h9.Draw("same")
        leg9.Draw("same")
        c4.cd(3).SetLogx()
        c4.cd(3).SetLogy()
        h10.Draw("same")
        leg10.Draw("same")
        c4.cd(4).SetLogx()
        c4.cd(4).SetLogy()
        h11.Draw("same")
        leg11.Draw("same")
    
    
    
def DrawPtSpectraPiEta(name_list, title_list):
    global c5, h12, h13, leg12, leg13

    c5 = ROOT.TCanvas("c5", "c5", 1000, 800)
    c5.Divide(2,2)
    
    leg12 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    leg13 = ROOT.TLegend(0.622,0.725,0.900,0.902,"","brNDC")
    
    
    for i in range(7):
        h12 = file.Get("h_pt_pion%s" %name_list[i])
        h13 = file.Get("h_pt_eta%s" %name_list[i])
        
        h12.SetLineColor(1+i)
        h12.SetStats(0)
        title_old=h12.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h12.SetTitle(title_new)
        leg12.AddEntry(h12, "%s" %title_list[i])
        
        h13.SetLineColor(1+i)
        h13.SetStats(0)
        title_old=h13.GetTitle()
        if title_old.endswith(string_remove):
           title_new = title_old.replace(string_remove, '')
        h13.SetTitle(title_new)
        leg13.AddEntry(h13, "%s" %title_list[i])
      
        c5.cd(1).SetLogx()
        c5.cd(1).SetLogy()
        h12.Draw("same")
        leg12.Draw("same")
        c5.cd(2).SetLogx()
        c5.cd(2).SetLogy()
        h13.Draw("same")
        leg13.Draw("same")
        



  
