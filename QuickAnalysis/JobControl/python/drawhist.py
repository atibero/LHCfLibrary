import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TH1
import math
import sys

#file = [ROOT.TFile("./a1hist2.root"),ROOT.TFile("../../LHCfLibrary/build/pedestal/ped_run44299_10.root")]
#file = [sys.argv[1],sys.argv[2]]

#def file(hist,ped,run):
#    global file,Run
#    file = [ROOT.TFile(hist),ROOT.TFile(ped)] # file[1](pedestal) = calibrated file
#    Run = run
#    return file,Run

def file(hist,ped,run,add,nsub):
    global file,Run,Add,Nsub
    file = [ROOT.TFile(hist),ROOT.TFile(ped)] # file[1](pedestal) = calibrated file
    Run = run
    Add = add
    Nsub = nsub
    return file,Run,Add,Nsub


def drawcalhist(tower,ir):
    global c1,h1,h2,leg1,h32,h33,h34,h35,h34_rebin,h2_rebin,h47,h48,h61,leg17
    #h1 = [[0] * 16 for i in range(2)]
    #h2 = [[0] * 16 for i in range(2)]
    h1 = []
    #h2 = []
    h32 = []
    h33 = []
    h34 = [0]*16
    h35 = [0]*16
    leg1=[]
    h34_rebin=[]
    #h2_rebin = []
    h61 = [[[[0]*2 for i in range(16)]for j in range(2)]for k in range(5)]
    leg17 = ROOT.TLegend(0.6,0.3,0.9,0.65,"")
   
    arm1=True

    #for it in range(2):
    for il in range(16):
        h1.append(file[0].Get("A1Cal0_%d_%d_%d" % (tower,il,ir)))
        #h2.append(file[0].Get("A1CalPed_%d_%d_%d" % (tower,il,ir)))
        h32.append(file[0].Get("a1_calped_%d_%d_%d" % (tower,il,ir)))
        h33.append(file[0].Get("a1_caloffped_%d_%d_%d" % (tower,il,ir)))
        if not h1[il]:
                arm1=False
        if not h32[il]:
                arm1=False
        if not h33[il]:
                arm1=False
        ROOT.gStyle.SetOptLogy()
        leg1.append(ROOT.TLegend(0.4,0.7,0.65,0.9,""))
        ROOT.gStyle.SetStatH(0.2);
        ROOT.gStyle.SetStatW(0.3);

    for il in range(16):
        #h2_rebin.append(h2[il].Rebin(10))
        h34[il] = h32[il]-h33[il]
        #h35[il] = h2[il]-h33[il]
    #    h34_rebin.append(h34[il].Rebin(10))

    for itrg in range(5):
        for il in range(16):
            h61[itrg][tower][il][ir] = file[0].Get("Arm1_cal_lvl0_eachtrg_%d_%d_%d_%d" % (itrg,tower,il,ir))
            if not h61[itrg][tower][il][ir]:
                arm1=False
    
    c1 = ROOT.TCanvas("c1","Hist%dmmCal" % (20 if tower==0 else 40),1000,800)

    if arm1:
    
        #for it in range(2):
        for il in range(16):
            h1[il].GetXaxis().SetTitle("ADCcount")
            h1[il].GetYaxis().SetTitle("$\Delta$N")
            h1[il].SetTitleSize(0.05,"x")
            h1[il].SetTitleSize(0.05,"y")
            h1[il].SetLabelSize(0.05,"x")
            h1[il].SetLabelSize(0.05,"y")
            h1[il].GetYaxis().CenterTitle()
            h1[il].GetYaxis().SetTitleOffset(0.6)
            h1[il].SetLineColor(ROOT.kRed)
            h32[il].SetLineColor(ROOT.kBlue)
            h33[il].SetLineColor(ROOT.kGreen+2)
            h34[il].SetLineColor(ROOT.kYellow+2)
            leg1[il].AddEntry(h1[il],"beam","lp")
            leg1[il].AddEntry(h32[il],"ped","lp")
            #leg1[il].AddEntry(h33[il],"pedoff","lp")
            #leg1[il].AddEntry(h34[il],"pedsubtract","lp")
            leg1[il].SetFillStyle(0)
            h61[0][tower][il][ir].SetLineColor(ROOT.kBlack)
            h61[1][tower][il][ir].SetLineColor(ROOT.kYellow+2)
            h61[2][tower][il][ir].SetLineColor(ROOT.kGreen+2)
            h61[3][tower][il][ir].SetLineColor(ROOT.kViolet+2)
            h61[4][tower][il][ir].SetLineColor(ROOT.kMagenta)
    
        leg17.AddEntry(h61[0][tower][0][ir],"Shower","lp")
        leg17.AddEntry(h61[1][tower][0][ir],"#pi^{0}","lp")
        leg17.AddEntry(h61[2][tower][0][ir],"HighEM","lp")
        leg17.AddEntry(h61[3][tower][0][ir],"Hadron","lp")
        leg17.AddEntry(h61[4][tower][0][ir],"ZDC","lp")
    
        c1.Divide(4,4,1e-10,1e-10)
        
        for il in range(16):
            c1.cd(il+1)
            h1[il].Draw("")
            h32[il].Draw("same")
            #h33[il].Draw("same")
            for itrg in range(5):
                h61[itrg][tower][il][ir].Draw("same")
            #h34[il].Draw("same")
            #h2_rebin[il].Draw("same")
            #h34_rebin[il].Draw("same")
            #h2[il].Draw("same")
            leg1[il].Draw("same")
            if il==0:
               leg17.Draw("same")

def drawflaghist():
    global c2,h3,leg2,h47
    h3 = []
    h47 = []
    leg2 = []
    maxbin_beam = []
    maxbin_ped = []
    maxvalue = []

    arm1=True

    for i in range(3):
        h3.append(file[0].Get("A1Flag_ev_%d" % i))
        h47.append(file[0].Get("A1Flag_ped_%d" % i))
        if not h3[i]:
            arm1=False
        if not h47[i]:
            arm1=False
        leg2.append(ROOT.TLegend(0.4,0.7,0.65,0.9,""))

    c2 = ROOT.TCanvas("c2","HistArm1Flag",400,800)

    if arm1:

        for i in range(3):
            h3[i].GetXaxis().SetTitle("Bit")
            h3[i].GetYaxis().SetTitle("$\Delta$N")
            h3[i].SetTitleSize(0.05,"x")
            h3[i].SetTitleSize(0.05,"y")
            h3[i].SetLabelSize(0.05,"x")
            h3[i].SetLabelSize(0.05,"y")
            h3[i].GetYaxis().SetTitleOffset(0.8)
            h3[i].SetStats(0)
            h3[i].SetLineColor(ROOT.kRed)
            h47[i].SetLineColor(ROOT.kBlue)
            leg2[i].AddEntry(h3[i],"beam","lp")
            leg2[i].AddEntry(h47[i],"ped","lp")
            leg2[i].SetFillStyle(0)
            maxbin_beam.append(h3[i].GetMaximumBin())
            maxbin_ped.append(h47[i].GetMaximumBin())
    
        for i in range(3):
            maxvalue.append(max(h3[i].GetBinContent(maxbin_beam[i]),h47[i].GetBinContent(maxbin_ped[i])))
            
        c2.Divide(1,3,1e-10,1e-10)
    
        for i in range(3):
            c2.cd(i+1)
            h3[i].GetYaxis().SetRangeUser(1,maxvalue[i]*1.1)
            h3[i].Draw("")
            h47[i].Draw("same")
            leg2[i].Draw("same")

def drawposhist():
    global c3,h4,h5,leg3,pad1
    h4 = [[0] * 4 for i in range(2)]
    h5 = [[0] * 4 for i in range(2)]
    leg3 = [[0] * 4 for i in range(2)]
    pad1 = []
    maxbin_TS = [[0] * 2 for i in range(4)]
    maxbin_TL = [[0] * 2 for i in range(4)]
    minbin_TS = [[0] * 2 for i in range(4)]
    minbin_TL = [[0] * 2 for i in range(4)]
    maxvalue_TS = []
    maxvalue_TL = []
    minvalue_TS = []
    minvalue_TL = []

    arm1=False

    for it in range(2):
        for il in range(4):
            h4[it][il] = file[0].Get("A1Pos_%d_%d_%d" % (it,il,0))#X
            h5[it][il] = file[0].Get("A1Pos_%d_%d_%d" % (it,il,1))#Y
            if not h4[it][il]:
                    arm1=False
            if not h5[it][il]:
                    arm1=False
            leg3[it][il] = ROOT.TLegend(0.8,0.7,0.9,0.9,"")
            ROOT.gStyle.SetStatH(0.2);
            ROOT.gStyle.SetStatW(0.3);
            ROOT.gStyle.SetOptLogy(0)

    c3 = ROOT.TCanvas("c3","GSObarAccumlate",1000,800)

    if arm1:

        for it in range(2):
            for il in range(4):
                h4[it][il].SetTitle("Tower:%d_Layer:%d" % (it,il))
                h4[it][il].GetXaxis().SetTitle("channel")
                h4[it][il].GetYaxis().SetTitle("ADCcount")
                h5[it][il].SetLineColor(ROOT.kRed)
                h4[it][il].SetTitleSize(0.05,"x")
                h4[it][il].SetTitleSize(0.05,"y")
                h4[it][il].SetLabelSize(0.05,"x")
                h4[it][il].SetLabelSize(0.05,"y")
                h4[it][il].GetYaxis().SetTitleOffset(0.8)
                #h4[it][il].GetYaxis().SetRangeUser(0,100000)
                leg3[it][il].AddEntry(h4[it][il],"X","lp")
                leg3[it][il].AddEntry(h5[it][il],"Y","lp")
                leg3[it][il].SetFillStyle(0)
                h4[it][il].SetStats(0)
    
        for il in range(4):#h4[it][il] = x, h5[it][il] = y, maxbin_TS[il][iv]
            maxbin_TS[il][0] = h4[0][il].GetMaximumBin() #TS,x,max
            minbin_TS[il][0] = h4[0][il].GetMinimumBin() #TS,x,min
            maxbin_TS[il][1] = h5[0][il].GetMaximumBin() #TS,y,max
            minbin_TS[il][1] = h5[0][il].GetMinimumBin() #TS,y,min
            maxbin_TL[il][0] = h4[1][il].GetMaximumBin()
            minbin_TL[il][0] = h4[1][il].GetMinimumBin()
            maxbin_TL[il][1] = h5[1][il].GetMaximumBin()
            minbin_TL[il][1] = h5[1][il].GetMinimumBin()
            
    
        for il in range(4):
            if h4[0][il].GetBinContent(maxbin_TS[il][0]) > h5[0][il].GetBinContent(maxbin_TS[il][1]):
                maxvalue_TS.append(h4[0][il].GetBinContent(maxbin_TS[il][0]))
            else:
                maxvalue_TS.append(h5[0][il].GetBinContent(maxbin_TS[il][1]))
                                   
            if h4[0][il].GetBinContent(minbin_TS[il][0]) < h5[0][il].GetBinContent(minbin_TS[il][1]):
                minvalue_TS.append(h4[0][il].GetBinContent(minbin_TS[il][0]))
            else:
                minvalue_TS.append(h5[0][il].GetBinContent(minbin_TS[il][1]))
                                   
            if h4[1][il].GetBinContent(maxbin_TL[il][0]) > h5[1][il].GetBinContent(maxbin_TL[il][1]):
                maxvalue_TL.append(h4[1][il].GetBinContent(maxbin_TL[il][0]))
            else:
                maxvalue_TL.append(h5[1][il].GetBinContent(maxbin_TL[il][1]))
                                   
            if h4[1][il].GetBinContent(minbin_TL[il][0]) < h5[1][il].GetBinContent(minbin_TL[il][1]):
                minvalue_TL.append(h4[1][il].GetBinContent(minbin_TL[il][0]))
            else:
                minvalue_TL.append(h5[1][il].GetBinContent(minbin_TL[il][1]))
    
            
        #c3.Divide(2,4,1e-10,1e-10)
        #Divide canvas to 4:6
        x1 = 0.0
        x2 = 0.0
        y1 = 0.75
        y2 = 1.0
        for j in range(8):
            if j%2 == 0:
                x1 = 0.0
                x2 = 0.4
            else:
                x1 = 0.4
                x2 = 1.0
            if (j%2 == 0 and j!=0):
                y1-=0.25
                y2-=0.25
            
            pad1.append(ROOT.TPad("pad1_%d" % j, "pad1_%d" % j, x1, y1, x2, y2))
        #print(pad1)
        for j in range(8):
            c3.cd(j)
            pad1[j].SetBottomMargin(0.1)  # joins upper and lower plot
            #pad1[j].SetGridx()
            pad1[j].Draw()
    
        i = 0
        for il in range(4):
            for it in range(2):
                pad1[i].cd()
                if it==0:
                    h4[it][il].GetYaxis().SetRangeUser(minvalue_TS[il],maxvalue_TS[il]*1.1)
                else:
                    h4[it][il].GetYaxis().SetRangeUser(minvalue_TL[il],maxvalue_TL[il]*1.1)
                h4[it][il].Draw("HIST")
                h5[it][il].Draw("HISTsame")
                leg3[it][il].Draw("same")
                i += 1

def calpedav():
    global c4,h6,h7,h8,h9,h36,h37,h38,h39
    h6 = [[0]*16 for i in range(2)]
    h7 = [[0]*16 for i in range(2)]
    h8 = []
    h9 = []
    h36 = [[0]*16 for i in range(2)]
    h37 = [[0]*16 for i in range(2)]
    h38 = []
    h39 = []

    arm1=True
    arm2=True

    for it in range(2):
        for il in range(16):
            h6[it][il] = file[0].Get("a1_calped_%d_%d_0" % (it,il))
            h7[it][il] = file[0].Get("a1_caloffped_%d_%d_0" % (it,il))
            if not h6[it][il]:
                arm1=False
            if not h7[it][il]:
                arm1=False
            h36[it][il] = file[0].Get("a2_calped_%d_%d_0" % (it,il))
            h37[it][il] = file[0].Get("a2_caloffped_%d_%d_0" % (it,il))
            if not h36[it][il]:
                arm2=False
            if not h37[it][il]:
                arm2=False
           
    for it in range(2):
        h8.append(ROOT.TH1F("h8_%d" % it,"Tower:%dmm_PedeAv" % (20 if it==0 else 40),16,0,16))
        h9.append(ROOT.TH1F("h9_%d" % it,"Tower:%dmm_PedeDelayAv" % (20 if it==0 else 40),16,0,16))
        if not h8[it]:
            arm1=False
        if not h9[it]:
            arm1=False
        h38.append(ROOT.TH1F("h38_%d" % it,"Tower:%dmm_PedeAv" % (25 if it==0 else 32),16,0,16))
        h39.append(ROOT.TH1F("h39_%d" % it,"Tower:%dmm_PedeDelayAv" % (25 if it==0 else 32),16,0,16))
        if not h38[it]:
            arm2=False
        if not h39[it]:
            arm2=False
        ROOT.gStyle.SetOptLogy(0)
        
    for it in range(2):
        if arm1:
            h8[it].SetStats(0)
            h9[it].SetStats(0)
            for il in range(16):
                h8[it].Fill(il,(h6[it][il].GetMean()))
                h9[it].Fill(il,(h7[it][il].GetMean()))
                h9[it].GetYaxis().SetRangeUser(-60,60)
                h8[it].GetXaxis().SetTitle("Layer")
                h9[it].GetXaxis().SetTitle("Layer")
                h8[it].GetYaxis().SetTitle("Pedestal Average")
                h9[it].GetYaxis().SetTitle("Pedestal Average")
                h8[it].SetTitleSize(0.05,"x")
                h8[it].SetTitleSize(0.05,"y")
                h8[it].SetLabelSize(0.05,"x")
                h8[it].SetLabelSize(0.05,"y")
                h9[it].SetTitleSize(0.05,"x")
                h9[it].SetTitleSize(0.05,"y")
                h9[it].SetLabelSize(0.05,"x")
                h9[it].SetLabelSize(0.05,"y")
                h8[it].GetYaxis().SetTitleOffset(0.8)
                h9[it].GetYaxis().SetTitleOffset(0.8)

        if arm2:
            h38[it].SetStats(0)
            h39[it].SetStats(0)
            for il in range(16):
                h38[it].Fill(il,(h36[it][il].GetMean()))
                h39[it].Fill(il,(h37[it][il].GetMean()))
                h39[it].GetYaxis().SetRangeUser(-60,60)
                h38[it].GetXaxis().SetTitle("Layer")
                h39[it].GetXaxis().SetTitle("Layer")
                h38[it].GetYaxis().SetTitle("Pedestal Average")
                h39[it].GetYaxis().SetTitle("Pedestal Average")
                h38[it].SetTitleSize(0.05,"x")
                h38[it].SetTitleSize(0.05,"y")
                h38[it].SetLabelSize(0.05,"x")
                h38[it].SetLabelSize(0.05,"y")
                h39[it].SetTitleSize(0.05,"x")
                h39[it].SetTitleSize(0.05,"y")
                h39[it].SetLabelSize(0.05,"x")
                h39[it].SetLabelSize(0.05,"y")
                h38[it].GetYaxis().SetTitleOffset(0.8)
                h39[it].GetYaxis().SetTitleOffset(0.8)
    c4 = ROOT.TCanvas("c4","CalPedestalAv",800,600)
    c4.Divide(2,4,1e-10,1e-10)

    i = 0
   
    if arm1:
        c4.cd(1)
        h8[0].Draw("HIST")
        c4.cd(2)
        h9[0].Draw("HIST")
        c4.cd(3)
        h8[1].Draw("HIST")
        c4.cd(4)
        h9[1].Draw("HIST")
    if arm2:
        c4.cd(5)
        h38[0].Draw("HIST")
        c4.cd(6)
        h39[0].Draw("HIST")
        c4.cd(7)
        h38[1].Draw("HIST")
        c4.cd(8)
        h39[1].Draw("HIST")
    

def calpedrms():
    global c5,h10,h11,h12,h13,leg4,h40,h41,h42,h43,leg9
    h10 = [[0]*16 for i in range(2)]
    h11 = [[0]*16 for i in range(2)]
    h12 = []
    h13 = []
    h40 = [[0]*16 for i in range(2)]
    h41 = [[0]*16 for i in range(2)]
    h42 = []
    h43 = []
    leg4 = []
    leg9 = []
    maxbin_Arm1 = []
    maxbin_Arm2 = []

    arm1=True
    arm2=True

    for it in range(2):
        for il in range(16):
            h10[it][il] = file[0].Get("a1_calped_%d_%d_0" % (it,il))
            h11[it][il] = file[0].Get("a1_caloffped_%d_%d_0" % (it,il))
            if not h10[it][il]:
                arm1=False
            if not h11[it][il]:
                arm1=False
            h40[it][il] = file[0].Get("a2_calped_%d_%d_0" % (it,il))
            h41[it][il] = file[0].Get("a2_caloffped_%d_%d_0" % (it,il))
            if not h40[it][il]:
                arm2=False
            if not h41[it][il]:
                arm2=False
            
    for it in range(2):
        h12.append(ROOT.TH1F("h12_%d" % it,"Tower:%dmm_Pede.RMS" % (20 if it==0 else 40),16,0,16))
        h13.append(ROOT.TH1F("h13_%d" % it,"Tower:%dmm_PedeDelay.RMS" % (20 if it==0 else 40),16,0,16))
        if not h12[it]:
            arm1=False
        if not h13[it]:
            arm1=False
        h42.append(ROOT.TH1F("h42_%d" % it,"Tower:%dmm_Pede.RMS" % (25 if it==0 else 32),16,0,16))
        h43.append(ROOT.TH1F("h43_%d" % it,"Tower:%dmm_PedeDelay.RMS" % (25 if it==0 else 32),16,0,16))
        if not h42[it]:
            arm2=False
        if not h43[it]:
            arm2=False
        leg4.append(ROOT.TLegend(0.7,0.7,0.9,0.9,""))
        leg9.append(ROOT.TLegend(0.7,0.7,0.9,0.9,""))

    for it in range(2):
        for il in range(16):
            if arm1:
                h12[it].Fill(il,(h10[it][il].GetRMS()))
                h13[it].Fill(il,(h11[it][il].GetRMS()))
            if arm2:
                h42[it].Fill(il,(h40[it][il].GetRMS()))
                h43[it].Fill(il,(h41[it][il].GetRMS()))
    for it in range(2):
        if arm1:
            h12[it].SetStats(0)
            h13[it].SetStats(0)
            h12[it].GetXaxis().SetTitle("Layer")
            h13[it].GetXaxis().SetTitle("Layer")
            h12[it].GetYaxis().SetTitle("Pedestal RMS")
            h13[it].GetYaxis().SetTitle("Pedestal RMS")
            h12[it].GetYaxis().SetRangeUser(0,120)
            h13[it].SetLineColor(ROOT.kRed)
            h12[it].SetTitleSize(0.05,"x")
            h12[it].SetTitleSize(0.05,"y")
            h12[it].SetLabelSize(0.05,"x")
            h12[it].SetLabelSize(0.05,"y")
            h12[it].GetYaxis().SetTitleOffset(0.8)
            leg4[it].AddEntry(h12[it],"Pede","lp")
            leg4[it].AddEntry(h13[it],"Pede_Delay","lp")
            leg4[it].SetFillStyle(0)
            maxbin_Arm1.append(h12[it].GetMaximumBin())

        if arm2:
            h42[it].SetStats(0)
            h43[it].SetStats(0)
            h42[it].GetXaxis().SetTitle("Layer")
            h43[it].GetXaxis().SetTitle("Layer")
            h42[it].GetYaxis().SetTitle("Pedestal RMS")
            h43[it].GetYaxis().SetTitle("Pedestal RMS")
            h42[it].GetYaxis().SetRangeUser(0,120)
            h43[it].SetLineColor(ROOT.kRed)
            h42[it].SetTitleSize(0.05,"x")
            h42[it].SetTitleSize(0.05,"y")
            h42[it].SetLabelSize(0.05,"x")
            h42[it].SetLabelSize(0.05,"y")
            h42[it].GetYaxis().SetTitleOffset(0.8)
            leg9[it].AddEntry(h42[it],"Pede","lp")
            leg9[it].AddEntry(h43[it],"Pede_Delay","lp")
            leg9[it].SetFillStyle(0)
            maxbin_Arm2.append(h42[it].GetMaximumBin())
        
    c5 = ROOT.TCanvas("c5","CalPedestalRMS",400,800)
    c5.Divide(1,4,1e-10,1e-10)
   
    if arm1:
        for it in range(2):
            c5.cd(it+1)
            h12[it].GetYaxis().SetRangeUser(0,(h12[it].GetBinContent(maxbin_Arm1[it]))*1.1)
            h12[it].Draw("HIST")
            h13[it].Draw("HISTsame")
            leg4[it].Draw("same")

    if arm2:
        for it in range(2):
            c5.cd(it+3)
            h42[it].GetYaxis().SetRangeUser(0,(h42[it].GetBinContent(maxbin_Arm2[it]))*1.1)
            h42[it].Draw("HIST")
            h43[it].Draw("HISTsame")
            leg9[it].Draw("same")
    
def pospedav(add,nsub):
    global c6,h14,h15,h16,h17,leg5,pad2
    #h14 = [[[0]*20 for i in range(2)] for j in range(4)]#TS
    #h15 = [[[0]*40 for i in range(2)] for j in range(4)]#TL
    #h14 = [[[0]*2 for i in range(4)] for j in range(2)] #[tower][layer][view]
    h16 = [[0]*2 for i in range(4)]
    h17 = [[0]*2 for i in range(4)]
    leg5 = []
    pad2 = []
    maxvalue_TS = []
    maxvalue_TL = []
    minvalue_TS = []
    minvalue_TL = []
    maxbin_TS = [[0]*2 for i in range(4)]
    minbin_TS = [[0]*2 for i in range(4)]
    maxbin_TL = [[0]*2 for i in range(4)]
    minbin_TL = [[0]*2 for i in range(4)]

    arm1=True;

    for il in range(4):
        for iv in range(2):
            h16[il][iv] = file[1].Get("a1_pos_ped_mean_0_%d_%d_0" % (il,iv))
            h17[il][iv] = file[1].Get("a1_pos_ped_mean_1_%d_%d_0" % (il,iv))
            if not h16[il][iv]:
                arm1=False
                arm2=False

    #for il in range(4):
    #    for iv in range(2):
    #        for ic in range(20):
    #            h14[il][iv][ic] = file[1].Get("a1_posped_0_%d_%d_%d_0" % (il,iv,ic))
    #        for ic in range(40):
    #            h15[il][iv][ic] = file[1].Get("a1_posped_1_%d_%d_%d_0" % (il,iv,ic))
            
    #for il in range(4):
    #    for iv in range(2):
    #        h16[il][iv] = ROOT.TH1F("h16_%d_%d" % (il,iv),"Tower:0_Layer:%d_GSOPede.Av" % il,20,0,20)
    #        h17[il][iv] = ROOT.TH1F("h17_%d_%d" % (il,iv),"Tower:1_Layer:%d_GSOPede.Av" % il,40,0,40)
        
    #for il in range(4):
    #    for iv in range(2):
    #        for ic in range(20):
    #            h16[il][iv].Fill(ic,(h14[il][iv][ic].GetMean()))
    #        for ic in range(40):
    #            h17[il][iv].Fill(ic,(h15[il][iv][ic].GetMean()))

    c6 = ROOT.TCanvas("c6","GSObarPedestalAv",1000,800)

    if arm1:
        Nsub=float(nsub)
        if add=="1":
            for il in range(4):
                for iv in range(2):
                    h16[il][iv].Scale(1./Nsub)
                    h17[il][iv].Scale(1./Nsub)
                
    
        for il in range(4):
            h16[il][1].SetLineColor(ROOT.kRed)
            h17[il][1].SetLineColor(ROOT.kRed)
            h16[il][0].SetLineColor(ROOT.kBlue)
            leg5.append(ROOT.TLegend(0.8,0.7,0.9,0.9,""))
            leg5[il].AddEntry(h16[il][0],"X","lp")
            leg5[il].AddEntry(h16[il][1],"Y","lp")
            leg5[il].SetFillStyle(0)
            ROOT.gStyle.SetStatH(0.3)
            ROOT.gStyle.SetStatW(0.15)
            ROOT.gStyle.SetTitleSize(0.2)
        
        for il in range(4):
            for iv in range(2):
                #h16[il][iv].GetYaxis().SetRangeUser(31000,34000)
                #h17[il][iv].GetYaxis().SetRangeUser(32000,33500)
                h16[il][iv].GetXaxis().SetTitle("channel")
                h16[il][iv].GetYaxis().SetTitle("ADC counts")
                h17[il][iv].GetXaxis().SetTitle("channel")
                h17[il][iv].GetYaxis().SetTitle("ADC counts")
                h16[il][iv].SetTitleSize(0.06,"x")
                h16[il][iv].SetTitleSize(0.06,"y")
                h17[il][iv].SetTitleSize(0.06,"x")
                h17[il][iv].SetTitleSize(0.06,"y")
                h16[il][iv].SetLabelSize(0.06,"x")
                h16[il][iv].SetLabelSize(0.06,"y")
                h17[il][iv].SetLabelSize(0.06,"x")
                h17[il][iv].SetLabelSize(0.06,"y")
                h16[il][iv].GetYaxis().SetTitleOffset(0.6)
                h17[il][iv].GetYaxis().SetTitleOffset(0.6)
                h16[il][iv].GetXaxis().SetTitleOffset(0.4)
                h17[il][iv].GetXaxis().SetTitleOffset(0.4)
                h16[il][iv].SetStats(0)
                h17[il][iv].SetStats(0)
    
        for il in range(4):
            for iv in range(2):
                maxbin_TS[il][iv] = h16[il][iv].GetMaximumBin()
                minbin_TS[il][iv] = h16[il][iv].GetMinimumBin()
                maxbin_TL[il][iv] = h17[il][iv].GetMaximumBin()
                minbin_TL[il][iv] = h17[il][iv].GetMinimumBin()
    
        for il in range(4):
            if h16[il][0].GetBinContent(maxbin_TS[il][0])>h16[il][1].GetBinContent(maxbin_TS[il][1]):
                maxvalue_TS.append(h16[il][0].GetBinContent(maxbin_TS[il][0]))
            else:
                maxvalue_TS.append(h16[il][1].GetBinContent(maxbin_TS[il][1]))
    
            if h16[il][0].GetBinContent(minbin_TS[il][0])<h16[il][1].GetBinContent(minbin_TS[il][1]):
                minvalue_TS.append(h16[il][0].GetBinContent(minbin_TS[il][0]))
            else:
                minvalue_TS.append(h16[il][1].GetBinContent(minbin_TS[il][1]))
    
            if h17[il][0].GetBinContent(maxbin_TL[il][0])>h17[il][1].GetBinContent(maxbin_TL[il][1]):
                maxvalue_TL.append(h17[il][0].GetBinContent(maxbin_TL[il][0]))
            else:
                maxvalue_TL.append(h17[il][1].GetBinContent(maxbin_TL[il][1]))
    
            if h17[il][0].GetBinContent(minbin_TL[il][0])<h17[il][1].GetBinContent(minbin_TL[il][1]):
                minvalue_TL.append(h17[il][0].GetBinContent(minbin_TL[il][0]))
            else:
                minvalue_TL.append(h17[il][1].GetBinContent(minbin_TL[il][1]))
            
    
        #c6.Divide(2,4,1e-10,1e-10)
    
        x1 = 0.0
        x2 = 0.0
        y1 = 0.75
        y2 = 1.0
        for j in range(8):
            if j%2 == 0:
                x1 = 0.0
                x2 = 0.4
            else:
                x1 = 0.4
                x2 = 1.0
            if (j%2 == 0 and j!=0):
                y1-=0.25
                y2-=0.25
            
            pad2.append(ROOT.TPad("pad2_%d" % j, "pad2_%d" % j, x1, y1, x2, y2))
    
        for j in range(8):
            c6.cd(j)
            pad2[j].SetBottomMargin(0.1)  # joins upper and lower plot
            pad2[j].Draw()
    
    
        i = 0
        for il in range(4):
            pad2[i].cd()
            h16[il][1].GetYaxis().SetRangeUser(minvalue_TS[il]*0.9,maxvalue_TS[il]*1.1)
            h16[il][1].Draw("HIST")
            h16[il][0].Draw("samehist")
            leg5[il].Draw("same")
            i+=1
            pad2[i].cd()
            h17[il][1].GetYaxis().SetRangeUser(minvalue_TL[il]*0.9,maxvalue_TL[il]*1.1)
            h17[il][1].Draw("HIST")
            h17[il][0].Draw("samehist")
            leg5[il].Draw("same")
            i+=1

def pospedrms(add,nsub):
    global c7,h18,h19,h20,h21,leg6,pad3
    #h18 = [[[0]*20 for i in range(2)] for j in range(4)]#TS
    #h19 = [[[0]*40 for i in range(2)] for j in range(4)]#TL
    h18 = [[[0]*2 for i in range(4)] for j in range(2)] #[tower][layer][view]
    h20 = [[0]*2 for i in range(4)]
    h21 = [[0]*2 for i in range(4)]
    leg6 = []
    pad3 = []
    maxvalue_TS = []
    maxvalue_TL = []
    maxbin_TS = [[0]*2 for i in range(4)]
    maxbin_TL = [[0]*2 for i in range(4)]

    arm1=True

    for it in range(2):
        for il in range(4):
            for iv in range(2):
                h18[it][il][iv] = file[1].Get("a1_pos_ped_rms_%d_%d_%d_0" % (it,il,iv))
                if not h18[it][il][iv]:
                    arm1=False

    c7 = ROOT.TCanvas("c7","GSObarPedestalRMS",1000,800)

    if arm1:
        for il in range(4):
            for iv in range(2):
                h20[il][iv] = h18[0][il][iv]
                h21[il][iv] = h18[1][il][iv]
    
        Nsub=float(nsub)
        if add=="1":
            for il in range(4):
                for iv in range(2):
                    h20[il][iv].Scale(1./Nsub)
                    h21[il][iv].Scale(1./Nsub)
    
    
        #for il in range(4):
        #    for iv in range(2):
        #        for ic in range(20):
        #            h18[il][iv][ic] = file[1].Get("a1_posped_0_%d_%d_%d_0" % (il,iv,ic))
        #        for ic in range(40):
        #            h19[il][iv][ic] = file[1].Get("a1_posped_1_%d_%d_%d_0" % (il,iv,ic))
                
        #for il in range(4):
        #    for iv in range(2):
        #        h20[il][iv] = ROOT.TH1F("h20_%d_%d" % (il,iv),"Tower:0_Layer:%d_GSOPede.RMS" % il,20,0,20)
        #        h21[il][iv] = ROOT.TH1F("h21_%d_%d" % (il,iv),"Tower:1_Layer:%d_GSOPede.RMS" % il,40,0,40)
            
        #for il in range(4):
        #    for iv in range(2):
        #        for ic in range(20):
        #            h20[il][iv].Fill(ic,(h18[il][iv][ic].GetRMS()))
        #        for ic in range(40):
        #            h21[il][iv].Fill(ic,(h19[il][iv][ic].GetRMS()))
    
        for il in range(4):
            h20[il][1].SetLineColor(ROOT.kRed)
            h21[il][1].SetLineColor(ROOT.kRed)
            leg6.append(ROOT.TLegend(0.8,0.7,0.9,0.9,""))
            leg6[il].AddEntry(h20[il][0],"X","lp")
            leg6[il].AddEntry(h20[il][1],"Y","lp")
            leg6[il].SetFillStyle(0)
            ROOT.gStyle.SetStatH(0.3)
            ROOT.gStyle.SetStatW(0.15)
            ROOT.gStyle.SetTitleSize(0.2)
        
        for il in range(4):
            for iv in range(2):
                h20[il][iv].GetXaxis().SetTitle("channel")
                h20[il][iv].GetYaxis().SetTitle("ADC counts")
                h21[il][iv].GetXaxis().SetTitle("channel")
                h21[il][iv].GetYaxis().SetTitle("ADC counts")
                h20[il][iv].SetTitleSize(0.06,"x")
                h20[il][iv].SetTitleSize(0.06,"y")
                h21[il][iv].SetTitleSize(0.06,"x")
                h21[il][iv].SetTitleSize(0.06,"y")
                h20[il][iv].SetLabelSize(0.06,"x")
                h20[il][iv].SetLabelSize(0.06,"y")
                h21[il][iv].SetLabelSize(0.06,"x")
                h21[il][iv].SetLabelSize(0.06,"y")
                h20[il][iv].GetYaxis().SetTitleOffset(0.6)
                h21[il][iv].GetYaxis().SetTitleOffset(0.6)
                h20[il][iv].GetXaxis().SetTitleOffset(0.4)
                h21[il][iv].GetXaxis().SetTitleOffset(0.4)
                h20[il][iv].SetStats(0)
                h21[il][iv].SetStats(0)
    
        for il in range(4):
            for iv in range(2):
                maxbin_TS[il][iv] = h20[il][iv].GetMaximumBin()
                maxbin_TL[il][iv] = h21[il][iv].GetMaximumBin()
    
        for il in range(4):
            if h20[il][0].GetBinContent(maxbin_TS[il][0]) > h20[il][1].GetBinContent(maxbin_TS[il][1]):
                maxvalue_TS.append(h20[il][0].GetBinContent(maxbin_TS[il][0]))
            else:
                maxvalue_TS.append(h20[il][1].GetBinContent(maxbin_TS[il][1]))
    
            if h21[il][0].GetBinContent(maxbin_TL[il][0]) > h21[il][1].GetBinContent(maxbin_TL[il][1]):
                maxvalue_TL.append(h21[il][0].GetBinContent(maxbin_TL[il][0]))
            else:
                maxvalue_TL.append(h21[il][1].GetBinContent(maxbin_TL[il][1]))
              
        #c7.Divide(2,4,1e-10,1e-10)
    
        x1 = 0.0
        x2 = 0.0
        y1 = 0.75
        y2 = 1.0
        for j in range(8):
            if j%2 == 0:
                x1 = 0.0
                x2 = 0.4
            else:
                x1 = 0.4
                x2 = 1.0
            if (j%2 == 0 and j!=0):
                y1-=0.25
                y2-=0.25
            
            pad3.append(ROOT.TPad("pad3_%d" % j, "pad3_%d" % j, x1, y1, x2, y2))
    
        for j in range(8):
            c7.cd(j)
            pad3[j].SetBottomMargin(0.1)  # joins upper and lower plot
            pad3[j].Draw()
    
    
        i = 0
        for il in range(4):
            pad3[i].cd()
            h20[il][1].GetYaxis().SetRangeUser(0,maxvalue_TS[il]*1.1)
            h20[il][1].Draw("HIST")
            h20[il][0].Draw("sameHIST")
            leg6[il].Draw("same")
            i+=1
            pad3[i].cd()
            h21[il][1].GetYaxis().SetRangeUser(0,maxvalue_TL[il]*1.1)
            h21[il][1].Draw("HIST")
            h21[il][0].Draw("sameHIST")
            leg6[il].Draw("same")
            i+=1


def drawa2calhist(tower,ir):
    global c8,h22,h23,leg7,h44,h45,h46,h46_rebin,h62,leg18
    h22 = []
    h23 = []
    h44 = []
    h45 = []
    h46 = [0]*16
    h46_rebin = []
    leg7=[]
    h62 = [[[[0]*2 for i in range(16)]for j in range(2)]for k in range(5)]
    leg18 = ROOT.TLegend(0.6,0.3,0.9,0.65,"") 

    arm2=True

    #for it in range(2):
    for il in range(16):
        h22.append(file[0].Get("A2Cal0_%d_%d_%d" % (tower,il,ir)))
        h23.append(file[0].Get("A2CalPed_%d_%d_%d" % (tower,il,ir)))
        h44.append(file[0].Get("a2_calped_%d_%d_%d" % (tower,il,ir)))
        h45.append(file[0].Get("a2_caloffped_%d_%d_%d" % (tower,il,ir)))
        if not h22[il]:
            arm2=False
        if not h23[il]:
            arm2=False
        if not h44[il]:
            arm2=False
        if not h45[il]:
            arm2=False
        ROOT.gStyle.SetOptLogy()
        leg7.append(ROOT.TLegend(0.4,0.7,0.65,0.9,""))
        ROOT.gStyle.SetStatH(0.2);
        ROOT.gStyle.SetStatW(0.3);
        for itrg in range(5):
            h62[itrg][tower][il][ir] = file[0].Get("Arm2_cal_lvl0_eachtrg_%d_%d_%d_%d" % (itrg,tower,il,ir))
            if not h62[itrg][tower][il][ir]:
                arm2=False
            
    #for il in range(16):
        #h46[il] = h44[il]-h45[il]
        #h46_rebin.append(h46[il].Rebin(10))

    c8 = ROOT.TCanvas("c8","Hist%dmmCal" % (25 if tower==0 else 32),1000,800)

    if arm2:
        #for it in range(2):
        for il in range(16):
            h22[il].GetXaxis().SetTitle("ADCcount")
            h22[il].GetYaxis().SetTitle("$\Delta$N")
            h22[il].SetTitleSize(0.05,"x")
            h22[il].SetTitleSize(0.05,"y")
            h22[il].SetLabelSize(0.05,"x")
            h22[il].SetLabelSize(0.05,"y")
            h22[il].GetYaxis().CenterTitle()
            h22[il].GetYaxis().SetTitleOffset(0.6)
            h22[il].SetLineColor(ROOT.kRed)
            leg7[il].AddEntry(h22[il],"beam","lp")
            leg7[il].AddEntry(h44[il],"ped","lp")
            #leg7[il].AddEntry(h45[il],"pedoff","lp");
            leg7[il].SetFillStyle(0)
            h44[il].SetLineColor(ROOT.kBlue)
            h45[il].SetLineColor(ROOT.kGreen+2)
            h62[0][tower][il][ir].SetLineColor(ROOT.kBlack)
            h62[1][tower][il][ir].SetLineColor(ROOT.kYellow+2)
            h62[2][tower][il][ir].SetLineColor(ROOT.kGreen+2)
            h62[3][tower][il][ir].SetLineColor(ROOT.kViolet+2)
            h62[4][tower][il][ir].SetLineColor(ROOT.kMagenta)
    
        leg18.AddEntry(h62[0][tower][0][ir],"Shower","lp")
        leg18.AddEntry(h62[1][tower][0][ir],"#pi^{0}","lp")
        leg18.AddEntry(h62[2][tower][0][ir],"HighEM","lp")
        leg18.AddEntry(h62[3][tower][0][ir],"Hadron","lp")
        leg18.AddEntry(h62[4][tower][0][ir],"ZDC","lp")
    
    
            
        c8.Divide(4,4,1e-10,1e-10)
    
        for il in range(16):
            c8.cd(il+1)
            h22[il].Draw("")
            h44[il].Draw("same")
            #h45[il].Draw("same")
            for itrg in range(5):
                h62[itrg][tower][il][ir].Draw("same")
            #h23[il].Draw("same")
            #h46_rebin[il].Draw("same")
            leg7[il].Draw("same")
            if il==0:
                leg18.Draw("same")
        
        

def drawa2poshist():
    global c9,h24,h25,h26,leg8
    h24 = [[0] * 2 for i in range(4)]
    h25 = [[0] * 2 for i in range(4)]
    h26 = [[0] * 2 for i in range(4)]
    leg8 = [[0] * 2 for i in range(4)]
    
    maxbin = [[[0] * 2 for i in range(3)]for j in range(4)] #[layer][sample][XY]
    minbin = [[[0] * 2 for i in range(3)]for j in range(4)]
    maxvalue = [[0] * 2 for i in range(4)] #[layer][XY]
    minvalue = [[0] * 2 for i in range(4)]

    arm2=True

    for il in range(4):
        for iv in range(2):
            h24[il][iv] = file[0].Get("A2Pos_0_%d_%d_%d" % (il,iv,0))#sample1
            h25[il][iv] = file[0].Get("A2Pos_0_%d_%d_%d" % (il,iv,1))#sample2
            h26[il][iv] = file[0].Get("A2Pos_0_%d_%d_%d" % (il,iv,2))#sample3
            if not h24[il][iv]:
                arm2=False
            if not h25[il][iv]:
                arm2=False
            if not h26[il][iv]:
                arm2=False
            leg8[il][iv] = ROOT.TLegend(0.8,0.7,0.9,0.9,"")
            ROOT.gStyle.SetStatH(0.2);
            ROOT.gStyle.SetStatW(0.3);
            ROOT.gStyle.SetOptLogy(0)
            
    c9 = ROOT.TCanvas("c9","SiliconAccumlate",1000,800)

    if arm2:
        for il in range(4):
            for iv in range(2):
                    h24[il][iv].SetTitle("Tower:0_Layer:%d_XY:%d" % (il,iv))
                    h24[il][iv].GetXaxis().SetTitle("channel")
                    h24[il][iv].GetYaxis().SetTitle("ADCcount")
                    h24[il][iv].SetLineColor(ROOT.kRed)
                    h25[il][iv].SetLineColor(ROOT.kBlack)
                    h26[il][iv].SetLineColor(ROOT.kGreen+2)
                    h24[il][iv].SetTitleSize(0.05,"x")
                    h24[il][iv].SetTitleSize(0.05,"y")
                    h24[il][iv].SetLabelSize(0.05,"x")
                    h24[il][iv].SetLabelSize(0.05,"y")
                    h24[il][iv].GetYaxis().SetTitleOffset(0.8)
                    #h24[il][iv].GetYaxis().SetRangeUser(0,1000000000)
                    leg8[il][iv].AddEntry(h24[il][iv],"1st","lp")
                    leg8[il][iv].AddEntry(h25[il][iv],"2nd","lp")
                    leg8[il][iv].AddEntry(h26[il][iv],"3rd","lp")
                    leg8[il][iv].SetFillStyle(0)
                    h26[il][iv].SetStats(0)
        
        for il in range(4):
            for iv in range(2):
                maxbin[il][0][iv] = h24[il][iv].GetMaximumBin()
                maxbin[il][1][iv] = h25[il][iv].GetMaximumBin()
                maxbin[il][2][iv] = h26[il][iv].GetMaximumBin()
                minbin[il][0][iv] = h24[il][iv].GetMinimumBin()
                minbin[il][1][iv] = h25[il][iv].GetMinimumBin()
                minbin[il][2][iv] = h26[il][iv].GetMinimumBin()
                
        for il in range(4):
            for iv in range(2):
                maxvalue[il][iv] = max(h24[il][iv].GetBinContent(maxbin[il][0][iv]),h25[il][iv].GetBinContent(maxbin[il][1][iv]),h26[il][iv].GetBinContent(maxbin[il][2][iv]));
                minvalue[il][iv] = min(h24[il][iv].GetBinContent(minbin[il][0][iv]),h25[il][iv].GetBinContent(minbin[il][1][iv]),h26[il][iv].GetBinContent(minbin[il][2][iv]));
    
        for il in range(4):
            for iv in range(2):
                h26[il][iv].GetYaxis().SetRangeUser(minvalue[il][iv],maxvalue[il][iv]);
                h26[il][iv].SetTitle("Tower:0 Layer:%d View:%s" % (il,"X" if iv==0 else "Y"));
                                   
    
        c9.Divide(2,4,1e-10,1e-10)
        i = 0
        for il in range(4):
            for iv in range(2):
                c9.cd(i+1)
                h25[il][iv].Draw("HIST")
                h26[il][iv].Draw("HISTsame")
                h24[il][iv].Draw("HISTsame")
                leg8[il][iv].Draw("same")
                i += 1

def a2pospedav(add,nsub):
    global c10,h27,h28
    h27 = [[[0]*384 for i in range(2)] for j in range(4)]
    h28 = [[0]*2 for i in range(4)]

    arm2=True

    #for il in range(4):
    #    for iv in range(2):
    #        for ic in range(384):
    #            h27[il][iv][ic] = file[1].Get("a2_posped_0_%d_%d_%d_1" % (il,iv,ic))
            
    #for il in range(4):
    #    for iv in range(2):
    #        h28[il][iv] = ROOT.TH1F("h28_%d_%d" % (il,iv),"Tower:0_Layer:%d_XY:%d_SiliconPede.Av" % (il,iv),384,0,384)
        
    for il in range(4):
        for iv in range(2):
            #for ic in range(384):
                #h28[il][iv].Fill(ic,(h27[il][iv][ic].GetMean()))
            #h28[il][iv] = file[1].Get("a2_mean_posped_0_%d_%d_1" % (il,iv))
            h28[il][iv] = file[1].Get("a2_pos_ped_mean_0_%d_%d_0" % (il,iv))
            if not h28[il][iv]:
                arm2=False
    
    c10 = ROOT.TCanvas("c10","SiliconPedestalAv",1000,800)

    if arm2:
        Nsub=float(nsub)
        if add=="1":
            for il in range(4):
                for iv in range(2):
                    h28[il][iv].Scale(1./Nsub)
    
        ROOT.gStyle.SetOptLogy(0)
        ROOT.gStyle.SetStatH(0.3)
        ROOT.gStyle.SetStatW(0.15)
        ROOT.gStyle.SetTitleSize(0.2)
        for il in range(4):
            for iv in range(2):
                h28[il][iv].GetXaxis().SetTitle("channel")
                h28[il][iv].GetYaxis().SetTitle("ADC counts")
                h28[il][iv].SetTitleSize(0.06,"x")
                h28[il][iv].SetTitleSize(0.06,"y")
                h28[il][iv].SetLabelSize(0.06,"x")
                h28[il][iv].SetLabelSize(0.06,"y")
                h28[il][iv].GetYaxis().SetTitleOffset(0.8)
                h28[il][iv].GetXaxis().SetTitleOffset(0.6)
                h28[il][iv].SetStats(0)
                
        c10.Divide(2,4,1e-10,1e-10)
    
        i = 0
        for il in range(4):
            c10.cd(i+1)
            h28[il][0].Draw("HIST")
            i+=1
            c10.cd(i+1)
            h28[il][1].Draw("HIST")
            i+=1

def a2pospedrms(add,nsub):
    global c11,h29,h30
    h29 = [[[0]*384 for i in range(2)] for j in range(4)]
    h30 = [[0]*2 for i in range(4)]

    arm2=True

    #for il in range(4):
    #    for iv in range(2):
    #        for ic in range(384):
    #            h29[il][iv][ic] = file[1].Get("a2_posped_0_%d_%d_%d_1" % (il,iv,ic))
    '''        
    for il in range(4):
        for iv in range(2):
            h30[il][iv] = ROOT.TH1F("h30_%d_%d" % (il,iv),"Tower:0_Layer:%d_XY:%d_SiliconPede.RMS" % (il,iv),384,0,384)
    '''    
    for il in range(4):
        for iv in range(2):
            #for ic in range(384):
                #h30[il][iv].Fill(ic,(h29[il][iv][ic].GetRMS()))
            #h30[il][iv] = file[1].Get("a2_rms_posped_0_%d_%d_1" % (il,iv))
            h30[il][iv] = file[1].Get("a2_pos_ped_rms_0_%d_%d_0" % (il,iv))
            if not h30[il][iv]:
                arm2=False

    c11 = ROOT.TCanvas("c11","SiliconPedestalRMS",1000,800)

    if arm2:
        Nsub=float(nsub)
        if add=="1":
            for il in range(4):
                for iv in range(2):
                    h30[il][iv].Scale(1./Nsub)
    
        ROOT.gStyle.SetOptLogy(0)
        ROOT.gStyle.SetStatH(0.3)
        ROOT.gStyle.SetStatW(0.15)
        ROOT.gStyle.SetTitleSize(0.2)
        
        for il in range(4):
            for iv in range(2):
                #h30[il][iv].GetYaxis().SetRangeUser(0,5)
                h30[il][iv].GetXaxis().SetTitle("channel")
                h30[il][iv].GetYaxis().SetTitle("ADC counts")
                h30[il][iv].SetTitleSize(0.06,"x")
                h30[il][iv].SetTitleSize(0.06,"y")
                h30[il][iv].SetLabelSize(0.06,"x")
                h30[il][iv].SetLabelSize(0.06,"y")
                h30[il][iv].GetYaxis().SetTitleOffset(0.8)
                h30[il][iv].GetXaxis().SetTitleOffset(0.7)
                h30[il][iv].SetStats(0)
    
        c11.Divide(2,4,1e-10,1e-10)
    
        i = 0
        for il in range(4):
            c11.cd(i+1)
            h30[il][0].Draw("HIST")
            i+=1
            c11.cd(i+1)
            h30[il][1].Draw("HIST")
            i+=1

def drawa2flaghist():
    global c12,h31,h48
    h31 = []
    h48 = []
    maxbin_beam = []
    maxbin_ped = []
    maxvalue = []
   
    arm2=True

    for i in range(3):
        h31.append(file[0].Get("A2Flag_ev_%d" % i))
        h48.append(file[0].Get("A2Flag_ped_%d" % i))
        if not h31[i]:
            arm2=False
        if not h48[i]:
            arm2=False

    c12 = ROOT.TCanvas("c12","HistArm2Flag",400,800)

    if arm2:
        for i in range(3):
            h31[i].GetXaxis().SetTitle("Bit")
            h31[i].GetYaxis().SetTitle("$\Delta$N")
            h31[i].SetTitleSize(0.05,"x")
            h31[i].SetTitleSize(0.05,"y")
            h31[i].SetLabelSize(0.05,"x")
            h31[i].SetLabelSize(0.05,"y")
            h31[i].GetYaxis().SetTitleOffset(0.8)
            h31[i].SetStats(0)
            h48[i].SetLineColor(ROOT.kBlue)
            h31[i].SetLineColor(ROOT.kRed)
            maxbin_beam.append(h31[i].GetMaximumBin())
            maxbin_ped.append(h48[i].GetMaximumBin())
    
        for i in range(3):
            maxvalue.append(max(h31[i].GetBinContent(maxbin_beam[i]),h48[i].GetBinContent(maxbin_ped[i])))
    
        ROOT.gStyle.SetOptLogy(1)
    
        c12.Divide(1,3,1e-10,1e-10)
    
        for i in range(3):
            c12.cd(i+1)
            h31[i].GetYaxis().SetRangeUser(1,maxvalue[i]*1.1)
            h31[i].Draw("")
            h48[i].Draw("same")

def drawTDChist(arm):#arm=0->Arm1, arm=1->Arm2
    global c13,h32,leg10
    h32 = []#0->BPTX1, 1->BPTX2, 2->Shower, 3->TS_OR, 4->TL_OR, 5->A1FC, 6->A2FC
    leg10 = []
    maxbin = []
    for i in range(3):
        leg10.append(ROOT.TLegend(0.7,0.5,0.9,0.7,""))

    c13=ROOT.TCanvas("c13","Hist Arm%d TDC" % (arm+1),400,800)

    for i in range(2):
        h32.append(file[0].Get("A%d_TDC_BPTX%d" % ((arm+1),(i+1))))
        if not h32[i]:
            return
    h32.append(file[0].Get("A%d_TDC_Shower_Trigger" % (arm+1)))
    for i in range(2):
        if(arm==0):
            h32.append(file[0].Get("TDC_%dmm_OR" % (20 if i==0 else 40)))
        elif(arm==1):
            h32.append(file[0].Get("TDC_%dmm_OR" % (25 if i==0 else 32)))
        if not h32[i+2]:
            return
    for i in range(2):
        h32.append(file[0].Get("A%d_TDC_Arm%d_FC" % ((arm+1),(i+1))))
        if not h32[i+4]:
            return

    h32[0].SetLineColor(ROOT.kBlue)
    h32[1].SetLineColor(ROOT.kRed)
    h32[2].SetLineColor(ROOT.kBlack)
    h32[3].SetLineColor(ROOT.kBlue)
    h32[4].SetLineColor(ROOT.kRed)
    h32[5].SetLineColor(ROOT.kBlue)
    h32[6].SetLineColor(ROOT.kRed)
    ROOT.gStyle.SetOptLogy(0)

    for i in range(7):
        maxbin.append(h32[i].GetMaximumBin())

    maximum = max(h32[0].GetBinContent(maxbin[0]),h32[1].GetBinContent(maxbin[1]),h32[2].GetBinContent(maxbin[2]),h32[3].GetBinContent(maxbin[3]),h32[4].GetBinContent(maxbin[4]),h32[5].GetBinContent(maxbin[5]),h32[6].GetBinContent(maxbin[6]))
    for i in range(7):
        h32[i].GetYaxis().SetRangeUser(0,maximum)

    for i in range(2):
        leg10[0].AddEntry(h32[i],"TDC BPTX%d" % (i+1),"lp")
        leg10[2].AddEntry(h32[i+5],"TDC Arm%d FC" % (i+1),"lp")
    leg10[1].AddEntry(h32[2],"TDC Shower Trigger","lp")
    for i in range(2):
        if(arm==0):
            leg10[1].AddEntry(h32[i+3],"TDC %dmm OR" % (20 if i==0 else 40),"lp")
        elif(arm==1):
            leg10[1].AddEntry(h32[i+3],"TDC %dmm OR" % (25 if i==0 else 32),"lp")

    for i in range(3):
        leg10[i].SetFillStyle(0)

    for i in range(6):
        h32[i].GetXaxis().SetTitle("time[ns]")
        h32[i].SetStats(0)

    c13.Divide(1,3,1e-10,1e-10)


    c13.cd(1)
    h32[0].Draw("")
    h32[1].Draw("same")
    leg10[0].Draw("same")
    c13.cd(2)
    h32[2].Draw("")
    h32[3].Draw("same")
    h32[4].Draw("same")
    leg10[1].Draw("same")
    c13.cd(3)
    h32[5].Draw("")
    h32[6].Draw("same")
    leg10[2].Draw("same")

def drawDSChist(arm,tower):
    global c14,h33

    hadc = []
    h33 = []

    c14=ROOT.TCanvas("c14","DSC Arm%d %s" % ((arm+1),("Small" if tower==0 else "Large")),1000,800)

    for il in range(16):
        hadc.append(file[0].Get("A%dCal_%d_%d_0" % ((arm+1),tower,il)))
        h33.append(file[0].Get("DSC_Arm%d-%s_layer%d_Range0" % ((arm+1),("Small" if tower==0 else "Large"),il)))
        if not hadc[il]:
            return
        if not h33[il]:
            return

    for i in range(16):
        h33[i].Divide(hadc[i])
    ROOT.gStyle.SetOptLogy(0)

    for i in range(16):
        h33[i].GetXaxis().SetTitle("WRADC")
        h33[i].SetStats(0)

    c14.Divide(4,4,1e-10,1e-10)

    for i in range(16):
        c14.cd(i+1)
        h33[i].Draw("")

def drawDSCFChist():
    global c15,h34,h35,h36,h37,h38,h39,leg11,leg12

    h34 = []
    h35 = []
    h36 = []
    h37 = []
    h38 = []
    h39 = []
    leg11 = []
    leg12 = []

    arm1=True
    arm2=True

    for i in range(4):
        leg11.append(ROOT.TLegend(0.6,0.6,0.9,0.9,""))
        leg12.append(ROOT.TLegend(0.6,0.6,0.9,0.9,""))

    for i in range(4):
        h34.append(file[0].Get("FC_Arm1_lvl1_channel%d" % i))
        h35.append(file[0].Get("FC_Arm2_lvl1_channel%d" % i))
        if not h34[i]:
            arm1=False
        if not h35[i]:
            arm2=False
        #h36.append(file[0].Get("DSC_FCs_Arm1_channel%d" % i))# DSC FC's lvl0
        #h37.append(file[0].Get("DSC_FCs_Arm2_channel%d" % i))
        h36.append(file[0].Get("DSC_FCs_Arm1_lvl1_channel%d" % i))
        h37.append(file[0].Get("DSC_FCs_Arm2_lvl1_channel%d" % i))
        if not h36[i]:
            arm1=False
        if not h37[i]:
            arm2=False
        
    for i in range(4):
        if arm1:
            h38.append(h36[i].Clone("DSC_FCs_Arm1"))
            h38[i].Divide(h34[i])
        if arm2:
            h39.append(h37[i].Clone("DSC_FCs_Arm2"))
            h39[i].Divide(h35[i])
        
    for i in range(4):
        if arm1:
            leg11[i].AddEntry(h34[i],"FC","lp")
            leg11[i].AddEntry(h36[i],"FC discri:ON","lp")
            leg11[i].SetFillStyle(0)
        if arm2:
            leg12[i].AddEntry(h35[i],"FC","lp")
            leg12[i].AddEntry(h37[i],"FC discri:ON","lp")
            leg12[i].SetFillStyle(0)

    for i in range(4):
        if arm1:
            h34[i].SetLineColor(ROOT.kBlack)
            h36[i].SetLineColor(ROOT.kRed)
            h34[i].GetXaxis().SetTitle("ADCcounts")
            h38[i].GetXaxis().SetTitle("ADCcounts")
            h34[i].SetStats(0)
            h36[i].SetStats(0)
            h38[i].SetStats(0)
        if arm2:
            h35[i].SetLineColor(ROOT.kBlack)
            h37[i].SetLineColor(ROOT.kRed)
            h35[i].GetXaxis().SetTitle("ADCcounts")
            h39[i].GetXaxis().SetTitle("ADCcounts")
            h35[i].SetStats(0)
            h37[i].SetStats(0)
            h39[i].SetStats(0)

    ROOT.gStyle.SetOptLogy(0)
        
    c15=ROOT.TCanvas("c15","DSC FCs",1000,800)
    c15.Divide(4,4,1e-10,1e-10)
        
    for i in range(4):
        if arm1:
            c15.cd(i+1)
            h34[i].Draw("")
            h36[i].Draw("same")
            leg11[i].Draw("same")
            c15.cd(i+9)
            h38[i].Draw("")
        if arm2:
            c15.cd(i+5)
            h35[i].Draw("")
            h37[i].Draw("same")
            leg12[i].Draw("same")
            c15.cd(i+13)
            h39[i].Draw("")

def drawotherhist(arm):
    global c16,h40,h41,h42,h43,h44,leg13,h49,h50,h51,h52,h53

    #h40 = []
    h41 = [[0]*2 for i in range(4)]
    h42 = [[0]*2 for i in range(4)]
    h43 = []
    h44 = []
    h49 = [[0]*2 for i in range(4)]
    #h50 = [[0]*2 for i in range(4)]
    h51 = []
    #h52 = [0]*4
    #h53 = [0]*2

    leg13 = []

    c16 = ROOT.TCanvas("c16","c16",1000,800)

    for ich in range(4):
        for ir in range(2):
            if arm==0:
                h41[ich][ir] = file[0].Get("FC_Arm1_channel%d_%d" % (ich,ir))
                h42[ich][ir] = file[0].Get("FCped_Arm1_channel%d_%d" % (ich,ir))
                h49[ich][ir] = file[0].Get("FCoffped_Arm1_channel%d_%d" % (ich,ir))
            elif arm==1:
                h41[ich][ir] = file[0].Get("FC_Arm2_channel%d_%d" % (ich,ir))
                h42[ich][ir] = file[0].Get("FCped_Arm2_channel%d_%d" % (ich,ir))
                h49[ich][ir] = file[0].Get("FCoffped_Arm2_channel%d_%d" % (ich,ir))
            if not h41[ich][ir]:
                return
            if not h42[ich][ir]:
                return
            if not h49[ich][ir]:
                return
    for it in range(2):
        if arm==0:
            h43.append(file[0].Get("Laser_Arm1_%d" % it))
            h44.append(file[0].Get("Laserped_Arm1_%d" % it))
            h51.append(file[0].Get("Laseroffped_Arm1_%d" % it))
        elif arm==1:
            h43.append(file[0].Get("Laser_Arm2_%d" % it))
            h44.append(file[0].Get("Laserped_Arm2_%d" % it))
            h51.append(file[0].Get("Laseroffped_Arm2_%d" % it))
        if not h43[it]:
            return
        if not h44[it]:
            return
        if not h51[it]:
            return
    #for ich in range(4):
    #    h52[ich]=h42[ich]-h49[ich]
    #for it in range(2):
    #    h53[it]=h44[it]-h51[it]

    for i in range(4):
        for j in range(2):
            h41[i][j].GetXaxis().SetTitle("ADCcounts")
            h41[i][j].SetLineColor(ROOT.kRed)
            h42[i][j].SetLineColor(ROOT.kBlue)
            #h49[i].SetLineColor(ROOT.kGreen+2)
            #h52[i].SetLineColor(ROOT.kYellow+2)
            h41[i][j].SetStats(0)
    for i in range(2):
        h43[i].GetXaxis().SetTitle("ADCcounts")
        h43[i].SetLineColor(ROOT.kRed)
        h44[i].SetLineColor(ROOT.kBlue)
        #h51[i].SetLineColor(ROOT.kGreen+2)
        #h53[i].SetLineColor(ROOT.kYellow+2)

        h43[i].SetStats(0)

    ROOT.gStyle.SetOptLogy(1)
    c16.Divide(4,2,1e-10,1e-10)

    for i in range(4):
        c16.cd(i+1)
        h41[i][0].Draw("")
        #h52[i].Draw("same")
        h42[i][0].Draw("same")
        #h49[i].Draw("same")
        c16.cd(i+5)
        h41[i][1].Draw("")
        h42[i][1].Draw("same")
    #for i in range(2):
        #c16.cd(i+9)
        #h43[i].Draw("")
        #h44[i].Draw("same")
        #h51[i].Draw("same")
        #h53[i].Draw("same")

def drawzdchist():
    global c17,h45,h46,leg14
    
    h45 = [[0]*2 for i in range(3)] #[module][range]
    h46 = [[0]*2 for i in range(3)]
    leg14 = []
    c17 = ROOT.TCanvas("c17","c17",1000,800)
    c17.Divide(3,2,1e-10,1e-10)

    arm1=True
    arm2=True

    for im in range(3):
        leg14.append(ROOT.TLegend(0.7,0.7,0.9,0.9,""))
        for ir in range(2):
            h45[im][ir] = file[0].Get("ZDC_Arm1_%d_%d" % (im,ir))
            if not h45[im][ir]:
                arm1=False
            h46[im][ir] = file[0].Get("ZDC_Arm2_%d_%d" % (im,ir))
            if not h46[im][ir]:
                arm2=False

    for im in range(3):
        if arm1:
            h45[im][0].SetLineColor(ROOT.kRed)
            h45[im][1].SetLineColor(ROOT.kBlue)
            h45[im][1].Scale(1./8.)
        if arm2:
            h46[im][0].SetLineColor(ROOT.kRed)
            h46[im][1].SetLineColor(ROOT.kBlue)
            h46[im][1].Scale(1./8.)
        if arm2:
            leg14[im].AddEntry(h46[im][0],"NRADC","lp")
            leg14[im].AddEntry(h46[im][1],"WRADC","lp")
        for ir in range(2):
            if arm1:
                h45[im][ir].GetXaxis().SetTitle("ADCcounts")
                h45[im][ir].SetStats(0)
            if arm2:
                h46[im][ir].GetXaxis().SetTitle("ADCcounts")
                h46[im][ir].SetStats(0)

    for im in range(3):
        if arm1:
            c17.cd(im+1)
            h45[im][0].Draw("")
            h45[im][1].Draw("samehist")
            leg14[im].Draw("same")
        if arm2:
            c17.cd(im+4)
            h46[im][0].Draw("")
            h46[im][1].Draw("samehist")
            leg14[im].Draw("same")


                   
def drawDSCFChist_lvl2():
    global c18,h54,h55,h56,h57,h58,h59,leg15,leg16

    h54 = []
    h55 = []
    h56 = []
    h57 = []
    h58 = []
    h59 = []
    leg15 = []
    leg16 = []

    arm1=True
    arm2=True

    for i in range(4):
        leg15.append(ROOT.TLegend(0.7,0.5,0.9,0.7,""))
        leg16.append(ROOT.TLegend(0.7,0.5,0.9,0.7,""))

    for i in range(4):
        h54.append(file[0].Get("FC_Arm1_lvl2_channel%d" % i))
        h55.append(file[0].Get("FC_Arm2_lvl2_channel%d" % i))
        h56.append(file[0].Get("DSC_FCs_Arm1_lvl2_channel%d" % i))
        h57.append(file[0].Get("DSC_FCs_Arm2_lvl2_channel%d" % i))
        if not h54:
            arm1=False
        if not h55:
            arm2=False
        if not h56:
            arm1=False
        if not h57:
            arm2=False

    for i in range(4):
        if arm1:
            h58.append(h36[i].Clone("DSC_FCs_Arm1_lvl2"))
            h58[i].Divide(h54[i])
        if arm2:
            h59.append(h37[i].Clone("DSC_FCs_Arm2_lvl2"))
            h59[i].Divide(h55[i])

    for i in range(4):
        if arm1:
            leg15[i].AddEntry(h54[i],"FC","lp")
            leg15[i].AddEntry(h56[i],"FC discri:ON","lp")
            leg15[i].SetFillStyle(0)
        if arm2:
            leg16[i].AddEntry(h55[i],"FC","lp")
            leg16[i].AddEntry(h57[i],"FC discri:ON","lp")
            leg16[i].SetFillStyle(0)

    for i in range(4):
        if arm1:
            h54[i].SetLineColor(ROOT.kBlack)
            h56[i].SetLineColor(ROOT.kRed)
            h54[i].GetXaxis().SetTitle("dE[GeV]")
            h58[i].GetXaxis().SetTitle("dE[GeV]")
            h54[i].SetStats(0)
            h56[i].SetStats(0)
            h58[i].SetStats(0)
        if arm2:
            h55[i].SetLineColor(ROOT.kBlack)
            h57[i].SetLineColor(ROOT.kRed)
            h55[i].GetXaxis().SetTitle("dE[GeV]")
            h59[i].GetXaxis().SetTitle("dE[GeV]")
            h55[i].SetStats(0)
            h57[i].SetStats(0)
            h59[i].SetStats(0)

    ROOT.gStyle.SetOptLogy(0)

    c18=ROOT.TCanvas("c18","DSC FCs lvl2",1000,800)
    c18.Divide(4,4,1e-10,1e-10)

    for i in range(4):
        if arm1:
            c18.cd(i+1)
            h54[i].Draw("")
            h56[i].Draw("same")
            leg15[i].Draw("same")
            c18.cd(i+9)
            h58[i].Draw("")
        if arm2:
            c18.cd(i+5)
            h55[i].Draw("")
            h57[i].Draw("same")
            leg16[i].Draw("same")
            c18.cd(i+13)
            h59[i].Draw("")

def drawDSChist_lvl2(arm,tower):
    global c19,h60

    hdE = []
    h60 = []

    for il in range(16):
        hdE.append(file[0].Get("A%dCal_lvl2_Tower%d_layer%d" % ((arm+1),tower,il)))
        h60.append(file[0].Get("DSC_lvl2_Arm%d-%s_layer%d" % ((arm+1),("Small" if tower==0 else "Large"),il)))

    for i in range(16):
        h60[i].Divide(hdE[i])
    ROOT.gStyle.SetOptLogy(0)

    for i in range(16):
        h60[i].GetXaxis().SetTitle("dE[GeV]")
        h60[i].SetStats(0)

    c19=ROOT.TCanvas("c19","DSC lvl2 Arm%d %s" % ((arm+1),("Small" if tower==0 else "Large")),1000,800)
    c19.Divide(4,4,1e-10,1e-10)

    for i in range(16):
        c19.cd(i+1)
        h60[i].Draw("")
