import ROOT
import drawhist_Adamo
import make_position_fit
import sys
ROOT.gROOT.SetBatch(True)
args = sys.argv

file,Run = drawhist_Adamo.file(args[1],args[2])


drawhist_Adamo.DrawHitMap()
drawhist_Adamo.c1.Print("%s/Arm1-Adamo-Hitmap.gif" % Run)
drawhist_Adamo.DrawHitMapCalRel()
drawhist_Adamo.c2.Print("%s/Arm1-Adamo-Hitmap-Cal-Rel.gif" % Run)

