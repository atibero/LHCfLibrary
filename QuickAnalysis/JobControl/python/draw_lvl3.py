import ROOT
import drawhist_lvl3
import sys

args = sys.argv

file,Run = drawhist_lvl3.file(args[1],args[2])


drawhist_lvl3.DrawPhotonEnergyST()
drawhist_lvl3.c1.Print("%s/photon_energy_st.gif" % Run)
drawhist_lvl3.DrawPhotonEnergyLT()
drawhist_lvl3.c2.Print("%s/photon_energy_lt.gif" % Run)
drawhist_lvl3.DrawPhotonHitMap()
drawhist_lvl3.c3.Print("%s/photon_hit_map.gif" % Run)

drawhist_lvl3.DrawNeutronEnergyST()
drawhist_lvl3.c4.Print("%s/neutron_energy_st.gif" % Run)
drawhist_lvl3.DrawNeutronEnergyLT()
drawhist_lvl3.c5.Print("%s/neutron_energy_lt.gif" % Run)
drawhist_lvl3.DrawNeutronHitMap()
drawhist_lvl3.c6.Print("%s/neutron_hit_map.gif" % Run)

drawhist_lvl3.DrawDiPhotonMgg()
drawhist_lvl3.c7.Print("%s/invariant_mass.gif" % Run)
drawhist_lvl3.DrawPionEnergy()
drawhist_lvl3.c8.Print("%s/pion_energy.gif" % Run)
drawhist_lvl3.DrawEtaEnergy()
drawhist_lvl3.c9.Print("%s/eta_energy.gif" % Run)
drawhist_lvl3.DrawDiPhotonHitMap()
drawhist_lvl3.c10.Print("%s/diphoton_hit_map.gif" % Run)
