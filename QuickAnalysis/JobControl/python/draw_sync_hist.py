import ROOT
import drawhist_sync
import sys
ROOT.gROOT.SetBatch(True)
args = sys.argv

file,Run = drawhist_sync.file(args[1],args[2])

drawhist_sync.DrawSyncHist()
drawhist_sync.csynctot.Print("%s/silicon_synchronization_total.gif" % Run)
drawhist_sync.csynclayST.Print("%s/silicon_synchronization_small_tower.gif" % Run)
drawhist_sync.csynclayLT.Print("%s/silicon_synchronization_large_tower.gif" % Run)
