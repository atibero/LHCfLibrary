import ROOT
from ROOT import double
from ROOT import TF1
from ROOT import TH1
from ROOT import TH2
import math
import sys

ROOT.gStyle.SetStatY(0.9);                
ROOT.gStyle.SetStatX(0.9);                
ROOT.gStyle.SetStatW(0.2);                
ROOT.gStyle.SetStatH(0.1);   


def file(hist, run):
    global file,Run
    file = ROOT.TFile(hist)
    Run = run
    return file,Run

def DrawSyncHist():
    global csynctot,csynclayST,csynclayLT

    hsynctot = [0 for k in range(2)]
    hsynclay = [[[0 for k in range(4)] for j in range(2)] for i in range(2)]

    for itower in range(2):
        for iview in range(2):
            for ilayer in range(4):
                hsynclay[itower][iview][ilayer] = file.Get("sil_vs_cal_%d_%d_%d_1" % (itower,ilayer,iview))
                hsynctot[itower] = file.Get("sil_vs_cal_sum_%d_1" % (itower))

    csynctot = ROOT.TCanvas("csynctot","csynctot",800,600)
    csynctot.Divide(2,1)

    for itower in range(2):
        csynctot.cd(itower+1)
        hsynctot[itower].Draw("COLZ")
        hsynctot[itower].SetStats(0)
        hsynctot[itower].SetTitle("Tower %d" % (itower))

    csynclayST = ROOT.TCanvas("csynctot_0","csynctot_0",800,600)
    csynclayST.Divide(4,2)
    for iview in range(2):
        for ilayer in range(4):
            csynclayST.cd(ilayer+iview*4+1)
            hsynclay[0][iview][ilayer].Draw("COLZ")
            hsynclay[0][iview][ilayer].SetStats(0)
            if iview==0:
                hsynclay[0][iview][ilayer].SetTitle("Tower 0 Layer %dX" % (ilayer))
            else:
                hsynclay[0][iview][ilayer].SetTitle("Tower 0 Layer %dY" % (ilayer))
 
    csynclayLT = ROOT.TCanvas("csynctot_1","csynctot_1",800,600)
    csynclayLT.Divide(4,2)
    for iview in range(2):
        for ilayer in range(4):
            csynclayLT.cd(ilayer+iview*4+1)
            hsynclay[1][iview][ilayer].Draw("COLZ")
            hsynclay[1][iview][ilayer].SetStats(0)
            if iview==0:
                hsynclay[1][iview][ilayer].SetTitle("Tower 1 Layer %dX" % (ilayer))
            else:
                hsynclay[1][iview][ilayer].SetTitle("Tower 1 Layer %dY" % (ilayer))

