import ROOT
import numpy as np

class SclData():
    def __init__(self, ):
        self.run = [0, 0]
        self.scl1_nev = 0
        self.scl1_stime = 0
        self.scl1_etime = 0
        self.scl1_dtime = 0
        self.scl2_nev = 0
        self.scl2_stime = 0
        self.scl2_etime = 0
        self.scl2_dtime = 0 
 
        self.nev     = np.zeros((2,4)) # total, pede, A1&A, w/ATLAS
        self.nev_l1t = np.zeros((2,2)) # l1t, l1t_BENABLE
        self.nev_l3t = np.zeros((2,5)) # shower, pi0, highem, hadron, zdc
        self.nev_l2t = np.zeros((2,5))
        self.nev_trg = np.zeros((2,5))
        self.nev_trgxx = np.zeros((2,5))
        self.rate     = np.zeros((2,4)) # total, pede, A1&A, w/ATLAS
        self.rate_l3t = np.zeros((2,5)) # shower, pi0, highem, hadron, zdc
        self.rate_l2t = np.zeros((2,5))
        self.rate_trg = np.zeros((2,5))
        self.rate_trgxx = np.zeros((2,5))
        self.livetime_l1t = np.zeros(2)
        self.livetime  = np.zeros((2,5))
        self.prescale  = np.zeros((2,5))

    def Calculate(self, filename=''):
        if filename != '':
            self.Open(filename)
            self.ReadData_Scl1()
            self.ReadData_Scl2()
            self.Close()
            
        self.CalculateRate()
        self.CalculateLiveTime()
        self.CalculatePrescale()

    def Open(self, filename):
        self.file = ROOT.TFile(filename)
        
    def Close(self):
        self.file.Close()

    def DumpResult(self, filename):
        f = open(filename, 'w')
        f.write('run {} {}\n'.format(self.run[0], self.run[1]))
        f.write('scl1 {} {:18.6f} {:18.6f} {:18.6f}\n'.format(self.scl1_nev, self.scl1_stime, self.scl1_etime, self.scl1_dtime))
        f.write('scl2 {} {:18.6f} {:18.6f} {:18.6f}\n'.format(self.scl2_nev, self.scl2_stime, self.scl2_etime, self.scl2_dtime))
        f.write('nev '+''.join(' {}'.format(int(k)) for index, k in np.ndenumerate(self.nev)) + '\n' )
        f.write('nev_l1t '+''.join(' {}'.format(int(k)) for index, k in np.ndenumerate(self.nev_l1t)) + '\n' )
        f.write('nev_l2t '+''.join(' {}'.format(int(k)) for index, k in np.ndenumerate(self.nev_l2t)) + '\n' )
        f.write('nev_trg '+''.join(' {}'.format(int(k)) for index, k in np.ndenumerate(self.nev_trg)) + '\n' )
        f.write('nev_trgxx '+''.join(' {}'.format(int(k)) for index, k in np.ndenumerate(self.nev_trgxx)) + '\n' )
        f.write('nev_l3t '+''.join(' {}'.format(int(k)) for index, k in np.ndenumerate(self.nev_l3t)) + '\n' )
        f.close()

    def ReadResult(self, filename):
        f = open(filename, 'r')
        lines = f.readlines()
        f.close()

        for line in lines:
            i = 1
            s = line.split()
            if s[0] == 'run':
                self.run[0]= int(s[1])
                self.run[1] = int(s[2])
            elif s[0] == 'scl1':
                self.scl1_nev = int(s[1])
                self.scl1_stime = float(s[2])
                self.scl1_etime = float(s[3])
                self.scl1_dtime = float(s[4])
            elif s[0] == 'scl2':
                self.scl2_nev = int(s[1])
                self.scl2_stime = float(s[2])
                self.scl2_etime = float(s[3])
                self.scl2_dtime = float(s[4])
            elif s[0] == 'nev':
                for index, k in np.ndenumerate(self.nev):
                    self.nev[index[0]][index[1]] = float(s[i])
                    i = i + 1
            elif s[0] == 'nev_l1t':
                for index, k in np.ndenumerate(self.nev_l1t):
                    self.nev_l1t[index[0]][index[1]] = float(s[i])
                    i = i + 1
            elif s[0] == 'nev_trg':
                for index, k in np.ndenumerate(self.nev_trg):
                    self.nev_trg[index[0]][index[1]] = float(s[i])
                    i = i + 1
            elif s[0] == 'nev_trgxx':
                for index, k in np.ndenumerate(self.nev_trgxx):
                    self.nev_trgxx[index[0]][index[1]] = float(s[i])
                    i = i + 1                    
            elif s[0] == 'nev_l2t':
                for index, k in np.ndenumerate(self.nev_l2t):
                    self.nev_l2t[index[0]][index[1]] = float(s[i])
                    i = i + 1
            elif s[0] == 'nev_l3t':
                for index, k in np.ndenumerate(self.nev_l3t):
                    self.nev_l3t[index[0]][index[1]] = float(s[i])
                    i = i + 1

        self.Calculate()

    def Add(self, d):
        self.run[0] = self.run[0] if self.run[0] < d.run[0] else d.run[0]
        self.run[1] = self.run[1] if self.run[1] > d.run[1] else d.run[1]
        self.scl1_stime = self.scl1_stime if self.scl1_stime < d.scl1_stime else d.scl1_stime
        self.scl1_etime = self.scl1_etime if self.scl1_etime > d.scl1_etime else d.scl1_etime
        self.scl2_stime = self.scl2_stime if self.scl2_stime < d.scl2_stime else d.scl2_stime
        self.scl2_etime = self.scl2_etime if self.scl2_etime > d.scl2_etime else d.scl2_etime
        self.scl1_dtime += d.scl1_dtime
        self.scl2_dtime += d.scl2_dtime
        self.nev     += d.nev
        self.nev_l2t += d.nev_l2t
        self.nev_l3t += d.nev_l3t
        self.nev_trg += d.nev_trg
        self.nev_trgxx += d.nev_trgxx
        
    def ReadData_Scl1(self):
        
        scl = self.file.Get('Scaler1')

        if not scl :
            print('Warning: No Scaler1 in the file')
            return 

        # Scalar1
        nev = scl.GetEntries()
        if nev == 0:
            return

        self.scl_nev = nev
        # First event
        scl.GetEntry(0)
        self.run[0] = scl.PRUN[0]
        self.run[1] = self.run[0] 
        self.scl1_stime = scl.PTIM[0] + 1.E-6 * scl.PTIM[1]
        # Last event 
        scl.GetEntry(nev-1)
        self.scl1_etime = scl.PTIM[0] + 1.E-6 * scl.PTIM[1]
        self.scl1_dtime = self.scl1_etime - self.scl1_stime
        arm = 0 # Arm1
        self.nev[arm][0] = scl.PSTA[0]
        self.nev_l3t[arm][0] = scl.PSTA[1]
        self.nev_l3t[arm][1] = scl.PSTA[2]
        self.nev_l3t[arm][2] = scl.PSTA[3]
        self.nev_l3t[arm][3] = scl.PSTA[4]
        self.nev_l3t[arm][4] = scl.PSTA[5]
        self.nev[arm][1] = scl.PSTA[6] # pede
        self.nev[arm][2] = scl.PSTA[7] # a1&a2
        self.nev[arm][3] = scl.PSTA[8] # a1&a2

        for arm in range (2):
            self.nev_l1t[arm][0] = scl.PSCL[arm*45+5]
            self.nev_l1t[arm][1] = scl.PSCL[arm*45+6]
            self.nev_trg[arm][0] = scl.PSCL[arm*45+25]
            self.nev_trgxx[arm][0] = scl.PSCL[arm*45+26]
            self.nev_l2t[arm][0] = scl.PSCL[arm*45+27]
            self.nev_trg[arm][1] = scl.PSCL[arm*45+28]
            self.nev_trgxx[arm][1] = scl.PSCL[arm*45+29]
            self.nev_l2t[arm][1] = scl.PSCL[arm*45+30]
            self.nev_trg[arm][2] = scl.PSCL[arm*45+31]
            self.nev_trgxx[arm][2] = scl.PSCL[arm*45+33]
            self.nev_l2t[arm][2] = scl.PSCL[arm*45+33]
            self.nev_trg[arm][3] = scl.PSCL[arm*45+34]
            self.nev_trgxx[arm][2] = scl.PSCL[arm*45+35]
            self.nev_l2t[arm][3] = scl.PSCL[arm*45+36]
            self.nev_trg[arm][4] = scl.PSCL[arm*45+37]
            self.nev_l2t[arm][4] = scl.PSCL[arm*45+38]
        return 
                                        
    def ReadData_Scl2(self):
        
        scl = self.file.Get('Scaler2')

        if not scl :
            print('Warning: No Scaler2 in the file')
            return 

        # Scalar1
        nev = scl.GetEntries()
        if nev == 0:
            return
        
        self.scl_nev = nev
        # First event
        scl.GetEntry(0)
        self.run[0] = scl.PRUN[0]
        self.run[1] = self.run[0]
        self.scl2_stime = scl.PTIM[0] + 1.E-6 * scl.PTIM[1]
        # Last event 
        scl.GetEntry(nev-1)
        self.scl2_etime = scl.PTIM[0] + 1.E-6 * scl.PTIM[1]
        self.scl2_dtime = self.scl1_etime - self.scl1_stime
        arm = 1 # Arm2
        self.nev[arm][0] = scl.PSTA[0]
        self.nev_l3t[arm][0] = scl.PSTA[1]
        self.nev_l3t[arm][1] = scl.PSTA[2]
        self.nev_l3t[arm][2] = scl.PSTA[3]
        self.nev_l3t[arm][3] = scl.PSTA[4]
        self.nev_l3t[arm][4] = scl.PSTA[5]
        self.nev[arm][1] = scl.PSTA[6] # pede
        self.nev[arm][2] = scl.PSTA[7] # a1&a2
        self.nev[arm][3] = scl.PSTA[8] # w/ATLAS
        return

    def CalculateLiveTime(self):
        for arm in range (2):
           if self.nev_l1t[arm][0] > 0:
               self.livetime_l1t[arm] = self.nev_l1t[arm][1]/self.nev_l1t[arm][0]
               
           for i in range(4):
               if self.nev_l2t[arm][i] > 0 :
                   self.livetime[arm][i] = self.nev_l3t[arm][i]/self.nev_l2t[arm][i]

    def CalculatePrescale(self):
        for arm in range (2):
            for i in range(5):
                if self.nev_trg[arm][i] > 0 :
                    self.prescale[arm][i] = self.nev_l2t[arm][i]/self.nev_trg[arm][i]
                    
                   
    def CalculateRate(self):
        for arm in range (2):
            dt = self.scl1_dtime if arm == 1 else self.scl2_dtime
            if dt < 1.E-6:
                return
            for i in range(4):
                self.rate[arm][i] = self.nev[arm][i] / dt
            
            for i in range(5):
                self.rate_trg[arm][i] = self.nev_trg[arm][i] / dt
                self.rate_trgxx[arm][i] = self.nev_trgxx[arm][i] / dt
                self.rate_l2t[arm][i] = self.nev_l2t[arm][i] / dt
                self.rate_l3t[arm][i] = self.nev_l3t[arm][i] / dt
            
        return


class DrawScl():
    def __init__(self):
        self.c = ''

    def Draw(self, data):
        self.c = ROOT.TCanvas("scl","scl",1200,900)

        self.text = ROOT.TLatex()
        self.text.SetNDC()
        self.text.DrawLatex(0.05, 0.95, "RUN {}".format(data.run[0]))

        self.lists = []
        for arm in range(2):
            armsize = 0.47
            ly = 0.80
            lsize = 0.06

            t = ROOT.TLatex()
            t.SetNDC()
            t.SetTextSize(0.04);
            t.DrawLatex(0.05, 0.90-(armsize*arm), "ARM{}".format(arm+1))
            t.SetTextSize(0.025);
            t.DrawLatex(0.16, 0.90-(armsize*arm), "Total: {:10,}   ( {:6.1f} Hz )".format(int(data.nev[arm][0]),data.rate[arm][0]))
            t.DrawLatex(0.45, 0.90-(armsize*arm), "w/ ATLAS: {:10,}   ( {:6.1f} Hz )".format(int(data.nev[arm][3]),data.rate[arm][3]))
            t.DrawLatex(0.80, 0.90-(armsize*arm), "L.F. {:6.3f}".format(data.livetime_l1t[arm]))

            t.SetTextSize(0.025);
            tmp = t.DrawLatex(0.18, ly-armsize*arm+0.05, '   Trg')
            tmp = t.DrawLatex(0.32, ly-armsize*arm+0.05, '   L2T')
            tmp = t.DrawLatex(0.45, ly-armsize*arm+0.05, '   L3T') 
            tmp = t.DrawLatex(0.55, ly-armsize*arm+0.05, '   Prescale')
            tmp = t.DrawLatex(0.72, ly-armsize*arm+0.05, '   Live Fraction') 
            
            labels = ['Shower','Pi0','HighEM','Hadron','ZDC','Pedestal']
            self.lists = []
            for i in range(6):
                t.SetTextSize(0.03);

                tmp = t.DrawLatex(0.05, ly-armsize*arm-lsize*i, "{}".format(labels[i]))
                self.lists.append(tmp)
                
                # trg
                t.SetTextSize(0.025);
                if i < 5:
                    tmp = t.DrawLatex(0.17, ly+lsize/4-armsize*arm-lsize*i, "{:10,}".format(int(data.nev_trg[arm][i])))
                    self.lists.append(tmp)
                    tmp = t.DrawLatex(0.17, ly-lsize/4-armsize*arm-lsize*i, "({:6.1f} Hz )".format(data.rate_trg[arm][i]))
                    self.lists.append(tmp)
                    tmp = t.DrawLatex(0.30, ly+lsize/4-armsize*arm-lsize*i, "{:10,}".format(int(data.nev_l2t[arm][i])))
                    self.lists.append(tmp)
                    tmp = t.DrawLatex(0.30, ly-lsize/4-armsize*arm-lsize*i, "({:6.1f} Hz )".format(data.rate_l2t[arm][i]))
                    self.lists.append(tmp)
                    tmp = t.DrawLatex(0.43, ly+lsize/4-armsize*arm-lsize*i, "{:10,}".format(int(data.nev_l3t[arm][i])))
                    self.lists.append(tmp)
                    tmp = t.DrawLatex(0.43, ly-lsize/4-armsize*arm-lsize*i, "({:6.1f} Hz )".format(data.rate_l3t[arm][i]))
                    self.lists.append(tmp)

                    # Prescale
                    tmp = t.DrawLatex(0.58, ly-armsize*arm-lsize*i, "{:6.3f}".format(data.prescale[arm][i]))
                    # Live fraction
                    tmp = t.DrawLatex(0.75, ly-armsize*arm-lsize*i, "{:6.3f}".format(data.livetime[arm][i]))
                    
                else:
                    # Pedestal
                    tmp = t.DrawLatex(0.43, ly+lsize/4-armsize*arm-lsize*i, "{:10,}".format(data.nev[arm][1]))
                    self.lists.append(tmp)
                    tmp = t.DrawLatex(0.43, ly-lsize/4-armsize*arm-lsize*i, "({:6.1f} Hz )".format(data.rate[arm][1]))
                    self.lists.append(tmp)              
                
                    
    def Print(self, file):
        self.c.Print(file)
