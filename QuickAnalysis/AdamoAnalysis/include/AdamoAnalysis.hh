#ifndef AdamoAnalysis_HH
#define AdamoAnalysis_HH

#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TString.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfParams.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "TGraph.h"
#include "TGraphErrors.h"
//#include "Level2.hh"
#include "LHCfEvent.hh"
#include "SPSAdamo.hh"
#include "SPSAdamoHist.hh"

using namespace std;

namespace nLHCf {

template <typename armclass>
class AdamoAnalysis : public armclass, public LHCfParams {
 public:
  AdamoAnalysis();
  ~AdamoAnalysis();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;   // input file name
  TString fInputDir;    // input file directory
  Int_t fRunNumber;     // run number
  Int_t fSubRunNumber;  // subrun number
  TChain *fInputTree;   // input TChain
  LHCfEvent *fInputEv;  // input LHCfEvent
  TString fOutputName;  // output file name
  TFile *fOutputFile;   // output file

  /*--- Level3 ---*/
  Level2<armclass> *fLvl2;  // LvL2 container
  SPSAdamo *adamomapall;    // Adamo container
  /*--- Histograms ---*/    // 0 small tower, 1 large tower

  // Adamo hitmap
  TH2D *hitmap_adamo;  // single photon hitmap (E>1 TeV)
  // Adamo hitmap with energy release in LHCf calorimeter
  TH2D *hitmap_adamo_cal_release;  // neutron hitmap (E>1 TeV)

  // hit strip
  Int_t hitstrip[SPSAdamo::NLAYER][SPSAdamo::NXY];

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }  // Set input directory
  void SetRunNumber(Int_t nrun) { fRunNumber = nrun; }        // set number of rec_run*****.root
  void SetRunNumber(Int_t nrun, Int_t nsub) {
    fRunNumber = nrun;
    fSubRunNumber = nsub;
  }                                                               // set number of rec_run*****.root
  void SetOutputName(const Char_t *name) { fOutputName = name; }  // set output file name
  void SetInputFile(const Char_t *name) { fInputFile = name; }    // set input file name
  void SetInputTree();                                            // set input tree
  void RunLoop();                                                 // run loop of tree reading and information storing
 private:
  // void SetInputTree();
  void MakeHistograms();  // initialize histograms
  void ClearEvent();      // empty event containers
  void WriteToOutput();   // write histograms into output file
  void CloseFiles();      // close files
  void FillAdamoHist();   // Fill Adamo histograms

  // special functions for Adamo
  int HitSearchInLayer(SPSAdamo *data, int ilayer, int ixy);
  double GetHitPosition(int ilayer, int ixy);
  double StripToPos(int ilayer, int ixy, int istrip);
};
}  // namespace nLHCf
#endif
