#include <TRint.h>

#include "AdamoAnalysis.hh"
#include "TApplication.h"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (or input file if ending with \".root\")\n");
  printf("\t-r <run>\t\tRun number to be analysed (default = -1)\n");
  printf("\t-u <subrun>\t\tSubrun number to be analysed (default = -1)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"converted.root\")\n");
  printf("\t--arm1\t\t\tArm1 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t--arm2\t\t\tArm2 input file (either \"--arm1\" or \"--arm2\" required)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  int run_number = -1;
  int sub_number = -1;
  string output_file = "analysis.root";
  int iarm = -1;
  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"run_number", required_argument, NULL, 'r'},
      {"subrun_number", required_argument, NULL, 'u'},
      {"output", required_argument, NULL, 'o'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'r':
        if (optarg) run_number = atoi(optarg);
        break;
      case 'u':
        if (optarg) sub_number = atoi(optarg);
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 1001:
        iarm = Arm1Params::kArmIndex;
        break;
      case 1002:
        iarm = Arm2Params::kArmIndex;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d\n", run_number);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);
  AdamoAnalysis<Arm1Params> an_1;
  AdamoAnalysis<Arm2Params> an_2;
  if (iarm == Arm1Params::kArmIndex) {
    an_1.SetInputDir(input_dir.c_str());
    // an_1.SetInputTree();
    if (sub_number >= 0)
      an_1.SetRunNumber(run_number, sub_number);
    else
      an_1.SetRunNumber(run_number);
    an_1.SetOutputName(output_file.c_str());
    an_1.RunLoop();
  } else if (iarm == Arm2Params::kArmIndex) {
    an_2.SetInputDir(input_dir.c_str());
    // an_2.SetInputTree();
    if (sub_number >= 0)
      an_2.SetRunNumber(run_number, sub_number);
    else
      an_2.SetRunNumber(run_number);
    an_2.SetOutputName(output_file.c_str());
    an_2.RunLoop();
  } else {
    Utils::Printf(Utils::kPrintError, "Please specify either \"--arm1\" or \"--arm2\"\n");
    return -1;
  }

  return 0;
}
