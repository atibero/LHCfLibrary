#include "AdamoAnalysis.hh"

#include <TH1F.h>
#include <TH2D.h>
#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <chrono>
#include <cstdlib>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "CoordinateTransformation.hh"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "Utils.hh"

using namespace nLHCf;
using namespace std::chrono;
typedef CoordinateTransformation CT;
typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass>
AdamoAnalysis<armclass>::AdamoAnalysis() : fRunNumber(-1), fSubRunNumber(-1) {
  fOutputFile = nullptr;
  fInputEv = nullptr;
  fLvl2 = nullptr;
  adamomapall = nullptr;
  /*fLvl2 = nullptr;
  fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;*/
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass>
AdamoAnalysis<armclass>::~AdamoAnalysis() {
  delete fOutputFile;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
template <typename armclass>
void AdamoAnalysis<armclass>::SetInputTree() {
  Int_t itree = 0;
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fRunNumber >= 0) {  // get file n. fRunNumber
    TString iname;
    if (fSubRunNumber >= 0)
      iname = Form("%s/cal_run%05d_%02d.root", fInputDir.Data(), fRunNumber, fSubRunNumber);
    else
      iname = Form("%s/cal_run%05d.root", fInputDir.Data(), fRunNumber);
    TFile ifile(iname.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
    } else {
      ifile.Close();
      fInputTree->Add(iname.Data());
    }
  } else {  // Error allert
    UT::Printf(UT::kPrintError, "Cannot find .root file!\n");
    exit(EXIT_FAILURE);
  }

  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void AdamoAnalysis<armclass>::ClearEvent() {
  fLvl2 = nullptr;
  adamomapall = nullptr;
  /*fLvl1 = nullptr;
  fLvl0 = nullptr;
  fPed0 = nullptr;*/

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armclass>
void AdamoAnalysis<armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "\nSaving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write();
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
void AdamoAnalysis<armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armclass>
int AdamoAnalysis<armclass>::HitSearchInLayer(SPSAdamo *data, int ilayer, int ixy) {
  const double threshold = 20;
  double value;
  int maxstrip = 0;
  double max = -10000000.;
  for (int is = 0; is < SPSAdamo::NSTRIP; is++) {
    if (ixy == 0) {
      value = data->strip[ilayer][ixy][is];
    } else if (ixy == 1) {
      value = -1. * data->strip[ilayer][ixy][is];
    }

    if (!SPSAdamo::IsValidStrip(ilayer, ixy, is)) {
      continue;
    }
    if (value < threshold) {
      continue;
    }

    if (value > max) {
      max = value;
      maxstrip = is;
    }
  }
  if (max < (-10000000. + 1)) {
    return -1;
  }
  hitstrip[ilayer][ixy] = maxstrip;
  return 1;
}

template <typename armclass>
double AdamoAnalysis<armclass>::GetHitPosition(int ilayer, int ixy) {
  return StripToPos(ilayer, ixy, hitstrip[ilayer][ixy]);
}

template <typename armclass>
double AdamoAnalysis<armclass>::StripToPos(int ilayer, int ixy, int istrip) {
  const double STRIPSTEP_X = 0.0510;
  const double STRIPSTEP_Y = 0.0665;

  if (ixy == 0) {
    return istrip * STRIPSTEP_X;
  }
  if (ixy == 1) {
    // the first strip is located at the top (??).
    istrip = SPSAdamo::NSTRIP - istrip - 1;
    return istrip * STRIPSTEP_Y;
  }
  return 0;
}

template <typename armclass>
void AdamoAnalysis<armclass>::FillAdamoHist() {
  const double sumdethreshold = 500. / 1000.;
  double sums = 0, suml = 0;
  for (int i = 0; i < this->kCalNlayer; i++) {
    sums += fLvl2->fCalorimeter[0][i];
    suml += fLvl2->fCalorimeter[1][i];
  }

  int adamochkx = 0, adamochky = 0;
  double adamox = -1., adamoy = -1.;
  for (int il = 0; il < 5; il++) {
    adamochkx = 0;
    adamochky = 0;

    adamochkx = HitSearchInLayer(adamomapall, il, 0);  // for x
    adamochky = HitSearchInLayer(adamomapall, il, 1);  // for y

    if (il == 1 && adamochky == 1 && adamochky == 1) {
      adamox = GetHitPosition(il, 0);
      adamoy = GetHitPosition(il, 1);
    }
    // cout << adamochkx << " " << adamochky << endl;

    // if(adamochkx==1) adamohist->hist[il][0]->Fill(adamohitsearch->GetHitStrip(il,0));
    // if(adamochky==1) adamohist->hist[il][1]->Fill(adamohitsearch->GetHitStrip(il,1));
  }

  hitmap_adamo->Fill(adamox, adamoy);
  // cout << sums << "     " << suml << endl;
  if (sums > sumdethreshold || suml > sumdethreshold) {
    // cout << "ADAMO HIT: " << adamox << "   " << adamoy << endl;
    hitmap_adamo_cal_release->Fill(adamox, adamoy);
  }
}

template <typename armclass>
void AdamoAnalysis<armclass>::MakeHistograms() {
  hitmap_adamo =
      new TH2D("hitmap_adamo", "ADAMO Hitmap for all", 1024, 0, 1024 * 0.051, (1024 + 20), 0, (1024 + 20) * 0.0665);
  hitmap_adamo->GetXaxis()->SetTitle("x");
  hitmap_adamo->GetYaxis()->SetTitle("y");
  hitmap_adamo_cal_release = (TH2D *)hitmap_adamo->Clone("hitmap_adamo_cal_release");
}

template <typename armclass>
void AdamoAnalysis<armclass>::RunLoop() {
  fOutputFile = new TFile(fOutputName, "RECREATE");
  MakeHistograms();
  SetInputTree();

  /*--- Event Loop ---*/
  if (fSubRunNumber >= 0)
    UT::Printf(UT::kPrintInfo, Form("Start of event loop run %d subrun %d\n", fRunNumber, fSubRunNumber));
  else
    UT::Printf(UT::kPrintInfo, Form("Start of event loop run %d\n", fRunNumber));
  const Int_t print_interval = 10000;
  auto start = high_resolution_clock::now();
  auto start_ev = start;
  Int_t nentries = fInputTree->GetEntries();
  for (Int_t ie = 0; ie < nentries; ++ie) {
    if (ie % 100 == 0 || ie == nentries - 1) {
      auto stop_ev = high_resolution_clock::now();
      auto duration = duration_cast<nanoseconds>(stop_ev - start_ev);
      Double_t diff = 1E9 * 100 / (Double_t)duration.count();
      UT::Printf(UT::kPrintInfo, "\rEvent %6d\t%5.0lf ev/s", ie, diff);
      fflush(stdout);
      start_ev = high_resolution_clock::now();
    }

    fInputTree->GetEntry(ie);
    fLvl2 = (Level2<armclass> *)fInputEv->Get(Form("lvl2_a%d", this->kArmIndex + 1));
    adamomapall = (SPSAdamo *)fInputEv->Get("adamo");

    if (!fLvl2) {
      UT::Printf(UT::kPrintDebug, "Level2 not found in event %d\n", ie);
      continue;
    }
    if (!adamomapall) {
      UT::Printf(UT::kPrintDebug, "Adamo not found in event %d\n", ie);
      continue;
    }
    FillAdamoHist();
  }

  // FillAdamoHist();
  WriteToOutput();
  CloseFiles();
}

namespace nLHCf {
template class AdamoAnalysis<Arm1Params>;
template class AdamoAnalysis<Arm2Params>;
}  // namespace nLHCf
