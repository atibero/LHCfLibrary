#ifndef MggStability_HH
#define MggStability_HH

#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TH1I.h>
#include <TH2D.h>
#include <TString.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfParams.hh"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TParameter.h"
#include "TVectorT.h"

using namespace std;

namespace nLHCf {

class MggStability {
 public:
  MggStability();
  ~MggStability();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;      // input file nam
  Int_t fRun;              // first run number
  Int_t fIterationNumber;  // Number of iteration, or number of Graph's point
  // TFile            *fInputFile;
  TString fParticle;  // particle to be analized, pi or eta

  TString fOutputName;  // output file name
  TFile *fOutputFile;   // output file

  // Int_t fCounter;
  TH1I *fCounter = new TH1I("counter", "counter", 1, 0, 1);  // Internal counter, it is increased for every run, when
                                                             // reach the desired value, stability analysis is triggered

  /*--- Input histogram ---*/  // 0 small tower, 1 large tower

  TH1D *h_mgg;  // diphoton invariant mass

  /*--- Output Graph ---*/
  TGraphErrors *g_stability;  // Mgg stability graph
  /*--- Fit function ---*/
  TF1 *fFit;  // Fit function

 public:
  void SetRun(Int_t run) { fRun = run; }                                      // Set run
  void SetOutputName(const Char_t *name) { fOutputName = name; }              // set output file name
  void SetInputFile(const Char_t *name) { fInputFile = name; }                // set input file name
  void SetIterationNumber(Int_t iteration) { fIterationNumber = iteration; }  // set iteration number
  void SetParticle(const Char_t *particle) { fParticle = particle; }          // set particle, pi or eta

  // void SetCounterNumber(Int_t counter) {fCounter=counter;}
  // oid IncrementCounterNumber() {(fCounter)++;}
  void SetCounterNumber(Int_t counter) { fCounter->SetBinContent(1, counter); }
  void IncrementCounterNumber() { fCounter->SetBinContent(1, fCounter->GetBinContent(1) + 1); }

  void StabilityCalculation();  // stability plot calculation

 private:
  void SetInputHisto();                      // Make input histograms
  void SetOutputGraphs();                    // set graph, only for the first iteration
  void GetOutputGraphs();                    // get output graph
  void GetCounterNumber();                   // get counter number
  void GetAndSumOutputHist();                // get the saved Mgg hist, and add the current one
  void WriteToOutput();                      // write histos and graphs to output
  void CloseFiles();                         // close files
  void FastFit(Double_t inf, Double_t sup);  // Mgg fast fit, gaussian+2nd order polynomial
};
}  // namespace nLHCf
#endif
