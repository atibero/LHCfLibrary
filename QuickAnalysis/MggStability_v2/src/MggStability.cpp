#include "MggStability.hh"

#include <TH1F.h>
#include <TH2D.h>
#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <chrono>
#include <cstdlib>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "CoordinateTransformation.hh"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TParameter.h"
#include "TSystem.h"
#include "TVectorT.h"
#include "Utils.hh"

using namespace nLHCf;
using namespace std::chrono;
typedef CoordinateTransformation CT;
typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
MggStability::MggStability() : fRun(-1) { fOutputFile = nullptr; }

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
MggStability::~MggStability() { delete fOutputFile; }

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/

void MggStability::SetInputHisto() {
  UT::Printf(UT::kPrintInfo, "Setting input Mgg histogram...");
  fflush(stdout);

  h_mgg = new TH1D("h_mgg", "Invariant mass;M_{#gamma#gamma} [MeV];Counts ", 50, 0, 700);

  if (fRun >= 0) {  // get run file
    TString iname(Form("%s", fInputFile.Data()));
    TFile ifile(iname.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
    } else {
      h_mgg->Add((TH1D*)ifile.Get("h_mgg"));
      ifile.Close();
    }
  } else {  // get all ROOT files in input directory

    UT::Printf(UT::kPrintError, "Cannot find file!\n");
    exit(EXIT_FAILURE);
  }
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::SetOutputGraphs() {
  if (fParticle == "pion") {
    g_stability = new TGraphErrors();
    g_stability->SetName("g_stability_pion");
    g_stability->SetTitle("Stability plot #pi^{0} peak");
    g_stability->SetMarkerStyle(kFullCircle);
    g_stability->SetMarkerColor(kBlack);
    g_stability->SetLineColor(kBlack);
    g_stability->SetLineWidth(3);
    g_stability->GetXaxis()->SetTitle("Run number");
    g_stability->GetYaxis()->SetTitle("pi0 peak [MeV]");

    // fOutputFile->cd();
    // g_stability->Write();
  }
  if (fParticle == "eta") {
    g_stability = new TGraphErrors();
    g_stability->SetName("g_stability_eta");
    g_stability->SetTitle("Stability plot  #eta");
    g_stability->SetMarkerStyle(kFullCircle);
    g_stability->SetMarkerColor(kBlack);
    g_stability->SetLineColor(kBlack);
    g_stability->SetLineWidth(3);
    g_stability->GetXaxis()->SetTitle("Run number");
    g_stability->GetYaxis()->SetTitle("Eta peak [MeV]");

    // fOutputFile->cd();
    // g_stability->Write();
  }
}

void MggStability::GetOutputGraphs() {
  if (fParticle == "pion") {
    g_stability = (TGraphErrors*)fOutputFile->Get("g_stability_pion");
  }

  if (fParticle == "eta") {
    g_stability = (TGraphErrors*)fOutputFile->Get("g_stability_eta");
  }
}

void MggStability::GetAndSumOutputHist() { h_mgg->Add((TH1D*)fOutputFile->Get("h_mgg")); }

void MggStability::GetCounterNumber() { fCounter->Add((TH1I*)fOutputFile->Get("counter")); }

void MggStability::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);
  fOutputFile->cd();
  g_stability->Write();
  h_mgg->Write();
  cout << fCounter->GetBinContent(1) << endl;
  // fOutputFile->WriteObject(&fCounter, "fCounter");
  fCounter->Write();
  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::FastFit(Double_t inf, Double_t sup) {
  fFit = new TF1("fFit", "[0]*TMath::Exp(-(x-[1])^2/(2*[2]^2))+[3]+[4]*x+[5]*x^2");
  fFit->SetParameter(0, 1000);
  fFit->SetParameter(1, (sup + inf) / 2);
  fFit->SetParameter(2, (sup - inf) / 2);
  h_mgg->Fit(fFit, "Q+", "", inf, sup);
}

void MggStability::StabilityCalculation() {
  SetInputHisto();
  Int_t count;
  SetCounterNumber(1);
  if (gSystem->AccessPathName(fOutputName.Data())) {
    fOutputFile = new TFile(fOutputName, "RECREATE");
    SetOutputGraphs();
    WriteToOutput();
    CloseFiles();

  } else {
    fOutputFile = new TFile(fOutputName, "READ");
    GetOutputGraphs();
    GetAndSumOutputHist();
    GetCounterNumber();
    // IncrementCounterNumber();
    cout << fCounter->GetBinContent(1) << endl;

    count = fCounter->GetBinContent(1);

    fOutputFile->Close();
    fOutputFile = new TFile(fOutputName, "RECREATE");

    SetCounterNumber(count);

    if (count % 2 == 0) {
      fIterationNumber = ((count) / 2) - 1;
      if (fParticle == "pion") {
        FastFit(100, 160);
      }
      if (fParticle == "eta") {
        FastFit(510, 570);
      }

      Double_t run_number = fRun - 1;
      Double_t peak = fFit->GetParameter(1);
      Double_t run_width = 1;
      Double_t peak_err = fFit->GetParError(1);

      for (Int_t i = 0; i < fIterationNumber; i++) {
        // g_stability->SetPoint(i,g_stability->GetPointX(i), g_stability->GetPointY(i));
        g_stability->SetPoint(i, g_stability->GetX()[i], g_stability->GetY()[i]);
        g_stability->SetPointError(i, run_width, g_stability->GetErrorY(i));
        cout << "ahhh" << endl;
      }

      cout << "ehhh" << endl;
      g_stability->SetPoint(fIterationNumber, run_number, peak);
      g_stability->SetPointError(fIterationNumber, run_width, peak_err);
    }

    WriteToOutput();
    CloseFiles();
  }
}

namespace nLHCf {
class MggStability;
}
