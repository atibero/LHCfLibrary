#include <TRint.h>

#include "MggStability.hh"
#include "TApplication.h"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

/*--------------------*/
/*--- Main program ---*/
/*--------------------*/
void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput file \n");
  printf("\t-r <run>\t\tRun to be analysed (default = -1)\n");
  // printf("\t-n <niteration>\t\tIteration number (default = 1)\n");
  printf("\t-p <particle>\t\tparticle to analysze (pion or eta) (default = pion)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"stability.root\")\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_file = "";
  int run = -1;
  // int    n_iteration=1;
  string particle = "pion";
  string output_file = "stability.root";

  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"run", required_argument, NULL, 'r'},
      //{"niteration",       required_argument, NULL, 'n'},
      {"particle", required_argument, NULL, 'p'},
      {"output", required_argument, NULL, 'o'},

      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_file = optarg;
        break;
      case 'r':
        if (optarg) run = atoi(optarg);
        break;
        /*case 'n':
          if(optarg) n_iteration = atoi(optarg);
          break;*/
      case 'p':
        if (optarg) particle = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  printf("Processing Run: %d\n", run);

  /*--- Initialize ROOT application ---*/
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Conversion ---*/
  Utils::SetVerbose(verbose);

  MggStability stability;

  stability.SetInputFile(input_file.c_str());
  stability.SetRun(run);
  //  stability.SetIterationNumber(n_iteration);
  stability.SetParticle(particle.c_str());
  stability.SetOutputName(output_file.c_str());
  stability.StabilityCalculation();

  return 0;
}
