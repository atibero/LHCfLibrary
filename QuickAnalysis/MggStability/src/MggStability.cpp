#include "MggStability.hh"

#include <TH1F.h>
#include <TH2D.h>
#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <chrono>
#include <cstdlib>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "CoordinateTransformation.hh"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TParameter.h"
#include "TSystem.h"
#include "TVectorT.h"
#include "Utils.hh"

using namespace nLHCf;
using namespace std::chrono;
typedef CoordinateTransformation CT;
typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
MggStability::MggStability() : fFirstRun(-1), fLastRun(-1) { fOutputFile = nullptr; }

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
MggStability::~MggStability() { delete fOutputFile; }

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/

void MggStability::SetInputHisto() {
  UT::Printf(UT::kPrintInfo, "Setting input Mgg histogram...");
  fflush(stdout);

  Int_t count;
  h_mgg = new TH1D("h_mgg", "Invariant mass;M_{#gamma#gamma} [MeV];Counts ", 50, 0, 700);

  if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/run%05d/lvl3_histo_arm%01d_run%05d.root", fInputDir.Data(), irun, fArm, irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        h_mgg->Add((TH1D*)ifile.Get("h_mgg"));
        ifile.Close();

        if (irun == fFirstRun) {
          fOutputFile = new TFile(fOutputName, "RECREATE");
          SetOutputGraphs();
          count = 0;
          WriteToOutput();
          CloseFiles();
        } else if (((irun - fFirstRun) + 1) % fRunWidth == 0) {
          fOutputFile = new TFile(fOutputName, "READ");
          GetOutputGraphs();
          fOutputFile->Close();
          fOutputFile = new TFile(fOutputName, "UPDATE");
          StabilityCalculation(count);
          count++;
        }

        ifile.Close();
      }
    }
  } else {  // get all ROOT files in input directory

    UT::Printf(UT::kPrintError, "Cannot find file!\n");
    exit(EXIT_FAILURE);
  }
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::SetOutputGraphs() {
  if (fParticle == "pion") {
    g_stability = new TGraphErrors();
    g_stability->SetName("g_stability_pion");
    g_stability->SetTitle("Stability plot #pi^{0} peak");
    g_stability->SetMarkerStyle(kFullCircle);
    g_stability->SetMarkerColor(kBlack);
    g_stability->SetLineColor(kBlack);
    g_stability->SetLineWidth(3);
    g_stability->GetXaxis()->SetTitle("Run number");
    g_stability->GetYaxis()->SetTitle("pi0 peak [MeV]");

    // fOutputFile->cd();
    // g_stability->Write();
  }
  if (fParticle == "eta") {
    g_stability = new TGraphErrors();
    g_stability->SetName("g_stability_eta");
    g_stability->SetTitle("Stability plot  #eta");
    g_stability->SetMarkerStyle(kFullCircle);
    g_stability->SetMarkerColor(kBlack);
    g_stability->SetLineColor(kBlack);
    g_stability->SetLineWidth(3);
    g_stability->GetXaxis()->SetTitle("Run number");
    g_stability->GetYaxis()->SetTitle("Eta peak [MeV]");

    // fOutputFile->cd();
    // g_stability->Write();
  }
}

void MggStability::GetOutputGraphs() {
  if (fParticle == "pion") {
    g_stability = (TGraphErrors*)fOutputFile->Get("g_stability_pion");
  }

  if (fParticle == "eta") {
    g_stability = (TGraphErrors*)fOutputFile->Get("g_stability_eta");
  }
}

/*void MggStability::GetCounterNumber()
{
  fCounter->Add((TH1I*)fOutputFile->Get("counter"));
}*/

void MggStability::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);
  fOutputFile->cd();
  g_stability->Write("", TObject::kOverwrite);
  h_mgg->Write();

  // fCounter->Write();
  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void MggStability::FastFit(Double_t inf, Double_t sup) {
  fFit = new TF1("fFit", "[0]*TMath::Exp(-(x-[1])^2/(2*[2]^2))+[3]+[4]*x+[5]*x^2");
  fFit->SetParameter(0, 1000);
  fFit->SetParameter(1, (sup + inf) / 2);
  fFit->SetParameter(2, (sup - inf) / 2);
  h_mgg->GetXaxis()->SetRangeUser(inf, sup);
  h_mgg->Fit(fFit, "Q+", "", inf, sup);
}

void MggStability::GoodFit(Double_t inf, Double_t sup) {
  // fFit = new TF1("fFit", fit_func, inf, sup, 8);
  fFit = new TF1(
      "fFit",
      " [0] * TMath::Exp(-(x-[1])*(x-[1])/2./[2]/[2])  +[3] + [4] * x + [5] * (x*x - 1) + [6] * (4*x*x*x - 3.*x) ");

  Double_t peak;
  if (fParticle == "pion") {
    if (fArm == 1) {
      peak = 95;
    } else if (fArm == 2) {
      peak = 115;
    }
  } else if (fParticle == "eta") {
    if (fArm == 1) {
      peak = 470;
    } else if (fArm == 2) {
      peak = 470;
    }
  }

  h_mgg->GetXaxis()->SetRangeUser(inf, sup);

  fFit->SetParName(0, "A");
  fFit->SetParameter(0, h_mgg->GetMaximum());
  fFit->SetParName(1, "#mu");
  fFit->SetParameter(1, peak);
  fFit->SetParName(2, "#sigma");
  fFit->SetParameter(2, fParticle == "pion" ? 10 : 25);

  fFit->SetParName(3, "p0");
  fFit->SetParameter(3, 150);
  fFit->SetParName(4, "p1");
  fFit->SetParameter(4, -2);
  fFit->SetParName(5, "p2");
  fFit->SetParameter(5, 0.01);
  fFit->SetParName(6, "p3");
  fFit->SetParameter(6, 0);
  fFit->SetParLimits(2, 1, 50);

  h_mgg->Fit(fFit, "Q+", "", inf, sup);
}

void MggStability::StabilityCalculation(Int_t count) {
  // if(fParticle=="pion"){FastFit(50, 180);}
  // if(fParticle=="eta"){FastFit(350, 600);}

  if (fParticle == "pion") {
    GoodFit(50, 180);
  }
  if (fParticle == "eta") {
    GoodFit(350, 600);
  }

  // Double_t run_number=fFirstRun+(count+1)*(fRunWidth/4.);
  Double_t run_number = ((fFirstRun + (count) * (fRunWidth)) + (fFirstRun + (count + 1) * (fRunWidth)-1)) / 2.;
  Double_t peak = fFit->GetParameter(1);
  // Double_t run_width=(fRunWidth/4.);
  Double_t run_width = ((fFirstRun + (count + 1) * (fRunWidth)-1) - (fFirstRun + (count) * (fRunWidth))) / 2.;
  Double_t peak_err = fFit->GetParError(1);

  if (count > 0) {
    for (Int_t i = 0; i < count; i++) {
      // g_stability->SetPoint(i,g_stability->GetPointX(i), g_stability->GetPointY(i));
      g_stability->SetPoint(i, g_stability->GetX()[i], g_stability->GetY()[i]);
      g_stability->SetPointError(i, run_width, g_stability->GetErrorY(i));
    }
  }

  g_stability->SetPoint(count, run_number, peak);
  g_stability->SetPointError(count, run_width, peak_err);
  h_mgg->SetName(Form("h_mgg_%05d_%05d", fFirstRun + (count) * (fRunWidth), fFirstRun + (count + 1) * (fRunWidth)-1));
  h_mgg->SetTitle(Form("Invariant mass %s run %05d-%05d", fParticle.Data(), fFirstRun + (count) * (fRunWidth),
                       fFirstRun + (count + 1) * (fRunWidth)-1));

  WriteToOutput();
  CloseFiles();
  h_mgg->Reset("ICESM");
}

namespace nLHCf {
class MggStability;
}
