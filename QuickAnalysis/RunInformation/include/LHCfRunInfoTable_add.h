#ifndef __LHCFRUNINFOTABLE_ADD_H__
#define __LHCFRUNINFOTABLE_ADD_H__

#include <TObject.h>
#include <globaldef.h>

#include <Arm1Params.hh>
#include <Arm2Params.hh>
#include <LHCfParams.hh>
#include <Level0.hh>
#include <Level1.hh>
#include <Level2.hh>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

namespace nLHCf {
template <typename armclass, typename armcal>
class LHCfRunInfo_add : public armcal, public LHCfParams {
 public:
  LHCfRunInfo_add();
  ~LHCfRunInfo_add();

  void clear();

  int GetBunchTag(Level1<armclass> *cal);

  int CheckTime(double time);
  int CheckTime(Level1<armclass> *cal) { return CheckTime(cal->fTime[0]); }
  int CheckTime(Level2<armclass> *cal) { return CheckTime(cal->fTime[0]); }
  int CheckBunchQuality(Level1<armclass> *cal);  // if bunch quality is good, return 1. if not, return 0.
};
}  // namespace nLHCf

#endif
