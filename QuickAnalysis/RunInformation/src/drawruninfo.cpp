#include <TApplication.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TRint.h>
#include <TStyle.h>

#include <cmath>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>

#include "../../MakeHistograms/include/gsetup.h"
#include "LHCfRunInfoTable.h"
#include "LHCfRunInfoTable_add.h"
using namespace std;

char* GetChar(const char* format, int val);
char* GetChar(const char* format, double val);
char* GetChar(const char* format, char* val);
string CutdownV1495Load(char csetup[]);
string CutdownGPIOLoad(char csetup[]);
string CutdownGPIOSetup(char csetup[]);
string CutdownDiscri(char csetup[]);

int main(int argc, char** argv) {
  gsetup();
  char runinfofile[256] = "";
  char printpath[256] = "graphs/";
  bool savecanvas = false;
  int run;
  int subrun = -1;
  bool addflag = false;
  char runinfodir[256] = "";
  char runinfofile_sub[256] = "";

  for (int ic = 0; ic < argc; ic++) {
    if (strcmp(argv[ic], "-runinfo") == 0) {
      strcpy(runinfofile, argv[++ic]);
      strcpy(argv[ic], "");
    }
    if (strcmp(argv[ic], "-nodraw") == 0) {
      strcpy(argv[ic], "-b");
    }
    if (strcmp(argv[ic], "-print") == 0) {
      savecanvas = true;
      if ((ic + 1) == argc || *(argv[ic + 1]) == '-') {
        strcpy(printpath, "./graphs");
      } else {
        strcpy(printpath, argv[++ic]);
        strcpy(argv[ic], "");
      }
    }
    if (strcmp(argv[ic], "-run") == 0) {
      run = atoi(argv[++ic]);
      cout << "RUN  : " << run << endl;
    }
    if (strcmp(argv[ic], "-subrun") == 0) {
      subrun = atoi(argv[++ic]);
      cout << "SUBRUN  : " << subrun << endl;
    }
    if (strcmp(argv[ic], "-add") == 0) {
      addflag = true;
    }
    if (strcmp(argv[ic], "-runinfodir") == 0) {
      // runinfodir = argv[++ic];
      strcpy(runinfodir, argv[++ic]);
      cout << "runinfodir =" << runinfodir << endl;
    }

    if (strcmp(argv[ic], "-h") == 0 || strcmp(argv[ic], "-help") == 0 || argc == 1) {
      cout << endl
           << " drawhists ---------------------------------------------------------- \n"
           << "  -run            :  run number\n"
           << "  -runinfo [file] :  Runinfo file\n"
           << "  -nodraw         :  Not draw any histograms. \n"
           << "  -print [path]   :  Save Canvases\n"
           << "  -q,-b,          :  ROOT options. \n"
           << endl;
      return 0;
    }
  }
  /*
  if(strcmp(runinfofile,"")==0 || run < 0){
    cerr << "Error: Give a filename." << endl;
    exit(-1);
  }
  */
  TRint app("drawruninfo", &argc, argv, 0, 0, kTRUE);

  TCanvas* c = new TCanvas("c", "RunInfo", 800, 700);
  char text[256];
  double x, y, X, Y, offx, offy;
  TLatex* latex = new TLatex();
  latex->SetTextFont(102);

  // ++++++ DRAW ++++++++++++++++++++++++++++++++++++++++++++++
  if (addflag == false) {
    LHCfRunInfo* info = NULL;
    LHCfRunInfoTable* infotable = new LHCfRunInfoTable();
    if (infotable->ReadTable(runinfofile) == OK) {
      if (infotable->GetRunInfo(run, subrun)) {
        info = infotable->GetRunInfo(run, subrun);
      }
    } else {
      cerr << "Cannot open " << runinfofile << endl;
      exit(-1);
    }
    if (info == NULL) {
      cerr << "No data for RUN:" << run << " in " << runinfofile << endl;
      exit(-1);
    }

    // RUN NUMBER
    latex->SetTextSize(0.06);
    sprintf(text, "RUN%05d_%02d", (run, subrun));
    latex->DrawTextNDC(0.38, 0.94, text);
    // TIME
    X = 0.10;
    Y = 0.89;
    latex->SetTextSize(0.028);
    latex->DrawTextNDC(X, Y, "START:");
    time_t ts = info->start_time;
    latex->DrawTextNDC(X + 0.1, Y, asctime(localtime(&ts)));
    latex->DrawTextNDC(X, Y - 0.028, "END:");
    time_t te = info->end_time;
    latex->DrawTextNDC(X + 0.1, Y - 0.028, asctime(localtime(&te)));
    latex->DrawTextNDC(X + 0.5, Y, "DAQ TIME:");
    sprintf(text, "%d sec", info->end_time - info->start_time);
    latex->DrawTextNDC(X + 0.63, Y, text);
    double deltat = info->end_time - info->start_time;

    // BEAM CONFIG
    int nbunch_col = 0, nbunch_non = 0, nbunch_dis = 0;
    for (int i = 0; i < info->beam1_nbunches; i++) {
      if (abs(info->beam1_tag[i]) == 1) {
        nbunch_col++;
      }
      if (abs(info->beam1_tag[i]) == 2) {
        nbunch_non++;
      }
      if (abs(info->beam1_tag[i]) == 3) {
        nbunch_dis++;
      }
    }
    X = 0.05;
    Y = 0.80;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "BEAM CONFIGURATION: ");
    sprintf(text, "%dx%d", info->beam1_nbunches, info->beam2_nbunches);
    latex->DrawTextNDC(X + 0.35, Y, text);
    latex->SetTextSize(0.028);
    latex->DrawTextNDC(X + 0.45, Y - offy * 1, GetChar("COL: %3d", nbunch_col));
    latex->DrawTextNDC(X + 0.60, Y - offy * 1, GetChar("NON: %3d", nbunch_non));
    latex->DrawTextNDC(X + 0.75, Y - offy * 1, GetChar("DIS: %3d", nbunch_dis));

    double livetime;
    // ARM1
    X = 0.05;
    Y = 0.74;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM1");
    latex->SetTextSize(0.025);
    X = 0.05;
    Y = 0.73;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, " #EVENT");
    latex->DrawTextNDC(X + 0.22, Y - offy * 0, "RATE[Hz]");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "TRIGGER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "L2T_SHW");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "L2T_PI0");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "L2T_HIGHEM");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "L2T_HADRON");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "L2T_PEDE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 7, "L2T_ZDC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 8, "LASER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 9, "SCALER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 10, "DAQ LIVE TIME");
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, GetChar("%7d", info->a1_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, GetChar("%7d", info->a1_nevent_L2T_Shower));
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, GetChar("%7d", info->a1_nevent_L2T_Pi0));
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, GetChar("%7d", info->a1_nevent_L2T_HighEM));
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, GetChar("%7d", info->a1_nevent_L2T_Hadron));
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, GetChar("%7d", info->a1_nevent_L2T_Pedestal));
    latex->DrawTextNDC(X + 0.12, Y - offy * 7, GetChar("%7d", info->a1_nevent_L2T_Zdc));
    latex->DrawTextNDC(X + 0.12, Y - offy * 8, GetChar("%7d", info->a1_nevent_laser));
    latex->DrawTextNDC(X + 0.12, Y - offy * 9, GetChar("%7d", info->a1scl_nevent));
    latex->DrawTextNDC(X + 0.22, Y - offy * 1, GetChar("%8.2lf", info->a1_nevent / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 2, GetChar("%8.2lf", info->a1_nevent_L2T_Shower / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 3, GetChar("%8.2lf", info->a1_nevent_L2T_Pi0 / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 4, GetChar("%8.2lf", info->a1_nevent_L2T_HighEM / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 5, GetChar("%8.2lf", info->a1_nevent_L2T_Hadron / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 6, GetChar("%8.2lf", info->a1_nevent_L2T_Pedestal / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 7, GetChar("%8.2lf", info->a1_nevent_L2T_Zdc / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 8, GetChar("%8.2lf", info->a1_nevent_laser / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 9, GetChar("%8.2lf", info->a1scl_nevent / deltat));
    livetime = 100. * (double)info->a1_nevent_l1t_enable / (double)info->a1_nevent_l1t;
    latex->DrawTextNDC(X + 0.21, Y - offy * 10, GetChar("%5.2lf [\%%]", livetime));

    // ARM2
    X = 0.40;
    Y = 0.74;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM2");
    latex->SetTextSize(0.025);
    X = 0.40;
    Y = 0.73;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, " #EVENT");
    latex->DrawTextNDC(X + 0.22, Y - offy * 0, "RATE[Hz]");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "TRIGGER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "L2T_SHW");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "L2T_PI0");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "L2T_HIGHEM");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "L2T_HADRON");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "L2T_PEDE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 7, "L2T_ZDC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 8, "LASER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 9, "SCALER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 10, "DAQ LIVE TIME");
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, GetChar("%7d", info->a2_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, GetChar("%7d", info->a2_nevent_L2T_Shower));
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, GetChar("%7d", info->a2_nevent_L2T_Pi0));
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, GetChar("%7d", info->a2_nevent_L2T_HighEM));
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, GetChar("%7d", info->a2_nevent_L2T_Hadron));
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, GetChar("%7d", info->a2_nevent_L2T_Pedestal));
    latex->DrawTextNDC(X + 0.12, Y - offy * 7, GetChar("%7d", info->a2_nevent_L2T_Zdc));
    latex->DrawTextNDC(X + 0.12, Y - offy * 8, GetChar("%7d", info->a2_nevent_laser));
    latex->DrawTextNDC(X + 0.12, Y - offy * 9, GetChar("%7d", info->a2scl_nevent));
    latex->DrawTextNDC(X + 0.22, Y - offy * 1, GetChar("%8.2lf", info->a2_nevent / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 2, GetChar("%8.2lf", info->a2_nevent_L2T_Shower / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 3, GetChar("%8.2lf", info->a2_nevent_L2T_Pi0 / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 4, GetChar("%8.2lf", info->a2_nevent_L2T_HighEM / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 5, GetChar("%8.2lf", info->a2_nevent_L2T_Hadron / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 6, GetChar("%8.2lf", info->a2_nevent_L2T_Pedestal / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 7, GetChar("%8.2lf", info->a2_nevent_L2T_Zdc / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 8, GetChar("%8.2lf", info->a2_nevent_laser / deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 9, GetChar("%8.2lf", info->a2scl_nevent / deltat));
    livetime = 100. * (double)info->a2_nevent_l1t_enable / (double)info->a2_nevent_l1t;
    latex->DrawTextNDC(X + 0.21, Y - offy * 10, GetChar("%5.2lf [\%%]", livetime));

    // Slow Data
    X = 0.75;
    Y = 0.74;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "SC");
    latex->SetTextSize(0.025);
    X = 0.75;
    Y = 0.73;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, " #EVENT");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "SC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "DIPL");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "DIPH");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "DIPVH");
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, GetChar("%7d", info->sc_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, GetChar("%7d", info->dipl_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, GetChar("%7d", info->diph_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, GetChar("%7d", info->dipvh_nevent));

    // DAQ Setup
    // Arm1
    X = 0.05;
    Y = 0.37;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM1 SETUP");
    latex->SetTextSize(0.025);
    X = 0.05;
    Y = 0.33;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.02, Y - offy * 0, "RUN MODE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "LOGIC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "SETUP");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "PMT_HV");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "SCIFI_HV");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "DISCRI");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "POSITION");
    X = 0.10;
    Y = 0.33;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, GetChar("%d", info->a1_runmode));
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, (CutdownV1495Load(info->a1_V1495Load)).c_str());
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, info->a1_LogicMode);
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, info->a1_PMTgain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, info->a1_scifigain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, (CutdownDiscri(info->a1_Discriminator)).c_str());
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, info->a1_position);
    // Arm2
    X = 0.50;
    Y = 0.37;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM2 SETUP");
    latex->SetTextSize(0.025);
    X = 0.50;
    Y = 0.33;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.02, Y - offy * 0, "RUN MODE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "LOGIC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "SETUP");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "PMT_HV");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "SILICON");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "DISCRI");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "POSITION");
    latex->DrawTextNDC(X + 0.02, Y - offy * 8, "CRC ERROR");
    X = 0.55;
    Y = 0.33;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, GetChar("%d", info->a2_runmode));
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, (CutdownV1495Load(info->a2_V1495Load)).c_str());
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, info->a2_LogicMode);
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, info->a2_PMTgain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, info->a2_silicongain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, (CutdownDiscri(info->a2_Discriminator)).c_str());
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, info->a2_position);
    latex->DrawTextNDC(X + 0.12, Y - offy * 8, Form("%d", info->a2_ncrcerror));
  }
  if (addflag == true) {
    time_t startfirstsubruntime;
    time_t endlastsubruntime;
    double deltat_all;
    int nbunch_col_all = 0, nbunch_non_all = 0, nbunch_dis_all = 0;
    int beam1_nbunches = 0, beam2_nbunches = 0;
    int a1_nevent = 0, a1_nevent_L2T_Shower = 0, a1_nevent_L2T_Pi0 = 0, a1_nevent_L2T_HighEM = 0,
        a1_nevent_L2T_Hadron = 0, a1_nevent_L2T_Pedestal = 0, a1_nevent_L2T_Zdc = 0, a1_nevent_laser = 0,
        a1scl_nevent = 0;
    double a1_nevent_deltat, a1_nevent_L2T_Shower_deltat, a1_nevent_L2T_Pi0_deltat, a1_nevent_L2T_HighEM_deltat,
        a1_nevent_L2T_Hadron_deltat, a1_nevent_L2T_Pedestal_deltat, a1_nevent_L2T_Zdc_deltat, a1_nevent_laser_deltat,
        a1scl_nevent_deltat, a1_nevent_l1t_enable, a1_nevent_l1t, a1_livetime_all;
    int a2_nevent = 0, a2_nevent_L2T_Shower = 0, a2_nevent_L2T_Pi0 = 0, a2_nevent_L2T_HighEM = 0,
        a2_nevent_L2T_Hadron = 0, a2_nevent_L2T_Pedestal = 0, a2_nevent_L2T_Zdc = 0, a2_nevent_laser = 0,
        a2scl_nevent = 0;
    double a2_nevent_deltat, a2_nevent_L2T_Shower_deltat, a2_nevent_L2T_Pi0_deltat, a2_nevent_L2T_HighEM_deltat,
        a2_nevent_L2T_Hadron_deltat, a2_nevent_L2T_Pedestal_deltat, a2_nevent_L2T_Zdc_deltat, a2_nevent_laser_deltat,
        a2scl_nevent_deltat, a2_nevent_l1t_enable, a2_nevent_l1t, a2_livetime_all;
    int sc_nevent = 0, dipl_nevent = 0, diph_nevent = 0, dipvh_nevent = 0;
    int a1_runmode;
    char *a1_LogicMode, *a1_PMTgain, *a1_scifigain, *a1_position;
    const char *a1_V1495Load, *a1_Discriminator;
    int a2_runmode;
    char *a2_LogicMode, *a2_PMTgain, *a2_silicongain, *a2_position;
    const char *a2_V1495Load, *a2_Discriminator;
    int a2_ncrcerror = 0;

    for (int isub = 0; isub < 100; isub++) {
      // runinfofile_sub =  GetChar("%s/run%05d_%02d/runinfo_run%05d_%02d.dat",(runinfodir,run,isub,run,isub)) ;
      // strcpy(runinfofile_sub,GetChar("%s/run%05d_%02d/runinfo_run%05d_%02d.dat",(runinfodir,run,isub,run,isub)));
      // std::string rifs = GetChar("%s/run%05d_%02d/runinfo_run%05d_%02d.dat",(runinfodir,run,isub,run,isub));
      // int length = rifs.length();
      // for(int i=0;i<length;i++){
      // runinfofile_sub[i] = rifs[i];
      //}
      cout << "RUN  : " << run << endl;
      cout << "isub  : " << isub << endl;
      sprintf(runinfofile_sub, "%s/run%05d_%02d/runinfo_run%05d_%02d.dat", runinfodir, run, isub, run, isub);
      cout << "runinfofile_sub = " << runinfofile_sub << endl;
      LHCfRunInfo* info_sub = NULL;
      LHCfRunInfoTable* infotable_sub = new LHCfRunInfoTable();
      if (infotable_sub->ReadTable(runinfofile_sub) == OK) {
        if (infotable_sub->GetRunInfo(run, isub)) {
          info_sub = infotable_sub->GetRunInfo(run, isub);
        }
      }

      else {
        cerr << "Cannot open " << runinfofile_sub << endl;
        continue;
        // exit(-1);
      }
      if (info_sub == NULL) {
        cerr << "No data for RUN:" << run << " in " << runinfofile_sub << endl;
        continue;
        // exit(-1);
      }

      if (isub == 0) {
        startfirstsubruntime = info_sub->start_time;
        cout << "SUBRUN:0   start time : " << asctime(localtime(&startfirstsubruntime)) << endl;
      }
      endlastsubruntime = info_sub->end_time;
      for (int i = 0; i < info_sub->beam1_nbunches; i++) {
        if (abs(info_sub->beam1_tag[i]) == 1) {
          nbunch_col_all++;
        }
        if (abs(info_sub->beam1_tag[i]) == 2) {
          nbunch_non_all++;
        }
        if (abs(info_sub->beam1_tag[i]) == 3) {
          nbunch_dis_all++;
        }
      }
      beam1_nbunches += info_sub->beam1_nbunches;
      beam2_nbunches += info_sub->beam2_nbunches;
      a1_nevent += info_sub->a1_nevent;
      a1_nevent_L2T_Shower += info_sub->a1_nevent_L2T_Shower;
      a1_nevent_L2T_Pi0 += info_sub->a1_nevent_L2T_Pi0;
      a1_nevent_L2T_HighEM += info_sub->a1_nevent_L2T_HighEM;
      a1_nevent_L2T_Hadron += info_sub->a1_nevent_L2T_Hadron;
      a1_nevent_L2T_Pedestal += info_sub->a1_nevent_L2T_Pedestal;
      a1_nevent_L2T_Zdc += info_sub->a1_nevent_L2T_Zdc;
      a1_nevent_laser += info_sub->a1_nevent_laser;
      a1scl_nevent += info_sub->a1scl_nevent;
      a1_nevent_l1t_enable += info_sub->a1_nevent_l1t_enable;
      a1_nevent_l1t += info_sub->a1_nevent_l1t;
      a2_nevent += info_sub->a2_nevent;
      a2_nevent_L2T_Shower += info_sub->a2_nevent_L2T_Shower;
      a2_nevent_L2T_Pi0 += info_sub->a2_nevent_L2T_Pi0;
      a2_nevent_L2T_HighEM += info_sub->a2_nevent_L2T_HighEM;
      a2_nevent_L2T_Hadron += info_sub->a2_nevent_L2T_Hadron;
      a2_nevent_L2T_Pedestal += info_sub->a2_nevent_L2T_Pedestal;
      a2_nevent_L2T_Zdc += info_sub->a2_nevent_L2T_Zdc;
      a2_nevent_laser += info_sub->a2_nevent_laser;
      a2scl_nevent += info_sub->a2scl_nevent;
      a2_nevent_l1t_enable += info_sub->a2_nevent_l1t_enable;
      a2_nevent_l1t += info_sub->a2_nevent_l1t;
      sc_nevent += info_sub->sc_nevent;
      dipl_nevent += info_sub->dipl_nevent;
      diph_nevent += info_sub->diph_nevent;
      dipvh_nevent += info_sub->dipvh_nevent;
      a2_ncrcerror += info_sub->a2_ncrcerror;

      if (isub == 0) {
        a1_runmode = info_sub->a1_runmode;
        a1_V1495Load = (CutdownV1495Load(info_sub->a1_V1495Load)).c_str();
        a1_LogicMode = info_sub->a1_LogicMode;
        a1_PMTgain = info_sub->a1_PMTgain;
        a1_scifigain = info_sub->a1_scifigain;
        a1_Discriminator = (CutdownDiscri(info_sub->a1_Discriminator)).c_str();
        a1_position = info_sub->a1_position;
        a2_runmode = info_sub->a2_runmode;
        a2_V1495Load = (CutdownV1495Load(info_sub->a2_V1495Load)).c_str();
        a2_LogicMode = info_sub->a2_LogicMode;
        a2_PMTgain = info_sub->a2_PMTgain;
        a2_silicongain = info_sub->a2_silicongain;
        a2_Discriminator = (CutdownDiscri(info_sub->a2_Discriminator)).c_str();
        a2_position = info_sub->a2_position;
      }
    }

    deltat_all = endlastsubruntime - startfirstsubruntime;
    a1_nevent_deltat = a1_nevent / deltat_all;
    a1_nevent_L2T_Shower_deltat = a1_nevent_L2T_Shower / deltat_all;
    a1_nevent_L2T_Pi0_deltat = a1_nevent_L2T_Pi0 / deltat_all;
    a1_nevent_L2T_HighEM_deltat = a1_nevent_L2T_HighEM / deltat_all;
    a1_nevent_L2T_Hadron_deltat = a1_nevent_L2T_Hadron / deltat_all;
    a1_nevent_L2T_Pedestal_deltat = a1_nevent_L2T_Pedestal / deltat_all;
    a1_nevent_L2T_Zdc_deltat = a1_nevent_L2T_Zdc / deltat_all;
    a1_nevent_laser_deltat = a1_nevent_laser / deltat_all;
    a1scl_nevent_deltat = a1scl_nevent / deltat_all;
    a1_livetime_all = 100. * a1_nevent_l1t_enable / a1_nevent_l1t;
    a2_nevent_deltat = a2_nevent / deltat_all;
    a2_nevent_L2T_Shower_deltat = a2_nevent_L2T_Shower / deltat_all;
    a2_nevent_L2T_Pi0_deltat = a2_nevent_L2T_Pi0 / deltat_all;
    a2_nevent_L2T_HighEM_deltat = a2_nevent_L2T_HighEM / deltat_all;
    a2_nevent_L2T_Hadron_deltat = a2_nevent_L2T_Hadron / deltat_all;
    a2_nevent_L2T_Pedestal_deltat = a2_nevent_L2T_Pedestal / deltat_all;
    a2_nevent_L2T_Zdc_deltat = a2_nevent_L2T_Zdc / deltat_all;
    a2_nevent_laser_deltat = a2_nevent_laser / deltat_all;
    a2scl_nevent_deltat = a2scl_nevent / deltat_all;
    a2_livetime_all = 100. * a2_nevent_l1t_enable / a2_nevent_l1t;

    // Initialize runinfo table
    //
    LHCfRunInfo* info;
    LHCfRunInfoTable* infotable = new LHCfRunInfoTable();
    if (strcmp(runinfofile, "") != 0) {
      infotable->ReadTable(runinfofile);
    }
    if (infotable->Get(run, subrun)) {
      info = infotable->Get(run, subrun);
      info->clear_nevents("ext_comments");
    } else {
      info = infotable->CreateInfo();
      info->clear();
    }
    // Write runinfo table
    infotable->WriteTable(runinfofile, 1);

    // Read Runinfo table
    if (infotable->ReadTable(runinfofile) == OK) {
      if (infotable->GetRunInfo(run, subrun)) {
        info = infotable->GetRunInfo(run, subrun);
      }
    } else {
      cerr << "Cannot open " << runinfofile << endl;
      exit(-1);
    }
    if (info == NULL) {
      cerr << "No data for RUN:" << run << " in " << runinfofile << endl;
      // exit(-1);
    }

    // RUN NUMBER
    latex->SetTextSize(0.06);
    sprintf(text, "RUN%05d", run);
    latex->DrawTextNDC(0.38, 0.94, text);
    // TIME
    X = 0.10;
    Y = 0.89;
    latex->SetTextSize(0.028);
    latex->DrawTextNDC(X, Y, "START:");
    time_t ts = startfirstsubruntime;
    latex->DrawTextNDC(X + 0.1, Y, asctime(localtime(&ts)));
    latex->DrawTextNDC(X, Y - 0.028, "END:");
    time_t te = endlastsubruntime;
    latex->DrawTextNDC(X + 0.1, Y - 0.028, asctime(localtime(&te)));
    latex->DrawTextNDC(X + 0.5, Y, "DAQ TIME:");
    sprintf(text, "%.0f sec", deltat_all);
    latex->DrawTextNDC(X + 0.63, Y, text);

    // BEAM CONFIG
    X = 0.05;
    Y = 0.80;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "BEAM CONFIGURATION: ");
    sprintf(text, "%dx%d", beam1_nbunches, beam2_nbunches);
    latex->DrawTextNDC(X + 0.35, Y, text);
    latex->SetTextSize(0.028);
    latex->DrawTextNDC(X + 0.45, Y - offy * 1, GetChar("COL: %3d", nbunch_col_all));
    latex->DrawTextNDC(X + 0.60, Y - offy * 1, GetChar("NON: %3d", nbunch_non_all));
    latex->DrawTextNDC(X + 0.75, Y - offy * 1, GetChar("DIS: %3d", nbunch_dis_all));

    // ARM1
    X = 0.05;
    Y = 0.74;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM1");
    latex->SetTextSize(0.025);
    X = 0.05;
    Y = 0.73;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, " #EVENT");
    latex->DrawTextNDC(X + 0.22, Y - offy * 0, "RATE[Hz]");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "TRIGGER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "L2T_SHW");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "L2T_PI0");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "L2T_HIGHEM");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "L2T_HADRON");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "L2T_PEDE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 7, "L2T_ZDC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 8, "LASER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 9, "SCALER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 10, "DAQ LIVE TIME");
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, GetChar("%7d", a1_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, GetChar("%7d", a1_nevent_L2T_Shower));
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, GetChar("%7d", a1_nevent_L2T_Pi0));
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, GetChar("%7d", a1_nevent_L2T_HighEM));
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, GetChar("%7d", a1_nevent_L2T_Hadron));
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, GetChar("%7d", a1_nevent_L2T_Pedestal));
    latex->DrawTextNDC(X + 0.12, Y - offy * 7, GetChar("%7d", a1_nevent_L2T_Zdc));
    latex->DrawTextNDC(X + 0.12, Y - offy * 8, GetChar("%7d", a1_nevent_laser));
    latex->DrawTextNDC(X + 0.12, Y - offy * 9, GetChar("%7d", a1scl_nevent));
    latex->DrawTextNDC(X + 0.22, Y - offy * 1, GetChar("%8.2lf", a1_nevent_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 2, GetChar("%8.2lf", a1_nevent_L2T_Shower_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 3, GetChar("%8.2lf", a1_nevent_L2T_Pi0_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 4, GetChar("%8.2lf", a1_nevent_L2T_HighEM_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 5, GetChar("%8.2lf", a1_nevent_L2T_Hadron_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 6, GetChar("%8.2lf", a1_nevent_L2T_Pedestal_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 7, GetChar("%8.2lf", a1_nevent_L2T_Zdc_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 8, GetChar("%8.2lf", a1_nevent_laser_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 9, GetChar("%8.2lf", a1scl_nevent_deltat));
    latex->DrawTextNDC(X + 0.21, Y - offy * 10, GetChar("%5.2lf [\%%]", a1_livetime_all));

    // ARM2
    X = 0.40;
    Y = 0.74;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM2");
    latex->SetTextSize(0.025);
    X = 0.40;
    Y = 0.73;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, " #EVENT");
    latex->DrawTextNDC(X + 0.22, Y - offy * 0, "RATE[Hz]");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "TRIGGER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "L2T_SHW");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "L2T_PI0");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "L2T_HIGHEM");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "L2T_HADRON");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "L2T_PEDE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 7, "L2T_ZDC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 8, "LASER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 9, "SCALER");
    latex->DrawTextNDC(X + 0.02, Y - offy * 10, "DAQ LIVE TIME");
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, GetChar("%7d", a2_nevent));
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, GetChar("%7d", a2_nevent_L2T_Shower));
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, GetChar("%7d", a2_nevent_L2T_Pi0));
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, GetChar("%7d", a2_nevent_L2T_HighEM));
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, GetChar("%7d", a2_nevent_L2T_Hadron));
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, GetChar("%7d", a2_nevent_L2T_Pedestal));
    latex->DrawTextNDC(X + 0.12, Y - offy * 7, GetChar("%7d", a2_nevent_L2T_Zdc));
    latex->DrawTextNDC(X + 0.12, Y - offy * 8, GetChar("%7d", a2_nevent_laser));
    latex->DrawTextNDC(X + 0.12, Y - offy * 9, GetChar("%7d", a2scl_nevent));
    latex->DrawTextNDC(X + 0.22, Y - offy * 1, GetChar("%8.2lf", a2_nevent_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 2, GetChar("%8.2lf", a2_nevent_L2T_Shower_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 3, GetChar("%8.2lf", a2_nevent_L2T_Pi0_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 4, GetChar("%8.2lf", a2_nevent_L2T_HighEM_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 5, GetChar("%8.2lf", a2_nevent_L2T_Hadron_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 6, GetChar("%8.2lf", a2_nevent_L2T_Pedestal_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 7, GetChar("%8.2lf", a2_nevent_L2T_Zdc_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 8, GetChar("%8.2lf", a2_nevent_laser_deltat));
    latex->DrawTextNDC(X + 0.22, Y - offy * 9, GetChar("%8.2lf", a2scl_nevent_deltat));
    latex->DrawTextNDC(X + 0.21, Y - offy * 10, GetChar("%5.2lf [\%%]", a2_livetime_all));

    // Slow Data
    X = 0.75;
    Y = 0.74;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "SC");
    latex->SetTextSize(0.025);
    X = 0.75;
    Y = 0.73;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, " #EVENT");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "SC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "DIPL");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "DIPH");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "DIPVH");
    // latex->DrawTextNDC(X+0.12,Y-offy*1, GetChar("%7d",info->sc_nevent));
    // latex->DrawTextNDC(X+0.12,Y-offy*2, GetChar("%7d",info->dipl_nevent));
    // latex->DrawTextNDC(X+0.12,Y-offy*3, GetChar("%7d",info->diph_nevent));
    // latex->DrawTextNDC(X+0.12,Y-offy*4, GetChar("%7d",info->dipvh_nevent));

    // DAQ Setup
    // Arm1
    X = 0.05;
    Y = 0.37;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM1 SETUP");
    latex->SetTextSize(0.025);
    X = 0.05;
    Y = 0.33;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.02, Y - offy * 0, "RUN MODE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "LOGIC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "SETUP");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "PMT_HV");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "SCIFI_HV");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "DISCRI");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "POSITION");
    X = 0.10;
    Y = 0.33;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, GetChar("%d", a1_runmode));
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, a1_V1495Load);
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, a1_LogicMode);
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, a1_PMTgain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, a1_scifigain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, a1_Discriminator);
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, a1_position);
    // Arm2
    X = 0.50;
    Y = 0.37;
    latex->SetTextSize(0.035);
    latex->DrawTextNDC(X, Y, "ARM2 SETUP");
    latex->SetTextSize(0.025);
    X = 0.50;
    Y = 0.33;
    offx = 0.028;
    offy = 0.027;
    latex->DrawTextNDC(X + 0.02, Y - offy * 0, "RUN MODE");
    latex->DrawTextNDC(X + 0.02, Y - offy * 1, "LOGIC");
    latex->DrawTextNDC(X + 0.02, Y - offy * 2, "SETUP");
    latex->DrawTextNDC(X + 0.02, Y - offy * 3, "PMT_HV");
    latex->DrawTextNDC(X + 0.02, Y - offy * 4, "SILICON");
    latex->DrawTextNDC(X + 0.02, Y - offy * 5, "DISCRI");
    latex->DrawTextNDC(X + 0.02, Y - offy * 6, "POSITION");
    latex->DrawTextNDC(X + 0.02, Y - offy * 8, "CRC ERROR");
    X = 0.55;
    Y = 0.33;
    latex->DrawTextNDC(X + 0.12, Y - offy * 0, GetChar("%d", a2_runmode));
    latex->DrawTextNDC(X + 0.12, Y - offy * 1, a2_V1495Load);
    latex->DrawTextNDC(X + 0.12, Y - offy * 2, a2_LogicMode);
    latex->DrawTextNDC(X + 0.12, Y - offy * 3, a2_PMTgain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 4, a2_silicongain);
    latex->DrawTextNDC(X + 0.12, Y - offy * 5, a2_Discriminator);
    latex->DrawTextNDC(X + 0.12, Y - offy * 6, a2_position);
    latex->DrawTextNDC(X + 0.12, Y - offy * 8, Form("%d", a2_ncrcerror));
  }

  // ++++++++ SAVE CANVASES ++++++++++++++++++++++++++++++++++++
  char ch_run[20] = "";
  char printname[256];
  if (savecanvas) {
    if (run > 0) {
      sprintf(ch_run, "run%05d", run);
    }
    sprintf(printname, "%s/%s_runinfo.gif", printpath, ch_run);
    c->Print(printname);
  }

  app.Run();

  return 0;
}

char* GetChar(const char* format, int val) {
  static char buf[256];
  sprintf(buf, format, val);
  return buf;
}

char* GetChar(const char* format, double val) {
  static char buf[256];
  sprintf(buf, format, val);
  return buf;
}

char* GetChar(const char* format, char* val) {
  static char buf[256];
  sprintf(buf, format, val);
  return buf;
}

string CutdownV1495Load(char csetup[]) {
  string ss = csetup;
  string cword;

  cword = "LHCF_";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(ss.find(cword) + cword.size(), ss.size() - (ss.find(cword) + cword.size()));
  }
  cword = ".rbf";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(0, ss.size() - (cword.size()));
  }
  return ss;
}

string CutdownGPIOLoad(char csetup[]) {
  string ss = csetup;
  string cword;

  // For setup_a1_****.sh
  cword = "load_";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(ss.find(cword) + cword.size(), ss.size() - (ss.find(cword) + cword.size()));
  }

  cword = ".csh";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(0, ss.size() - (cword.size()));
  }
  return ss;
}

string CutdownGPIOSetup(char csetup[]) {
  string ss = csetup;
  string cword;

  // For setup_a1_****.sh
  cword = "setup_a1_";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(ss.find(cword) + cword.size(), ss.size() - (ss.find(cword) + cword.size()));
  }
  // For setup_a1_****.sh
  cword = "setup_a2_";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(ss.find(cword) + cword.size(), ss.size() - (ss.find(cword) + cword.size()));
  }

  cword = ".sh";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(0, ss.size() - (cword.size()));
  }
  return ss;
}

string CutdownDiscri(char csetup[]) {
  string ss = csetup;
  string cword;

  cword = "discri.param.";
  if (ss.find(cword) != string::npos) {
    ss = ss.substr(ss.find(cword) + cword.size(), ss.size() - (ss.find(cword) + cword.size()));
  }
  return ss;
}
