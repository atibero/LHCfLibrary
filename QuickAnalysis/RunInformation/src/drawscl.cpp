#include <TApplication.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1F.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TRint.h>
#include <TStyle.h>

#include <cmath>
#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>

#include "LHCfRunInfoTable.h"
#include "LHCfRunInfoTable_add.h"
#include "gsetup.h"
using namespace std;

char* GetChar(const char* format, int val);
char* GetChar(const char* format, double val);

int main(int argc, char** argv) {
  gsetup();

  // ++++++++ Manage optioons +++++++++++++++++++++++++++++
  char datafile[256] = "";
  char runinfofile[256] = "";
  char printpath[256] = "graphs/";
  bool savecanvas = false;
  int run = -1;

  for (int ic = 0; ic < argc; ic++) {
    if (strcmp(argv[ic], "-i") == 0) {
      strcpy(datafile, argv[++ic]);
      strcpy(argv[ic], "");
