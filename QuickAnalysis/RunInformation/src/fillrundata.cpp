#include <iostream>
//#include <fstream>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iomanip>
using namespace std;

#include <LHCfRunInfoTable.h>
#include <LHCfRunInfoTable_add.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TLHCFDIPDATAH.h>
#include <TLHCFDIPDATAL.h>
#include <TROOT.h>
#include <TRint.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <gsetup.h>

#include <Arm1Params.hh>
#include <Arm2Params.hh>
#include <LHCfEvent.hh>
#include <Level0.hh>
#include <Level1.hh>
#include <Level2.hh>

using namespace nLHCf;

int main(int argc, char **argv) {
  // Manage Options
  int nevent = 100000;
  TString filename = "";  // calibrated data
  char *rundatafile = "rundatafile.root";
  char *runinfofile = "runinfofile.dat";
  int verbose = 1;
  int run;
  int subrun;
  for (int i = 1; i < argc; i++) {
    TString ss = argv[i];
    if (ss == "-i" || ss == "--input") {
      filename = argv[++i];
    }
    if (ss == "-rd" || ss == "--rundata") {
      rundatafile = argv[++i];
    }
    if (ss == "-ri" || ss == "--runinfo") {
      runinfofile = argv[++i];
    }
    if (ss == "-v") {
      verbose = atoi(argv[++i]);
    }
    if (ss == "-run") {
      run = atoi(argv[++i]);
    }
    if (ss == "-subrun") {
      subrun = atoi(argv[++i]);
    }
  }

  cout << "INTPUT FILENAME: " << filename << endl;
  cout << "RUNINFO FILENAME: " << runinfofile << endl;
  cout << "RUNDATA FILANAME: " << rundatafile << endl;

  gsetup();

  // Beam Configuration
  TH1F *h10 = new TH1F("hbconf_a1_all", "hbconf_a1_all", 4000, 0, 4000);
  TH1F *h11 = new TH1F("hbconf_a1_l2ta", "hbconf_a1_l2ta", 4000, 0, 4000);
  TH1F *h12 = new TH1F("hbconf_a2_all", "hbconf_a2_all", 4000, 0, 4000);
  TH1F *h13 = new TH1F("hbconf_a2_l2ta", "hbconf_a2_l2ta", 4000, 0, 4000);
  TH1F *h14 = new TH1F("hbconf_a1_bptx1", "hbconf_a1_bptx1", 4000, 0, 4000);
  TH1F *h15 = new TH1F("hbconf_a1_bptx2", "hbconf_a1_bptx2", 4000, 0, 4000);
  TH1F *h16 = new TH1F("hbconf_a2_bptx1", "hbconf_a2_bptx1", 4000, 0, 4000);
  TH1F *h17 = new TH1F("hbconf_a2_bptx2", "hbconf_a2_bptx2", 4000, 0, 4000);

  gROOT->cd();

  // Open input file (calibrate file)

  int nev = 0;
  TChain *tree = new TChain("LHCfEvents");
  tree->AddFile(filename);
  tree->SetCacheSize(10000000);
  LHCfEvent *ev = new LHCfEvent("event", "LHCfEvent");
  tree->SetBranchAddress("ev.", &ev);
  tree->AddBranchToCache("*");
  nev = tree->GetEntries();
  tree->GetEntry(0);
  // run = ev->run;
  // cout << "RUN number : " << setw(15) << run << endl;
  cout << "Total Events in the file: " << nev << endl;

  // Initialize valuables and functions
  TLHCFDIPDATAH *diph;
  TLHCFDIPDATAL *dipl;

  // Initialize runinfo table
  LHCfRunInfo *info;
  LHCfRunInfoTable *infotable = new LHCfRunInfoTable();
  if (strcmp(runinfofile, "") != 0) {
    infotable->ReadTable(runinfofile);
  }
  if (infotable->Get(run, subrun)) {
    info = infotable->Get(run, subrun);
    info->clear_nevents("ext_comments");
  } else {
    info = infotable->CreateInfo();
    info->clear();
  }

  // Initialize rundata file which including firsr slow control and dip, last event data
  TFile *ofile_rundata = NULL;
  if (strcmp(rundatafile, "") != 0) {
    ofile_rundata = new TFile(rundatafile, "UPDATE");
    gROOT->cd();
  }

  // LOOP
  if (tree->GetEntries() < nevent) {
    nevent = tree->GetEntries();
    cerr << "nevent : " << nevent << endl;
  }

  const int arm2_clkoffset = 1;
  int na1ev_all = 0;
  int na1ev_beam = 0;
  int na1ev_pede = 0;
  int na1ev_delay = 0;
  int na2ev_all = 0;
  int na2ev_beam = 0;
  int na2ev_pede = 0;
  int na2ev_delay = 0;
  int tt, tstart = -1, tend = -1, tstart_tmp = -1, tend_tmp = -1;
  int tend_tmpa1 = -1, tend_tmpa2 = -1;
  int lumiscan = -1;
  bool chk_diph_first = false, chk_dipl_first = false;
  int bunchpos = 0;
  int n_crcerror = 0;

  for (int i = 0; i < nev; i++) {
    // cout << "event  : " << i << endl;
    ev->Delete();
    tree->GetEntry(i);

    // Fill to run info
    if (ev->Check("lvl2_a1") == 1) {
      Level0<Arm1Params> *lvl0_a1 = (Level0<Arm1Params> *)ev->Get("lvl0_a1");
      Level2<Arm1Params> *lvl2_a1 = (Level2<Arm1Params> *)ev->Get("lvl2_a1");
      info->run = lvl0_a1->fRun;
      info->subrun = subrun;
    } else if (ev->Check("lvl2_a2") == 1) {
      Level0<Arm2Params> *lvl0_a2 = (Level0<Arm2Params> *)ev->Get("lvl0_a2");
      info->run = lvl0_a2->fRun;
      info->subrun = subrun;
    }
    // if(info->run<=0) {info->run = ev->GetRun();}
    //
    // Arm1
    if (ev->Check("lvl2_a1") == 1) {
      Level0<Arm1Params> *lvl0_a1 = (Level0<Arm1Params> *)ev->Get("lvl0_a1");
      Level1<Arm1Params> *lvl1_a1 = (Level1<Arm1Params> *)ev->Get("lvl1_a1");
      Level2<Arm1Params> *lvl2_a1 = (Level2<Arm1Params> *)ev->Get("lvl2_a1");
      Level0<Arm1Params> *ped0_a1 = (Level0<Arm1Params> *)ev->Get("ped0_a1");

      if (lvl2_a1->fEvent % 1000 == 0) {
        cout << "----------- A1 : Run " << lvl2_a1->fRun << "  "
             << "Event " << lvl2_a1->fEvent << endl;
      }

      if (ev->Check("lvl0_a1")) {
        info->a1_nevent++;
      }
      if (lvl0_a1->fEventFlag[0]) {
        if (lvl0_a1->IsShowerTrg()) {
          info->a1_nevent_L2T_Shower++;
        }
        if (lvl0_a1->IsSpecialTrg()) {
          info->a1_nevent_L2T_Special++;
        }
        if (lvl0_a1->IsPi0Trg()) {
          info->a1_nevent_L2T_Pi0++;
        }
        if (lvl0_a1->IsHighEMTrg()) {
          info->a1_nevent_L2T_HighEM++;
        }
        if (lvl0_a1->IsHadronTrg()) {
          info->a1_nevent_L2T_Hadron++;
        }
        if (lvl0_a1->IsZdcTrg()) {
          info->a1_nevent_L2T_Zdc++;
        }
        if (lvl0_a1->IsFcTrg()) {
          info->a1_nevent_L2T_FC++;
        }
        if (lvl0_a1->IsL1tTrg()) {
          info->a1_nevent_L2T_L1T++;
        }
      } else if (lvl0_a1->fEventFlag[1]) {
        if (lvl0_a1->IsPedestalTrg()) {
          info->a1_nevent_L2T_Pedestal++;
        }
      }
      if (lvl0_a1->IsLaserTrg()) {
        info->a1_nevent_laser++;
      }
    }
    // Arm2
    if (ev->Check("lvl2_a2") == 1) {
      Level0<Arm2Params> *lvl0_a2 = (Level0<Arm2Params> *)ev->Get("lvl0_a2");
      Level1<Arm2Params> *lvl1_a2 = (Level1<Arm2Params> *)ev->Get("lvl1_a2");
      Level2<Arm2Params> *lvl2_a2 = (Level2<Arm2Params> *)ev->Get("lvl2_a2");
      Level0<Arm2Params> *ped0_a2 = (Level0<Arm2Params> *)ev->Get("ped0_a2");

      if (lvl2_a2->fEvent % 1000 == 0) {
        cout << "----------- A2 : Run " << lvl2_a2->fRun << "  "
             << "Event " << lvl2_a2->fEvent << endl;
      }

      if (ev->Check("lvl0_a2")) {
        info->a2_nevent++;
      }
      if (lvl0_a2->fEventFlag[0]) {
        if (lvl0_a2->IsShowerTrg()) {
          info->a2_nevent_L2T_Shower++;
        }
        if (lvl0_a2->IsSpecialTrg()) {
          info->a2_nevent_L2T_Special++;
        }
        if (lvl0_a2->IsPi0Trg()) {
          info->a2_nevent_L2T_Pi0++;
        }
        if (lvl0_a2->IsHighEMTrg()) {
          info->a2_nevent_L2T_HighEM++;
        }
        if (lvl0_a2->IsHadronTrg()) {
          info->a2_nevent_L2T_Hadron++;
        }
        if (lvl0_a2->IsZdcTrg()) {
          info->a2_nevent_L2T_Zdc++;
        }
        if (lvl0_a2->IsFcTrg()) {
          info->a2_nevent_L2T_FC++;
        }
        if (lvl0_a2->IsL1tTrg()) {
          info->a2_nevent_L2T_L1T++;
        }
      } else if (lvl0_a2->fEventFlag[1]) {
        if (lvl0_a2->IsPedestalTrg()) {
          info->a2_nevent_L2T_Pedestal++;
        }
      }
      if (lvl0_a2->IsLaserTrg()) {
        info->a2_nevent_laser++;
      }
    }

    // Scaler Event  lvl0_a1->fScaler[16] = SCL0.SCL0[i] ?? Scaler1,2,3 in raw data(Scaler3 = SC)
    // if(ev->Check("a1scl")) { info->a1scl_nevent++;}
    // if(ev->Check("a2scl")) { info->a2scl_nevent++;}

    // SlowControl
    // if(ev->Check("sc"))    {
    //   Fill first slow control event to the rundata file.
    //  if(ofile_rundata && info->sc_nevent==0){
    //	ofile_rundata->cd();
    //	ev->Get("sc")->Write("sc_first",TObject::kOverwrite);
    //	gROOT->cd();
    // }
    //   info->sc_nevent++;
    //}

    if (ev->Check("diph")) {
      diph = (TLHCFDIPDATAH *)ev->Get("diph");
      // Fill first diph event to the rundata file.
      if (ofile_rundata && chk_diph_first == false && diph->GetData()->time) {
        ofile_rundata->cd();
        diph->Write("diph_first", TObject::kOverwrite);
        gROOT->cd();
        chk_diph_first = true;
      }
      // Check Luminosity Scan
      if (!strstr(diph->GetData()->Lumiscan.LumiScan_Status, "No Scan")) {
        if (lumiscan != 1) {
          lumiscan = diph->GetData()->Lumiscan.IP;
        }
      } else {
        if (lumiscan == -1) {
          lumiscan = 0;
        }
      }
      info->diph_nevent++;
    }
    if (ev->Check("dipl")) {
      dipl = (TLHCFDIPDATAL *)ev->Get("dipl");
      // Fill first dipl event to the rundata file.
      if (ofile_rundata && chk_dipl_first == false && dipl->GetData()->time) {
        ofile_rundata->cd();
        ev->Get("dipl")->Write("dipl_first", TObject::kOverwrite);
        gROOT->cd();
        chk_dipl_first = true;
      }
      info->dipl_nevent++;
    }
    if (ev->Check("dipvh")) {
      info->dipvh_nevent++;
    }

    // tt = ev->EventTime(); // <-not implimented
    if (ev->Check("lvl0_a1")) {
      Level0<Arm1Params> *lvl0_a1 = (Level0<Arm1Params> *)ev->Get("lvl0_a1");
      tt = lvl0_a1->fTime[0];
    }
    // else if(ev->Check("lvl0_a2") == 1) {
    //  Level0<Arm2Params> *lvl0_a2 = (Level0<Arm2Params> *) ev->Get("lvl0_a2");
    //  tt = lvl0_a2->fTime[0];
    //}

    if (tt > 0 && tstart < 0) {
      tstart = tt;
    }
    if (tt > 0) {
      tend = tt;
    }
    if (ev->Check("lvl0_a1")) {
      info->analysis_a1_nevent++;
      Level0<Arm1Params> *lvl0_a1 = (Level0<Arm1Params> *)ev->Get("lvl0_a1");
      if (lvl0_a1->IsShowerTrg()) {
        info->analysis_a1_nevent_L2TA++;
      }
    }
    if (ev->Check("lvl0_a2")) {
      info->analysis_a2_nevent++;
      Level0<Arm1Params> *lvl0_a2 = (Level0<Arm1Params> *)ev->Get("lvl0_a2");
      if (lvl0_a2->IsShowerTrg()) {
        info->analysis_a2_nevent_L2TA++;
      }
    }
    // if(ev->Check("lvl0_a1")) {
    //  Level0<Arm1Params> *lvl0_a1 = (Level0<Arm1Params> *) ev->Get("lvl0_a1");
    //  run = run;
    //}

    // run = ev->GetRun();
    // cerr << "na1ev_all  " << na1ev_all << endl;
    // cerr << "nevent     " << nevent << endl;
    // -------------- For Arm1 -------------------
    if (na1ev_all < nevent && ev->Check("lvl2_a1")) {  // && ev->CheckA1Trg()){// ?? trg , flag ??
      Level0<Arm1Params> *lvl0_a1 = (Level0<Arm1Params> *)ev->Get("lvl0_a1");
      Level0<Arm1Params> *ped0_a1 = (Level0<Arm1Params> *)ev->Get("ped0_a1");
      Level2<Arm1Params> *lvl2_a1 = (Level2<Arm1Params> *)ev->Get("lvl2_a1");
      tend_tmpa1 = (int)lvl0_a1->fTime[0];
      info->a1_nevent_l1t = lvl2_a1->fCounter[5];
      info->a1_nevent_l1t_enable = lvl2_a1->fCounter[6];

      // Bunch configration
      h10->Fill(lvl0_a1->BunchID());
      // bool IsL2TA = lvl0_a1->fFlag[0] & (0x0010);
      // bool IsL2TSpecial = lvl0_a1->fFlag[0] & (0x0020);
      // if(lvl0_a1->fEventFlag[0] && (IsL2TA || IsL2TSpecial)){
      // h11->Fill(lvl0_a1->fCounter[22]);
      //}
      bool bptx1 = lvl0_a1->IsBeam1();
      bool bptx2 = lvl0_a1->IsBeam2();
      if (lvl0_a1->fEventFlag[0] && bptx1) {
        h14->Fill(lvl2_a1->BunchID());
      }
      if (lvl0_a1->fEventFlag[0] && bptx2) {
        h15->Fill(lvl2_a1->BunchID());
      }
    }
    // -------------- END For Arm1 ----------------------

    // ---------------   For Arm2   ---------------------
    if (na2ev_all < nevent && ev->Check("lvl2_a2")) {  //&& ev->CheckA2Trg()
      Level0<Arm2Params> *lvl0_a2 = (Level0<Arm2Params> *)ev->Get("lvl0_a2");
      Level0<Arm2Params> *ped0_a2 = (Level0<Arm2Params> *)ev->Get("ped0_a2");
      Level2<Arm2Params> *lvl2_a2 = (Level2<Arm2Params> *)ev->Get("lvl2_a2");
      tend_tmpa2 = (int)lvl0_a2->fTime[0];
      info->a2_nevent_l1t = lvl2_a2->fCounter[5];
      info->a2_nevent_l1t_enable = lvl2_a2->fCounter[6];

      // Bunch configration
      // bool IsL2TA = lvl0_a2->fFlag[0] & (0x0010);
      // bool IsL2TSpecial = lvl0_a2->fFlag[0] & (0x0020);
      h12->Fill(lvl0_a2->BunchID());
      // if(lvl0_a2->fEventFlag[0] && (IsL2TA || IsL2TSpecial)){
      // h13->Fill(lvl0_a2->fCounter[22]);
      //}
      bool bptx1 = lvl0_a2->IsBeam1();
      bool bptx2 = lvl0_a2->IsBeam2();
      if (lvl0_a2->fEventFlag[0] && bptx1) {
        h16->Fill(lvl0_a2->BunchID());
      }
      if (lvl0_a2->fEventFlag[0] && bptx2) {
        h17->Fill(lvl0_a2->BunchID());
      }
    }
    // -----------   End for Arm2    ------------------
  }
  // End of Loop

  // Print Run Information
  if (tend_tmpa1 < tend_tmpa2) {
    tend_tmp = tend_tmpa1;
  } else {
    tend_tmp = tend_tmpa2;
  }
  tstart_tmp = tstart;

  time_t ts, te;
  ts = (time_t)tstart;
  te = (time_t)tend;
  cout << "DATA" << endl;
  cout << " START:  " << setw(15) << tstart << "   " << asctime(localtime(&ts)) << " END:    " << setw(15) << tend
       << "   " << asctime(localtime(&te)) << endl;
  ts = (time_t)tstart_tmp;
  te = (time_t)tend_tmp;
  cout << "CUT" << endl;
  cout << " START:  " << setw(15) << tstart_tmp << "   " << asctime(localtime(&ts)) << " END:    " << setw(15)
       << tend_tmp << "   " << asctime(localtime(&te)) << endl;

  // if(strcmp(runinfofile,"")){
  info->nevent = nevent;
  info->start_time = tstart;
  info->end_time = tend;
  info->analysis_cut_stime = tstart_tmp;
  info->analysis_cut_etime = tend_tmp;
  info->analysis_quality = 1;
  info->clear_beam("exc_comments");
  info->a2_ncrcerror = n_crcerror;

  int offset_beam1 = 176;
  int offset_beam2 = 176;

  const int range_displaced = 10;
  for (int i = 1; i < h10->GetNbinsX(); i++) {  // h10 = hbconf_a1_all
    if (h15->GetBinContent(i) > 0.1 &&          // h15 = hbconf_a1_bptx2
        h16->GetBinContent(i) > 0.1) {          // h16 = hbconf_a2_bptx1
      info->beam1_bunches[info->beam1_nbunches] = i + offset_beam1;
      info->beam2_bunches[info->beam2_nbunches] = i + offset_beam2;
      info->beam1_tag[info->beam1_nbunches] = LHCfRunInfo::COLLIDING;
      info->beam2_tag[info->beam2_nbunches] = LHCfRunInfo::COLLIDING;
      info->beam1_nbunches++;
      info->beam2_nbunches++;
    } else if (h15->GetBinContent(i) > 0.1) {
      info->beam2_bunches[info->beam2_nbunches] = i + offset_beam2;
      if (h16->Integral(i - range_displaced, i + range_displaced) > 0.1) {
        info->beam2_tag[info->beam2_nbunches] = LHCfRunInfo::DISPLACED;
      } else {
        info->beam2_tag[info->beam2_nbunches] = LHCfRunInfo::NONCOLLIDING;
      }
      info->beam2_nbunches++;
    } else if (h16->GetBinContent(i) > 0.1) {
      info->beam1_bunches[info->beam1_nbunches] = i + offset_beam1;
      if (h15->Integral(i - range_displaced, i + range_displaced) > 0.1) {
        info->beam1_tag[info->beam1_nbunches] = LHCfRunInfo::DISPLACED;
      } else {
        info->beam1_tag[info->beam1_nbunches] = LHCfRunInfo::NONCOLLIDING;
      }
      info->beam1_nbunches++;
    }
    if (info->beam1_nbunches >= LHCfRunInfo::NBUNCHES || info->beam2_nbunches >= LHCfRunInfo::NBUNCHES) {
      cerr << " !! Over the NBUNCHES !! " << endl;
      break;
    }
  }
  info->done_lumiscan = lumiscan;
  infotable->WriteTable(runinfofile, 1);
  //}

  // +++++ Print +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  cout << endl;
  cout << "Numbers of filled events." << endl
       << " Arm1 #All           : " << info->a1_nevent << endl
       << " Arm1 #BEAM or LASER : " << info->a1_nevent_L2T_Shower << endl
       << " Arm1 #PEDE          : " << info->a1_nevent_L2T_Pedestal << endl
       << " Arm2 #All           : " << info->a2_nevent << endl
       << " Arm2 #BEAM or LASER : " << info->a2_nevent_L2T_Shower << endl
       << " Arm2 #PEDE          : " << info->a2_nevent_L2T_Pedestal << endl;

  // +++++ Write last events (A1Cal1 & A1Cal2) ++++++++++++++++++
  if (ofile_rundata) {
    ofile_rundata->cd();
    if (ev->Check("lvl2_a1")) {
      Level2<Arm1Params> *lvl2_a1 = (Level2<Arm1Params> *)ev->Get("lvl2_a1");
      lvl2_a1->Write("a1cal1last", TObject::kOverwrite);
    }
    if (ev->Check("lvl2_a2")) {
      Level2<Arm2Params> *lvl2_a2 = (Level2<Arm2Params> *)ev->Get("lvl2_a2");
      lvl2_a2->Write("a2cal1last", TObject::kOverwrite);
    }
    ofile_rundata->Close();
  }

  return 0;
}
