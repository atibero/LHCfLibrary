#!/ bin / csh

set RUN = $argv[1] #RUN set COMFILE = $argv[2] #COMMENT FILE set OPTION = $argv[3] #FORCE OR NONE

    set WORKDIR = / home / lhcf / LHCf / Analysis / LHC / general / Tools / MakeHistograms / set BINDIR =
                      ${WORKDIR} / bin

                                       set TMP =`printf '%05d' $RUN`;
set INPUTFILE = "/mnt/lhcfds4/data3/Op2015/root_files/run${TMP}.root"

    set ANALBASEDIR = "/mnt/lhcfds4/data3/Op2015/QuickAnalysis" set INITRUNFILE =
        "${ANALBASEDIR}/.initrun.out" set QUICKANALDIR = "${ANALBASEDIR}/run${TMP}" set OUTPUTDIR =
            "${QUICKANALDIR}/hists_run${TMP}" set HISTFILE_NOR = "${OUTPUTDIR}/hists_run${TMP}.root" set HISTFILE_SUB =
                "${OUTPUTDIR}/hists_run${TMP}_sub.root" set PEDEFILE =
                    "${QUICKANALDIR}/pede_run${TMP}.root" set RUNINFOFILE =
                        "${QUICKANALDIR}/runinfo_run${TMP}.dat" set RUNDATAFILE =
                            "${QUICKANALDIR}/rundata_run${TMP}.root"

    set A1FELOG = "/mnt/lhcfds4/data3/Op2015/DAQlogs/LogFrontend1/frontend_arm1_log2" set A2FELOG =
        "/mnt/lhcfds4/data3/Op2015/DAQlogs/LogFrontend2/frontend_arm2_log2"

#Clear Comment
            / bin / rm -
        f $COMFILE;
echo "" > $COMFILE

#CHIECK EXISTANCE OF DATA FILE
              if (!-e $INPUTFILE) then echo $INPUTFILE " do not exist." exit endif

#MAKE DIRECTORY FOR QUICK ANALYSIS RESULTS
              if (!-d $QUICKANALDIR) then mkdir -
              p $QUICKANALDIR endif

#MAKE DIRECTORY FOR HISTOGRAMS
              if (!-d $OUTPUTDIR) then mkdir -
              p $OUTPUTDIR endif

                  if (!-e $HISTFILE_NOR || !-e $HISTFILE_SUB || $OPTION == "FORCE") then

                      cd $WORKDIR

#MAKE HISTOGRAM
                  / bin / rm -
              f $COMFILE;
echo "Making hists(nor)." > $COMFILE $BINDIR / adchists_Op2015 - i $INPUTFILE - o $HISTFILE_NOR - opede $PEDEFILE -
                                nodraw - q -
                                b

                                    / bin / rm -
                                f $COMFILE;
echo "Making hists(sub)." > $COMFILE $BINDIR / adchists_Op2015 - i $INPUTFILE - o $HISTFILE_SUB - opede $PEDEFILE -
                                ipede $PEDEFILE - runinfo $RUNINFOFILE - rundata $RUNDATAFILE - subd - nodraw - q -
                                b

#UPDATE RUN INFO
#FILL INFORMATION FROM FRONTEND LOGS ADN RUNDATA
                                        $BINDIR /
                                    filldaqinfo_Op2015 -
                                run $RUN - runinfo $RUNINFOFILE - rundata $RUNDATAFILE - a1log $A1FELOG -
                                a2log $A2FELOG

                                        cd $BINDIR
#MAKE GRAPHS
                                    / bin / rm -
                                f $COMFILE;
echo "Making graphs" >
    $COMFILE
#MAINLY FOR CALORIMETERS
                $BINDIR /
            drawhists_Op2015 -
        i $HISTFILE_NOR - print $OUTPUTDIR - printtag "rawL" - Low - Log - run $RUN - b - q $BINDIR / drawhists_Op2015 -
        i $HISTFILE_NOR - print $OUTPUTDIR - printtag "rawH" - High - Log - run $RUN - b -
        q
#MAINLY FOR SCIFI AND SILICON
                $BINDIR /
            drawhists_Op2015 -
        i $HISTFILE_SUB - print $OUTPUTDIR - printtag "subL" - Low - Log - run $RUN - b -
        q
#PEDESTAL
                $BINDIR /
            drawpede_Op2015 -
        i $PEDEFILE - print $OUTPUTDIR - run $RUN - b -
        q

#BEAM CONFIGURATION
                $BINDIR /
            drawbeamconfig_Op2015 -
        i $HISTFILE_SUB - print $OUTPUTDIR - run $RUN - q -
        b

#RUN INFO
                $BINDIR /
            drawruninfo_Op2015 -
        run $RUN - runinfo $RUNINFOFILE - print $OUTPUTDIR - q -
        b

#SCALERS
                $BINDIR /
            drawscl_Op2015 -
        run $RUN - runinfo $RUNINFOFILE - i $RUNDATAFILE - print $OUTPUTDIR - q -
        b

#DSC Check
                $BINDIR /
            drawdschists_Op2015-- run $RUN -
        i $HISTFILE_SUB-- print $OUTPUTDIR - q -
        b

#UPDATE INIT RUN NUMBER
            rm -
        f ${INITRUNFILE} echo $RUN >
    ${INITRUNFILE}

    endif
