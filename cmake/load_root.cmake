#-----------------#
#--- Load ROOT ---#
#-----------------#

# Require to set the version of ROOT 5 or 6

if (NOT USE_ROOT_VERSION)
    set(USE_ROOT_VERSION "ROOT6" CACHE STRING
            "OPTION FOR CHOOSING ROOT5 or ROOT6"
            FORCE)
endif (NOT USE_ROOT_VERSION)

#message ("USE_ROOT_VERSION = ${USE_ROOT_VERSION}")

if (USE_ROOT_VERSION STREQUAL "ROOT5")
    set(CMAKE_MODULE_PATH $ENV{ROOTSYS} $ENV{ROOTSYS}/etc/cmake ${CMAKE_MODULE_PATH})
else ()
    set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR} ${CMAKE_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})
endif ()

set(ENV{CMAKE_PREFIX_PATH} $ENV{ROOTSYS})
set(CMAKE_PREFIX_PATH $ENV{ROOTSYS})

find_package(ROOT REQUIRED COMPONENTS Spectrum EG)
include_directories(${ROOT_INCLUDE_DIR})
