
if ( ${CLUSTER} MATCHES "NAGOYA" OR ${CLUSTER} MATCHES "Nagoya")
    # Nagoya Computer configuration (lhcfs2 or crcpu0)
    file(GLOB SCRIPTS ${PROJECT_SOURCE_DIR}/Script/Nagoya/*.sh)
    foreach (script_src ${SCRIPTS})
        file(COPY ${script_src} DESTINATION ${PROJECT_BINARY_DIR})
    endforeach (script_src)

    file(GLOB SWITCHES ${PROJECT_SOURCE_DIR}/Script/Nagoya/*.inp)
    foreach (switch_src ${SWITCHES})
        file(COPY ${switch_src} DESTINATION ${PROJECT_BINARY_DIR})
    endforeach (switch_src)

    file(GLOB SWITCHES ${PROJECT_SOURCE_DIR}/Script/Nagoya/example*.py)
    foreach (switch_src ${SWITCHES})
        file(COPY ${switch_src} DESTINATION ${PROJECT_BINARY_DIR})
    endforeach (switch_src)   

    configure_file(
        ${PROJECT_SOURCE_DIR}/Script/Nagoya/setup_common.sh.in
        ${PROJECT_BINARY_DIR}/setup_common.sh
        @ONLY
    )

else ()
    # Italian Computer Configuration (INFN Farm and CNAF)
    file(GLOB SCRIPTS ${PROJECT_SOURCE_DIR}/Script/Italy/*.sh)
    foreach (script_src ${SCRIPTS})
        file(COPY ${script_src} DESTINATION ${PROJECT_BINARY_DIR})
    endforeach (script_src)

    file(GLOB SWITCHES ${PROJECT_SOURCE_DIR}/Script/Italy/*.inp)
    foreach (switch_src ${SWITCHES})
        file(COPY ${switch_src} DESTINATION ${PROJECT_BINARY_DIR})
    endforeach (switch_src)

endif ()
