#---------------------------#
#--- Set CMake variables ---#
#---------------------------#
# Set build type:
# None, Debug, Release, RelWithDebInfo, MinSizeRel
set (CMAKE_BUILD_TYPE Release)

# CMAKE_SOURCE_DIR: this is the directory, from which cmake was started, i.e. the top level source directory

# PROJECT_SOURCE_DIR: contains the full path to the root of your project source directory, i.e. to the nearest directory where CMakeLists.txt contains the project() command.

# CMAKE_BINARY_DIR: if you are building in-source, this is the same as CMAKE_SOURCE_DIR, otherwise this is the top level directory of your build tree
set (PROJECT_BINARY_DIR ${CMAKE_BINARY_DIR})

# EXECUTABLE_OUTPUT_PATH: set this variable to specify a common place where CMake should put all executable files (instead of CMAKE_CURRENT_BINARY_DIR)
set (EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

# LIBRARY_OUTPUT_PATH set this variable to specify a common place where CMake should put all libraries (instead of CMAKE_CURRENT_BINARY_DIR)
set (LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)

# Print compile commands
# set(CMAKE_VERBOSE_MAKEFILE on)

#-----------------------------#
#--- Set project variables ---#
#-----------------------------#

#--- Version number ---#
set (project_VERSION_MAJOR 1)
set (project_VERSION_MINOR 0)

#--- Dictionary library: directory and name ---#
set (Dict_DIR Dictionary)
set (Dict_LIB Dictionary)

#--- Reconstruction library: directory and name ---#
set (Rec_DIR Reconstruction)
set (Rec_LIB Reconstruction)

#--- Analysis library: directory and name ---#
set (An_DIR Analysis)
set (An_LIB Analysis)

#--- Reduction library: directory and name ---#
set (Red_DIR Reduction)
set (Red_LIB ReductionLib)