
set (TMP_GIT_HASH "@GIT_HASH@")
configure_file (
  ${PROJECT_SOURCE_DIR}/include/version.h.in
  ${PROJECT_BINARY_DIR}/include/version.h.in
)

# Add command to generate version header...
add_custom_command(
        OUTPUT
        ${PROJECT_BINARY_DIR}/include/version.h
        ALL
        COMMAND
        ${CMAKE_COMMAND}  -D IN_FILE=${PROJECT_BINARY_DIR}/include/version.h.in -D OUT_FILE=${PROJECT_BINARY_DIR}/include/version.h -P ${PROJECT_SOURCE_DIR}/cmake/GenerateVersion.cmake
        WORKING_DIRECTORY
        ${PROJECT_SOURCE_DIR}
)

# ...make sure it exists in filesystem (configuration WILL fail if it's not)...
file(WRITE ${PROJECT_BINARY_DIR}/include/version.h)

# ...add custom target to rebuild version.hpp every time
add_custom_target(
        generate_version_header
        ALL
        COMMAND
        ${CMAKE_COMMAND}  -D IN_FILE=${PROJECT_BINARY_DIR}/include/version.h.in -D OUT_FILE=${PROJECT_BINARY_DIR}/include/version.h -P ${PROJECT_SOURCE_DIR}/cmake/GenerateVersion.cmake
        WORKING_DIRECTORY
        ${PROJECT_SOURCE_DIR}
)
