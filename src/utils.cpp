#include "utils.h"

/* Automatically build getopt option-characters string */
void build_getopt_str(const struct option *opt, int nopt, char *str) {
  if (nopt * 2 >= STRLEN) {
    printf("Error: getopt optstring size too small, increase STRLEN\n");
    exit(EXIT_FAILURE);
  }
  int istr = 0;
  for (int iopt = 0; iopt < nopt; ++iopt) {
    char val = (char)opt[iopt].val;
    if (val != 0) {
      // add character to getopt string
      str[istr] = val;
      ++istr;
      // add ':' if argument is required
      if (opt[iopt].has_arg == required_argument) {
        str[istr] = ':';
        ++istr;
      }
    }
  }
  str[istr] = 0;  // terminate string
}
