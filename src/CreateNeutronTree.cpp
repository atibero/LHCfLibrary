#include <TRint.h>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "LHCfAn.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (must contains files named \"rec_run*****.root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = 1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = 1)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"analysed.root\")\n");
  printf("\t-t <tables_dir>\t\tSet tables directory (default = \"../Tables\")\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> and <subfill> are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t-O <option>\t\tOption (mandatory)\n");
  printf("\t-X <fill>\t\tSpecify non zero MC Beam X offset (default = 0)\n");
  printf("\t-Y <fill>\t\tSpecify non zero MC Beam Y offset (default = 0)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  string input_dir = "";
  int fill = -1;
  int subfill = -1;
  double mcoffx = 0.;
  double mcoffy = 0.;
  int first_run = 1;
  int last_run = 1;
  string output_file = "analysed.root";
  bool arm1_en = false;
  bool arm2_en = true;
  bool pho_en = true;
  bool neu_en = false;
  string option = "";
  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"tables-dir", required_argument, NULL, 't'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"<option>", required_argument, NULL, 'O'},
      {"mcoffx", required_argument, NULL, 'X'},
      {"mcoffy", required_argument, NULL, 'Y'},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 't':
        if (optarg) {
          Arm1Params::SetTableDirectory(optarg);
          Arm2Params::SetTableDirectory(optarg);
        }
        break;
      case 'O':
        if (optarg) option += Form(" %s ", optarg);
        break;
      case 'F':
        if (optarg) fill = atoi(optarg);
        break;
      case 'X':
        if (optarg) mcoffx = atof(optarg);
        break;
      case 'Y':
        if (optarg) mcoffy = atof(optarg);
        break;
      case 'S':
        if (optarg) subfill = atoi(optarg);
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }
  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Analysis ---*/
  Utils::SetVerbose(verbose);
  Arm1AnPars::SetFill(fill, subfill);
  Arm2AnPars::SetFill(fill, subfill);
  LHCfAn an_func;
  an_func.SetInputDir(input_dir.c_str());
  an_func.SetFirstRun(first_run);
  an_func.SetLastRun(last_run);
  an_func.SetOutputName(output_file.c_str());
  if (!arm1_en) an_func.DisableArm1();
  if (!arm2_en) an_func.DisableArm2();
  if (!pho_en) an_func.DisablePhoton();
  if (!neu_en) an_func.DisableNeutron();

  an_func.CreateOutputTree("eugenio", "NeutronLHC", option.c_str());

  // theApp.Run();

  return 0;
}
