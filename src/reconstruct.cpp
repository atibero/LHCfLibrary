#include <TError.h>
#include <TRint.h>
#include <TMath.h>

#include "Arm1AnPars.hh"
#include "Arm1CalPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2CalPars.hh"
#include "LHCfRec.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\t\tInput ROOT file (mandatory)\n");
  printf("\t-o <output>\t\t\tOutput ROOT file (default = \"reconstructed.root\")\n");
  printf("\t-f <event>\t\t\tFirst event to be reconstructed (default = 0)\n");
  printf("\t-l <event>\t\t\tLast event to be reconstructed (default = all)\n");
  printf("\t-p <mode>\t\t\tPosition reconstruction mode: \"fast\" or \"full\" (default = \"full\")\n");
  printf("\t-t <tables_dir>\t\t\tSet tables directory (default = \"../Tables\")\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> (and <subfill>) are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t--disable-arm1\t\t\tDisable Arm1 reconstruction (default = enabled)\n");
  printf("\t--disable-arm2\t\t\tDisable Arm2 reconstruction (default = enabled)\n");
  printf("\t--disable-photon\t\tDisable photon reconstruction (default = enabled)\n");
  printf("\t--disable-neutron\t\tDisable neutron reconstruction (default = enabled)\n");
  printf("\t--disable-fastpeaksearch\tDisable peak search in fast position reconstruction (default = enabled)\n");
  printf("\t--photon-fit-type\t\tPhoton position fit mode (default = %d for Arm1, %d for Arm2)\n",
         Arm1RecPars::kDefaultPhotonFitType, Arm2RecPars::kDefaultPhotonFitType);
  printf("\t--neutron-fit-type\t\tNeutron position fit mode (default = %d for Arm1, %d for Arm2)\n",
         Arm1RecPars::kDefaultNeutronFitType, Arm2RecPars::kDefaultNeutronFitType);
  printf("\t--photon-erec-type\t\tPhoton position fit mode (default = %d for Arm1, %d for Arm2)\n",
         Arm1RecPars::kDefaultPhotonErecType, Arm2RecPars::kDefaultPhotonErecType);
  // printf("\t--neutron-erec-type\t\tPhoton position fit mode (default = 0)\n");
  printf("\t--level0\t\t\tSave Level0 to output file (default = disabled)\n");
  printf("\t--level1\t\t\tSave Level1 to output file (default = disabled)\n");
  printf("\t--level2\t\t\tSave Level2 to output file (default = disabled)\n");
  printf("\t--level0cal\t\t\tSave Level0 (no silicon/GSO bars) to output file (default = disabled)\n");
  printf("\t--level1cal\t\t\tSave Level1 (no silicon/GSO bars) to output file (default = disabled)\n");
  printf("\t--level2cal\t\t\tSave Level2 (no silicon/GSO bars) to output file (default = disabled)\n");
  printf("\t--true-coor\t\t\tUse true instead of reconstructed coordinates\n");
  printf("\t--development\t\t\tCompute development variables\n");
  printf("\t-X <fill>\t\t\tSpecify non zero MC Beam X offset (default = 0)\n");
  printf("\t-Y <fill>\t\t\tSpecify non zero MC Beam Y offset (default = 0)\n");
  printf("\t-O <old_file>\t\t\tCompare with old tree in <old_file> (default = off)\n");
  printf("\t-v <verbose>\t\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_file = "";
  string output_file = "reconstructed.root";
  string table_folder = "";
  string old_file = "";
  bool old_flag = false;
  string pos_rec_mode = "full";
  int pho_fit_type = -1;
  int neu_fit_type = -1;
  int pho_erec_type = -1;
  // int neu_erec_type = 0;
  int first_ev = 0;
  int last_ev = -1;
  int fill = -1;
  int subfill = -1;
  double mcoffx = 0.;
  double mcoffy = 0.;
  bool arm1_en = true;
  bool arm2_en = true;
  bool pho_en = true;
  bool neu_en = true;
  bool fps_en = true;
  bool save_lvl0 = false;
  bool save_lvl1 = false;
  bool save_lvl2 = false;
  bool save_lvl0cal = false;
  bool save_lvl1cal = false;
  bool save_lvl2cal = false;
  bool true_coor = false;
  bool development = false;
  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first-ev", required_argument, NULL, 'f'},
      {"last-ev", required_argument, NULL, 'l'},
      {"pos-rec-mode", required_argument, NULL, 'p'},
      {"tables-dir", required_argument, NULL, 't'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"disable-arm1", no_argument, NULL, 1001},
      {"disable-arm2", no_argument, NULL, 1002},
      {"disable-photon", no_argument, NULL, 1003},
      {"disable-neutron", no_argument, NULL, 1004},
      {"disable-fastpeaksearch", no_argument, NULL, 1005},
      {"rotated-geometry", no_argument, NULL, 1100},
      {"photon-fit-type", required_argument, NULL, 1401},
      {"neutron-fit-type", required_argument, NULL, 1402},
      {"photon-erec-type", required_argument, NULL, 1403},
      //{"neutron-erec-type", required_argument, NULL, 1404},
      {"level0", no_argument, NULL, 1201},
      {"level1", no_argument, NULL, 1202},
      {"level2", no_argument, NULL, 1203},
      {"level0cal", no_argument, NULL, 1204},
      {"level1cal", no_argument, NULL, 1205},
      {"level2cal", no_argument, NULL, 1206},
      {"silcharge", no_argument, NULL, 1207},
      {"true-coor", no_argument, NULL, 1300},
      {"development", no_argument, NULL, 1400},
      {"mcoffx", required_argument, NULL, 'X'},
      {"mcoffy", required_argument, NULL, 'Y'},
      {"old", required_argument, NULL, 'O'},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_file = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'f':
        if (optarg) first_ev = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_ev = atoi(optarg);
        break;
      case 'p':
        if (optarg) pos_rec_mode = optarg;
        break;
      case 't':
        if (optarg) table_folder = optarg;
        break;
      case 'F':
        if (optarg) fill = TMath::Abs(atoi(optarg));
        break;
      case 'S':
        if (optarg) subfill = TMath::Abs(atoi(optarg));
        break;
      case 'X':
        if (optarg) mcoffx = atof(optarg);
        break;
      case 'Y':
        if (optarg) mcoffy = atof(optarg);
        break;
      case 'O':
        if (optarg) old_file = optarg;
        old_flag = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 1001:
        arm1_en = false;
        break;
      case 1002:
        arm2_en = false;
        break;
      case 1003:
        pho_en = false;
        break;
      case 1004:
        neu_en = false;
        break;
      case 1005:
        fps_en = false;
        break;
      case 1201:
        save_lvl0 = true;
        break;
      case 1202:
        save_lvl1 = true;
        break;
      case 1203:
        save_lvl2 = true;
        break;
      case 1204:
        save_lvl0cal = true;
        break;
      case 1205:
        save_lvl1cal = true;
        break;
      case 1206:
        save_lvl2cal = true;
        break;
      case 1300:
        true_coor = true;
        break;
      case 1400:
        development = true;
        break;
      case 1401:
        pho_fit_type = atoi(optarg);
        break;
      case 1402:
        neu_fit_type = atoi(optarg);
        break;
      case 1403:
        pho_erec_type = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  if (input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  gErrorIgnoreLevel = 3001;

  Utils::SetVerbose(verbose);

  /*--- Reconstruction ---*/

  LHCfRec rec_func;

  printf("\n");
  printf("===\n");
  printf("======= Reconstruction\n");
  printf("===========================\n");
  printf("\tInput file: %s\n", input_file.c_str());
  printf("\tOutput file: %s\n", output_file.c_str());
  printf("\tFirst event: %d\n", first_ev);
  printf("\tLast event: %d\n", last_ev >= 0 ? last_ev : 0);
  printf("\tFill number: %d [%d]\n", fill, subfill);
  printf("\tPosition reconstruction: %s\n", pos_rec_mode.c_str());
  printf("\tPosition fit type (photons): %d%s\n", pho_fit_type,
         (pho_fit_type >= 0 ? ""
                            : Form(" (auto-set to %d(Arm1),%d(Arm2))", Arm1RecPars::kDefaultPhotonFitType,
                                   Arm2RecPars::kDefaultPhotonFitType)));
  printf("\tPosition fit type (neutrons): %d%s\n", neu_fit_type,
         (neu_fit_type >= 0 ? ""
                            : Form(" (auto-set to %d(Arm1),%d(Arm2))", Arm1RecPars::kDefaultNeutronFitType,
                                   Arm2RecPars::kDefaultNeutronFitType)));
  printf("\tMH Energy reconstruction type (photons): %d%s\n", pho_erec_type,
         (pho_erec_type >= 0 ? ""
                             : Form(" (auto-set to %d(Arm1),%d(Arm2))", Arm1RecPars::kDefaultPhotonErecType,
                                    Arm2RecPars::kDefaultPhotonErecType)));
  // printf("\tMH Energy reconstruction type (neutrons): %d%s\n", neu_erec_type, (neu_erec_type>=0 ? "": "(auto-set to
  // 2(arm1),1(arm2))"));
  rec_func.SetInputName(input_file.c_str());
  rec_func.SetOutputName(output_file.c_str());
  rec_func.SetPosRecMode(pos_rec_mode.c_str());
  rec_func.SetPhotonFitType(pho_fit_type);
  rec_func.SetNeutronFitType(neu_fit_type);
  rec_func.SetPhotonErecType(pho_erec_type);
  // rec_func.SetNeutronErecType(neu_erec_type);

  printf("\tOptions:\n");
  if (!arm1_en) {
    printf("\t\tArm1 disabled\n");
    rec_func.DisableArm1();
  }
  if (!arm2_en) {
    printf("\t\tArm2 disabled\n");
    rec_func.DisableArm2();
  }
  if (!pho_en) {
    printf("\t\tPhoton disabled\n");
    rec_func.DisablePhoton();
  }
  if (!neu_en) {
    printf("\t\tNeutron disabled\n");
    rec_func.DisableNeutron();
  }
  if (!fps_en) {
    printf("\t\tFastPeakSearch disabled\n");
    rec_func.DisableFastPeakSearch();
  }
  if (save_lvl0) {
    printf("\t\tSaving lvl0\n");
    rec_func.SaveLevel0();
  }
  if (save_lvl1) {
    printf("\t\tSaving lvl1\n");
    rec_func.SaveLevel1();
  }
  if (save_lvl2) {
    printf("\t\tSaving lvl2\n");
    rec_func.SaveLevel2();
  }
  if (save_lvl0cal) {
    printf("\t\tSaving lvl0cal\n");
    rec_func.SaveLevel0Cal();
  }
  if (save_lvl1cal) {
    printf("\t\tSaving lvl1cal\n");
    rec_func.SaveLevel1Cal();
  }
  if (save_lvl2cal) {
    printf("\t\tSaving lvl2cal\n");
    rec_func.SaveLevel2Cal();
  }
  if (true_coor) {
    printf("\t\tUsing true coordinates\n");
    rec_func.UseTrueCoo();
  }
  if (development) {
    printf("\t\tCompute development variables\n");
    rec_func.Development();
  }
  if (old_flag) {
    printf("\t\tSetting old flag\n");
    rec_func.SetOldName(old_file.c_str());
  }
  printf("\n");

  if (table_folder != "") {
    Arm1Params::SetTableDirectory(table_folder.c_str());
    Arm2Params::SetTableDirectory(table_folder.c_str());
  }
  if (arm1_en) printf("\tArm1 table folder: %s\n", Arm1Params::GetTableDirectory().Data());
  if (arm2_en) printf("\tArm2 table folder: %s\n", Arm2Params::GetTableDirectory().Data());
  Arm1CalPars::SetFill(fill, subfill);
  Arm2CalPars::SetFill(fill, subfill);
  Arm1AnPars::SetFill(fill, subfill);
  Arm2AnPars::SetFill(fill, subfill);

  rec_func.Reconstruction(first_ev, last_ev);

  // theApp.Run();

  return 0;
}
