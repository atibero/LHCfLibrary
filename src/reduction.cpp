#include <iomanip>
#include <iostream>
#include <string>

using namespace std;

#include <TRint.h>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "LHCfReduction.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (mandatory)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"reduction.root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = -1)\n");
  printf("\t\t\t\t*** If either <first>=-1 or <last>=-1, all ROOT  ***\n");
  printf("\t\t\t\t*** files from input directory are loaded.       ***\n");
  printf("\t\t\t\t*** Otherwise, <input> must contains files named ***\n");
  printf("\t\t\t\t*** \"rec_runXXXXX.root\" (XXXXX=<first>...<last>) ***\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> and <subfill> are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t-n <number>\t\tLimitation of #event (default = -1)\n");
  printf("\t-T <filter>\t\tSelection by Trigger Flag (default = 0xFFFF(no selection))\n");
  printf("\t--disable-arm1\t\tDisable Arm1 analysis (default = enabled)\n");
  printf("\t--disable-arm2\t\tDisable Arm2 analysis (default = enabled)\n");
  printf("\t--disable-photon\tDisable photon analysis (default = enabled)\n");
  printf("\t--disable-neutron\tDisable neutron analysis (default = enabled)\n");
  printf("\t--disable-pion\t\tDisable pion analysis (default = enabled)\n");
  printf("\t--enable-true\t\tEnable MC truth (default = disabled)\n");
  printf("\t--enable-lvl2\t\tEnable lvl2 (default = disabled)\n");
  printf("\t--enable-lvl2pos\t\tEnable lvl2 for position detector information (default = disabled)\n");
  printf("\t--disable-modification\t\tEnergy Rescaling (default = enabled)\n");
  printf("\t--disable-bptx-cut\t\tDisable BPTX cut, must be done for MC!!! (default = enabled)\n");
  printf("\t-E <E_scale_factor>\tScale factor for photon energy (default = 1)\n");
  printf("\t-X <beam_centre_offset_x>\tShift beam centre along x (default = 0)\n");
  printf("\t-Y <beam_centre_offset_x>\tShift beam centre along y (default = 0)\n");
  printf("\t-P <pid_efficiency>\tSet Pi0 PID selection efficiency (default = 90, available: 85, 90, 95)\n");
  printf("\t-c <text>\t\tcomments are save into output file.\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
  printf("\n");
  printf("Bitmap for Trigger:\n");
  printf("\tShower Trigger\t\t0x001\n");
  printf("\tPi0 Trigger\t\t0x002\n");
  printf("\tHighEM Trigger\t\t0x004\n");
  printf("\tHadron Trigger\t\t0x008\n");
  printf("\tZDC Trigger\t\t0x010\n");
  printf("\tFC Trigger\t\t0x020\n");
  printf("\tL1T Trigger\t\t0x040\n");
  printf("\tLaser Trigger\t\t0x080\n");
  printf("\tPedestal Trigger\t0x100\n");
}

/****** Main ******/
int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_dir = "";
  string output_file = "analysed.root";
  string table_folder = "";
  Int_t first_run = -1;
  Int_t last_run = -1;
  Int_t fill = -1;
  Int_t subfill = -1;
  Int_t evlimit = -1;
  UInt_t trgfilter = 0xFFFF;
  bool arm1_en = true;
  bool arm2_en = true;
  bool pho_en = true;
  bool neu_en = true;
  bool pion_en = true;
  bool true_en = false;
  bool lvl2_en = false;
  bool lvl2pos_en = false;
  bool modification_en = true;  // energy rescale
  bool bptx_en = true;
  Double_t ene_fact = 1.;
  Double_t bc_off_x = 0.;
  Double_t bc_off_y = 0.;
  Int_t pi0_pid_eff = 90;
  Int_t verbose = 1;
  string comment = "";

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"tables-dir", required_argument, NULL, 't'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"evlimit", required_argument, NULL, 'n'},
      {"trigger", required_argument, NULL, 'T'},
      {"disable-arm1", no_argument, NULL, 1001},
      {"disable-arm2", no_argument, NULL, 1002},
      {"disable-photon", no_argument, NULL, 1003},
      {"disable-neutron", no_argument, NULL, 1004},
      {"disable-pion", no_argument, NULL, 1005},
      {"enable-true", no_argument, NULL, 1008},
      {"enable-lvl2", no_argument, NULL, 1006},
      {"enable-lvl2pos", no_argument, NULL, 1010},
      {"disable-modification", no_argument, NULL, 1007},
      {"disable-bptx-cut", no_argument, NULL, 1009},
      {"scale-factor", required_argument, NULL, 'E'},
      {"bc-offset-x", required_argument, NULL, 'X'},
      {"bc-offset-y", required_argument, NULL, 'Y'},
      {"pion-pid-eff", required_argument, NULL, 'P'},
      {"comment", required_argument, NULL, 'c'},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 't':
        if (optarg) table_folder = optarg;
        break;
      case 'F':
        if (optarg) fill = atoi(optarg);
        break;
      case 'S':
        if (optarg) subfill = atoi(optarg);
        break;
      case 'n':
        if (optarg) evlimit = atoi(optarg);
        break;
      case 'T':
        if (optarg) trgfilter = (UInt_t)(stod(optarg) + 0.001);
        break;
      case 'E':
        if (optarg) ene_fact = atof(optarg);
        break;
      case 'X':
        if (optarg) bc_off_x = atof(optarg);
        break;
      case 'Y':
        if (optarg) bc_off_y = atof(optarg);
        break;
      case 'P':
        if (optarg) pi0_pid_eff = atof(optarg);
        break;
      case 'c':
        if (optarg) comment = optarg;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 1001:
        arm1_en = false;
        break;
      case 1002:
        arm2_en = false;
        break;
      case 1003:
        pho_en = false;
        break;
      case 1004:
        neu_en = false;
        break;
      case 1005:
        pion_en = false;
        break;
      case 1008:
        true_en = true;
        break;
      case 1006:
        lvl2_en = true;
        break;
      case 1010:
        lvl2pos_en = true;
        break;
      case 1007:
        modification_en = false;
        break;
      case 1009:
        bptx_en = false;
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Set the comment ---*/
  string com = "";
  com += "Reduction \n";
  com += Form("%s Version %d.%d\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  com += Form("Git Hash: %s", GIT_HASH);
  com += Form("Input directory: %s\n", input_dir.c_str());
  com += Form("Output file: %s\n", output_file.c_str());
  com += Form("Filter: %x\n", trgfilter);
  com += Form("Modification: %s\n", (modification_en ? "enabled" : "disabled"));
  com += comment;

  /*--- Reduction ---*/
  Utils::SetVerbose(verbose);

  printf("\n");
  printf("===\n");
  printf("======= Reduction\n");
  printf("===========================\n");
  printf("\tInput firectory: %s\n", input_dir.c_str());
  printf("\tOutput file: %s\n", output_file.c_str());
  printf("\tFirst run: %d\n", first_run);
  printf("\tLast run: %d\n", last_run);
  printf("\tFill number: %d [%d]\n", fill, subfill);
  LHCfReduction red_func;
  red_func.SetInputDir(input_dir.c_str());
  red_func.SetFirstRun(first_run);
  red_func.SetLastRun(last_run);
  red_func.SetOutputName(output_file.c_str());

  printf("\tOptions:\n");
  if (!arm1_en) {
    printf("\t\tArm1 disabled\n");
    red_func.DisableArm1();
  }
  if (!arm2_en) {
    printf("\t\tArm2 disabled\n");
    red_func.DisableArm2();
  }
  if (!pho_en) {
    printf("\t\tPhoton disabled\n");
    red_func.DisablePhoton();
  }
  if (!neu_en) {
    printf("\t\tNeutron disabled\n");
    red_func.DisableNeutron();
  }
  if (!pion_en) {
    printf("\t\tPion disabled\n");
    red_func.DisablePi0();
  }
  if (true_en) {
    printf("\t\tMC truth enabled\n");
    red_func.EnableTrue();
  }
  if (!modification_en) {
    printf("\t\tModification disabled\n");
    red_func.DisableModification();
  }
  if (!bptx_en) {
    printf("\t\tBPTX cut disabled\n");
    red_func.DisablefBptxCut();
  }
  if (lvl2_en) {
    printf("\t\tSaving lvl2\n");
    red_func.EnableLvl2();
  }
  if (lvl2pos_en) {
    printf("\t\tSaving lvl2 position detector\n");
    red_func.EnableLvl2Pos();
  }
  printf("\n");

  if (table_folder != "") {
    Arm1Params::SetTableDirectory(table_folder.c_str());
    Arm2Params::SetTableDirectory(table_folder.c_str());
  }
  if (arm1_en) printf("\tArm1 table folder: %s\n", Arm1Params::GetTableDirectory().Data());
  if (arm2_en) printf("\tArm2 table folder: %s\n", Arm2Params::GetTableDirectory().Data());
  Arm1AnPars::SetFill(fill, subfill);
  Arm2AnPars::SetFill(fill, subfill);

  red_func.SetTriggerFilter(trgfilter);
  red_func.SetPhotonEnergyFactor(ene_fact);
  red_func.SetBeamCentreOffset(bc_off_x, bc_off_y);
  red_func.SetPionPidEff(pi0_pid_eff);
  red_func.SetComment(com.c_str());

  red_func.Reduction(0, evlimit);

  // theApp.Run();
  return 0;
}
