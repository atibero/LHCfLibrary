#include <TRint.h>
#include <TMath.h>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "LHCfCalib.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\tInput ROOT file (mandatory)\n");
  printf("\t-o <output>\tOutput ROOT file (default = \"calibrated.root\")\n");
  printf("\t-f <event>\tFirst event to be processed (default = 0)\n");
  printf("\t-l <event>\tLast event to be processed (default = all)\n");
  printf("\t-t <tables_dir>\tSet tables directory (default = \"../Tables\")\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> (and <subfill>) are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t--disable-arm1\tDisable Arm1 calibration (default = enabled)\n");
  printf("\t--disable-arm2\tDisable Arm2 calibration (default = enabled)\n");
  printf("\t--ped\t\tProcess and save pedestal events to output file (default = disabled)\n");
  printf("\t--level0\tSave Level0 to output file (default = disabled)\n");
  printf("\t--level1\tSave Level1 to output file (default = disabled)\n");
  printf("\t--level0cal\tSave Level0 (no silicon/GSO bars) to output file (default = disabled)\n");
  printf("\t--level1cal\tSave Level1 (no silicon/GSO bars) to output file (default = disabled)\n");
  printf("\t--level2cal\tSave Level2 (no silicon/GSO bars) to output file (default = disabled)\n");
  printf("\t--hist\t\tSave pedestal mean/RMS histograms to output file (default = disabled)\n");
  printf("\t--realign-arm2\tRealign silicon to calorimeter events (default = disabled)\n");
  printf("\t--average-pedestal\tSubtract pedestal average instead of delayed gate (default = disabled)\n");
  printf("\t--iterate-pedestal\tEmploy iterative procedure in pedestal computation (default = disabled)\n");
  printf("\t-v <verbose>\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\tShow usage\n");
}

int main(int argc, char **argv) {
  /*--- Default values ---*/
  string input_file = "";
  string output_file = "calibrated.root";
  string table_folder = "";
  int first_ev = 0;
  int last_ev = -1;
  int fill = -1;
  int subfill = -1;
  int verbose = 1;
  bool arm1_en = true;
  bool arm2_en = true;
  bool save_ped = false;
  bool save_lvl0 = false;
  bool save_lvl1 = false;
  bool save_lvl0cal = false;
  bool save_lvl1cal = false;
  bool save_lvl2cal = false;
  bool save_ped_hist = false;
  bool realign_arm2 = false;
  bool average_pedestal = false;
  bool iterate_pedestal = false;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first-ev", required_argument, NULL, 'f'},
      {"last-ev", required_argument, NULL, 'l'},
      {"tables-dir", required_argument, NULL, 't'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"disable-arm1", no_argument, NULL, 1001},
      {"disable-arm2", no_argument, NULL, 1002},
      {"ped", no_argument, NULL, 1010},
      {"level0", no_argument, NULL, 1011},
      {"level1", no_argument, NULL, 1012},
      {"level0cal", no_argument, NULL, 1013},
      {"level1cal", no_argument, NULL, 1014},
      {"level2cal", no_argument, NULL, 1015},
      {"hist", no_argument, NULL, 1030},
      {"realign-arm2", no_argument, NULL, 1040},
      {"average-pedestal", no_argument, NULL, 1050},
      {"iterate-pedestal", no_argument, NULL, 1060},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_file = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'f':
        if (optarg) first_ev = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_ev = atoi(optarg);
        break;
      case 't':
        if (optarg) table_folder = optarg;
        break;
      case 'F':
        if (optarg) fill = TMath::Abs(atoi(optarg));
        break;
      case 'S':
        if (optarg) subfill = TMath::Abs(atoi(optarg));
        break;
      case 1001:
        arm1_en = false;
        break;
      case 1002:
        arm2_en = false;
        break;
      case 1010:
        save_ped = true;
        break;
      case 1011:
        save_lvl0 = true;
        break;
      case 1012:
        save_lvl1 = true;
        break;
      case 1013:
        save_lvl0cal = true;
        break;
      case 1014:
        save_lvl1cal = true;
        break;
      case 1015:
        save_lvl2cal = true;
        break;
      case 1030:
        save_ped_hist = true;
        break;
      case 1040:
        realign_arm2 = true;
        break;
      case 1050:
        average_pedestal = true;
        break;
      case 1060:
        iterate_pedestal = true;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  if (input_file == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  Utils::SetVerbose(verbose);

  /*--- Calibration ---*/

  LHCfCalib calib_func;

  printf("\n");
  printf("===\n");
  printf("======= Calibration\n");
  printf("===========================\n");
  printf("\tInput file: %s\n", input_file.c_str());
  printf("\tOutput file: %s\n", output_file.c_str());
  printf("\tFirst event: %d\n", first_ev);
  printf("\tLast event: %d\n", last_ev >= 0 ? last_ev : 0);
  printf("\tFill number: %d [%d]\n", fill, subfill);
  calib_func.SetInputName(input_file.c_str());
  calib_func.SetOutputName(output_file.c_str());

  printf("\tOptions:\n");
  if (!arm1_en) {
    printf("\t\tArm1 disabled\n");
    calib_func.DisableArm1();
  }
  if (!arm2_en) {
    printf("\t\tArm2 disabled\n");
    calib_func.DisableArm2();
  }
  if (save_ped) {
    printf("\t\tSaving pedestal\n");
    calib_func.SavePedestal();
  }
  if (save_lvl0) {
    printf("\t\tSaving lvl0\n");
    calib_func.SaveLevel0();
  }
  if (save_lvl1) {
    printf("\t\tSaving lvl1\n");
    calib_func.SaveLevel1();
  }
  if (save_lvl0cal) {
    printf("\t\tSaving calorimeter lvl0\n");
    calib_func.SaveLevel0Cal();
  }
  if (save_lvl1cal) {
    printf("\t\tSaving calorimeter lvl1\n");
    calib_func.SaveLevel1Cal();
  }
  if (save_lvl2cal) {
    printf("\t\tSaving calorimeter lvl2\n");
    calib_func.SaveLevel2Cal();
  }
  if (save_ped_hist) {
    printf("\t\tSaving pedestal histograms\n");
    calib_func.DoPedHist();
  }
  if (realign_arm2) {
    printf("\t\tRealigning silicon events\n");
    calib_func.RealignSilicon();
  }
  if (average_pedestal) {
    printf("\t\tAvoiding delayed gate subtraction\n");
    calib_func.AveragePedestal();
  }
  if (iterate_pedestal) {
    printf("\t\tUsing iterative procedure in pedestal computation\n");
    calib_func.IteratePedestal();
  }
  printf("\n");

  if (table_folder != "") {
    Arm1Params::SetTableDirectory(table_folder.c_str());
    Arm2Params::SetTableDirectory(table_folder.c_str());
  }
  if (arm1_en) printf("\tArm1 table folder: %s\n", Arm1Params::GetTableDirectory().Data());
  if (arm2_en) printf("\tArm2 table folder: %s\n", Arm2Params::GetTableDirectory().Data());
  Arm1CalPars::SetFill(fill, subfill);
  Arm2CalPars::SetFill(fill, subfill);

  calib_func.Calibration(first_ev, last_ev);

  // theApp.Run();

  return 0;
}
