#include <TRint.h>

#include "LHCfAn.hh"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

void usage(char *argv0) {
  printf("\nUsage: %s [options]\n\n", argv0);
  printf("Available options:\n");
  printf("\t-i <input>\t\tInput directory (mandatory)\n");
  printf("\t-o <output>\t\tOutput ROOT file (default = \"analysed.root\")\n");
  printf("\t-f <first>\t\tFirst run to be analysed (default = -1)\n");
  printf("\t-l <last>\t\tLast run to be analysed (default = -1)\n");
  printf("\t\t\t\t*** If either <first>=-1 or <last>=-1, all ROOT  ***\n");
  printf("\t\t\t\t*** files from input directory are loaded.       ***\n");
  printf("\t\t\t\t*** Otherwise, <input> must contains files named ***\n");
  printf("\t\t\t\t*** \"rec_runXXXXX.root\" (XXXXX=<first>...<last>) ***\n");
  printf("\t-F <fill>\t\tAutomatically set parameters for specified fill (mandatory)\n");
  printf("\t-S <subfill>\t\tAutomatically set parameters for specified subfill (mandatory/optional)\n");
  printf("\t\t\t\t*** <fill> and <subfill> are mandatory arguments ***\n");
  printf("\t\t\t\t*** They are ignored if operation is = SPS2015,2021,2022 ***\n");
  printf("\t\t\t\t*** Otherwise you must specify both of them accordingly ***\n");
  printf("\t\t\t\t*** Or set <fill>=0 to select default dummy configuration ***\n");
  printf("\t--disable-arm1\t\tDisable Arm1 analysis (default = enabled)\n");
  printf("\t--disable-arm2\t\tDisable Arm2 analysis (default = enabled)\n");
  printf("\t--disable-photon\tDisable photon analysis (default = enabled)\n");
  printf("\t--disable-neutron\tDisable neutron analysis (default = enabled)\n");
  printf("\t--disable-pion\t\tDisable pion analysis (default = enabled)\n");
  printf("\t--save-tree\tSave L90 photon tree for template fit\n");
  printf("\t--photon-cuts <cuts>\tSet photon event cuts\n");
  printf("\t\t\t\t(default = \"pid energy position rapidity multi-hit bptx\")\n");
  printf("\t--neutron-cuts <cuts>\tSet neutron event cuts\n");
  printf("\t\t\t\t(default = \"pid energy position rapidity bptx\")\n");
  printf("\t-b <border>\t\tSet fiducial border for position cut (default = 2 [mm])\n");
  printf("\t-t <tables_dir>\t\tSet tables directory (default = \"../Tables\")\n");
  printf("\t-O <old_dir>\t\tCompare with old tree in <old_dir> (default = off)\n");
  printf("\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  printf("\t\t\t\t\t0 -> only errors\n");
  printf("\t\t\t\t\t1 -> some info\n");
  printf("\t\t\t\t\t2 -> debug\n");
  printf("\t\t\t\t\t3 -> all debug\n");
  printf("\t-h\t\t\tShow usage\n");
}

int main(int argc, char **argv) {
  string input_dir = "";
  int first_run = -1;
  int last_run = -1;
  string output_file = "analysed.root";
  int fill = -1;
  int subfill = -1;
  string old_dir = "";
  bool arm1_en = true;
  bool arm2_en = true;
  bool pho_en = true;
  bool neu_en = true;
  bool pion_en = true;
  bool modification_en = true;  // energy rescale
  bool tree_en = false;         //
  string pho_defs = "pid energy position rapidity multi-hit bptx";
  string neu_defs = "pid energy position rapidity multi-hit bptx";
  string pho_cuts = "";
  string neu_cuts = "";
  double border = 2.;
  int verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"input", required_argument, NULL, 'i'},
      {"output", required_argument, NULL, 'o'},
      {"first", required_argument, NULL, 'f'},
      {"last", required_argument, NULL, 'l'},
      {"fill", required_argument, NULL, 'F'},
      {"subfill", required_argument, NULL, 'S'},
      {"disable-arm1", no_argument, NULL, 1001},
      {"disable-arm2", no_argument, NULL, 1002},
      {"disable-photon", no_argument, NULL, 1003},
      {"disable-neutron", no_argument, NULL, 1004},
      {"disable-pion", no_argument, NULL, 1005},
      {"disable-modification", no_argument, NULL, 1006},
      {"save-tree", no_argument, NULL, 1007},
      {"photon-cuts", required_argument, NULL, 1008},
      {"neutron-cuts", required_argument, NULL, 1009},
      {"border", required_argument, NULL, 'b'},
      {"tables-dir", required_argument, NULL, 't'},
      {"old", required_argument, NULL, 'O'},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'i':
        if (optarg) input_dir = optarg;
        break;
      case 'o':
        if (optarg) output_file = optarg;
        break;
      case 'f':
        if (optarg) first_run = atoi(optarg);
        break;
      case 'l':
        if (optarg) last_run = atoi(optarg);
        break;
      case 'F':
        if (optarg) fill = atoi(optarg);
        break;
      case 'S':
        if (optarg) subfill = atoi(optarg);
        break;
      case 'O':
        if (optarg) old_dir = optarg;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 1001:
        arm1_en = false;
        break;
      case 1002:
        arm2_en = false;
        break;
      case 1003:
        pho_en = false;
        break;
      case 1004:
        neu_en = false;
        break;
      case 1005:
        pion_en = false;
        break;
      case 1006:
        modification_en = false;
        break;
      case 1007:
        tree_en = true;
        break;
      case 1008:
        if (optarg) {
          pho_cuts += Form(" %s ", optarg);
          ;
          pho_defs = "";
        }
        break;
      case 1009:
        if (optarg) {
          neu_cuts += Form(" %s ", optarg);
          ;
          neu_defs = "";
        }
        break;
      case 'b':
        if (optarg) border = atof(optarg);
        break;
      case 't':
        if (optarg) {
          Arm1Params::SetTableDirectory(optarg);
          Arm2Params::SetTableDirectory(optarg);
        }
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (pho_defs != "") pho_cuts = pho_defs;
  if (neu_defs != "") neu_cuts = neu_defs;

  if (verbose >= 2) {
    printf("nopt: = %d\n", nopt);
    printf("opt_str: \"%s\"\n", opt_str);
  }

  if (input_dir == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    printf("\n*** %s v%d.%d ***\n\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  /*--- Initialize ROOT application ---*/
  // TRint theApp("App", NULL, NULL, 0, 0, kTRUE);
  TApplication theApp("App", NULL, NULL, 0, 0);

  /*--- Analysis ---*/
  Utils::SetVerbose(verbose);
  Arm1AnPars::SetFill(fill, subfill);
  Arm2AnPars::SetFill(fill, subfill);

  printf("\n");
  printf("===\n");
  printf("======= Analysis\n");
  printf("===========================\n");
  printf("\tInput firectory: %s\n", input_dir.c_str());
  printf("\tOutput file: %s\n", output_file.c_str());
  printf("\tFirst run: %d\n", first_run);
  printf("\tLast run: %d\n", last_run);
  printf("\tFill number: %d [%d]\n", fill, subfill);
  printf("\tFiducial border: %f\n", border);
  Arm1AnPars::SetFiducialBorder(border);
  Arm2AnPars::SetFiducialBorder(border);
  LHCfAn an_func;
  an_func.SetInputDir(input_dir.c_str());
  an_func.SetFirstRun(first_run);
  an_func.SetLastRun(last_run);
  an_func.SetOutputName(output_file.c_str());
  if (!arm1_en) {
    printf("\t\tArm1 disabled\n");
    an_func.DisableArm1();
  }
  if (!arm2_en) {
    printf("\t\tArm2 disabled\n");
    an_func.DisableArm2();
  }
  if (!pho_en) {
    printf("\t\tPhoton disabled\n");
    an_func.DisablePhoton();
  }
  if (!neu_en) {
    printf("\t\tNeutron disabled\n");
    an_func.DisableNeutron();
  }
  if (!pion_en) {
    printf("\t\tPion disabled\n");
    an_func.DisablePion();
  }
  if (!modification_en) {
    printf("\t\tModification disabled\n");
    an_func.DisableModification();
  }
  if (tree_en) {
    printf("\t\tL90 Tree enabled\n");
    an_func.EnableTree();
  }
  if (pho_en) {
    printf("\tPhoton cuts: %s\n", pho_cuts.c_str());
    an_func.SetPhotonCuts(pho_cuts);
  }
  if (neu_en) {
    printf("\tNeutron cuts: %s\n", neu_cuts.c_str());
    an_func.SetNeutronCuts(neu_cuts);
  }
  if (old_dir != "") an_func.SetOldDir(old_dir.c_str());

  an_func.Analysis();

  // theApp.Run();

  return 0;
}
