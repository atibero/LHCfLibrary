#
#@file   sample.py
#@brief  Simplest example of python code with LHCfLibrary
#

import ROOT
#ROOT.gSystem.Load('../../build/lib/libDictionary.dylib');
ROOT.gSystem.Load('../../build/lib/libDictionary.so');
# for Linux, replace the path to ../../build/lib/libDictionary.so

# For Utility Class
ROOT.nLHCf.Utils.Printf(1, "Test \n")

# For Level2 class
# You may get a segmentation error once you quit python
# maybe relate to release memory
lvl2 = ROOT.nLHCf.Level2("nLHCf::Arm1Params")()
lvl2.Print()

