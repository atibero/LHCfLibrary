#
#@file   sample_eventloop.py
#@brief  An example of python code to analyze a LHCf tree file (calib file)
#

import ROOT
#ROOT.gSystem.Load('../../build/lib/libDictionary.dylib');
ROOT.gSystem.Load('../../build/lib/libDictionary.so');

# Open a Calibrated file
tree = ROOT.TChain('LHCfEvents')
tree.AddFile('../../build/test.root')

# Define histograms
ofile = ROOT.TFile('hist.root','RECREATE')
ofile.cd()
hist = [[0]*16 for _ in range(2)]
for it in range(0, 2):
    for il in range(0, 16):
        hist[it][il] = ROOT.TH1D('hist_{}_{}'.format(it,il),'hist',100,0,50)

ROOT.gROOT.cd()

# Event loop
nev = tree.GetEntries()
print('Nevents = {}'.format(nev))

for i in range (0, nev):
    tree.GetEntry(i)

    if tree.ev.Check('lvl2_a1') :
        lvl2_a1 = tree.ev.Get('lvl2_a1')

        for it in range(0, 2):
            for il in range(0, 16):
                hist[it][il].Fill(lvl2_a1.fCalorimeter[it][il])

# End of loop

ofile.Write()
ofile.Close()

