import ROOT
from ROOT import Double
from ROOT import TF1
from ROOT import TH2F
ROOT.gSystem.Load('../../build/lib/libDictionary.so');
#file = ROOT.TFile("../../build/Rawdata/analysed_run44299_4.root")
file = ROOT.TFile("../../Tables/a2_photon_leakage-efficiency.root")

def DrawHist():
    global c1,h1

    #h1 = file.Get("a1_photon_energy_0")
    h1 = file.Get("leakage_map_0_08")
    c1 = ROOT.TCanvas("c1","PhotonEnergy")

    ROOT.gStyle.SetTitle("MC Analysed PhotonEnergy")
    h1.SetMarkerStyle(7)
    h1.Draw("")
    
