import ROOT

class print_reduction :
    filename = ''
    file = 0
    tree = 0
    def __init__(self, filename=''):
        if filename != '':
            self.open(filename)
    def open(self, filename):
        self.filename = filename
        self.file = ROOT.TFile(filename)
        self.tree = self.file['lhcfarm1']
        print(f'opened file : {filename}')
        print(type(self.tree))

    def show(self, i):
        self.tree.GetEntry(i)
        print(f'Event: {i} ------------------------')
        print('photon_nhit   ', self.tree.photon_nhit)
        print('photon_pid.   ', self.tree.photon_pid)
        print('photon_energy ', self.tree.photon_energy)
        print('photon_pos    ', self.tree.photon_pos)
        print('photon_poscal ', self.tree.photon_poscal)

        print('mc_nhit       ', self.tree.mc_nhit)
        print('mc_pdgcode    ', self.tree.mc_pdgcode)
        print('mc_energy     ', self.tree.mc_energy)
        print('mc_pos.       ', self.tree.mc_pos)
        print('mc_poscal.    ', self.tree.mc_poscal)

if __name__ == "__main__":

    a = print_reduction('test.root')
    a.show(0)
    a.show(1)
    a.show(2)
