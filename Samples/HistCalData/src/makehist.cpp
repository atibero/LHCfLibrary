#include <iomanip>
#include <iostream>
using namespace std;

#include <TApplication.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TROOT.h>
#include <TString.h>

#include <Arm1Params.hh>
#include <Arm2Params.hh>
#include <LHCfEvent.hh>
#include <Level2.hh>
using namespace nLHCf;

int main(int argc, char **argv) {
  TString filename = "/crhome/kondo.moe/lhcflibrary/LHCfLibrary/build/cal_run44300.root";
  TString ofilename = "hist.root";

  for (int i = 1; i < argc; i++) {
    TString ss = argv[i];
    if (ss == "-i" || ss == "--input") {
      filename = argv[++i];
    }
    if (ss == "-o" || ss == "--output") {
      ofilename = argv[++i];
    }
  }

  cout << "INTPUT FILENAME: " << filename << endl;
  cout << "OUTPUT FILENAME: " << ofilename << endl;

  // Open input file (calibrate file)
  TChain *tree = new TChain("LHCfEvents");
  tree->AddFile(filename);
  tree->SetCacheSize(10000000);
  LHCfEvent *ev = new LHCfEvent("event", "LHCfEvent");
  tree->SetBranchAddress("ev.", &ev);
  tree->AddBranchToCache("*");

  // Open output file and memory allocation of histograms
  TFile *ofile = new TFile(ofilename, "RECREATE");
  ofile->cd();

  TH1D *h[Arm1Params::kCalNtower][Arm1Params::kCalNlayer];
  for (int it = 0; it < Arm1Params::kCalNtower; it++) {
    for (int il = 0; il < Arm1Params::kCalNlayer; il++) {
      h[it][il] = new TH1D(Form("h_%d_%d", it, il), "dE", 200, -0.5, 9.5);
    }
  }

  gROOT->cd();

  int nentries = tree->GetEntries();
  int nev = 0, nev_a1 = 0, nev_a2 = 0;
  cout << "Event Loop..." << endl;
  for (int ie = 0; ie < nentries; ie++) {
    tree->GetEntry(ie);

    // ev->Show();
    if (ev->Check("lvl2_a1")) {
      Level2<Arm1Params> *lvl2_a1 = (Level2<Arm1Params> *)ev->Get("lvl2_a1");

      cout << "Run " << lvl2_a1->fRun << "  "
           << "Event " << lvl2_a1->fEvent << endl;

      // Print the results
      // Scintillator plates
      for (int it = 0; it < Arm1Params::kCalNtower; it++) {
        cout << (it == 0 ? "TS " : "TL ");
        for (int il = 0; il < Arm1Params::kCalNlayer; il++) {
          cout << lvl2_a1->fCalorimeter[it][il] << "  ";
        }
        cout << endl;
      }
      // GSO bar hodoscope [tower][layer][xy][channel][sample]
      // parameter [sample] is for Arm2.
      // Only one sample for each channel in Arm1
      // The value seems to be filled into 1st sample [0]
      cout << lvl2_a1->fPosDet[0][0][0][0][0] << "  " << lvl2_a1->fPosDet[0][0][0][0][1] << "  "
           << lvl2_a1->fPosDet[0][0][0][0][2] << endl;

      // Fill to histograms
      for (int it = 0; it < Arm1Params::kCalNtower; it++) {
        for (int il = 0; il < Arm1Params::kCalNlayer; il++) {
          h[it][il]->Fill(lvl2_a1->fCalorimeter[it][il]);
        }
      }

      nev_a1++;
    }

    /* Clear event */
    ev->HeaderClear();
    ev->ObjDelete();
    nev++;

    if (ie > 100) break;
  }

  cout << "Number of analyzed a1 events = " << nev_a1 << endl;

  // Write the histograms to the output file
  ofile->Write();
  ofile->Close();

  return 0;
}
