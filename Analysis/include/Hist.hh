#ifndef Hist_HH
#define Hist_HH

#include <TFile.h>
#include <TTree.h>

#include "EventCut.hh"
#include "LHCfParams.hh"
#include "Level3.hh"
#include "PionData.hh"

namespace nLHCf {

template <typename arman>
class Hist : public arman, public LHCfParams {
 public:
  Hist(TString name);
  ~Hist();

 private:
  /* Histograms parameters */
  TString fNamePrefix;
  vector<vector<Double_t> > fPhotonEbins;
  vector<vector<Double_t> > fPhotonLbins;
  vector<vector<Double_t> > fNeutronEbins;

  /* Histograms */
  TH2F *fPhotonBeamCentre2D;

  vector<TH1D *> fPhotonTowerEnergy;
  vector<TH1D *> fPhotonTowerL90;

  vector<TH1D *> fPhotonEnergy;
  TH2F *fPhotonHitmap2D;
  vector<TH1D *> fPhotonHitmap1D;
  vector<TH1D *> fPhotonL90;

  vector<TH1D *> fPhotonTrueEnergy;
  TH2F *fPhotonTrueHitmap2D;
  vector<TH1D *> fPhotonTrueHitmap1D;
  vector<TH2F *> fPhotonEneRes;
  vector<vector<TH2F *> > fPhotonPosRes;

  TH2F *fNeutronBeamCentre2D;

  vector<TH1D *> fNeutronEnergy;
  TH2F *fNeutronHitmap2D;
  vector<TH1D *> fNeutronHitmap1D;
  vector<TH1D *> fNeutronL90;

  vector<TH1D *> fNeutronTrueEnergy;
  TH2F *fNeutronTrueHitmap2D;
  vector<TH1D *> fNeutronTrueHitmap1D;
  vector<TH2F *> fNeutronEneRes;
  vector<vector<TH2F *> > fNeutronPosRes;

  // For Photon analysis using Alessio's code
  // multihit
  vector<TH1D *> fPhotonInclusiveTrueEnergy;
  vector<TH1D *> fPhotonMultihitFraction;
  // energy
  vector<TH1D *> fPhotonPirateEnergy;
  vector<TH1D *> fPhotonPirateEnergyPIDCorrected;
  vector<TH1D *> fPhotonPirateEnergyPIDCorrectedTrueThreshold;
  // l90
  TTree *fL90Tree;
  vector<vector<TH1D *> > fPhotonPirateL90;
  vector<vector<vector<TH1D *> > > fPhotonPirateL90Template;
  vector<vector<vector<vector<Double_t> > > > fPhotonL90Vector;

  // Eugenio: used to study shower width
  /*
  vector<TH1I *>                     fNeutronXmax;
  vector<TH1I *>                     fNeutronYmax;
  vector<TH1I *>                     fNeutronFint;
  vector<TH1D *>                     fNeutronWtot;
  vector<TH1D *>                     fNeutronWave;
  vector<TH2D *>           		   fNeutronWlay;
  vector<TH1D *>                     fNeutronL2D;
  vector<TH2D *>                     fNeutronL2F;
  vector<TH2D *>			           fNeutronL2W;

  vector<TH1I *>                     fPhotonXmax;
  vector<TH1I *>                     fPhotonYmax;
  vector<TH1I *>                     fPhotonFint;
  vector<TH1D *>                     fPhotonWtot;
  vector<TH1D *>                     fPhotonWave;
  vector<TH2D *>			           fPhotonWlay;
  vector<TH1D *>                     fPhotonL2D;
  vector<TH2D *>                     fPhotonL2F;
  vector<TH2D *>			           fPhotonL2W;
  */

  TH1D *fT1Pi0_Mass;
  vector<TH1D *> fT2Pi0_Mass;
  TH1D *fT1Pi0_Mass_TrueCut;
  vector<TH1D *> fT2Pi0_Mass_TrueCut;
  TH1D *fT1Pi0_TrueMass;
  vector<TH1D *> fT2Pi0_TrueMass;
  vector<TH1D *> fT1Pi0_Mass_vs_y;
  vector<vector<TH1D *> > fT2Pi0_Mass_vs_y;
  vector<TH1D *> fT1Pi0_Pt_vs_y;
  vector<vector<TH1D *> > fT2Pi0_Pt_vs_y;

  Bool_t fSaveTree;

 public:
  void Initialise(TFile *fout, Bool_t savetree = false);
  template <typename level3, typename eventcut>
  void FillPhotonHistograms(level3 *, eventcut *, McEvent * = NULL);
  template <typename level3, typename eventcut>
  void FillNeutronHistograms(level3 *, eventcut *, McEvent * = NULL);
  void FillPionHistograms(vector<PionData> *pdata, vector<PionData> *ptrue);
  void FillL90Vector();

 private:
  void AllocateMemory();
  void ClearMemory();
  void SetBinning();
  void SetEnergyBinning();
  void SetL90Binning();
  void MakeHistograms(TFile *fout);
  void MakeTrees(TFile *fout);

 public:
  ClassDef(Hist, 1);
};
}  // namespace nLHCf

#endif
