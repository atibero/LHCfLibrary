#ifndef DataModification_HH
#define DataModification_HH

#include "Arm1AnPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "LHCfParams.hh"
#include "Level2.hh"
#include "Level3.hh"

namespace nLHCf {

template <typename arman, typename armrec, typename arm>
class DataModification : public arman, public LHCfParams {
 public:
  DataModification();
  ~DataModification();

 public:
  void Modify(Level3<armrec> *lvl3, Level2<arm> *lvl2 = NULL);
  void RescaleEnergy(Level3<armrec> *lvl3);

  ClassDef(DataModification, 1);
};
}  // namespace nLHCf
#endif
