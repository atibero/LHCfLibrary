#ifndef Tree_HH
#define Tree_HH

#include <TFile.h>
#include <TTree.h>

#include "EventCut.hh"
#include "LHCfParams.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"

namespace nLHCf {

template <typename arman>
class Tree : public arman, public LHCfParams {
 public:
  Tree(TString name, TString option);
  ~Tree();

  struct Particle {
    int user_flag;
    int code;
    int charge;
    int subcode;
    double energy;
    double momentum[3];
    double poslhc[2];
    double postow[2];
  };

  int currentEntry;
  vector<Particle> p;

 private:
  TString fTreeName;
  TString fTreeOption;
  TTree *tree;

  vector<int> ventry;
  vector<int> vrun;
  vector<int> vevent;
  vector<int> vtime;
  vector<bool> vbeam_flag;
  vector<int> vl2t;
  vector<unsigned int> vgpio0;
  vector<unsigned int> vcounter;
  vector<int> vtower;
  vector<bool> vtrigger;
  vector<bool> vmulti;
  vector<double> venergy;
  vector<double> vpid;
  vector<double> vl2d;
  vector<double> vl90;
  vector<double> vl3t;
  vector<int> vmaxposdetx;
  vector<int> vmaxposdety;
  vector<double> vposrecx;
  vector<double> vposrecy;
  vector<double> vlhcx;
  vector<double> vlhcy;
  vector<int> vthit;
  vector<int> vuser_flag;
  vector<int> vcode;
  vector<int> vcharge;
  vector<double> vtenergy;
  vector<double> vtmomentum;
  vector<double> vtlhcx;
  vector<double> vtlhcy;
  vector<double> vtposx;
  vector<double> vtposy;
  vector<int> vthit_all;
  vector<int> vuser_flag_all;
  vector<int> vcode_all;
  vector<int> vcharge_all;
  vector<double> vtenergy_all;
  vector<double> vtmomentum_all;
  vector<double> vtlhcx_all;
  vector<double> vtlhcy_all;
  vector<double> vtposx_all;
  vector<double> vtposy_all;
  vector<double> vcalderec;
  vector<double> vcalde;
  vector<double> vsilicon;
  vector<double> vsilcharge;

 public:
  int armnum;
  bool datamc;
  bool neupho;
  bool l3tvar;
  bool siedep;
  bool debug1;
  bool debug2;
  bool debugs;

  void Initialise(TFile *fout);
  template <typename level1, typename level2, typename level3, typename mcevent, typename eventcut>
  void FillTree(level1 *lvl1, level2 *lvl2, level3 *lvl3, mcevent *mc, eventcut *evcut);
  void SetCurrentEntry(int entry);

 private:
  void ClearMemory();
  void SetFlags();
  void MakeTree(TFile *fout);

 public:
  ClassDef(Tree, 1);
};

}  // namespace nLHCf

#endif
