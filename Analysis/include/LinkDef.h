#ifndef LinkDefAn_H
#define LinkDefAn_H

#ifdef __CINT__
//#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ namespace nLHCf;
#pragma link C++ class nLHCf::EventCut < nLHCf::Arm1AnPars, nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::EventCut < nLHCf::Arm2AnPars, nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::Hist < nLHCf::Arm1AnPars> + ;
#pragma link C++ class nLHCf::Hist < nLHCf::Arm2AnPars> + ;
#pragma link C++ class nLHCf::Tree < nLHCf::Arm1AnPars> + ;
#pragma link C++ class nLHCf::Tree < nLHCf::Arm2AnPars> + ;
#pragma link C++ class nLHCf::LHCfAn + ;
#pragma link C++ class nLHCf::PionRec + ;
#pragma link C++ class nLHCf::DataModification < nLHCf::Arm1AnPars, nLHCf::Arm1RecPars, nLHCf::Arm1Params> + ;
#pragma link C++ class nLHCf::DataModification < nLHCf::Arm2AnPars, nLHCf::Arm2RecPars, nLHCf::Arm2Params> + ;

#endif

#endif
