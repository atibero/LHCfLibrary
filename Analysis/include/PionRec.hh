#ifndef PionRec_HH
#define PionRec_HH

#include <vector>

using namespace std;

#include "PionData.hh"

namespace nLHCf {

class PionRec {
 public:
  PionRec();
  ~PionRec();

  static const Int_t kNtypes = 3;
  PionData fResult[kNtypes];

 private:
 public:
  template <typename level3, typename eventcut>
  void Reconstruct(level3 *lvl3, eventcut *cut, Int_t pid_eff = 90);
  template <typename level3, typename eventcut>
  Int_t ReconstructType1(level3 *lvl3, eventcut *cut, PionData *result, Int_t pid_eff = 90);
  template <typename level3, typename eventcut>
  Int_t ReconstructType2(level3 *lvl3, Int_t tower, eventcut *cut, PionData *result, Int_t pid_eff = 90);

  template <typename eventcut>
  void Reconstruct(McEvent *mcev, eventcut *cut);
  template <typename eventcut>
  Int_t ReconstructType1(McEvent *mcev, eventcut *cut, PionData *result);
  template <typename eventcut>
  Int_t ReconstructType2(McEvent *mcev, Int_t tower, eventcut *cut, PionData *result);

  void Clear();

 public:
  ClassDef(PionRec, 1);
};
}  // namespace nLHCf

#endif
