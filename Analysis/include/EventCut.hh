#ifndef EventCut_HH
#define EventCut_HH

#include "Arm1AnPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2RecPars.hh"
#include "LHCfParams.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "Utils.hh"

namespace nLHCf {

template <typename arman, typename armrec>
class EventCut : public arman, public LHCfParams {
 public:
  EventCut();
  ~EventCut();

 public:
  void SetPhotonCuts(TString cuts);
  void SetNeutronCuts(TString cuts);

  /* Reconstructed cuts */
  Bool_t CutPhotonEvent(Level3<armrec> *lvl3, Int_t rap);
  Bool_t CutPhotonEventL90(Level3<armrec> *lvl3, Int_t rap);
  Bool_t CutPhotonEventTower(Level3<armrec> *lvl3, Int_t tower);
  Bool_t CutPhotonEventTowerMultiHit(Level3<armrec> *lvl3, Int_t tower);
  Bool_t CutPhotonEventBeamCentre(Level3<armrec> *lvl3, Int_t tower);
  Bool_t CutNeutronEventTower(Level3<armrec> *lvl3, Int_t tower);
  Bool_t CutNeutronEvent(Level3<armrec> *lvl3, Int_t rap);

  Bool_t BPTXCut(Level3<armrec> *lvl3);
  Bool_t L2TShowerCut(Level3<armrec> *lvl3);
  Bool_t PhotonDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t PhotonPIDCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t PhotonEnergyLinCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower, Int_t part);
  Bool_t PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower, Int_t part);
  Bool_t PhotonRapidityCut(Level3<armrec> *lvl3, Int_t rap);
  Bool_t PhotonMultiHitCut(Level3<armrec> *lvl3, Int_t rap);
  Bool_t NeutronDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t NeutronPIDCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t NeutronEnergyCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t NeutronPositionCut(Level3<armrec> *lvl3, Int_t tower);
  Bool_t NeutronRapidityCut(Level3<armrec> *lvl3, Int_t rap);
  Bool_t NeutronMultiHitCut(Level3<armrec> *lvl3, Int_t rap);
  Bool_t PionPIDCut(Level3<armrec> *lvl3, Int_t tower, Int_t type, Int_t eff = 90);
  Bool_t PionTypeICut(Level3<armrec> *lvl3, Int_t eff = 90, UInt_t *result_cutflag = NULL);
  Bool_t PionTypeIICut(Level3<armrec> *lvl3, Int_t tower, Int_t eff = 90, UInt_t *result_cutflag = NULL);
  Bool_t PionMassCut(Double_t mass, Int_t type);
  Double_t PionL90Boundary(Int_t tower, Double_t energy, Int_t type, Int_t eff = 90);

  /* Utilities */
  TVector3 PhotonGlobalPosition(Level3<armrec> *lvl3, Int_t tower);
  TVector3 NeutronGlobalPosition(Level3<armrec> *lvl3, Int_t tower);

  /* MC truth cuts */
  Bool_t CutTruePhotonEvent(McEvent *mcev, Int_t rap);
  Bool_t CutTruePhotonEventInclusive(McEvent *mcev, Int_t rap, Int_t par);
  Bool_t CutTrueNeutronEvent(McEvent *mcev, Int_t rap);

  Bool_t PhotonTruePIDCut(McEvent *mcev, Int_t tower, Int_t part = 0);
  Bool_t PhotonTrueEnergyCut(McEvent *mcev, Int_t tower, Int_t part = 0);
  Bool_t PhotonTruePositionCut(McEvent *mcev, Int_t tower, Int_t part = 0);
  Bool_t PhotonTrueRapidityCut(McEvent *mcev, Int_t rap, Int_t part = 0);
  Bool_t PhotonTrueMultiHitCut(McEvent *mcev, Int_t tower);
  Bool_t NeutronTruePIDCut(McEvent *mcev, Int_t tower);
  Bool_t NeutronTrueEnergyCut(McEvent *mcev, Int_t tower);
  Bool_t NeutronTruePositionCut(McEvent *mcev, Int_t tower);
  Bool_t NeutronTrueRapidityCut(McEvent *mcev, Int_t rap);
  Bool_t NeutronTrueMultiHitCut(McEvent *mcev, Int_t tower);
  Bool_t PionTrueTypeICut(McEvent *mcev);
  Bool_t PionTrueTypeIICut(McEvent *mcev, Int_t tower);

 public:
  ClassDef(EventCut, 1);

 private:
  TString fPhotonCuts;
  TString fNeutronCuts;
  Bool_t fPhotonCutPID;
  Bool_t fPhotonCutEnergy;
  Bool_t fPhotonCutPosition;
  Bool_t fPhotonCutRapidity;
  Bool_t fPhotonCutMultiHit;
  Bool_t fPhotonCutBPTX;
  Bool_t fPhotonCutL2T;
  Bool_t fPhotonCutDsc;
  Bool_t fNeutronCutPID;
  Bool_t fNeutronCutEnergy;
  Bool_t fNeutronCutPosition;
  Bool_t fNeutronCutRapidity;
  Bool_t fNeutronCutMultiHit;
  Bool_t fNeutronCutBPTX;
  Bool_t fNeutronCutL2T;
  Bool_t fNeutronCutDsc;
};

}  // namespace nLHCf
#endif
