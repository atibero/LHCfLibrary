#ifndef LHCfAn_HH
#define LHCfAn_HH

#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TMath.h>
#include <TString.h>

#include "Arm1AnPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "Hist.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level3.hh"
#include "PionData.hh"
#include "Tree.hh"

using namespace std;

namespace nLHCf {

class LHCfAn : public LHCfParams {
 public:
  LHCfAn();
  ~LHCfAn();

 private:
  /*--- Input/Output ---*/
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;

  /*--- Level3 ---*/
  Level1<Arm1Params> *fLvl1_Arm1;
  Level2<Arm1Params> *fLvl2_Arm1;
  Level3<Arm1RecPars> *fLvl3_Arm1;
  McEvent *fTrue_Arm1;
  Level1<Arm2Params> *fLvl1_Arm2;
  Level2<Arm2Params> *fLvl2_Arm2;
  Level3<Arm2RecPars> *fLvl3_Arm2;
  McEvent *fTrue_Arm2;
  Hist<Arm1AnPars> *fHistArm1;
  Hist<Arm2AnPars> *fHistArm2;
  Tree<Arm1AnPars> *fTreeArm1;
  Tree<Arm2AnPars> *fTreeArm2;

  /*--- Pions ---*/
  vector<PionData> fPion_Arm1;
  vector<PionData> fPion_Arm2;
  vector<PionData> fTruePion_Arm1;
  vector<PionData> fTruePion_Arm2;

  /*--- Event cuts ---*/
  TString fPhotonCuts;
  TString fNeutronCuts;

  /*--- Analysis variables ---*/

  // enable/disable analysis for each Arm
  vector<Bool_t> fArmEnable;
  // enable/disable photon analysis
  Bool_t fPhotonEnable;
  // enable/disable neutron analysis
  Bool_t fNeutronEnable;
  // enable/disable pion analysis
  Bool_t fPionEnable;
  // enable/disable rescale energy
  Bool_t fModificationEnable;

  // save photon l90 tree
  Bool_t fTreeEnable;

  /* Debug */
  Bool_t fOldFlag;
  TString fOldInputDir;
  Hist<Arm1AnPars> *fOldHistArm1;
  Hist<Arm2AnPars> *fOldHistArm2;

  /* Trigger counters */
  Int_t fTotalTrgArm1;
  Int_t fShowerTrgArm1;
  Int_t fPi0TrgArm1;
  Int_t fHighEMTrgArm1;
  Int_t fHadronTrgArm1;
  Int_t fZdcTrgArm1;
  Int_t fFcTrgArm1;
  Int_t fL1tTrgArm1;
  Int_t fLaserTrgArm1;
  Int_t fPedestalTrgArm1;

  Int_t fTotalTrgArm2;
  Int_t fShowerTrgArm2;
  Int_t fPi0TrgArm2;
  Int_t fHighEMTrgArm2;
  Int_t fHadronTrgArm2;
  Int_t fZdcTrgArm2;
  Int_t fFcTrgArm2;
  Int_t fL1tTrgArm2;
  Int_t fLaserTrgArm2;
  Int_t fPedestalTrgArm2;

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }

  void DisableArm1() { fArmEnable[Arm1Params::kArmIndex] = false; }
  void DisableArm2() { fArmEnable[Arm2Params::kArmIndex] = false; }

  void DisablePhoton() { fPhotonEnable = false; }
  void DisableNeutron() { fNeutronEnable = false; }
  void DisablePion() { fPionEnable = false; }

  void EnableTree() { fTreeEnable = true; }

  void SetPhotonCuts(TString cuts) { fPhotonCuts = cuts; }
  void SetNeutronCuts(TString cuts) { fNeutronCuts = cuts; }

  void DisableModification() { fModificationEnable = false; }

  void Analysis();

  void CreateOutputTree(TString dataFormat = "", TString treeName = "", TString treeOption = "");

  /* Debug */
  void SetOldDir(const Char_t *name) {
    fOldInputDir = name;
    if (fOldInputDir != "") fOldFlag = true;
  }

 private:
  /* Memory allocation */
  void AllocateVectors();

  /* Input/Output */
  void SetInputTree();
  void OpenOutputFile();
  void WriteToOutput();
  void CloseFiles();

  /* Histograms and Tree*/
  void MakeHistograms();
  void BookTree(TString dataFormat = "", TString treeName = "", TString treeOption = "");

  /* Event loop */
  void EventLoop();
  template <typename armrec, typename arman>
  vector<PionData> GetPionData(Level3<armrec> *, EventCut<arman, armrec> *, McEvent *mcev = nullptr);
  template <typename armrec, typename arman>
  vector<PionData> GetPionData(McEvent *, EventCut<arman, armrec> *);
  void FillTree(TString dataFormat = "", TString treeName = "", TString treeOption = "");

  /* Print Event */
  template <typename level3, typename eventcut>
  void Print(level3 *, eventcut *, Int_t ev, McEvent *mcev = NULL);

 public:
  ClassDef(LHCfAn, 1);
};
}  // namespace nLHCf
#endif
