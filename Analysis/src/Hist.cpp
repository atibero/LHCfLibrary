#include "Hist.hh"

#include <TMath.h>
#include <TROOT.h>

#include "Arm1AnPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(Hist);
#endif

typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename arman>
Hist<arman>::Hist(TString name) : fNamePrefix(name) {}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename arman>
Hist<arman>::~Hist() {
  ClearMemory();
}

/*----------------------*/
/*--- Initialisation ---*/
/*----------------------*/
template <typename arman>
void Hist<arman>::Initialise(TFile *fout, Bool_t savetree) {
  SetBinning();
  AllocateMemory();
  fSaveTree = savetree;
  MakeHistograms(fout);
  if (fSaveTree) {
    MakeTrees(fout);
  }
}

template <typename arman>
void Hist<arman>::AllocateMemory() {
  Utils::AllocateVector1D(fPhotonTowerEnergy, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonTowerL90, this->kCalNtower);

  Utils::AllocateVector1D(fPhotonEnergy, this->kPhotonNrap);
  Utils::AllocateVector1D(fPhotonHitmap1D, this->kNxy);
  Utils::AllocateVector1D(fPhotonL90, this->kPhotonNrap);

  Utils::AllocateVector1D(fPhotonTrueEnergy, this->kPhotonNrap);
  Utils::AllocateVector1D(fPhotonTrueHitmap1D, this->kNxy);
  Utils::AllocateVector1D(fPhotonEneRes, this->kPhotonNrap);
  Utils::AllocateVector2D(fPhotonPosRes, this->kPhotonNrap, this->kNxy);

  Utils::AllocateVector1D(fPhotonInclusiveTrueEnergy, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonMultihitFraction, this->kCalNtower);

  Utils::AllocateVector1D(fPhotonPirateEnergy, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonPirateEnergyPIDCorrected, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonPirateEnergyPIDCorrectedTrueThreshold, this->kCalNtower);
  Utils::AllocateVector2D(fPhotonPirateL90, this->kCalNtower, fPhotonEbins[0].size());
  Utils::AllocateVector3D(fPhotonPirateL90Template, this->kCalNtower, 2, fPhotonEbins[0].size());

  Utils::AllocateVector1D(fNeutronEnergy, this->kNeutronNrap);
  Utils::AllocateVector1D(fNeutronHitmap1D, this->kNxy);
  Utils::AllocateVector1D(fNeutronL90, this->kNeutronNrap);

  Utils::AllocateVector1D(fNeutronTrueEnergy, this->kNeutronNrap);
  Utils::AllocateVector1D(fNeutronTrueHitmap1D, this->kNxy);
  Utils::AllocateVector1D(fNeutronEneRes, this->kNeutronNrap);
  Utils::AllocateVector2D(fNeutronPosRes, this->kNeutronNrap, this->kNxy);

  // Eugenio: used to study shower width
  /*
  Utils::AllocateVector1D(fNeutronXmax, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronYmax, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronFint, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronWtot, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronWave, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronWlay, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronL2D, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronL2F, this->kCalNtower);
  Utils::AllocateVector1D(fNeutronL2W, this->kCalNtower);

  Utils::AllocateVector1D(fPhotonXmax, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonYmax, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonFint, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonWtot, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonWave, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonWlay, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonL2D, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonL2F, this->kCalNtower);
  Utils::AllocateVector1D(fPhotonL2W, this->kCalNtower);
  */

  Utils::AllocateVector1D(fT2Pi0_Mass, this->kCalNtower);
  Utils::AllocateVector1D(fT2Pi0_Mass_TrueCut, this->kCalNtower);
  Utils::AllocateVector1D(fT2Pi0_TrueMass, this->kCalNtower);
  Utils::AllocateVector1D(fT1Pi0_Mass_vs_y, this->kPionNrapTypeI);
  Utils::AllocateVector1D(fT2Pi0_Mass_vs_y, this->kCalNtower);
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    Utils::AllocateVector1D(fT2Pi0_Mass_vs_y[it], this->kPionNrapTypeII[it]);

  Utils::AllocateVector1D(fT1Pi0_Pt_vs_y, this->kPionNrapTypeI);
  Utils::AllocateVector1D(fT2Pi0_Pt_vs_y, this->kCalNtower);
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    Utils::AllocateVector1D(fT2Pi0_Pt_vs_y[it], this->kPionNrapTypeII[it]);
}

template <typename arman>
void Hist<arman>::ClearMemory() {}

template <typename arman>
void Hist<arman>::SetBinning() {
  SetEnergyBinning();
  SetL90Binning();
}

template <typename arman>
void Hist<arman>::SetEnergyBinning() {
  Utils::Printf(Utils::kPrintDebug, "Setting energy binning...");
  fflush(stdout);

  Utils::AllocateVector1D(fPhotonEbins, this->kPhotonNrap);
  for (Int_t ir = 0; ir < this->kPhotonNrap; ++ir) {
    Int_t it = this->RapToTower(ir);
    if (it == 0) {
      // small tower
      for (Int_t ie = 1; ie <= 20; ++ie) fPhotonEbins[ir].push_back((Double_t)ie * 100.);  // eugenio
      for (Int_t ie = 1; ie <= 10; ++ie) fPhotonEbins[ir].push_back(2000. + (Double_t)ie * 200.);
      for (Int_t ie = 1; ie <= 2; ++ie) fPhotonEbins[ir].push_back(4000. + (Double_t)ie * 500.);
      for (Int_t ie = 1; ie <= 2; ++ie) fPhotonEbins[ir].push_back(5000. + (Double_t)ie * 1000.);
    } else {
      // large tower
      for (Int_t ie = 1; ie <= 20; ++ie) fPhotonEbins[ir].push_back((Double_t)ie * 100.);  // eugenio
      for (Int_t ie = 1; ie <= 5; ++ie) fPhotonEbins[ir].push_back(2000. + (Double_t)ie * 200.);
      for (Int_t ie = 1; ie <= 2; ++ie) fPhotonEbins[ir].push_back(3000. + (Double_t)ie * 500.);
      for (Int_t ie = 1; ie <= 3; ++ie) fPhotonEbins[ir].push_back(4000. + (Double_t)ie * 1000.);
    }
  }

  Utils::AllocateVector1D(fNeutronEbins, this->kNeutronNrap);
  for (Int_t ir = 0; ir < this->kNeutronNrap; ++ir) {
    for (Int_t ie = 5; ie <= 150; ++ie) fNeutronEbins[ir].push_back((Double_t)ie * 100.);
  }

  // Rescale energy to use the same Feynman binning
  if (this->fOperation == kLHC2022) {
    for (Int_t ir = 0; ir < fPhotonEbins.size(); ++ir) {
      for (Int_t ie = 0; ie < fPhotonEbins[ir].size(); ++ie) {
        fPhotonEbins[ir][ie] *= 6.8 / 6.5;
      }
    }
    for (Int_t ir = 0; ir < fNeutronEbins.size(); ++ir) {
      for (Int_t ie = 0; ie < fNeutronEbins[ir].size(); ++ie) {
        fNeutronEbins[ir][ie] *= 6.8 / 6.5;
      }
    }
  }

  Utils::Printf(Utils::kPrintDebug, " Done.\n");
}

template <typename arman>
void Hist<arman>::SetL90Binning() {
  Utils::Printf(Utils::kPrintDebug, "Setting L90 binning...");
  fflush(stdout);

  Utils::AllocateVector1D(fPhotonLbins, 4);

  for (int ib = 0; ib <= 44; ++ib) fPhotonLbins[0].push_back((double)ib * 0.5);
  for (int ib = 1; ib <= 28; ++ib) fPhotonLbins[0].push_back(22. + (double)ib * 1.);
  for (int ib = 0; ib <= 22; ++ib) fPhotonLbins[1].push_back((double)ib * 1.);
  for (int ib = 1; ib <= 14; ++ib) fPhotonLbins[1].push_back(22. + (double)ib * 2.);
  for (int ib = 0; ib <= 11; ++ib) fPhotonLbins[2].push_back((double)ib * 2.);
  for (int ib = 1; ib <= 7; ++ib) fPhotonLbins[2].push_back(22. + (double)ib * 4.);
  fPhotonLbins[3].push_back(0.);
  fPhotonLbins[3].push_back(4.);
  fPhotonLbins[3].push_back(8.);
  fPhotonLbins[3].push_back(12.);
  fPhotonLbins[3].push_back(18.);
  fPhotonLbins[3].push_back(22.);
  fPhotonLbins[3].push_back(28.);
  fPhotonLbins[3].push_back(32.);
  fPhotonLbins[3].push_back(38.);
  fPhotonLbins[3].push_back(44.);
  fPhotonLbins[3].push_back(50.);
}

template <typename arman>
void Hist<arman>::MakeHistograms(TFile *fout) {
  Utils::Printf(Utils::kPrintDebug, "Allocating histograms...");
  fflush(stdout);

  fout->cd();

  TH1::SetDefaultSumw2();

  Int_t pos_nbin[] = {384, 384};
  Double_t pos_lowbin[] = {-18.56, -13.88};
  Double_t pos_highbin[] = {42.88, 47.56};
  TString view_name[] = {"X", "Y"};
  fPhotonBeamCentre2D =
      new TH2F(Form("%s_photon_beam_centre", fNamePrefix.Data()), Form("Arm%d Photon Hitmap", this->kArmIndex + 1),
               pos_nbin[0], pos_lowbin[0], pos_highbin[0], pos_nbin[1], pos_lowbin[1], pos_highbin[1]);
  /*  Photons */

  // inclusive histograms
  Int_t ene_inc_bin = 1000;
  Double_t ene_inc_min = 0.;
  Double_t ene_inc_max = 500.;
  for (Int_t it = 0; it < fPhotonTowerEnergy.size(); ++it) {
    fPhotonTowerEnergy[it] = new TH1D(Form("%s_photon_tower_energy_%d", fNamePrefix.Data(), it),
                                      Form("Arm%d Photon Energy (tower %d)", this->kArmIndex + 1, it), ene_inc_bin,
                                      ene_inc_min, ene_inc_max);
  }

  for (Int_t it = 0; it < fPhotonTowerL90.size(); ++it) {
    fPhotonTowerL90[it] = new TH1D(Form("%s_photon_tower_l90_%d", fNamePrefix.Data(), it),
                                   Form("Arm%d L_{90%%} (tower %d)", this->kArmIndex + 1, it), 45, 0., 45.);
  }

  // energy spectrum
  for (Int_t ir = 0; ir < fPhotonEnergy.size(); ++ir) {
    fPhotonEnergy[ir] = new TH1D(Form("%s_photon_energy_%d", fNamePrefix.Data(), ir),
                                 Form("Arm%d Photon Energy (rapidity %d)", this->kArmIndex + 1, ir),
                                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
  }

  // hitmap (2D & 1D)
  fPhotonHitmap2D =
      new TH2F(Form("%s_photon_hitmap", fNamePrefix.Data()), Form("Arm%d Photon Hitmap", this->kArmIndex + 1),
               pos_nbin[0], pos_lowbin[0], pos_highbin[0], pos_nbin[1], pos_lowbin[1], pos_highbin[1]);
  for (Int_t iv = 0; iv < fPhotonHitmap1D.size(); ++iv) {
    fPhotonHitmap1D[iv] = new TH1D(Form("%s_photon_hitmap_%d", fNamePrefix.Data(), iv),
                                   Form("Arm%d Photon Hitmap (%s view)", this->kArmIndex + 1, view_name[iv].Data()),
                                   pos_nbin[iv], pos_lowbin[iv], pos_highbin[iv]);
  }

  // L90%
  for (Int_t ir = 0; ir < fPhotonL90.size(); ++ir)
    fPhotonL90[ir] = new TH1D(Form("%s_photon_l90_%d", fNamePrefix.Data(), ir),
                              Form("Arm%d L_{90%%} (rapidity %d)", this->kArmIndex + 1, ir), 45, 0., 45.);

  // true energy spectrum
  for (Int_t ir = 0; ir < fPhotonTrueEnergy.size(); ++ir) {
    fPhotonTrueEnergy[ir] =
        new TH1D(Form("%s_photon_true_energy_%d", fNamePrefix.Data(), ir),
                 Form("Arm%d Photon Energy (Arm%d, rapidity %d)", this->kArmIndex + 1, this->kArmIndex + 1, ir),
                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
  }

  // true hitmap (2D & 1D)
  fPhotonTrueHitmap2D =
      new TH2F(Form("%s_photon_true_hitmap", fNamePrefix.Data()), Form("Arm%d Photon True Hitmap", this->kArmIndex + 1),
               pos_nbin[0], pos_lowbin[0], pos_highbin[0], pos_nbin[1], pos_lowbin[1], pos_highbin[1]);
  for (Int_t iv = 0; iv < fPhotonTrueHitmap1D.size(); ++iv) {
    fPhotonTrueHitmap1D[iv] =
        new TH1D(Form("%s_photon_true_hitmap_%d", fNamePrefix.Data(), iv),
                 Form("Arm%d Photon True Hitmap (%s view)", this->kArmIndex + 1, view_name[iv].Data()), pos_nbin[iv],
                 pos_lowbin[iv], pos_highbin[iv]);
  }

  // energy resolution
  Int_t ene_res_bin = 400;
  Double_t ene_res_min = -0.2;
  Double_t ene_res_max = 0.2;
  for (Int_t ir = 0; ir < fPhotonEnergy.size(); ++ir) {
    fPhotonEneRes[ir] =
        new TH2F(Form("%s_photon_ene_res_%d", fNamePrefix.Data(), ir),
                 Form("Arm%d Photon Energy Resolution (rapidity %d)", this->kArmIndex + 1, ir),
                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0], ene_res_bin, ene_res_min, ene_res_max);
  }

  // position resolution
  Int_t pos_res_bin = 1000;
  Double_t pos_res_min = -10;
  Double_t pos_res_max = 10;
  for (Int_t ir = 0; ir < fPhotonPosRes.size(); ++ir)
    for (Int_t iv = 0; iv < fPhotonPosRes[ir].size(); ++iv) {
      fPhotonPosRes[ir][iv] = new TH2F(
          Form("%s_photon_pos_res_%d_%d", fNamePrefix.Data(), ir, iv),
          Form("Arm%d Photon Energy Resolution (%s view, rapidity %d)", this->kArmIndex + 1, view_name[iv].Data(), ir),
          Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0], pos_res_bin, pos_res_min, pos_res_max);
    }

  // true energy inclusive spectrum
  for (Int_t it = 0; it < fPhotonInclusiveTrueEnergy.size(); ++it) {
    Int_t ir = it == 0 ? 0 : 4;
    fPhotonInclusiveTrueEnergy[it] =
        new TH1D(Form("%s_photon_true_inclusive_energy_%d", fNamePrefix.Data(), it),
                 Form("Arm%d Inclusive Photon Energy (Arm%d, Tower %d)", this->kArmIndex + 1, this->kArmIndex + 1, it),
                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
  }

  // reco multihit fraction
  for (Int_t it = 0; it < fPhotonMultihitFraction.size(); ++it) {
    Int_t ir = it == 0 ? 0 : 4;
    fPhotonMultihitFraction[it] =
        new TH1D(Form("%s_photon_multihit_fraction_energy_%d", fNamePrefix.Data(), it),
                 Form("Arm%d Multihit Fraction (Arm%d, Tower %d)", this->kArmIndex + 1, this->kArmIndex + 1, it),
                 Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
  }

  // pirate energy spectrum
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    Int_t ir = it == 0 ? 0 : 4;
    string title = Form("Arm%d Photon Energy (Tower %d); Energy [GeV]", this->kArmIndex + 1, it);
    fPhotonPirateEnergy[it] =
        new TH1D(Form("energy[%d]", it), title.c_str(), Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
    fPhotonPirateEnergyPIDCorrected[it] = new TH1D(Form("energy_TruePID[%d]", it), title.c_str(),
                                                   Int_t(fPhotonEbins[ir].size()) - 1, &fPhotonEbins[ir][0]);
    fPhotonPirateEnergyPIDCorrectedTrueThreshold[it] =
        new TH1D(Form("energy_TruePID_TrueEnergyThreshold[%d]", it), title.c_str(), Int_t(fPhotonEbins[ir].size()) - 1,
                 &fPhotonEbins[ir][0]);
  }
  // pirate L90%
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    Int_t ir = it == 0 ? 0 : 4;
    for (Int_t ie = 0; ie < fPhotonEbins[ir].size(); ++ie) {
      int ibin;
      double mean = (fPhotonEbins[ir][ie] + fPhotonEbins[ir][ie + 1]) / 2.;
      if (mean < 2000.)
        ibin = 0;
      else if (mean < 3600.)
        ibin = 1;
      else if (mean < 4000.)
        ibin = 2;
      else
        ibin = 3;
      string title = Form("Arm%d L_{90%%} (Tower %d - %.0f < Energy [GeV] < %.0f)", this->kArmIndex + 1, it,
                          fPhotonEbins[ir][ie], fPhotonEbins[ir][ie + 1]);
      fPhotonPirateL90[it][ie] = new TH1D(Form("l90_all_bin[%d][%d]", it, ie), title.c_str(),
                                          Int_t(fPhotonLbins[ibin].size()) - 1, &fPhotonLbins[ibin][0]);
      for (Int_t ip = 0; ip < 2; ++ip) {
        title = Form("Arm%d L_{90%%} (Tower %d - %.0f < Energy [GeV] < %.0f)", this->kArmIndex + 1, it,
                     fPhotonEbins[ir][ie], fPhotonEbins[ir][ie + 1]);
        fPhotonPirateL90Template[it][ip][ie] = new TH1D(Form("l90_all_bin[%d][%d][%d]", it, ip, ie), title.c_str(),
                                                        Int_t(fPhotonLbins[ibin].size()) - 1, &fPhotonLbins[ibin][0]);
      }
    }
  }

  /* Pions */

  // pi0 mass (typeI)
  fT1Pi0_Mass = new TH1D(Form("%s_t1_mgg", fNamePrefix.Data()), Form("Arm%d M_{gg} (type I)", this->kArmIndex + 1),
                         1000, 0., 1000.);
  // pi0 mass (typeII)
  TString tower_name[] = {"small", "large"};
  for (Int_t it = 0; it < fT2Pi0_Mass.size(); ++it)
    fT2Pi0_Mass[it] =
        new TH1D(Form("%s_t2_mgg_%d", fNamePrefix.Data(), it),
                 Form("Arm%d M_{gg} (type II, %s tower)", this->kArmIndex + 1, tower_name[it].Data()), 1000, 0., 1000.);

  // pi0 mass (true typeI)
  fT1Pi0_Mass_TrueCut = new TH1D(Form("%s_true_t1_mgg", fNamePrefix.Data()),
                                 Form("Arm%d M_{gg} (true type I)", this->kArmIndex + 1), 1000, 0., 1000.);
  // pi0 mass (true typeII)
  for (Int_t it = 0; it < fT2Pi0_Mass_TrueCut.size(); ++it)
    fT2Pi0_Mass_TrueCut[it] = new TH1D(
        Form("%s_true_t2_mgg_%d", fNamePrefix.Data(), it),
        Form("Arm%d M_{gg} (true type II, %s tower)", this->kArmIndex + 1, tower_name[it].Data()), 1000, 0., 1000.);

  // true pi0 mass (typeI)
  fT1Pi0_TrueMass = new TH1D(Form("%s_t1_true_mgg", fNamePrefix.Data()),
                             Form("Arm%d true M_{gg} (type I)", this->kArmIndex + 1), 1000, 130., 140.);
  // true pi0 mass (typeII)
  for (Int_t it = 0; it < fT2Pi0_TrueMass.size(); ++it)
    fT2Pi0_TrueMass[it] = new TH1D(
        Form("%s_t2_true_mgg_%d", fNamePrefix.Data(), it),
        Form("Arm%d true M_{gg} (type II, %s tower)", this->kArmIndex + 1, tower_name[it].Data()), 1000, 130., 140.);

  // pi0 mass vs y (typeI)
  for (Int_t ir = 0; ir < fT1Pi0_Mass_vs_y.size(); ++ir)
    fT1Pi0_Mass_vs_y[ir] =
        new TH1D(Form("%s_t1_mgg_%d", fNamePrefix.Data(), ir),
                 Form("Arm%d M_{gg} (type I, rapidity %d)", this->kArmIndex + 1, ir), 1000, 0., 1000.);
  // pi0 mass vs y (typeII)
  for (Int_t it = 0; it < fT2Pi0_Mass_vs_y.size(); ++it)
    for (Int_t ir = 0; ir < fT2Pi0_Mass_vs_y[it].size(); ++ir)
      fT2Pi0_Mass_vs_y[it][ir] = new TH1D(
          Form("%s_t2_mgg_%d_%d", fNamePrefix.Data(), it, ir),
          Form("Arm%d M_{gg} (type II, %s tower, rapidity %d)", this->kArmIndex + 1, tower_name[it].Data(), ir), 1000,
          0., 1000.);
  // Pt vs y (typeI)
  for (Int_t ir = 0; ir < fT1Pi0_Pt_vs_y.size(); ++ir)
    fT1Pi0_Pt_vs_y[ir] = new TH1D(Form("%s_t1_pt_%d", fNamePrefix.Data(), ir),
                                  Form("Arm%d P_{T} (type I, rapidity %d)", this->kArmIndex + 1, ir), 20, 0., 2.);
  // Pt vs y (typeII)
  for (Int_t it = 0; it < fT2Pi0_Pt_vs_y.size(); ++it)
    for (Int_t ir = 0; ir < fT2Pi0_Pt_vs_y[it].size(); ++ir)
      fT2Pi0_Pt_vs_y[it][ir] =
          new TH1D(Form("%s_t2_pt_%d_%d", fNamePrefix.Data(), it, ir),
                   Form("Arm%d P_{T} (type II, %s tower, rapidity %d)", this->kArmIndex + 1, tower_name[it].Data(), ir),
                   20, 0., 2.);

  /* Neutrons */

  fNeutronBeamCentre2D =
      new TH2F(Form("%s_neutron_beam_centre", fNamePrefix.Data()), Form("Arm%d Neutron Hitmap", this->kArmIndex + 1),
               pos_nbin[0], pos_lowbin[0], pos_highbin[0], pos_nbin[1], pos_lowbin[1], pos_highbin[1]);

  // energy spectrum
  for (Int_t ir = 0; ir < fNeutronEnergy.size(); ++ir) {
    fNeutronEnergy[ir] = new TH1D(Form("%s_neutron_energy_%d", fNamePrefix.Data(), ir),
                                  Form("Arm%d Neutron Energy (rapidity %d)", this->kArmIndex + 1, ir),
                                  Int_t(fNeutronEbins[ir].size()) - 1, &fNeutronEbins[ir][0]);
  }

  // hitmap (2D & 1D)
  fNeutronHitmap2D =
      new TH2F(Form("%s_neutron_hitmap", fNamePrefix.Data()), Form("Arm%d Neutron Hitmap", this->kArmIndex + 1),
               pos_nbin[0], pos_lowbin[0], pos_highbin[0], pos_nbin[1], pos_lowbin[1], pos_highbin[1]);
  for (Int_t iv = 0; iv < fNeutronHitmap1D.size(); ++iv) {
    fNeutronHitmap1D[iv] = new TH1D(Form("%s_neutron_hitmap_%d", fNamePrefix.Data(), iv),
                                    Form("Arm%d Neutron Hitmap (%s view)", this->kArmIndex + 1, view_name[iv].Data()),
                                    pos_nbin[iv], pos_lowbin[iv], pos_highbin[iv]);
  }

  // L90%
  for (Int_t ir = 0; ir < fNeutronL90.size(); ++ir)
    fNeutronL90[ir] = new TH1D(Form("%s_neutron_l90_%d", fNamePrefix.Data(), ir),
                               Form("Arm%d L_{90%%} (rapidity %d)", this->kArmIndex + 1, ir), 45, 0., 45.);

  // true energy spectrum
  for (Int_t ir = 0; ir < fNeutronTrueEnergy.size(); ++ir) {
    fNeutronTrueEnergy[ir] =
        new TH1D(Form("%s_neutron_true_energy_%d", fNamePrefix.Data(), ir),
                 Form("Arm%d Neutron Energy (Arm%d, rapidity %d)", this->kArmIndex + 1, this->kArmIndex + 1, ir),
                 Int_t(fNeutronEbins[ir].size()) - 1, &fNeutronEbins[ir][0]);
  }

  // true hitmap (2D & 1D)
  fNeutronTrueHitmap2D = new TH2F(Form("%s_neutron_true_hitmap", fNamePrefix.Data()),
                                  Form("Arm%d Neutron True Hitmap", this->kArmIndex + 1), pos_nbin[0], pos_lowbin[0],
                                  pos_highbin[0], pos_nbin[1], pos_lowbin[1], pos_highbin[1]);
  for (Int_t iv = 0; iv < fNeutronTrueHitmap1D.size(); ++iv) {
    fNeutronTrueHitmap1D[iv] =
        new TH1D(Form("%s_neutron_true_hitmap_%d", fNamePrefix.Data(), iv),
                 Form("Arm%d Neutron True Hitmap (%s view)", this->kArmIndex + 1, view_name[iv].Data()), pos_nbin[iv],
                 pos_lowbin[iv], pos_highbin[iv]);
  }

  // energy resolution
  for (Int_t ir = 0; ir < fNeutronEnergy.size(); ++ir) {
    fNeutronEneRes[ir] =
        new TH2F(Form("%s_neutron_ene_res_%d", fNamePrefix.Data(), ir),
                 Form("Arm%d Neutron Energy Resolution (rapidity %d)", this->kArmIndex + 1, ir),
                 Int_t(fNeutronEbins[ir].size()) - 1, &fNeutronEbins[ir][0], ene_res_bin, ene_res_min, ene_res_max);
  }

  // position resolution
  for (Int_t ir = 0; ir < fNeutronPosRes.size(); ++ir)
    for (Int_t iv = 0; iv < fNeutronPosRes[ir].size(); ++iv) {
      fNeutronPosRes[ir][iv] = new TH2F(
          Form("%s_neutron_pos_res_%d_%d", fNamePrefix.Data(), ir, iv),
          Form("Arm%d Neutron Energy Resolution (%s view, rapidity %d)", this->kArmIndex + 1, view_name[iv].Data(), ir),
          Int_t(fNeutronEbins[ir].size()) - 1, &fNeutronEbins[ir][0], pos_res_bin, pos_res_min, pos_res_max);
    }

  // Eugenio: used to study shower width
  /*
  for (Int_t it = 0; it < fNeutronWtot.size(); ++it) {
          fNeutronXmax[it] = new TH1I(Form("%s_xmax_neutron", tower_name[it].Data()),
                          Form("%s tower; Si X_{max}", tower_name[it].Data()), 4, 0, 4);
          fNeutronYmax[it] = new TH1I(Form("%s_ymax_neutron", tower_name[it].Data()),
                          Form("%s tower; Si Y_{max}", tower_name[it].Data()), 4, 0, 4);
          fNeutronFint[it] = new TH1I(Form("%s_fint_neutron", tower_name[it].Data()),
                          Form("%s tower; GSO 1^{st} interaction", tower_name[it].Data()), 16, 0, 16);
          fNeutronWtot[it] = new TH1D(Form("%s_wtot_neutron", tower_name[it].Data()),
                          Form("%s tower; Si Total width [strip]", tower_name[it].Data()), 100, 0, 100);
          fNeutronWave[it] = new TH1D(Form("%s_wave_neutron", tower_name[it].Data()),
                          Form("%s tower; Si Initial width [strip]", tower_name[it].Data()), 100, 0, 100);
          fNeutronWlay[it] = new TH2D(Form("%s_wlay_neutron", tower_name[it].Data()),
                          Form("%s tower; Si X_{max} width [strip]; Si Y_{max} width [strip]", tower_name[it].Data()),
  100, 0, 100, 100, 0, 100); fNeutronL2D[it] = new TH1D(Form("%s_l2d_neutron", tower_name[it].Data()), Form("%s tower;
  L2D [X0]", tower_name[it].Data()), 44, 0, 44); fNeutronL2F[it] = new TH2D(Form("%s_l2f_neutron",
  tower_name[it].Data()), Form("%s tower; L2D [X0]; GSO 1^{st} interaction", tower_name[it].Data()), 44, 0, 44, 16, 0,
  16); fNeutronL2W[it] = new TH2D(Form("%s_l2w_neutron", tower_name[it].Data()), Form("%s tower; L2D [X0]; Si Total
  width [strip]", tower_name[it].Data()), 44, 0, 44, 100, 0, 100); fNeutronXmax[it]->SetLineColor(kRed);
          fNeutronYmax[it]->SetLineColor(kRed);
          fNeutronFint[it]->SetLineColor(kRed);
          fNeutronWtot[it]->SetLineColor(kRed);
          fNeutronWave[it]->SetLineColor(kRed);
          fNeutronWlay[it]->SetLineColor(kRed);
          fNeutronL2D[it]->SetLineColor(kRed);
          fNeutronL2F[it]->SetLineColor(kRed);
          fNeutronL2W[it]->SetLineColor(kRed);
  }
  for (Int_t it = 0; it < fPhotonWtot.size(); ++it) {
          fPhotonXmax[it] = new TH1I(Form("%s_xmax_photon", tower_name[it].Data()),
                          Form("%s tower; Si X_{max}", tower_name[it].Data()), 4, 0, 4);
          fPhotonYmax[it] = new TH1I(Form("%s_ymax_photon", tower_name[it].Data()),
                          Form("%s tower; Si Y_{max}", tower_name[it].Data()), 4, 0, 4);
          fPhotonFint[it] = new TH1I(Form("%s_fint_photon", tower_name[it].Data()),
                          Form("%s tower; GSO 1^{st} interaction", tower_name[it].Data()), 16, 0, 16);
          fPhotonWtot[it] = new TH1D(Form("%s_wtot_photon", tower_name[it].Data()),
                          Form("%s tower; Si Total width [strip]", tower_name[it].Data()), 100, 0, 100);
          fPhotonWave[it] = new TH1D(Form("%s_wave_photon", tower_name[it].Data()),
                          Form("%s tower; Si Initial width [strip]", tower_name[it].Data()), 100, 0, 100);
          fPhotonWlay[it] = new TH2D(Form("%s_wlay_photon", tower_name[it].Data()),
                          Form("%s tower; Si X_{max} width [strip]; Si Y_{max} width [strip]", tower_name[it].Data()),
  100, 0, 100, 100, 0, 100); fPhotonL2D[it] = new TH1D(Form("%s_l2d_photon", tower_name[it].Data()), Form("%s tower; L2D
  [X0]", tower_name[it].Data()), 44, 0, 44); fPhotonL2F[it] = new TH2D(Form("%s_l2f_photon", tower_name[it].Data()),
                          Form("%s tower; L2D [X0]; GSO 1^{st} interaction", tower_name[it].Data()), 44, 0, 44, 16, 0,
  16); fPhotonL2W[it] = new TH2D(Form("%s_l2w_photon", tower_name[it].Data()), Form("%s tower; L2D [X0]; Si Total width
  [strip]", tower_name[it].Data()), 44, 0, 44, 100, 0, 100); fPhotonXmax[it]->SetLineColor(kBlue);
          fPhotonYmax[it]->SetLineColor(kBlue);
          fPhotonFint[it]->SetLineColor(kBlue);
          fPhotonWtot[it]->SetLineColor(kBlue);
          fPhotonWave[it]->SetLineColor(kBlue);
          fPhotonWlay[it]->SetLineColor(kBlue);
          fPhotonL2D[it]->SetLineColor(kBlue);
          fPhotonL2F[it]->SetLineColor(kBlue);
          fPhotonL2W[it]->SetLineColor(kBlue);
  }
  */

  gROOT->cd();
  Utils::Printf(Utils::kPrintDebug, " Done.\n");
}

template <typename arman>
void Hist<arman>::MakeTrees(TFile *fout) {
  Utils::Printf(Utils::kPrintDebug, "Allocating trees...");
  fflush(stdout);

  fout->cd();

  Utils::AllocateVector4D(fPhotonL90Vector, this->kCalNtower, 2, fPhotonEbins[0].size() - 1, 0);
  // Utils::AllocateVector4D(fPhotonL90EntriesVector, this->kCalNtower, 2, fPhotonEbins[0].size()-1, 0);

  fL90Tree = new TTree("l90tree", "l90tree");
  for (int itower = 0; itower < this->kCalNtower; ++itower) {
    Int_t ir = itower == 0 ? 0 : 4;
    for (int ipid = 0; ipid < 2; ++ipid) {
      for (int ie = 0; ie < Int_t(fPhotonEbins[ir].size()) - 1; ++ie) {
        string bname;
        bname = Form("l90vec_%d_%d_%d", itower, ipid, ie);
        fL90Tree->Branch(bname.c_str(), &fPhotonL90Vector[itower][ipid][ie]);
        // bname = Form("l90entries_%d_%d_%d", itower, ipid, ie);
        // fL90Tree->Branch(bname.c_str(), &fPhotonL90EntriesVector[itower][ipid][ie]);
      }
    }
  }

  gROOT->cd();
  Utils::Printf(Utils::kPrintDebug, " Done.\n");
}

template <typename arman>
template <typename level3, typename eventcut>
void Hist<arman>::FillPhotonHistograms(level3 *lvl3, eventcut *evcut, McEvent *mcev) {
  Utils::Printf(Utils::kPrintDebugFull, "Hist::FillPhotonHistograms(%p, %p, %p)\n", (void *)lvl3, (void *)evcut,
                (void *)mcev);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    // inclusive E and L90
    Utils::Printf(Utils::kPrintDebugFull, " cut photon in tower %d\n", it);
    if (evcut->CutPhotonEventTower(lvl3, it)) {
      Utils::Printf(Utils::kPrintDebugFull, "  fill tower E\n");
      fPhotonTowerEnergy[it]->Fill(lvl3->fPhotonEnergy[it]);
      Utils::Printf(Utils::kPrintDebugFull, "  fill tower L90\n");
      fPhotonTowerL90[it]->Fill(lvl3->fPhotonL90[it]);
    }

    // beam centre
    Utils::Printf(Utils::kPrintDebugFull, " cut photon E, pos, MH, BPTX (and L90 > 20)\n");
    Utils::Printf(Utils::kPrintDebugFull, "  L90=%lf\n", lvl3->fPhotonL90[it]);
    if (evcut->CutPhotonEventTower(lvl3, it)) {
      // TODO [TrackProjection]: Check this change
      Int_t lx = lvl3->fPosMaxLayer[it][0];
      Int_t ly = lvl3->fPosMaxLayer[it][1];
      Double_t px = lvl3->fPhotonPosition[it][0];
      Double_t py = lvl3->fPhotonPosition[it][1];
      TVector3 colpos = CT::GetRecoCollisionCoordinates(this->kArmIndex, it, px, py, lx, ly);
      Double_t x_rec = -colpos.X();
      Double_t y_rec = colpos.Y();
      //
      Utils::Printf(Utils::kPrintDebugFull, "  fill beam centre\n");
      fPhotonBeamCentre2D->Fill(x_rec, y_rec);
    }
  }

  // rapidity loop
  for (Int_t ir = 0; ir < this->kPhotonNrap; ++ir) {
    const Int_t it = this->RapToTower(ir);

    if (mcev != NULL) mcev->CheckParticleInTower(this->kArmIndex, it, 10., 0.);

    Utils::Printf(Utils::kPrintDebugFull, " cut photon in rapidity %d\n", ir);
    if (evcut->CutPhotonEvent(lvl3, ir)) {
      Utils::Printf(Utils::kPrintDebugFull, "  fill photon E\n");
      fPhotonEnergy[ir]->Fill(lvl3->fPhotonEnergy[it]);
      // TODO [TrackProjection]: Check this change
      Int_t lx = lvl3->fPosMaxLayer[it][0];
      Int_t ly = lvl3->fPosMaxLayer[it][1];
      Double_t px = lvl3->fPhotonPosition[it][0];
      Double_t py = lvl3->fPhotonPosition[it][1];
      TVector3 col_pos_reco = CT::GetRecoCollisionCoordinates(this->kArmIndex, it, px, py, lx, ly);
      Double_t x_rec = -col_pos_reco.X();
      Double_t y_rec = col_pos_reco.Y();
      //
      Utils::Printf(Utils::kPrintDebugFull, "  fill photon hitmap\n");
      fPhotonHitmap2D->Fill(x_rec, y_rec);
      fPhotonHitmap1D[0]->Fill(x_rec);
      fPhotonHitmap1D[1]->Fill(y_rec);

      if (mcev != NULL) {
        if (mcev->fReference.size() > 0) {
          // energy resolution
          Double_t ene_res = lvl3->fPhotonEnergy[it] / mcev->fReference[0]->Energy() - 1.;
          fPhotonEneRes[ir]->Fill(mcev->fReference[0]->Energy(), ene_res);
          // TODO [TrackProjection]: Check this change
          TVector3 lhc_pos_true = mcev->fReference[0]->Position();
          TVector3 col_pos_true = CT::GetTrueCollisionCoordinates(this->kArmIndex, lhc_pos_true);
          // TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(this->kArmIndex, it, lhc_pos_true);
          Double_t x_true = -col_pos_true.X();
          Double_t y_true = col_pos_true.Y();
          //
          // position resolution
          fPhotonPosRes[ir][0]->Fill(mcev->fReference[0]->Energy(), x_rec - x_true);
          fPhotonPosRes[ir][1]->Fill(mcev->fReference[0]->Energy(), y_rec - y_true);

          // debug
          // printf("\n");
          // printf("reco_cal_pos   %lf %lf\n", lvl3->fPhotonPosition[it][0],lvl3->fPhotonPosition[it][1]);
          // printf("true_cal_pos  %lf %lf %lf\n", cal_pos_true.X(), cal_pos_true.Y(), cal_pos_true.Z());
          // printf("\n");
          // printf("true_col_pos %lf %lf %lf\n", col_pos_true.X(), col_pos_true.Y(), col_pos_true.Z());
          // printf("reco_col_pos  %lf %lf %lf\n", col_pos_reco.X(), col_pos_reco.Y(), col_pos_reco.Z());
          // getchar();
        }
      }
    }  // photon cuts

    // L90 vs rapidity
    Utils::Printf(Utils::kPrintDebugFull, " cut photon E, pos, rap, MH, BPTX\n");
    if (evcut->CutPhotonEventL90(lvl3, ir)) {
      Utils::Printf(Utils::kPrintDebugFull, " fill L90\n");
      fPhotonL90[ir]->Fill(lvl3->fPhotonL90[it]);
    }

    // Pirate information
    if (ir == 0 || ir == 4) {
      if (evcut->CutPhotonEvent(lvl3, ir)) {
        Utils::Printf(Utils::kPrintDebugFull, " fill pirate E\n");
        fPhotonPirateEnergy[it]->Fill(lvl3->fPhotonEnergy[it]);
      }
      if (evcut->CutPhotonEventL90(lvl3, ir)) {
        Utils::Printf(Utils::kPrintDebugFull, " fill pirate L90\n");
        auto matchingBin = std::upper_bound(fPhotonEbins[ir].begin(), fPhotonEbins[ir].end(), lvl3->fPhotonEnergy[it]);
        Int_t ib = std::distance(fPhotonEbins[ir].begin(), matchingBin - 1);
        if (ib < 0 && ib > fPhotonEbins[ir].size() - 1) continue;
        fPhotonPirateL90[it][ib]->Fill(lvl3->fPhotonL90[it]);
        if (mcev != NULL) {
          mcev->CheckParticleInTower(this->kArmIndex, it, 10., 0.);
          if (mcev->fReference.size() > 0) {
            int ipid = evcut->PhotonTruePIDCut(mcev, it) ? 0 : 1;
            fPhotonPirateL90Template[it][ipid][ib]->Fill(lvl3->fPhotonL90[it]);
            if (fSaveTree) {
              fPhotonL90Vector[it][ipid][ib].push_back(lvl3->fPhotonL90[it]);
            }
            if (ipid == 0) {
              fPhotonPirateEnergyPIDCorrected[it]->Fill(lvl3->fPhotonEnergy[it]);
              if (mcev->fReference[0]->Energy() > fPhotonEbins[ir][0])
                fPhotonPirateEnergyPIDCorrectedTrueThreshold[it]->Fill(lvl3->fPhotonEnergy[it]);
            }
          }
        }
      }
      if (evcut->CutPhotonEventTowerMultiHit(lvl3, it) && lvl3->fPhotonMultiHit[it]) {
        Utils::Printf(Utils::kPrintDebugFull, " fill multihit fraction\n");
        fPhotonMultihitFraction[it]->Fill(lvl3->fPhotonEnergyMH[it][0] + lvl3->fPhotonEnergyMH[it][1]);
      }
    }

    // MC truth
    if (mcev != NULL) {
      if (mcev->fReference.size() > 0) {
        if (evcut->CutTruePhotonEvent(mcev, ir)) {
          fPhotonTrueEnergy[ir]->Fill(mcev->fReference[0]->Energy());
          // TODO [TrackProjection]: Check this change
          TVector3 lhc_pos = mcev->fReference[0]->Position();
          TVector3 col_pos = CT::GetTrueCollisionCoordinates(this->kArmIndex, lhc_pos);
          Double_t x_true = -col_pos.X();
          Double_t y_true = col_pos.Y();
          //
          fPhotonTrueHitmap2D->Fill(x_true, y_true);
          fPhotonTrueHitmap1D[0]->Fill(x_true);
          fPhotonTrueHitmap1D[1]->Fill(y_true);
        }
      }
    }
    // MC truth

    // MC truth as final photon spectrum
    if (ir == 0 || ir == 4) {
      if (mcev != NULL) {
        if (mcev->fReference.size() > 0) {
          const Int_t nsize = mcev->fReference.size();
          for (Int_t ip = 0; ip < nsize; ++ip)
            if (evcut->CutTruePhotonEventInclusive(mcev, ir, ip)) {
              fPhotonInclusiveTrueEnergy[it]->Fill(mcev->fReference[ip]->Energy());
            }
        }
      }
    }
    // MC truth as final photon spectrum
  }  // rapidity loop
  Utils::Printf(Utils::kPrintDebugFull, "End of Hist::FillPhotonHistograms\n");
}

template <typename arman>
template <typename level3, typename eventcut>
void Hist<arman>::FillNeutronHistograms(level3 *lvl3, eventcut *evcut, McEvent *mcev) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    // beam centre
    Utils::Printf(Utils::kPrintDebugFull, " cut neutron E, pos, MH, BPTX (and L90 > 20)\n");
    Utils::Printf(Utils::kPrintDebugFull, "  L90=%lf\n", lvl3->fNeutronL90[it]);
    if (evcut->CutNeutronEventTower(lvl3, it)) {
      // TODO [TrackProjection]: Check this change
      Int_t lx = lvl3->fPosMaxLayer[it][0];
      Int_t ly = lvl3->fPosMaxLayer[it][1];
      Double_t px = lvl3->fNeutronPosition[it][0];
      Double_t py = lvl3->fNeutronPosition[it][1];
      TVector3 colpos = CT::GetRecoCollisionCoordinates(this->kArmIndex, it, px, py, lx, ly);
      Double_t x_rec = -colpos.X();
      Double_t y_rec = colpos.Y();
      //
      Utils::Printf(Utils::kPrintDebugFull, "  fill beam centre\n");
      fNeutronBeamCentre2D->Fill(x_rec, y_rec);
    }
  }

  for (Int_t ir = 0; ir < this->kNeutronNrap; ++ir) {
    const Int_t it = this->RapToTower(ir);

    if (mcev != NULL) mcev->CheckParticleInTower(this->kArmIndex, it, 10., 0.);

    Utils::Printf(Utils::kPrintDebugFull, "cut neutron\n");
    if (evcut->CutNeutronEvent(lvl3, ir)) {
      Utils::Printf(Utils::kPrintDebugFull, "fill neutron\n");
      fNeutronEnergy[ir]->Fill(lvl3->fNeutronEnergy[it]);
      // TODO [TrackProjection]: Check this change
      Int_t lx = lvl3->fPosMaxLayer[it][0];
      Int_t ly = lvl3->fPosMaxLayer[it][1];
      Double_t px = lvl3->fNeutronPosition[it][0];
      Double_t py = lvl3->fNeutronPosition[it][1];
      TVector3 colpos = CT::GetRecoCollisionCoordinates(this->kArmIndex, it, px, py, lx, ly);
      Double_t x_rec = -colpos.X();
      Double_t y_rec = colpos.Y();
      //
      fNeutronHitmap2D->Fill(x_rec, y_rec);
      fNeutronHitmap1D[0]->Fill(x_rec);
      fNeutronHitmap1D[1]->Fill(y_rec);

      if (mcev != NULL) {
        if (mcev->fReference.size() > 0) {
          // energy resolution
          Double_t ene_res = lvl3->fNeutronEnergy[it] / mcev->fReference[0]->Energy() - 1.;
          fNeutronEneRes[ir]->Fill(mcev->fReference[0]->Energy(), ene_res);
          // TODO [TrackProjection]: Check this change
          TVector3 lhc_pos = mcev->fReference[0]->Position();
          TVector3 col_pos = CT::GetTrueCollisionCoordinates(this->kArmIndex, lhc_pos);
          Double_t x_true = -col_pos.X();
          Double_t y_true = col_pos.Y();
          //
          // position resolution
          fNeutronPosRes[ir][0]->Fill(mcev->fReference[0]->Energy(), x_rec - x_true);
          fNeutronPosRes[ir][1]->Fill(mcev->fReference[0]->Energy(), y_rec - y_true);

          // debug
          // TVector3 coll_pos = CT::LhcToCollision(lhc_pos.X(), lhc_pos.Y());
          // TVector3 cal_pos = CT::LhcToCalorimeter(lhc_pos.X(), lhc_pos.Y(), this->kArmIndex, it);
          // printf("\n");
          // printf("true_cal_pos  %lf %lf %lf\n", cal_pos.X(), cal_pos.Y(), cal_pos.Z());
          // printf("rec_cal_pos   %lf %lf\n", lvl3->fNeutronPosition[it][0],lvl3->fNeutronPosition[it][1]);
          // printf("\n");
          // printf("true_lhc_pos  %lf %lf %lf\n", lhc_pos.X(), lhc_pos.Y(), lhc_pos.Z());
          // printf("true_det_pos  %lf %lf %lf\n", det_pos.X(), det_pos.Y(), det_pos.Z());
          // printf("true_coll_pos %lf %lf %lf\n", coll_pos.X(), coll_pos.Y(), coll_pos.Z());
          // printf("rec_coll_pos  %lf %lf %lf\n", lhcpos.X(), lhcpos.Y(), lhcpos.Z());
          // getchar();
        }
      }
    }
    if (evcut->NeutronEnergyCut(lvl3, it) && evcut->NeutronPositionCut(lvl3, it) &&
        evcut->NeutronRapidityCut(lvl3, ir) && evcut->NeutronMultiHitCut(lvl3, it) && evcut->BPTXCut(lvl3)) {
      fNeutronL90[ir]->Fill(lvl3->fNeutronL90[it]);
    }

    if (mcev != NULL) {
      if (mcev->fReference.size() > 0) {
        if (evcut->CutTrueNeutronEvent(mcev, ir)) {
          fNeutronTrueEnergy[ir]->Fill(mcev->fReference[0]->Energy());
          // TODO [TrackProjection]: Check this change
          TVector3 lhc_pos = mcev->fReference[0]->Position();
          TVector3 col_pos = CT::GetTrueCollisionCoordinates(this->kArmIndex, lhc_pos);
          Double_t x_true = -col_pos.X();
          Double_t y_true = col_pos.Y();
          //
          fNeutronTrueHitmap2D->Fill(x_true, y_true);
          fNeutronTrueHitmap1D[0]->Fill(x_true);
          fNeutronTrueHitmap1D[1]->Fill(y_true);
        }
      }
    }
  }  // rapidity loop
}

template <typename arman>
void Hist<arman>::FillPionHistograms(vector<PionData> *pdata, vector<PionData> *ptrue) {
  Utils::Printf(Utils::kPrintDebugFull, "Fill pion histograms\n");
  for (Int_t ip = 0; ip < pdata->size(); ++ip) {
    Utils::Printf(Utils::kPrintDebugFull, "pion %d\n", ip);
    Double_t mass = pdata->at(ip).GetMass();
    // Double_t y    = pdata->at(ip).GetRapidity();
    Double_t y = pdata->at(ip).GetEnergy() / 6500.;
    Double_t pt = pdata->at(ip).GetPt();
    /* Type I */
    if (pdata->at(ip).IsTypeI()) {
      Utils::Printf(Utils::kPrintDebugFull, "fill typeI\n");
      fT1Pi0_Mass->Fill(mass);
      if (pdata->at(ip).IsTrue()) fT1Pi0_Mass_TrueCut->Fill(mass);
      // find rapidity bin
      Double_t step = (this->kPionRapHighTypeI - this->kPionRapLowTypeI) / Double_t(this->kPionNrapTypeI);
      for (Int_t ir = 0; ir < this->kPionNrapTypeI; ++ir) {
        Utils::Printf(Utils::kPrintDebugFull, "rapidity %d\n", ir);
        Double_t ylow = this->kPionRapLowTypeI + ir * step;
        Double_t yhigh = ylow + step;
        if (y >= ylow && y < yhigh) {
          Utils::Printf(Utils::kPrintDebugFull, "fill mass (ir=%d) %lf\n", ir, mass);
          fT1Pi0_Mass_vs_y[ir]->Fill(mass);
          Utils::Printf(Utils::kPrintDebugFull, "fill pt (ir=%d) %lf\n", ir, pt);
          fT1Pi0_Pt_vs_y[ir]->Fill(pt);
          break;
        }
      }
    }
    /* Type II */
    else if (pdata->at(ip).IsTypeII()) {
      Utils::Printf(Utils::kPrintDebugFull, "fill typeII\n");
      Int_t it = pdata->at(ip).IsTypeII_ST() ? 0 : 1;
      fT2Pi0_Mass[it]->Fill(mass);
      if (pdata->at(ip).IsTrue()) fT2Pi0_Mass_TrueCut[it]->Fill(mass);
      // find rapidity bin
      Double_t step =
          (this->kPionRapHighTypeII[it] - this->kPionRapLowTypeII[it]) / Double_t(this->kPionNrapTypeII[it]);
      for (Int_t ir = 0; ir < this->kPionNrapTypeII[it]; ++ir) {
        Utils::Printf(Utils::kPrintDebugFull, "rapidity %d\n", ir);
        Double_t ylow = this->kPionRapLowTypeII[it] + ir * step;
        Double_t yhigh = ylow + step;
        if (y >= ylow && y < yhigh) {
          Utils::Printf(Utils::kPrintDebugFull, "fill mass (it=%d ,ir=%d) %lf\n", it, ir, mass);
          fT2Pi0_Mass_vs_y[it][ir]->Fill(mass);
          Utils::Printf(Utils::kPrintDebugFull, "fill pt (it=%d ,ir=%d) %lf\n", it, ir, pt);
          fT2Pi0_Pt_vs_y[it][ir]->Fill(pt);
          break;
        }
      }
    }
  }  // end pion loop

  for (Int_t ip = 0; ip < ptrue->size(); ++ip) {
    Double_t mass = ptrue->at(ip).GetMass();
    /* Type I */
    if (ptrue->at(ip).IsTypeI()) {
      fT1Pi0_TrueMass->Fill(mass);
    }
    /* Type II */
    else if (ptrue->at(ip).IsTypeII()) {
      Int_t it = ptrue->at(ip).IsTypeII_ST() ? 0 : 1;
      fT2Pi0_TrueMass[it]->Fill(mass);
    }
  }
}

template <typename arman>
void Hist<arman>::FillL90Vector() {
  if (fSaveTree) fL90Tree->Fill();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class Hist<Arm1AnPars>;
template class Hist<Arm2AnPars>;

template void Hist<Arm1AnPars>::FillPhotonHistograms<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars> >(
    Level3<Arm1RecPars> *lvl3, EventCut<Arm1AnPars, Arm1RecPars> *evcut, McEvent *mcev);
template void Hist<Arm2AnPars>::FillPhotonHistograms<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars> >(
    Level3<Arm2RecPars> *lvl3, EventCut<Arm2AnPars, Arm2RecPars> *evcut, McEvent *mcev);
template void Hist<Arm1AnPars>::FillNeutronHistograms<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars> >(
    Level3<Arm1RecPars> *lvl3, EventCut<Arm1AnPars, Arm1RecPars> *evcut, McEvent *mcev);
template void Hist<Arm2AnPars>::FillNeutronHistograms<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars> >(
    Level3<Arm2RecPars> *lvl3, EventCut<Arm2AnPars, Arm2RecPars> *evcut, McEvent *mcev);
}  // namespace nLHCf
