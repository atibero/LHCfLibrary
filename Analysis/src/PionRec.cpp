#include "PionRec.hh"

#include "Arm1AnPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "EventCut.hh"
#include "Level3.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(PionRec);
#endif

typedef Utils UT;
typedef CoordinateTransformation CT;

PionRec::PionRec() { ; }

PionRec::~PionRec() { ; }

template <typename level3, typename eventcut>
void PionRec::Reconstruct(level3 *lvl3, eventcut *cut, Int_t pid_eff) {
  ReconstructType1(lvl3, cut, &fResult[0], pid_eff);
  ReconstructType2(lvl3, 0, cut, &fResult[1], pid_eff);
  ReconstructType2(lvl3, 1, cut, &fResult[2], pid_eff);
}

/*-------------------*/
/*---- Pi0 Type1 ----*/
/*-------------------*/
template <typename level3, typename eventcut>
Int_t PionRec::ReconstructType1(level3 *lvl3, eventcut *evcut, PionData *result, Int_t pid_eff) {
  // Clear the result buffer
  result->Clear();

  // // Event Cut
  // if (!evcut->PionTypeICut(lvl3, pid_eff)) return 0;

  // Reconstruction
  Double_t ene[2] = {0., 0.};
  Double_t px[2] = {0., 0.};
  Double_t py[2] = {0., 0.};
  Double_t pz[2] = {0., 0.};
  TVector3 pos[2];
  for (Int_t it = 0; it < lvl3->kCalNtower; ++it) {
    // TODO [TrackProjection]: Check this change
    Int_t xlay = lvl3->fPosMaxLayer[it][0];
    Int_t ylay = lvl3->fPosMaxLayer[it][1];
    Double_t xpos = lvl3->fPhotonPosition[it][0];
    Double_t ypos = lvl3->fPhotonPosition[it][1];
    pos[it] = CT::GetRecoCollisionCoordinates(evcut->kArmIndex, it, xpos, ypos, xlay, ylay);
    //
    Double_t len = pos[it].Mag();
    ene[it] = lvl3->fPhotonEnergyLin[it];
    px[it] = ene[it] * pos[it].X() / len;
    py[it] = ene[it] * pos[it].Y() / len;
    pz[it] = ene[it] * pos[it].Z() / len;
  }

  result->SetEnergy(ene[0] + ene[1]);
  result->SetMomentum(TVector3(px[0] + px[1], py[0] + py[1], pz[0] + pz[1]));
  result->SetType(kTypeI);

  // Event Cut Flag and  Pi0 mass cut result
  UInt_t flag = 1;
  evcut->PionTypeICut(lvl3, pid_eff, &flag);
  flag |= evcut->PionMassCut(result->GetMass(), kTypeI) ? 0x2 : 0;
  result->fFlag = flag;

  // Photons info
  result->fPhotonTower[0] = 0;
  result->fPhotonTower[1] = 1;
  result->fPhotonEnergy[0] = lvl3->fPhotonEnergyLin[0];
  result->fPhotonEnergy[1] = lvl3->fPhotonEnergyLin[1];
  result->fPhotonPos[0][0] = pos[0].X();
  result->fPhotonPos[0][1] = pos[0].Y();
  result->fPhotonPos[1][0] = pos[1].X();
  result->fPhotonPos[1][1] = pos[1].Y();
  result->fPhotonR = TMath::Sqrt(TMath::Power(pos[0].X() - pos[1].X(), 2.) + TMath::Power(pos[0].Y() - pos[1].Y(), 2.));

  return 1;
}

template <typename level3, typename eventcut>
Int_t PionRec::ReconstructType2(level3 *lvl3, Int_t it, eventcut *evcut, PionData *result, Int_t pid_eff) {
  // Clear the result buffer
  result->Clear();

  // // Event Cut
  // if (!evcut->PionTypeIICut(lvl3, it, pid_eff)) return 0;

  // Reconstruction
  Double_t ene[2] = {0., 0.};
  Double_t px[2] = {0., 0.};
  Double_t py[2] = {0., 0.};
  Double_t pz[2] = {0., 0.};
  TVector3 pos[2];
  for (Int_t ip = 0; ip < 2; ++ip) {
    // TODO [TrackProjection]: Check this change
    Int_t xlay = lvl3->GetMaxLayerMH(it, 0);
    Int_t ylay = lvl3->GetMaxLayerMH(it, 1);
    Double_t xpos = lvl3->fPhotonPositionMH[it][0][ip];
    Double_t ypos = lvl3->fPhotonPositionMH[it][1][ip];
    pos[ip] = CT::GetRecoCollisionCoordinates(evcut->kArmIndex, it, xpos, ypos, xlay, ylay);
    //
    Double_t len = pos[ip].Mag();
    ene[ip] = lvl3->fPhotonEnergyMH[it][ip];
    px[ip] = ene[ip] * pos[ip].X() / len;
    py[ip] = ene[ip] * pos[ip].Y() / len;
    pz[ip] = ene[ip] * pos[ip].Z() / len;
  }
  result->SetEnergy(ene[0] + ene[1]);
  result->SetMomentum(TVector3(px[0] + px[1], py[0] + py[1], pz[0] + pz[1]));
  result->SetType(it == 0 ? kTypeII_ST : kTypeII_LT);

  // Pi0 mass cut result
  UInt_t flag = 1;
  evcut->PionTypeIICut(lvl3, it, pid_eff, &flag);
  flag |= evcut->PionMassCut(result->GetMass(), it == 0 ? kTypeII_ST : kTypeII_LT) ? 0x2 : 0;
  result->fFlag = flag;

  // Photons info
  result->fPhotonTower[0] = it;
  result->fPhotonTower[1] = it;
  result->fPhotonEnergy[0] = lvl3->fPhotonEnergyMH[it][0];
  result->fPhotonEnergy[1] = lvl3->fPhotonEnergyMH[it][1];
  result->fPhotonPos[0][0] = pos[0].X();
  result->fPhotonPos[0][1] = pos[0].Y();
  result->fPhotonPos[1][0] = pos[1].X();
  result->fPhotonPos[1][1] = pos[1].Y();
  result->fPhotonR = TMath::Sqrt(TMath::Power(pos[0].X() - pos[1].X(), 2.) + TMath::Power(pos[0].Y() - pos[1].Y(), 2.));

  return 1;
}

template <typename eventcut>
void PionRec::Reconstruct(McEvent *mcev, eventcut *cut) {
  ReconstructType1(mcev, cut, &fResult[0]);
  ReconstructType2(mcev, 0, cut, &fResult[1]);
  ReconstructType2(mcev, 1, cut, &fResult[2]);
}

/*----------------------*/
/*---- MC Pi0 Type1 ----*/
/*----------------------*/
template <typename eventcut>
Int_t PionRec::ReconstructType1(McEvent *mcev, eventcut *evcut, PionData *result) {
  // Clear the result buffer
  result->Clear();

  // Event Cut
  if (!evcut->PionTrueTypeICut(mcev)) return 0;

  // Reconstruction
  Double_t ene[2] = {0., 0.};
  Double_t px[2] = {0., 0.};
  Double_t py[2] = {0., 0.};
  Double_t pz[2] = {0., 0.};
  TVector3 pos[2];
  for (Int_t it = 0; it < evcut->kCalNtower; ++it) {
    if (mcev->fRefArm != evcut->kArmIndex || mcev->fRefTower != it || mcev->fReference.size() == 0)
      mcev->CheckParticleInTower(evcut->kArmIndex, it, evcut->kEnergyThr, 0.);
    // search most energetic photon in tower
    for (Int_t ip = 0; ip < mcev->fReference.size(); ++ip) {
      if (evcut->PhotonTruePIDCut(mcev, it, ip) && evcut->PhotonTrueEnergyCut(mcev, it, ip) &&
          evcut->PhotonTruePositionCut(mcev, it, ip)) {
        px[it] = mcev->fReference[ip]->MomentumX();
        py[it] = mcev->fReference[ip]->MomentumY();
        pz[it] = mcev->fReference[ip]->MomentumZ();
        ene[it] = mcev->fReference[ip]->Energy();
        // TODO [TrackProjection]: Check this change
        pos[it] = CT::GetTrueCollisionCoordinates(evcut->kArmIndex, mcev->fReference[ip]->Position());
        //
        // search a second particle
        for (Int_t kp = 0; kp < mcev->fReference.size(); ++kp) {
          const Double_t rel_thr = evcut->kArmIndex == Arm1Params::kArmIndex ? Arm1RecPars::kPeakRatioThrPhoton
                                                                             : Arm2RecPars::kPeakRatioThrPhoton;
          if (kp == ip) continue;
          if ((mcev->fReference[kp]->Energy() / mcev->fReference[ip]->Energy() > rel_thr) &&
              evcut->PhotonTruePositionCut(mcev, it, kp)) {
            result->fTrueMH = true;
            break;
          }
        }
        break;
      }
    }
  }
  Double_t mass = TMath::Sqrt(2 * ene[0] * ene[1] * (1 - pos[0].Unit() * pos[1].Unit()));
  // result->SetEnergy(ene[0] + ene[1]);
  result->SetEnergy(TMath::Sqrt(TMath::Power(px[0] + px[1], 2.) + TMath::Power(py[0] + py[1], 2.) +
                                TMath::Power(pz[0] + pz[1], 2.) + TMath::Power(mass, 2.)));
  result->SetMomentum(TVector3(px[0] + px[1], py[0] + py[1], pz[0] + pz[1]));
  result->SetType(kTypeI);
  result->SetTrue();

  // Pi0 mass cut result
  UInt_t flag = 1;
  flag |= evcut->PionMassCut(result->GetMass(), kTypeI) ? 0x2 : 0;
  result->fFlag = flag;

  // Photons info
  result->fPhotonTower[0] = 0;
  result->fPhotonTower[1] = 1;
  result->fPhotonEnergy[0] = ene[0];
  result->fPhotonEnergy[1] = ene[1];
  result->fPhotonPos[0][0] = pos[0].X();
  result->fPhotonPos[0][1] = pos[0].Y();
  result->fPhotonPos[1][0] = pos[1].X();
  result->fPhotonPos[1][1] = pos[1].Y();
  result->fPhotonR = TMath::Sqrt(TMath::Power(pos[0].X() - pos[1].X(), 2.) + TMath::Power(pos[0].Y() - pos[1].Y(), 2.));

  return 1;
}

/*----------------------*/
/*---- MC Pi0 Type2 ----*/
/*----------------------*/
template <typename eventcut>
Int_t PionRec::ReconstructType2(McEvent *mcev, Int_t it, eventcut *evcut, PionData *result) {
  // Clear the result buffer
  result->Clear();

  // Event Cut
  if (!evcut->PionTrueTypeIICut(mcev, it)) return 0;

  // Reconstruction
  Double_t ene[2] = {0., 0.};
  Double_t px[2] = {0., 0.};
  Double_t py[2] = {0., 0.};
  Double_t pz[2] = {0., 0.};
  TVector3 pos[2];
  if (mcev->fRefArm != evcut->kArmIndex || mcev->fRefTower != it || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(evcut->kArmIndex, it, evcut->kEnergyThr, 0.);
  // search first photon
  for (Int_t ip = 0; ip < mcev->fReference.size(); ++ip) {
    if (evcut->PhotonTruePIDCut(mcev, it, ip) && evcut->PhotonTrueEnergyCut(mcev, it, ip) &&
        evcut->PhotonTruePositionCut(mcev, it, ip)) {
      px[0] = mcev->fReference[ip]->MomentumX();
      py[0] = mcev->fReference[ip]->MomentumY();
      pz[0] = mcev->fReference[ip]->MomentumZ();
      ene[0] = mcev->fReference[ip]->Energy();
      // TODO [TrackProjection]: Check this change
      pos[0] = CT::GetTrueCollisionCoordinates(evcut->kArmIndex, mcev->fReference[ip]->Position());
      //
      // search second photon
      for (Int_t jp = ip + 1; jp < mcev->fReference.size(); ++jp) {
        const Double_t rel_thr = evcut->kArmIndex == Arm1Params::kArmIndex ? Arm1RecPars::kPeakRatioThrPhoton
                                                                           : Arm2RecPars::kPeakRatioThrPhoton;
        if (evcut->PhotonTruePIDCut(mcev, it, jp) &&
            (mcev->fReference[jp]->Energy() / mcev->fReference[ip]->Energy() > rel_thr) &&
            evcut->PhotonTruePositionCut(mcev, it, jp)) {
          px[1] = mcev->fReference[jp]->MomentumX();
          py[1] = mcev->fReference[jp]->MomentumY();
          pz[1] = mcev->fReference[jp]->MomentumZ();
          ene[1] = mcev->fReference[jp]->Energy();
          // TODO [TrackProjection]: Check this change
          pos[1] = CT::GetTrueCollisionCoordinates(evcut->kArmIndex, mcev->fReference[jp]->Position());
          //
          // search a third particle
          for (Int_t kp = 0; kp < mcev->fReference.size(); ++kp) {
            if (kp == ip || kp == jp) continue;
            if ((mcev->fReference[kp]->Energy() / mcev->fReference[ip]->Energy() > rel_thr) &&
                evcut->PhotonTruePositionCut(mcev, it, kp)) {
              result->fTrueMH = true;
              break;
            }
          }
          break;
        }
      }
      break;
    }
  }
  Double_t mass = TMath::Sqrt(2 * ene[0] * ene[1] * (1 - pos[0].Unit() * pos[1].Unit()));
  // result->SetEnergy(ene[0] + ene[1]);
  result->SetEnergy(TMath::Sqrt(TMath::Power(px[0] + px[1], 2.) + TMath::Power(py[0] + py[1], 2.) +
                                TMath::Power(pz[0] + pz[1], 2.) + TMath::Power(mass, 2.)));
  result->SetMomentum(TVector3(px[0] + px[1], py[0] + py[1], pz[0] + pz[1]));
  result->SetType(it == 0 ? kTypeII_ST : kTypeII_LT);
  result->SetTrue();

  // Pi0 mass cut result
  UInt_t flag = 1;
  flag |= evcut->PionMassCut(result->GetMass(), it == 0 ? kTypeII_ST : kTypeII_LT) ? 0x2 : 0;
  result->fFlag = flag;

  // Photons info
  result->fPhotonTower[0] = it;
  result->fPhotonTower[1] = it;
  result->fPhotonEnergy[0] = ene[0];
  result->fPhotonEnergy[1] = ene[1];
  result->fPhotonPos[0][0] = pos[0].X();
  result->fPhotonPos[0][1] = pos[0].Y();
  result->fPhotonPos[1][0] = pos[1].X();
  result->fPhotonPos[1][1] = pos[1].Y();
  result->fPhotonR = TMath::Sqrt(TMath::Power(pos[0].X() - pos[1].X(), 2.) + TMath::Power(pos[0].Y() - pos[1].Y(), 2.));

  return 1;
}

void PionRec::Clear() {
  for (int i = 0; i < kNtypes; ++i) fResult[i].Clear();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {

template void PionRec::Reconstruct<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars> >(
    Level3<Arm1RecPars> *, EventCut<Arm1AnPars, Arm1RecPars> *, Int_t);

template void PionRec::Reconstruct<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars> >(
    Level3<Arm2RecPars> *, EventCut<Arm2AnPars, Arm2RecPars> *, Int_t);

template Int_t PionRec::ReconstructType1<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars> >(
    Level3<Arm1RecPars> *, EventCut<Arm1AnPars, Arm1RecPars> *, PionData *, Int_t);

template Int_t PionRec::ReconstructType1<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars> >(
    Level3<Arm2RecPars> *, EventCut<Arm2AnPars, Arm2RecPars> *, PionData *, Int_t);

template Int_t PionRec::ReconstructType2<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars> >(
    Level3<Arm1RecPars> *, Int_t, EventCut<Arm1AnPars, Arm1RecPars> *, PionData *, Int_t);

template Int_t PionRec::ReconstructType2<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars> >(
    Level3<Arm2RecPars> *, Int_t, EventCut<Arm2AnPars, Arm2RecPars> *, PionData *, Int_t);

template void PionRec::Reconstruct<EventCut<Arm1AnPars, Arm1RecPars> >(McEvent *, EventCut<Arm1AnPars, Arm1RecPars> *);

template void PionRec::Reconstruct<EventCut<Arm2AnPars, Arm2RecPars> >(McEvent *, EventCut<Arm2AnPars, Arm2RecPars> *);

template Int_t PionRec::ReconstructType1<EventCut<Arm1AnPars, Arm1RecPars> >(McEvent *,
                                                                             EventCut<Arm1AnPars, Arm1RecPars> *,
                                                                             PionData *);

template Int_t PionRec::ReconstructType1<EventCut<Arm2AnPars, Arm2RecPars> >(McEvent *,
                                                                             EventCut<Arm2AnPars, Arm2RecPars> *,
                                                                             PionData *);

template Int_t PionRec::ReconstructType2<EventCut<Arm1AnPars, Arm1RecPars> >(McEvent *, Int_t,
                                                                             EventCut<Arm1AnPars, Arm1RecPars> *,
                                                                             PionData *);

template Int_t PionRec::ReconstructType2<EventCut<Arm2AnPars, Arm2RecPars> >(McEvent *, Int_t,
                                                                             EventCut<Arm2AnPars, Arm2RecPars> *,
                                                                             PionData *);

}  // namespace nLHCf
