#include "Tree.hh"

#include <TMath.h>
#include <TROOT.h>

#include <sstream>

#include "Arm1AnPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(Tree);
#endif

typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename arman>
Tree<arman>::Tree(TString name, TString option) : fTreeName(name), fTreeOption(option) {}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename arman>
Tree<arman>::~Tree() {
  ClearMemory();
}

/*----------------------*/
/*--- Initialisation ---*/
/*----------------------*/
template <typename arman>
void Tree<arman>::Initialise(TFile *fout) {
  SetFlags();
  MakeTree(fout);
  currentEntry = 0;
}

template <typename arman>
void Tree<arman>::ClearMemory() {}

template <typename arman>
void Tree<arman>::SetFlags() {
  armnum = -1;
  datamc = true;
  neupho = true;
  l3tvar = false;
  siedep = false;
  debug1 = false;
  debug2 = false;
  debugs = false;

  istringstream iss(fTreeOption.Data());
  vector<string> level((istream_iterator<string>(iss)), istream_iterator<string>());
  for (int i = 0; i < (int)level.size(); i++) {
    transform(level[i].begin(), level[i].end(), level[i].begin(), ::tolower);

    // DEBUG LEVEL
    if (level[i] == "arm1")
      armnum = Arm1Params::kArmIndex;
    else if (level[i] == "arm2")
      armnum = Arm2Params::kArmIndex;
    else if (level[i] == "data")
      datamc = true;
    else if (level[i] == "mc")
      datamc = false;
    else if (level[i] == "neutron")
      neupho = true;
    else if (level[i] == "l3tvar")
      l3tvar = true;
    else if (level[i] == "photon")
      neupho = false;
    else if (level[i] == "siedep")
      siedep = true;
    else if (level[i] == "debug1")
      debug1 = true;
    else if (level[i] == "debug2")
      debug2 = true;
    else if (level[i] == "debugs")
      debugs = true;
    else {
      Utils::Printf(Utils::kPrintError, "Invalid tree selection option \"%s\"\n", level[i].c_str());
      exit(EXIT_FAILURE);
    }
  }

  if ((armnum != Arm1Params::kArmIndex) && (armnum != Arm2Params::kArmIndex)) {
    Utils::Printf(Utils::kPrintError, "Please specify a valid detector number : Exit...");
    exit(EXIT_FAILURE);
  }

  Utils::Printf(Utils::kPrintInfo, "Filling output tree with the following options\n");

  if (armnum == Arm1Params::kArmIndex) Utils::Printf(Utils::kPrintInfo, "- Arm1\n");
  if (armnum == Arm2Params::kArmIndex) Utils::Printf(Utils::kPrintInfo, "- Arm2\n");
  if (datamc)
    Utils::Printf(Utils::kPrintInfo, "- Experimental input file\n");
  else
    Utils::Printf(Utils::kPrintInfo, "- Simulation input file\n");
  if (neupho)
    Utils::Printf(Utils::kPrintInfo, "- Neutron output file\n");
  else
    Utils::Printf(Utils::kPrintInfo, "- Photon output file\n");
  if (l3tvar) Utils::Printf(Utils::kPrintInfo, "- Adding L3T trigger information\n");
  if (siedep) Utils::Printf(Utils::kPrintInfo, "- Adding silicon charge\n");
  if (debug1) Utils::Printf(Utils::kPrintInfo, "- Debug level 1\n");
  if (debug2) Utils::Printf(Utils::kPrintInfo, "- Debug level 2\n");
  if (debugs) Utils::Printf(Utils::kPrintInfo, "- Debug silicon\n");
}

template <typename arman>
void Tree<arman>::MakeTree(TFile *fout) {
  Utils::Printf(Utils::kPrintDebug, "Allocating tree...");
  fflush(stdout);

  fout->cd();

  // Create a TTree
  tree = new TTree(fTreeName, fTreeName);
  tree->Branch("entry", &ventry);
  tree->Branch("run", &vrun);
  tree->Branch("event", &vevent);
  if (datamc) {
    tree->Branch("time", &vtime);
    tree->Branch("beam_flag", &vbeam_flag);
    tree->Branch("l2t", &vl2t);
    tree->Branch("gpio0", &vgpio0);
    tree->Branch("counter", &vcounter);
  }
  tree->Branch("tower", &vtower);
  tree->Branch("trigger", &vtrigger);
  tree->Branch("multi", &vmulti);
  tree->Branch("energy", &venergy);
  tree->Branch("maxposdetx", &vmaxposdetx);
  tree->Branch("maxposdety", &vmaxposdety);
  tree->Branch("posrecx", &vposrecx);
  tree->Branch("posrecy", &vposrecy);
  tree->Branch("lhcx", &vlhcx);
  tree->Branch("lhcy", &vlhcy);
  tree->Branch("pid", &vpid);
  if (neupho)
    tree->Branch("l2d", &vl2d);
  else
    tree->Branch("l90", &vl90);
  if (l3tvar) tree->Branch("l3t", &vl3t);
  if (siedep) {
    tree->Branch("silcharge", &vsilcharge);
  }
  if (!datamc) {
    tree->Branch("hit", &vthit);
    tree->Branch("user_flag", &vuser_flag);
    tree->Branch("code", &vcode);
    tree->Branch("charge", &vcharge);
    tree->Branch("tenergy", &vtenergy);
    tree->Branch("tmomentum", &vtmomentum);
    tree->Branch("tlhcx", &vtlhcx);
    tree->Branch("tlhcy", &vtlhcy);
    tree->Branch("tposx", &vtposx);
    tree->Branch("tposy", &vtposy);
    tree->Branch("hit_all", &vthit_all);
    tree->Branch("user_flag_all", &vuser_flag_all);
    tree->Branch("code_all", &vcode_all);
    tree->Branch("charge_all", &vcharge_all);
    tree->Branch("tenergy_all", &vtenergy_all);
    tree->Branch("tmomentum_all", &vtmomentum_all);
    tree->Branch("tlhcx_all", &vtlhcx_all);
    tree->Branch("tlhcy_all", &vtlhcy_all);
    tree->Branch("tposx_all", &vtposx_all);
    tree->Branch("tposy_all", &vtposy_all);
  }
  if (debug1) tree->Branch("calderec", &vcalderec);
  if (debug2) tree->Branch("calde", &vcalde);
  if (debugs) tree->Branch("silicon", &vsilicon);

  gROOT->cd();
  Utils::Printf(Utils::kPrintDebug, " Done.\n");
}

template <typename arman>
template <typename level1, typename level2, typename level3, typename mcevent, typename eventcut>
void Tree<arman>::FillTree(level1 *lvl1, level2 *lvl2, level3 *lvl3, mcevent *mc, eventcut *evcut) {
  /***********************************************************************
   * NB: In case of neutron analysis we always look for singlehit events *
   ***********************************************************************/
  for (int itower = 0; itower < this->kCalNtower; itower++) {
    /* ============= *
     * Clear Vectors *
     * ============= */
    ventry.clear();
    vrun.clear();
    vevent.clear();
    if (datamc) {
      vtime.clear();
      vbeam_flag.clear();
      vl2t.clear();
      vgpio0.clear();
      vcounter.clear();
    }
    vtower.clear();
    vtrigger.clear();
    vmulti.clear();
    venergy.clear();
    vmaxposdetx.clear();
    vmaxposdety.clear();
    vposrecx.clear();
    vposrecy.clear();
    vlhcx.clear();
    vlhcy.clear();
    vpid.clear();
    if (neupho)
      vl2d.clear();
    else
      vl90.clear();
    vl3t.clear();
    vsilcharge.clear();
    if (!datamc) {
      vthit.clear();
      vuser_flag.clear();
      vcode.clear();
      vcharge.clear();
      vtenergy.clear();
      vtmomentum.clear();
      vtmomentum.clear();
      vtmomentum.clear();
      vtlhcx.clear();
      vtlhcy.clear();
      vtposx.clear();
      vtposy.clear();

      vthit_all.clear();
      vuser_flag_all.clear();
      vcode_all.clear();
      vcharge_all.clear();
      vtenergy_all.clear();
      vtmomentum_all.clear();
      vtmomentum_all.clear();
      vtmomentum_all.clear();
      vtlhcx_all.clear();
      vtlhcy_all.clear();
      vtposx_all.clear();
      vtposy_all.clear();
    }
    if (debug1) vcalderec.clear();
    if (debug2) vcalde.clear();
    if (debugs) vsilicon.clear();

    /* ==================== *
     * Initialize Variables *
     * ==================== */
    int entry = currentEntry;
    ventry.push_back(entry);
    int tower = itower;

    int run;
    int event;
    int l2t;
    int time[this->kTime];
    bool beam_flag;
    unsigned int gpio0[this->kNflag];
    unsigned int counter[this->kNcounter];
    bool trigger;
    bool nhit;
    double energy;
    int lay_tow[this->kPosNview];
    double pos_tow[this->kPosNview];
    double pos_lhc[this->kPosNview];
    double pid;
    double l2d;
    double l90;
    unsigned int l3t;
    int ntruehit;
    double calrec[this->kCalNlayer];
    double calraw[this->kCalNlayer][this->kCalNrange];
    double silicon[this->kPosNlayer][this->kPosNview][this->kPosNchannel[0]];
    double silcharge[this->kCalNtower][this->kPosNlayer][this->kPosNview];

    run = 0;
    event = 0;
    l2t = 0;
    for (int i = 0; i < this->kTime; ++i) time[i] = 0;
    beam_flag = false;
    for (int i = 0; i < this->kNflag; ++i) gpio0[i] = 0;
    for (int i = 0; i < this->kNcounter; ++i) counter[i] = 0;
    trigger = false;
    nhit = false;
    energy = 0;
    for (int i = 0; i < this->kPosNview; ++i) {
      lay_tow[i] = 0;
      pos_tow[i] = 0;
      pos_lhc[i] = 0;
    }
    pid = 0;
    l2d = 0;
    l90 = 0;
    ntruehit = 0;
    for (int i = 0; i < this->kCalNlayer; ++i) {
      calrec[i] = 0;
      for (int j = 0; j < this->kCalNrange; ++j) calraw[i][j] = 0;
    }
    if (debugs)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ilayer++)
        for (int ixy = 0; ixy < this->kPosNview; ixy++)
          for (int ichannel = 0; ichannel < this->kPosNchannel[0]; ichannel++) silicon[ilayer][ixy][ichannel] = 0.;
    if (l3tvar) l3t = 0;
    if (siedep)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ilayer++)
        for (int ixy = 0; ixy < this->kPosNview; ixy++) {
          silcharge[itower][ilayer][ixy] = 0.;
        }

    /* ========================= *
     * Fill MonteCarlo Variables *
     * ========================= */
    if (mc) {
      if (!datamc) {
        run = mc->fRun;
        vrun.push_back(run);
        event = 1 + mc->fEvent;
        vevent.push_back(event);
        ntruehit = mc->CheckParticleInTower(armnum, itower, 1., 0.);
        vthit.push_back(ntruehit > 2 ? 2 : ntruehit);
        vthit_all.push_back(ntruehit);
        if (ntruehit > 0) {
          for (int iP = 0; iP < ntruehit; ++iP) {
            int userflag = mc->fReference[iP]->fUserCode;
            int code, subcode, charge;
            UtilsMc::PdgToEpicsCode(mc->fReference[iP]->fPdgCode, code, subcode, charge);
            double true_energy = mc->fReference[iP]->fMomentum.Energy();
            double true_momentum[3] = {mc->fReference[iP]->fMomentum.X(), mc->fReference[iP]->fMomentum.Y(),
                                       mc->fReference[iP]->fMomentum.Z()};
            double true_pos_lhc[this->kPosNview];
            double true_pos_tow[this->kPosNview];
            // TODO [TrackProjection]: Check this change
            TVector3 lhc_pos = mc->fReference[iP]->Position();
            //
            TVector3 col_pos = CT::GetTrueCollisionCoordinates(this->kArmIndex, lhc_pos);
            true_pos_lhc[0] = col_pos.X();
            true_pos_lhc[1] = col_pos.Y();
            TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(this->kArmIndex, itower, lhc_pos);
            true_pos_tow[0] = cal_pos.X();
            true_pos_tow[1] = cal_pos.Y();
            //
            if (iP > 1)
              if (true_energy > vtenergy_all[vtenergy_all.size() - 1])
                Utils::Printf(Utils::kPrintError, "Particle in not descending energy order: %f after %f: Exit...\n",
                              true_energy, vtenergy_all[vtenergy_all.size() - 1]);
            if (iP < 2) {
              vuser_flag.push_back(userflag);
              vcode.push_back(code);
              vcharge.push_back(charge);
              vtenergy.push_back(true_energy);
              vtmomentum.push_back(true_momentum[0]);
              vtmomentum.push_back(true_momentum[1]);
              vtmomentum.push_back(true_momentum[2]);
              vtlhcx.push_back(true_pos_lhc[0]);
              vtlhcy.push_back(true_pos_lhc[1]);
              vtposx.push_back(true_pos_tow[0]);
              vtposy.push_back(true_pos_tow[1]);
            }
            vuser_flag_all.push_back(userflag);
            vcode_all.push_back(code);
            vcharge_all.push_back(charge);
            vtenergy_all.push_back(true_energy);
            vtmomentum_all.push_back(true_momentum[0]);
            vtmomentum_all.push_back(true_momentum[1]);
            vtmomentum_all.push_back(true_momentum[2]);
            vtlhcx_all.push_back(true_pos_lhc[0]);
            vtlhcy_all.push_back(true_pos_lhc[1]);
            vtposx_all.push_back(true_pos_tow[0]);
            vtposy_all.push_back(true_pos_tow[1]);
          }
        } else {
          vuser_flag.push_back(-1000);
          vcode.push_back(-1000);
          vcharge.push_back(-1000);
          vtenergy.push_back(-1000.);
          vtmomentum.push_back(-1000.);
          vtmomentum.push_back(-1000.);
          vtmomentum.push_back(-1000.);
          vtlhcx.push_back(-1000.);
          vtlhcy.push_back(-1000.);
          vtposx.push_back(-1000.);
          vtposy.push_back(-1000.);
          vuser_flag_all.push_back(-1000);
          vcode_all.push_back(-1000);
          vcharge_all.push_back(-1000);
          vtenergy_all.push_back(-1000.);
          vtmomentum_all.push_back(-1000.);
          vtmomentum_all.push_back(-1000.);
          vtmomentum_all.push_back(-1000.);
          vtlhcx_all.push_back(-1000.);
          vtlhcy_all.push_back(-1000.);
          vtposx_all.push_back(-1000.);
          vtposy_all.push_back(-1000.);
        }
      }
    }

    /* ============================ *
     * Fill Reconstructed Variables *
     * ============================ */
    if (lvl3) {
      if (datamc) {
        run = lvl3->fRun;
        event = lvl3->fEvent;
        l2t = -1;
        beam_flag = false;
        for (int ie = 0; ie < this->kTime; ++ie) time[ie] = lvl3->fTime[ie];
        if ((lvl3->fFlag[0] >> 12) & 0x1) beam_flag = true;
        if ((lvl3->fFlag[0] & (int)(0x0020)) || (lvl3->fFlag[0] & (int)(0x0010)))
          l2t = 2 * (lvl3->fFlag[0] & (int)(0x0020)) + (lvl3->fFlag[0] & (int)(0x0010));
        for (int ie = 0; ie < this->kNflag; ++ie) gpio0[ie] = lvl3->fFlag[ie];
        for (int ie = 0; ie < this->kNcounter; ++ie) counter[ie] = lvl3->fCounter[ie];
      }
      trigger = neupho ? lvl3->fSoftwareTrigger[itower] : lvl3->fSoftwareTrigger[itower];
      nhit = neupho ? lvl3->fNeutronMultiHit[itower] : lvl3->fPhotonMultiHit[itower];
      energy = neupho ? lvl3->fNeutronEnergy[itower] : lvl3->fPhotonEnergy[itower];
      for (int ie = 0; ie < this->kPosNview; ++ie) {
        lay_tow[ie] = lvl3->fPosMaxLayer[itower][ie];
      }
      // TODO [TrackProjection]: Check this change
      pos_tow[0] = lvl3->fNeutronPosition[itower][0];
      pos_tow[1] = lvl3->fNeutronPosition[itower][1];
      TVector3 col_pos =
          CT::GetRecoCollisionCoordinates(this->kArmIndex, itower, pos_tow[0], pos_tow[1], lay_tow[0], lay_tow[1]);
      pos_lhc[0] = col_pos.X();
      pos_lhc[1] = col_pos.Y();
      //
      // printf("%f %f\n", pos_lhc[0], pos_lhc[1]);
      if (lvl3->fIsPhoton[itower]) pid = +1.;
      if (lvl3->fIsNeutron[itower]) pid = -1.;
      l2d = lvl3->fNeutronL90[itower] - 0.25 * lvl3->fNeutronL20[itower];
      l90 = lvl3->fPhotonL90[itower];
      if (l3tvar) {
        if (lvl3->IsDaqFlagBeam() && lvl3->IsDaqFlagOtherArmShowerTrg()) l3t += 0x1;
        if (lvl3->IsDaqFlagBeam() && lvl3->IsShowerTrg()) l3t += 0x2;
      }
      if (siedep)
        for (int ilayer = 0; ilayer < this->kPosNlayer; ilayer++)
          for (int ixy = 0; ixy < this->kPosNview; ixy++) {
            silcharge[itower][ilayer][ixy] = lvl3->fSiliconCharge[itower][ilayer][ixy];
          }
    }
    if (lvl2)
      if (debug1)
        for (int ilayer = 0; ilayer < this->kCalNlayer; ilayer++) calrec[ilayer] = lvl2->fCalorimeter[itower][ilayer];
    if (lvl1)
      if (debug2)
        for (int ilayer = 0; ilayer < this->kCalNlayer; ilayer++)
          for (int irange = 0; irange < this->kCalNrange; irange++)
            calraw[ilayer][irange] = lvl1->fCalorimeter[itower][ilayer][irange];
    if (debugs)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ilayer++)
        for (int ixy = 0; ixy < this->kPosNview; ixy++)
          for (int ichannel = 0; ichannel < this->kPosNchannel[0]; ichannel++)
            silicon[ilayer][ixy][ichannel] = lvl2->fPosDet[0][ilayer][ixy][ichannel][1];
    if (datamc) {
      vrun.push_back(run);
      vevent.push_back(event);
      for (int ie = 0; ie < this->kTime; ++ie) vtime.push_back(time[ie]);
      vbeam_flag.push_back(beam_flag);
      vl2t.push_back(l2t);
      for (int ie = 0; ie < this->kNflag; ++ie) vgpio0.push_back(gpio0[ie]);
      for (int ie = 0; ie < this->kNcounter; ++ie) vcounter.push_back(counter[ie]);
    }
    vtower.push_back(itower);
    vtrigger.push_back(trigger);
    vmulti.push_back(nhit);
    venergy.push_back(energy);
    vmaxposdetx.push_back(lay_tow[0]);
    vmaxposdety.push_back(lay_tow[1]);
    vposrecx.push_back(pos_tow[0]);
    vposrecy.push_back(pos_tow[1]);
    vlhcx.push_back(pos_lhc[0]);
    vlhcy.push_back(pos_lhc[1]);
    vpid.push_back(pid);
    if (neupho)
      vl2d.push_back(l2d);
    else
      vl90.push_back(l90);
    if (l3tvar) vl3t.push_back(l3t);
    if (siedep)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ilayer++)
        for (int ixy = 0; ixy < this->kPosNview; ixy++) {
          vsilcharge.push_back(silcharge[itower][ilayer][ixy]);
        }

    if (debug1)
      for (int ilayer = 0; ilayer < this->kCalNlayer; ilayer++) vcalderec.push_back(calrec[ilayer]);
    if (debug2)
      for (int ilayer = 0; ilayer < this->kCalNlayer; ilayer++)
        for (int irange = 0; irange < this->kCalNrange; irange++) vcalde.push_back(calraw[ilayer][irange]);
    if (debugs)
      for (int ilayer = 0; ilayer < this->kPosNlayer; ilayer++)
        for (int ixy = 0; ixy < this->kPosNview; ixy++)
          for (int ichannel = 0; ichannel < this->kPosNchannel[0]; ichannel++)
            vsilicon.push_back(silicon[ilayer][ixy][ichannel]);
    /* ================ *
     * Fill Output Tree *
     * ================ */
    tree->Fill();
  }
}

template <typename arman>
void Tree<arman>::SetCurrentEntry(int entry) {
  currentEntry = entry;
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class Tree<Arm1AnPars>;
template class Tree<Arm2AnPars>;

template void Tree<Arm1AnPars>::FillTree<Level1<Arm1Params>, Level2<Arm1Params>, Level3<Arm1RecPars>, McEvent,
                                         EventCut<Arm1AnPars, Arm1RecPars> >(Level1<Arm1Params> *lvl1,
                                                                             Level2<Arm1Params> *lvl2,
                                                                             Level3<Arm1RecPars> *lvl3,
                                                                             McEvent *mcevent,
                                                                             EventCut<Arm1AnPars, Arm1RecPars> *evcut);
template void Tree<Arm2AnPars>::FillTree<Level1<Arm2Params>, Level2<Arm2Params>, Level3<Arm2RecPars>, McEvent,
                                         EventCut<Arm2AnPars, Arm2RecPars> >(Level1<Arm2Params> *lvl1,
                                                                             Level2<Arm2Params> *lvl2,
                                                                             Level3<Arm2RecPars> *lvl3,
                                                                             McEvent *mcevent,
                                                                             EventCut<Arm2AnPars, Arm2RecPars> *evcut);
}  // namespace nLHCf
