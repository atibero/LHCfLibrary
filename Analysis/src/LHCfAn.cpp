#include "LHCfAn.hh"

#include <dirent.h>

#include <cstdio>
using namespace std;

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "CoordinateTransformation.hh"
#include "DataModification.hh"
#include "EventCut.hh"
#include "OldInterface.hh"
#include "PionRec.hh"
#include "TROOT.h"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CoordinateTransformation CT;

#if !defined(__CINT__)
ClassImp(LHCfAn);
#endif

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
LHCfAn::LHCfAn()
    : fInputDir("."),
      fFirstRun(-1),
      fLastRun(-1),
      fOutputName("analyzed.root"),
      fPhotonEnable(true),
      fNeutronEnable(true),
      fPionEnable(true),
      fTreeEnable(false),
      fPhotonCuts("pid energy position rapidity multi-hit bptx"),
      fNeutronCuts("pid energy position rapidity bptx"),
      fModificationEnable(true),
      fOldFlag(false) {
  fInputTree = NULL;
  fInputEv = NULL;

  fOutputFile = NULL;

  fHistArm1 = NULL;
  fHistArm2 = NULL;
  fOldHistArm1 = NULL;
  fOldHistArm2 = NULL;

  AllocateVectors();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
LHCfAn::~LHCfAn() {
  delete fInputTree;
  delete fInputEv;

  delete fOutputFile;

  delete fHistArm1;
  delete fHistArm2;
  delete fOldHistArm1;
  delete fOldHistArm2;
}

/*------------------------------*/
/*--- Allocate/clear vectors ---*/
/*------------------------------*/
void LHCfAn::AllocateVectors() {
  UT::AllocateVector1D(fArmEnable, this->kNarm);
  UT::ClearVector1D(fArmEnable, true);
}

/*-----------------------*/
/*--- Make histograms ---*/
/*-----------------------*/
void LHCfAn::MakeHistograms() {
  UT::Printf(UT::kPrintInfo, "Creating histograms...");

  fflush(stdout);
  if (fArmEnable[Arm1Params::kArmIndex]) {
    fHistArm1 = new Hist<Arm1AnPars>("a1");
    fHistArm1->Initialise(fOutputFile, fTreeEnable);

    if (fOldFlag) {
      fOldHistArm1 = new Hist<Arm1AnPars>("old_a1");
      fOldHistArm1->Initialise(fOutputFile, fTreeEnable);
    }
  }

  if (fArmEnable[Arm2Params::kArmIndex]) {
    fHistArm2 = new Hist<Arm2AnPars>("a2");
    fHistArm2->Initialise(fOutputFile, fTreeEnable);

    if (fOldFlag) {
      fOldHistArm2 = new Hist<Arm2AnPars>("old_a2");
      fOldHistArm2->Initialise(fOutputFile, fTreeEnable);
    }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-----------------*/
/*--- Book Tree ---*/
/*-----------------*/
void LHCfAn::BookTree(TString dataFormat, TString treeName, TString treeOption) {
  UT::Printf(UT::kPrintInfo, "Creating tree...");
  fflush(stdout);

  if (dataFormat == "eugenio") {
    if (fArmEnable[Arm1Params::kArmIndex]) {
      UT::Printf(UT::kPrintError, "Unavailable Arm1 tree output format corresponding to \"%s\": Exit...",
                 dataFormat.Data());
      exit(EXIT_FAILURE);
    }

    if (fArmEnable[Arm2Params::kArmIndex]) {
      fTreeArm2 = new Tree<Arm2AnPars>(treeName, treeOption);
      fTreeArm2->Initialise(fOutputFile);
    }
  } else {
    UT::Printf(UT::kPrintError, "Unknown tree output format corresponding to \"%s\": Exit...", dataFormat.Data());
    exit(EXIT_FAILURE);
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*------------------------------*/
/*--- Main analysis function ---*/
/*------------------------------*/
void LHCfAn::Analysis() {
  /* Initialization */
  SetInputTree();
  OpenOutputFile();

  /* Build histograms */
  MakeHistograms();

  /* Event loop */
  EventLoop();

  /* Write to file */
  WriteToOutput();
  CloseFiles();
}

/*----------------------------------------------------------*/
/*--- Neutron output tree (Eugenio data format) function ---*/
/*----------------------------------------------------------*/
void LHCfAn::CreateOutputTree(TString dataFormat, TString treeName, TString treeOption) {
  /* Initialization */
  SetInputTree();
  OpenOutputFile();

  /* Build histograms */
  BookTree(dataFormat, treeName, treeOption);

  /* Event loop */
  FillTree(dataFormat, treeName, treeOption);

  /* Write to file */
  WriteToOutput();
  CloseFiles();
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
void LHCfAn::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/rec_run%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void LHCfAn::OpenOutputFile() {
  UT::Printf(UT::kPrintInfo, "Opening output file...");
  fflush(stdout);

  fOutputFile = new TFile(fOutputName.Data(), "RECREATE");
  if (!fOutputFile->IsOpen()) {
    UT::Printf(UT::kPrintError, "Error: output file \"%s\" not opened\n", fOutputName.Data());
    exit(EXIT_FAILURE);
  }
  gROOT->cd();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void LHCfAn::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write("", TObject::kOverwrite);

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

void LHCfAn::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing files...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*------------------*/
/*--- Event loop ---*/
/*------------------*/
void LHCfAn::EventLoop() {
  /* Event selection classes */
  EventCut<Arm1AnPars, Arm1RecPars> a1_cut;
  EventCut<Arm2AnPars, Arm2RecPars> a2_cut;
  if (fPhotonEnable) {
    if (fArmEnable[Arm1Params::kArmIndex]) a1_cut.SetPhotonCuts(fPhotonCuts);
    if (fArmEnable[Arm2Params::kArmIndex]) a2_cut.SetPhotonCuts(fPhotonCuts);
  }
  if (fNeutronEnable) {
    if (fArmEnable[Arm1Params::kArmIndex]) a1_cut.SetNeutronCuts(fNeutronCuts);
    if (fArmEnable[Arm2Params::kArmIndex]) a2_cut.SetNeutronCuts(fNeutronCuts);
  }

  /* Level3 Modification */
  DataModification<Arm1AnPars, Arm1RecPars, Arm1Params> a1_modify;
  DataModification<Arm2AnPars, Arm2RecPars, Arm2Params> a2_modify;

  /* Debug (interface to old software) */
  OldInterface<Arm1Params, Arm1RecPars> a1_old(fOldInputDir.Data(), fFirstRun, fLastRun);
  OldInterface<Arm2Params, Arm2RecPars> a2_old(fOldInputDir.Data(), fFirstRun, fLastRun);
  if (fArmEnable[Arm1Params::kArmIndex] && fOldFlag) a1_old.Open();
  if (fArmEnable[Arm2Params::kArmIndex] && fOldFlag) a2_old.Open();

  // reset arm1 trigger counters
  fTotalTrgArm1 = 0;
  fShowerTrgArm1 = 0;
  fPi0TrgArm1 = 0;
  fHighEMTrgArm1 = 0;
  fHadronTrgArm1 = 0;
  fZdcTrgArm1 = 0;
  fFcTrgArm1 = 0;
  fL1tTrgArm1 = 0;
  fLaserTrgArm1 = 0;
  fPedestalTrgArm1 = 0;
  // reset arm2 trigger counters
  fTotalTrgArm2 = 0;
  fShowerTrgArm2 = 0;
  fPi0TrgArm2 = 0;
  fHighEMTrgArm2 = 0;
  fHadronTrgArm2 = 0;
  fZdcTrgArm2 = 0;
  fFcTrgArm2 = 0;
  fL1tTrgArm2 = 0;
  fLaserTrgArm2 = 0;
  fPedestalTrgArm2 = 0;

  /* Event Loop */
  const Int_t nentries = fInputTree->GetEntries();
  UT::Printf(UT::kPrintInfo, "Total events: %d\n", nentries);
  for (Int_t ie = 0; ie < nentries; ++ie) {
    if (ie % 10000 == 0 || ie == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    fInputTree->GetEntry(ie);

    /* Arm1 event */
    if (fArmEnable[Arm1Params::kArmIndex]) {
      // UT::Printf(UT::kPrintInfo, "Arm1: event %d\n", ie);
      UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);
      // get Level3
      fLvl3_Arm1 = (Level3<Arm1RecPars> *)fInputEv->Get("lvl3_a1");
      fTrue_Arm1 = (McEvent *)fInputEv->Get("true_a1");
      UT::Printf(UT::kPrintDebugFull, "fLvl3_Arm1 = %p\n", fLvl3_Arm1);

      // modification of the lvl3 values
      if (fModificationEnable && !fTrue_Arm1) a1_modify.Modify(fLvl3_Arm1);

      if (fLvl3_Arm1) {
        // fill trigger counters
        if (fLvl3_Arm1->IsShowerTrg()) ++fShowerTrgArm1;
        if (fLvl3_Arm1->IsPi0Trg()) ++fPi0TrgArm1;
        if (fLvl3_Arm1->IsHighEMTrg()) ++fHighEMTrgArm1;
        if (fLvl3_Arm1->IsHadronTrg()) ++fHadronTrgArm1;
        if (fLvl3_Arm1->IsZdcTrg()) ++fZdcTrgArm1;
        if (fLvl3_Arm1->IsFcTrg()) ++fFcTrgArm1;
        if (fLvl3_Arm1->IsL1tTrg()) ++fL1tTrgArm1;
        if (fLvl3_Arm1->IsLaserTrg()) ++fLaserTrgArm1;
        if (fLvl3_Arm1->IsPedestalTrg()) ++fPedestalTrgArm1;
        ++fTotalTrgArm1;

        // fill histograms
        if (fPhotonEnable) UT::Printf(UT::kPrintInfo, "Fill Photon Hist,%d\n", ie);
        fHistArm1->FillPhotonHistograms<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars>>(fLvl3_Arm1, &a1_cut,
                                                                                                fTrue_Arm1);
        if (fNeutronEnable)
          fHistArm1->FillNeutronHistograms<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars>>(fLvl3_Arm1, &a1_cut,
                                                                                                   fTrue_Arm1);
        if (fPionEnable) {
          fPion_Arm1 = GetPionData<Arm1RecPars, Arm1AnPars>(fLvl3_Arm1, &a1_cut);
          fTruePion_Arm1 = GetPionData<Arm1RecPars, Arm1AnPars>(fTrue_Arm1, &a1_cut);
          fHistArm1->FillPionHistograms(&fPion_Arm1, &fTruePion_Arm1);
        }
      }
      // debug
      if (fOldFlag) {
        if (a1_old.GetEvent(ie) == 0) {
          if (fPhotonEnable)
            fOldHistArm1->FillPhotonHistograms<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars>>(
                a1_old.GetLevel3(), &a1_cut);
          if (fNeutronEnable)
            fOldHistArm1->FillNeutronHistograms<Level3<Arm1RecPars>, EventCut<Arm1AnPars, Arm1RecPars>>(
                a1_old.GetLevel3(), &a1_cut);
        }
      }
    }
    /* Arm2 event */
    if (fArmEnable[Arm2Params::kArmIndex]) {
      UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
      // get Level3
      fLvl3_Arm2 = (Level3<Arm2RecPars> *)fInputEv->Get("lvl3_a2");
      fTrue_Arm2 = (McEvent *)fInputEv->Get("true_a2");
      UT::Printf(UT::kPrintDebugFull, "fLvl3_Arm2 = %p\n", fLvl3_Arm2);
      // modification of the lvl3 values
      if (fModificationEnable && !fTrue_Arm2) a2_modify.Modify(fLvl3_Arm2);

      if (fLvl3_Arm2) {
        // fill trigger counters
        if (fLvl3_Arm2->IsShowerTrg()) ++fShowerTrgArm2;
        if (fLvl3_Arm2->IsPi0Trg()) ++fPi0TrgArm2;
        if (fLvl3_Arm2->IsHighEMTrg()) ++fHighEMTrgArm2;
        if (fLvl3_Arm2->IsHadronTrg()) ++fHadronTrgArm2;
        if (fLvl3_Arm2->IsZdcTrg()) ++fZdcTrgArm2;
        if (fLvl3_Arm2->IsFcTrg()) ++fFcTrgArm2;
        if (fLvl3_Arm2->IsL1tTrg()) ++fL1tTrgArm2;
        if (fLvl3_Arm2->IsLaserTrg()) ++fLaserTrgArm2;
        if (fLvl3_Arm2->IsPedestalTrg()) ++fPedestalTrgArm2;
        ++fTotalTrgArm2;

        // fill histograms
        if (fPhotonEnable)
          fHistArm2->FillPhotonHistograms<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars>>(fLvl3_Arm2, &a2_cut,
                                                                                                  fTrue_Arm2);
        if (fNeutronEnable)
          fHistArm2->FillNeutronHistograms<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars>>(fLvl3_Arm2, &a2_cut,
                                                                                                   fTrue_Arm2);
        if (fPionEnable) {
          fPion_Arm2 = GetPionData<Arm2RecPars, Arm2AnPars>(fLvl3_Arm2, &a2_cut);
          fTruePion_Arm2 = GetPionData<Arm2RecPars, Arm2AnPars>(fTrue_Arm2, &a2_cut);
          fHistArm2->FillPionHistograms(&fPion_Arm2, &fTruePion_Arm2);
        }
      }

      // debug
      if (fOldFlag) {
        if (a2_old.GetEvent(ie) == 0) {
          if (fPhotonEnable)
            fOldHistArm2->FillPhotonHistograms<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars>>(
                a2_old.GetLevel3(), &a2_cut);
          if (fNeutronEnable)
            fOldHistArm2->FillNeutronHistograms<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars>>(
                a2_old.GetLevel3(), &a2_cut);
        }
      }

      if (UT::fVerbose >= UT::kPrintDebugFull && fLvl3_Arm2) {
        Print<Level3<Arm2RecPars>, EventCut<Arm2AnPars, Arm2RecPars>>(fLvl3_Arm2, &a2_cut, ie, fTrue_Arm2);
        if (fOldFlag) a2_old.Print(ie);
      }
    }

    /* Clear event */
    fInputEv->HeaderClear();
    fInputEv->ObjDelete();
  }
  UT::Printf(UT::kPrintInfo, "\n");

  // print arm1 trigger counters
  if (fArmEnable[Arm1Params::kArmIndex]) {
    UT::Printf(UT::kPrintInfo, "Arm1 Trigger Counters:\n");
    UT::Printf(UT::kPrintInfo, "\tTotal : %d\n", fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tShower : %d [%.2f\%]\n", fShowerTrgArm1, 100. * fShowerTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tPi0 : %d [%.2f\%]\n", fPi0TrgArm1, 100. * fPi0TrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tHighEM : %d [%.2f\%]\n", fHighEMTrgArm1, 100. * fHighEMTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tHadron : %d [%.2f\%]\n", fHadronTrgArm1, 100. * fHadronTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tZdc : %d [%.2f\%]\n", fZdcTrgArm1, 100. * fZdcTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tFc : %d [%.2f\%]\n", fFcTrgArm1, 100. * fFcTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tL1t : %d [%.2f\%]\n", fL1tTrgArm1, 100. * fL1tTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tLaser : %d [%.2f\%]\n", fLaserTrgArm1, 100. * fLaserTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\tPedestal : %d [%.2f\%]\n", fPedestalTrgArm1, 100. * fPedestalTrgArm1 / fTotalTrgArm1);
    UT::Printf(UT::kPrintInfo, "\n");
  }
  // print arm2 trigger counters
  if (fArmEnable[Arm2Params::kArmIndex]) {
    UT::Printf(UT::kPrintInfo, "Arm2 Trigger Counters:\n");
    UT::Printf(UT::kPrintInfo, "\tTotal : %d\n", fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tShower : %d [%.2f\%]\n", fShowerTrgArm2, 100. * fShowerTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tPi0 : %d [%.2f\%]\n", fPi0TrgArm2, 100. * fPi0TrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tHighEM : %d [%.2f\%]\n", fHighEMTrgArm2, 100. * fHighEMTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tHadron : %d [%.2f\%]\n", fHadronTrgArm2, 100. * fHadronTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tZdc : %d [%.2f\%]\n", fZdcTrgArm2, 100. * fZdcTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tFc : %d [%.2f\%]\n", fFcTrgArm2, 100. * fFcTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tL1t : %d [%.2f\%]\n", fL1tTrgArm2, 100. * fL1tTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tLaser : %d [%.2f\%]\n", fLaserTrgArm2, 100. * fLaserTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\tPedestal : %d [%.2f\%]\n", fPedestalTrgArm2, 100. * fPedestalTrgArm2 / fTotalTrgArm2);
    UT::Printf(UT::kPrintInfo, "\n");
  }

  if (fArmEnable[Arm1Params::kArmIndex]) fHistArm1->FillL90Vector();
  if (fArmEnable[Arm2Params::kArmIndex]) fHistArm2->FillL90Vector();

  // debug
  if (fArmEnable[Arm1Params::kArmIndex] && fOldFlag) a1_old.Close();
  if (fArmEnable[Arm2Params::kArmIndex] && fOldFlag) a2_old.Close();
}

/*----------------------*/
/*--- Fill pion data ---*/
/*----------------------*/
template <typename armrec, typename arman>
vector<PionData> LHCfAn::GetPionData(Level3<armrec> *lvl3, EventCut<arman, armrec> *evcut, McEvent *mcev) {
  vector<PionData> pdata;

  // The Pion Reconstruction code were moved to PionRec class
  PionRec rec;
  rec.Reconstruct<Level3<armrec>, EventCut<arman, armrec>>(lvl3, evcut);
  for (int i = 0; i < rec.kNtypes; ++i) {
    if (rec.fResult[i].fFlag & 0x1) pdata.push_back(rec.fResult[i]);
  }

  if (mcev) {
    vector<PionData> tmp = GetPionData<armrec, arman>(mcev, evcut);
    for (unsigned int ip = 0; ip < tmp.size(); ++ip) pdata.push_back(tmp.at(ip));
  }

  return pdata;
}

template <typename armrec, typename arman>
vector<PionData> LHCfAn::GetPionData(McEvent *mcev, EventCut<arman, armrec> *evcut) {
  vector<PionData> pdata;

  PionRec rec;
  rec.Reconstruct<EventCut<arman, armrec>>(mcev, evcut);
  for (int i = 0; i < rec.kNtypes; ++i) {
    if (rec.fResult[i].fFlag & 0x1) pdata.push_back(rec.fResult[i]);
  }

  return pdata;
}

/*-----------------*/
/*--- Fill Tree ---*/
/*-----------------*/
void LHCfAn::FillTree(TString dataFormat, TString treeName, TString treeOption) {
  if (dataFormat == "eugenio") {
    /* Event selection classes */
    EventCut<Arm1AnPars, Arm1RecPars> a1_cut;
    EventCut<Arm2AnPars, Arm2RecPars> a2_cut;

    /* Event Loop */
    const Int_t nentries = fInputTree->GetEntries();
    UT::Printf(UT::kPrintInfo, "Total events: %d\n", nentries);
    for (Int_t ie = 0; ie < nentries; ++ie) {
      if (ie % 1000 == 0 || ie == nentries - 1) {
        UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
        fflush(stdout);
      }

      fInputTree->GetEntry(ie);

      /* Arm1 event */
      if (fArmEnable[Arm1Params::kArmIndex]) {
        UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);
        // get Level1,2,3
        fLvl1_Arm1 = (Level1<Arm1Params> *)fInputEv->Get("lvl1_a1");
        fLvl2_Arm1 = (Level2<Arm1Params> *)fInputEv->Get("lvl2_a1");
        fLvl3_Arm1 = (Level3<Arm1RecPars> *)fInputEv->Get("lvl3_a1");
        fTrue_Arm1 = (McEvent *)fInputEv->Get("true_a1");
        // fill trees
        fTreeArm1->SetCurrentEntry(ie);
        fTreeArm1->FillTree<Level1<Arm1Params>, Level2<Arm1Params>, Level3<Arm1RecPars>, McEvent,
                            EventCut<Arm1AnPars, Arm1RecPars>>(fLvl1_Arm1, fLvl2_Arm1, fLvl3_Arm1, fTrue_Arm1, &a1_cut);
      }
      /* Arm2 event */
      if (fArmEnable[Arm2Params::kArmIndex]) {
        UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
        // get Level1,2,3
        fLvl1_Arm2 = (Level1<Arm2Params> *)fInputEv->Get("lvl1_a2");
        fLvl2_Arm2 = (Level2<Arm2Params> *)fInputEv->Get("lvl2_a2");
        fLvl3_Arm2 = (Level3<Arm2RecPars> *)fInputEv->Get("lvl3_a2");
        fTrue_Arm2 = (McEvent *)fInputEv->Get("true_a2");
        // fill trees
        fTreeArm2->SetCurrentEntry(ie);
        fTreeArm2->FillTree<Level1<Arm2Params>, Level2<Arm2Params>, Level3<Arm2RecPars>, McEvent,
                            EventCut<Arm2AnPars, Arm2RecPars>>(fLvl1_Arm2, fLvl2_Arm2, fLvl3_Arm2, fTrue_Arm2, &a2_cut);
      }

      /* Clear event */
      fInputEv->HeaderClear();
      fInputEv->ObjDelete();
    }
    UT::Printf(UT::kPrintInfo, "\n");
  } else {
    UT::Printf(UT::kPrintError, "Unknown tree output format corresponding to \"%s\": Exit...", dataFormat.Data());
    exit(EXIT_FAILURE);
  }
}

/*-------------------*/
/*--- Print Event ---*/
/*-------------------*/
template <typename level3, typename eventcut>
void LHCfAn::Print(level3 *lvl3, eventcut *evcut, Int_t ev, McEvent *mcev) {
  lvl3->Print();
  if (mcev != NULL) mcev->Print();

  // !!! DEBUG !!! //
  if (fPion_Arm2.size() > 0) {
    for (Int_t ip = 0; ip < fPion_Arm2.size(); ++ip) {
      if (fPion_Arm2.at(ip).IsTypeI()) UT::Printf(UT::kPrintInfo, "Type I\n");
      if (fPion_Arm2.at(ip).IsTypeII_ST()) UT::Printf(UT::kPrintInfo, "Type II in small tower\n");
      if (fPion_Arm2.at(ip).IsTypeII_LT()) UT::Printf(UT::kPrintInfo, "Type II in large tower\n");
    }
  }
  getchar();
}
