#include "DataModification.hh"

#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(DataModification);
#endif

typedef Utils UT;

template <typename arman, typename armrec, typename arm>
DataModification<arman, armrec, arm>::DataModification() {
  ;
}

template <typename arman, typename armrec, typename arm>
DataModification<arman, armrec, arm>::~DataModification() {
  ;
}

template <typename arman, typename armrec, typename arm>
void DataModification<arman, armrec, arm>::Modify(Level3<armrec> *lvl3, Level2<arm> *lvl2) {
  RescaleEnergy(lvl3);
  return;
}

template <typename arman, typename armrec, typename arm>
void DataModification<arman, armrec, arm>::RescaleEnergy(Level3<armrec> *lvl3) {
  // For Photon (+Pion) results
  for (int i = 0; i < this->kCalNtower; ++i) {
    lvl3->fPhotonEnergy[i] *= this->kEnergyRescaleFactor[i];
    lvl3->fPhotonEnergyLin[i] *= this->kEnergyRescaleFactor[i];
    UT::MultiplyVector1D(lvl3->fPhotonEnergyMH[i], this->kEnergyRescaleFactor[i]);
  }

  // For Neutron results
  for (int i = 0; i < this->kCalNtower; ++i) {
    lvl3->fNeutronEnergy[i] *= this->kEnergyRescaleFactor[i];
  }
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class DataModification<Arm1AnPars, Arm1RecPars, Arm1Params>;
template class DataModification<Arm2AnPars, Arm2RecPars, Arm2Params>;
}  // namespace nLHCf
