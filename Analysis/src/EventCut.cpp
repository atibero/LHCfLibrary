#include "EventCut.hh"

#include <TMath.h>
#include <TVector3.h>

#include "CoordinateTransformation.hh"
#include "CutFunctions.hh"
#include "PionData.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(EventCut);
#endif

typedef Utils UT;
typedef CoordinateTransformation CT;
typedef CutFunctions CF;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename arman, typename armrec>
EventCut<arman, armrec>::EventCut() {
  // TODO [TrackProjection]: Is this call necessary any more? This was moved to ArmXAnPars.cpp
  /*
  CT::SetBeamCrossingAngle(this->kArmIndex, this->kCrossingAngle);
  CT::SetDetectorPosition(this->kArmIndex, this->kDetectorOffset[1]);
  CT::SetMeasuredBeamCentre(this->kArmIndex,                             //
                            this->kBeamCentre[0], this->kBeamCentre[1],  //
                            this->kBeamCentreAverageLayer[0], this->kBeamCentreAverageLayer[1]);
  CT::SetDetectorPositionVector(this->kArmIndex);
  CT::SetBeamCentreVector(this->kArmIndex);
  CT::PrintParameters(this->kArmIndex, "EventCut");
  */
  //

  fPhotonCutPID = true;
  fPhotonCutEnergy = true;
  fPhotonCutPosition = true;
  fPhotonCutRapidity = true;
  fPhotonCutMultiHit = true;
  fPhotonCutBPTX = true;
  fPhotonCutL2T = true;
  fPhotonCutDsc = true;

  fNeutronCutPID = true;
  fNeutronCutEnergy = true;
  fNeutronCutPosition = true;
  fNeutronCutRapidity = true;
  fNeutronCutMultiHit = false;
  fNeutronCutBPTX = true;
  fNeutronCutL2T = true;
  fNeutronCutDsc = true;

  if (this->fOperation == kSPS2015 || this->fOperation == kSPS2021 || this->fOperation == kSPS2022) {
    this->kEnergyThr = 50.;
  }
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename arman, typename armrec>
EventCut<arman, armrec>::~EventCut() {}

/*-----------------------*/
/*--- Event Selection ---*/
/*-----------------------*/
template <typename arman, typename armrec>
void EventCut<arman, armrec>::SetPhotonCuts(TString cuts) {
  Utils::Printf(Utils::kPrintInfo, "Arm%d: setting the following cuts for photons:\n", this->kArmIndex + 1);

  if (cuts.Contains("discriminator ", TString::kIgnoreCase) || cuts.Contains(" discriminator", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutDsc = true;
    Utils::Printf(Utils::kPrintInfo, "\tDiscriminator\n");
  } else {
    fPhotonCutDsc = false;
  }
  if (cuts.Contains("pid ", TString::kIgnoreCase) || cuts.Contains(" pid", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutPID = true;
    Utils::Printf(Utils::kPrintInfo, "\tPID\n");
  } else {
    fPhotonCutPID = false;
  }
  if (cuts.Contains("energy ", TString::kIgnoreCase) || cuts.Contains(" energy", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutEnergy = true;
    Utils::Printf(Utils::kPrintInfo, "\tenergy\n");
  } else {
    fPhotonCutEnergy = false;
  }
  if (cuts.Contains("position ", TString::kIgnoreCase) || cuts.Contains(" position", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutPosition = true;
    Utils::Printf(Utils::kPrintInfo, "\tposition\n");
  } else {
    fPhotonCutPosition = false;
  }
  if (cuts.Contains("rapidity ", TString::kIgnoreCase) || cuts.Contains(" rapidity", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutRapidity = true;
    Utils::Printf(Utils::kPrintInfo, "\trapidity\n");
  } else {
    fPhotonCutRapidity = false;
  }
  if (cuts.Contains("multi-hit ", TString::kIgnoreCase) || cuts.Contains(" multi-hit", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutMultiHit = true;
    Utils::Printf(Utils::kPrintInfo, "\tmulti-hit\n");
  } else {
    fPhotonCutMultiHit = false;
  }
  if (cuts.Contains("bptx ", TString::kIgnoreCase) || cuts.Contains(" bptx", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutBPTX = true;
    Utils::Printf(Utils::kPrintInfo, "\tBPTX\n");
  } else {
    fPhotonCutBPTX = false;
  }
  if (cuts.Contains("l2t ", TString::kIgnoreCase) || cuts.Contains(" l2t", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fPhotonCutL2T = true;
    Utils::Printf(Utils::kPrintInfo, "\tL2T\n");
  } else {
    fPhotonCutL2T = false;
  }
}

template <typename arman, typename armrec>
void EventCut<arman, armrec>::SetNeutronCuts(TString cuts) {
  Utils::Printf(Utils::kPrintInfo, "Arm%d: setting the following cuts for neutrons:\n", this->kArmIndex + 1);

  if (cuts.Contains("discriminator ", TString::kIgnoreCase) || cuts.Contains(" discriminator", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutDsc = true;
    Utils::Printf(Utils::kPrintInfo, "\tDiscriminator\n");
  } else {
    fNeutronCutDsc = false;
  }
  if (cuts.Contains("pid ", TString::kIgnoreCase) || cuts.Contains(" pid", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutPID = true;
    Utils::Printf(Utils::kPrintInfo, "\tPID\n");
  } else {
    fNeutronCutPID = false;
  }
  if (cuts.Contains("energy ", TString::kIgnoreCase) || cuts.Contains(" energy", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutEnergy = true;
    Utils::Printf(Utils::kPrintInfo, "\tenergy\n");
  } else {
    fNeutronCutEnergy = false;
  }
  if (cuts.Contains("position ", TString::kIgnoreCase) || cuts.Contains(" position", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutPosition = true;
    Utils::Printf(Utils::kPrintInfo, "\tposition\n");
  } else {
    fNeutronCutPosition = false;
  }
  if (cuts.Contains("rapidity ", TString::kIgnoreCase) || cuts.Contains(" rapidity", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutRapidity = true;
    Utils::Printf(Utils::kPrintInfo, "\trapidity\n");
  } else {
    fNeutronCutRapidity = false;
  }
  if (cuts.Contains("multi-hit ", TString::kIgnoreCase) || cuts.Contains(" multi-hit", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutMultiHit = true;
    Utils::Printf(Utils::kPrintInfo, "\tmulti-hit\n");
  } else {
    fNeutronCutMultiHit = false;
  }
  if (cuts.Contains("bptx ", TString::kIgnoreCase) || cuts.Contains(" bptx", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutBPTX = true;
    Utils::Printf(Utils::kPrintInfo, "\tBPTX\n");
  } else {
    fNeutronCutBPTX = false;
  }
  if (cuts.Contains("l2t ", TString::kIgnoreCase) || cuts.Contains(" l2t", TString::kIgnoreCase) ||
      cuts.Contains("all", TString::kIgnoreCase)) {
    fNeutronCutL2T = true;
    Utils::Printf(Utils::kPrintInfo, "\tL2T\n");
  } else {
    fNeutronCutL2T = false;
  }
}

/*
 * Composite cuts on RECO Photon
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutPhotonEvent(Level3<armrec> *lvl3, Int_t rap) {
  const Int_t it = this->RapToTower(rap);

  Bool_t bptx_cut = fPhotonCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fPhotonCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fPhotonCutDsc ? PhotonDiscriminatorCut(lvl3, it) : true;
  Bool_t pid_cut = fPhotonCutPID ? PhotonPIDCut(lvl3, it) : true;
  Bool_t energy_cut = fPhotonCutEnergy ? PhotonEnergyCut(lvl3, it) : true;
  Bool_t position_cut = fPhotonCutPosition ? PhotonPositionCut(lvl3, it) : true;
  Bool_t rapidity_cut = fPhotonCutRapidity ? PhotonRapidityCut(lvl3, rap) : true;
  Bool_t multihit_cut = fPhotonCutMultiHit ? PhotonMultiHitCut(lvl3, it) : true;

  return bptx_cut && l2t_cut && dsc_cut && pid_cut && energy_cut && position_cut && rapidity_cut && multihit_cut;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutPhotonEventL90(Level3<armrec> *lvl3, Int_t rap) {
  const Int_t it = this->RapToTower(rap);

  Bool_t bptx_cut = fPhotonCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fPhotonCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fPhotonCutDsc ? PhotonDiscriminatorCut(lvl3, it) : true;
  Bool_t energy_cut = fPhotonCutEnergy ? PhotonEnergyCut(lvl3, it) : true;
  Bool_t position_cut = fPhotonCutPosition ? PhotonPositionCut(lvl3, it) : true;
  Bool_t rapidity_cut = fPhotonCutRapidity ? PhotonRapidityCut(lvl3, rap) : true;
  Bool_t multihit_cut = fPhotonCutMultiHit ? PhotonMultiHitCut(lvl3, it) : true;

  return bptx_cut && l2t_cut && dsc_cut && energy_cut && position_cut && rapidity_cut && multihit_cut;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutPhotonEventTower(Level3<armrec> *lvl3, Int_t it) {
  Bool_t bptx_cut = fPhotonCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fPhotonCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fPhotonCutDsc ? PhotonDiscriminatorCut(lvl3, it) : true;
  Bool_t pid_cut = fPhotonCutPID ? PhotonPIDCut(lvl3, it) : true;
  Bool_t energy_cut = fPhotonCutEnergy ? PhotonEnergyCut(lvl3, it) : true;
  Bool_t position_cut = fPhotonCutPosition ? PhotonPositionCut(lvl3, it) : true;
  Bool_t multihit_cut = fPhotonCutMultiHit ? PhotonMultiHitCut(lvl3, it) : true;

  return bptx_cut && l2t_cut && dsc_cut && pid_cut && energy_cut && position_cut && multihit_cut;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutPhotonEventTowerMultiHit(Level3<armrec> *lvl3, Int_t rap) {
  const Int_t it = this->RapToTower(rap);

  Bool_t bptx_cut = fPhotonCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fPhotonCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fPhotonCutDsc ? PhotonDiscriminatorCut(lvl3, it) : true;
  Bool_t pid_cut = fPhotonCutPID ? PhotonPIDCut(lvl3, it) : true;
  Bool_t energy_cut = fPhotonCutEnergy ? PhotonEnergyCut(lvl3, it) : true;
  Bool_t position_cut = fPhotonCutPosition ? PhotonPositionCut(lvl3, it) : true;

  return bptx_cut && l2t_cut && dsc_cut && pid_cut && energy_cut && position_cut;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutPhotonEventBeamCentre(Level3<armrec> *lvl3, Int_t it) {
  Bool_t bptx_cut = fPhotonCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fPhotonCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fPhotonCutDsc ? PhotonDiscriminatorCut(lvl3, it) : true;
  Bool_t pid_cut = lvl3->fPhotonL90[it] > 20.;
  Bool_t energy_cut = lvl3->fPhotonEnergy[it] > 500.;
  Bool_t position_cut = PhotonPositionCut(lvl3, it);
  Bool_t multihit_cut = PhotonMultiHitCut(lvl3, it);

  return bptx_cut && l2t_cut && dsc_cut && pid_cut && energy_cut && position_cut && multihit_cut;
}

/*
 * Composite cuts on RECO Neutron
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutNeutronEventTower(Level3<armrec> *lvl3, Int_t it) {
  Bool_t bptx_cut = fNeutronCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fNeutronCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fNeutronCutDsc ? NeutronDiscriminatorCut(lvl3, it) : true;
  Bool_t pid_cut = fNeutronCutPID ? NeutronPIDCut(lvl3, it) : true;
  Bool_t energy_cut = fNeutronCutEnergy ? NeutronEnergyCut(lvl3, it) : true;
  Bool_t position_cut = fNeutronCutPosition ? NeutronPositionCut(lvl3, it) : true;
  Bool_t multihit_cut = fNeutronCutMultiHit ? NeutronMultiHitCut(lvl3, it) : true;

  return bptx_cut && l2t_cut && dsc_cut && pid_cut && energy_cut && position_cut && multihit_cut;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutNeutronEvent(Level3<armrec> *lvl3, Int_t rap) {
  const Int_t it = this->RapToTower(rap);

  Bool_t bptx_cut = fNeutronCutBPTX ? BPTXCut(lvl3) : true;
  Bool_t l2t_cut = fNeutronCutL2T ? L2TShowerCut(lvl3) : true;
  Bool_t dsc_cut = fNeutronCutDsc ? NeutronDiscriminatorCut(lvl3, it) : true;
  Bool_t pid_cut = fNeutronCutPID ? NeutronPIDCut(lvl3, it) : true;
  Bool_t energy_cut = fNeutronCutEnergy ? NeutronEnergyCut(lvl3, it) : true;
  Bool_t position_cut = fNeutronCutPosition ? NeutronPositionCut(lvl3, it) : true;
  Bool_t rapidity_cut = fNeutronCutRapidity ? NeutronRapidityCut(lvl3, rap) : true;
  Bool_t multihit_cut = fNeutronCutMultiHit ? NeutronMultiHitCut(lvl3, it) : true;

  return bptx_cut && l2t_cut && dsc_cut && pid_cut && energy_cut && position_cut && rapidity_cut && multihit_cut;
}

/*
 * Composite cuts on TRUE Photon
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutTruePhotonEvent(McEvent *mcev, Int_t rap) {
  const Int_t it = this->RapToTower(rap);

  Bool_t pid_cut = fPhotonCutPID ? PhotonTruePIDCut(mcev, it) : true;
  Bool_t energy_cut = fPhotonCutEnergy ? PhotonTrueEnergyCut(mcev, it) : true;
  Bool_t position_cut = fPhotonCutPosition ? PhotonTruePositionCut(mcev, it) : true;
  Bool_t rapidity_cut = fPhotonCutRapidity ? PhotonTrueRapidityCut(mcev, rap) : true;
  Bool_t multihit_cut = fPhotonCutMultiHit ? PhotonTrueMultiHitCut(mcev, it) : true;

  return pid_cut && energy_cut && position_cut && rapidity_cut && multihit_cut;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutTruePhotonEventInclusive(McEvent *mcev, Int_t rap, Int_t par) {
  const Int_t it = this->RapToTower(rap);

  Bool_t pid_cut = fPhotonCutPID ? PhotonTruePIDCut(mcev, it, par) : true;
  Bool_t energy_cut = fPhotonCutEnergy ? PhotonTrueEnergyCut(mcev, it, par) : true;
  Bool_t position_cut = fPhotonCutPosition ? PhotonTruePositionCut(mcev, it, par) : true;
  Bool_t rapidity_cut = fPhotonCutRapidity ? PhotonTrueRapidityCut(mcev, rap, par) : true;

  return pid_cut && energy_cut && position_cut && rapidity_cut;
}

/*
 * Composite cuts on TRUE Neutron
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::CutTrueNeutronEvent(McEvent *mcev, Int_t rap) {
  const Int_t it = this->RapToTower(rap);

  Bool_t pid_cut = fNeutronCutPID ? NeutronTruePIDCut(mcev, it) : true;
  Bool_t energy_cut = fNeutronCutEnergy ? NeutronTrueEnergyCut(mcev, it) : true;
  Bool_t position_cut = fNeutronCutPosition ? NeutronTruePositionCut(mcev, it) : true;
  Bool_t rapidity_cut = fNeutronCutRapidity ? NeutronTrueRapidityCut(mcev, rap) : true;
  Bool_t multihit_cut = fNeutronCutMultiHit ? NeutronTrueMultiHitCut(mcev, it) : true;

  return pid_cut && energy_cut && position_cut && rapidity_cut && multihit_cut;
}

/*
 * Single cuts on common variables
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::BPTXCut(Level3<armrec> *lvl3) {
  return CF::BPTXCut<armrec, arman>(lvl3);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::L2TShowerCut(Level3<armrec> *lvl3) {
  return CF::L2TShowerCut<armrec, arman>(lvl3);
}

/*
 * Single cuts on Photon RECO variables
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonDiscriminatorCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonPIDCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonPIDCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonEnergyCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonEnergyLinCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonEnergyLinCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonEnergyCut(Level3<armrec> *lvl3, Int_t tower, Int_t part) {
  return CF::PhotonEnergyCut<armrec, arman>(lvl3, tower, part);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonPositionCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonPositionCut(Level3<armrec> *lvl3, Int_t tower, Int_t part) {
  return CF::PhotonPositionCut<armrec, arman>(lvl3, tower, part);
}

template <typename arman, typename armrec>
TVector3 EventCut<arman, armrec>::PhotonGlobalPosition(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonGlobalPosition<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonRapidityCut(Level3<armrec> *lvl3, Int_t rap) {
  return CF::PhotonRapidityCut<armrec, arman>(lvl3, rap);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonMultiHitCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::PhotonMultiHitCut<armrec, arman>(lvl3, tower);
}

/*
 * Single cuts on Neutron RECO variables
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronDiscriminatorCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::NeutronDiscriminatorCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronPIDCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::NeutronPIDCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronEnergyCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::NeutronEnergyCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronPositionCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::NeutronPositionCut<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
TVector3 EventCut<arman, armrec>::NeutronGlobalPosition(Level3<armrec> *lvl3, Int_t tower) {
  return CF::NeutronGlobalPosition<armrec, arman>(lvl3, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronRapidityCut(Level3<armrec> *lvl3, Int_t rap) {
  return CF::NeutronRapidityCut<armrec, arman>(lvl3, rap);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronMultiHitCut(Level3<armrec> *lvl3, Int_t tower) {
  return CF::NeutronMultiHitCut<armrec, arman>(lvl3, tower);
}

/*
 * Single cuts on Photon TRUE variables
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonTruePIDCut(McEvent *mcev, Int_t tower, Int_t part) {
  return CF::PhotonTruePIDCut<armrec, arman>(mcev, tower, part);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonTrueEnergyCut(McEvent *mcev, Int_t tower, Int_t part) {
  return CF::PhotonTrueEnergyCut<armrec, arman>(mcev, tower, part);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonTruePositionCut(McEvent *mcev, Int_t tower, Int_t part) {
  return CF::PhotonTruePositionCut<armrec, arman>(mcev, tower, part);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonTrueRapidityCut(McEvent *mcev, Int_t rap, Int_t part) {
  return CF::PhotonTrueRapidityCut<armrec, arman>(mcev, rap, part);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PhotonTrueMultiHitCut(McEvent *mcev, Int_t tower) {
  return CF::PhotonTrueMultiHitCut<armrec, arman>(mcev, tower);
}

/*
 * Single cuts on Neutron TRUE variables
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronTruePIDCut(McEvent *mcev, Int_t tower) {
  return CF::NeutronTruePIDCut<armrec, arman>(mcev, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronTrueEnergyCut(McEvent *mcev, Int_t tower) {
  return CF::NeutronTrueEnergyCut<armrec, arman>(mcev, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronTruePositionCut(McEvent *mcev, Int_t tower) {
  return CF::NeutronTruePositionCut<armrec, arman>(mcev, tower);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronTrueRapidityCut(McEvent *mcev, Int_t rap) {
  return CF::NeutronTrueRapidityCut<armrec, arman>(mcev, rap);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::NeutronTrueMultiHitCut(McEvent *mcev, Int_t tower) {
  return CF::NeutronTrueMultiHitCut<armrec, arman>(mcev, tower);
}

/*
 * Neutral Pion
 */

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PionPIDCut(Level3<armrec> *lvl3, Int_t tower, Int_t type, Int_t eff) {
  return CF::PionPIDCut<armrec, arman>(lvl3, tower, type, eff);
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PionTypeICut(Level3<armrec> *lvl3, Int_t eff, UInt_t *result_cutflag) {
  // return (PionPIDCut(lvl3, 0, 1, eff) && PhotonEnergyLinCut(lvl3, 0) && PhotonPositionCut(lvl3, 0) &&
  //         PhotonMultiHitCut(lvl3, 0) && PionPIDCut(lvl3, 1, 1, eff) && PhotonEnergyLinCut(lvl3, 1) &&
  //         PhotonPositionCut(lvl3, 1) && PhotonMultiHitCut(lvl3, 1) && (fPhotonCutBPTX ? BPTXCut(lvl3) : true));

  UInt_t flag = 0;

  if (PionPIDCut(lvl3, 0, 1, eff)) flag += (1 << 4);
  if (PhotonEnergyLinCut(lvl3, 0)) flag += (1 << 5);
  if (PhotonPositionCut(lvl3, 0)) flag += (1 << 6);
  if (PhotonMultiHitCut(lvl3, 0)) flag += (1 << 7);
  if (PionPIDCut(lvl3, 1, 1, eff)) flag += (1 << 8);
  if (PhotonEnergyLinCut(lvl3, 1)) flag += (1 << 9);
  if (PhotonPositionCut(lvl3, 1)) flag += (1 << 10);
  if (PhotonMultiHitCut(lvl3, 1)) flag += (1 << 11);
  if (fPhotonCutBPTX) {
    if (BPTXCut(lvl3)) flag += (1 << 12);
  } else {
    flag += (1 << 12);
  }

  // Add full path flag (0x1)
  if (flag == 0x1FF0) flag += 0x1;
  if (result_cutflag != NULL) {
    *result_cutflag = flag;
  }

  if (flag & 0x1)
    return true;
  else
    return false;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PionTypeIICut(Level3<armrec> *lvl3, Int_t tower, Int_t eff, UInt_t *result_cutflag) {
  // return (PionPIDCut(lvl3, tower, 2, eff) && PhotonEnergyCut(lvl3, tower) && PhotonPositionCut(lvl3, tower, 0) &&
  //         PhotonPositionCut(lvl3, tower, 1) && !PhotonMultiHitCut(lvl3, tower) &&
  //         (fPhotonCutBPTX ? BPTXCut(lvl3) : true));

  UInt_t flag = 0;
  if (PionPIDCut(lvl3, tower, 2, eff)) flag += (1 << 4);
  if (PhotonEnergyCut(lvl3, tower)) flag += (1 << 5);
  if (PhotonPositionCut(lvl3, tower, 0)) flag += (1 << 6);
  if (PhotonPositionCut(lvl3, tower, 1)) flag += (1 << 7);
  if (!PhotonMultiHitCut(lvl3, tower)) flag += (1 << 8);
  if (fPhotonCutBPTX) {
    if (BPTXCut(lvl3)) flag += (1 << 12);
  } else {
    flag += (1 << 12);
  }

  // Add full path flag (0x1)
  if (flag == 0x11F0) flag += 0x1;
  if (result_cutflag != NULL) {
    *result_cutflag = flag;
  }

  if (flag & 0x1)
    return true;
  else
    return false;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PionMassCut(Double_t mass, Int_t type) {
  if (type == kTypeI)
    return mass > arman::kPionMassRange[0][0] && mass < arman::kPionMassRange[0][1];
  else if (type == kTypeII_ST)
    return mass > arman::kPionMassRange[1][0] && mass < arman::kPionMassRange[1][1];
  else if (type == kTypeII_LT)
    return mass > arman::kPionMassRange[2][0] && mass < arman::kPionMassRange[2][1];

  return false;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PionTrueTypeICut(McEvent *mcev) {
  for (Int_t it = 0; it < arman::kCalNtower; ++it) {
    if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != it || mcev->fReference.size() == 0)
      mcev->CheckParticleInTower(arman::kArmIndex, it, arman::kEnergyThr, arman::fFiducial);
    // search most energetic photon in tower
    Bool_t found = false;
    for (Int_t ip = 0; ip < mcev->fReference.size(); ++ip) {
      if (PhotonTruePIDCut(mcev, it, ip) && PhotonTrueEnergyCut(mcev, it, ip) && PhotonTruePositionCut(mcev, it, ip)) {
        found = true;
        break;
      }
    }
    if (!found) return false;
  }
  return true;
}

template <typename arman, typename armrec>
Bool_t EventCut<arman, armrec>::PionTrueTypeIICut(McEvent *mcev, Int_t tower) {
  if (mcev->fRefArm != arman::kArmIndex || mcev->fRefTower != tower || mcev->fReference.size() == 0)
    mcev->CheckParticleInTower(arman::kArmIndex, tower, arman::kEnergyThr, arman::fFiducial);
  // search two most energetic photons in tower
  Bool_t found = false;
  for (Int_t ip = 0; ip < mcev->fReference.size(); ++ip) {
    if (PhotonTruePIDCut(mcev, tower, ip) && PhotonTrueEnergyCut(mcev, tower, ip) &&
        PhotonTruePositionCut(mcev, tower, ip)) {
      for (Int_t jp = ip + 1; jp < mcev->fReference.size(); ++jp) {
        if (PhotonTruePIDCut(mcev, tower, jp) &&
            (mcev->fReference[jp]->Energy() / mcev->fReference[ip]->Energy() > armrec::kPeakRatioThrPhoton) &&
            PhotonTruePositionCut(mcev, tower, jp)) {
          found = true;
          break;
        }
      }
      break;
    }
  }
  return found;
}

template <typename arman, typename armrec>
Double_t EventCut<arman, armrec>::PionL90Boundary(Int_t tower, Double_t energy, Int_t type, Int_t eff) {
  return CF::PionL90Boundary<armrec, arman>(tower, energy, type, eff);
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventCut<Arm1AnPars, Arm1RecPars>;
template class EventCut<Arm2AnPars, Arm2RecPars>;
}  // namespace nLHCf
