#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <install_directory> <tables_directory>"
    exit
fi

install_dir=$1
if [ $# -gt 1 ]; then
    tables_dir=$2
fi

echo "Creating setevn.sh in $install_dir"

cat > $install_dir/setenv.sh <<EOF
#!/bin/bash
export PATH=$install_dir/bin:\$PATH
export LD_LIBRARY_PATH=$install_dir/lib:\$LD_LIBRARY_PATH
export LHCFLIBRARY=$install_dir
export LHCFLIBRARYTABLES=$tables_dir
EOF
