#!/usr/bin/env python

import os
import sys
import fileinput
import random
import time
import subprocess
import shlex
import shutil
import glob
import getopt
import re

from FileList import *
	
#command line options
idir = None
odir = None

def PrnHelp():
	
	help_list = "\n" \
	+ "====================================================\n" \
	+ "===================== MakeLink =====================\n" \
	+ "====================================================\n" \
	+ "\n" \
	+ "-i  input directory\n" \
	+ "-o output directory\n" \

def GetArgs():
	
	global idir, odir
	
	try: 
		opts, args = getopt.getopt(sys.argv[1:],"i:o:h",['help'])
	except getopt.GetoptError as e:
		PrnHelp()

	for o, a in opts:
		if o == '--help' or o == "-h":
			PrnHelp()
		elif o == '-i':
		   	idir=a
		elif o == '-o':
		   	odir=a
		else:
			PrnHelp()

	if idir is None:
		print "Please specify input folder"
		sys.exit()
	if odir is None:
		print "Please specify output folder"
		sys.exit()

	return idir, odir

def main():
	
	global idir, odir
	
	GetArgs()
	
	if not os.path.exists(idir):
		print "Input folder \"" + idir + "\" does not exist: Exit..."
		sys.exit()
	if not os.path.exists(odir):
		print "Output folder \"" + odir + "\" does not exist: Exit..."
		sys.exit()
	
	for filename in FILELIST: #Loop over all files
		ifile = idir + "/" + filename
		ofile = odir + "/" + filename

		ifold = os.path.dirname(ifile)
		ofold = os.path.dirname(ofile)
		if not os.path.exists(ifold):
			print "Input folder \"" + ifold + "\" does not exist: Exit..."
			sys.exit()
		if not os.path.exists(ofold):
			print "Output folder \"" + ofold + "\" does not exist: Exit..."
			sys.exit()
	
		if not os.path.exists(ifile):
			print "Input file \"" + ifile + "\" does not exist: Exit..."
			sys.exit()
		if os.path.islink(ofile):
			print "Overwriting the target symbolic link \"" + ofile
			os.remove(ofile)
		print ifile + " -> " + ofile
		os.symlink(ifile, ofile)


if __name__ == '__main__':
        main()

