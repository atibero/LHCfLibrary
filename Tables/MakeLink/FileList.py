#!/usr/bin/env python

Filelist_Lhc2022 = [ \
	"Op2022/a2_cal_ped_fill8178_80262_80346.root", \
	"Op2022/a2_cal_ped_fill8178_80351_80431.root", \
	"Op2022/a2_cal_ped_fill8178_80351_80372.root", \
	"Op2022/a2_cal_ped_fill8178_80377_80431.root", \
	"Op2022/a2_cal_ped_fill8178_80434_80520.root", \
	"Op2022/a2_cal_ped_fill8178_80523_80626.root", \
	"Op2022/a2_cal_ped_fill8181.root", \
	"Op2022/a2_pos_ped_fill8178_80262_80346.root", \
	"Op2022/a2_pos_ped_fill8178_80351_80431.root", \
	"Op2022/a2_pos_ped_fill8178_80351_80372.root", \
	"Op2022/a2_pos_ped_fill8178_80377_80431.root", \
	"Op2022/a2_pos_ped_fill8178_80434_80520.root", \
	"Op2022/a2_pos_ped_fill8178_80523_80626.root", \
	"Op2022/a2_pos_ped_fill8181.root" \
	]

FILELIST = Filelist_Lhc2022
