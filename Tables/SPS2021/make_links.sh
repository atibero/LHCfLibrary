#!/bin/bash
if [ $# -lt 1 ]; then
    echo "Usage: $0 <source_dir>"
    echo "Create a symbolic link for each table file found in <source_dir> directory into the current directory"
    exit 0
fi

source=$1
if [ ! -d $source ]; then
    echo "source directory does not exist!"
    return -1
fi

for ifile in $(ls $source/a1_*.dat) $(ls $source/a2_*.dat) $(ls $source/a1_*.root) $(ls $source/a2_*.root)
do
    ln -s $ifile ./
done
