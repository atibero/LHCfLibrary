#include "MakePhotonResponseArm2.hh"

#include <TMath.h>
#include <TROOT.h>
#include <dirent.h>

#include <cstdlib>
#include <map>

#include "CoordinateTransformation.hh"
#include "CutFunctions.hh"
#include "Utils.hh"

using namespace nLHCf;

typedef Utils UT;
typedef CutFunctions CF;
typedef CoordinateTransformation CT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
MakePhotonResponseArm2<armcal, armrec, arman, armclass>::MakePhotonResponseArm2()
    : fFirstRun(-1), fLastRun(-1), fEntry(-1), fNevents(-1), fRapidity(-1) {
  CT::SetDetectorPosition(this->kCrossAngleOffset[1]);
  Double_t mp_z = this->kPosLayerDepth[this->kBeamCentreLayer[0]][0];
  mp_z += this->kPosLayerDepth[this->kBeamCentreLayer[1]][1];
  mp_z /= 2.;
  Double_t bc_z = CT::kDetectorZpos + mp_z;
  CT::SetBeamCentrePosition(this->kBeamCentre[0], this->kBeamCentre[1], bc_z);

  fTrueThreshold = 0;

  fOutputFile = nullptr;
  fInputEv = nullptr;

  fMC = nullptr;
  fLvl3 = nullptr;
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armcal, typename armrec, typename arman, typename armclass>
MakePhotonResponseArm2<armcal, armrec, arman, armclass>::~MakePhotonResponseArm2() {
  delete fOutputFile;
  delete fInputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::SetInputTree() {
  UT::Printf(UT::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputTree = new TChain("LHCfEvents");
  fInputTree->SetCacheSize(10000000);

  /* Add input files to TChain */
  if (fInputDir.EndsWith(".root")) {
    TFile ifile(fInputDir.Data(), "READ");
    if (ifile.IsZombie()) {
      ifile.Close();
      UT::Printf(UT::kPrintInfo, "skipping file %s\n", fInputDir.Data());
    } else {
      ifile.Close();
      fInputTree->Add(fInputDir.Data());
    }
  } else if (fFirstRun >= 0 && fLastRun >= 0) {  // get files in [fFirstRun, fLastRun] range
    for (Int_t irun = fFirstRun; irun <= fLastRun; ++irun) {
      TString iname(Form("%s/rec_run%05d.root", fInputDir.Data(), irun));
      TFile ifile(iname.Data(), "READ");
      if (ifile.IsZombie()) {
        ifile.Close();
        UT::Printf(UT::kPrintInfo, "skipping file %s\n", iname.Data());
      } else {
        ifile.Close();
        fInputTree->Add(iname.Data());
      }
    }
  } else {  // get all ROOT files in input directory
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(fInputDir.Data())) != NULL) {
      /* add all .root files to TChain */
      while ((ent = readdir(dir)) != NULL) {
        TString iname = ent->d_name;
        if (iname.EndsWith(".root")) {
          TFile ifile((fInputDir + "/" + iname).Data(), "READ");
          if (ifile.IsZombie()) {
            ifile.Close();
            UT::Printf(UT::kPrintInfo, "skipping file %s\n", (fInputDir + "/" + iname).Data());
          } else {
            ifile.Close();
            fInputTree->Add((fInputDir + "/" + iname).Data());
          }
        }
      }
      closedir(dir);
    } else {
      /* could not open directory */
      UT::Printf(UT::kPrintError, "Cannot open input directory!\n");
      exit(EXIT_FAILURE);
    }
  }
  gROOT->cd();

  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  fInputTree->AddBranchToCache("*");

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::MakeHistograms() {
  UT::Printf(UT::kPrintInfo, "Making histograms...\n");
  fflush(stdout);

  // Create histograms for provide the binning in the response matrix.
  Utils::AllocateVector2D(fH1, this->kPhotonNrap, cNDataType);
  for (Int_t type = 0; type < cNDataType; type++) {
    fH1[fRapidity][type] = CreateHistogram(fRapidity, type);
    if (type == cMCtrue) {
      fTrueThreshold = fH1[fRapidity][type]->GetXaxis()->GetBinLowEdge(1);
      UT::Printf(UT::kPrintInfo, "Setting true energy threshold to %.3f GeV.\n", fTrueThreshold);
    }
  }

  // Initialization for RooUnfoldResponse
  Utils::AllocateVector1D(fResponse, this->kPhotonNrap);
  fResponse[fRapidity] = new RooUnfoldResponse(Form("response_%d", fRapidity), "Response Function");
  fResponse[fRapidity]->Setup(fH1[fRapidity][cMCmeasured], fH1[fRapidity][cMCtrue]);  // measured, true
  // fResponse[fRapidity]->UseOverflow();
  if (fResponse[fRapidity]->UseOverflowStatus()) {
    UT::Printf(UT::kPrintInfo, "Consider under/over-flow in response matrix\n");
  } else {
    UT::Printf(UT::kPrintInfo, "Ignore under/over-flow in response matrix\n");
  }

  // Visualize Response Function and True/Reco histograms
  Utils::AllocateVector1D(fH2, this->kPhotonNrap);
  Utils::AllocateVector1D(fTrue, this->kPhotonNrap);
  Utils::AllocateVector1D(fReco, this->kPhotonNrap);

  // Create histograms for fake and miss events by event type
  Utils::AllocateVector2D(fFake, this->kPhotonNrap, cNEventType);
  Utils::AllocateVector2D(fMiss, this->kPhotonNrap, cNEventType);
  for (Int_t type = 0; type < cNEventType; type++) {
    fFake[fRapidity][type] = CreateHistogram(fRapidity, cFake, type);
    fMiss[fRapidity][type] = CreateHistogram(fRapidity, cMissed, type);
  }

  // Reco Hitmap for check
  TString label = "Singlehit without cuts; x_{RECO} [mm]; y_{RECO} [mm]";
  fRecoSingleHitmap = new TH2D("reco_singlehitmap", label.Data(), 200, -50., 50., 200, -30., 70.);
  fRecoSingleHitmapWithCut = (TH2D *)fRecoSingleHitmap->Clone("reco_singlehitmap_withcut");
  label = "Singlehit with cuts; x_{RECO} [mm]; y_{RECO} [mm]";
  fRecoSingleHitmapWithCut->SetTitle(label.Data());

  // True Hitmap for check
  label = "Singlehit without cuts; x_{TRUE} [mm]; y_{TRUE} [mm]";
  fTrueSingleHitmap = new TH2D("true_singlehitmap", label.Data(), 200, -50., 50., 200, -30., 70.);
  fTrueSingleHitmapWithCut = (TH2D *)fTrueSingleHitmap->Clone("true_singlehitmap_withcut");
  label = "Singlehit with cuts; x_{TRUE} [mm]; y_{TRUE} [mm]";
  fTrueSingleHitmapWithCut->SetTitle(label.Data());

  // Energy Reconstuction for check (Etrue, Emeasured/Etrue)
  Utils::AllocateVector1D(fH2EnergyRes, this->kPhotonNrap);
  string name = Form("h2eneres_%d", fRapidity);
  string title = "Energy Resolusion";
  fH2EnergyRes[fRapidity] = new TH2D(name.c_str(), title.c_str(), 70, 0., 7000., 500, 0.5, 1.5);
  // X = Etrue, Y = Emeasured/Etrue

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
TH1D *MakePhotonResponseArm2<armcal, armrec, arman, armclass>::CreateHistogram(Int_t rapidity, Int_t datatype,
                                                                               Int_t eventtype) {
  // Create the histogram with the binning for spectrum
  Int_t nbin;
  Double_t xbin[50];
  Double_t val;

  // Binning for Small Tower
  if (RapidityToTower(rapidity) == 0) {
    // val = ((datatype == cMCmeasured || datatype == cFake) ? 200. : 100.);
    val = 100.;  // eugenio: uniform true and reco binning
    nbin = 0;
    xbin[0] = val;
    for (Int_t i = 1; i < 50; i++) {
      if (val < 2000. - 0.01)
        val += 100.;
      else if (val < 4000. - 0.01)
        val += 200.;
      else if (val < 5000. - 0.01)
        val += 500.;
      else if (val < 7000. - 0.01)
        val += 1000.;
      else
        break;
      xbin[i] = val;
      nbin++;
    }
  }
  // Binning for Large Tower
  else if (RapidityToTower(rapidity) == 1) {
    // val = ((datatype == cMCmeasured || datatype == cFake) ? 200. : 100.);
    val = 100.;  // eugenio: uniform true and reco binning
    nbin = 0;
    xbin[0] = val;
    for (Int_t i = 1; i < 50; i++) {
      if (val < 2000. - 0.01)
        val += 100.;
      else if (val < 3000. - 0.01)
        val += 200.;
      else if (val < 4000. - 0.01)
        val += 500.;
      else if (val < 7000. - 0.01)
        val += 1000.;
      else
        break;
      xbin[i] = val;
      nbin++;
    }
  } else {
    return NULL;
  }

  // Rescale energy to use the same Feynman binning
  if (this->fOperation == kLHC2022) {
    if (eventtype < 0)
      UT::Printf(UT::kPrintInfo, "Scaling energy binning to 13.6 TeV for histogram %d!\n", datatype);
    else
      UT::Printf(UT::kPrintInfo, "Scaling energy binning to 13.6 TeV for histogram %d[%d]!\n", datatype, eventtype);
    for (Int_t ib = 0; ib <= nbin; ++ib) {
      xbin[ib] *= 6.8 / 6.5;
    }
  }

  // Memory allocation
  TString tag = "None";
  if (datatype == cMCmeasured)
    tag = "Measured";
  else if (datatype == cMCtrue)
    tag = "True";
  else if (datatype == cMissed)
    tag = "Missed";
  else if (datatype == cFake)
    tag = "Fake";
  if (eventtype == cEVENT_NOHIT)
    tag += " - NoHit";
  else if (eventtype == cEVENT_SINGLE_G)
    tag += " - SingleG";
  else if (eventtype == cEVENT_MULTI_GG)
    tag += " - MultiGG";
  else if (eventtype == cEVENT_MULTI_GH)
    tag += " - MultiGH";
  else if (eventtype == cEVENT_MULTI_HH)
    tag += " - MultiHH";
  else if (eventtype == cEVENT_MULTI_MANY)
    tag += " - MultiMany";
  else if (eventtype == cEVENT_OTHERS)
    tag += " - Others";
  TString name;
  if (eventtype < 0)
    name = Form("h1_%d_%d", rapidity, datatype);
  else
    name = Form("h1bis_%d_%d_%d", rapidity, datatype, eventtype);
  TString title = Form("Region %d - %s; E [GeV]; Counts", rapidity, tag.Data());
  TH1D *h = new TH1D(name.Data(), title.Data(), nbin, xbin);
  return h;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::ClearEvent() {
  fMC = nullptr;
  fLvl3 = nullptr;

  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::WriteToOutput() {
  UT::Printf(UT::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputFile->Write();
  fResponse[fRapidity]->Write();

  gROOT->cd();
  UT::Printf(UT::kPrintInfo, " Done.\n");
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::CloseFiles() {
  UT::Printf(UT::kPrintInfo, "Closing output file...");
  fflush(stdout);

  fOutputFile->Close();

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*
 * Build Response Matrix
 */

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::FillEntry() {
  // for (Int_t rapidity = 0; rapidity < this->kPhotonNrap; ++rapidity) {
  Int_t tower = RapidityToTower(fRapidity);

  //  // Reject event rejected by trigger condition
  //  if (!CF::PhotonDiscriminatorCut<armrec, arman>(fLvl3, tower)) return;
  //  // NB: This effect must be corrected outside unfolding

  // Selection MC-TRUE single-photon event
  EVENTTYPE evtype = SelectionByMCtrue(tower);

  UT::Printf(UT::kPrintDebug, "RECO %d %f\n", evtype, fLvl3->fPhotonEnergy[tower]);
  if (fMC->fReference.size() > 0) {
    for (int ip = 0; ip < fMC->fReference.size(); ++ip) {
      UT::Printf(UT::kPrintDebug, "TRUE %d %f\n", ip, fMC->fReference[ip]->Energy());
    }
  }

  switch (evtype) {
    case cEVENT_SINGLE_G:
      EventSinglePhoton(tower);
      break;
    case cEVENT_MULTI_GG:
      EventMultiGG(tower);
      break;
    case cEVENT_MULTI_GH:
      EventMultiGH(tower);
      break;
    case cEVENT_MULTI_MANY:
      EventMultiMany(tower);
      break;
    default:;
      ;  // continue;
      break;
  }
  //}
}

template <typename armcal, typename armrec, typename arman, typename armclass>
EVENTTYPE MakePhotonResponseArm2<armcal, armrec, arman, armclass>::SelectionByMCtrue(Int_t tower) {
  Int_t trueHit = fMC->CheckParticleInTower(this->kArmIndex, tower, 50., 0., false, false);

  Bool_t code1 = CF::PhotonTruePIDCut<armrec, arman>(fMC, tower, 0);
  Bool_t kene1 = false;
  if (fMC->fReference.size() > 0) {
    if (fMC->fReference[0]->Energy() > fTrueThreshold) {
      kene1 = true;
    }
  }
  // Bool_t kene1 = CF::PhotonTrueEnergyCut<armrec, arman>(fMC, tower, 0); //This is 200 GeV

  // Selection of Event type
  // Single-Photon
  if (trueHit == 1) {
    if (code1 && kene1) {
      return cEVENT_SINGLE_G;
    }
    return cEVENT_NOHIT;
  } else if (trueHit == 2) {
    if (!kene1) {
      return cEVENT_NOHIT;
    }
    Bool_t code2 = CF::PhotonTruePIDCut<armrec, arman>(fMC, tower, 1);
    if (code1 && code2) {
      return cEVENT_MULTI_GG;
    } else if (!code1 && !code2) {
      return cEVENT_MULTI_HH;
    } else {
      return cEVENT_MULTI_GH;
    }
  } else if (trueHit >= 3) {
    return cEVENT_MULTI_MANY;
  }
  return cEVENT_NOHIT;
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Bool_t MakePhotonResponseArm2<armcal, armrec, arman, armclass>::IsPositionWellReconstructed(Int_t tower) {
  // Check the detection of photon in the reconstructed data.
  static const Double_t threshold_distance = 3.0;  // [mm];

  // True position
  TVector3 abspos = fMC->fReference[0]->Position();
  TVector3 trupos = CT::LhcToCollision(abspos);
  trupos = CT::ExtrapolateToZ(trupos, CT::kDetectorZpos);

  // Reco position
  TVector3 recpos = CF::PhotonGlobalPosition<armrec, arman>(fLvl3, tower);

  // Difference
  TVector3 difpos = recpos - trupos;
  if (TMath::Abs(difpos.X()) > threshold_distance) return false;
  if (TMath::Abs(difpos.y()) > threshold_distance) return false;

  return true;
}

/*
 * This is 2015 approach: eugenio does not like it
 */
// template <typename armcal, typename armrec, typename arman, typename armclass>
// void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::EventSinglePhoton(Int_t tower) {
//  // True position
//  TVector3 abspos = fMC->fReference[0]->Position();
//  TVector3 trupos = CT::LhcToCollision(abspos);
//  trupos = CT::ExtrapolateToZ(trupos, CT::kDetectorZpos);
//  // Reco position
//  TVector3 recpos = CF::PhotonGlobalPosition<armrec, arman>(fLvl3, tower);
//
//  fRecoSingleHitmap->Fill(-recpos.X(), recpos.Y());
//  fTrueSingleHitmap->Fill(-trupos.X(), trupos.Y());
//
//  // Acceptance cut
//  // !! IMPORTANT !! the acceptance cut must be performed with
//  // one of the MC-true or the MC-measured for an event.
//  if (!CF::PhotonTrueRapidityCut<armrec, arman>(fMC, fRapidity, 0)) return;
//  // Do not perform the PID cut by L90% and multi-hit cut in reconstructed data
//  // because they should be already corrected before the unfolding.
//
//  // Fill to the response matrix
//  Double_t Emeasured = fLvl3->fPhotonEnergy[tower];
//  Double_t Etrue = fMC->fReference[0]->Energy();
//  // trupos.Print();
//  if (CF::PhotonDiscriminatorCut<armrec, arman>(fLvl3, tower) && IsPositionWellReconstructed(tower) &&
//      CF::PhotonMultiHitCut<armrec, arman>(fLvl3, tower)) {  // eugenio
//    fRecoSingleHitmapWithCut->Fill(-recpos.X(), recpos.Y());
//    fTrueSingleHitmapWithCut->Fill(-trupos.X(), trupos.Y());
//    Fill(Emeasured, Etrue);
//  } else {
//    fMiss[fRapidity][cEVENT_SINGLE_G]->Fill(Etrue);
//    Miss(Etrue);
//  }
//}

/*
 * This is 2022 approach: implemented by eugenio
 */
template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::EventSinglePhoton(Int_t tower) {
  // True position
  TVector3 abspos = fMC->fReference[0]->Position();
  TVector3 trupos = CT::LhcToCollision(abspos);
  trupos = CT::ExtrapolateToZ(trupos, CT::kDetectorZpos);
  // Reco position
  TVector3 recpos = CF::PhotonGlobalPosition<armrec, arman>(fLvl3, tower);

  fRecoSingleHitmap->Fill(-recpos.X(), recpos.Y());
  fTrueSingleHitmap->Fill(-trupos.X(), trupos.Y());

  // Do not perform the PID cut by L90% and multi-hit cut in reconstructed data
  // because they should be already corrected before the unfolding.

  Bool_t accreco = CF::PhotonPositionCut<armrec, arman>(fLvl3, tower);
  accreco = accreco && CF::PhotonRapidityCut<armrec, arman>(fLvl3, fRapidity);
  Bool_t acctrue = CF::PhotonTruePositionCut<armrec, arman>(fMC, tower, 0);
  acctrue = acctrue && CF::PhotonTrueRapidityCut<armrec, arman>(fMC, fRapidity, 0);

  // Remove the events for which there is no hit in region
  // both and true and reconstructed variables
  if (!accreco && !acctrue) return;

  // Fill to the response matrix
  Double_t Emeasured = fLvl3->fPhotonEnergy[tower];
  Double_t Etrue = fMC->fReference[0]->Energy();
  // trupos.Print();
  if (CF::PhotonDiscriminatorCut<armrec, arman>(fLvl3, tower) && CF::PhotonMultiHitCut<armrec, arman>(fLvl3, tower)) {
    if (accreco && acctrue) {
      Fill(Emeasured, Etrue);
    } else if (!accreco && acctrue) {
      fMiss[fRapidity][cEVENT_SINGLE_G]->Fill(Etrue);
      Miss(Etrue);
    } else if (accreco && !acctrue) {
      fFake[fRapidity][cEVENT_SINGLE_G]->Fill(Emeasured);
      Fake(Emeasured);
    }
    if (accreco) fRecoSingleHitmapWithCut->Fill(-recpos.X(), recpos.Y());
    if (acctrue) fTrueSingleHitmapWithCut->Fill(-trupos.X(), trupos.Y());
  } else {
    // Artificially fill cEVENT_NOHIT to distinguish from previous Miss
    fMiss[fRapidity][cEVENT_NOHIT]->Fill(Etrue);
    Miss(Etrue);
  }
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::EventMultiGG(Int_t tower) {
  // == For MC true ==
  for (Int_t ip = 0; ip < 2; ip++) {
    if (fMC->fReference[ip]->Energy() > fTrueThreshold &&
        CF::PhotonTrueRapidityCut<armrec, arman>(fMC, fRapidity, ip) &&
        CF::PhotonTruePIDCut<armrec, arman>(fMC, tower, ip)) {
      Double_t Etrue = fMC->fReference[ip]->Energy();
      fMiss[fRapidity][cEVENT_MULTI_GG]->Fill(Etrue);
      Miss(Etrue);
    }
  }

  // Case : Miss-ID of Multi-hit event
  if (CF::PhotonDiscriminatorCut<armrec, arman>(fLvl3, tower) && CF::PhotonMultiHitCut<armrec, arman>(fLvl3, tower) &&
      CF::PhotonRapidityCut<armrec, arman>(fLvl3, fRapidity)) {
    Double_t Emeasured = fLvl3->fPhotonEnergy[tower];
    fFake[fRapidity][cEVENT_MULTI_GG]->Fill(Emeasured);
    Fake(Emeasured);
  }

  // Case : MH detected
  // nothing
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::EventMultiGH(Int_t tower) {
  // == For MC true ==
  for (Int_t ip = 0; ip < 2; ip++) {
    if (fMC->fReference[ip]->Energy() > fTrueThreshold &&
        CF::PhotonTrueRapidityCut<armrec, arman>(fMC, fRapidity, ip) &&
        CF::PhotonTruePIDCut<armrec, arman>(fMC, tower, ip)) {
      Double_t Etrue = fMC->fReference[ip]->Energy();
      fMiss[fRapidity][cEVENT_MULTI_GH]->Fill(Etrue);
      Miss(Etrue);
    }
  }

  // Case : Miss-ID of Multi-hit event
  if (CF::PhotonDiscriminatorCut<armrec, arman>(fLvl3, tower) && CF::PhotonMultiHitCut<armrec, arman>(fLvl3, tower) &&
      CF::PhotonRapidityCut<armrec, arman>(fLvl3, fRapidity) && CF::PhotonPIDCut<armrec, arman>(fLvl3, tower)) {
    Double_t Emeasured = fLvl3->fPhotonEnergy[tower];
    fFake[fRapidity][cEVENT_MULTI_GH]->Fill(Emeasured);
    Fake(Emeasured);
  }

  // Case : MH detected or hadron-like
  // nothing
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::EventMultiMany(Int_t tower) {
  // Actually tower is mostly same as EventMultiGH

  // == For MC true ==
  for (Int_t ip = 0; ip < 3; ip++) {  // Only up to third energetic particles are considered
    if (fMC->fReference[ip]->Energy() > fTrueThreshold &&
        CF::PhotonTrueRapidityCut<armrec, arman>(fMC, fRapidity, ip) &&
        CF::PhotonTruePIDCut<armrec, arman>(fMC, tower, ip)) {
      Double_t Etrue = fMC->fReference[ip]->Energy();
      fMiss[fRapidity][cEVENT_MULTI_MANY]->Fill(Etrue);
      Miss(Etrue);
    }
  }

  // Case : Miss-ID of Multi-hit event
  if (CF::PhotonDiscriminatorCut<armrec, arman>(fLvl3, tower) && CF::PhotonMultiHitCut<armrec, arman>(fLvl3, tower) &&
      CF::PhotonRapidityCut<armrec, arman>(fLvl3, fRapidity) && CF::PhotonPIDCut<armrec, arman>(fLvl3, tower)) {
    Double_t Emeasured = fLvl3->fPhotonEnergy[tower];
    fFake[fRapidity][cEVENT_MULTI_MANY]->Fill(Emeasured);
    Fake(Emeasured);
  }

  // Case : MH detected or hadron-like
  // nothing
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Int_t MakePhotonResponseArm2<armcal, armrec, arman, armclass>::Fill(Double_t Emeasured, Double_t Etrue) {
  fH1[fRapidity][cMCmeasured]->Fill(Emeasured);
  fH1[fRapidity][cMCtrue]->Fill(Etrue);
  fH2EnergyRes[fRapidity]->Fill(Etrue, Emeasured / Etrue);
  return fResponse[fRapidity]->Fill(Emeasured, Etrue);
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Int_t MakePhotonResponseArm2<armcal, armrec, arman, armclass>::Miss(Double_t Etrue) {
  // fH1[fRapidity][cMCtrue]->Fill(Etrue);
  fH1[fRapidity][cMissed]->Fill(Etrue);
  return fResponse[fRapidity]->Miss(Etrue);
}

template <typename armcal, typename armrec, typename arman, typename armclass>
Int_t MakePhotonResponseArm2<armcal, armrec, arman, armclass>::Fake(Double_t Emeasured) {
  // fH1[fRapidity][cMeasured]->Fill(Emeasured);
  fH1[fRapidity][cFake]->Fill(Emeasured);
  return fResponse[fRapidity]->Fake(Emeasured);
}

/*
 * Extract information from Response Matrix
 */

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::ExtractHistograms() {
  // Response Matrix
  fH2[fRapidity] = (TH2D *)fResponse[fRapidity]->Hresponse()->Clone(Form("h2response_%d", fRapidity));

  // True distrubution
  fTrue[fRapidity] = (TH1D *)fH1[fRapidity][cMCtrue]->Clone();
  fTrue[fRapidity]->SetName(Form("true_energy_%d", fRapidity));
  fTrue[fRapidity]->SetTitle("; E_{TRUE} [GeV]; Counts");
  fTrue[fRapidity]->Reset();
  fTrue[fRapidity]->Add(fH1[fRapidity][cMCtrue]);
  fTrue[fRapidity]->Add(fH1[fRapidity][cMissed]);

  // Reco distribution
  fReco[fRapidity] = (TH1D *)fH1[fRapidity][cMCmeasured]->Clone();
  fReco[fRapidity]->SetName(Form("reco_energy_%d", fRapidity));
  fReco[fRapidity]->SetTitle("; E_{RECO} [GeV]; Counts");
  fReco[fRapidity]->Reset();
  fReco[fRapidity]->Add(fH1[fRapidity][cMCmeasured]);
  fReco[fRapidity]->Add(fH1[fRapidity][cFake]);
}

/*
 * Draw Response Matrix
 */

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::DrawHistograms() {
  DrawResponseFunction();
  DrawFilledHistograms();
  DrawHitMap();
  DrawEnergyReco();
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::DrawResponseFunction() {
  TCanvas *c = new TCanvas("c_res", "Response matrix", 600, 600);
  // c->Divide(2, 1, 0.001, 0.001);
  // for (Int_t tower = 0; tower < this->kPhotonNrap; tower++) {
  // c->cd(tower + 1);
  gPad->SetLogz();
  gPad->SetGrid();

  TH2D *h = fH2[fRapidity];
  h->Draw("COLZ");
  h->SetTitle(Form("Response Function : Region %d", fRapidity));
  h->SetXTitle("MC measured");
  h->SetYTitle("MC true");
  h->GetYaxis()->SetTitleOffset(1.3);

  gPad->Update();
  //}
  c->Print(Form("img/response_matrix_%d.eps", fRapidity));
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::DrawFilledHistograms() {
  TCanvas *c = new TCanvas("c_hist", "Histograms", 600, 600);
  // c->Divide(2, 1, 0.001, 0.001);
  // for (Int_t tower = 0; tower < this->kPhotonNrap; tower++) {
  // c->cd(tower + 1);
  gPad->SetLogy();
  gPad->SetTicks();

  fH1[fRapidity][cMCtrue]->SetMarkerStyle(20);
  fH1[fRapidity][cMCtrue]->SetMarkerSize(0.8);
  fH1[fRapidity][cMCtrue]->SetMarkerColor(kBlue);
  fH1[fRapidity][cMCtrue]->SetLineColor(kBlue);
  fH1[fRapidity][cMCmeasured]->SetMarkerStyle(24);
  fH1[fRapidity][cMCmeasured]->SetMarkerSize(0.8);
  fH1[fRapidity][cMCmeasured]->SetMarkerColor(kRed);
  fH1[fRapidity][cMCmeasured]->SetLineColor(kRed);
  fH1[fRapidity][cMissed]->SetMarkerStyle(20);
  fH1[fRapidity][cMissed]->SetMarkerSize(0.8);
  fH1[fRapidity][cMissed]->SetMarkerColor(kGreen + 2);
  fH1[fRapidity][cMissed]->SetLineColor(kGreen + 2);
  fH1[fRapidity][cFake]->SetMarkerStyle(20);
  fH1[fRapidity][cFake]->SetMarkerSize(0.8);
  fH1[fRapidity][cFake]->SetMarkerColor(kYellow + 2);
  fH1[fRapidity][cFake]->SetLineColor(kYellow + 2);

  fH1[fRapidity][cMCtrue]->Draw("PE1");
  fH1[fRapidity][cMCmeasured]->Draw("PE1,same");
  fH1[fRapidity][cMissed]->Draw("PE1,same");
  fH1[fRapidity][cFake]->Draw("PE1,same");

  TH1D *h = fH1[fRapidity][cMCtrue];
  h->SetTitle(Form("Region %d", fRapidity));
  h->SetXTitle("E_{true} or E_{rec} [GeV]");
  h->SetYTitle("Event/bin");

  TLegend *legend = new TLegend(0.5, 0.7, 0.78, 0.88);
  legend->AddEntry(fH1[fRapidity][cMCmeasured], "Measured", "PLE");
  legend->AddEntry(fH1[fRapidity][cMCtrue], "True", "PLE");
  legend->AddEntry(fH1[fRapidity][cMissed], "Missed", "PLE");
  legend->AddEntry(fH1[fRapidity][cFake], "Fake", "PLE");
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->Draw();

  gPad->Update();
  //}
  c->Print(Form("img/histograms_%d.eps", fRapidity));
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::DrawHitMap() {
  // TCanvas *c = new TCanvas("c_hitmap","Hit Map",600,600);
  TCanvas *c = new TCanvas("c_hitmap", "Hit Map", 1200, 600);
  c->Divide(2, 1);
  c->cd(1);
  fRecoSingleHitmap->Draw("BOX");
  fRecoSingleHitmapWithCut->Draw("COLZ,same");
  c->cd(2);
  fTrueSingleHitmap->Draw("BOX");
  fTrueSingleHitmapWithCut->Draw("COLZ,same");
  gPad->Update();
  c->Print(Form("img/hitmap_%d.eps", fRapidity));
}

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::DrawEnergyReco() {
  TCanvas *c = new TCanvas("c_erec", "Energy Rec.", 600, 600);
  // c->Divide(2, 1, 0.001, 0.001);
  // for (Int_t tower = 0; tower < this->kPhotonNrap; tower++) {
  // c->cd(tower + 1);
  gPad->SetGrid();
  fH2EnergyRes[fRapidity]->Draw("COLZ");
  gPad->Update();
  //}
  c->Print(Form("img/resolution_%d.eps", fRapidity));
}

/*-------------------*/
/*--- Main Method ---*/
/*-------------------*/

template <typename armcal, typename armrec, typename arman, typename armclass>
void MakePhotonResponseArm2<armcal, armrec, arman, armclass>::Run() {
  SetInputTree();

  fOutputFile = new TFile(fOutputName, "RECREATE");

  MakeHistograms();

  /*--- Event Loop ---*/
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  Int_t nentries = fInputTree->GetEntries();
  if (fNevents > 0) nentries = TMath::Min(nentries, fNevents);
  for (fEntry = 0; fEntry <= nentries; ++fEntry) {
    if (fEntry % 100000 == 0 || fEntry == nentries - 1) {
      UT::Printf(UT::kPrintInfo, "\r\tEvent %d", fEntry);
      fflush(stdout);
    }
    fInputTree->GetEntry(fEntry);

    fMC = (McEvent *)fInputEv->Get(Form("true_a%d", this->kArmIndex + 1));
    if (!fMC) continue;

    fLvl3 = (Level3<armrec> *)fInputEv->Get(Form("lvl3_a%d", this->kArmIndex + 1));
    if (!fLvl3) continue;

    FillEntry();

    ClearEvent();
  }
  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");

  ExtractHistograms();
  DrawHistograms();

  /* Write to output file */
  WriteToOutput();
  CloseFiles();
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class MakePhotonResponseArm2<Arm1CalPars, Arm1RecPars, Arm1AnPars, Arm1Params>;
template class MakePhotonResponseArm2<Arm2CalPars, Arm2RecPars, Arm2AnPars, Arm2Params>;
}  // namespace nLHCf
