#include <TApplication.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLegend.h>
#include <TMath.h>
#include <TROOT.h>
#include <TRandom3.h>
#include <TRint.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "Arm1AnPars.hh"
#include "Arm2AnPars.hh"
#include "RooUnfoldBayes.h"
#include "RooUnfoldResponse.h"
#include "Utils.hh"
#include "utils.h"
#include "version.h"

using namespace nLHCf;

typedef Utils UT;

class UnfoldHistogram {
 public:
  vector<RooUnfoldResponse*> fResponse;
  vector<TH1D*> fTruth;
  vector<TH1D*> fMeasured;
  vector<TH1D*> fUnfolded;
  vector<TH1D*> fRebinned;
  vector<TH1D*> fRatio;
  vector<TH1D*> fCheck;

  Int_t fNRegions;
  Int_t fRapidity;
  Int_t fNIterations;

 public:
  UnfoldHistogram() {
    fNRegions = -1;
    fRapidity = -1;
    fNIterations = -1;
  }

  void SetArm(Int_t arm) {
    int nrapidity = -1;
    if (Arm1Params::kArmIndex) {
      nrapidity = Arm1AnPars::kPhotonNrap;
    } else if (Arm2Params::kArmIndex) {
      nrapidity = Arm2AnPars::kPhotonNrap;
    } else {
      exit(EXIT_FAILURE);
    }
    Utils::AllocateVector1D(fResponse, nrapidity);
    Utils::AllocateVector1D(fTruth, nrapidity);
    Utils::AllocateVector1D(fMeasured, nrapidity);
    Utils::AllocateVector1D(fUnfolded, nrapidity);
    Utils::AllocateVector1D(fRebinned, nrapidity);
    Utils::AllocateVector1D(fRatio, nrapidity);
    Utils::AllocateVector1D(fCheck, nrapidity);
  };
  void SetRapidity(Int_t rapidity) { fRapidity = rapidity; };
  void SetNIterations(Int_t niter) { fNIterations = niter; };

  Int_t SetResponseFunction(TString matrixfile);
  Int_t SetMeasuredHistogram(TString histofile, TString histoname);
  Int_t SetTrueHistogram(TString histofile, TString histoname);

  Int_t RapidityToTower(Int_t rapidity);

  TH1D* Rebin(TH1D* hold, TH1D* hnew);

  TH1D* GetMeasuredHistogram(Int_t rapidity) { return fMeasured[rapidity]; }
  TH1D* GetUnfoldedHistogram(Int_t rapidity) { return fUnfolded[rapidity]; }

  Int_t Calculate();
  Int_t Unfold(Int_t rapidity, Int_t fNIterations);
  Int_t Write(TString outputfile);
  Int_t Draw();
  Int_t DrawHistogram();
  Int_t DrawRatio();
};

Int_t UnfoldHistogram::SetResponseFunction(TString matrixfile) {
  TFile* file = new TFile(matrixfile.Data());
  if (file->IsZombie()) {
    cerr << "Cannot Open" << matrixfile.Data() << endl;
    exit(EXIT_FAILURE);
  }

  TString name = Form("response_%d", fRapidity);
  if (!file->Get(name)) {
    cerr << "Cannot find " << name << endl;
    exit(EXIT_FAILURE);
  }
  fResponse[fRapidity] = (RooUnfoldResponse*)file->Get(name);

  return 0;
}

Int_t UnfoldHistogram::SetMeasuredHistogram(TString histofile, TString histoname) {
  TFile* file = new TFile(histofile.Data());
  if (file->IsZombie()) {
    cerr << "Cannot Open" << histofile.Data() << endl;
    exit(EXIT_FAILURE);
  }

  TString name = histoname.Data();
  if (!file->Get(name)) {
    cerr << "Cannot find " << name << endl;
    exit(EXIT_FAILURE);
  }

  fMeasured[fRapidity] = (TH1D*)file->Get(name);

  return 0;
}

Int_t UnfoldHistogram::SetTrueHistogram(TString histofile, TString histoname) {
  TFile* file = new TFile(histofile.Data());
  if (file->IsZombie()) {
    cerr << "Cannot Open" << histofile.Data() << endl;
    exit(EXIT_FAILURE);
  }

  TString name = histoname.Data();
  if (!file->Get(name)) {
    return -1;
  }

  fTruth[fRapidity] = (TH1D*)file->Get(name);

  return 0;
}

Int_t UnfoldHistogram::RapidityToTower(Int_t rapidity) {
  Int_t tower = -1;
  if (rapidity >= 0 && rapidity <= 2) {
    tower = 0;
  } else if (rapidity >= 3 && rapidity <= 5) {
    tower = 1;
  }
  return tower;
}

TH1D* UnfoldHistogram::Rebin(TH1D* hold, TH1D* hnew) {
  TH1D* h = (TH1D*)hold->Clone();
  h->SetName(Form("hrebinned_%d", fRapidity));
  h->GetXaxis()->SetTitle(hnew->GetXaxis()->GetTitle());
  h->GetYaxis()->SetTitle(hnew->GetYaxis()->GetTitle());
  h->SetTitle(hnew->GetTitle());

  h->Reset();
  for (Int_t ib = 0; ib < h->GetXaxis()->GetNbins(); ++ib) {
    Double_t center = h->GetXaxis()->GetBinCenter(ib);

    Int_t bin = hnew->FindBin(center);
    Double_t con = hnew->GetBinContent(bin);
    Double_t err = hnew->GetBinError(bin);

    h->SetBinContent(ib, con);
    h->SetBinError(ib, err);
  }

  return h;
}

Int_t UnfoldHistogram::Calculate() {
  Unfold(fRapidity, fNIterations);
  return 0;
}

Int_t UnfoldHistogram::Unfold(Int_t rapidity, Int_t niterations) {
  UT::Printf(UT::kPrintInfo, "Unfolding region %d with %d iterations\n", rapidity, niterations);
  RooUnfoldBayes unfold(fResponse[rapidity], fMeasured[rapidity], niterations);
  // unfold.IncludeSystematics(1);
  fUnfolded[rapidity] = (TH1D*)unfold.Hreco();

  fRebinned[rapidity] = Rebin(fMeasured[rapidity], fUnfolded[rapidity]);

  TH1D* h = (TH1D*)fMeasured[rapidity]->Clone();
  for (int i = 1; i <= h->GetNbinsX(); i++) {
    double x, val[2];
    x = h->GetBinCenter(i);
    val[0] = fMeasured[rapidity]->GetBinContent(fMeasured[rapidity]->FindFixBin(x));
    val[1] = fUnfolded[rapidity]->GetBinContent(fUnfolded[rapidity]->FindFixBin(x));
    if (val[1] == 0) {
      continue;
    }
    h->SetBinContent(i, val[0] / val[1]);
    h->SetBinError(i, sqrt(val[0]) / val[1]);
  }
  fRatio[rapidity] = h;

  if (fTruth[rapidity]) {
    fCheck[rapidity] = (TH1D*)fTruth[rapidity]->Clone();
    fCheck[rapidity]->SetName("hcheck");
    fCheck[rapidity]->SetXTitle("Energy [GeV]");
    fCheck[rapidity]->SetYTitle("Truth/Unfolded");
    fCheck[rapidity]->Divide(fRebinned[rapidity]);
  }

  return 0;
}

Int_t UnfoldHistogram::Write(TString outputfile) {
  TFile* file = new TFile(outputfile.Data(), "RECREATE");
  file->cd();

  if (fTruth[fRapidity]) fTruth[fRapidity]->SetName(Form("htruth_%d", fRapidity));
  fMeasured[fRapidity]->SetName(Form("hmeasured_%d", fRapidity));
  fUnfolded[fRapidity]->SetName(Form("hunfolded_%d", fRapidity));
  fRebinned[fRapidity]->SetName(Form("hrebinned_%d", fRapidity));
  fRatio[fRapidity]->SetName(Form("hratio_%d", fRapidity));
  if (fCheck[fRapidity]) fCheck[fRapidity]->SetName(Form("hcheck_%d", fRapidity));

  if (fTruth[fRapidity]) fTruth[fRapidity]->SetTitle(Form("Region %d", fRapidity));
  fMeasured[fRapidity]->SetTitle(Form("Region %d", fRapidity));
  fUnfolded[fRapidity]->SetTitle(Form("Region %d", fRapidity));
  fRebinned[fRapidity]->SetTitle(Form("Region %d", fRapidity));
  fRatio[fRapidity]->SetTitle(Form("Region %d", fRapidity));
  if (fCheck[fRapidity]) fCheck[fRapidity]->SetTitle(Form("Region %d", fRapidity));

  if (fTruth[fRapidity]) fTruth[fRapidity]->Write();
  fMeasured[fRapidity]->Write();
  fUnfolded[fRapidity]->Write();
  fRebinned[fRapidity]->Write();
  fRatio[fRapidity]->Write();
  if (fCheck[fRapidity]) fCheck[fRapidity]->Write();

  file->Write();
  file->Close();
  return 0;
}

Int_t UnfoldHistogram::Draw() {
  DrawHistogram();
  DrawRatio();
  return 0;
}

Int_t UnfoldHistogram::DrawHistogram() {
  TCanvas* c = new TCanvas("c_unfold", "Unfold", 600, 600);
  c->Divide(1, 1);
  c->cd(1);

  gPad->SetLogy();
  gPad->SetTicks();

  fMeasured[fRapidity]->SetLineColor(kRed);
  fMeasured[fRapidity]->SetMarkerColor(kRed);
  fMeasured[fRapidity]->SetMarkerStyle(24);
  fMeasured[fRapidity]->SetMarkerSize(0.8);
  fUnfolded[fRapidity]->SetLineColor(kBlack);
  fUnfolded[fRapidity]->SetMarkerColor(kBlack);
  fUnfolded[fRapidity]->SetMarkerStyle(20);
  fUnfolded[fRapidity]->SetMarkerSize(0.8);

  fMeasured[fRapidity]->Draw("hist,PE1");
  fUnfolded[fRapidity]->Draw("hist,PE1,same");

  TH1D* h = fMeasured[fRapidity];
  h->SetXTitle("Energy [GeV]");
  h->SetYTitle("Events/bin");

  gPad->Update();

  c->Update();
  return 0;
}

Int_t UnfoldHistogram::DrawRatio() {
  TCanvas* c = new TCanvas("c_ratio", "Ratio", 600, 600);
  c->Divide(1, 1);
  c->cd(1);

  gPad->SetGrid();
  fRatio[fRapidity]->Draw("hist,PE1");

  TH1D* h = fRatio[fRapidity];
  h->SetXTitle("Energy [GeV]");
  h->SetYTitle("Reco/Unfolded");

  c->Update();
  return 0;
}

void usage(char* argv0) {
  UT::Printf(UT::kPrintInfo, "\nUsage: %s [options]\n\n", argv0);
  UT::Printf(UT::kPrintInfo, "Available options:\n");
  UT::Printf(UT::kPrintInfo, "\t-d <inputfile>\t\tInput file to be unfolded\n");
  UT::Printf(UT::kPrintInfo, "\t-m <responsefile>\tInput file containing response matrix\n");
  UT::Printf(UT::kPrintInfo, "\t-o <outputfile>\t\tOutput ROOT file (default = \"unfolded.root\")\n");
  UT::Printf(UT::kPrintInfo, "\t-r <rapidity>\t\tRapidity region to be considered\n");
  UT::Printf(UT::kPrintInfo, "\t-n <niter>\t\tNumber of iterations (default = 4)\n");
  UT::Printf(UT::kPrintInfo, "\t-v <verbose>\t\tSet verbose level (default = 1)\n");
  UT::Printf(UT::kPrintInfo, "\t\t\t\t\t0 -> only errors\n");
  UT::Printf(UT::kPrintInfo, "\t\t\t\t\t1 -> some info\n");
  UT::Printf(UT::kPrintInfo, "\t\t\t\t\t2 -> debug\n");
  UT::Printf(UT::kPrintInfo, "\t\t\t\t\t3 -> all debug\n");
  UT::Printf(UT::kPrintInfo, "\t-h\t\t\tShow usage\n");
}

int main(int argc, char** argv) {
  /*--- Default values ---*/
  int arm = 4;
  int niter = 4;
  int rapidity = -1;
  TString file_response = "";
  TString file_data = "";
  TString file_out = "unfolded.root";
  bool verbose = 1;

  /*--- Options list ---*/
  const struct option opt_list[] = {
      /* {const char *name, int has_arg, int *flag, int val}         */
      /* has_arg = no_argument, required_argument, optional_argument */
      {"inputfile", required_argument, NULL, 'd'},
      {"responsefile", required_argument, NULL, 'm'},
      {"outputfile", required_argument, NULL, 'o'},
      {"rapidity", required_argument, NULL, 'r'},
      {"niter", required_argument, NULL, 'n'},
      {"arm1", no_argument, NULL, 1001},
      {"arm2", no_argument, NULL, 1002},
      {"verbose", required_argument, NULL, 'v'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0} /* required by getopt_long */
  };
  const int nopt = sizeof(opt_list) / sizeof(opt_list[0]);

  /* Automatically build getopt option-characters string */
  char opt_str[STRLEN];
  build_getopt_str(opt_list, nopt, opt_str);

  /* Parse options */
  int c;
  while ((c = getopt_long(argc, argv, opt_str, opt_list, NULL)) != -1) {
    switch (c) {
      case 'd':
        if (optarg) file_data = optarg;
        break;
      case 'm':
        if (optarg) file_response = optarg;
        break;
      case 'o':
        if (optarg) file_out = optarg;
        break;
      case 'r':
        if (optarg) rapidity = atoi(optarg);
        break;
      case 'n':
        if (optarg) niter = atoi(optarg);
        break;
      case 1001:
        arm = Arm1Params::kArmIndex;
        break;
      case 1002:
        arm = Arm2Params::kArmIndex;
        break;
      case 'v':
        if (optarg) verbose = atoi(optarg);
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
        break;
    }
  }

  if (rapidity < 0) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  if (file_response.Data() == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  if (file_data.Data() == "") {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (verbose >= 2) {
    UT::Printf(UT::kPrintInfo, "nopt: = %d\n", nopt);
    UT::Printf(UT::kPrintInfo, "opt_str: \"%s\"\n", opt_str);
  }

  /*--- Print software version ---*/
  if (verbose >= 1) {
    UT::Printf(UT::kPrintInfo, "\n*** %s v%d.%d ***\n", PROJECT_NAME, PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
  }

  // TRint theApp("makeresponsefun",&argc,argv,0,0,kTRUE);
  gROOT->SetBatch();

  // TString file_response = "./responsefunction_arm2.root";
  // TString file_data = "./hist_arm2.root";
  // Initialization
  UnfoldHistogram* unfold = new UnfoldHistogram();

  // Set Arm
  unfold->SetArm(arm);
  // Set Response Rapidity
  unfold->SetRapidity(rapidity);
  // Set Number of Iterations
  unfold->SetNIterations(niter);
  // Set Response Function
  unfold->SetResponseFunction(file_response);

  Int_t tower = unfold->RapidityToTower(rapidity);

  TFile* file = new TFile(file_data);

  // Set Measured Histograms
  TString reconame = Form("energy_data_PIDcorr[%d]", tower);
  unfold->SetMeasuredHistogram(file_data, reconame.Data());
  // Set True Histograms (if any)
  TString truename = Form("true_energy_%d", tower);
  unfold->SetTrueHistogram(file_data, truename.Data());

  // Do "Unfold"
  unfold->Calculate();
  // Draw the results
  unfold->Draw();

  // Write to file
  unfold->Write(file_out);

  // theApp.Run();

  return 0;
}
