#ifndef MakePhotonResponseArm2_HH
#define MakePhotonResponseArm2_HH

#include <RooUnfoldResponse.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TLegend.h>
#include <TLine.h>
#include <TString.h>

#include "Arm1AnPars.hh"
#include "Arm1CalPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2AnPars.hh"
#include "Arm2CalPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "McParticle.hh"

using namespace std;

namespace nLHCf {

/*--- Unfolding categories ---*/
static const Int_t cNDataType = 4;
static const Int_t cNEventType = 7;

enum DATATYPE { cMCmeasured = 0, cMCtrue = 1, cMissed = 2, cFake = 3 };

/*--- Event categories ---*/
enum EVENTTYPE {
  cEVENT_NOHIT = 0,
  cEVENT_SINGLE_G = 1,
  cEVENT_MULTI_GG = 2,
  cEVENT_MULTI_GH = 3,
  cEVENT_MULTI_HH = 4,
  cEVENT_MULTI_MANY = 5,
  cEVENT_OTHERS = 6
};

template <typename armcal, typename armrec, typename arman, typename armclass>
class MakePhotonResponseArm2 : public armcal, public armrec, public arman, public LHCfParams {
 public:
  MakePhotonResponseArm2();
  ~MakePhotonResponseArm2();

 private:
  /*--- Input/Output ---*/
  TString fInputFile;
  TString fInputDir;
  Int_t fFirstRun;
  Int_t fLastRun;
  TChain *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;

  Int_t fEntry;
  Int_t fNevents;

  Int_t fRapidity;

  Double_t fTrueThreshold;

  /*--- MCEvent and Level3 ---*/
  McEvent *fMC;
  Level3<armrec> *fLvl3;

  /* --- Response Matrix --- */
  vector<RooUnfoldResponse *> fResponse;

  /* --- Histograms --- */
  vector<vector<TH1D *>> fH1;  // For Filled spectrum
  vector<TH2D *> fH2;          // For Response function
  vector<TH1D *> fTrue;        // For True distribution
  vector<TH1D *> fReco;        // For Reco distribution

  // Fake and Miss histograms by event type
  vector<vector<TH1D *>> fFake;  // For Fake spectrum
  vector<vector<TH1D *>> fMiss;  // For Miss spectrum

  // Reco coordinates
  TH2D *fRecoSingleHitmap;         // HitMap of all single-photon events
  TH2D *fRecoSingleHitmapWithCut;  // HitMap of events passed all cuts
  // True coordinates
  TH2D *fTrueSingleHitmap;         // HitMap of all single-photon events
  TH2D *fTrueSingleHitmapWithCut;  // HitMap of events passed all cuts

  vector<TH2D *> fH2EnergyRes;  // Check of Energy Reconstruction

 public:
  void SetInputDir(const Char_t *name) { fInputDir = name; }
  void SetFirstRun(Int_t first) { fFirstRun = first; }
  void SetLastRun(Int_t last) { fLastRun = last; }
  void SetRapidity(Int_t rap) { fRapidity = rap; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }
  void SetInputFile(const Char_t *name) { fInputFile = name; }
  void SetEvents(Int_t ev) { fNevents = ev; }

  Int_t RapidityToTower(Int_t rapidity) {
    Int_t tower = -1;
    if (rapidity >= 0 && rapidity <= 2) {
      tower = 0;
    } else if (rapidity >= 3 && rapidity <= 5) {
      tower = 1;
    }
    return tower;
  }

  void Run();

 private:
  void SetInputTree();
  void MakeHistograms();
  TH1D *CreateHistogram(Int_t tower, Int_t datatype, Int_t eventtype = -1);
  void FillEntry();
  EVENTTYPE SelectionByMCtrue(Int_t tower);
  Bool_t IsPositionWellReconstructed(Int_t tower);
  void EventSinglePhoton(Int_t tower);
  void EventMultiGG(Int_t tower);
  void EventMultiGH(Int_t tower);
  void EventMultiMany(Int_t tower);
  Int_t Fill(Double_t Emeasured, Double_t Etrue);
  Int_t Miss(Double_t Etrue);
  Int_t Fake(Double_t Emeasured);
  void ExtractHistograms();
  void DrawHistograms();
  void DrawResponseFunction();
  void DrawFilledHistograms();
  void DrawHitMap();
  void DrawEnergyReco();
  void ClearEvent();
  void WriteToOutput();
  void CloseFiles();
};
}  // namespace nLHCf
#endif
