project (PhotonUnfolding)
set (MAKRES_NAME MakePhotonResponseArm2)
set (UNFOLD_NAME UnfoldPhotonArm2)

#------------------------#
#--- Set include path ---#
#------------------------#
include_directories (${PROJECT_SOURCE_DIR}/include)
include_directories (${CMAKE_BINARY_DIR}/include)
include_directories (${CMAKE_SOURCE_DIR}/include)
include_directories (${CMAKE_SOURCE_DIR}/RooUnfold/src)
include_directories (${CMAKE_SOURCE_DIR}/${Dict_DIR}/include)

#------------------------#
#--- Set source files ---#
#------------------------#
set (MAKRES_SOURCES ${PROJECT_SOURCE_DIR}/src/makeresparm2.cpp ${PROJECT_SOURCE_DIR}/src/${MAKRES_NAME}.cpp ${CMAKE_SOURCE_DIR}/src/utils.cpp)
set (UNFOLD_SOURCES ${PROJECT_SOURCE_DIR}/src/${UNFOLD_NAME}.cpp ${CMAKE_SOURCE_DIR}/src/utils.cpp)

#---------------------#
#--- Set libraries ---#
#---------------------#
set (ROOUNFOLD_LIBRARIES ${CMAKE_SOURCE_DIR}/RooUnfold/libRooUnfold.so)
set (PROJECT_LINK_LIBS ${Dict_LIB} ${ROOT_LIBRARIES} ${ROOUNFOLD_LIBRARIES})
link_directories (${LIBRARY_OUTPUT_PATH})

#----------------------#
#--- Set executable ---#
#----------------------#
add_executable(${MAKRES_NAME} ${MAKRES_SOURCES})
add_executable(${UNFOLD_NAME} ${UNFOLD_SOURCES})

target_link_libraries (${MAKRES_NAME} ${PROJECT_LINK_LIBS})
target_link_libraries (${UNFOLD_NAME} ${PROJECT_LINK_LIBS})

#---------------#
#--- Install ---#
#---------------#
install (TARGETS ${MAKRES_NAME} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
install (TARGETS ${UNFOLD_NAME} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
