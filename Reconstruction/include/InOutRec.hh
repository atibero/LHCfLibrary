#ifndef InOutRec_HH
#define InOutRec_HH

#include <TFile.h>
#include <TString.h>
#include <TTree.h>

#include "Arm1CalPars.hh"
#include "Arm1Params.hh"
#include "Arm1RecPars.hh"
#include "Arm2CalPars.hh"
#include "Arm2Params.hh"
#include "Arm2RecPars.hh"
#include "LHCfEvent.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "McEvent.hh"

using namespace std;

namespace nLHCf {

class InOutRec {
 public:
  InOutRec(TString in_name, TString out_name);
  ~InOutRec();

 private:
  TString fInputName;
  TFile *fInputFile;
  TTree *fInputTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;
  TTree *fOutputTree;
  LHCfEvent *fOutputEv;

 public:
  Int_t GetEntries();
  void GetEntry(Int_t ie);
  Level0<Arm1Params> *GetArm1Pedestal();
  Level0<Arm1Params> *GetArm1Level0();
  Level1<Arm1Params> *GetArm1Level1();
  Level2<Arm1Params> *GetArm1Level2();
  Level3<Arm1RecPars> *GetArm1Level3();
  Level0<Arm2Params> *GetArm2Pedestal();
  Level0<Arm2Params> *GetArm2Level0();
  Level1<Arm2Params> *GetArm2Level1();
  Level2<Arm2Params> *GetArm2Level2();
  Level3<Arm2RecPars> *GetArm2Level3();
  template <typename armclass>
  McEvent *GetMcEvent();
  void AddObject(TObject *obj);
  void FillEvent();
  void ClearEvent();

  void WriteToOutput();
  void CloseFiles();

 private:
  void Initialize();
  void SetInputTree();
  void BuildOutputTree();

 public:
  ClassDef(InOutRec, 1);
};

}  // namespace nLHCf
#endif
