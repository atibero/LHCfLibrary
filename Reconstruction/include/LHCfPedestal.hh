#ifndef LHCfPedestal_HH
#define LHCfPedestal_HH

#include <TFile.h>
#include <TString.h>
#include <TTree.h>

#include "Arm1CalPars.hh"
#include "Arm1Params.hh"
#include "Arm2CalPars.hh"
#include "Arm2Params.hh"
#include "EventCal.hh"
#include "InOutCal.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "McEvent.hh"

using namespace std;

namespace nLHCf {

class LHCfPedestal : public LHCfParams {
 public:
  LHCfPedestal();
  ~LHCfPedestal();

 private:
  /* MC flag */
  Bool_t fIsMC;

  /*--- Input/Output ---*/
  TString fInputName;
  TString fOutputName;

  /*--- Save pedestal histograms ---*/
  Bool_t fSaveHistogram;
  /*--- Realign silicon events ---*/
  Bool_t fRealignSilicon;
  /*--- Pedestal without delayed gate subtraction ---*/
  Bool_t fAveragePedestal;

  /*--- Level0/1/2 ---*/
  Level0<Arm1Params> fLvl0_Arm1;
  Level0<Arm2Params> fLvl0_Arm2;
  Level0<Arm1Params> fPed0_Arm1;
  Level0<Arm2Params> fPed0_Arm2;

  /*--- Pedestal Histograms ---*/
  vector<vector<vector<vector<TH1F *> > > > fPedCal;
  vector<vector<vector<vector<TH1F *> > > > fPedCalDel;
  vector<vector<vector<vector<TH1F *> > > > fPedCalOffset;
  vector<vector<vector<vector<vector<vector<TH1F *> > > > > > fPedPos;
  vector<vector<vector<TH1F *> > > fPedCalMean;
  vector<vector<vector<TH1F *> > > fPedCalRMS;
  vector<vector<vector<vector<vector<TH1F *> > > > > fPedPosMean;
  vector<vector<vector<vector<vector<TH1F *> > > > > fPedPosRMS;

  /*--- Contaminated Pedestal Histograms ---*/
  vector<vector<vector<vector<TH1F *> > > > fBkgCal;
  vector<vector<vector<vector<TH1F *> > > > fBkgCalDel;
  vector<vector<vector<vector<TH1F *> > > > fBkgCalOffset;
  vector<vector<vector<vector<vector<vector<TH1F *> > > > > > fBkgPos;

  /*--- L2T without Shower Histograms ---*/
  vector<vector<vector<vector<TH1F *> > > > fTrgCal;
  vector<vector<vector<vector<TH1F *> > > > fTrgCalDel;
  vector<vector<vector<vector<TH1F *> > > > fTrgCalOffset;
  vector<vector<vector<vector<vector<vector<TH1F *> > > > > > fTrgPos;
  vector<vector<vector<TH1F *> > > fTrgCalMean;
  vector<vector<vector<TH1F *> > > fTrgCalRMS;
  vector<vector<vector<vector<vector<TH1F *> > > > > fTrgPosMean;
  vector<vector<vector<vector<vector<TH1F *> > > > > fTrgPosRMS;

  /*--- Contaminated L2T without Shower Histograms ---*/
  vector<vector<vector<vector<TH1F *> > > > fGrtCal;
  vector<vector<vector<vector<TH1F *> > > > fGrtCalDel;
  vector<vector<vector<vector<TH1F *> > > > fGrtCalOffset;
  vector<vector<vector<vector<vector<vector<TH1F *> > > > > > fGrtPos;

  vector<vector<Double_t> > cal_ped;
  vector<vector<Double_t> > pos_ped;

  vector<TTree *> caltree;
  vector<TTree *> postree;

  vector<vector<vector<vector<vector<Double_t> > > > > aCalPedVec;
  vector<vector<vector<vector<Double_t> > > > aCalPedAve;
  vector<vector<vector<vector<Double_t> > > > aCalPedRMS;

  vector<vector<vector<vector<vector<Double_t> > > > > aCalNosVec;
  vector<vector<vector<vector<Double_t> > > > aCalNosAve;
  vector<vector<vector<vector<Double_t> > > > aCalNosRMS;

  InOutCal *io_class;

  /*--- Arm & particle enable variables ---*/

  // enable/disable calibration for each Arm
  vector<Bool_t> fArmEnable;

  // Variables used for iterative pedestal cleaning
  vector<Int_t> fNiters;
  vector<Int_t> fNsigma;

 public:
  void SetInputName(const Char_t *name) { fInputName = name; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }

  void DisableArm1() { fArmEnable[Arm1Params::kArmIndex] = false; }
  void DisableArm2() { fArmEnable[Arm2Params::kArmIndex] = false; }

  void SavePedestal() { fSaveHistogram = true; }
  void RealignSilicon() { fRealignSilicon = true; }
  void AveragePedestal() { fAveragePedestal = true; }

  template <typename armclass>
  void InitPedestalTree(Level0<armclass> *lvl0);
  template <typename armclass>
  void FillPedestalTree(Level0<armclass> *lvl0, Level0<armclass> *ped0);
  template <typename armclass>
  void WriteShiftTree(Level0<armclass> *lvl0);

  template <typename armclass>
  void InitMeanRMSHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void InitPedestalHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void InitContaminHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void InitNoShowerHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void InitNimatnocHistogram(Level0<armclass> *lvl0);

  template <typename armclass, typename armcal>
  void FillMeanRMSHistogram(EventCal<armclass, armcal> *evcal);
  template <typename armclass>
  void FillPedestalHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0);
  template <typename armclass>
  void FillContaminHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0);
  template <typename armclass>
  void FillNoShowerHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0);
  template <typename armclass>
  void FillNimatnocHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0);

  template <typename armclass>
  void WriteMeanRMSHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void WritePedestalHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void WriteContaminHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void WriteNoShowerHistogram(Level0<armclass> *lvl0);
  template <typename armclass>
  void WriteNimatnocHistogram(Level0<armclass> *lvl0);

  template <typename armclass>
  void InitVector(Level0<armclass> *lvl0);

  template <typename armclass>
  void FillPedestalVector(Level0<armclass> *lvl0, Level0<armclass> *ped0);
  template <typename armclass>
  void ComputeInitialPedestalMeanAndRMS(Level0<armclass> *lvl0);
  template <typename armclass>
  void CleanPedestalVector(Level0<armclass> *lvl0);
  template <typename armclass>
  Bool_t isGoodPedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0);

  template <typename armclass>
  void FillNoShowerVector(Level0<armclass> *lvl0, Level0<armclass> *ped0);
  template <typename armclass>
  void ComputeInitialNoShowerMeanAndRMS(Level0<armclass> *lvl0);
  template <typename armclass>
  void CleanNoShowerVector(Level0<armclass> *lvl0);
  template <typename armclass>
  Bool_t isGoodNoShower(Level0<armclass> *lvl0, Level0<armclass> *ped0);

  void PedestalRun(Int_t first_ev, Int_t last_ev);
  void PedestalRun(Int_t last_ev) { PedestalRun(0, last_ev); }
  void PedestalRun() { PedestalRun(0, -1); }  // all events

  static const Int_t calbin[2];
  static const Double_t calmin[2];
  static const Double_t calmax[2];
  static const Int_t offbin[2];
  static const Double_t offmin[2];
  static const Double_t offmax[2];
  static const Int_t barbin;
  static const Double_t barmin;
  static const Double_t barmax;
  static const Int_t silbin;
  static const Double_t silmin;
  static const Double_t silmax;
  static const Int_t lisbin;
  static const Double_t lismin;
  static const Double_t lismax;

 private:
  /* Memory allocation */
  void AllocateVectors();

  /* Fill handling Under/OverFlow events */
  void FillSpecial(TH1F *histo, Float_t value);

 public:
  ClassDef(LHCfPedestal, 1);
};

}  // namespace nLHCf
#endif
