#ifndef EventRec_HH
#define EventRec_HH

#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TMarker.h>
#include <TMultiGraph.h>
#include <TPolyMarker.h>

#include "LHCfParams.hh"
#include "Level2.hh"
#include "Level3.hh"
#include "LonFitFCN.hh"
#include "McEvent.hh"
#include "PosFitFCN.hh"
#include "ReadTableCal.hh"
#include "ReadTableRec.hh"
#include "Utils.hh"

using namespace std;

namespace nLHCf {

template <typename armclass, typename armcal, typename armrec>
class EventRec : public armrec, public LHCfParams {
 public:
  EventRec(Bool_t en_pho, Bool_t en_neu, TString pos_rec);
  ~EventRec();

 public:
  /* Debug */
  TCanvas *fDebug_cal_cv[2];
  TCanvas *fDebug_pos_cv[2];
  TCanvas *fDebug_wait;
  TH1D *fDebug_hcal[2];
  TMultiGraph *fDebug_lonfi[2];
  TGraphAsymmErrors *fDebug_calx0[2];
  TGraphAsymmErrors *fDebug_posx0[2][3];
  TGraphErrors *fDebug_graph[2][4][2];
  TGraph *fDebug_tspectrum[2][4][2];
  TPolyMarker *fDebug_pm[2][4][2];
  TMarker *fDebug_mh[2][4][2][3];
  TLine *fDebug_true[2][4][2][3];
  TF1 *fDebug_func[2][4][2];
  TF1 *fDebug_fminuit[2][4][2];
  TF1 *fDebug_lonprotot[2];
  TF1 *fDebug_lonpropar[2][3];
  TGraph *fDebug_lonproint[2][3];
  Bool_t fDebug_isTrueMultihit[2];

 private:
  /*--- Reconstruction variables ---*/

  // enable/disable photon reconstruction
  Bool_t fPhotonEnable;
  // enable/disable neutron reconstruction
  Bool_t fNeutronEnable;

  // enable/disable peak search in photon fast reconstruction (used only for multi-hit)
  Bool_t fFastPeakSearch;

  // compute development variables
  Bool_t fDevelopment;

  // position reconstruction mode ("full" or "fast")
  TString fPosRecMode;
  // channel range for each tower (for Arm2)
  vector<vector<vector<Int_t>>> fChannelRange;
  // silicon sample used (Arm2 only)
  vector<vector<vector<Int_t>>> fSiSample;
  // position of peaks found by TSpectrum
  vector<vector<vector<vector<Double_t>>>> fPeakX;
  // height of peaks found by TSpectrum
  vector<vector<vector<vector<Double_t>>>> fPeakY;
  // number of peaks found by TSpectrum (above thresholds)
  vector<vector<vector<Int_t>>> fPeakN;
  // true if two or more peaks confirmed after fit
  vector<vector<vector<Bool_t>>> fMultiPeak;
  // cluster number, position and height
  vector<vector<vector<vector<vector<Double_t>>>>> fCluster;
  // store chi2 value of single track in single layers
  vector<vector<vector<vector<Double_t>>>> fValCom;
  // position of peaks found by TSpectrum for track association
  vector<vector<vector<vector<Double_t>>>> fPeakTrackX;
  // height of peaks found by TSpectrum  for track association
  vector<vector<vector<vector<Double_t>>>> fPeakTrackY;
  // silicon layer with maximum peak among remaining tracks
  vector<vector<Double_t>> fmaxPeakPos;
  // silicon release with maximum peak among remaining tracks
  vector<vector<Double_t>> fmaxPeakDep;
  // chi2 among all combinations
  vector<vector<Double_t>> fChi2;
  // ndof among all combinations
  vector<vector<Double_t>> fNdof;
  // number of particles among all combinations
  vector<vector<Int_t>> fNpar;
  // number of track cluster in each tower
  vector<Int_t> fNCluster;
  // combination corresponding to track realignment
  vector<Int_t> fCombination;
  // energy deposit associated to each multihit particle
  vector<vector<vector<Double_t>>> fMultihitCalorimeter;

  // Fixed combination map used for multhit track clustering
  vector<vector<vector<vector<Int_t>>>> fMapCombination;

  // leakage-corrected calorimeter
  vector<vector<Double_t>> fPhotonCal;
  vector<vector<Double_t>> fNeutronCal;

  /*--- Parameters from tables ---*/
  ReadTableCal<armcal> fReadTableCal;
  ReadTableRec<armrec> fReadTableRec;

 public:
  /* Event reconstruction */
  void Initialise();
  void EventReconstruction(Level2<armclass> *, Level3<armrec> *);
  void EventRecoTrueCoord(McEvent *mc, Level2<armclass> *, Level3<armrec> *);
  void EventRecoDebug(McEvent *mc, Level2<armclass> *, Level3<armrec> *);

  /* disable peak search in photon fast reconstruction */
  void DisableFastPeakSearch() { fFastPeakSearch = false; }
  /* disable peak search in photon fast reconstruction */
  void Development() { fDevelopment = true; }

  /* Fit functions */
  PosFitBaseFCN *GetFuncPhoton() { return fFuncPhoton; }
  PosFitBaseFCN *GetFuncNeutron() { return fFuncNeutron; }
  void SetPhotonFitType(Int_t type) { fPhotonFitType = type; }
  void SetNeutronFitType(Int_t type) { fNeutronFitType = type; }
  /* Energy Reconstruction type */
  void SetPhotonErecType(Int_t type) { fPhotonErecType = type; }
  void SetNeutronErecType(Int_t type) { fNeutronErecType = type; }

  /* Debug */
  void Print(Int_t ev, Level2<armclass> *lvl2, Level3<armrec> *lvl3);

 private:
  /* Fit functions */
  PosFitBaseFCN *fFuncPhoton;
  PosFitBaseFCN *fFuncNeutron;
  Int_t fPhotonFitType;
  Int_t fNeutronFitType;

  /* MH energy reconstruction type */
  Int_t fPhotonErecType;
  Int_t fNeutronErecType;

  /* Memory allocation */
  void AllocateVectors();
  void ClearVectors();

  /* Map for multihit track clustering */
  void SetTrackCombMap();

  /* Reconstruction parameters */
  void SetParameters();
  void SetChannelRange();
  void SetFitFunction();

  /* HeaderInformation */
  void HeaderInformation(Level2<armclass> *, Level3<armrec> *);

  /* Software trigger */
  void SoftwareTrigger(Level2<armclass> *, Level3<armrec> *);
  void SetDiscriminatorTrigger(Level2<armclass> *, Level3<armrec> *);
  void FirstShowerInteraction(Level2<armclass> *, Level3<armrec> *);

  /* Position reconstruction */
  void PositionReconstruction(Level2<armclass> *, Level3<armrec> *, TString &mode);
  void TrueCoordToRecoCoord(McEvent *mc, Level2<armclass> *, Level3<armrec> *);

  Double_t ChannelToPosition(Int_t it, Int_t iv, Double_t ch);            // without correction
  Double_t ChannelToPosition(Int_t it, Int_t il, Int_t iv, Double_t ch);  // with shift (not tilt) correction
  Double_t PositionToChannel(Int_t it, Int_t iv, Double_t ch);            // without correction
  Double_t PositionToChannel(Int_t it, Int_t il, Int_t iv, Double_t ch);  // with shift (not tilt) correction

  void SetMaxLayer(Level2<armclass> *, Level3<armrec> *);
  void SetSaturationFlag(Level2<armclass> *, Level3<armrec> *);
  void SetSiliconSample(Level3<armrec> *);

  void PhotonSearchPeaks(Level2<armclass> *);
  void PhotonSearchPeaks(Level2<armclass> *, Int_t it, Int_t il, Int_t iv);
  void PhotonFullPositionFit(Level2<armclass> *, Level3<armrec> *);
  void PhotonFastPositionRec(Level2<armclass> *, Level3<armrec> *);
  void PhotonSingleHitFit(Level2<armclass> *, Level3<armrec> *, Int_t it, Int_t il, Int_t iv);
  void PhotonMultiHitFit(Level2<armclass> *, Level3<armrec> *, Int_t it, Int_t il, Int_t iv, Int_t npeak);
  // void PhotonOldFit(Level2<armclass> *, Level3<armrec> *, Int_t it, Int_t il, Int_t iv);

  void NeutronSearchPeaks(Level2<armclass> *);
  void NeutronSearchPeaks(Level2<armclass> *, Int_t it, Int_t il, Int_t iv);
  void NeutronFullPositionFit(Level2<armclass> *, Level3<armrec> *);
  void NeutronFastPositionRec(Level2<armclass> *, Level3<armrec> *);
  void NeutronSingleHitFit(Level2<armclass> *, Level3<armrec> *, Int_t it, Int_t il, Int_t iv);
  void NeutronMultiHitFit(Level2<armclass> *, Level3<armrec> *, Int_t it, Int_t il, Int_t iv, Int_t npeak);

  void TrackClustering();
  void TrackRefinement(Level2<armclass> *);
  void TrackLorentzFit(Level2<armclass> *, Level3<armrec> *);
  void TrackExtraction();
  void TrackCorrection(Level3<armrec> *);
  void TrackSeparation(Level3<armrec> *);
  void TrackGamma2DFit(Level2<armclass> *, Level3<armrec> *);
  void MultihitIdentification(Level2<armclass> *, Level3<armrec> *);
  void LateralShowerWidth(Level2<armclass> *, Level3<armrec> *, bool neupho);

  void GetPhotonSiliconArea(Level2<armclass> *, Level3<armrec> *);
  void GetNeutronSiliconArea(Level2<armclass> *, Level3<armrec> *);
  void GetSiliconDeposit(Level2<armclass> *, Level3<armrec> *);
  void SiliconCharge(Level2<armclass> *, Level3<armrec> *, bool neupho);

  /* Energy reconstruction */
  void EnergyReconstruction(Level2<armclass> *, Level3<armrec> *);
  void EnergyReconstructionMultiHitVersion1(Level2<armclass> *, Level3<armrec> *);
  void EnergyReconstructionMultiHitVersion2(Level2<armclass> *, Level3<armrec> *);

  Double_t GetGSObarAttenuationFactor(Int_t it, Int_t il, Int_t iv, Double_t pos);

  void GetPhotonLevel2(Level2<armclass> *);
  void GetNeutronLevel2(Level2<armclass> *);

  void PhotonLeakEffCorrection(Level3<armrec> *);
  void PhotonLeakInCorrection(Level2<armclass> *, Level3<armrec> *);
  Double_t GetPhotonLeakEffFactor(Int_t it, Int_t il, Double_t x, Double_t y);
  Double_t GetPhotonLeakInFactor(Int_t it, Int_t il, Double_t x, Double_t y, Double_t energy);
  void NeutronLeakCorrection(Level3<armrec> *);
  void NeutronEffCorrection(Level3<armrec> *);

  Double_t GetPhotonSumdE(Int_t it);
  Double_t GetNeutronSumdE(Int_t it);

  Double_t GetPhotonEnergy(Double_t sumde, Int_t it);
  Double_t GetNeutronEnergy(Double_t sumde, Int_t it);

  /* PID reconstruction */
  void PIDReconstruction(Level3<armrec> *);
  void PhotonPIDRec(Level3<armrec> *);
  void NeutronPIDRec(Level3<armrec> *);
  void CalculateL20L90(vector<vector<Double_t>> &cal, vector<Double_t> &l20, vector<Double_t> &l90);
  Double_t GetLayerDepth(Int_t il);
  static Double_t GetL2D(Double_t l20, Double_t l90);
  static Double_t L90Boundary(Int_t tower, Double_t energy, int eff_thr = 90);
  static Double_t L2DBoundary(Int_t tower);

  /* Debug */
  void DrawTrueInformation(McEvent *mc, Int_t it, Int_t il, Int_t iv);
  template <typename fitfunc>
  void DrawPositionFit(McEvent *mc, Level2<armclass> *, Int_t it, Int_t il, Int_t iv, TF1 *func = NULL,
                       fitfunc *hfun = NULL, const Double_t *min_par = NULL);
  void DrawLongitudinalFit(Int_t it, Int_t nhit);
  void DrawCalorimeter(Level2<armclass> *, Int_t it);
  void DrawCalX0(Level2<armclass> *, Int_t it);
  void DrawPosX0(Level3<armrec> *, Int_t it);
  void PrintLevel2(Level2<armclass> *lvl2);
  void PrintMc(McEvent *mc);

 public:
  ClassDef(EventRec, 1);
};

}  // namespace nLHCf
#endif
