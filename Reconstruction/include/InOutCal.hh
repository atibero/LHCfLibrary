#ifndef InOutCal_HH
#define InOutCal_HH

#include <TFile.h>
#include <TString.h>
#include <TTree.h>

#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "McEvent.hh"

using namespace std;

namespace nLHCf {

class InOutCal : public LHCfParams {
 public:
  InOutCal(TString in_name, TString out_name, bool save_event = true, bool use_mini_tree = false);
  ~InOutCal();

 private:
  TString fInputName;
  TFile *fInputFile;
  vector<TTree *> fInputTree;
  vector<TTree *> fInputMiniTree;
  LHCfEvent *fInputEv;

  TString fOutputName;
  TFile *fOutputFile;
  TTree *fOutputTree;
  LHCfEvent *fOutputEv;

  bool fUseMiniTree;
  bool fSaveLHCfEvent;

 public:
  Int_t GetEntries(Int_t iarm);
  Bool_t GetEntry(Int_t iarm, Int_t ie);

  template <typename evcal>
  void SetInputTree(evcal *);
  template <typename evcal>
  Bool_t RealignSilicon(Int_t iarm, Int_t ie, evcal *);
  template <typename armclass>
  Level0<armclass> *GetLevel0();
  template <typename armclass>
  Level0<armclass> *GetPedestal0();
  template <typename armclass>
  McEvent *GetMcEvent();

  void AddObject(TObject *obj);
  void WriteObject(TObject *obj);
  void FillHeader();
  void FillEvent();
  void ClearEvent();

  void WriteToOutput();
  void CloseFiles();

  TFile *GetOutputFile() { return fOutputFile; }

 private:
  void Initialize();
  void BuildOutputTree();

 public:
  ClassDef(InOutCal, 1);
};

}  // namespace nLHCf
#endif
