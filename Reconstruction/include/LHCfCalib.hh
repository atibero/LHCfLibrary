#ifndef LHCfCalib_HH
#define LHCfCalib_HH

#include <TFile.h>
#include <TString.h>
#include <TTree.h>

#include "Arm1Params.hh"
#include "Arm2Params.hh"
#include "LHCfEvent.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "McEvent.hh"
#include "SPSAdamo.hh"

using namespace std;

namespace nLHCf {

class LHCfCalib : public LHCfParams {
 public:
  LHCfCalib();
  ~LHCfCalib();

 private:
  /* MC flag */
  Bool_t fIsMC;

  /*--- Input/Output ---*/
  TString fInputName;
  TString fOutputName;

  Bool_t fSavePedestal;
  Bool_t fSaveLevel0;
  Bool_t fSaveLevel1;
  Bool_t fSaveLevel0Cal;
  Bool_t fSaveLevel1Cal;
  Bool_t fSaveLevel2Cal;

  /*--- Pedestal mean/rms histograms ---*/
  Bool_t fPedHist;

  /*--- Realign silicon events ---*/
  Bool_t fRealignSilicon;

  /*--- Subtract pedestal mean instead of delayed gate ---*/
  Bool_t fAveragePedestal;

  /*--- Use iterative procedure in pedestal computation ---*/
  Bool_t fIteratePedestal;

  /*--- Level0/1/2 ---*/
  Level0<Arm1Params> fLvl0_Arm1;
  Level0<Arm2Params> fLvl0_Arm2;
  Level0<Arm1Params> fPed0_Arm1;
  Level0<Arm2Params> fPed0_Arm2;
  Level0<Arm1Params> fPed0_Arm1_aux;
  Level0<Arm2Params> fPed0_Arm2_aux;
  Level1<Arm1Params> fLvl1_Arm1;
  Level1<Arm2Params> fLvl1_Arm2;
  Level2<Arm1Params> fLvl2_Arm1;
  Level2<Arm2Params> fLvl2_Arm2;
  McEvent *fMcEvent;

  /*---- For SPS  ---*/
  SPSAdamo fAdamo;

  /*--- Arm & particle enable variables ---*/

  // enable/disable calibration for each Arm
  vector<Bool_t> fArmEnable;

 public:
  void SetInputName(const Char_t *name) { fInputName = name; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }

  void DisableArm1() { fArmEnable[Arm1Params::kArmIndex] = false; }
  void DisableArm2() { fArmEnable[Arm2Params::kArmIndex] = false; }

  void SavePedestal() { fSavePedestal = true; }
  void SaveLevel0() { fSaveLevel0 = true; }
  void SaveLevel1() { fSaveLevel1 = true; }
  void SaveLevel0Cal() { fSaveLevel0Cal = true; }
  void SaveLevel1Cal() { fSaveLevel1Cal = true; }
  void SaveLevel2Cal() { fSaveLevel2Cal = true; }

  void DoPedHist() { fPedHist = true; }

  void RealignSilicon() { fRealignSilicon = true; }
  void AveragePedestal() { fAveragePedestal = true; }
  void IteratePedestal() { fIteratePedestal = true; }

  void Calibration(Int_t first_ev, Int_t last_ev);
  void Calibration(Int_t last_ev) { Calibration(0, last_ev); }
  void Calibration() { Calibration(0, -1); }  // all events

 private:
  /* Memory allocation */
  void AllocateVectors();

  /* Copy old data */
  void CopyOldData();
  template <typename level>
  void CopyOldLevel2(level *, vector<double> *, vector<double> *);

 public:
  ClassDef(LHCfCalib, 1);
};

}  // namespace nLHCf
#endif
