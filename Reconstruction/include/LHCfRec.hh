#ifndef LHCfRec_HH
#define LHCfRec_HH

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "Level3.hh"

using namespace std;

namespace nLHCf {

class LHCfRec : public LHCfParams {
 public:
  LHCfRec();
  ~LHCfRec();

 private:
  /*--- Input/Output ---*/
  TString fInputName;
  TString fOutputName;

  Bool_t fSaveLevel0;
  Bool_t fSaveLevel1;
  Bool_t fSaveLevel2;
  Bool_t fSaveLevel0Cal;
  Bool_t fSaveLevel1Cal;
  Bool_t fSaveLevel2Cal;
  Bool_t fReadInputPos;

  Bool_t fUseTrueCoo;
  Bool_t fDevelopment;

  /*--- Level0/1/2/3 ---*/
  Level0<Arm1Params> *fPed0_Arm1;
  Level0<Arm2Params> *fPed0_Arm2;

  Level0<Arm1Params> *fLvl0_Arm1;
  Level0<Arm2Params> *fLvl0_Arm2;

  Level1<Arm1Params> *fLvl1_Arm1;
  Level1<Arm2Params> *fLvl1_Arm2;

  Level2<Arm1Params> *fLvl2_Arm1;
  Level2<Arm2Params> *fLvl2_Arm2;

  Level3<Arm1RecPars> *fLvl3_Input_Arm1;
  Level3<Arm2RecPars> *fLvl3_Input_Arm2;

  Level3<Arm1RecPars> fLvl3_Arm1;
  Level3<Arm2RecPars> fLvl3_Arm2;

  /*--- Arm & particle enable variables ---*/

  // enable/disable reconstruction for each Arm
  vector<Bool_t> fArmEnable;
  // enable/disable photon reconstruction
  Bool_t fPhotonEnable;
  // enable/disable neutron reconstruction
  Bool_t fNeutronEnable;
  // position reconstruction mode ("full" or "fast")
  TString fPosRecMode;
  // position fit function type
  Int_t fPhotonFitType;
  Int_t fNeutronFitType;
  // type of energy reconstruction algorithm for MH events
  Int_t fPhotonErecType;
  // Int_t fNeutronErecType;
  // enable/disable peak search in photon fast reconstruction (used only for multi-hit)
  Bool_t fFastPeakSearch;

  /*--- Debug ---*/
  Bool_t fOldFlag;
  TString fOldName;

 public:
  void SetInputName(const Char_t *name) { fInputName = name; }
  void SetOutputName(const Char_t *name) { fOutputName = name; }

  void DisableArm1() { fArmEnable[Arm1Params::kArmIndex] = false; }
  void DisableArm2() { fArmEnable[Arm2Params::kArmIndex] = false; }

  void DisablePhoton() { fPhotonEnable = false; }
  void DisableNeutron() { fNeutronEnable = false; }

  void DisableFastPeakSearch() { fFastPeakSearch = false; }

  void SaveLevel0() { fSaveLevel0 = true; }
  void SaveLevel1() { fSaveLevel1 = true; }
  void SaveLevel2() { fSaveLevel2 = true; }
  void SaveLevel0Cal() { fSaveLevel0Cal = true; }
  void SaveLevel1Cal() { fSaveLevel1Cal = true; }
  void SaveLevel2Cal() { fSaveLevel2Cal = true; }

  void UseTrueCoo() { fUseTrueCoo = true; }
  void Development() { fDevelopment = true; }

  void SetPosRecMode(const Char_t *mode) {
    fPosRecMode = mode;
    if (fPosRecMode == "none") fReadInputPos = true;
  }
  void SetPhotonFitType(Int_t type) { fPhotonFitType = type; }
  void SetNeutronFitType(Int_t type) { fNeutronFitType = type; }
  void SetPhotonErecType(Int_t type) { fPhotonErecType = type; }
  // void SetNeutronErecType(Int_t type) { fNeutronErecType = type; }

  void Reconstruction(Int_t first_ev, Int_t last_ev);
  void Reconstruction(Int_t last_ev) { Reconstruction(0, last_ev); }
  void Reconstruction() { Reconstruction(0, -1); }  // all events

  /* Debug */
  void SetOldName(const Char_t *name) {
    fOldName = name;
    fOldFlag = true;
  }

 private:
  /* Memory allocation */
  void AllocateVectors();

 public:
  ClassDef(LHCfRec, 1);
};

}  // namespace nLHCf
#endif
