#ifndef LinkDefRec_H
#define LinkDefRec_H

#ifdef __CINT__
//#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ namespace nLHCf;
#pragma link C++ class nLHCf::InOutCal + ;
#pragma link C++ class nLHCf::EventCal < nLHCf::Arm1Params, nLHCf::Arm1CalPars> + ;
#pragma link C++ class nLHCf::EventCal < nLHCf::Arm2Params, nLHCf::Arm2CalPars> + ;
#pragma link C++ class nLHCf::LHCfCalib + ;
#pragma link C++ class nLHCf::LHCfPedestal + ;
#pragma link C++ class nLHCf::LonFitBaseFCN + ;
#pragma link C++ class nLHCf::LonFitSingleHitFCN + ;
#pragma link C++ class nLHCf::LonFitMultiHitFCN + ;
#pragma link C++ class nLHCf::InOutRec + ;
#pragma link C++ class nLHCf::EventRec < nLHCf::Arm1Params, nLHCf::Arm1CalPars, nLHCf::Arm1RecPars> + ;
#pragma link C++ class nLHCf::EventRec < nLHCf::Arm2Params, nLHCf::Arm2CalPars, nLHCf::Arm2RecPars> + ;
#pragma link C++ class nLHCf::LHCfRec + ;
#pragma link C++ class nLHCf::EventReduction < nLHCf::Arm1Params, nLHCf::Arm1RecPars, nLHCf::Arm1AnPars> + ;
#pragma link C++ class nLHCf::EventReduction < nLHCf::Arm2Params, nLHCf::Arm2RecPars, nLHCf::Arm2AnPars> + ;
#pragma link C++ class nLHCf::LHCfReduction + ;

#pragma link C++ class nLHCf::AdamoPacket + ;
#pragma link C++ class nLHCf::AdamoDecode + ;
#pragma link C++ class nLHCf::SPSAdamoPedeSubtract + ;

#endif

#endif
