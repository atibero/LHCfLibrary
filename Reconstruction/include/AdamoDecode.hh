#ifndef __ADAMODECODE_H__
#define __ADAMODECODE_H__

#include <SPSAdamo.hh>

namespace nLHCf {
class AdamoPacket {
 public:
  static const int NBYTE = 7694;
  static const int NHEADER = 6;
  static const int NBYTE_EC = 3;
  static const int NBYTE_DATA = 7680;
  static const int NLAYER = 5;
  static const int NSTRIP = 1024;
  static const int NBYTE_CRC = 1;
  static const int NTRAILER = 4;

  unsigned char header[NHEADER];       // 6 bytes
  unsigned int eventcounter;           // 3 bytes
  unsigned int strip[NLAYER][NSTRIP];  // 12bits for each (total 7680 bytes)
  unsigned int CRC;                    // 1 bytes
  unsigned char trailer[NTRAILER];     // 4 bytes

 public:
  void Clear();
  int CheckHeader(int ixy);   // if OK, return 1. if not, return 0
  int CheckTrailer(int ixy);  // if OK, return 1. if not, return 0
  int CheckCRC(char *data);   // if OK, return 1. if not, return 0
  unsigned char crc8_8(unsigned char old, unsigned char data);
  // CRC Calculation

 public:
  ClassDef(AdamoPacket, 1);
};

class AdamoDecode {
 public:
  static const int NBYTE = AdamoPacket::NBYTE * 2;
  static const int NXY = 2;
  static const int BYTE0 = 1;
  static const int BYTE1 = 256;
  static const int BYTE2 = 65536;
  static const int NSR = 3;

  unsigned int SR[3];       // Status Register (3 words)
  AdamoPacket packet[NXY];  // 0 : X, 1 : Y
  bool valid;               // valid event

 public:
  AdamoDecode();
  ~AdamoDecode();

  int Clear();
  int Decode(char *data);
  int DecodePacket(char *data, int ixy);
  int SetSR(char *data);
  int FillToSPSAdamo(nLHCf::SPSAdamo *d);

 public:
  ClassDef(AdamoDecode, 1);
};
}  // namespace nLHCf

#endif
