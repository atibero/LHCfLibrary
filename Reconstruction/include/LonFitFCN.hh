#ifndef LonFitFCN_HH
#define LonFitFCN_HH

#include <Math/PdfFuncMathCore.h>
#include <Rtypes.h>
#include <TMath.h>

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

namespace nLHCf {

class LonFitBaseFCN {
 public:
  LonFitBaseFCN(Int_t npars) : fNpoint(0), fNparam(npars) {}
  ~LonFitBaseFCN() {}

  Double_t operator()(const Double_t *par) { return DoEval(par); }

  /* to be implemented by derived classes */
  virtual Double_t FitFunc(const Double_t *x, const Double_t *par) { return 0.; }
  virtual Double_t DoEval(const Double_t *par) { return 0.; }

  Int_t GetNpoint() const { return fNpoint; }
  Int_t GetNparam() const { return fNparam; }

 protected:
  vector<Double_t> fXCal;
  vector<Double_t> fYCal;
  vector<Double_t> fLCal;
  vector<Double_t> fHCal;

  vector<vector<Double_t>> fXSil;
  vector<vector<Double_t>> fYSil;
  vector<vector<Double_t>> fLSil;
  vector<vector<Double_t>> fHSil;

  Int_t fNpoint;        // number of points used in the fit (i.e., with error > 0)
  const Int_t fNparam;  // number of parameters

 public:
  ClassDef(LonFitBaseFCN, 1);
};

class LonFitSingleHitFCN : public LonFitBaseFCN {
 public:
  LonFitSingleHitFCN(Int_t npars) : LonFitBaseFCN(npars) {}
  ~LonFitSingleHitFCN() {}

  /* Gamma Distribution longitudinal development fit (single-hit) */
  Double_t FitFunc(const Double_t *x, const Double_t *par) {
    const Double_t t = x[0];
    const Double_t alpha = par[0];
    const Double_t theta = par[1];
    const Double_t start = par[2];
    return ROOT::Math::gamma_pdf(t, alpha, theta, start);
  }

  /* Compute chi2 */
  Double_t DoEval(const Double_t *par) {
    Double_t chi2 = 0.;

    for (UInt_t ip = 0; ip < fXCal.size(); ++ip) {
      Double_t pos = fXCal[ip];
      Double_t meas = fYCal[ip];
      Double_t err = 0.5 * (fLCal[ip] + fHCal[ip]);
      Double_t fit = FitFunc(&pos, par);

      if (err > 0.) chi2 += TMath::Power((meas - fit) / err, 2.);
    }

    return chi2;
  }

  void SetDataVectors(vector<Double_t> &xpcal, vector<Double_t> &ypcal, vector<Double_t> &elcal,
                      vector<Double_t> &ehcal) {
    if (fXCal.size() != fYCal.size() || fYCal.size() != fLCal.size() || fLCal.size() != fHCal.size()) {
      cerr << "LonFitBaseFCN::SetDataVectors: size mismatch in input vectors!" << endl;
      exit(EXIT_FAILURE);
    }
    ClearDataVectors();
    fXCal = xpcal;
    fYCal = ypcal;
    fLCal = elcal;
    fHCal = ehcal;

    Int_t npoint = 0;
    for (UInt_t ip = 0; ip < fXCal.size(); ++ip)
      if (0.5 * (fLCal[ip] + fHCal[ip])) ++npoint;
    fNpoint = npoint;
  }

  void ClearDataVectors() {
    fXCal.clear();
    fYCal.clear();
    fLCal.clear();
    fHCal.clear();
  }

 public:
  ClassDef(LonFitSingleHitFCN, 1);
};

class LonFitMultiHitFCN : public LonFitBaseFCN {
 public:
  LonFitMultiHitFCN(Int_t npars, Int_t nhit = 2) : LonFitBaseFCN(npars), fNhit(nhit) {
    if (!(fNparam % fNhit == 0)) {
      cerr << "LonFitMultiHitFCN::LonFitMultiHitFCN: fNparam is not a multiple of fNhit!" << endl;
      exit(EXIT_FAILURE);
    }
    fNparamSingle = fNparam / fNhit;
  }

  ~LonFitMultiHitFCN() {}

  /* Gamma Distribution longitudinal development fit in GSO (multi-hit) */
  Double_t FitFuncConv(const Double_t *x, const Double_t *par) {
    Double_t fval = 0.;
    for (Int_t ih = 0; ih < fNhit; ++ih) {
      const Double_t t = x[0];
      const Int_t ipar = ih * fNparamSingle;
      const Double_t sumde = par[0 + ipar];
      const Double_t alpha = par[1 + ipar];
      const Double_t theta = par[2 + ipar];
      const Double_t start = par[3 + ipar];
      fval += sumde * ROOT::Math::gamma_pdf(t, alpha, theta, start);
    }
    return fval;
  }

  /* Gamma Distribution longitudinal development fit in silicon (single-hit) */
  Double_t FitFuncPart(const Double_t *x, const Double_t *par, const Int_t hit) {
    Double_t fval = 0.;

    const Double_t t = x[0];
    const Int_t ipar = hit * fNparamSingle;
    const Double_t sumde = par[0 + ipar];
    const Double_t alpha = par[1 + ipar];
    const Double_t theta = par[2 + ipar];
    const Double_t start = par[3 + ipar];
    fval = sumde * ROOT::Math::gamma_pdf(t, alpha, theta, start);

    return fval;
  }

  /* Compute chi2 */
  Double_t DoEval(const Double_t *par) {
    Double_t chi2 = 0.;
    // Calorimeter
    for (UInt_t ip = 0; ip < fXCal.size(); ++ip) {
      Double_t pos = fXCal[ip];
      Double_t meas = fYCal[ip];
      Double_t err = 0.5 * (fLCal[ip] + fHCal[ip]);
      Double_t fit = FitFuncConv(&pos, par);
      if (err > 0.) chi2 += TMath::Power((meas - fit) / err, 2.);
    }
    // Silicon
    for (Int_t ih = 0; ih < fNhit; ++ih)
      for (UInt_t ip = 0; ip < fXSil[ih].size(); ++ip) {
        Double_t pos = fXSil[ih][ip];
        Double_t meas = fYSil[ih][ip];
        Double_t err = 0.5 * (fLSil[ih][ip] + fHSil[ih][ip]);
        Double_t fit = FitFuncPart(&pos, par, ih);
        if (err > 0.) chi2 += TMath::Power((meas - fit) / err, 2.);
      }
    return chi2;
  }

  void SetDataVectors(vector<Double_t> &xpcal, vector<Double_t> &ypcal, vector<Double_t> &elcal,
                      vector<Double_t> &ehcal, vector<vector<Double_t>> &xpsil, vector<vector<Double_t>> &ypsil,
                      vector<vector<Double_t>> &elsil, vector<vector<Double_t>> &ehsil) {
    if (fXCal.size() != fYCal.size() || fYCal.size() != fLCal.size() || fLCal.size() != fHCal.size()) {
      cerr << "LonFitBaseFCN::SetDataVectors: size mismatch in calorimeter input vectors!" << endl;
      exit(EXIT_FAILURE);
    }
    if (fXSil.size() != fYSil.size() || fYSil.size() != fLSil.size() || fLSil.size() != fHSil.size()) {
      cerr << "LonFitBaseFCN::SetDataVectors: size mismatch in silicon input vectors!" << endl;
      exit(EXIT_FAILURE);
    }
    ClearDataVectors();

    fXCal = xpcal;
    fYCal = ypcal;
    fLCal = elcal;
    fHCal = ehcal;

    fXSil = xpsil;
    fYSil = ypsil;
    fLSil = elsil;
    fHSil = ehsil;

    Int_t npoint = 0;
    // Calorimeter
    for (UInt_t ip = 0; ip < fXCal.size(); ++ip)
      if (0.5 * (fLCal[ip] + fHCal[ip])) ++npoint;
    // Silicon
    for (Int_t ih = 0; ih < fNhit; ++ih)
      for (UInt_t ip = 0; ip < fXSil[ih].size(); ++ip)
        if (0.5 * (fLSil[ih][ip] + fHSil[ih][ip])) ++npoint;
    fNpoint = npoint;
  }

  void ClearDataVectors() {
    fXCal.clear();
    fYCal.clear();
    fLCal.clear();
    fHCal.clear();

    fXSil.clear();
    fYSil.clear();
    fLSil.clear();
    fHSil.clear();
  }

  Int_t GetNhit() const { return fNhit; }
  Int_t GetNparamSingle() const { return fNparamSingle; }

 private:
  Int_t fNhit;
  Int_t fNparamSingle;

 public:
  ClassDef(LonFitMultiHitFCN, 1);
};

}  // namespace nLHCf
#endif
