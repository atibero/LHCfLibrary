#ifndef EventCal_HH
#define EventCal_HH

#include <TH1D.h>

#include "AdamoDecode.hh"
#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "InOutCal.hh"
#include "LHCfParams.hh"
#include "Level0.hh"
#include "Level1.hh"
#include "Level2.hh"
#include "ReadTableCal.hh"
#include "SPSAdamo.hh"
#include "SPSAdamoPedeSubtract.hh"
#include "SiliconDecode.hh"

/*--- Silicon related constants ---*/
#define CRC8_INIT_VALUE 0x00
#define CRC8_XOR_VALUE 0x00

#define DAQMAGIC1 (0x73)  // 1st magic word at start of readout board trasm
#define DAQMAGIC2 (0x21)  // 2nd magic word at start of readout board trasm

#define NSAMPL (3)                                                         // # of samplings (= # of read PACE cols)
#define NBOARDS 4                                                          // # of readout boards
#define NLAYXBOARD 2                                                       // # of detector layers read by 1 board
#define NCHIPXLAY 12                                                       // # of front-end chips per detector layer
#define NCHIPXBOARD (NLAYXBOARD * NCHIPXLAY)                               // # of PACEs per readout board
#define NCHXCHIP 32                                                        // # of channels per front-end chip
#define NREADXCH 3                                                         // # of reading per channels
#define NCHANTOT (NBOARDS * NLAYXBOARD * NCHIPXLAY * NREADXCH * NCHXCHIP)  // Total # of readout channels

#define NCHXLAY (NCHIPXLAY * NCHXCHIP)  // # of channels per detector layer
#define NFIFOXBOARD 16                  // # of FIFO
#define NBLOCKSXFIFO 12                 // # of transmission blocks per each FIFO
#define NWORDSXFIFO (NCHXCHIP * NREADXCH)
#define NWORDSXBLOCK (NWORDSXFIFO / NBLOCKSXFIFO)

#define NBYTESXBLOCK 18                                           // Data size (1 block)
#define NBYTESXFIFO (NBLOCKSXFIFO * NBYTESXBLOCK)                 // Data size (1 FIFO)
#define NBYTESXBOARD (NFIFOXBOARD * NBLOCKSXFIFO * NBYTESXBLOCK)  // Data size (1 board)
#define NBYTESXEV (NBOARDS * NBYTESXBOARD)                        // Data size (1 ev)

#define TRKBYTESXBOARD (sizeof(struct DATA_Header) + NBYTESXBOARD + sizeof(struct DATA_Trailer))
#define TRKBYTESXEV (NBOARDS * TRKBYTESXBOARD)  // Tot DAQ data size (1 ev)
#define NPLANES (NBOARDS)                       // Number of silicon modules (each module has x and y sensors)
#define NVIEWXPLANE (NLAYXBOARD)                // Number of view per module (2 views: x and y)
#define NSTRIPXVIEW (NCHIPXLAY * NCHXCHIP)
/*
// Number of readout strips (sensor has 768 implantations, but
// we readout 1 out of 2 --> 384) (NOT REALLY: first 4 and last
// 4 implanted are not readout, that means 380 readout strips;
// good readout channels are from 3 to 382 included)
*/
#define NBOARDSMAX 4
#define NBOARDSINFILE 4  // # of readout boards stored in datafile
#define NPLANESMAX (NBOARDSMAX)
#define NSEC 4  // Number of sections on each MDAQ board (num of readout half-hybrids)

using namespace std;

namespace nLHCf {

template <typename armclass, typename armcal>
class EventCal : public armcal, public LHCfParams {
 public:
  EventCal();
  ~EventCal();

  /* MC flag */
  Bool_t fIsMC;

  /* Pedestal subtraction from average instead from delayed fate */
  Bool_t fAveragePedestal;

  /* Event calibration */
  void Initialise();
  void InitialiseWithoutTables();
  void CreatePedestalVector(Level0<armclass> *);
  void FillPedestalVector(Level0<armclass> *, Level0<armclass> *);
  void GetPedestalVectorMeanAndRMS(Level0<armclass> *);
  void CleanPedestalVector(Level0<armclass> *);
  void DeletePedestalVector();
  void DeletePedestalVariables();
  Bool_t isGoodPedestal(Level0<armclass> *, Level0<armclass> *);
  void GetPedestalMeanRms(Level0<armclass> *);
  Int_t GivePedestal(Level0<armclass> *, Level0<armclass> *, InOutCal *io_class = NULL);
  Int_t SavePedestal(Level0<armclass> *, Level0<armclass> *);
  Int_t ReadPedestal(Level0<armclass> *, Level0<armclass> *, InOutCal *io_class = NULL);
  Int_t EventCalibration(Level0<armclass> *lvl0, Level0<armclass> *ped0, Level1<armclass> *lvl1, Level2<armclass> *lvl2,
                         InOutCal *io_class, Bool_t keep_all);
  Int_t EventCalibration(Level0<armclass> *lvl0, Level0<armclass> *ped0, Level1<armclass> *lvl1, Level2<armclass> *lvl2,
                         InOutCal *io_class) {
    return EventCalibration(lvl0, ped0, lvl1, lvl2, io_class, false);
  }
  Int_t EventCalibration(Level0<armclass> *lvl0, Level0<armclass> *ped0, Level1<armclass> *lvl1, Level2<armclass> *lvl2,
                         Bool_t keep_all) {
    return EventCalibration(lvl0, ped0, lvl1, lvl2, NULL, keep_all);
  }
  Int_t EventCalibration(Level0<armclass> *lvl0, Level0<armclass> *ped0, Level1<armclass> *lvl1,
                         Level2<armclass> *lvl2) {
    return EventCalibration(lvl0, ped0, lvl1, lvl2, NULL, false);
  }

  /* Wrapper of private to public function */
  void EventToLevel0(Level0<armclass> *lvl0, Level0<armclass> *ped0, InOutCal *io_class = NULL) {
    GivePedestal(lvl0, ped0, io_class);
  }

  /* Event number necessary to check silicon alignment in new electronics */
  void SetSiliconEvent(UInt_t silicon_event = 0) { fSiliconEvent = silicon_event; }

  /* Get Methods */
  Double_t GetPedCalMean(int tower, int layer, int range) { return fCalPedMean[tower][layer][range]; }
  Double_t GetPedCalRms(int tower, int layer, int range) { return fCalPedRms[tower][layer][range]; }
  Double_t GetPedPosMean(int tower, int layer, int view, int channel, int sample) {
    return fPosPedMean[tower][layer][view][channel][sample];
  }
  Double_t GetPedPosRms(int tower, int layer, int view, int channel, int sample) {
    return fPosPedRms[tower][layer][view][channel][sample];
  }

  void AveragePedestal() { fAveragePedestal = true; }

  /* For SPS */
  Bool_t fAdamoPresence;

  Int_t EventAdamo(SPSAdamo *, InOutCal *io_class = NULL);
  void InitialiseAdamo();
  void ReadPedestalAdamo(SPSAdamo *, InOutCal *io_class = NULL);
  void CalculatePedestalAdamo();

  /* Pedestal histograms */
  void MakeHistograms();
  void FillHistograms();

 private:
  /* Validity flag */
  Bool_t fValidEvent;

  /* Silicon Event Number */
  UInt_t fSiliconEvent;

  /* Pedestal parameters */
  Int_t fPedCnt;
  vector<vector<vector<Double_t> > > fCalPedShift;
  vector<vector<vector<Double_t> > > fCalPedMean;
  vector<vector<vector<Double_t> > > fCalPedRms;
  vector<vector<vector<Double_t> > > aTmpPedAve;
  vector<vector<vector<Double_t> > > aTmpPedRMS;
  vector<vector<vector<vector<Double_t> > > > aTmpPedVec;
  vector<vector<vector<vector<vector<Double_t> > > > > fPosPedMean;
  vector<vector<vector<vector<vector<Double_t> > > > > fPosPedRms;
  vector<vector<vector<vector<Double_t> > > > fCalPedPars;
  vector<vector<vector<vector<vector<vector<Double_t> > > > > > fPosPedPars;

  /* Calibration data from tables */
  ReadTableCal<armcal> fReadTableCal;

  /*--- Pedestal mean/rms histograms ---*/
  vector<vector<TH1D *> > fHCalMean;
  vector<vector<TH1D *> > fHCalRMS;
  vector<vector<vector<vector<TH1D *> > > > fHPosDetMean;
  vector<vector<vector<vector<TH1D *> > > > fHPosDetRMS;

  // Variables used for iterative pedestal cleaning
  Int_t fNiters;
  Int_t fNsigma;

  /* Memory allocation */
  void AllocateVectors();
  void ClearVectors();

  /* Set calibration parameters */
  void SetParameters();
  void SetParametersWithoutTables();

  /* Set silicon hybrid sequence */
  void SetSiliconHybSeq();
  void SetSiliconHybSeqUpgrade();

  /* Convert raw to Level0 */
  void ConvertToLevel0(Level0<armclass> *, Level0<armclass> *);
  unsigned int CounterFilter(unsigned int val, int mask, int offset = 0);
  unsigned int CounterCombine(unsigned int *val);
  void GetCalorimeterPedestal(Level0<armclass> *ped0, Level0<armclass> *lvl0);
  void GetPosDetPedestal(Level0<armclass> *ped0, Level0<armclass> *lvl0);
  void GaussKalmanFilter(Double_t *observe, Double_t *estimate);

  void CheckSaturation(Level0<armclass> *);

  void ConvertToLevel1(Level1<armclass> *, Level0<armclass> *, Level0<armclass> *);
  template <typename level_d, typename level_s>
  void CopyHeader(level_d *, level_s *);
  void PedestalSubtraction(Level1<armclass> *, Level0<armclass> *, Level0<armclass> *);
  void PedestalCorrection(Level1<armclass> *);
  void PedestalShiftCorrection(Level1<armclass> *);
  void PedestalDelayedCorrection(Level1<armclass> *);
  void TdcOffsetSubtraction(Level1<armclass> *, Level0<armclass> *);
  void SilCorrCorrection(Level1<armclass> *);

  void ConvertToLevel2(Level2<armclass> *, Level1<armclass> *);
  void CopyData(Level2<armclass> *, Level1<armclass> *);
  void GainCalibration(Level2<armclass> *);
  void CrossTalkCorrection(Level2<armclass> *);
  void SetErrors(Level2<armclass> *);
  void SetOldErrors(Level2<armclass> *);  // debug

  /* Silicon related members */
  char HybSeqMDAQ[NBOARDS][NSEC][4];
  int id_board_[NBOARDS];

  char HybSeqMDAQUpgrade[NMDAQSECTIONS][4];

  // for check of error
  int err[NBOARDS];
  int errold[NBOARDS];
  char crceval[NBOARDS];

  struct DATA_Header {
    char Magics[2];                   /* 2 magic numbers for the event, 1byte+1byte */
    char ColAdd[NSAMPL][NCHIPXBOARD]; /* Column address for 3 samplings of 24 PACE */
    char JUMP_FF;   /* 4 bits to check jumper ID of board 4 bits to check FULL FIFO flag in fifos of 4 half-hybrids */
    char ColAddChk; /* 8bit variable for checking of column address: (MSB) C12 - C23 - 3x - C1 -C2 - C3 (LSB)   (x=bit
                       not used) */
    char DV_Al[NSAMPL][3]; /* Data valid alarm for 3 columns, 24 PACE (3x3 bits). // For each col 3 bytes contains info
                              for PACEs: (MSB) 1,...,24 (LSB) */
    char Alarms;           /* (MSB) Master(2bit) - PLL - AE - FNE(4bits -> 4 half-hybrids) (LSB) */
    char Counters[4]; /* ((Counters >> 28) & 0xF)  ->  rejected LV1 events, (Counters & 0xFFFFFFF)    ->  read event
                         counter */
    char End[6];      /* 6 magic numbers (bytes) for the event */
  };

  struct DATA_Trailer {
    char CRC;    /* Check for consistency of data transmission */
    char End[7]; /* 7 fixed words (bytes) to end data transmission */
  };

  struct DAQ_Board_Out {
    struct DATA_Header head;
    char data[NBYTESXBOARD];
    struct DATA_Trailer trail;
  };

  struct VME_Header {
    int dummy; /* magic number for the VME header */
    int time_sec;
    int time_usec;
    // long int dummy;               //To be changed for actual LHC2015 operation: Scandalo!
    // long int time_sec;
    // long int time_usec;
    int event_no;    /* event number from start */
    int acq_flag;    /* packet integrity flag */
    int acq_type;    /* acquisition type */
    int acq_nboards; /* number of acquisition sets */
    int acq_size;    /* byte sizes of acquired sets */
  };

  struct VME_Trailer {
    int dummy[2]; /* magic number for the VME trailer  */
  };

  struct SILICON_RAW {
    struct VME_Header acq_header;
    struct DAQ_Board_Out boardout[NBOARDS];
    struct VME_Trailer acq_trailer;
  };

  SILICON_RAW data;

  void ExtractSilicon(Level0<armclass> *);
  void ExtractSiliconUpgrade(Level0<armclass> *);

  int CheckSiliconIDBoard();

  void ExtractSiliconData(Level0<armclass> *);
  void DecodeSiliconHybSeq(int ibo, int isec, int *iplane, int *iview, int *ihyb);
  void DecodeSiliconHybSeqUpgrade(int ibo, int *iplane, int *iview);
  int CheckStatusRegister();
  int CheckStatusRegisterUpgrade(RawPackB packet[NMDAQSECTIONS]);
  int CheckEventNumberUpgrade(RawPackA packetA[NMDAQSECTIONS], RawPackB packetB[NMDAQSECTIONS]);

  // Functions for CRC check
  void CRC8_InitChecksum(unsigned char *crcvalue);
  void CRC8_UpdateChecksum(unsigned char *crcvalue, const void *data, int length);
  void CRC8_FinishChecksum(unsigned char *crcvalue);
  unsigned char CRC8_BlockChecksum(const void *data, int length);

  // For Adamo
  AdamoDecode fAdamodecoder;
  SPSAdamoPedeSubtract fAdamoPedeSub;

  void ConvertToAdamo(SPSAdamo *);

 public:
  ClassDef(EventCal, 1);
};

}  // namespace nLHCf
#endif
