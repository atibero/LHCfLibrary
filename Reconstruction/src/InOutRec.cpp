#include "InOutRec.hh"

#include <TROOT.h>

#include "Arm1RecPars.hh"
#include "Arm2RecPars.hh"
#include "Level3.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(InOutRec);
#endif

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
InOutRec::InOutRec(TString in_name, TString out_name) : fInputName(in_name), fOutputName(out_name) { Initialize(); }

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
InOutRec::~InOutRec() {
  delete fInputFile;
  delete fInputEv;
  delete fOutputFile;
  delete fOutputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
void InOutRec::Initialize() {
  SetInputTree();
  BuildOutputTree();
}

void InOutRec::SetInputTree() {
  Utils::Printf(Utils::kPrintInfo, "Setting input tree...");
  fflush(stdout);

  fInputFile = new TFile(fInputName.Data(), "READ");
  if (!fInputFile->IsOpen()) {
    Utils::Printf(Utils::kPrintError, "Error: input file \"%s\" not opened\n", fInputName.Data());
    exit(EXIT_FAILURE);
  }
  fInputFile->GetObject("LHCfEvents", fInputTree);
  fInputEv = new LHCfEvent("event", "LHCfEvent");
  fInputTree->SetBranchAddress("ev.", &fInputEv);
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

void InOutRec::BuildOutputTree() {
  Utils::Printf(Utils::kPrintInfo, "Building output tree...");
  fflush(stdout);

#if defined(NO_RCOMPRESSION)
  fOutputFile = new TFile(fOutputName.Data(), "RECREATE");
#else
  fOutputFile =
      new TFile(fOutputName.Data(), "RECREATE", "",
                ROOT::RCompressionSetting::EAlgorithm::kLZ4 * 100 + ROOT::RCompressionSetting::ELevel::kUncompressed);
#endif
  if (!fOutputFile->IsOpen()) {
    Utils::Printf(Utils::kPrintError, "Error: output file \"%s\" not opened\n", fOutputName.Data());
    exit(EXIT_FAILURE);
  }
  fOutputTree = new TTree("LHCfEvents", "LHCf events (reconstructed)");
  fOutputEv = new LHCfEvent("event", "LHCfEvent");
  fOutputTree->Branch("ev.", "nLHCf::LHCfEvent", &fOutputEv);
  fOutputTree->SetMaxTreeSize(1000 * Long64_t(1000000000));
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

void InOutRec::WriteToOutput() {
  Utils::Printf(Utils::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  fOutputTree->Write("", TObject::kOverwrite);
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

void InOutRec::CloseFiles() {
  Utils::Printf(Utils::kPrintInfo, "Closing files...");
  fflush(stdout);

  fOutputFile->Close();
  fInputFile->Close();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

/*------------------*/
/*--- Interfaces ---*/
/*------------------*/
Int_t InOutRec::GetEntries() { return fInputTree->GetEntries(); }

void InOutRec::GetEntry(Int_t ie) { fInputTree->GetEntry(ie); }

Level0<Arm1Params> *InOutRec::GetArm1Pedestal() { return (Level0<Arm1Params> *)fInputEv->Get("ped0_a1"); }

Level0<Arm1Params> *InOutRec::GetArm1Level0() { return (Level0<Arm1Params> *)fInputEv->Get("lvl0_a1"); }

Level1<Arm1Params> *InOutRec::GetArm1Level1() { return (Level1<Arm1Params> *)fInputEv->Get("lvl1_a1"); }

Level2<Arm1Params> *InOutRec::GetArm1Level2() { return (Level2<Arm1Params> *)fInputEv->Get("lvl2_a1"); }

Level3<Arm1RecPars> *InOutRec::GetArm1Level3() { return (Level3<Arm1RecPars> *)fInputEv->Get("lvl3_a1"); }

Level0<Arm2Params> *InOutRec::GetArm2Pedestal() { return (Level0<Arm2Params> *)fInputEv->Get("ped0_a2"); }

Level0<Arm2Params> *InOutRec::GetArm2Level0() { return (Level0<Arm2Params> *)fInputEv->Get("lvl0_a2"); }

Level1<Arm2Params> *InOutRec::GetArm2Level1() { return (Level1<Arm2Params> *)fInputEv->Get("lvl1_a2"); }

Level2<Arm2Params> *InOutRec::GetArm2Level2() { return (Level2<Arm2Params> *)fInputEv->Get("lvl2_a2"); }

Level3<Arm2RecPars> *InOutRec::GetArm2Level3() { return (Level3<Arm2RecPars> *)fInputEv->Get("lvl3_a2"); }

template <typename armclass>
McEvent *InOutRec::GetMcEvent() {
  return (McEvent *)fInputEv->Get(Form("true_a%d", armclass::kArmIndex + 1));
}

void InOutRec::AddObject(TObject *obj) { fOutputEv->Add(obj); }

void InOutRec::FillEvent() {
  fOutputEv->HeaderCopy(fInputEv);
  fOutputTree->Fill();
}

void InOutRec::ClearEvent() {
  fInputEv->HeaderClear();
  fInputEv->ObjDelete();
  fOutputEv->HeaderClear();
  fOutputEv->ObjClear();
  // fOutputEv->ObjDelete();
}

namespace nLHCf {
template McEvent *InOutRec::GetMcEvent<Arm1Params>();
template McEvent *InOutRec::GetMcEvent<Arm2Params>();
}  // namespace nLHCf
