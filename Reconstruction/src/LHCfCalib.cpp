#include "LHCfCalib.hh"

#include <TROOT.h>

#include <iostream>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "EventCal.hh"
#include "InOutCal.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(LHCfCalib);
#endif

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
LHCfCalib::LHCfCalib()
    : fPed0_Arm1_aux("ped0_a1_aux", "Pedestal (Arm1)"),
      fPed0_Arm2_aux("ped0_a2_aux", "Pedestal (Arm2)"),
      fPed0_Arm1("ped0_a1", "Pedestal (Arm1)"),
      fPed0_Arm2("ped0_a2", "Pedestal (Arm2)"),
      fLvl0_Arm1("lvl0_a1", "Level0 (Arm1)"),
      fLvl0_Arm2("lvl0_a2", "Level0 (Arm2)"),
      fLvl1_Arm1("lvl1_a1", "Level1 (Arm1)"),
      fLvl1_Arm2("lvl1_a2", "Level1 (Arm2)"),
      fLvl2_Arm1("lvl2_a1", "Level2 (Arm1)"),
      fLvl2_Arm2("lvl2_a2", "Level2 (Arm2)"),
      fAdamo((char *)"adamo", (char *)"ADAMO"),
      fInputName("raw.root"),
      fOutputName("calibrated.root"),
      fSavePedestal(false),
      fSaveLevel0(false),
      fSaveLevel1(false),
      fSaveLevel0Cal(false),
      fSaveLevel1Cal(false),
      fSaveLevel2Cal(false),
      fPedHist(false),
      fRealignSilicon(false),
      fAveragePedestal(false),
      fIteratePedestal(false) {
  AllocateVectors();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
LHCfCalib::~LHCfCalib() {}

/*------------------------------*/
/*--- Allocate/clear vectors ---*/
/*------------------------------*/
void LHCfCalib::AllocateVectors() {
  UT::AllocateVector1D(fArmEnable, this->kNarm);
  UT::ClearVector1D(fArmEnable, true);
}

/*---------------------------------*/
/*--- Main calibration function ---*/
/*---------------------------------*/
void LHCfCalib::Calibration(Int_t first_ev, Int_t last_ev) {
  /* Input/Output class initialization */
  Bool_t savEvent = true;
  Bool_t miniTree = (Arm2Params::fOperation == OpType::kLHC2025) ? true : false;
  InOutCal io_class(fInputName, fOutputName, savEvent, miniTree);

  /* Initialization */
  EventCal<Arm1Params, Arm1CalPars> ev_cal_a1;
  EventCal<Arm2Params, Arm2CalPars> ev_cal_a2;
  Int_t a1_entries = -1;
  Int_t a2_entries = -1;
  if (fArmEnable[Arm1Params::kArmIndex]) {
    ev_cal_a1.Initialise();
    io_class.SetInputTree(&ev_cal_a1);
    a1_entries = io_class.GetEntries(Arm1Params::kArmIndex);
    if (ev_cal_a1.fAdamoPresence &&
        (Arm1Params::fOperation == OpType::kSPS2015 || Arm1Params::fOperation == OpType::kSPS2021 ||
         Arm1Params::fOperation == OpType::kSPS2022)) {
      ev_cal_a1.InitialiseAdamo();
    }
    if (fPedHist) {
      io_class.GetOutputFile()->cd();
      ev_cal_a1.MakeHistograms();
      gROOT->cd();
    }
    if (fAveragePedestal) {
      ev_cal_a1.AveragePedestal();
    }
  }
  if (fArmEnable[Arm2Params::kArmIndex]) {
    ev_cal_a2.Initialise();
    io_class.SetInputTree(&ev_cal_a2);
    a2_entries = io_class.GetEntries(Arm2Params::kArmIndex);
    if (ev_cal_a2.fAdamoPresence &&
        (Arm2Params::fOperation == OpType::kSPS2015 || Arm2Params::fOperation == OpType::kSPS2021 ||
         Arm2Params::fOperation == OpType::kSPS2022)) {
      ev_cal_a2.InitialiseAdamo();
    }
    if (fPedHist) {
      io_class.GetOutputFile()->cd();
      ev_cal_a2.MakeHistograms();
      gROOT->cd();
    }
    if (fAveragePedestal) {
      ev_cal_a2.AveragePedestal();
    }
  }

  /* Check if input is DATA or MC */
  if (fArmEnable[Arm1Params::kArmIndex] && fArmEnable[Arm2Params::kArmIndex]) {
    if (ev_cal_a1.fIsMC != ev_cal_a2.fIsMC) {
      UT::Printf(UT::kPrintError, "Error: different fIsMC between Arm1 and Arm2 (that should never happen...)\n");
      exit(EXIT_FAILURE);
    }
    fIsMC = ev_cal_a1.fIsMC;
  } else if (fArmEnable[Arm1Params::kArmIndex]) {
    fIsMC = ev_cal_a1.fIsMC;
  } else if (fArmEnable[Arm2Params::kArmIndex]) {
    fIsMC = ev_cal_a2.fIsMC;
  }

  /* Get number of entries in input tree */
  Int_t tot_entries = 0;
  if (a1_entries > 0 && a2_entries > 0) {
    tot_entries = TMath::Max(a1_entries, a2_entries);
    if (a1_entries != a2_entries)
      UT::Printf(UT::kPrintError, "Warning: Nev(Arm1) != Nev(Arm2) (%d != %d)\n", a1_entries, a2_entries);
  } else if (a1_entries > 0) {
    tot_entries = a1_entries;
  } else if (a2_entries > 0) {
    tot_entries = a2_entries;
  } else {
    UT::Printf(UT::kPrintError, "No entries found in input trees!\n");
    exit(EXIT_FAILURE);
  }
  first_ev = first_ev >= 0 ? first_ev : 0;
  last_ev = last_ev >= 0 ? TMath::Min(last_ev, tot_entries - 1) : tot_entries - 1;
  if (fRealignSilicon) --last_ev;
  UT::Printf(UT::kPrintInfo, "Total events: %d\n", tot_entries);
  Int_t step_prnt = 10;
  if (last_ev - first_ev + 1 > 100000)
    step_prnt = 10000;
  else if (last_ev - first_ev + 1 > 10000)
    step_prnt = 1000;
  else if (last_ev - first_ev + 1 > 1000)
    step_prnt = 100;
  else if (last_ev - first_ev + 1 > 100)
    step_prnt = 10;
  else
    step_prnt = 1;

  if (!fIsMC && fIteratePedestal) {
    /*--- Pedestal iterative cleaning ---*/
    UT::Printf(UT::kPrintInfo, "Start of pedestal iterative cleaning\n");

    // create pedestal vectors
    if (fArmEnable[Arm1Params::kArmIndex]) {
      ev_cal_a1.CreatePedestalVector(&fLvl0_Arm1);
    }
    if (fArmEnable[Arm2Params::kArmIndex]) {
      ev_cal_a2.CreatePedestalVector(&fLvl0_Arm2);
    }

    if (fArmEnable[Arm2Params::kArmIndex]) ev_cal_a2.SetSiliconEvent(first_ev);

    for (Int_t ie = first_ev; ie <= last_ev; ++ie) {
      if (ie % step_prnt == 0 || ie == last_ev) {
        UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
        fflush(stdout);
      }

      /* Arm1 pedestal */
      if (fArmEnable[Arm1Params::kArmIndex]) {
        if (io_class.GetEntry(Arm1Params::kArmIndex, ie)) {  // False if last Arm1 event is reached
          UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);
          ev_cal_a1.EventToLevel0(&fLvl0_Arm1, &fPed0_Arm1);
          if (fLvl0_Arm1.IsPedestal())  // Pedestal event
            ev_cal_a1.FillPedestalVector(&fLvl0_Arm1, &fPed0_Arm1);
        }
      }
      /* Arm2 pedestal */
      if (fArmEnable[Arm2Params::kArmIndex]) {
        if (io_class.GetEntry(Arm2Params::kArmIndex, ie)) {  // False if last Arm2 event is reached
          UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
          // This asymmetry is necessary in Arm2 in case silicon realignment is required
          Bool_t valid_entry = true;
          if (fRealignSilicon) valid_entry = io_class.RealignSilicon(Arm2Params::kArmIndex, ie + 1, &ev_cal_a2);
          if (valid_entry) {
            ev_cal_a2.EventToLevel0(&fLvl0_Arm2, &fPed0_Arm2);
            if (fLvl0_Arm2.IsPedestal())  // Pedestal event
              ev_cal_a2.FillPedestalVector(&fLvl0_Arm2, &fPed0_Arm2);
          }
        }
      }
      io_class.ClearEvent();
    }

    // iterative calculation of pedestal values
    if (fArmEnable[Arm1Params::kArmIndex]) {
      ev_cal_a1.GetPedestalVectorMeanAndRMS(&fLvl0_Arm1);
    }
    if (fArmEnable[Arm2Params::kArmIndex]) {
      ev_cal_a2.GetPedestalVectorMeanAndRMS(&fLvl0_Arm2);
    }

    // delete pedestal vectors
    if (fArmEnable[Arm1Params::kArmIndex]) {
      ev_cal_a1.DeletePedestalVector();
    }
    if (fArmEnable[Arm2Params::kArmIndex]) {
      ev_cal_a2.DeletePedestalVector();
    }

    UT::Printf(UT::kPrintInfo, "End of pedestal iterative cleaning\n");
  }

  /*--- Pedestal mean calculation ---*/
  UT::Printf(UT::kPrintInfo, "Start of pedestal mean calculation\n");

  if (fArmEnable[Arm2Params::kArmIndex]) ev_cal_a2.SetSiliconEvent(first_ev);

  for (Int_t ie = first_ev; ie <= last_ev; ++ie) {
    if (ie % step_prnt == 0 || ie == last_ev) {
      UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    /* Arm1 pedestal */
    if (fArmEnable[Arm1Params::kArmIndex]) {
      if (io_class.GetEntry(Arm1Params::kArmIndex, ie)) {  // False if last Arm1 event is reached
        UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);
        if (fIsMC) {  // Simulation
          if (io_class.GetLevel0<Arm1Params>() && io_class.GetPedestal0<Arm1Params>()) {
            ev_cal_a1.ReadPedestal(&fLvl0_Arm1, &fPed0_Arm1, &io_class);
          }
        } else {                   // LHC-Data
          if (fIteratePedestal) {  // Iterative Pedestal Subraction
            ev_cal_a1.GivePedestal(&fLvl0_Arm1, &fPed0_Arm1, &io_class);
            if (ev_cal_a1.isGoodPedestal(&fLvl0_Arm1, &fPed0_Arm1)) ev_cal_a1.SavePedestal(&fLvl0_Arm1, &fPed0_Arm1);
          } else {
            ev_cal_a1.ReadPedestal(&fLvl0_Arm1, &fPed0_Arm1, &io_class);
          }
          // ADAMO
          if (ev_cal_a1.fAdamoPresence) {
            if (Arm1Params::fOperation == OpType::kSPS2015 || Arm1Params::fOperation == OpType::kSPS2021 ||
                Arm1Params::fOperation == OpType::kSPS2022) {
              if (!fIteratePedestal || ev_cal_a1.isGoodPedestal(&fLvl0_Arm1, &fPed0_Arm1))
                ev_cal_a1.ReadPedestalAdamo(&fAdamo, &io_class);
            }
          }
        }
      }
    }
    /* Arm2 pedestal */
    if (fArmEnable[Arm2Params::kArmIndex]) {
      if (io_class.GetEntry(Arm2Params::kArmIndex, ie)) {  // False if last Arm2 event is reached
        UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
        // This asymmetry is necessary in Arm2 in case silicon realignment is required
        Bool_t valid_entry = true;
        if (fRealignSilicon) valid_entry = io_class.RealignSilicon(Arm2Params::kArmIndex, ie + 1, &ev_cal_a2);
        if (valid_entry) {
          if (fIsMC) {  // Simulation
            if (io_class.GetLevel0<Arm2Params>() && io_class.GetPedestal0<Arm2Params>()) {
              ev_cal_a2.ReadPedestal(&fLvl0_Arm2, &fPed0_Arm2, &io_class);
            }
          } else {                   // LHC-Data
            if (fIteratePedestal) {  // Iterative Pedestal Subraction
              ev_cal_a2.GivePedestal(&fLvl0_Arm2, &fPed0_Arm2, &io_class);
              if (ev_cal_a2.isGoodPedestal(&fLvl0_Arm2, &fPed0_Arm2)) ev_cal_a2.SavePedestal(&fLvl0_Arm2, &fPed0_Arm2);
            } else {
              ev_cal_a2.ReadPedestal(&fLvl0_Arm2, &fPed0_Arm2, &io_class);
            }
            // ADAMO
            if (ev_cal_a2.fAdamoPresence) {
              if (Arm2Params::fOperation == OpType::kSPS2015 || Arm2Params::fOperation == OpType::kSPS2021 ||
                  Arm2Params::fOperation == OpType::kSPS2022) {
                if (!fIteratePedestal || ev_cal_a2.isGoodPedestal(&fLvl0_Arm2, &fPed0_Arm2))
                  ev_cal_a2.ReadPedestalAdamo(&fAdamo, &io_class);
              }
            }
          }
        }
      }
    }
    io_class.ClearEvent();
  }

  // calculation of pedestal values
  if (fArmEnable[Arm1Params::kArmIndex]) {
    ev_cal_a1.GetPedestalMeanRms(&fPed0_Arm1);
    if (fPedHist) {
      io_class.GetOutputFile()->cd();
      ev_cal_a1.FillHistograms();
      gROOT->cd();
    }
  }
  if (fArmEnable[Arm2Params::kArmIndex]) {
    ev_cal_a2.GetPedestalMeanRms(&fPed0_Arm2);
    if (fPedHist) {
      io_class.GetOutputFile()->cd();
      ev_cal_a2.FillHistograms();
      gROOT->cd();
    }
  }

  if (!fIsMC && fArmEnable[Arm1Params::kArmIndex] && ev_cal_a1.fAdamoPresence &&
      (Arm1Params::fOperation == OpType::kSPS2015 || Arm1Params::fOperation == OpType::kSPS2021 ||
       Arm1Params::fOperation == OpType::kSPS2022))
    ev_cal_a1.CalculatePedestalAdamo();
  if (!fIsMC && fArmEnable[Arm2Params::kArmIndex] && ev_cal_a2.fAdamoPresence &&
      (Arm2Params::fOperation == OpType::kSPS2015 || Arm2Params::fOperation == OpType::kSPS2021 ||
       Arm2Params::fOperation == OpType::kSPS2022))
    ev_cal_a2.CalculatePedestalAdamo();

  if (fIteratePedestal) {
    if (fArmEnable[Arm1Params::kArmIndex]) {
      ev_cal_a1.DeletePedestalVariables();
    }
    if (fArmEnable[Arm2Params::kArmIndex]) {
      ev_cal_a2.DeletePedestalVariables();
    }
  }

  UT::Printf(UT::kPrintInfo, "\nEnd of pedestal mean calculation\n");

  /*--- Event Loop ---*/
  UT::Printf(UT::kPrintInfo, "Start of event loop\n");

  if (fArmEnable[Arm2Params::kArmIndex]) ev_cal_a2.SetSiliconEvent(first_ev);

  Int_t nfill_a1 = 0, nfill_a2 = 0;
  for (Int_t ie = first_ev; ie <= last_ev; ++ie) {
    if (ie % step_prnt == 0 || ie == last_ev) {
      UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    /* Arm1 event calibration */
    Bool_t arm1_valid = false;
    if (fArmEnable[Arm1Params::kArmIndex]) {
      UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);
      if (io_class.GetEntry(Arm1Params::kArmIndex, ie)) {  // False if last Arm1 event is reached
        if (!fIsMC || (io_class.GetLevel0<Arm1Params>() && io_class.GetPedestal0<Arm1Params>())) {
          // UT::Printf(UT::kPrintInfo, "Calibrate Arm%d",(Arm1Params::kArmIndex));
          if (ev_cal_a1.EventCalibration(&fLvl0_Arm1, &fPed0_Arm1, &fLvl1_Arm1, &fLvl2_Arm1, &io_class,
                                         fSavePedestal) == 0) {
            //--- Level 0 ---//
            if (fSaveLevel0 || fSaveLevel0Cal) {
              if (fSaveLevel0Cal) {
                fPed0_Arm1_aux.DataCopy(&fPed0_Arm1);  // store ped values needed by next events
                fPed0_Arm1.fPosDet.clear();
                fLvl0_Arm1.fPosDet.clear();
              }
              io_class.AddObject(&fPed0_Arm1);
              io_class.AddObject(&fLvl0_Arm1);
            }
            //--- Level 1 ---//
            if (fSaveLevel1 || fSaveLevel1Cal) {
              if (fSaveLevel1Cal) {
                fLvl1_Arm1.fPosDet.clear();
              }
              io_class.AddObject(&fLvl1_Arm1);
            }
            //--- Level 2 ---//
            if (fSaveLevel2Cal) {
              fLvl2_Arm1.fPosDet.clear();
            }
            io_class.AddObject(&fLvl2_Arm1);

            arm1_valid = true;
            nfill_a1++;
          }
        }
        if (fIsMC) {
          fMcEvent = io_class.GetMcEvent<Arm1Params>();
          io_class.AddObject(fMcEvent);
        }
        // For Adamo
        if (!fIsMC && ev_cal_a1.fAdamoPresence &&
            (Arm1Params::fOperation == OpType::kSPS2015 || Arm1Params::fOperation == OpType::kSPS2021 ||
             Arm1Params::fOperation == OpType::kSPS2022)) {
          if (ev_cal_a1.EventAdamo(&fAdamo, &io_class) == 0) io_class.AddObject(&fAdamo);
        }
      }
    }
    UT::Printf(UT::kPrintDebugFull, "arm1_valid = %d\n", arm1_valid);

    /* Arm2 event calibration */
    Bool_t arm2_valid = false;
    if (fArmEnable[Arm2Params::kArmIndex]) {
      UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
      if (io_class.GetEntry(Arm2Params::kArmIndex, ie)) {  // False if last Arm2 event is reached
        Bool_t valid_entry = true;
        if (fRealignSilicon) valid_entry = io_class.RealignSilicon(Arm2Params::kArmIndex, ie + 1, &ev_cal_a2);
        if (valid_entry) {  // This asimmetry is necessary in Arm2 in case silicon realignment is required
          if (!fIsMC || (io_class.GetLevel0<Arm2Params>() && io_class.GetPedestal0<Arm2Params>())) {
            if (ev_cal_a2.EventCalibration(&fLvl0_Arm2, &fPed0_Arm2, &fLvl1_Arm2, &fLvl2_Arm2, &io_class,
                                           fSavePedestal) == 0) {
              //--- Level 0 ---//
              if (fSaveLevel0 || fSaveLevel0Cal) {
                if (fSaveLevel0Cal) {
                  fPed0_Arm2_aux.DataCopy(&fPed0_Arm2);  // store ped values needed by next events
                  fPed0_Arm2.fPosDet.clear();
                  fLvl0_Arm2.fPosDet.clear();
                }
                io_class.AddObject(&fPed0_Arm2);
                io_class.AddObject(&fLvl0_Arm2);
              }
              //--- Level 1 ---//
              if (fSaveLevel1 || fSaveLevel1Cal) {
                if (fSaveLevel1Cal) {
                  fLvl1_Arm2.fPosDet.clear();
                }
                io_class.AddObject(&fLvl1_Arm2);
              }
              //--- Level 2 ---//
              if (fSaveLevel2Cal) {
                fLvl2_Arm2.fPosDet.clear();
              }
              io_class.AddObject(&fLvl2_Arm2);

              arm2_valid = true;
              nfill_a2++;
            }
          }
          if (fIsMC) {
            fMcEvent = io_class.GetMcEvent<Arm2Params>();
            io_class.AddObject(fMcEvent);
          }
          // For Adamo
          if (!fIsMC && ev_cal_a2.fAdamoPresence &&
              (Arm2Params::fOperation == OpType::kSPS2015 || Arm2Params::fOperation == OpType::kSPS2021 ||
               Arm2Params::fOperation == OpType::kSPS2022)) {
            if (ev_cal_a2.EventAdamo(&fAdamo, &io_class) == 0) io_class.AddObject(&fAdamo);
          }
        }
      }
    }
    UT::Printf(UT::kPrintDebugFull, "arm2_valid = %d\n", arm2_valid);

    /* Store and clear event */
    if (fIsMC || arm1_valid || arm2_valid) io_class.FillEvent();
    io_class.ClearEvent();

    /* Restore original fPosDet size (if using fSaveLevel*Cal) */
    if (arm1_valid) {
      if (fSaveLevel0Cal) {
        fPed0_Arm1.AllocateVectors();
        fPed0_Arm1.DataCopy(&fPed0_Arm1_aux);  // copy back posdet pedestals
        fLvl0_Arm1.AllocateVectors();
      }
      if (fSaveLevel1Cal) {
        fLvl1_Arm1.AllocateVectors();
      }
      if (fSaveLevel2Cal) {
        fLvl2_Arm1.AllocateVectors();
      }
    }
    if (arm2_valid) {
      if (fSaveLevel0Cal) {
        fPed0_Arm2.AllocateVectors();
        fPed0_Arm2.DataCopy(&fPed0_Arm2_aux);  // copy back posdet pedestals
        fLvl0_Arm2.AllocateVectors();
      }
      if (fSaveLevel1Cal) {
        fLvl1_Arm2.AllocateVectors();
      }
      if (fSaveLevel2Cal) {
        fLvl2_Arm2.AllocateVectors();
      }
    }
  }
  UT::Printf(UT::kPrintInfo, "\nEnd of event loop\n");
  UT::Printf(UT::kPrintInfo, Form("Arm1 Filled Events = %d\n", nfill_a1));
  UT::Printf(UT::kPrintInfo, Form("Arm2 Filled Events = %d\n", nfill_a2));

  /* Write to output file */
  io_class.WriteToOutput();
  io_class.CloseFiles();
}
