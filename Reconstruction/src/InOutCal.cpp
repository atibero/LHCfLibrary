#include "InOutCal.hh"

#include <TROOT.h>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "EventCal.hh"
#include "Level3.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(InOutCal);
#endif

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
InOutCal::InOutCal(TString in_name, TString out_name, bool save_event, bool use_mini_tree)
    : fInputName(in_name), fOutputName(out_name), fSaveLHCfEvent(save_event), fUseMiniTree(use_mini_tree) {
  fInputEv = NULL;
  Initialize();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
InOutCal::~InOutCal() {
  delete fInputFile;
  delete fInputEv;
  delete fOutputFile;
  delete fOutputEv;
}

/*------------------------------*/
/*--- Input/Output functions ---*/
/*------------------------------*/
void InOutCal::Initialize() {
  Utils::AllocateVector1D(fInputTree, this->kNarm);
  if (fUseMiniTree) {
    Utils::AllocateVector1D(fInputMiniTree, this->kNarm);
  }
  BuildOutputTree();
}

template <typename evcal>
void InOutCal::SetInputTree(evcal *ev_cal) {
  Utils::Printf(Utils::kPrintInfo, "\nSetting input tree...");
  fflush(stdout);

  /* Open input ROOT file */
  fInputFile = new TFile(fInputName.Data(), "READ");
  if (!fInputFile->IsOpen()) {
    Utils::Printf(Utils::kPrintError, "Error: input file \"%s\" not opened\n", fInputName.Data());
    exit(EXIT_FAILURE);
  }
  if (fInputFile->Get(ev_cal->fMidasTree.Data())) {
    ev_cal->fIsMC = false;  // DATA (MIDAS tree)
  } else if (fInputFile->Get("LHCfEvents")) {
    ev_cal->fIsMC = true;  // MC (LHCfEvent with Level0 already converted from End2End)
  } else {
    Utils::Printf(Utils::kPrintError, "Error: cannot find a valid TTree in input file\n");
    exit(EXIT_FAILURE);
  }

  const Int_t iarm = ev_cal->kArmIndex;

  if (!ev_cal->fIsMC) {
    /* Load DATA tree */
    fInputFile->GetObject(ev_cal->fMidasTree.Data(), fInputTree[iarm]);
    if (ev_cal->fOperation == OpType::kLHC2025)
      fInputFile->GetObject(ev_cal->fMidasMiniTree.Data(), fInputMiniTree[iarm]);

    if (ev_cal->fOperation != OpType::kSPS2021 && ev_cal->fOperation != OpType::kSPS2022 &&
        ev_cal->fOperation != OpType::kLHC2022 && ev_cal->fOperation != OpType::kLHC2025) {
      if (fInputTree[iarm]->FindBranch("Number"))
        fInputTree[iarm]->SetBranchAddress("Number", &(ev_cal->Number));
      else {
        Utils::Printf(Utils::kPrintError, "Error : cannot find BANK[Number]\n");
        exit(EXIT_FAILURE);
      }
    }

    if (fInputTree[iarm]->FindBranch("TIME"))
      fInputTree[iarm]->SetBranchAddress("TIME", &(ev_cal->TIME));
    else {
      Utils::Printf(Utils::kPrintError, "Error : cannot find BANK[TIME].\n");
      exit(EXIT_FAILURE);
    }

    if (ev_cal->fOperation == OpType::kSPS2014 || ev_cal->fOperation == OpType::kSPS2015 ||
        ev_cal->fOperation == OpType::kLHC2015 || ev_cal->fOperation == OpType::kLHC2016) {
      if (fInputTree[iarm]->FindBranch("SCL0"))
        fInputTree[iarm]->SetBranchAddress("SCL0", &(ev_cal->SCL0));
      else {
        Utils::Printf(Utils::kPrintError, "Error : cannot find BANK[SCL0].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputMiniTree[iarm]->FindBranch("ADC0"))
        fInputMiniTree[iarm]->SetBranchAddress("ADC0", &(ev_cal->ADC0));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[ADC0].\n");
        exit(EXIT_FAILURE);
      }
    } else {
      if (fInputTree[iarm]->FindBranch("ADC0"))
        fInputTree[iarm]->SetBranchAddress("ADC0", &(ev_cal->ADC0));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[ADC0].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputMiniTree[iarm]->FindBranch("ADC1"))
        fInputMiniTree[iarm]->SetBranchAddress("ADC1", &(ev_cal->ADC1));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[ADC1].\n");
        exit(EXIT_FAILURE);
      }
    } else {
      if (fInputTree[iarm]->FindBranch("ADC1"))
        fInputTree[iarm]->SetBranchAddress("ADC1", &(ev_cal->ADC1));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[ADC1].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (fInputTree[iarm]->FindBranch("ADC2"))
      fInputTree[iarm]->SetBranchAddress("ADC2", &(ev_cal->ADC2));
    else {
      Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[ADC2].\n");
      exit(EXIT_FAILURE);
    }

    if (ev_cal->fOperation == OpType::kSPS2014 || ev_cal->fOperation == OpType::kSPS2015 ||
        ev_cal->fOperation == OpType::kLHC2015 || ev_cal->fOperation == OpType::kLHC2016) {
      if (fInputTree[iarm]->FindBranch("ADC3"))
        fInputTree[iarm]->SetBranchAddress("ADC3", &(ev_cal->ADC3));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[ADC3].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputMiniTree[iarm]->FindBranch("CAD0"))
        fInputMiniTree[iarm]->SetBranchAddress("CAD0", &(ev_cal->CAD0));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[CAD0].\n");
        exit(EXIT_FAILURE);
      }
    } else {
      if (fInputTree[iarm]->FindBranch("CAD0"))
        fInputTree[iarm]->SetBranchAddress("CAD0", &(ev_cal->CAD0));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[CAD0].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputMiniTree[iarm]->FindBranch("CAD1"))
        fInputMiniTree[iarm]->SetBranchAddress("CAD1", &(ev_cal->CAD1));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[CAD1].\n");
        exit(EXIT_FAILURE);
      }
    } else {
      if (fInputTree[iarm]->FindBranch("CAD1"))
        fInputTree[iarm]->SetBranchAddress("CAD1", &(ev_cal->CAD1));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[CAD1].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputMiniTree[iarm]->FindBranch((ev_cal->fPosDetKey).Data()))
        fInputMiniTree[iarm]->SetBranchAddress((ev_cal->fPosDetKey).Data(), &(ev_cal->POSDET));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[SCIF/ARM2].\n");
        exit(EXIT_FAILURE);
      }
    } else {
      if (fInputTree[iarm]->FindBranch((ev_cal->fPosDetKey).Data()))
        fInputTree[iarm]->SetBranchAddress((ev_cal->fPosDetKey).Data(), &(ev_cal->POSDET));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[SCIF/ARM2].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (fInputTree[iarm]->FindBranch("TDC0"))
      fInputTree[iarm]->SetBranchAddress("TDC0", &(ev_cal->TDC0));
    else {
      Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[TDC0].\n");
      exit(EXIT_FAILURE);
    }

    if (fInputTree[iarm]->FindBranch("GPI0"))
      fInputTree[iarm]->SetBranchAddress("GPI0", &(ev_cal->GPI0));
    else {
      Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[GPI0].\n");
      exit(EXIT_FAILURE);
    }

    if (ev_cal->fOperation == OpType::kLHC2022 || ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputTree[iarm]->FindBranch("RUNI"))
        fInputTree[iarm]->SetBranchAddress("RUNI", &(ev_cal->RUNI));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[RUNI].\n");
        exit(EXIT_FAILURE);
      }
    }

    // Auxiliary check of event alignment in 2025
    if (ev_cal->fOperation == OpType::kLHC2025) {
      if (fInputMiniTree[iarm]->FindBranch("GPI0"))
        fInputMiniTree[iarm]->SetBranchAddress("GPI0", &(ev_cal->MiniGPI0));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[(Mini)GPI0].\n");
        exit(EXIT_FAILURE);
      }

      if (fInputMiniTree[iarm]->FindBranch("RUNI"))
        fInputMiniTree[iarm]->SetBranchAddress("RUNI", &(ev_cal->MiniRUNI));
      else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[(Mini)RUNI].\n");
        exit(EXIT_FAILURE);
      }
    }

    if (ev_cal->fOperation == OpType::kSPS2015 || ev_cal->fOperation == OpType::kSPS2021 ||
        ev_cal->fOperation == OpType::kSPS2022) {
      if (fInputTree[iarm]->FindBranch("FSIL")) {
        // Utils::Printf(Utils::kPrintDebug, "Found FSIL branch\n");
        fInputTree[iarm]->SetBranchAddress("FSIL", &(ev_cal->FSIL));
        ev_cal->fAdamoPresence = kTRUE;
      } else {
        Utils::Printf(Utils::kPrintError, "Warning: cannot find BANK[FSIL].\n");
        ev_cal->fAdamoPresence = kFALSE;
      }
    }

  } else {
    /* Load MC tree */
    fInputFile->GetObject("LHCfEvents", fInputTree[iarm]);
    fInputEv = new LHCfEvent("event", "LHCfEvent");
    fInputTree[iarm]->SetBranchAddress("ev.", &fInputEv);
  }

  gROOT->cd();
  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

void InOutCal::BuildOutputTree() {
  Utils::Printf(Utils::kPrintInfo, "Building output tree...");
  fflush(stdout);

#if defined(NO_RCOMPRESSION)
  fOutputFile = new TFile(fOutputName.Data(), "RECREATE");
#else
  fOutputFile =
      new TFile(fOutputName.Data(), "RECREATE", "",
                ROOT::RCompressionSetting::EAlgorithm::kLZ4 * 100 + ROOT::RCompressionSetting::ELevel::kUncompressed);
#endif

  if (!fOutputFile->IsOpen()) {
    Utils::Printf(Utils::kPrintError, "Error: output file \"%s\" not opened\n", fOutputName.Data());
    exit(EXIT_FAILURE);
  }
  if (fSaveLHCfEvent) {
    fOutputEv = new LHCfEvent("event", "LHCfEvent");
    fOutputTree = new TTree("LHCfEvents", "LHCf events (calibrated)");
    fOutputTree->Branch("ev.", "nLHCf::LHCfEvent", &fOutputEv);
    fOutputTree->SetMaxTreeSize(1000 * Long64_t(1000000000));
  }
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

void InOutCal::WriteToOutput() {
  Utils::Printf(Utils::kPrintInfo, "Saving to file...");
  fflush(stdout);

  fOutputFile->cd();
  if (fSaveLHCfEvent) fOutputTree->Write("", TObject::kOverwrite);
  gROOT->cd();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

void InOutCal::CloseFiles() {
  Utils::Printf(Utils::kPrintInfo, "Closing files...");
  fflush(stdout);

  fOutputFile->Close();
  fInputFile->Close();

  Utils::Printf(Utils::kPrintInfo, " Done.\n");
}

/*------------------*/
/*--- Interfaces ---*/
/*------------------*/
Int_t InOutCal::GetEntries(Int_t iarm) { return fInputTree[iarm]->GetEntries(); }

Bool_t InOutCal::GetEntry(Int_t iarm, Int_t ie) {
  if (ie >= this->GetEntries(iarm)) return false;

  fInputTree[iarm]->GetEntry(ie);
  if (fUseMiniTree) {
    fInputMiniTree[iarm]->GetEntry(ie);
  }

  return true;
}

template <typename evcal>
Bool_t InOutCal::RealignSilicon(Int_t iarm, Int_t ie, evcal *ev_cal) {
  // Save all MIDAS bank from previous event to copy later
  EventCal<Arm2Params, Arm2CalPars> ve_lac = *ev_cal;

  if (!fInputTree[iarm]->GetEntry(ie)) return false;
  if (ev_cal->fOperation == OpType::kLHC2025) {
    if (!fInputMiniTree[iarm]->GetEntry(ie)) return false;
  }

  // Copy all MIDAS bank from previous event except POSDET
  ev_cal->Number = ve_lac.Number;
  ev_cal->TIME = ve_lac.TIME;
  ev_cal->SCL0 = ve_lac.SCL0;
  ev_cal->ADC0 = ve_lac.ADC0;
  ev_cal->ADC1 = ve_lac.ADC1;
  ev_cal->ADC2 = ve_lac.ADC2;
  ev_cal->ADC3 = ve_lac.ADC3;
  ev_cal->ADC4 = ve_lac.ADC4;
  ev_cal->CAD0 = ve_lac.CAD0;
  ev_cal->CAD1 = ve_lac.CAD1;
  ev_cal->TDC0 = ve_lac.TDC0;
  ev_cal->GPI0 = ve_lac.GPI0;
  ev_cal->GPI1 = ve_lac.GPI1;
  ev_cal->FSIL = ve_lac.FSIL;
  ev_cal->RUNI = ve_lac.RUNI;

  if (ev_cal->fOperation == OpType::kLHC2025) {
    ev_cal->MiniGPI0 = ve_lac.MiniGPI0;
    ev_cal->MiniRUNI = ve_lac.MiniRUNI;
  }

  return true;
}

template <typename armclass>
Level0<armclass> *InOutCal::GetLevel0() {
  return (Level0<armclass> *)fInputEv->Get(Form("lvl0_a%d", armclass::kArmIndex + 1));
}

template <typename armclass>
Level0<armclass> *InOutCal::GetPedestal0() {
  return (Level0<armclass> *)fInputEv->Get(Form("ped0_a%d", armclass::kArmIndex + 1));
}

template <typename armclass>
McEvent *InOutCal::GetMcEvent() {
  return (McEvent *)fInputEv->Get(Form("true_a%d", armclass::kArmIndex + 1));
}

void InOutCal::AddObject(TObject *obj) {
  if (fSaveLHCfEvent) fOutputEv->Add(obj);
}

void InOutCal::WriteObject(TObject *obj) {
  fOutputFile->cd();
  obj->Write("", TObject::kOverwrite);
  gROOT->cd();
}

void InOutCal::FillHeader() {
  // Clear the header information
  fOutputEv->HeaderClear();

  if (fOutputEv->Check("lvl2_a1")) {
    Level2<Arm1Params> *lvl2_a1 = (Level2<Arm1Params> *)fOutputEv->FindObject("lvl2_a1");
    fOutputEv->fRun = lvl2_a1->fRun;
    fOutputEv->fA1number = lvl2_a1->fEvent;
    fOutputEv->fGnumber = lvl2_a1->fGevent;
    fOutputEv->fA1flag[0] = lvl2_a1->fFlag[0];
    fOutputEv->fA1flag[1] = lvl2_a1->fFlag[1];
  }

  if (fOutputEv->Check("lvl2_a2")) {
    Level2<Arm2Params> *lvl2_a2 = (Level2<Arm2Params> *)fOutputEv->FindObject("lvl2_a2");
    fOutputEv->fRun = lvl2_a2->fRun;
    fOutputEv->fA2number = lvl2_a2->fEvent;
    if (fOutputEv->fGnumber <= 0) fOutputEv->fGnumber = lvl2_a2->fGevent;
    fOutputEv->fA2flag[0] = lvl2_a2->fFlag[0];
    fOutputEv->fA2flag[1] = lvl2_a2->fFlag[1];
  }

  return;
}

void InOutCal::FillEvent() {
  if (fSaveLHCfEvent) {
    if (fInputEv != NULL) {
      fOutputEv->HeaderCopy(fInputEv);
    } else {
      FillHeader();
    }

    fOutputTree->Fill();
  }
}

void InOutCal::ClearEvent() {
  if (fInputEv != NULL) {
    fInputEv->HeaderClear();
    fInputEv->ObjDelete();
  }
  if (fSaveLHCfEvent) {
    fOutputEv->HeaderClear();
    fOutputEv->ObjClear();
  }
  // fOutputEv->ObjDelete();
}

namespace nLHCf {
template void InOutCal::SetInputTree(EventCal<Arm1Params, Arm1CalPars> *);
template void InOutCal::SetInputTree(EventCal<Arm2Params, Arm2CalPars> *);
template Bool_t InOutCal::RealignSilicon(Int_t iarm, Int_t ie, EventCal<Arm2Params, Arm2CalPars> *);
template Level0<Arm1Params> *InOutCal::GetLevel0<Arm1Params>();
template Level0<Arm2Params> *InOutCal::GetLevel0<Arm2Params>();
template Level0<Arm1Params> *InOutCal::GetPedestal0<Arm1Params>();
template Level0<Arm2Params> *InOutCal::GetPedestal0<Arm2Params>();
template McEvent *InOutCal::GetMcEvent<Arm1Params>();
template McEvent *InOutCal::GetMcEvent<Arm2Params>();
}  // namespace nLHCf
