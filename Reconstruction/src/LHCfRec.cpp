#include "LHCfRec.hh"

#include <TH2F.h>
#include <TMath.h>
#include <TROOT.h>

#include "EventRec.hh"
#include "InOutRec.hh"
#include "OldInterface.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(LHCfRec);
#endif

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
LHCfRec::LHCfRec()
    : fLvl3_Arm1("lvl3_a1", "Level3 (Arm1)"),
      fLvl3_Arm2("lvl3_a2", "Level3 (Arm2)"),
      fInputName("calibrated.root"),
      fOutputName("reconstructed.root"),
      fPhotonEnable(true),
      fNeutronEnable(true),
      fSaveLevel0(false),
      fSaveLevel1(false),
      fSaveLevel2(false),
      fSaveLevel0Cal(false),
      fSaveLevel1Cal(false),
      fSaveLevel2Cal(false),
      fReadInputPos(false),
      fUseTrueCoo(false),
      fDevelopment(false),
      fPosRecMode("full"),
      fPhotonFitType(0),
      fNeutronFitType(0),
      fPhotonErecType(1),
      //fNeutronErecType(1),
      fFastPeakSearch(true),
      fOldName(""),
      fOldFlag(false) {
  AllocateVectors();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
LHCfRec::~LHCfRec() {}

/*------------------------------*/
/*--- Allocate/clear vectors ---*/
/*------------------------------*/
void LHCfRec::AllocateVectors() {
  Utils::AllocateVector1D(fArmEnable, this->kNarm);
  Utils::ClearVector1D(fArmEnable, true);
}

/*------------------------------------*/
/*--- Main reconstruction function ---*/
/*------------------------------------*/
void LHCfRec::Reconstruction(Int_t first_ev, Int_t last_ev) {
  /* Input/Output class initialization */
  InOutRec io_class(fInputName, fOutputName);

  /* Debug */
  OldInterface<Arm1Params, Arm1RecPars> old_data_a1(fOldName.Data());
  OldInterface<Arm2Params, Arm2RecPars> old_data_a2(fOldName.Data());
  if (fOldFlag && Utils::fVerbose >= Utils::kPrintDebug) {
    if (fArmEnable[Arm1Params::kArmIndex]) old_data_a1.Open();
    if (fArmEnable[Arm2Params::kArmIndex]) old_data_a2.Open();
  }

  /* Event reconstruction class initialization */
  EventRec<Arm1Params, Arm1CalPars, Arm1RecPars> ev_rec_a1(fPhotonEnable, fNeutronEnable, fPosRecMode);
  EventRec<Arm2Params, Arm2CalPars, Arm2RecPars> ev_rec_a2(fPhotonEnable, fNeutronEnable, fPosRecMode);

  /* Reconstruction Type */
  if (fPhotonFitType < 0) {
    // Set Default Type
    ev_rec_a1.SetPhotonFitType(Arm1RecPars::kDefaultPhotonFitType);
    ev_rec_a2.SetPhotonFitType(Arm2RecPars::kDefaultPhotonFitType);
  }
  else {
    ev_rec_a1.SetPhotonFitType(fPhotonFitType);
    ev_rec_a2.SetPhotonFitType(fPhotonFitType);
  }

  if (fNeutronFitType < 0) {
    ev_rec_a1.SetNeutronFitType(Arm1RecPars::kDefaultNeutronFitType);
    ev_rec_a2.SetNeutronFitType(Arm2RecPars::kDefaultNeutronFitType);
  }
  else {
    ev_rec_a1.SetNeutronFitType(fNeutronFitType);
    ev_rec_a2.SetNeutronFitType(fNeutronFitType);
  }

  if (fPhotonErecType < 0) {
    // Set Default Type
    ev_rec_a1.SetPhotonErecType(Arm1RecPars::kDefaultPhotonErecType);
    ev_rec_a2.SetPhotonErecType(Arm2RecPars::kDefaultPhotonErecType);
  }
  else {
    ev_rec_a1.SetPhotonFitType(fPhotonFitType);
    ev_rec_a2.SetPhotonFitType(fPhotonFitType);
  }

  if (fArmEnable[Arm1Params::kArmIndex]) ev_rec_a1.Initialise();
  if (fArmEnable[Arm2Params::kArmIndex]) ev_rec_a2.Initialise();
  if (!fFastPeakSearch) {
    ev_rec_a1.DisableFastPeakSearch();
    ev_rec_a2.DisableFastPeakSearch();
  }
  if (fDevelopment) {
    ev_rec_a1.Development();
    ev_rec_a2.Development();
  }

  /* Check if MC */
  Bool_t is_MC[this->kNarm];
  io_class.GetEntry(0);
  if (fArmEnable[Arm1Params::kArmIndex]) {
    if (io_class.GetMcEvent<Arm1Params>() != 0)
      is_MC[Arm1Params::kArmIndex] = true;
    else
      is_MC[Arm1Params::kArmIndex] = false;
  }
  if (fArmEnable[Arm2Params::kArmIndex]) {
    if (io_class.GetMcEvent<Arm2Params>() != 0)
      is_MC[Arm2Params::kArmIndex] = true;
    else
      is_MC[Arm2Params::kArmIndex] = false;
  }

  /* Event Loop */
  last_ev = last_ev > 0 ? TMath::Min(last_ev, io_class.GetEntries() - 1) : io_class.GetEntries() - 1;
  Utils::Printf(Utils::kPrintInfo, "Total events: %d\n", io_class.GetEntries());
  Int_t step_prnt = 10;
  if (last_ev - first_ev + 1 > 100000)
    step_prnt = 10000;
  else if (last_ev - first_ev + 1 > 10000)
    step_prnt = 1000;
  else if (last_ev - first_ev + 1 > 1000)
    step_prnt = 100;
  else if (last_ev - first_ev + 1 > 100)
    step_prnt = 10;
  else
    step_prnt = 1;
  for (Int_t ie = first_ev; ie <= last_ev; ++ie) {
    if (ie % step_prnt == 0 || ie == last_ev) {
      Utils::Printf(Utils::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    /* Get entry from input tree */
    io_class.GetEntry(ie);
    // Utils::Printf(Utils::kPrintInfo,"\n finish to get entries");

    /* Arm1 event reconstruction */
    if (fArmEnable[Arm1Params::kArmIndex]) {
      // Utils::Printf(Utils::kPrintInfo,"\n arm1: event %d\n", ie);
      Utils::Printf(Utils::kPrintDebugFull, "Arm1: event %d\n", ie);
      if (fLvl2_Arm1 = io_class.GetArm1Level2()) {
        if (fReadInputPos) {
          fLvl3_Arm1.DataClear();
          if (fLvl3_Input_Arm1 = io_class.GetArm1Level3()) {
            fLvl3_Arm1.DataCopy(fLvl3_Input_Arm1, "photon_pos neutron_pos common");
          } else {
            Utils::Printf(Utils::kPrintError,
                          "LHCfRec::Reconstruction: cannot find required input Level3 (fPosRecMode = \"none\")\n");
            exit(EXIT_FAILURE);
          }
        }

        if (fUseTrueCoo) {
          McEvent *mc_Arm1 = io_class.GetMcEvent<Arm1Params>();
          ev_rec_a1.EventRecoTrueCoord(mc_Arm1, fLvl2_Arm1, &fLvl3_Arm1);
        } else {
          ev_rec_a1.EventReconstruction(fLvl2_Arm1, &fLvl3_Arm1);
        }
        io_class.AddObject(&fLvl3_Arm1);
        //--- Level 0 ---//
        if (fSaveLevel0 || fSaveLevel0Cal) {
          if (fPed0_Arm1 = io_class.GetArm1Pedestal()) {
            if (fSaveLevel0Cal) {
              fPed0_Arm1->fPosDet.clear();
            }
            io_class.AddObject(fPed0_Arm1);
          }
          if (fLvl0_Arm1 = io_class.GetArm1Level0()) {
            if (fSaveLevel0Cal) {
              fLvl0_Arm1->fPosDet.clear();
            }
            io_class.AddObject(fLvl0_Arm1);
          }
        }
        //--- Level 1 ---//
        if (fSaveLevel1 || fSaveLevel1Cal) {
          if (fLvl1_Arm1 = io_class.GetArm1Level1()) {
            if (fSaveLevel1Cal) {
              fLvl1_Arm1->fPosDet.clear();
            }
            io_class.AddObject(fLvl1_Arm1);
          }
        }
        //--- Level 2 ---//
        if (fSaveLevel2 || fSaveLevel2Cal) {
          if (fLvl2_Arm1 = io_class.GetArm1Level2()) {
            if (fSaveLevel2Cal) {
              fLvl2_Arm1->fPosDet.clear();
              fLvl2_Arm1->fErrPosDet.clear();
            }
            io_class.AddObject(fLvl2_Arm1);
          }
        }
      }
      if (is_MC[Arm1Params::kArmIndex]) io_class.AddObject(io_class.GetMcEvent<Arm1Params>());

      // !!! DEBUG !!!
      if (fLvl2_Arm1 && Utils::fVerbose >= Utils::kPrintDebugFull
          //&& (fLvl3_Arm1.fPhotonMultiHit[0] || fLvl3_Arm1.fPhotonMultiHit[1])
          //&& (fLvl3_Arm1.fPhotonEnergyMH[0][1] > 0. || fLvl3_Arm1.fPhotonEnergyMH[1][1] > 0.)
      ) {
        ev_rec_a1.Print(ie, fLvl2_Arm1, &fLvl3_Arm1);
#ifdef DEBUG_POS
        ev_rec_a1.fDebug_cal_cv[0]->Update();
        ev_rec_a1.fDebug_cal_cv[1]->Update();
        ev_rec_a1.fDebug_pos_cv[0]->Update();
        ev_rec_a1.fDebug_pos_cv[1]->Update();
        ev_rec_a1.fDebug_wait->cd();
        ev_rec_a1.fDebug_wait->WaitPrimitive();
#else
        getchar();
#endif
      }
    }
    /* Arm2 event reconstruction */
    if (fArmEnable[Arm2Params::kArmIndex]) {
      Utils::Printf(Utils::kPrintDebugFull, "Arm2: event %d\n", ie);
      if (fLvl2_Arm2 = io_class.GetArm2Level2()) {
        if (fReadInputPos) {
          fLvl3_Arm2.DataClear();
          if (fLvl3_Input_Arm2 = io_class.GetArm2Level3()) {
            fLvl3_Arm2.DataCopy(fLvl3_Input_Arm2, "photon_pos neutron_pos common");
          } else {
            Utils::Printf(Utils::kPrintError,
                          "LHCfRec::Reconstruction: cannot find required input Level3 (fPosRecMode = \"none\")\n");
            exit(EXIT_FAILURE);
          }
        }

        if (fUseTrueCoo) {
          McEvent *mc_Arm2 = io_class.GetMcEvent<Arm2Params>();
          ev_rec_a2.EventRecoTrueCoord(mc_Arm2, fLvl2_Arm2, &fLvl3_Arm2);
        } else {
          //          McEvent *mc_Arm2 = io_class.GetMcEvent<Arm2Params>();
          //          ev_rec_a2.EventRecoDebug(mc_Arm2, fLvl2_Arm2, &fLvl3_Arm2);  // Eugenio
          ev_rec_a2.EventReconstruction(fLvl2_Arm2, &fLvl3_Arm2);
        }

        io_class.AddObject(&fLvl3_Arm2);
        //--- Level 0 ---//
        if (fSaveLevel0 || fSaveLevel0Cal) {
          if (fPed0_Arm2 = io_class.GetArm2Pedestal()) {
            if (fSaveLevel0Cal) {
              fPed0_Arm2->fPosDet.clear();
            }
            io_class.AddObject(fPed0_Arm2);
          }
          if (fLvl0_Arm2 = io_class.GetArm2Level0()) {
            if (fSaveLevel0Cal) {
              fLvl0_Arm2->fPosDet.clear();
            }
            io_class.AddObject(fLvl0_Arm2);
          }
        }
        //--- Level 1 ---//
        if (fSaveLevel1 || fSaveLevel1Cal) {
          if (fLvl1_Arm2 = io_class.GetArm2Level1()) {
            if (fSaveLevel1Cal) {
              fLvl1_Arm2->fPosDet.clear();
            }
            io_class.AddObject(fLvl1_Arm2);
          }
        }
        //--- Level 2 ---//
        if (fSaveLevel2 || fSaveLevel2Cal) {
          if (fLvl2_Arm2 = io_class.GetArm2Level2()) {
            if (fSaveLevel2Cal) {
              fLvl2_Arm2->fPosDet.clear();
              fLvl2_Arm2->fErrPosDet.clear();
            }
            io_class.AddObject(fLvl2_Arm2);
          }
        }
      }
      if (is_MC[Arm2Params::kArmIndex]) io_class.AddObject(io_class.GetMcEvent<Arm2Params>());

      // !!! DEBUG !!!  //Eugenio
      if (fLvl2_Arm2 && Utils::fVerbose >= Utils::kPrintDebugFull
          //&& (fLvl3_Arm2.fPhotonMultiHit[0] || fLvl3_Arm2.fPhotonMultiHit[1])
          //&& (fLvl3_Arm2.fPhotonEnergyMH[0][1] > 0. || fLvl3_Arm2.fPhotonEnergyMH[1][1] > 0.)
      ) {
        ev_rec_a2.Print(ie, fLvl2_Arm2, &fLvl3_Arm2);
        if (fOldFlag) {
          old_data_a2.GetEvent(ie);
          old_data_a2.Print(ie);
        }
#ifdef DEBUG_POS
        ev_rec_a2.fDebug_cal_cv[0]->Update();
        ev_rec_a2.fDebug_cal_cv[1]->Update();
        ev_rec_a2.fDebug_pos_cv[0]->Update();
        ev_rec_a2.fDebug_pos_cv[1]->Update();
        ev_rec_a2.fDebug_wait->cd();
        ev_rec_a2.fDebug_wait->WaitPrimitive();
#else
        getchar();
#endif
      }
    }

    /* Store and clear event */
    io_class.FillEvent();
    io_class.ClearEvent();
  }
  Utils::Printf(Utils::kPrintInfo, "\n");

  if (fOldFlag && Utils::fVerbose >= Utils::kPrintDebug) {
    if (fArmEnable[Arm1Params::kArmIndex]) old_data_a1.Close();
    if (fArmEnable[Arm2Params::kArmIndex]) old_data_a2.Close();
  }

  /* Write to output file */
  io_class.WriteToOutput();
  io_class.CloseFiles();
}
