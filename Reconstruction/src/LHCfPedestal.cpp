#include "LHCfPedestal.hh"

#include <TROOT.h>

#include <iostream>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(LHCfPedestal);
#endif

typedef Utils UT;

const Int_t LHCfPedestal::calbin[2] = {4000, 400};
const Double_t LHCfPedestal::calmin[2] = {0, 0};
const Double_t LHCfPedestal::calmax[2] = {4000, 400};
const Int_t LHCfPedestal::offbin[2] = {4000, 400};
const Double_t LHCfPedestal::offmin[2] = {-2000, -200};
const Double_t LHCfPedestal::offmax[2] = {+2000, +200};
const Int_t LHCfPedestal::barbin = 1000;
const Double_t LHCfPedestal::barmin = 30000;
const Double_t LHCfPedestal::barmax = 40000;
const Int_t LHCfPedestal::silbin = 1000;
const Double_t LHCfPedestal::silmin = 1000;
const Double_t LHCfPedestal::silmax = 2000;
const Int_t LHCfPedestal::lisbin = 1250;
const Double_t LHCfPedestal::lismin = 5000;
const Double_t LHCfPedestal::lismax = 7500;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
LHCfPedestal::LHCfPedestal()
    : fPed0_Arm1("ped0_a1", "Pedestal (Arm1)"),
      fPed0_Arm2("ped0_a2", "Pedestal (Arm2)"),
      fLvl0_Arm1("lvl0_a1", "Level0 (Arm1)"),
      fLvl0_Arm2("lvl0_a2", "Level0 (Arm2)"),
      fInputName("raw.root"),
      fOutputName("pedestal.root"),
      fSaveHistogram(false),
      fRealignSilicon(false),
      fAveragePedestal(false) {
  AllocateVectors();
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
LHCfPedestal::~LHCfPedestal() {}

/*------------------------------*/
/*--- Allocate/clear vectors ---*/
/*------------------------------*/
void LHCfPedestal::AllocateVectors() {
  UT::AllocateVector1D(fArmEnable, this->kNarm);

  UT::AllocateVector1D(aCalPedVec, this->kNarm);
  UT::AllocateVector1D(aCalPedAve, this->kNarm);
  UT::AllocateVector1D(aCalPedRMS, this->kNarm);

  UT::AllocateVector1D(fNiters, this->kNarm);
  UT::AllocateVector1D(fNsigma, this->kNarm);

  UT::AllocateVector1D(aCalNosVec, this->kNarm);
  UT::AllocateVector1D(aCalNosAve, this->kNarm);
  UT::AllocateVector1D(aCalNosRMS, this->kNarm);

  UT::ClearVector1D(fArmEnable, true);
}

/*-----------------------------------------------*/
/*--- Fill handling Underflow/Overflow events ---*/
/*-----------------------------------------------*/
void LHCfPedestal::FillSpecial(TH1F *histo, Float_t value) {
  Int_t valbin = histo->FindBin(value);
  if (histo->IsBinUnderflow(valbin))
    histo->Fill(histo->GetBinCenter(valbin + 1));
  else if (histo->IsBinOverflow(valbin))
    histo->Fill(histo->GetBinCenter(valbin - 1));
  else
    histo->Fill(value);
}

/*--------------------------------------*/
/*------ Initialize Pedestal Tree ------*/
/*--------------------------------------*/
template <typename armclass>
void LHCfPedestal::InitPedestalTree(Level0<armclass> *lvl0) {
  cal_ped.resize(this->kNarm);
  pos_ped.resize(this->kNarm);

  caltree.resize(this->kNarm);
  postree.resize(this->kNarm);

  if (lvl0->kArmIndex == Arm1Params::kArmIndex) {
    caltree[lvl0->kArmIndex] = new TTree("a1_cal_ped_tree", "Calorimeter Pedestal Tree");
    postree[lvl0->kArmIndex] = new TTree("a1_pos_ped_tree", "Position Detector Pedestal Tree");
  } else if (lvl0->kArmIndex == Arm2Params::kArmIndex) {
    caltree[lvl0->kArmIndex] = new TTree("a2_cal_ped_tree", "Calorimeter Pedestal Tree");
    postree[lvl0->kArmIndex] = new TTree("a2_pos_ped_tree", "Position Detector Pedestal Tree");
  }

  caltree[lvl0->kArmIndex]->Branch("pedestal", &cal_ped[lvl0->kArmIndex]);
  postree[lvl0->kArmIndex]->Branch("pedestal", &pos_ped[lvl0->kArmIndex]);
}

/*--------------------------------*/
/*------ Fill Pedestal Tree ------*/
/*--------------------------------*/
template <typename armclass>
void LHCfPedestal::FillPedestalTree(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  cal_ped[lvl0->kArmIndex].clear();
  for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir)
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
        if (fAveragePedestal) {  // ROOT File contains only pedestal signal
          cal_ped[lvl0->kArmIndex].push_back(lvl0->fCalorimeter[it][il][ir]);
        } else {  // ROOT File contains pedestal-delayed gate signal
          cal_ped[lvl0->kArmIndex].push_back(lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir]);
        }
      }
  caltree[lvl0->kArmIndex]->Fill();

  pos_ped[lvl0->kArmIndex].clear();
  if (lvl0->kArmIndex == Arm1Params::kArmIndex) {
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            pos_ped[lvl0->kArmIndex].push_back(lvl0->fPosDet[it][il][ixy][ic][0]);
  } else if (lvl0->kArmIndex == Arm2Params::kArmIndex) {
    for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[0]; ++ic)
            pos_ped[lvl0->kArmIndex].push_back(lvl0->fPosDet[0][il][ixy][ic][is]);
    }
  }
  postree[lvl0->kArmIndex]->Fill();
}

/*------------------------------------*/
/*----- Write Pedestal Histogram -----*/
/*------------------------------------*/
template <typename armclass>
void LHCfPedestal::WriteShiftTree(Level0<armclass> *lvl0) {
  if (fArmEnable[lvl0->kArmIndex]) {
    io_class->WriteObject(caltree[lvl0->kArmIndex]);
    io_class->WriteObject(postree[lvl0->kArmIndex]);
  }
}

/*-------------------------------------*/
/*--- Initialize Mean-RMS Histogram ---*/
/*-------------------------------------*/
template <typename armclass>
void LHCfPedestal::InitMeanRMSHistogram(Level0<armclass> *lvl0) {
  UT::Printf(UT::kPrintInfo, "Building pedestal Mean and RMS histogram...");
  fflush(stdout);

  fPedCalMean.resize(this->kNarm);
  fPedCalRMS.resize(this->kNarm);
  fPedPosMean.resize(this->kNarm);
  fPedPosRMS.resize(this->kNarm);

  if (fArmEnable[lvl0->kArmIndex]) {
    const string armtag = lvl0->kArmIndex == Arm1Params::kArmIndex ? "a1" : "a2";

    UT::AllocateVector2D(fPedCalMean[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNrange);
    UT::AllocateVector2D(fPedCalRMS[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNrange);
    UT::AllocateVector4D(fPedPosMean[lvl0->kArmIndex], lvl0->kPosNtower, lvl0->kPosNlayer, lvl0->kPosNview,
                         lvl0->kPosNsample);
    UT::AllocateVector4D(fPedPosRMS[lvl0->kArmIndex], lvl0->kPosNtower, lvl0->kPosNlayer, lvl0->kPosNview,
                         lvl0->kPosNsample);

    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        string name, title;
        name = Form("%s_raw_mean_calped_%d_%d", armtag.c_str(), it, ir);
        title = Form("Tower %d Range %d; Layer; <#mu> [ADC]", it, ir);
        fPedCalMean[lvl0->kArmIndex][it][ir] =
            new TH1F(name.c_str(), title.c_str(), lvl0->kCalNlayer, 0, lvl0->kCalNlayer);
        name = Form("%s_raw_rms_calped_%d_%d", armtag.c_str(), it, ir);
        title = Form("Tower %d Range %d; Layer; <#sigma> [ADC]", it, ir);
        fPedCalRMS[lvl0->kArmIndex][it][ir] =
            new TH1F(name.c_str(), title.c_str(), lvl0->kCalNlayer, 0, lvl0->kCalNlayer);
      }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
            string name, title;
            name = Form("%s_raw_mean_posped_%d_%d_%d_%d", armtag.c_str(), it, il, ixy, is);
            title = Form("Tower %d Layer %d View %d Sample %d; Layer; <#mu> [ADC]", it, il, ixy, is);
            fPedPosMean[lvl0->kArmIndex][it][il][ixy][is] =
                new TH1F(name.c_str(), title.c_str(), lvl0->kPosNchannel[it], 0, lvl0->kPosNchannel[it]);
            name = Form("%s_raw_rms_posped_%d_%d_%d_%d", armtag.c_str(), it, il, ixy, is);
            title = Form("Tower %d Layer %d View %d Sample %d; Layer; <#sigma> [ADC]", it, il, ixy, is);
            fPedPosRMS[lvl0->kArmIndex][it][il][ixy][is] =
                new TH1F(name.c_str(), title.c_str(), lvl0->kPosNchannel[it], 0, lvl0->kPosNchannel[it]);
          }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-------------------------------------*/
/*--- Initialize Pedestal Histogram ---*/
/*-------------------------------------*/
template <typename armclass>
void LHCfPedestal::InitPedestalHistogram(Level0<armclass> *lvl0) {
  UT::Printf(UT::kPrintInfo, "Building pedestal histogram...");
  fflush(stdout);

  fPedCal.resize(this->kNarm);
  fPedCalDel.resize(this->kNarm);
  fPedCalOffset.resize(this->kNarm);
  fPedPos.resize(this->kNarm);

  if (fArmEnable[lvl0->kArmIndex]) {
    const string armtag = lvl0->kArmIndex == Arm1Params::kArmIndex ? "a1" : "a2";

    UT::AllocateVector3D(fPedCal[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fPedCalDel[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fPedCalOffset[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector1D(fPedPos[lvl0->kArmIndex], lvl0->kPosNtower);
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      UT::AllocateVector4D(fPedPos[lvl0->kArmIndex][it], lvl0->kPosNlayer, lvl0->kPosNview, lvl0->kPosNchannel[it],
                           lvl0->kPosNsample);

    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          int xbin;
          double xmin, xmax;
          string name, title;
          name = Form("%s_calped_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal [ADC]", (it == 0 ? "ST" : "LT"), il, (ir == 0 ? "NRADC" : "WRADC"));
          fPedCal[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caldelped_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fPedCalDel[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caloffped_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal - delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fPedCalOffset[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), offbin[ir], offmin[ir], offmax[ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              string name = Form("%s_posped_%d_%d_%d_%d_%d", armtag.c_str(), it, il, ixy, ic, is);
              string title =
                  Form("%s %d - Layer %d%s - Channel %d; signal [ADC]", (this->kNarm == 0 ? "Tower" : "Sample"),
                       (this->kNarm == 0 ? it : is), il, (ixy == 0 ? "x" : "y"), ic);
              if (lvl0->kArmIndex == Arm1Params::kArmIndex)
                fPedPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                    new TH1F(name.c_str(), title.c_str(), barbin, barmin, barmax);
              else if (lvl0->kArmIndex == Arm2Params::kArmIndex) {
                if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 ||
                    Arm2Params::fOperation == kLHC2022 || Arm2Params::fOperation == kLHC2025)
                  fPedPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), lisbin, lismin, lismax);
                else
                  fPedPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), silbin, silmin, silmax);
              }
            }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*--------------------------------------------------*/
/*--- Initialize Contaminated Pedestal Histogram ---*/
/*--------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::InitContaminHistogram(Level0<armclass> *lvl0) {
  UT::Printf(UT::kPrintInfo, "Building contaminated pedestal histogram...");
  fflush(stdout);

  fBkgCal.resize(this->kNarm);
  fBkgCalDel.resize(this->kNarm);
  fBkgCalOffset.resize(this->kNarm);
  fBkgPos.resize(this->kNarm);

  if (fArmEnable[lvl0->kArmIndex]) {
    const string armtag = lvl0->kArmIndex == Arm1Params::kArmIndex ? "a1" : "a2";

    UT::AllocateVector3D(fBkgCal[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fBkgCalDel[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fBkgCalOffset[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector1D(fBkgPos[lvl0->kArmIndex], lvl0->kPosNtower);
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      UT::AllocateVector4D(fBkgPos[lvl0->kArmIndex][it], lvl0->kPosNlayer, lvl0->kPosNview, lvl0->kPosNchannel[it],
                           lvl0->kPosNsample);

    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          int xbin;
          double xmin, xmax;
          string name, title;
          name = Form("%s_calbkg_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal [ADC]", (it == 0 ? "ST" : "LT"), il, (ir == 0 ? "NRADC" : "WRADC"));
          fBkgCal[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caldelbkg_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fBkgCalDel[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caloffbkg_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal - delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fBkgCalOffset[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), offbin[ir], offmin[ir], offmax[ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              string name = Form("%s_posbkg_%d_%d_%d_%d_%d", armtag.c_str(), it, il, ixy, ic, is);
              string title =
                  Form("%s %d - Layer %d%s - Channel %d; signal [ADC]", (this->kNarm == 0 ? "Tower" : "Sample"),
                       (this->kNarm == 0 ? it : is), il, (ixy == 0 ? "x" : "y"), ic);
              if (lvl0->kArmIndex == Arm1Params::kArmIndex)
                fBkgPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                    new TH1F(name.c_str(), title.c_str(), barbin, barmin, barmax);
              else if (lvl0->kArmIndex == Arm2Params::kArmIndex) {
                if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 ||
                    Arm2Params::fOperation == kLHC2022 || Arm2Params::fOperation == kLHC2025)
                  fBkgPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), lisbin, lismin, lismax);
                else
                  fBkgPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), silbin, silmin, silmax);
              }
            }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*-----------------------------------------------*/
/*--- Initialize L2T without Shower Histogram ---*/
/*-----------------------------------------------*/
template <typename armclass>
void LHCfPedestal::InitNoShowerHistogram(Level0<armclass> *lvl0) {
  UT::Printf(UT::kPrintInfo, "Building L2T without shower histogram...");
  fflush(stdout);

  fTrgCal.resize(this->kNarm);
  fTrgCalDel.resize(this->kNarm);
  fTrgCalOffset.resize(this->kNarm);
  fTrgPos.resize(this->kNarm);

  if (fArmEnable[lvl0->kArmIndex]) {
    const string armtag = lvl0->kArmIndex == Arm1Params::kArmIndex ? "a1" : "a2";

    UT::AllocateVector3D(fTrgCal[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fTrgCalDel[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fTrgCalOffset[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector1D(fTrgPos[lvl0->kArmIndex], lvl0->kPosNtower);
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      UT::AllocateVector4D(fTrgPos[lvl0->kArmIndex][it], lvl0->kPosNlayer, lvl0->kPosNview, lvl0->kPosNchannel[it],
                           lvl0->kPosNsample);

    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          int xbin;
          double xmin, xmax;
          string name, title;
          name = Form("%s_caltrg_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal [ADC]", (it == 0 ? "ST" : "LT"), il, (ir == 0 ? "NRADC" : "WRADC"));
          fTrgCal[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caldeltrg_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fTrgCalDel[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_calofftrg_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal - delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fTrgCalOffset[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), offbin[ir], offmin[ir], offmax[ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              string name = Form("%s_postrg_%d_%d_%d_%d_%d", armtag.c_str(), it, il, ixy, ic, is);
              string title =
                  Form("%s %d - Layer %d%s - Channel %d; signal [ADC]", (this->kNarm == 0 ? "Tower" : "Sample"),
                       (this->kNarm == 0 ? it : is), il, (ixy == 0 ? "x" : "y"), ic);
              if (lvl0->kArmIndex == Arm1Params::kArmIndex)
                fTrgPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                    new TH1F(name.c_str(), title.c_str(), barbin, barmin, barmax);
              else if (lvl0->kArmIndex == Arm2Params::kArmIndex) {
                if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 ||
                    Arm2Params::fOperation == kLHC2022 || Arm2Params::fOperation == kLHC2025)
                  fTrgPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), lisbin, lismin, lismax);
                else
                  fTrgPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), silbin, silmin, silmax);
              }
            }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*------------------------------------------------------------*/
/*--- Initialize Contaminated L2T without Shower Histogram ---*/
/*------------------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::InitNimatnocHistogram(Level0<armclass> *lvl0) {
  UT::Printf(UT::kPrintInfo, "Building contaminated L2T without shower histogram...");
  fflush(stdout);

  fGrtCal.resize(this->kNarm);
  fGrtCalDel.resize(this->kNarm);
  fGrtCalOffset.resize(this->kNarm);
  fGrtPos.resize(this->kNarm);

  if (fArmEnable[lvl0->kArmIndex]) {
    const string armtag = lvl0->kArmIndex == Arm1Params::kArmIndex ? "a1" : "a2";

    UT::AllocateVector3D(fGrtCal[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fGrtCalDel[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector3D(fGrtCalOffset[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
    UT::AllocateVector1D(fGrtPos[lvl0->kArmIndex], lvl0->kPosNtower);
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      UT::AllocateVector4D(fGrtPos[lvl0->kArmIndex][it], lvl0->kPosNlayer, lvl0->kPosNview, lvl0->kPosNchannel[it],
                           lvl0->kPosNsample);

    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          int xbin;
          double xmin, xmax;
          string name, title;
          name = Form("%s_calgrt_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal [ADC]", (it == 0 ? "ST" : "LT"), il, (ir == 0 ? "NRADC" : "WRADC"));
          fGrtCal[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caldelgrt_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fGrtCalDel[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), calbin[ir], calmin[ir], calmax[ir]);
          name = Form("%s_caloffgrt_%d_%d_%d", armtag.c_str(), it, il, ir);
          title = Form("%s - Layer %d - %s; signal - delayed-gate [ADC]", (it == 0 ? "ST" : "LT"), il,
                       (ir == 0 ? "NRADC" : "WRADC"));
          fGrtCalOffset[lvl0->kArmIndex][it][il][ir] =
              new TH1F(name.c_str(), title.c_str(), offbin[ir], offmin[ir], offmax[ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              string name = Form("%s_posgrt_%d_%d_%d_%d_%d", armtag.c_str(), it, il, ixy, ic, is);
              string title =
                  Form("%s %d - Layer %d%s - Channel %d; signal [ADC]", (this->kNarm == 0 ? "Tower" : "Sample"),
                       (this->kNarm == 0 ? it : is), il, (ixy == 0 ? "x" : "y"), ic);
              if (lvl0->kArmIndex == Arm1Params::kArmIndex)
                fGrtPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                    new TH1F(name.c_str(), title.c_str(), barbin, barmin, barmax);
              else if (lvl0->kArmIndex == Arm2Params::kArmIndex) {
                if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 ||
                    Arm2Params::fOperation == kLHC2022 || Arm2Params::fOperation == kLHC2025)
                  fGrtPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), lisbin, lismin, lismax);
                else
                  fGrtPos[lvl0->kArmIndex][it][il][ixy][ic][is] =
                      new TH1F(name.c_str(), title.c_str(), silbin, silmin, silmax);
              }
            }
  }

  UT::Printf(UT::kPrintInfo, " Done.\n");
}

/*----------------------------------------------*/
/*------ Fill Pedestal Mean/RMS Histogram ------*/
/*----------------------------------------------*/
template <typename armclass, typename armcal>
void LHCfPedestal::FillMeanRMSHistogram(EventCal<armclass, armcal> *evcal) {
  for (Int_t it = 0; it < evcal->kCalNtower; ++it)
    for (Int_t il = 0; il < evcal->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < evcal->kCalNrange; ++ir) {
        fPedCalMean[evcal->kArmIndex][it][ir]->Fill(il, evcal->GetPedCalMean(it, il, ir));
        fPedCalRMS[evcal->kArmIndex][it][ir]->Fill(il, evcal->GetPedCalRms(it, il, ir));
      }
  for (Int_t it = 0; it < evcal->kPosNtower; ++it)
    for (Int_t il = 0; il < evcal->kPosNlayer; ++il)
      for (Int_t ixy = 0; ixy < evcal->kPosNview; ++ixy)
        for (Int_t ic = 0; ic < evcal->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < evcal->kPosNsample; ++is) {
            fPedPosMean[evcal->kArmIndex][it][il][ixy][is]->Fill(ic, evcal->GetPedPosMean(it, il, ixy, ic, is));
            fPedPosRMS[evcal->kArmIndex][it][il][ixy][is]->Fill(ic, evcal->GetPedPosRms(it, il, ixy, ic, is));
          }
}

/*-------------------------------------*/
/*------ Fill Pedestal Histogram ------*/
/*-------------------------------------*/
template <typename armclass>
void LHCfPedestal::FillPedestalHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        fPedCal[lvl0->kArmIndex][it][il][ir]->Fill(lvl0->fCalorimeter[it][il][ir]);
        fPedCalDel[lvl0->kArmIndex][it][il][ir]->Fill(ped0->fCalorimeter[it][il][ir]);
        fPedCalOffset[lvl0->kArmIndex][it][il][ir]->Fill(lvl0->fCalorimeter[it][il][ir] -
                                                         ped0->fCalorimeter[it][il][ir]);
      }
  for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
    for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
      for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
        for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
            fPedPos[lvl0->kArmIndex][it][il][ixy][ic][is]->Fill(lvl0->fPosDet[it][il][ixy][ic][is]);
          }
}

/*--------------------------------------------------*/
/*------ Fill Contaminated Pedestal Histogram ------*/
/*--------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::FillContaminHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        FillSpecial(fBkgCal[lvl0->kArmIndex][it][il][ir], lvl0->fCalorimeter[it][il][ir]);
        FillSpecial(fBkgCalDel[lvl0->kArmIndex][it][il][ir], ped0->fCalorimeter[it][il][ir]);
        FillSpecial(fBkgCalOffset[lvl0->kArmIndex][it][il][ir],
                    lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir]);
      }
  for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
    for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
      for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
        for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
            FillSpecial(fBkgPos[lvl0->kArmIndex][it][il][ixy][ic][is], lvl0->fPosDet[it][il][ixy][ic][is]);
          }
}

/*-----------------------------------------------*/
/*------ Fill L2T without Shower Histogram ------*/
/*-----------------------------------------------*/
template <typename armclass>
void LHCfPedestal::FillNoShowerHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        fTrgCal[lvl0->kArmIndex][it][il][ir]->Fill(lvl0->fCalorimeter[it][il][ir]);
        fTrgCalDel[lvl0->kArmIndex][it][il][ir]->Fill(ped0->fCalorimeter[it][il][ir]);
        fTrgCalOffset[lvl0->kArmIndex][it][il][ir]->Fill(lvl0->fCalorimeter[it][il][ir] -
                                                         ped0->fCalorimeter[it][il][ir]);
      }
  for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
    for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
      for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
        for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < lvl0->kPosNsample; ++is)
            fTrgPos[lvl0->kArmIndex][it][il][ixy][ic][is]->Fill(lvl0->fPosDet[it][il][ixy][ic][is]);
}

/*-----------------------------------------------*/
/*------ Fill L2T without Shower Histogram ------*/
/*-----------------------------------------------*/
template <typename armclass>
void LHCfPedestal::FillNimatnocHistogram(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        fGrtCal[lvl0->kArmIndex][it][il][ir]->Fill(lvl0->fCalorimeter[it][il][ir]);
        fGrtCalDel[lvl0->kArmIndex][it][il][ir]->Fill(ped0->fCalorimeter[it][il][ir]);
        fGrtCalOffset[lvl0->kArmIndex][it][il][ir]->Fill(lvl0->fCalorimeter[it][il][ir] -
                                                         ped0->fCalorimeter[it][il][ir]);
      }
  for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
    for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
      for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
        for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < lvl0->kPosNsample; ++is)
            fGrtPos[lvl0->kArmIndex][it][il][ixy][ic][is]->Fill(lvl0->fPosDet[it][il][ixy][ic][is]);
}

/*------------------------------------*/
/*----- Write Mean-RMS Histogram -----*/
/*------------------------------------*/
template <typename armclass>
void LHCfPedestal::WriteMeanRMSHistogram(Level0<armclass> *lvl0) {
  if (fArmEnable[lvl0->kArmIndex]) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        io_class->WriteObject(fPedCalMean[lvl0->kArmIndex][it][ir]);
        io_class->WriteObject(fPedCalRMS[lvl0->kArmIndex][it][ir]);
      }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
            io_class->WriteObject(fPedPosMean[lvl0->kArmIndex][it][il][ixy][is]);
            io_class->WriteObject(fPedPosRMS[lvl0->kArmIndex][it][il][ixy][is]);
          }
  }
}

/*------------------------------------*/
/*----- Write Pedestal Histogram -----*/
/*------------------------------------*/
template <typename armclass>
void LHCfPedestal::WritePedestalHistogram(Level0<armclass> *lvl0) {
  if (fArmEnable[lvl0->kArmIndex]) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          io_class->WriteObject(fPedCal[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fPedCalDel[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fPedCalOffset[lvl0->kArmIndex][it][il][ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              io_class->WriteObject(fPedPos[lvl0->kArmIndex][it][il][ixy][ic][is]);
            }
  }
}

/*-------------------------------------------------*/
/*----- Write Contaminated Pedestal Histogram -----*/
/*-------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::WriteContaminHistogram(Level0<armclass> *lvl0) {
  if (fArmEnable[lvl0->kArmIndex]) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          io_class->WriteObject(fBkgCal[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fBkgCalDel[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fBkgCalOffset[lvl0->kArmIndex][it][il][ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              io_class->WriteObject(fBkgPos[lvl0->kArmIndex][it][il][ixy][ic][is]);
            }
  }
}

/*----------------------------------------------*/
/*----- Write L2T without Shower Histogram -----*/
/*----------------------------------------------*/
template <typename armclass>
void LHCfPedestal::WriteNoShowerHistogram(Level0<armclass> *lvl0) {
  if (fArmEnable[lvl0->kArmIndex]) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          io_class->WriteObject(fTrgCal[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fTrgCalDel[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fTrgCalOffset[lvl0->kArmIndex][it][il][ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              io_class->WriteObject(fTrgPos[lvl0->kArmIndex][it][il][ixy][ic][is]);
            }
  }
}

/*----------------------------------------------*/
/*----- Write L2T without Shower Histogram -----*/
/*----------------------------------------------*/
template <typename armclass>
void LHCfPedestal::WriteNimatnocHistogram(Level0<armclass> *lvl0) {
  if (fArmEnable[lvl0->kArmIndex]) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          io_class->WriteObject(fGrtCal[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fGrtCalDel[lvl0->kArmIndex][it][il][ir]);
          io_class->WriteObject(fGrtCalOffset[lvl0->kArmIndex][it][il][ir]);
        }
    for (Int_t it = 0; it < lvl0->kPosNtower; ++it)
      for (Int_t il = 0; il < lvl0->kPosNlayer; ++il)
        for (Int_t ixy = 0; ixy < lvl0->kPosNview; ++ixy)
          for (Int_t ic = 0; ic < lvl0->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < lvl0->kPosNsample; ++is) {
              io_class->WriteObject(fGrtPos[lvl0->kArmIndex][it][il][ixy][ic][is]);
            }
  }
}

/*-------------------------------*/
/*----- Initizialize vector -----*/
/*-------------------------------*/
template <typename armclass>
void LHCfPedestal::InitVector(Level0<armclass> *lvl0) {
  UT::AllocateVector4D(aCalPedVec[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange, 0);
  UT::AllocateVector3D(aCalPedAve[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
  UT::AllocateVector3D(aCalPedRMS[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);

  UT::AllocateVector4D(aCalNosVec[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange, 0);
  UT::AllocateVector3D(aCalNosAve[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
  UT::AllocateVector3D(aCalNosRMS[lvl0->kArmIndex], lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
}

/*--------------------------------*/
/*----- Fill pedestal vector -----*/
/*--------------------------------*/
template <typename armclass>
void LHCfPedestal::FillPedestalVector(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        if (fAveragePedestal) {  // Pedestals are defined as signal only
          aCalPedVec[lvl0->kArmIndex][it][il][ir].push_back(lvl0->fCalorimeter[it][il][ir]);
        } else {  // Pedestals are defined as signal-delayed gate
          aCalPedVec[lvl0->kArmIndex][it][il][ir].push_back(lvl0->fCalorimeter[it][il][ir] -
                                                            ped0->fCalorimeter[it][il][ir]);
        }
      }
}

/*--------------------------------*/
/*----- Fill no-shower vector -----*/
/*--------------------------------*/
template <typename armclass>
void LHCfPedestal::FillNoShowerVector(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        if (fAveragePedestal) {  // Pedestals are defined as signal only
          aCalNosVec[lvl0->kArmIndex][it][il][ir].push_back(lvl0->fCalorimeter[it][il][ir]);
        } else {  // Pedestals are defined as signal-delayed gate
          aCalNosVec[lvl0->kArmIndex][it][il][ir].push_back(lvl0->fCalorimeter[it][il][ir] -
                                                            ped0->fCalorimeter[it][il][ir]);
        }
      }
}

/*-------------------------------------------------*/
/*----- Compute initial pedestal mean and RMS -----*/
/*-------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::ComputeInitialPedestalMeanAndRMS(Level0<armclass> *lvl0) {
  if (lvl0->kArmIndex == Arm1Params::kArmIndex)
    UT::Printf(UT::kPrintInfo, "\nPedestal Iteration - Arm1\n");
  else if (lvl0->kArmIndex == Arm2Params::kArmIndex)
    UT::Printf(UT::kPrintInfo, "\nPedestal Iteration - Arm2\n");

  for (int iter = 0; iter < fNiters[lvl0->kArmIndex]; ++iter) {
    UT::Printf(UT::kPrintInfo, "Iteration %d\n", iter + 1);
    if (iter > 0) CleanPedestalVector(lvl0);
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          UT::GetMeanRMS(aCalPedVec[lvl0->kArmIndex][it][il][ir], aCalPedAve[lvl0->kArmIndex][it][il][ir],
                         aCalPedRMS[lvl0->kArmIndex][it][il][ir]);
        }
  }
}

/*-------------------------------------------------*/
/*----- Compute initial no-shower mean and RMS -----*/
/*-------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::ComputeInitialNoShowerMeanAndRMS(Level0<armclass> *lvl0) {
  if (lvl0->kArmIndex == Arm1Params::kArmIndex)
    UT::Printf(UT::kPrintInfo, "\nNoShower Iteration - Arm1\n");
  else if (lvl0->kArmIndex == Arm2Params::kArmIndex)
    UT::Printf(UT::kPrintInfo, "\nNoShower Iteration - Arm2\n");

  for (int iter = 0; iter < fNiters[lvl0->kArmIndex]; ++iter) {
    UT::Printf(UT::kPrintInfo, "Iteration %d\n", iter + 1);
    if (iter > 0) CleanNoShowerVector(lvl0);
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir)
          UT::GetMeanRMS(aCalNosVec[lvl0->kArmIndex][it][il][ir], aCalNosAve[lvl0->kArmIndex][it][il][ir],
                         aCalNosRMS[lvl0->kArmIndex][it][il][ir]);
  }
}

/*-----------------------------------------------------*/
/*----- Remove contaminated pedestals from vector -----*/
/*-----------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::CleanPedestalVector(Level0<armclass> *lvl0) {
  vector<Int_t> removeMe = vector<Int_t>();
  const int npedestal = aCalPedVec[lvl0->kArmIndex][0][0][0].size();
  for (Int_t ip = 0; ip < npedestal; ++ip) {
    bool good = true;
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it) {
      if (!good) break;
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
        if (!good) break;
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          if (!good) break;
          if (TMath::Abs(aCalPedVec[lvl0->kArmIndex][it][il][ir][ip] - aCalPedAve[lvl0->kArmIndex][it][il][ir]) >
              fNsigma[lvl0->kArmIndex] * aCalPedRMS[lvl0->kArmIndex][it][il][ir]) {
            removeMe.push_back(ip);
            good = false;
          }
        }
      }
    }
  }
  for (Int_t ip = (Int_t)removeMe.size(); ip > 0; --ip) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir)
          aCalPedVec[lvl0->kArmIndex][it][il][ir].erase(aCalPedVec[lvl0->kArmIndex][it][il][ir].begin() +
                                                        removeMe[ip - 1]);
  }
}

/*-----------------------------------------------------*/
/*----- Remove contaminated no-shower from vector -----*/
/*-----------------------------------------------------*/
template <typename armclass>
void LHCfPedestal::CleanNoShowerVector(Level0<armclass> *lvl0) {
  vector<Int_t> removeMe = vector<Int_t>();
  const int npedestal = aCalNosVec[lvl0->kArmIndex][0][0][0].size();
  for (Int_t ip = 0; ip < npedestal; ++ip) {
    bool good = true;
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it) {
      if (!good) break;
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
        if (!good) break;
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          if (!good) break;
          if (TMath::Abs(aCalNosVec[lvl0->kArmIndex][it][il][ir][ip] - aCalNosAve[lvl0->kArmIndex][it][il][ir]) >
              fNsigma[lvl0->kArmIndex] * aCalNosRMS[lvl0->kArmIndex][it][il][ir]) {
            removeMe.push_back(ip);
            good = false;
          }
        }
      }
    }
  }
  for (Int_t ip = (Int_t)removeMe.size(); ip > 0; --ip) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir)
          aCalNosVec[lvl0->kArmIndex][it][il][ir].erase(aCalNosVec[lvl0->kArmIndex][it][il][ir].begin() +
                                                        removeMe[ip - 1]);
  }
}

/*---------------------------------------------*/
/*----- Check if pedestal is contaminated -----*/
/*---------------------------------------------*/
template <typename armclass>
Bool_t LHCfPedestal::isGoodPedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  Bool_t good = true;
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it) {
    if (!good) break;
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
      if (!good) break;
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        if (!good) break;
        Double_t signal = 0.;
        if (fAveragePedestal) {  // Pedestals are defined as signal only
          signal = lvl0->fCalorimeter[it][il][ir];
        } else {  // Pedestals are defined as signal-delayed gate
          signal = lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir];
        }
        if (TMath::Abs(signal - aCalPedAve[lvl0->kArmIndex][it][il][ir]) >
            fNsigma[lvl0->kArmIndex] * aCalPedRMS[lvl0->kArmIndex][it][il][ir]) {
          good = false;
          break;
        }
      }
    }
  }
  return good;
}

/*----------------------------------------------*/
/*----- Check if no-shower is contaminated -----*/
/*----------------------------------------------*/
template <typename armclass>
Bool_t LHCfPedestal::isGoodNoShower(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  bool good = true;
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it) {
    if (!good) break;
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
      if (!good) break;
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        if (!good) break;
        Double_t signal = 0.;
        if (fAveragePedestal) {  // Pedestals are defined as signal only
          signal = lvl0->fCalorimeter[it][il][ir];
        } else {  // Pedestals are defined as signal-delayed gate
          signal = lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir];
        }
        if (TMath::Abs(signal - aCalNosAve[lvl0->kArmIndex][it][il][ir]) >
            fNsigma[lvl0->kArmIndex] * aCalNosRMS[lvl0->kArmIndex][it][il][ir]) {
          good = false;
          break;
        }
      }
    }
  }
  return good;
}

/*---------------------------------*/
/*--- Main calibration function ---*/
/*---------------------------------*/
void LHCfPedestal::PedestalRun(Int_t first_ev, Int_t last_ev) {
  /* Initialization */
  io_class = new InOutCal(fInputName, fOutputName, fSaveHistogram);

  EventCal<Arm1Params, Arm1CalPars> ev_cal_a1;
  EventCal<Arm2Params, Arm2CalPars> ev_cal_a2;
  Int_t a1_entries = -1;
  Int_t a2_entries = -1;
  if (fArmEnable[Arm1Params::kArmIndex]) {
    ev_cal_a1.InitialiseWithoutTables();
    io_class->SetInputTree(&ev_cal_a1);
    a1_entries = io_class->GetEntries(Arm1Params::kArmIndex);
    InitPedestalTree(&fPed0_Arm1);
    if (fSaveHistogram) {
      InitMeanRMSHistogram(&fPed0_Arm1);
      InitPedestalHistogram(&fPed0_Arm1);
      InitContaminHistogram(&fPed0_Arm1);
      InitNoShowerHistogram(&fPed0_Arm1);
      InitNimatnocHistogram(&fPed0_Arm1);
    }
  }
  if (fArmEnable[Arm2Params::kArmIndex]) {
    ev_cal_a2.InitialiseWithoutTables();
    io_class->SetInputTree(&ev_cal_a2);
    a2_entries = io_class->GetEntries(Arm2Params::kArmIndex);
    InitPedestalTree(&fPed0_Arm2);
    if (fSaveHistogram) {
      InitMeanRMSHistogram(&fPed0_Arm2);
      InitPedestalHistogram(&fPed0_Arm2);
      InitContaminHistogram(&fPed0_Arm2);
      InitNoShowerHistogram(&fPed0_Arm2);
      InitNimatnocHistogram(&fPed0_Arm2);
    }
  }
  /* Check if input is DATA or MC */
  if (fArmEnable[Arm1Params::kArmIndex] && fArmEnable[Arm2Params::kArmIndex]) {
    if (ev_cal_a1.fIsMC != ev_cal_a2.fIsMC) {
      UT::Printf(UT::kPrintError, "Error: different fIsMC between Arm1 and Arm2 (that should never happens...)\n");
      exit(EXIT_FAILURE);
    }
    fIsMC = ev_cal_a1.fIsMC;
  } else if (fArmEnable[Arm1Params::kArmIndex]) {
    fIsMC = ev_cal_a1.fIsMC;
  } else if (fArmEnable[Arm2Params::kArmIndex]) {
    fIsMC = ev_cal_a2.fIsMC;
  }

  if (fSaveHistogram && fIsMC) UT::Printf(UT::kPrintError, "Warning: Cannot fill pedestal histogram for MC\n");

  /* Get number of entries in input tree */
  Int_t tot_entries = 0;
  if (a1_entries > 0 && a2_entries > 0) {
    tot_entries = TMath::Max(a1_entries, a2_entries);
    if (a1_entries != a2_entries)
      UT::Printf(UT::kPrintError, "Warning: Nev(Arm1) != Nev(Arm2) (%d != %d)\n", a1_entries, a2_entries);
  } else if (a1_entries > 0) {
    tot_entries = a1_entries;
  } else if (a2_entries > 0) {
    tot_entries = a2_entries;
  } else {
    UT::Printf(UT::kPrintError, "No entries found in input trees!\n");
    exit(EXIT_FAILURE);
  }

  first_ev = first_ev >= 0 ? first_ev : 0;
  last_ev = last_ev >= 0 ? TMath::Min(last_ev, tot_entries - 1) : tot_entries - 1;
  if (fRealignSilicon) --last_ev;
  UT::Printf(UT::kPrintInfo, "Total events: %d\n", tot_entries);
  Int_t step_prnt = 10;
  if (last_ev - first_ev + 1 > 100000)
    step_prnt = 10000;
  else if (last_ev - first_ev + 1 > 10000)
    step_prnt = 1000;
  else if (last_ev - first_ev + 1 > 1000)
    step_prnt = 100;
  else if (last_ev - first_ev + 1 > 100)
    step_prnt = 10;
  else
    step_prnt = 1;

  /*--- Fill calorimeter pedestal for initial value computation ---*/

  UT::Printf(UT::kPrintInfo, "Fill calorimeter vector for initial pedestal estimation\n");

  if (fArmEnable[Arm2Params::kArmIndex]) ev_cal_a2.SetSiliconEvent(first_ev);

  if (fArmEnable[Arm1Params::kArmIndex]) {
    fNiters[fLvl0_Arm1.kArmIndex] = Arm1CalPars::nPedSubIters;
    fNsigma[fLvl0_Arm1.kArmIndex] = Arm1CalPars::nPedSubSigma;
    InitVector(&fLvl0_Arm1);
  }
  if (fArmEnable[Arm2Params::kArmIndex]) {
    fNiters[fLvl0_Arm2.kArmIndex] = Arm2CalPars::nPedSubIters;
    fNsigma[fLvl0_Arm2.kArmIndex] = Arm2CalPars::nPedSubSigma;
    InitVector(&fLvl0_Arm2);
  }

  for (Int_t ie = first_ev; ie <= last_ev; ++ie) {
    if (ie % step_prnt == 0 || ie == last_ev) {
      UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    if (fArmEnable[Arm1Params::kArmIndex]) {
      if (io_class->GetEntry(Arm1Params::kArmIndex, ie)) {  // False if last Arm1 event is reached
        UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);

        if (!fIsMC || (io_class->GetLevel0<Arm1Params>() && io_class->GetPedestal0<Arm1Params>()))
          ev_cal_a1.EventToLevel0(&fLvl0_Arm1, &fPed0_Arm1);
        if (fLvl0_Arm1.IsPedestal())  // Pedestal event
          FillPedestalVector(&fLvl0_Arm1, &fPed0_Arm1);
        if (fLvl0_Arm1.IsBeam() && fLvl0_Arm1.fFlag[1] == 0)  // Beam event without triggered discriminator
          FillNoShowerVector(&fLvl0_Arm1, &fPed0_Arm1);
      }
    }

    if (fArmEnable[Arm2Params::kArmIndex]) {
      if (io_class->GetEntry(Arm2Params::kArmIndex, ie)) {  // False if last Arm2 event is reached
        UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
        // This asymmetry is necessary in Arm2 in case silicon realignment is required
        Bool_t valid_entry = true;
        if (fRealignSilicon) valid_entry = io_class->RealignSilicon(Arm2Params::kArmIndex, ie + 1, &ev_cal_a2);
        if (valid_entry) {
          if (!fIsMC || (io_class->GetLevel0<Arm2Params>() && io_class->GetPedestal0<Arm2Params>()))
            ev_cal_a2.EventToLevel0(&fLvl0_Arm2, &fPed0_Arm2);
          if (fLvl0_Arm2.IsPedestal())  // Pedestal event
            FillPedestalVector(&fLvl0_Arm2, &fPed0_Arm2);
          if (fLvl0_Arm2.IsBeam() && fLvl0_Arm2.fFlag[1] == 0)  // Beam event without triggered discriminator
            FillNoShowerVector(&fLvl0_Arm2, &fPed0_Arm2);
        }
      }
    }

    io_class->ClearEvent();
  }

  /*--- Iteratively compute initial pedestal and mean value of calorimeter pedestals ---*/

  UT::Printf(UT::kPrintInfo, "\nIteratively compute initial value of mean and rms pedestal\n");

  if (fArmEnable[Arm1Params::kArmIndex]) {
    ComputeInitialPedestalMeanAndRMS(&fLvl0_Arm1);
    ComputeInitialNoShowerMeanAndRMS(&fLvl0_Arm1);
  }
  if (fArmEnable[Arm2Params::kArmIndex]) {
    ComputeInitialPedestalMeanAndRMS(&fLvl0_Arm2);
    ComputeInitialNoShowerMeanAndRMS(&fLvl0_Arm2);
  }

  /*--- Loop for final pedestal calculation ---*/

  UT::Printf(UT::kPrintInfo, "Start of pedestal mean and rms calculation\n");

  if (fArmEnable[Arm2Params::kArmIndex]) ev_cal_a2.SetSiliconEvent(first_ev);

  for (Int_t ie = first_ev; ie <= last_ev; ++ie) {
    if (ie % step_prnt == 0 || ie == last_ev) {
      UT::Printf(UT::kPrintInfo, "\r\tevent %d", ie);
      fflush(stdout);
    }

    /* Arm1 pedestal */
    if (fArmEnable[Arm1Params::kArmIndex]) {
      UT::Printf(UT::kPrintDebugFull, "Arm1: event %d\n", ie);
      io_class->GetEntry(Arm1Params::kArmIndex, ie);
      if (!fIsMC || (io_class->GetLevel0<Arm1Params>() && io_class->GetPedestal0<Arm1Params>()))
        ev_cal_a1.EventToLevel0(&fLvl0_Arm1, &fPed0_Arm1);
      if (fLvl0_Arm1.IsPedestal()) {                     // Pedestal event
        if (isGoodPedestal(&fLvl0_Arm1, &fPed0_Arm1)) {  // Pedestal event
          ev_cal_a1.SavePedestal(&fLvl0_Arm1, &fPed0_Arm1);
          FillPedestalTree(&fLvl0_Arm1, &fPed0_Arm1);
        }
        if (fSaveHistogram) {
          if (isGoodPedestal(&fLvl0_Arm1, &fPed0_Arm1))
            FillPedestalHistogram(&fLvl0_Arm1, &fPed0_Arm1);
          else
            FillContaminHistogram(&fLvl0_Arm1, &fPed0_Arm1);
        }
      } else if (fLvl0_Arm1.IsBeam() && fLvl0_Arm1.fFlag[1] == 0) {  // Beam event without triggered discriminator
        if (fSaveHistogram) {
          if (isGoodNoShower(&fLvl0_Arm1, &fPed0_Arm1))
            FillNoShowerHistogram(&fLvl0_Arm1, &fPed0_Arm1);
          else
            FillNimatnocHistogram(&fLvl0_Arm1, &fPed0_Arm1);
        }
      }
    }
    /* Arm2 pedestal */
    if (fArmEnable[Arm2Params::kArmIndex]) {
      UT::Printf(UT::kPrintDebugFull, "Arm2: event %d\n", ie);
      io_class->GetEntry(Arm2Params::kArmIndex, ie);
      // This asymmetry is necessary in Arm2 in case silicon realignment is required
      Bool_t valid_entry = true;
      if (fRealignSilicon) valid_entry = io_class->RealignSilicon(Arm2Params::kArmIndex, ie + 1, &ev_cal_a2);
      if (valid_entry) {
        if (!fIsMC || (io_class->GetLevel0<Arm2Params>() && io_class->GetPedestal0<Arm2Params>()))
          ev_cal_a2.EventToLevel0(&fLvl0_Arm2, &fPed0_Arm2);
        if (fLvl0_Arm2.IsPedestal()) {                     // Pedestal event
          if (isGoodPedestal(&fLvl0_Arm2, &fPed0_Arm2)) {  // Pedestal event
            ev_cal_a2.SavePedestal(&fLvl0_Arm2, &fPed0_Arm2);
            FillPedestalTree(&fLvl0_Arm2, &fPed0_Arm2);
          }
          if (fSaveHistogram) {
            if (isGoodPedestal(&fLvl0_Arm2, &fPed0_Arm2))
              FillPedestalHistogram(&fLvl0_Arm2, &fPed0_Arm2);
            else
              FillContaminHistogram(&fLvl0_Arm2, &fPed0_Arm2);
          }
        } else if (fLvl0_Arm2.IsBeam() && fLvl0_Arm2.fFlag[1] == 0) {  // Beam event without triggered discriminator
          if (fSaveHistogram) {
            if (isGoodNoShower(&fLvl0_Arm2, &fPed0_Arm2))
              FillNoShowerHistogram(&fLvl0_Arm2, &fPed0_Arm2);
            else
              FillNimatnocHistogram(&fLvl0_Arm2, &fPed0_Arm2);
          }
        }
      }
    }
    io_class->ClearEvent();
  }

  if (fArmEnable[Arm1Params::kArmIndex]) {
    ev_cal_a1.GetPedestalMeanRms(&fPed0_Arm1);
    WriteShiftTree(&fPed0_Arm1);
    if (fSaveHistogram) {
      FillMeanRMSHistogram(&ev_cal_a1);
      WriteMeanRMSHistogram(&fPed0_Arm1);
      WritePedestalHistogram(&fPed0_Arm1);
      WriteContaminHistogram(&fPed0_Arm1);
      WriteNoShowerHistogram(&fPed0_Arm1);
      WriteNimatnocHistogram(&fPed0_Arm1);
    }
  }

  if (fArmEnable[Arm2Params::kArmIndex]) {
    ev_cal_a2.GetPedestalMeanRms(&fPed0_Arm2);
    WriteShiftTree(&fPed0_Arm2);
    if (fSaveHistogram) {
      FillMeanRMSHistogram(&ev_cal_a2);
      WriteMeanRMSHistogram(&fPed0_Arm2);
      WritePedestalHistogram(&fPed0_Arm2);
      WriteContaminHistogram(&fPed0_Arm2);
      WriteNoShowerHistogram(&fPed0_Arm2);
      WriteNimatnocHistogram(&fPed0_Arm2);
    }
  }

  UT::Printf(UT::kPrintInfo, "\nEnd of pedestal mean and rms calculation\n");

  io_class->WriteToOutput();
  io_class->CloseFiles();
}
