

#include <AdamoDecode.hh>

using namespace nLHCf;

#if !defined(__CINT__)
ClassImp(AdamoPacket);
ClassImp(AdamoDecode);
#endif

#include <math.h>

#include <iomanip>
#include <iostream>
using namespace std;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++                       AdamoPacket                     +++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

const int AdamoPacket::NBYTE;
const int AdamoPacket::NHEADER;
const int AdamoPacket::NBYTE_EC;
const int AdamoPacket::NBYTE_DATA;
const int AdamoPacket::NLAYER;
const int AdamoPacket::NSTRIP;
const int AdamoPacket::NBYTE_CRC;
const int AdamoPacket::NTRAILER;

void AdamoPacket::Clear() {
  for (int i = 0; i < NHEADER; i++) {
    header[i] = 0;
  }
  eventcounter = 0;
  for (int il = 0; il < NLAYER; il++) {
    for (int is = 0; is < NSTRIP; is++) {
      strip[il][is] = 0;
    }
  }
  CRC = 0;
  for (int i = 0; i < NTRAILER; i++) {
    trailer[i] = 0;
  }
}

int AdamoPacket::CheckHeader(int ixy) {
  // if OK, return 1. if not, return 0
  // X side
  if (ixy == 0) {
    if (header[0] == 0xE1 && header[1] == 0xE2 && header[2] == 0xE3 && header[3] == 0xE4 && header[4] == 0xE5 &&
        header[5] == 0xE6) {
      return 1;
    } else {
      return 0;
    }
  }
  // Y side
  else if (ixy == 1) {
    if (header[0] == 0xF1 && header[1] == 0xF2 && header[2] == 0xF3 && header[3] == 0xF4 && header[4] == 0xF5 &&
        header[5] == 0xF6) {
      return 1;
    } else {
      return 0;
    }
  }
  return 0;
}

int AdamoPacket::CheckTrailer(int ixy) {
  // if OK, return 1. if not, return 0
  // X side
  if (ixy == 0) {
    if (trailer[0] == 0xE7 && trailer[1] == 0xE8 && trailer[2] == 0xE9 && trailer[3] == 0xEA) {
      return 1;
    } else {
      return 0;
    }
  }
  // Y side
  else if (ixy == 1) {
    if (trailer[0] == 0xF7 && trailer[1] == 0xF8 && trailer[2] == 0xF9 && trailer[3] == 0xFA) {
      return 1;
    } else {
      return 0;
    }
  }
  return 0;
}

int AdamoPacket::CheckCRC(char *data) {
  unsigned char *datacrc = (unsigned char *)data;
  unsigned char icc = 0;
  for (int i = 0; i < NBYTE_DATA; i++) {
    icc = crc8_8(icc, datacrc[i]);
  }
  if (icc != CRC) {
    cerr << "[AdamoPacket::CheckCRC] Failure " << (int)icc << " : " << (int)CRC << endl;
    return 0;
  }
  return 1;
}

unsigned char AdamoPacket::crc8_8(unsigned char old, unsigned char data) {
  union crc_word {
    unsigned char word;
    struct bit_field_w {
      /* CRC correct */
      unsigned b7 : 1;
      unsigned b6 : 1;
      unsigned b5 : 1;
      unsigned b4 : 1;
      unsigned b3 : 1;
      unsigned b2 : 1;
      unsigned b1 : 1;
      unsigned b0 : 1;
    } bit;
  } c, d, r;

  c.word = old;
  d.word = (unsigned short)data;

  r.bit.b0 = c.bit.b0 ^ c.bit.b6 ^ c.bit.b7 ^ d.bit.b0 ^ d.bit.b6 ^ d.bit.b7;
  r.bit.b1 = c.bit.b0 ^ c.bit.b1 ^ c.bit.b6 ^ d.bit.b0 ^ d.bit.b1 ^ d.bit.b6;
  r.bit.b2 = c.bit.b0 ^ c.bit.b1 ^ c.bit.b2 ^ c.bit.b6 ^ d.bit.b0 ^ d.bit.b1 ^ d.bit.b2 ^ d.bit.b6;
  r.bit.b3 = c.bit.b1 ^ c.bit.b2 ^ c.bit.b3 ^ c.bit.b7 ^ d.bit.b1 ^ d.bit.b2 ^ d.bit.b3 ^ d.bit.b7;
  r.bit.b4 = c.bit.b2 ^ c.bit.b3 ^ c.bit.b4 ^ d.bit.b2 ^ d.bit.b3 ^ d.bit.b4;
  r.bit.b5 = c.bit.b3 ^ c.bit.b4 ^ c.bit.b5 ^ d.bit.b3 ^ d.bit.b4 ^ d.bit.b5;
  r.bit.b6 = c.bit.b4 ^ c.bit.b5 ^ c.bit.b6 ^ d.bit.b4 ^ d.bit.b5 ^ d.bit.b6;
  r.bit.b7 = c.bit.b5 ^ c.bit.b6 ^ c.bit.b7 ^ d.bit.b5 ^ d.bit.b6 ^ d.bit.b7;

  return r.word;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++                       AdamoDecode                     +++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const int AdamoDecode::NBYTE;
const int AdamoDecode::NXY;
const int AdamoDecode::BYTE0;
const int AdamoDecode::BYTE1;
const int AdamoDecode::BYTE2;

AdamoDecode::AdamoDecode() { ; }

AdamoDecode::~AdamoDecode() { ; }

int AdamoDecode::Clear() {
  valid = true;
  for (int ixy = 0; ixy < NXY; ixy++) {
    packet[ixy].Clear();
  }
  return 0;
}

int AdamoDecode::Decode(char *data) {
  int chk = 0, chk_x = 0, chk_y = 0;
  // Decode for X
  chk_x = DecodePacket(data, 0);
  // Decode for Y
  chk_y = DecodePacket(data + AdamoPacket::NBYTE, 1);

  // Check Event Counter Values
  if (packet[0].eventcounter != packet[1].eventcounter) {
    cerr << "[AdamoDecode::Decode] Warning: Event Counter values in X and Y are incompatible." << endl;
    chk = -1;
  }

  if (chk == 0 && chk_x == 0 && chk_y == 0) {
    return 0;
  } else {
    valid = false;
    return -1;
  }
}

int AdamoDecode::DecodePacket(char *data, int ixy) {
  int ibyte = 0;
  char *datacrc;

  // Decode =============================================
  // Decode Header ---------------------------------------
  memcpy(packet[ixy].header, &(data[ibyte]), AdamoPacket::NHEADER);
  ibyte += AdamoPacket::NHEADER;

  // Decode Event Counter --------------------------------
  packet[ixy].eventcounter = BYTE2 * (unsigned char)data[ibyte + 2] + BYTE1 * (unsigned char)data[ibyte + 1] +
                             BYTE0 * (unsigned char)data[ibyte];
  ibyte += AdamoPacket::NBYTE_EC;

  // Decode Data ----------------------------------------
  datacrc = (char *)(&data[ibyte]);
  unsigned int dtmp;
  int idata = 0, idata_n2, idata_n1;
  int ilayer = 0;
  int istrip = 0;
  for (int i = 0; i < AdamoPacket::NBYTE_DATA; i++) {
    if (i % 3 == 0 || i % 3 == 1) {
      dtmp = (unsigned char)data[ibyte + i];
      ilayer = idata % AdamoPacket::NLAYER;
      istrip = idata / AdamoPacket::NLAYER;
      packet[ixy].strip[ilayer][istrip] = BYTE0 * dtmp;
      idata++;
    } else {
      idata_n2 = idata - 2;
      idata_n1 = idata - 1;

      dtmp = (unsigned char)(data[ibyte + i] & 0xF);
      ilayer = idata_n2 % AdamoPacket::NLAYER;
      istrip = idata_n2 / AdamoPacket::NLAYER;
      packet[ixy].strip[ilayer][istrip] += BYTE1 * dtmp;

      dtmp = (unsigned char)((data[ibyte + i] & 0xF0) >> 4);
      ilayer = idata_n1 % AdamoPacket::NLAYER;
      istrip = idata_n1 / AdamoPacket::NLAYER;
      packet[ixy].strip[ilayer][istrip] += BYTE1 * dtmp;
    }
  }
  ibyte += AdamoPacket::NBYTE_DATA;

  // Decode CRC -----------------------------------------
  packet[ixy].CRC = (unsigned char)data[ibyte];
  ibyte += AdamoPacket::NBYTE_CRC;

  // Decode Trailer -------------------------------------
  memcpy(packet[ixy].trailer, &(data[ibyte]), AdamoPacket::NTRAILER);
  ibyte += AdamoPacket::NTRAILER;

  // Check Decode =======================================
  // Check Header
  int chk = 0;
  if (!packet[ixy].CheckHeader(ixy)) {
    cerr << "[AdamoDecode::DecodePacket] Warning. "
         << " the header values is not correct. ";
    for (int i = 0; i < AdamoPacket::NHEADER; i++) {
      cerr << hex << (int)packet[ixy].header[i] << " ";
    }
    cerr << dec << endl;
    chk = -1;
  }
  // Check Trailer
  if (!packet[ixy].CheckTrailer(ixy)) {
    cerr << "[AdamoDecode::DecodePacket] Warning. "
         << " the trailer values is not correct. ";
    for (int i = 0; i < AdamoPacket::NTRAILER; i++) {
      cerr << hex << (int)packet[ixy].trailer[i] << " ";
    }
    cerr << dec << endl;
    chk = -1;
  }
  //   // CRC check (Not well implemented yet)
  //   if(! packet[ixy].CheckCRC(datacrc)){
  //     cerr << "[AdamoDecode::DecodePacket] Warning. CRC value is not correct " << endl;
  //   }

  return chk;
}

// ============================ SetSR ============================
int AdamoDecode::SetSR(char *data) {
  unsigned int *tmp = (unsigned int *)data;
  for (int i = 0; i < NSR; i++) {
    SR[i] = tmp[i];
  }

  // Check --------------------------------
  // Check fixed value (first word) ------
  int chk = 0;
  if (SR[0] != 0x73210abc) {
    cerr << "[AdamoDecode::SetSR] Error "
         << "first status register word (fixed:073210abc) is invalid : " << hex << SR[0] << dec << endl;
    chk = -1;
  }
  // Check Alarm bits (2nd word)--------------------
  if ((SR[1] & 0xFFFF0000)) {
    chk = -1;
  }
  // Check Alarm bits (3nd word)--------------------
  if ((SR[2] & 0x00000000)) {
    chk = -1;
  }
  //   struct alarmbit8 {
  //     unsigned char word;
  //     struct alarmbit_field{
  //       unsigned FV:1;
  //       unsigned FF:1;
  //       unsigned FDN:1;
  //       unsigned GFE:1;
  //       unsigned N0:1;
  //       unsigned N1:1;
  //       unsigned LFE:1;
  //       unsigned FM:1;
  //     };
  //   } alarmbyte[4];
  //   alarmbyte[0].word = (unsigned char) ((SR[1] & 0x000F) >> 0 );
  //   alarmbyte[1].word = (unsigned char) ((SR[1] & 0x00F0) >> 8 );
  //   alarmbyte[2].word = (unsigned char) ((SR[1] & 0x0F00) >> 16);
  //   alarmbyte[3].word = (unsigned char) ((SR[1] & 0xF000) >> 24);

  if (chk != 0) valid = false;
  return chk;
}

// ================= AdamoDecode::FillToSPSAdamo ======================
int AdamoDecode::FillToSPSAdamo(SPSAdamo *d) {
  // Copy Data Quality Flag
  d->valid = valid;

  // Copy Event Counter Value
  d->eventcounter = packet[0].eventcounter;

  // Copy Data
  for (int il = 0; il < AdamoPacket::NLAYER; il++) {
    for (int ixy = 0; ixy < NXY; ixy++) {
      for (int is = 0; is < AdamoPacket::NSTRIP; is++) {
        d->strip[il][ixy][is] = packet[ixy].strip[il][is];
      }
    }
  }

  // Copy Status Registers
  for (int i = 0; i < NSR; i++) {
    d->SR[i] = SR[i];
  }

  return 0;
}
