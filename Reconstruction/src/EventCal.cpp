#include "EventCal.hh"

#include <TMath.h>
#include <TVectorD.h>

#include <cstdlib>
#include <cstring>
#include <fstream>

#include "Arm1CalPars.hh"
#include "Arm2CalPars.hh"
#include "Utils.hh"

/**
 *  @class nLHCf::EventCal
 *  @brief Main class for Gain Calibration and format conversion
 *  @details The class converts the data format from raw to level2. (intermediate steps of level 0 and 1)
 */

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(EventCal);
#endif

typedef Utils UT;

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass, typename armcal>
EventCal<armclass, armcal>::EventCal() : fAdamoPresence(false), fAveragePedestal(false) {}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass, typename armcal>
EventCal<armclass, armcal>::~EventCal() {}

/*-----------------------------*/
/*--- Allocate/clear memory ---*/
/*-----------------------------*/
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::Initialise() {
  SetSiliconEvent(0);
  AllocateVectors();
  SetParameters();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::InitialiseWithoutTables() {
  SetSiliconEvent(0);
  AllocateVectors();
  SetParametersWithoutTables();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::AllocateVectors() {
  fPedCnt = 0;

  Utils::AllocateVector3D(fCalPedShift, this->kCalNtower, this->kCalNlayer, this->kCalNrange);
  Utils::AllocateVector3D(fCalPedMean, this->kCalNtower, this->kCalNlayer, this->kCalNrange);
  Utils::AllocateVector3D(fCalPedRms, this->kCalNtower, this->kCalNlayer, this->kCalNrange);
  Utils::AllocateVector1D(fPosPedMean, this->kPosNtower);
  Utils::AllocateVector1D(fPosPedRms, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    Utils::AllocateVector4D(fPosPedMean[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it],
                            this->kPosNsample);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    Utils::AllocateVector4D(fPosPedRms[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it],
                            this->kPosNsample);

  if (this->kArmIndex == Arm1Params::kArmIndex) {
    fNiters = Arm1CalPars::nPedSubIters;
    fNsigma = Arm1CalPars::nPedSubSigma;
  } else if (this->kArmIndex == Arm2Params::kArmIndex) {
    fNiters = Arm2CalPars::nPedSubIters;
    fNsigma = Arm2CalPars::nPedSubSigma;
  }

  Utils::AllocateVector4D(fCalPedPars, this->kCalNtower, this->kCalNlayer, this->kCalNrange, 4);
  Utils::AllocateVector1D(fPosPedPars, this->kPosNtower);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    Utils::AllocateVector5D(fPosPedPars[it], this->kPosNlayer, this->kPosNview, this->kPosNchannel[it],
                            this->kPosNsample, 4);

  ClearVectors();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ClearVectors() {
  Utils::ClearVector3D<Double_t>(fCalPedShift, 0.);
  Utils::ClearVector3D<Double_t>(fCalPedMean, 0.);
  Utils::ClearVector3D<Double_t>(fCalPedRms, 0.);
  Utils::ClearVector5D<Double_t>(fPosPedMean, 0.);
  Utils::ClearVector5D<Double_t>(fPosPedRms, 0.);
}

/*----------------------------------*/
/*--- Set calibration parameters ---*/
/*----------------------------------*/
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SetParameters() {
  if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kLHC2022 ||
      Arm2Params::fOperation == kLHC2025)
    SetSiliconHybSeqUpgrade();
  else
    SetSiliconHybSeq();
  fReadTableCal.ReadTables();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SetParametersWithoutTables() {
  if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 || Arm2Params::fOperation == kLHC2022 ||
      Arm2Params::fOperation == kLHC2025)
    SetSiliconHybSeqUpgrade();
  else
    SetSiliconHybSeq();
  fReadTableCal.SetPosDetID();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SetSiliconHybSeq() {
  /* New configuration (from November 2014) */
  char tmp[4][4][10] = {{"3XL", "3XR", "1XL", "1XR"},
                        {"1YL", "1YR", "3YL", "3YR"},
                        {"4XL", "4XR", "2XL", "2XR"},
                        {"2YL", "2YR", "4YL", "4YR"}};

  for (int ibo = 0; ibo < NBOARDS; ibo++)
    for (int ise = 0; ise < NSEC; ise++) strcpy(HybSeqMDAQ[ibo][ise], tmp[ibo][ise]);
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SetSiliconHybSeqUpgrade() {
  /* New configuration (from September 2021) */
  char tmp[8][10] = {"3X", "1X", "1Y", "3Y", "4X", "2X", "2Y", "4Y"};

  if (Arm2Params::fOperation == kSPS2021)  // The hybrid on the last layer were swapped in 2021
  {
    strcpy(tmp[6], "4Y");
    strcpy(tmp[7], "2Y");
  }

  for (int ibo = 0; ibo < NMDAQSECTIONS; ibo++) strcpy(HybSeqMDAQUpgrade[ibo], tmp[ibo]);
}

/*---------------------------------------------------------------------*/
/*--- Cleaning and Computation of Calorimeter Pedestal Mean and RMS ---*/
/*---------------------------------------------------------------------*/

/* Initialize vectors used for pedestal cleaning */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CreatePedestalVector(Level0<armclass> *lvl0) {
  Utils::AllocateVector4D(aTmpPedVec, lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange, 0);
  Utils::AllocateVector3D(aTmpPedAve, lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
  Utils::AllocateVector3D(aTmpPedRMS, lvl0->kCalNtower, lvl0->kCalNlayer, lvl0->kCalNrange);
}

/* Fill Calorimeter Pedestal vector event-by-event */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::FillPedestalVector(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        if (fAveragePedestal) {  // Pedestals are defined as signal only
          aTmpPedVec[it][il][ir].push_back(lvl0->fCalorimeter[it][il][ir]);
        } else {  // Pedestals are defined as signal-delayed gate
          aTmpPedVec[it][il][ir].push_back(lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir]);
        }
      }
}

/* Clean vector during each iterative procedure */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CleanPedestalVector(Level0<armclass> *lvl0) {
  vector<Int_t> removeMe = vector<Int_t>();
  const int npedestal = aTmpPedVec[0][0][0].size();
  for (Int_t ip = 0; ip < npedestal; ++ip) {
    bool good = true;
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it) {
      if (!good) break;
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
        if (!good) break;
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          if (!good) break;
          if (TMath::Abs(aTmpPedVec[it][il][ir][ip] - aTmpPedAve[it][il][ir]) > fNsigma * aTmpPedRMS[it][il][ir]) {
            removeMe.push_back(ip);
            good = false;
          }
        }
      }
    }
  }
  for (Int_t ip = (Int_t)removeMe.size(); ip > 0; --ip) {
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir)
          aTmpPedVec[it][il][ir].erase(aTmpPedVec[it][il][ir].begin() + removeMe[ip - 1]);
  }
}

/* Compute final value of Calorimeter Pedestal Mean and RMS variables via iterative procedure */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::GetPedestalVectorMeanAndRMS(Level0<armclass> *lvl0) {
  if (lvl0->kArmIndex == Arm1Params::kArmIndex)
    Utils::Printf(Utils::kPrintInfo, "\nArm1\n");
  else if (lvl0->kArmIndex == Arm2Params::kArmIndex)
    Utils::Printf(Utils::kPrintInfo, "\nArm2\n");

  for (int iter = 0; iter < fNiters; ++iter) {
    Utils::Printf(Utils::kPrintInfo, "Iteration %d\n", iter + 1);
    if (iter > 0) CleanPedestalVector(lvl0);
    for (Int_t it = 0; it < lvl0->kCalNtower; ++it)
      for (Int_t il = 0; il < lvl0->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
          Utils::GetMeanRMS(aTmpPedVec[it][il][ir], aTmpPedAve[it][il][ir], aTmpPedRMS[it][il][ir]);
        }
  }
}

/* Delete vectors used for pedestal cleaning */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::DeletePedestalVector() {
  aTmpPedVec.clear();
}

/* Delete variables used for pedestal cleaning */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::DeletePedestalVariables() {
  aTmpPedAve.clear();
  aTmpPedRMS.clear();
}

/* Check if Calorimeter Pedestal event significantly deviates due to beam contamination*/
template <typename armclass, typename armcal>
Bool_t EventCal<armclass, armcal>::isGoodPedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  Bool_t good = true;
  for (Int_t it = 0; it < lvl0->kCalNtower; ++it) {
    if (!good) break;
    for (Int_t il = 0; il < lvl0->kCalNlayer; ++il) {
      if (!good) break;
      for (Int_t ir = 0; ir < lvl0->kCalNrange; ++ir) {
        if (!good) break;
        Double_t signal = 0.;
        if (fAveragePedestal) {  // Pedestals are defined as signal only
          signal = lvl0->fCalorimeter[it][il][ir];
        } else {  // Pedestals are defined as signal-delayed gate
          signal = lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir];
        }
        if (TMath::Abs(signal - aTmpPedAve[it][il][ir]) > fNsigma * aTmpPedRMS[it][il][ir]) {
          good = false;
          break;
        }
      }
    }
  }
  return good;
}

/*--------------------------------------------*/
/*--- Computation of Pedestal Mean and RMS ---*/
/*--------------------------------------------*/

template <typename armclass, typename armcal>
Int_t EventCal<armclass, armcal>::GivePedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0, InOutCal *io_class) {
  if (fIsMC) {  // Simulation
    /* Get generated pedestal for MC */
    if (io_class == NULL) {
      Utils::Printf(Utils::kPrintError, "Error: NULL pointer for InOutCal class in ReadPedestal\n");
      exit(EXIT_FAILURE);
    }
    ped0->DataCopy(io_class->GetPedestal0<armclass>(), "all");
    return 0;
  } else {  // LHC-Data
    /* Convert from MIDAS to Level0 (only for DATA) */
    ConvertToLevel0(lvl0, ped0);
    return 0;
  }
  return -1;
}

/* Fill Pedestal Mean and RMS variables event-by-event */
template <typename armclass, typename armcal>
Int_t EventCal<armclass, armcal>::SavePedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  if (fIsMC) {  // Simulation
    for (Int_t it = 0; it < this->kCalNtower; ++it)
      for (Int_t il = 0; il < this->kCalNlayer; ++il)
        for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
          fCalPedMean[it][il][ir] += ped0->fCalorimeter[it][il][ir];
          fCalPedRms[it][il][ir] += ped0->fCalorimeter[it][il][ir] * ped0->fCalorimeter[it][il][ir];
        }

    for (Int_t it = 0; it < this->kPosNtower; it++)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
            for (Int_t is = 0; is < this->kPosNsample; ++is) {
              fPosPedMean[it][il][iv][ic][is] += ped0->fPosDet[it][il][iv][ic][is];
              fPosPedRms[it][il][iv][ic][is] += ped0->fPosDet[it][il][iv][ic][is] * ped0->fPosDet[it][il][iv][ic][is];
            }
    ++fPedCnt;
    return 0;
  } else {                                     // LHC-Data
    if (fValidEvent && lvl0->fEventFlag[1]) {  // Good Pedestal Event
      for (Int_t it = 0; it < this->kCalNtower; ++it)
        for (Int_t il = 0; il < this->kCalNlayer; ++il)
          for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
            fCalPedMean[it][il][ir] += lvl0->fCalorimeter[it][il][ir];
            fCalPedRms[it][il][ir] += lvl0->fCalorimeter[it][il][ir] * lvl0->fCalorimeter[it][il][ir];
            fCalPedShift[it][il][ir] += lvl0->fCalorimeter[it][il][ir] - ped0->fCalorimeter[it][il][ir];
          }
      for (Int_t it = 0; it < this->kPosNtower; it++)
        for (Int_t il = 0; il < this->kPosNlayer; ++il)
          for (Int_t iv = 0; iv < this->kPosNview; ++iv)
            for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
              for (Int_t is = 0; is < this->kPosNsample; ++is) {
                fPosPedMean[it][il][iv][ic][is] += lvl0->fPosDet[it][il][iv][ic][is];
                fPosPedRms[it][il][iv][ic][is] += lvl0->fPosDet[it][il][iv][ic][is] * lvl0->fPosDet[it][il][iv][ic][is];
              }
      ++fPedCnt;
      return 0;
    }
  }
  return -1;
}

/* Read Event and Fill Pedestal */
template <typename armclass, typename armcal>
Int_t EventCal<armclass, armcal>::ReadPedestal(Level0<armclass> *lvl0, Level0<armclass> *ped0, InOutCal *io_class) {
  GivePedestal(lvl0, ped0, io_class);
  SavePedestal(lvl0, ped0);

  return -2;
}

/* Compute final value of Pedestal Mean and RMS variables */
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::GetPedestalMeanRms(Level0<armclass> *ped0) {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
        fCalPedShift[it][il][ir] /= (Double_t)fPedCnt;

        Double_t sum = fCalPedMean[it][il][ir];
        fCalPedMean[it][il][ir] /= (Double_t)fPedCnt;
        // if (!fIsMC) {                                             // TEMP
        fCalPedRms[it][il][ir] -= sum * sum / (Double_t)fPedCnt;
        fCalPedRms[it][il][ir] /= (Double_t)(fPedCnt - 1);
        fCalPedRms[it][il][ir] = TMath::Sqrt(fCalPedRms[it][il][ir]);
        // } else {                                                  // TEMP
        //   fCalPedRms[it][il][ir]   = fCalPedRmsTable[it][il][ir]; // TEMP
        // }                                                         // TEMP

        fCalPedPars[it][il][ir][0] = fCalPedRms[it][il][ir];
        fCalPedPars[it][il][ir][1] = 0;
        fCalPedPars[it][il][ir][2] = 0;
        fCalPedPars[it][il][ir][3] = fCalPedMean[it][il][ir];

        ped0->fCalorimeter[it][il][ir] = fCalPedMean[it][il][ir];
      }

  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            Double_t sum = fPosPedMean[it][il][iv][ic][is];
            fPosPedMean[it][il][iv][ic][is] /= (Double_t)fPedCnt;
            // if (!fIsMC) {                                                             // TEMP
            fPosPedRms[it][il][iv][ic][is] -= sum * sum / (Double_t)fPedCnt;
            fPosPedRms[it][il][iv][ic][is] /= (Double_t)(fPedCnt - 1);
            fPosPedRms[it][il][iv][ic][is] = TMath::Sqrt(fPosPedRms[it][il][iv][ic][is]);
            // } else {                                                                  // TEMP
            //   fPosPedRms[it][il][iv][ic][is]   = fPosPedRmsTable[it][il][iv][ic][is]; // TEMP
            // }                                                                         // TEMP

            fPosPedPars[it][il][iv][ic][is][0] = fPosPedRms[it][il][iv][ic][is];
            fPosPedPars[it][il][iv][ic][is][1] = 0;
            fPosPedPars[it][il][iv][ic][is][2] = 0;
            fPosPedPars[it][il][iv][ic][is][3] = fPosPedMean[it][il][iv][ic][is];

            ped0->fPosDet[it][il][iv][ic][is] = fPosPedMean[it][il][iv][ic][is];
          }
}

/*-------------------------*/
/*--- Event calibration ---*/
/*-------------------------*/

////////////////////////////////////////////////////////////
/// Core function for this class; Gain Calibration and Format conversion
/// \tparam armclass
/// \tparam armcal
/// \param lvl0
/// \param ped0
/// \param lvl1
/// \param lvl2
/// \param io_class
/// \return 0: normal events (Shower or Pi0 triggers), -1: Error or Pedestal events or Non-colliding bunches
template <typename armclass, typename armcal>
Int_t EventCal<armclass, armcal>::EventCalibration(Level0<armclass> *lvl0, Level0<armclass> *ped0,
                                                   Level1<armclass> *lvl1, Level2<armclass> *lvl2, InOutCal *io_class,
                                                   Bool_t keep_all) {
  if (!fIsMC) {
    /* Conver tfrom MIDAS to Level0 (only for DATA) */
    ConvertToLevel0(lvl0, ped0);

    if (!fValidEvent) return -1;  // skip corrupted events

    if (lvl0->fEventFlag[1]) {  // pedestal event
      GetCalorimeterPedestal(ped0, lvl0);
      GetPosDetPedestal(ped0, lvl0);
      if (!keep_all) return -1;
    }

    if (this->fOperation == kLHC2015) {
      if (!lvl0->fEventFlag[0] && !lvl0->IsShowerTrg() && !lvl0->IsSpecialTrg() && !keep_all) return -1;
    } else if (this->fOperation == kSPS2015) {
      if (!lvl0->fEventFlag[0] && !keep_all) return -1;  // Remove only pedestal events
    } else if (this->fOperation == kSPS2021) {
      if (!lvl0->fEventFlag[0] && !keep_all) return -1;  // Remove only pedestal events
    } else if (this->fOperation == kSPS2022) {
      if (!lvl0->fEventFlag[0] && !keep_all) return -1;  // Remove only pedestal events
    } else if (this->fOperation == kLHC2022) {
      if (!lvl0->fEventFlag[0] && !keep_all) return -1;  // Remove only pedestal events
    } else if (this->fOperation == kLHC2025) {
      if (!lvl0->fEventFlag[0] && !keep_all) return -1;  // Remove only pedestal events
    }

    Utils::Printf(Utils::kPrintDebugFull, "step 2\n");

  } else {
    /* Get converted Level0 and generated pedestal for MC */
    lvl0->DataCopy(io_class->GetLevel0<armclass>(), "all");
    ped0->DataCopy(io_class->GetPedestal0<armclass>(), "all");
  }

  CheckSaturation(lvl0);

  ConvertToLevel1(lvl1, lvl0, ped0);

  ConvertToLevel2(lvl2, lvl1);

  Utils::Printf(Utils::kPrintDebug, "\n");
  for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        Utils::Printf(Utils::kPrintDebug, "Calorimeter[%d][%d][%d] = %f - %f -> %f : %d? -> %f\n", it, il, ir,
                      lvl0->fCalorimeter[it][il][ir], ped0->fCalorimeter[it][il][ir], lvl1->fCalorimeter[it][il][ir],
                      (Int_t)lvl1->fCalSatFlag[it][il][ir], lvl2->fCalorimeter[it][il]);
      }
    }
  }
  for (Int_t it = 0; it < this->kPosNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            if (lvl2->fPosDet[it][il][iv][ic][is] > 0.)
              Utils::Printf(Utils::kPrintDebug, "Silicon[%d][%d][%d][%d][%d] = %f\n", it, il, iv, ic, is,
                            lvl2->fPosDet[it][il][iv][ic][is]);
          }
        }
      }
    }
  }

  return 0;
}

////////////////////////////////////////////////////
/// Format conversion from Raw to Level0
/// The raw format corresponds to the MIDAS root format (direct output or conversion from .mid file)
/// \tparam armclass
/// \tparam armcal
/// \param lvl0
/// \param ped0
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ConvertToLevel0(Level0<armclass> *lvl0, Level0<armclass> *ped0) {
  fValidEvent = true;

  /* Preliminary check of event alignment in 2025 operations */
  if (this->fOperation == kLHC2025) {
    for (Int_t iruni = 0; iruni <= 2; ++iruni) {
      if (this->RUNI.RUNI[iruni] != this->MiniRUNI.MiniRUNI[iruni]) {
        Utils::Printf(Utils::kPrintError, Form("RUNI[%d] is not consistent between VME and MiniVME %u vs %u\n", iruni,
                                               this->RUNI.RUNI[iruni], this->MiniRUNI.MiniRUNI[iruni]));
      }
    }
    for (Int_t igpi0 = 0; igpi0 <= 3; ++igpi0) {
      if (this->GPI0.GPI0[igpi0] != this->MiniGPI0.MiniGPI0[igpi0]) {
        Utils::Printf(Utils::kPrintError, Form("GPI0[%d] is not consistent between VME and MiniVME %x vs %x\n", igpi0,
                                               this->GPI0.GPI0[igpi0], this->MiniGPI0.MiniGPI0[igpi0]));
      }
    }
    for (Int_t igpi0 = 10; igpi0 <= 10; ++igpi0) {
      if (this->GPI0.GPI0[igpi0] != this->MiniGPI0.MiniGPI0[igpi0]) {
        Utils::Printf(Utils::kPrintError, Form("GPI0[%d] is not consistent between VME and MiniVME %x vs %x\n", igpi0,
                                               this->GPI0.GPI0[igpi0], this->MiniGPI0.MiniGPI0[igpi0]));
      }
    }
  }

  /* Run/event number */
  if (this->fOperation == kLHC2022 || this->fOperation == kLHC2025) {
    lvl0->fRun = this->RUNI.RUNI[0];
    lvl0->fEvent = this->RUNI.RUNI[2];
    lvl0->fGevent = this->GPI0.GPI0[11];
  } else {
    lvl0->fRun = this->Number.Run;
    lvl0->fEvent = this->Number.Number;
    lvl0->fGevent = this->kNgpi0 > 18 ? (this->GPI0.GPI0[18] >> 16) : -1;
  }
  lvl0->fTime[0] = this->TIME.TIME[0];
  lvl0->fTime[1] = this->TIME.TIME[1];

  /* GPIO */
  lvl0->fFlag[0] = this->GPI0.GPI0[0];
  lvl0->fFlag[1] = this->GPI0.GPI0[1];
  lvl0->fFlag[2] = this->GPI0.GPI0[2];

  /* Event flag */
  lvl0->fEventFlag[0] = lvl0->IsBeam();      // Beam Event
  lvl0->fEventFlag[1] = lvl0->IsPedestal();  // Pedestal Event (Combination of DAQ flag and Discriminator flag(=0) )
  lvl0->fEventFlag[2] = 0;

  /* Calorimeter */
  for (Int_t il = 0; il < this->kCalNlayer; ++il)
    for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
      // Signal
      int tmpch = il * 2 + (1 - ir);
      lvl0->fCalorimeter[0][il][ir] = this->ADC0.ADC0[tmpch];
      lvl0->fCalorimeter[1][il][ir] = this->ADC1.ADC1[tmpch];
      // If pedestal is computed from average, delayed-gate is always ignored
      if (!fAveragePedestal) {
        // Pedestal (delayed gate)
        int tmpchd = il * 2 + (1 - ir) + 32;
        ped0->fCalorimeter[0][il][ir] = this->ADC0.ADC0[tmpchd];
        ped0->fCalorimeter[1][il][ir] = this->ADC1.ADC1[tmpchd];
      }
    }
  /* swap for SPS2021 */
  if (this->fOperation == kSPS2021) {
    if (this->kArmIndex == Arm1Params::kArmIndex) {
      double tmpcalori[8];
      double tmppede[8];
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
        for (Int_t il = 0; il < 8; ++il) {
          tmpcalori[il] = lvl0->fCalorimeter[1][il][ir];
          tmppede[il] = ped0->fCalorimeter[1][il][ir];
        }
        for (Int_t il = 0; il < 8; ++il) {
          lvl0->fCalorimeter[1][il][ir] = tmpcalori[7 - il];
          ped0->fCalorimeter[1][il][ir] = tmppede[7 - il];
        }
      }
    }
  }
  // Swap for LHC 2022 (Replacement of broken channel (TS05 -> ADC2 channel 12))
  else if (this->fOperation == kLHC2022 || this->fOperation == kSPS2022) {
    if (this->kArmIndex == Arm1Params::kArmIndex) {
      lvl0->fCalorimeter[0][5][0] = this->ADC2.ADC2[12 * 2 + 1];
      lvl0->fCalorimeter[0][5][1] = this->ADC2.ADC2[12 * 2];
      ped0->fCalorimeter[0][5][0] = this->ADC2.ADC2[12 * 2 + 1 + 32];
      ped0->fCalorimeter[0][5][1] = this->ADC2.ADC2[12 * 2 + 32];
    }
  }

  /* OpenADC for noise check */
  if (this->fOperation == kSPS2021) {
    lvl0->fOpenADC[0] = this->ADC2.ADC2[2 * 4 + 1] - this->ADC2.ADC2[32 + 2 * 4 + 1];  // HAD-ZDC ch 1
    lvl0->fOpenADC[1] = this->ADC2.ADC2[2 * 5 + 1] - this->ADC2.ADC2[32 + 2 * 5 + 1];  // HAD-ZDC ch 2
    lvl0->fOpenADC[2] = this->ADC2.ADC2[2 * 6 + 1] - this->ADC2.ADC2[32 + 2 * 6 + 1];  // HAD-ZDC ch 3
    lvl0->fOpenADC[3] = this->ADC2.ADC2[2 * 7 + 1] - this->ADC2.ADC2[32 + 2 * 7 + 1];  // empty
  } else {
    lvl0->fOpenADC[0] = this->ADC2.ADC2[17];  //  LRADC
    lvl0->fOpenADC[1] = this->ADC2.ADC2[19];  //  LRADC  delayed gate
    lvl0->fOpenADC[2] = this->ADC2.ADC2[16];  //  HRADC
    lvl0->fOpenADC[3] = this->ADC2.ADC2[18];  //  HRADC  delayed gate
  }

  /* Front counter */
  for (Int_t il = 0; il < this->kFcNlayer; ++il) {
    for (Int_t ir = 0; ir < this->kFcNrange; ++ir) {
      int ch;
      if (this->kArmIndex == Arm1Params::kArmIndex && il == 2)
        ch = 2 * 4 + 1 - ir;
      else
        ch = 2 * il + 1 - ir;

      lvl0->fFrontCounter[il][ir] = this->ADC2.ADC2[ch];       // !!! TODO for Arm1 !!!
      ped0->fFrontCounter[il][ir] = this->ADC2.ADC2[ch + 32];  // !!! TODO for Arm1 !!!
      // Utils::Printf(Utils::kPrintDebugFull, Form("FC ch %d  %lf\n", il, lvl0->fFrontCounter[il]));
    }
  }

  /* ZDC */
  for (Int_t i = 0; i < this->kZdcNchannel; ++i) {
    for (Int_t ir = 0; ir < this->kZdcNrange; ++ir) {
      if (this->fOperation == kLHC2022 || this->fOperation == kLHC2025) {
        lvl0->fZdc[i][ir] = this->ADC2.ADC2[2 * (8 + i) + 1 - ir];
        ped0->fZdc[i][ir] = this->ADC2.ADC2[32 + 2 * (8 + i) + 1 - ir];
      } else if (this->fOperation == kSPS2021) {
        lvl0->fZdc[i][ir] = this->ADC2.ADC2[2 * (4 + i) + 1 - ir];
        ped0->fZdc[i][ir] = this->ADC2.ADC2[32 + 2 * (4 + i) + 1 - ir];
      }
    }
  }

  /* Laser monitor */
  if (this->ADC3.nADC3 > 0) {
    for (Int_t i = 0; i < 2; ++i) {
      lvl0->fLaser[i] = this->ADC3.ADC3[i];
      ped0->fLaser[i] = this->ADC3.ADC3[i + 2];
    }
  }

  /* TDC */
  const int L3T_channel = 1;
  double L3T_value = 0.;
  vector<double> tmptdc0[12];
  vector<char> tmptdc0flag[12];
  unsigned int tmpdata;
  int tmpvalue;
  int tmpchannel = 0;
  int tmpheader;
  int tmpphase;
  int nhit[12] = {0};

  for (int ib = 0; ib < this->kNtdc0; ib++) {
    tmpdata = this->TDC0.TDC0[ib];
    if (tmpdata == 0xFFFFFFFF) {
      break;
    }
    tmpheader = (tmpdata >> 27) & 0x1F;
    if (tmpheader == 0) {
      tmpvalue = tmpdata & 0x1FFFFF;
      tmpchannel = (tmpdata >> 21) & 0x1F;
      tmpphase = (tmpdata >> 26) & 0x1;
      // Utils::Printf(Utils::kPrintDebugFull, Form("TDC %d %d %d\n", tmpvalue,tmpchannel, tmpphase));

      if (tmpchannel >= 12) {
        Utils::Printf(Utils::kPrintError, Form("Warning: #TDC channels is >= %d\n", this->kTdcNch));
      } else {
        tmptdc0[tmpchannel].push_back(tmpvalue * 0.025);
        // tmptdc0flag[tmpchannel].push_back(0x1*tmpphase);
        tmptdc0flag[tmpchannel].push_back(0x1);
      }
    } else if (tmpheader == 0x10) {
      break;
    }
  }
  // Check of number of hits of L3T and Set base value (L3T_value)
  if (tmptdc0[L3T_channel].size() == 1) {
    L3T_value = tmptdc0[L3T_channel][0];
  } else if (tmptdc0[L3T_channel].size() > 1) {
    Utils::Printf(Utils::kPrintError, Form("Warning: TDC ch1 (L3T) has more than one hit\n"));
    L3T_value = tmptdc0[L3T_channel][0];
  } else if (tmptdc0[L3T_channel].size() == 0) {
    Utils::Printf(Utils::kPrintError, Form("Warning: TDC ch1 (L3T) has no hit\n"));
    L3T_value = 9200;
  }
  // Fill with the window
  for (int ich = 0; ich < this->kTdcNch; ich++) {
    for (int i = 0; i < (int)tmptdc0[ich].size(); i++) {
      // check the signal is in the window or not.
      // The window is [L3T-2usec, L3T+1usec]
      if (tmptdc0[ich][i] < L3T_value - 2000 || tmptdc0[ich][i] > L3T_value + 1000) {
        continue;
      }

      if (nhit[ich] >= this->kTdcNhit) {
        Utils::Printf(Utils::kPrintError, Form("#hits of TDC[%d] is over 16.\n", ich));
      } else {
        lvl0->fTdc[ich][nhit[ich]] = tmptdc0[ich][i];
        lvl0->fTdcFlag[ich][nhit[ich]] = tmptdc0flag[ich][i];
        nhit[ich]++;
      }
    }
  }

  /* Scaler */
  if (this->SCL0.nSCL0 > 0) {
    for (int i = 0; i < this->kNscl0; ++i) lvl0->fScaler[i] = this->SCL0.SCL0[i];
  }

  /* Counter */
  unsigned int gpio0[this->kNgpi0];
  for (int iele = 0; iele < this->kNgpi0; ++iele) gpio0[iele] = this->GPI0.GPI0[iele];

  unsigned int *val = &(gpio0[0]);
  if (lvl0->fOperation == kLHC2015 || lvl0->fOperation == kSPS2015 || lvl0->fOperation == kSPS2021 ||
      lvl0->fOperation == kSPS2022) {                     // For Menjo-san: Is this assignment correct?
    lvl0->fCounter[0] = CounterFilter(val[3], 32);        // CLK
    lvl0->fCounter[1] = CounterFilter(val[4], 32);        // BPTX1
    lvl0->fCounter[2] = CounterFilter(val[5], 32);        // BPTX2
    lvl0->fCounter[3] = CounterFilter(val[6], 32);        // BPTX_AND
    lvl0->fCounter[4] = CounterFilter(val[7], 24);        // ORBIT
    lvl0->fCounter[5] = CounterFilter(val[8], 32);        // L1T
    lvl0->fCounter[6] = CounterFilter(val[9], 32);        // L1T ENABLE
    lvl0->fCounter[7] = CounterFilter(val[10], 24);       // STRG(Small Shower Trigger)
    lvl0->fCounter[8] = CounterFilter(val[11], 24);       // LTRG(Large Shower Trigger)
    lvl0->fCounter[9] = CounterFilter(val[12], 24);       // SLOGIC(Small Shower Logic-out)
    lvl0->fCounter[10] = CounterCombine(&(val[10]));      // LLOGIC(Large Shower logic-out)
    lvl0->fCounter[11] = CounterFilter(val[13], 24);      // L2T_Shower_Trg
    lvl0->fCounter[12] = CounterFilter(val[14], 24);      // L2T_Shower_BPTX
    lvl0->fCounter[13] = CounterFilter(val[15], 24);      // L2T_Special_Trg
    lvl0->fCounter[14] = CounterCombine(&(val[13]));      // L2T_Special_BPTX
    lvl0->fCounter[15] = CounterFilter(val[16], 16);      // L2T_SHOWER "AND" L3T
    lvl0->fCounter[16] = CounterFilter(val[16], 16, 16);  // L2T_SPECIAL "AND" L3T
    lvl0->fCounter[17] = CounterFilter(val[17], 16);      // L2T_PEDE "AND" L3T
    lvl0->fCounter[18] = CounterFilter(val[17], 16, 16);  // L2T_L1T "AND" L3T
    lvl0->fCounter[19] = CounterFilter(val[18], 16);      // L3T
    lvl0->fCounter[20] = CounterFilter(val[18], 16, 16);  // L3T_A1 "OR" L3T_A2
    lvl0->fCounter[21] =
        CounterFilter(val[19], 32);  // !! Two Counter raw.gpio0_ues !! A1 A2 Showers "AND" / ATLAS_L1_LHCF
    lvl0->fCounter[22] = CounterFilter(val[20], 16);     // CLK cleared by Orbit
    lvl0->fCounter[23] = CounterFilter(val[20], 8, 16);  // BPTX1 cleared by Orbit
    lvl0->fCounter[24] = CounterFilter(val[20], 8, 24);  // BPTX2 cleared by Orbit
    lvl0->fCounter[25] = CounterFilter(val[21], 32);     // CLK cleared by ATLAS-ECR
    lvl0->fCounter[26] = CounterFilter(val[22], 32);     // ATLAS_L1A cleared by ATLAS-ECR
    lvl0->fCounter[27] = CounterFilter(val[23], 24);     // FC(0) (A2_FC(0) or A2_FCL(0))
    lvl0->fCounter[28] = CounterFilter(val[24], 24);     // FC(1) (A2_FC(1) or A2_FCL(1))
    lvl0->fCounter[29] = CounterFilter(val[25], 24);     // FC(2) (A2_FC(2) or A2_FCL(2))
    if (this->kArmIndex == Arm1Params::kArmIndex) {      // Arm1
      lvl0->fCounter[30] = CounterCombine(&(val[23]));   // FC(3) (A1_FC(3) or A1_FCL(3))
      lvl0->fCounter[31] = CounterFilter(val[26], 24);   // A1_FCL
      lvl0->fCounter[32] = CounterFilter(val[27], 24);   // A1_FC_TRG
      lvl0->fCounter[33] = CounterFilter(val[28], 24);   // A1_SHOWER_BPTX AND A2_FC_TRG
      lvl0->fCounter[34] = CounterCombine(&(val[26]));   // A1_FC_TRG AND A2_FC_TRG
    }

    lvl0->fFifoCounter[0][0] = this->GPI0.GPI0[29];
    lvl0->fFifoCounter[0][1] = this->GPI0.GPI0[30];
    lvl0->fFifoCounter[0][2] = this->GPI0.GPI0[31];
    lvl0->fFifoCounter[1][0] = this->GPI0.GPI0[32];
    lvl0->fFifoCounter[1][1] = this->GPI0.GPI0[33];
    lvl0->fFifoCounter[1][2] = this->GPI0.GPI0[34];
  } else {                                            // LHC 2022, 2025
    lvl0->fCounter[0] = CounterFilter(val[3], 32);    // CLK
    lvl0->fCounter[1] = CounterFilter(val[4], 32);    // BPTX1
    lvl0->fCounter[2] = CounterFilter(val[5], 32);    // BPTX2
    lvl0->fCounter[3] = CounterFilter(val[6], 32);    // BPTX_AND
    lvl0->fCounter[4] = CounterFilter(val[7], 32);    // ORBIT
    lvl0->fCounter[5] = CounterFilter(val[8], 32);    // L1T
    lvl0->fCounter[6] = CounterFilter(val[9], 32);    // L1T ENABLE
    lvl0->fCounter[7] = CounterFilter(val[10], 32);   // L3T
    lvl0->fCounter[8] = CounterFilter(val[11], 32);   // L3T OR
    lvl0->fCounter[9] = CounterFilter(val[12], 32);   // L3T AND
    lvl0->fCounter[10] = (val[13] & 0x3FFF);          // Bunch ID
    lvl0->fCounter[11] = (val[13] >> 14) & 0x1FF;     // Bunch Beam1
    lvl0->fCounter[12] = (val[13] >> 23) & 0x1FF;     // Bunch Beam2
    lvl0->fCounter[13] = CounterFilter(val[14], 32);  // LHCf trigget to ATLAS
    lvl0->fCounter[14] = CounterFilter(val[15], 32);  // ATLAS CLK
    lvl0->fCounter[15] = CounterFilter(val[16], 8);   // ATLAS CLK (higher bits)
    lvl0->fCounter[16] = (val[16] >> 16) & 0xFFFF;    // ATLAS ECR
    lvl0->fCounter[17] = CounterFilter(val[17], 32);  // ATLAS L1A

    lvl0->fFifoCounter[0][0] = val[18];
    lvl0->fFifoCounter[0][1] = val[19];
    lvl0->fFifoCounter[0][2] = val[20];
    lvl0->fFifoCounter[1][0] = val[21];
    lvl0->fFifoCounter[1][1] = val[22];
    lvl0->fFifoCounter[1][2] = val[23];
  }

  /* Position detectors */
  if (this->kArmIndex == Arm1Params::kArmIndex) {  // Arm1
    for (int it = 0; it < this->kPosNtower; ++it)
      for (int il = 0; il < this->kPosNlayer; ++il)
        for (int ixy = 0; ixy < this->kPosNview; ++ixy)
          for (int ipos = 0; ipos < this->kPosNchannel[it]; ++ipos) {
            const int iposdet = fReadTableCal.fPosDetID[it][il][ixy][ipos];
            if (iposdet != -1) lvl0->fPosDet[it][il][ixy][ipos][0] = this->GetPOSDETElements(iposdet);
          }
  }
  // Arm2 ----------------------------------------------------
  else {
    if (Arm2Params::fOperation == kSPS2021 || Arm2Params::fOperation == kSPS2022 ||
        Arm2Params::fOperation == kLHC2022 || Arm2Params::fOperation == kLHC2025)
      ExtractSiliconUpgrade(lvl0);
    else
      ExtractSilicon(lvl0);
  }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ExtractSilicon(Level0<armclass> *lvl0) {
  // temporal buffer for conversion
  char arm2_buffer[this->kNposd];
  for (int i = 0; i < this->kNposd; ++i) arm2_buffer[i] = (char)this->GetPOSDETElements(i);

  int sizeof_header = sizeof(struct VME_Header);
  memcpy((char *)&(data.acq_header), arm2_buffer, sizeof_header);

  int nbyte = 0;
  int sizeof_boarddata = TRKBYTESXBOARD * NBOARDS;
  for (unsigned int i = 0; i < TRKBYTESXBOARD; i++) {
    for (int j = 0; j < NBOARDS; j++) {
      ((char *)&(data.boardout[j]))[i] = arm2_buffer[nbyte + sizeof_header];
      nbyte++;
    }
  }
  int sizeof_trailer = sizeof(struct VME_Trailer);
  memcpy((char *)&(data.acq_trailer), &(arm2_buffer[sizeof_header + sizeof_boarddata]), sizeof_trailer);

  // Check the parameter of id_board_;
  // CheckSiliconIDBoard();

  int missing = CheckSiliconIDBoard();  // header check
  if (missing != 0) {
    Utils::Printf(Utils::kPrintError, "[Convert] board ");
    fflush(stdout);
    for (int i = 1; i < NBOARDS + 1; i++) {
      if ((missing >> (i - 1)) & 1) {
        Utils::Printf(Utils::kPrintError, "%d ", i);
        fflush(stdout);
      }
    }
    Utils::Printf(Utils::kPrintError, "missing\n");
    fValidEvent = false;
  }

  if (CheckStatusRegister() != 0)  // CRS check
  {
    Utils::Printf(Utils::kPrintError, "[Convert] detect CRC error\n");
    fValidEvent = false;
  }

  ExtractSiliconData(lvl0);
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ExtractSiliconUpgrade(Level0<armclass> *lvl0) {
  char arm2_buffer[this->kNposd];
  static int checkerr=0;
  for (int i = 0; i < this->kNposd; ++i) arm2_buffer[i] = (char)this->GetPOSDETElements(i);

  short ADCVAL[NMDAQSECTIONS][NSAMPLES][NCHXPACE3][NPACE3XSEC];  // Used to extract values from data buffer
  short STRIPADC[NSILAYERS][NSTRIPXLAYER][NSAMPLES];             // Silicon strip ADC values
  char CAD[NMDAQSECTIONS][NSAMPLES][NPACE3XSEC];                 // Used to extract column addresses from data buffer

  //	unsigned int IDEV[NSILAYERS];
  //	unsigned int PACK[NSILAYERS];
  //	unsigned int BOARD_ID[NMDAQSECTIONS];
  //	unsigned int CLK[NMDAQSECTIONS][2];
  //	unsigned int EVCNT[NMDAQSECTIONS][2];

  RawPackA evA[NMDAQSECTIONS];
  RawPackB evB[NMDAQSECTIONS];

  // HEADER
  for (int iMDAQSEC = 0; iMDAQSEC < NMDAQSECTIONS; iMDAQSEC++) {
    evA[iMDAQSEC] = *(RawPackA *)(arm2_buffer + iMDAQSEC * (sizeof(RawPackA) + sizeof(RawPackB)));
    evB[iMDAQSEC] = *(RawPackB *)(arm2_buffer + iMDAQSEC * (sizeof(RawPackA) + sizeof(RawPackB)) + sizeof(RawPackA));

    memcpy((char *)ADCVAL + iMDAQSEC * 2 * (DATA_PACKET_1ST_LEN - 16), (char *)&evA[iMDAQSEC].VAL[0],
           DATA_PACKET_1ST_LEN - 16);
    memcpy((char *)ADCVAL + iMDAQSEC * 2 * (DATA_PACKET_1ST_LEN - 16) + (DATA_PACKET_1ST_LEN - 16),
           (char *)&evB[iMDAQSEC].VAL[0], DATA_PACKET_1ST_LEN - 16);

    memcpy((char *)CAD + iMDAQSEC * 36, (char *)&evB[iMDAQSEC].CAD[0], 36);

    //		IDEV[iMDAQSEC] =
    //(((evA[iMDAQSEC].ID0)<<24)&(0xFF000000))|(((evA[iMDAQSEC].ID1)<<16)&(0xFF0000))|(((evB[iMDAQSEC].ID0)<<8)&(0xFF00))|(((evB[iMDAQSEC].ID1))&(0xFF));
    //		PACK[iMDAQSEC] = (((evA[iMDAQSEC].BLK)<<16)&(0xFF0000))|(((evB[iMDAQSEC].BLK))&(0xFF));
    //		BOARD_ID[iMDAQSEC] = (((evA[iMDAQSEC].MSI)<<16)&(0x70000))|(((evB[iMDAQSEC].MSI))&(0x7));
    //		CLK[iMDAQSEC][0] = (evA[iMDAQSEC].ELCC);
    //		CLK[iMDAQSEC][1] = (evB[iMDAQSEC].ELCC);
    //		EVCNT[iMDAQSEC][0] = (evA[iMDAQSEC].EVC);
    //		EVCNT[iMDAQSEC][1] = (evB[iMDAQSEC].EVC);
  }

  // Status registers were wrongly implemented in SPS 2021 ...and event counter was not reset in SPS 2022!
  if (Arm2Params::fOperation != kSPS2021 &&
      Arm2Params::fOperation != kSPS2022) {      // Status registers were wrongly implemented in 2021
    if (CheckStatusRegisterUpgrade(evB) != 0 && checkerr==0) {  // fSiliconEvent is incremented in this method
      Utils::Printf(Utils::kPrintError, "[Convert] detect silicon error\n");
      //fValidEvent = false;
      fValidEvent = true;
      checkerr ++;
    }
    if (CheckEventNumberUpgrade(evA, evB) != 0) {  // This is not checked after last arm2 event
      Utils::Printf(Utils::kPrintError, "[Convert] event number mismatch\n");
      fValidEvent = false;
    }
  }

  // STRIP
  for (int iMDAQSEC = 0; iMDAQSEC < NMDAQSECTIONS; iMDAQSEC++) {
    for (int iCAD = 0; iCAD < NSAMPLES; iCAD++) {
      for (int iCHAN = 0; iCHAN < NCHXPACE3; iCHAN++) {
        for (int iCHIP = 0; iCHIP < NPACE3XSEC; iCHIP++) {
          int iLAYER = iMDAQSEC;
          int iPACEChip = (iCHIP % 2 == 0) ? iCHIP / 2 : 5 + (iCHIP + 1) / 2;
          if (Arm2Params::fOperation == kSPS2021)  // The hybrid on the last layer were swapped in 2021
            if (iMDAQSEC == 6) iPACEChip = (iCHIP % 2 == 0) ? 6 + iCHIP / 2 : (iCHIP + 1) / 2 - 1;
          if (iPACEChip >= 6) iPACEChip = 11 - (iPACEChip - 6);
          int iPACEChannel = 31 - iCHAN;
          int iSTRIP = (iPACEChip * NCHXPACE3) + iPACEChannel;

          //					//PLEASE REMOVE ME!
          //					if(iMDAQSEC==6 && iCHIP==0 && iCAD==1) {
          //						int ilayer=0;
          //						int iview=0;
          //						//DecodeSiliconHybSeq(iboard,isec,&ilayer,&iview,&iwaste);
          //						DecodeSiliconHybSeqUpgrade(iMDAQSEC,&ilayer,&iview);
          //						printf("%d\t%d\t%d\t%d\t%d\n", iMDAQSEC, iCHIP, ilayer, iview,
          // iSTRIP);
          //					}

          STRIPADC[iLAYER][iSTRIP][iCAD] = ADCVAL[iMDAQSEC][iCAD][iCHAN][iCHIP] / 4;
          STRIPADC[iLAYER][iSTRIP][iCAD] += 1 << 13;
        }
      }
    }
  }

  for (int iboard = 0; iboard < NMDAQSECTIONS; ++iboard) {
    for (int istrip = 0; istrip < NSTRIPXLAYER; ++istrip) {
      for (int isample = 0; isample < NSAMPLES; ++isample) {
        int ilayer = 0;
        int iview = 0;
        // DecodeSiliconHybSeq(iboard,isec,&ilayer,&iview,&iwaste);
        DecodeSiliconHybSeqUpgrade(iboard, &ilayer, &iview);
        lvl0->fPosDet[0][ilayer][iview][istrip][isample] = (float)STRIPADC[iboard][istrip][isample];
      }
    }
  }

  /*
  //Just for map debug
  for (int iMDAQSEC = 0; iMDAQSEC < NMDAQSECTIONS; iMDAQSEC++) {
          for (int iCAD = 0; iCAD < NSAMPLES; iCAD++) {
                  for (int iCHAN = 0; iCHAN < NCHXPACE3; iCHAN++) {
                          for (int iCHIP = 0; iCHIP < NPACE3XSEC; iCHIP++) {
                                  int iLAYER    = iMDAQSEC;
                                  int iPACEChip = (iCHIP % 2 == 0) ? iCHIP / 2 : 5 + (iCHIP + 1) / 2;
                                  if (Arm2Params::fOperation == kSPS2021) //The hybrid on the last layer were swapped in
  2021 if (iMDAQSEC == 6) iPACEChip = (iCHIP % 2 == 0) ? 6 + iCHIP / 2 : (iCHIP + 1) / 2 - 1; if (iPACEChip >= 6)
                                          iPACEChip   = 11 - (iPACEChip - 6);
                                  int iPACEChannel = 31 - iCHAN;
                                  int iSTRIP       = (iPACEChip * NCHXPACE3) + iPACEChannel;

                                  //                                      //PLEASE REMOVE ME!
                                  //                                      if(iMDAQSEC==6 && iCHIP==0 && iCAD==1) {
                                  //                                              int ilayer=0;
                                  //                                              int iview=0;
                                  // //DecodeSiliconHybSeq(iboard,isec,&ilayer,&iview,&iwaste);
                                  // DecodeSiliconHybSeqUpgrade(iMDAQSEC,&ilayer,&iview);
                                  //                                              printf("%d\t%d\t%d\t%d\t%d\n",
  iMDAQSEC, iCHIP, ilayer, iview, iSTRIP);
                                  //                                      }

                                  STRIPADC[iLAYER][iSTRIP][iCAD] = ADCVAL[iMDAQSEC][iCAD][iCHAN][iCHIP] / 4;
                                  STRIPADC[iLAYER][iSTRIP][iCAD] += 1 << 13;
                          }
                  }
          }
  }
  */
}

template <typename armclass, typename armcal>
unsigned int EventCal<armclass, armcal>::CounterFilter(unsigned int val, int mask, int offset) {
  int filter = 0;

  switch (mask) {
    case (8):
      filter = 0xFF;
      break;
    case (16):
      filter = 0xFFFF;
      break;
    case (24):
      filter = 0xFFFFFF;
      break;
    case (32):
      filter = 0xFFFFFFFF;
      break;
  }

  return (val >> offset) & filter;
}

template <typename armclass, typename armcal>
unsigned int EventCal<armclass, armcal>::CounterCombine(unsigned int *val) {
  return ((val[0] >> 24) & 0xFF) + ((val[1] >> 16) & 0xFF00) + ((val[2] >> 8) & 0xFF0000);
}

template <typename armclass, typename armcal>
int EventCal<armclass, armcal>::CheckSiliconIDBoard() {
  // Check the board identification number;
  for (int i = 0; i < NBOARDS; i++) {
    if ((data.boardout[i].head.Magics[0] == 0x0) && (data.boardout[i].head.Magics[1] == 0x0))
      id_board_[i] = -1;
    else
      switch ((data.boardout[i].head.JUMP_FF >> 4) & 0x3) {
        case 1:
          id_board_[i] = 1;
          break;
        case 2:
          id_board_[i] = 2;
          break;
        case 3:
          id_board_[i] = 3;
          break;
        case 0:
          id_board_[i] = 4;
          break;
      }
  }
  // return 0;

  // Check missing board; //eugenio
  int value = 0;
  // cout << endl;
  for (int i = 1; i < NBOARDS + 1; i++) {
    // cout << "Looking for board " << i;
    for (int j = 0; j < NBOARDS; j++) {
      // cout << " " << id_board_[j];
      if (id_board_[j] == i) break;
      if (j == NBOARDS - 1) value = (value | (1 << (i - 1)));
    }
    // cout << endl;
  }
  // cout << "Return value is " << hex << value << dec << endl;
  return value;
}

template <typename armclass, typename armcal>
int EventCal<armclass, armcal>::CheckStatusRegister() {
  short tmp;
  char Daqtrail_end[7];
  Daqtrail_end[0] = 0xC1;
  Daqtrail_end[1] = 0xCA;
  Daqtrail_end[2] = 0xC0;
  Daqtrail_end[3] = 0xBA;
  Daqtrail_end[4] = 0xDA;
  Daqtrail_end[5] = 0xBA;
  Daqtrail_end[6] = 0xDA;

  for (int ibo = 0; ibo < NBOARDS; ibo++) {
    err[ibo] = 0;

    if (id_board_[ibo] >= 0) {
      //----------------------------------------------------------------------------------------
      // struct DATA_Header {
      //   char Magics[2];           // 2 magic numbers for the event, 1byte+1byte
      //   char ColAdd[NSAMPL][NCHIPXBOARD];// Column address for 3 samplings of 24 PACE
      //   char JUMP_FF;             // 4 bits to check jumper ID of board
      //                             // 4 bits to check FULL FIFO flag in fifos of 4 half-hybrids
      //   char ColAddChk;           // 8bit variable for checking of column address:
      //                             //(MSB) C12 - C23 - 3x - C1 -C2 - C3 (LSB)   (x=bit not used)
      //   char DV_Al[NSAMPL][3];    // Data valid alarm for 3 columns, 24 PACE (3x3 bits).
      //                             // For each col 3 bytes contains info for PACEs: (MSB) 1,...,24 (LSB)
      //   char Alarms;              // (MSB) Master(2bit) - PLL - AE - FNE(4bits -> 4 half-hybrids) (LSB)
      //   char Counters[4];         // ((Counters >> 28) & 0xF)  ->  rejected LV1 events
      //                             //(Counters & 0xFFFFFFF)    ->  read event counter
      //   char End[6];              // 6 magic numbers (bytes) for the event
      // };
      //----------------------------------------------------------------------------------------
      //
      // Readout-board trailer.
      //----------------------------------------------------------------------------------------
      // struct DATA_Trailer {
      //   char CRC;                 // Check for consistency of data trasmission
      //   char End[7];              // 7 fixed words (bytes) to end data trasmission
      // };

      // Check of the dummy words at beginning of header
      if ((data.boardout[ibo].head.Magics[0] == DAQMAGIC1) && (data.boardout[ibo].head.Magics[1] == DAQMAGIC2)) {
      } else {
        err[ibo] |= 0x2;
      }

      // Chk col. addr.
      tmp = (int)data.boardout[ibo].head.ColAddChk;
      if (((tmp >> 7) & 0x1) || ((tmp >> 6) & 0x1) || ((tmp >> 2) & 0x1) || ((tmp >> 1) & 0x1) || (tmp & 0x1))
        err[ibo] |= 0x4;

      // Check FF status
      tmp = (int)data.boardout[ibo].head.JUMP_FF;
      if (tmp & 0xF) err[ibo] |= 0x8;

      // Check data valid alarm
      tmp = 0;
      for (int i = 0; i < NSAMPL; i++)
        if ((data.boardout[ibo].head.DV_Al[i][0] << 16) | (data.boardout[ibo].head.DV_Al[i][1] << 8) |
            data.boardout[ibo].head.DV_Al[i][2])
          tmp = 1;

      if (tmp) err[ibo] |= 0x10;

      // Check general alarm variable
      tmp = data.boardout[ibo].head.Alarms;
      if ((((tmp >> 7) & 0x1) & ((tmp >> 6) & 0x1)) || ((tmp >> 5) & 0x1) || ((tmp >> 4) & 0x1) || (tmp & 0xF))
        err[ibo] |= 0x20;

      //********************************************
      //******* Check event trailer ****************
      //********************************************

      // Perform check of dummy word in DAQ trailer
      for (int i = 0; i < (int)sizeof(data.boardout[ibo].trail.End); i++)
        if (data.boardout[ibo].trail.End[i] != Daqtrail_end[i]) err[ibo] |= 0x40;

      // Perform CRC calculation and compare with transmitted value
      crceval[ibo] = 0x0;
      crceval[ibo] = CRC8_BlockChecksum((const char *)&data.boardout[ibo],
                                        (sizeof(struct DAQ_Board_Out) - sizeof(struct DATA_Trailer)));
      if (crceval[ibo] != data.boardout[ibo].trail.CRC) {
        err[ibo] |= 0x80;
        return 1;
      }

    }  // End od if(id_board_[ibo]>=0)  (active boards)
  }    // End of loop over all boards

  return 0;
}

template <typename armclass, typename armcal>
int EventCal<armclass, armcal>::CheckStatusRegisterUpgrade(RawPackB packet[NMDAQSECTIONS]) {
  int status = 0;
  static int checkerr = 0;
  for (int iMDAQSEC = 0; iMDAQSEC < NMDAQSECTIONS; iMDAQSEC++) {
    unsigned char reg0 = packet[iMDAQSEC].REG[0];
    unsigned char reg1 = packet[iMDAQSEC].REG[1];
    unsigned char reg2 = packet[iMDAQSEC].REG[2];
    unsigned char reg3 = packet[iMDAQSEC].REG[3];
    if (reg0 & 0xFF) {
      if ((reg0 >> 4) & (0x01))
        Utils::Printf(Utils::kPrintError, "*** Warning: WFCA Silicon status error from MDAQ section %d\n", iMDAQSEC);
      if ((reg0 >> 5) & (0x01))
        Utils::Printf(Utils::kPrintError, "*** Warning: PUA Silicon status error from MDAQ section %d\n", iMDAQSEC);
      status += 8;
    }
    if (reg1 & 0xFF) {
      bool flag_reg1 = false;
      if ((reg1 >> 0) & (0x01)) {
        if (iMDAQSEC != 6) {
          flag_reg1 = true;
	  if (checkerr==0)
	  {
          Utils::Printf(Utils::kPrintError, "*** Warning: CAA Silicon status error from MDAQ section %d\n", iMDAQSEC);
          for (int is = 0; is < 3; ++is)
            for (int ic = 0; ic < NPACE3XSEC; ++ic)
              Utils::Printf(Utils::kPrintError, "\tCAD[%d][%d] = %d\n", is, ic, (int)packet[iMDAQSEC].CAD[is][ic]);
	  }
	  checkerr ++;
        }
      }
      if (flag_reg1) status += 4;
    }
    if (reg2 & 0xFF) {
      bool flag_reg2 = false;
      for (int ic = 0; ic < 8; ++ic) {
        if ((reg2 >> ic) & (0x01)) {
          if ((iMDAQSEC != 3 && ic != 7) && (iMDAQSEC != 7 && ic != 7)) {
            flag_reg2 = true;
            Utils::Printf(Utils::kPrintError, "*** Warning: DVA(%d) Silicon status error from MDAQ section %d\n", ic,
                          iMDAQSEC);
          }
        }
      }
      if (flag_reg2) status += 2;
    }
    if (reg3 & 0xFF) {
      bool flag_reg3 = false;
      // cout << hex << (int)reg3 << dec << endl;
      for (int ic = 0; ic < 4; ++ic) {
        if ((reg3 >> ic) & (0x01)) {
          if (iMDAQSEC != 7 && ic != 3) {
            flag_reg3 = false;
            Utils::Printf(Utils::kPrintError, "*** Warning: DVA(%d) Silicon status error from MDAQ section %d\n",
                          ic + 8, iMDAQSEC);
          }
        }
      }
      if (flag_reg3) status += 1;
    }
  }

  return status;
}

template <typename armclass, typename armcal>
int EventCal<armclass, armcal>::CheckEventNumberUpgrade(RawPackA packetA[NMDAQSECTIONS],
                                                        RawPackB packetB[NMDAQSECTIONS]) {
  int status = 0;

  // UInt_t evnum = fCurrentEvent+1;
  UInt_t evnum = fSiliconEvent + 1;
  for (int iMDAQSEC = 0; iMDAQSEC < NMDAQSECTIONS; iMDAQSEC++) {
    if (packetA[iMDAQSEC].EVC != evnum) {
      Utils::Printf(Utils::kPrintError, "*** Warning: Packet A -  MDAQ %d Event Counter %d instead of %d\n", iMDAQSEC,
                    packetA[iMDAQSEC].EVC, evnum);
      status += 1;
    }
    if (packetB[iMDAQSEC].EVC != evnum) {
      Utils::Printf(Utils::kPrintError, "*** Warning: Packet B -  MDAQ %d Event Counter %d instead of %d\n", iMDAQSEC,
                    packetB[iMDAQSEC].EVC, evnum);
      status += 1;
    }
  }
  // fCurrentEvent = evnum;
  fSiliconEvent = evnum;

  return status;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ExtractSiliconData(Level0<armclass> *lvl0) {
  char FIFOTRASM[NBOARDS][NFIFOXBOARD][NBLOCKSXFIFO][NBYTESXBLOCK];
  // Store a single event as transmitted
  int FIFO[NBOARDS][NFIFOXBOARD][NWORDSXFIFO];
  // Store a single event in FIFO order
  int ADC[NBOARDS][NLAYXBOARD][NCHIPXLAY][NCHXCHIP * NREADXCH];
  // Store a single event in ADC order

  //****************************************
  // Take data buffer and fill FIFOTRASM
  // with 8 bit data as they are transmitted
  //****************************************
  // FIFOTRASM[NBOARDS][NFIFOXBOARD][NBLOCKSXFIFO][NBYTESXBLOCK]

  for (int ibo = 0; ibo < NBOARDS; ibo++)
    for (int ifi = 0; ifi < NFIFOXBOARD; ifi++)
      for (int iblk = 0; iblk < NBLOCKSXFIFO; iblk++)
        for (int ibyte = 0; ibyte < NBYTESXBLOCK; ibyte++) {
          // Define integer to select bytes of FIFO-data for board 'ibo'.
          // First line jump to FIFO data for correct board
          // second line select single bytes.
          int i = ifi * NBYTESXFIFO + iblk * NBYTESXBLOCK + ibyte;
          FIFOTRASM[ibo][ifi][iblk][ibyte] = data.boardout[ibo].data[i] & 0xff;
        }

  //************************************************************************************
  //************************************************************************************
  //                    This part must be changed when data
  //                    packing changes in trasmission from
  //                    readout board to VME
  //************************************************************************************
  //************************************************************************************

  //***************************************
  // Now take FIFOTRASM, containing 8 bit
  // data and fill var FIFO with real
  // 18 bit fifo words
  //***************************************
  // FIFO[NBOARDS][NFIFOXBOARD][NWORDSXFIFO]

  for (int ibo = 0; ibo < NBOARDS; ibo++)
    for (int ifi = 0; ifi < NFIFOXBOARD; ifi++)
      for (int iblk = 0; iblk < NBLOCKSXFIFO; iblk++)
        for (int ibyte = 0; ibyte < NBYTESXBLOCK - 2; ibyte += 2) {
          int i = iblk * NWORDSXBLOCK + ibyte / 2;  // Index of fifo word
          int j = ((ibyte / 2) < 4 ? 16 : 17);      // Index of word containing the 2 LSB
          int k = ((ibyte / 2) % 4);                // Position of the 2 LSB in packed word
          FIFO[ibo][ifi][i] = ((FIFOTRASM[ibo][ifi][iblk][ibyte] & 0xFF) << 10);
          FIFO[ibo][ifi][i] |= ((FIFOTRASM[ibo][ifi][iblk][ibyte + 1] & 0xFF) << 2);
          FIFO[ibo][ifi][i] |= ((FIFOTRASM[ibo][ifi][iblk][j] >> ((3 - k) * 2)) & 0x3);
        }

  //**************************************************
  // Take FIFO and extract data on adc basis, filling
  // the new variable ADC
  //**************************************************
  // Three 12 bit chips are stored in two 18 bit fifos
  // There is a repetitive structure for each 3
  // consecutive chips. Operations to do are similar
  // depending from the position of considered chip in
  // its triplet.
  //**************************************************
  // ADC[NBOARDS][NLAYXBOARD][NCHIPXLAY][NCHXCHIP*NREADXCH]

  for (int ibo = 0; ibo < NBOARDS; ibo++)
    for (int ilay = 0; ilay < NLAYXBOARD; ilay++)
      for (int ichip = 0; ichip < NCHIPXLAY; ichip++)
        for (int iread = 0; iread < NCHXCHIP * NREADXCH; iread++) {
          if (ichip % 3 == 0) {
            int ifi = ilay * (NFIFOXBOARD / NLAYXBOARD) + (ichip / 3) * 2;
            ADC[ibo][ilay][ichip][iread] = (FIFO[ibo][ifi][iread]) & 0xFFF;
          } else if (ichip % 3 == 1) {
            int i = ilay * (NFIFOXBOARD / NLAYXBOARD) + (2 * ichip + 1) / 3 - 1;
            int j = ilay * (NFIFOXBOARD / NLAYXBOARD) + (2 * ichip + 1) / 3;
            ADC[ibo][ilay][ichip][iread] = ((FIFO[ibo][i][iread] >> 12) & 0x3F) | ((FIFO[ibo][j][iread] & 0x3F) << 6);
          } else if (ichip % 3 == 2) {
            int ifi = ilay * (NFIFOXBOARD / NLAYXBOARD) + (2 * ichip - 1) / 3;
            ADC[ibo][ilay][ichip][iread] = (FIFO[ibo][ifi][iread] >> 6) & 0xFFF;
          }
        }

  //*******************************************************
  // Now rearranging ADC values to obtain the correct
  // sequence of strips. Note that it is not anymore
  // true that board #i is used to store data for plane #i.
  // In fact new connection of hybrid sections on the 4
  // motherboards are given in the following table.
  //********************************************************
  // Considering that each MB read 2 hybrids and is therefore
  // divided into two parts, 1 and 2, each divided in two
  // sections, F and S (First and Second), due to difficulties
  // in the cabling procedures, the choosen assignment of
  // half-hybrids on the motherboard sections is as following:
  //
  // MDAQ-BOARD   sec-1F  sec-1S  sec-2F  sec-2S
  //     1          3XL     3XR     1XR     1XL
  //     2          1YR     1YL     3YL     3YR
  //     3          4XL     4XR     2XR     2XL
  //     4          2YR     2YL     4YL     4YR
  //**************************************************
  // Filling STRIP variable: remember that
  // STRIP[...][...][0,1,382,383][...] are not wire bonded!

  for (int ibo = 0; ibo < NBOARDS; ibo++)
    if (id_board_[ibo] >= 0)
      for (int ilay = 0; ilay < NLAYXBOARD; ilay++)
        for (int ichip = 0; ichip < NCHIPXLAY; ichip++) {
          int isec = -1;
          if (ichip < (int)NCHIPXLAY / 2)
            isec = 2 * ilay;
          else
            isec = 2 * ilay + 1;

          int iplane = -1;
          int iview = -1;
          int istrip = -1;
          int isampl = -1;
          int ihyb = -1;
          DecodeSiliconHybSeq(id_board_[ibo] - 1, isec, &iplane, &iview, &ihyb);
          for (int iread = 0; iread < NCHXCHIP * NREADXCH; iread++) {
            if (ihyb == 0) {
              istrip = ichip * NCHXCHIP + (NCHXCHIP - 1 - iread % NCHXCHIP);
              if (isec == 1 || isec == 3) istrip = istrip - (int)(NCHIPXLAY / 2) * NCHXCHIP;
            } else {
              istrip = ((int)(3 * NCHIPXLAY / 2) - 1 - ichip) * NCHXCHIP + (NCHXCHIP - 1 - iread % NCHXCHIP);
              if (isec == 0 || isec == 2) istrip = istrip - (int)(NCHIPXLAY / 2) * NCHXCHIP;
            }
            isampl = (int)(iread / NCHXCHIP);

            // printf("%d %d %d %d\n", iplane, iview, istrip, isampl);
            lvl0->fPosDet[0][iplane][iview][istrip][isampl] = (Double_t)ADC[ibo][ilay][ichip][iread];
          }
        }

}  //    End of ExtractData routine

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::DecodeSiliconHybSeq(int ibo, int isec, int *iplane, int *iview, int *ihyb) {
  TString temp[1];

  temp[0] = HybSeqMDAQ[ibo][isec][0];
  // printf("%c",temp[0]);

  *iplane = temp->Atoi() - 1;

  temp[0] = HybSeqMDAQ[ibo][isec][1];
  // printf("%c",temp[0]);

  if (*temp == 'X' || *temp == 'x')
    *iview = 0;
  else
    *iview = 1;

  temp[0] = HybSeqMDAQ[ibo][isec][2];
  // printf("%c\n",temp[0]);

  if (*temp == 'L' || *temp == 'l')
    *ihyb = 0;
  else
    *ihyb = 1;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::DecodeSiliconHybSeqUpgrade(int ibo, int *iplane, int *iview) {
  TString temp[1];

  temp[0] = HybSeqMDAQUpgrade[ibo][0];
  // printf("%c",temp[0]);

  *iplane = temp->Atoi() - 1;

  temp[0] = HybSeqMDAQUpgrade[ibo][1];
  // printf("%c",temp[0]);

  if (*temp == 'X' || *temp == 'x')
    *iview = 0;
  else
    *iview = 1;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CRC8_InitChecksum(unsigned char *crcvalue) {
  *crcvalue = CRC8_INIT_VALUE;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CRC8_UpdateChecksum(unsigned char *crcvalue, const void *data, int length) {
  unsigned char crctable[256] = {
      0x00, 0x07, 0x0E, 0x09, 0x1C, 0x1B, 0x12, 0x15, 0x38, 0x3F, 0x36, 0x31, 0x24, 0x23, 0x2A, 0x2D, 0x70, 0x77, 0x7E,
      0x79, 0x6C, 0x6B, 0x62, 0x65, 0x48, 0x4F, 0x46, 0x41, 0x54, 0x53, 0x5A, 0x5D, 0xE0, 0xE7, 0xEE, 0xE9, 0xFC, 0xFB,
      0xF2, 0xF5, 0xD8, 0xDF, 0xD6, 0xD1, 0xC4, 0xC3, 0xCA, 0xCD, 0x90, 0x97, 0x9E, 0x99, 0x8C, 0x8B, 0x82, 0x85, 0xA8,
      0xAF, 0xA6, 0xA1, 0xB4, 0xB3, 0xBA, 0xBD, 0xC7, 0xC0, 0xC9, 0xCE, 0xDB, 0xDC, 0xD5, 0xD2, 0xFF, 0xF8, 0xF1, 0xF6,
      0xE3, 0xE4, 0xED, 0xEA, 0xB7, 0xB0, 0xB9, 0xBE, 0xAB, 0xAC, 0xA5, 0xA2, 0x8F, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9D,
      0x9A, 0x27, 0x20, 0x29, 0x2E, 0x3B, 0x3C, 0x35, 0x32, 0x1F, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0D, 0x0A, 0x57, 0x50,
      0x59, 0x5E, 0x4B, 0x4C, 0x45, 0x42, 0x6F, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7D, 0x7A, 0x89, 0x8E, 0x87, 0x80, 0x95,
      0x92, 0x9B, 0x9C, 0xB1, 0xB6, 0xBF, 0xB8, 0xAD, 0xAA, 0xA3, 0xA4, 0xF9, 0xFE, 0xF7, 0xF0, 0xE5, 0xE2, 0xEB, 0xEC,
      0xC1, 0xC6, 0xCF, 0xC8, 0xDD, 0xDA, 0xD3, 0xD4, 0x69, 0x6E, 0x67, 0x60, 0x75, 0x72, 0x7B, 0x7C, 0x51, 0x56, 0x5F,
      0x58, 0x4D, 0x4A, 0x43, 0x44, 0x19, 0x1E, 0x17, 0x10, 0x05, 0x02, 0x0B, 0x0C, 0x21, 0x26, 0x2F, 0x28, 0x3D, 0x3A,
      0x33, 0x34, 0x4E, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5C, 0x5B, 0x76, 0x71, 0x78, 0x7F, 0x6A, 0x6D, 0x64, 0x63, 0x3E,
      0x39, 0x30, 0x37, 0x22, 0x25, 0x2C, 0x2B, 0x06, 0x01, 0x08, 0x0F, 0x1A, 0x1D, 0x14, 0x13, 0xAE, 0xA9, 0xA0, 0xA7,
      0xB2, 0xB5, 0xBC, 0xBB, 0x96, 0x91, 0x98, 0x9F, 0x8A, 0x8D, 0x84, 0x83, 0xDE, 0xD9, 0xD0, 0xD7, 0xC2, 0xC5, 0xCC,
      0xCB, 0xE6, 0xE1, 0xE8, 0xEF, 0xFA, 0xFD, 0xF4, 0xF3};

  unsigned char crc = *crcvalue;
  const unsigned char *buf = (const unsigned char *)data;

  while (length--) crc = crctable[crc ^ *buf++];

  *crcvalue = crc;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CRC8_FinishChecksum(unsigned char *crcvalue) {
  *crcvalue ^= CRC8_XOR_VALUE;
}

template <typename armclass, typename armcal>
unsigned char EventCal<armclass, armcal>::CRC8_BlockChecksum(const void *data, int length) {
  unsigned char crc;

  CRC8_InitChecksum(&crc);
  CRC8_UpdateChecksum(&crc, data, length);
  CRC8_FinishChecksum(&crc);
  return crc;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::GetCalorimeterPedestal(Level0<armclass> *ped0, Level0<armclass> *lvl0) {
  Double_t observe[5];   // z, R, K, P, x
  Double_t estimate[4];  // R, K, P, x

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
        observe[0] = lvl0->fCalorimeter[it][il][ir];  // z
        observe[1] = fCalPedPars[it][il][ir][0];      // R
        observe[2] = fCalPedPars[it][il][ir][1];      // K
        observe[3] = fCalPedPars[it][il][ir][2];      // P
        observe[4] = fCalPedPars[it][il][ir][3];      // x

        // Kalman filter
        GaussKalmanFilter(observe, estimate);

        for (Int_t i = 0; i < 4; ++i) fCalPedPars[it][il][ir][i] = estimate[i];

        ped0->fCalorimeter[it][il][ir] = estimate[3];  // x
      }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::GetPosDetPedestal(Level0<armclass> *ped0, Level0<armclass> *lvl0) {
  Double_t observe[5];   // z, R, K, P, x
  Double_t estimate[4];  // R, K, P, x

  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            observe[0] = lvl0->fPosDet[it][il][iv][ic][is];   // z
            observe[1] = fPosPedPars[it][il][iv][ic][is][0];  // R
            observe[2] = fPosPedPars[it][il][iv][ic][is][1];  // K
            observe[3] = fPosPedPars[it][il][iv][ic][is][2];  // P
            observe[4] = fPosPedPars[it][il][iv][ic][is][3];  // x

            // Kalman filter
            GaussKalmanFilter(observe, estimate);

            for (Int_t i = 0; i < 4; ++i) fPosPedPars[it][il][iv][ic][is][i] = estimate[i];

            ped0->fPosDet[it][il][iv][ic][is] = estimate[3];  // x
          }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::GaussKalmanFilter(Double_t *observe, Double_t *estimate) {
  Double_t H = 1.;
  Double_t Q = 1.;
  Double_t Psi = 1.;

  Double_t z = observe[0];
  Double_t R = observe[1];
  Double_t K = observe[2];
  Double_t P = observe[3];
  Double_t x = observe[4];

  // Kalman filter
  K = P * H / (H * P * H + R);
  x = x + K * (z - H * x);
  P = (1. - K * H) * P;
  x = Psi * x;
  P = Psi * P * Psi + Q;

  estimate[0] = R;
  estimate[1] = K;
  estimate[2] = P;
  estimate[3] = x;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CheckSaturation(Level0<armclass> *lvl0) {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il)
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir)
        if (lvl0->fCalorimeter[it][il][ir] > this->kCalSat) {
          lvl0->fCalSatFlag[it][il][ir] = true;  // saturated channel
        } else {
          lvl0->fCalSatFlag[it][il][ir] = false;
        }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ConvertToLevel1(Level1<armclass> *lvl1, Level0<armclass> *lvl0,
                                                 Level0<armclass> *ped0) {
  CopyHeader(lvl1, lvl0);
  PedestalSubtraction(lvl1, lvl0, ped0);
  PedestalCorrection(lvl1);
  TdcOffsetSubtraction(lvl1, lvl0);
  if (this->kArmIndex == Arm2Params::kArmIndex && this->fOperation == kLHC2022) SilCorrCorrection(lvl1);  // Arm2
}

template <typename armclass, typename armcal>
template <typename level_d, typename level_s>
void EventCal<armclass, armcal>::CopyHeader(level_d *dest, level_s *src) {
  dest->fRun = src->fRun;
  dest->fEvent = src->fEvent;
  dest->fGevent = src->fGevent;
  dest->fTime[0] = src->fTime[0];
  dest->fTime[1] = src->fTime[1];
  Utils::CopyVector1D(dest->fFlag, src->fFlag);
  Utils::CopyVector1D(dest->fEventFlag, src->fEventFlag);
  Utils::CopyVector2D(dest->fTdc, src->fTdc);
  Utils::CopyVector2D(dest->fTdcFlag, src->fTdcFlag);
  Utils::CopyVector1D(dest->fCounter, src->fCounter);
  Utils::CopyVector1D(dest->fQualityFlag, src->fQualityFlag);
  Utils::CopyVector1D(dest->fOpenADC, src->fOpenADC);
  Utils::CopyVector1D(dest->fScaler, src->fScaler);
  Utils::CopyVector2D(dest->fFifoCounter, src->fFifoCounter);
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::PedestalSubtraction(Level1<armclass> *lvl1, Level0<armclass> *lvl0,
                                                     Level0<armclass> *ped0) {
  Utils::CopyVector3D(lvl1->fCalorimeter, lvl0->fCalorimeter);
  Utils::SubVector3D(lvl1->fCalorimeter, ped0->fCalorimeter);

  Utils::CopyVector3D(lvl1->fCalSatFlag, lvl0->fCalSatFlag);

  Utils::CopyVector5D(lvl1->fPosDet, lvl0->fPosDet);
  Utils::SubVector5D(lvl1->fPosDet, ped0->fPosDet);

  Utils::CopyVector2D(lvl1->fFrontCounter, lvl0->fFrontCounter);
  Utils::SubVector2D(lvl1->fFrontCounter, ped0->fFrontCounter);

  Utils::CopyVector1D(lvl1->fLaser, lvl0->fLaser);
  Utils::SubVector1D(lvl1->fLaser, ped0->fLaser);

  Utils::CopyVector2D(lvl1->fZdc, lvl0->fZdc);
  Utils::SubVector2D(lvl1->fZdc, ped0->fZdc);
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::PedestalCorrection(Level1<armclass> *lvl1) {
  if (this->kArmIndex == Arm1Params::kArmIndex) {
    if (!fAveragePedestal) PedestalShiftCorrection(lvl1);
  } else if (this->kArmIndex == Arm2Params::kArmIndex) {
    if (!fIsMC) PedestalDelayedCorrection(lvl1);
  }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::PedestalShiftCorrection(Level1<armclass> *lvl1) {
  if (this->fOperation == kLHC2022) {
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
          Utils::Printf(Utils::kPrintDebugFull, "\ntower=%d, layer=%d, range=%d, shift=%lf", it, il, ir,
                        fCalPedShift[it][il][ir]);
          lvl1->fCalorimeter[it][il][ir] -= fCalPedShift[it][il][ir];
        }
      }
    }
  } else {
    Int_t irun = 0;
    if (!fIsMC) {
      // TODO: This is just a temporary solution, this check on this->kFirstRun must be moved elsewhere
      if (this->kFirstRun < 0 || this->kLastRun < 0) return;
      irun = lvl1->fRun - this->kFirstRun;
    } else {
      // irun = lvl1->fRun - 10001;
      irun = lvl1->fRun % (this->kLastRun - this->kFirstRun);
    }
    if (irun < 0 || irun > this->kLastRun - this->kFirstRun) {
      Utils::Printf(Utils::kPrintInfo, "\nlvl1->fRun=%d\nthis->kFirstRun=%d\nthis->kLastNrun=%d\nirun=%d\n", lvl1->fRun,
                    this->kFirstRun, this->kLastRun, irun);
      Utils::Printf(Utils::kPrintError, "Error: run number outside range! (run=%d)\n", irun + this->kFirstRun);
      exit(EXIT_FAILURE);
    }

    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
          Utils::Printf(Utils::kPrintDebugFull, "\ntower=%d, layer=%d, range=%d, pedcor=%lf", it, il, ir,
                        fReadTableCal.fCalPedShift[it][il][ir][irun]);
          lvl1->fCalorimeter[it][il][ir] -= fReadTableCal.fCalPedShift[it][il][ir][irun];
        }
      }
    }
  }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::PedestalDelayedCorrection(Level1<armclass> *lvl1) {
  Int_t irun = 0;
  if (!fIsMC) {
    if (this->kFirstRun < 0 || this->kLastRun < 0) return;
    irun = lvl1->fRun - this->kFirstRun;
  } else {
    irun = lvl1->fRun % (this->kLastRun - this->kFirstRun);
  }

  if (irun < 0 || irun > this->kLastRun - this->kFirstRun) {
    Utils::Printf(Utils::kPrintInfo, "\nlvl1->fRun=%d\nthis->kFirstRun=%d\nthis->kLastNrun=%d\nirun=%d\n", lvl1->fRun,
                  this->kFirstRun, this->kLastRun, irun);
    Utils::Printf(Utils::kPrintError, "Error: run number outside range! (run=%d)\n", irun + this->kFirstRun);
    exit(EXIT_FAILURE);
  }

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      for (Int_t ir = 0; ir < this->kCalNrange; ++ir) {
        Utils::Printf(Utils::kPrintDebugFull, "\ntower=%d, layer=%d, range=%d, delcor=%lf", it, il, ir,
                      fReadTableCal.fCalDelShift[it][il][ir][irun]);
        lvl1->fCalorimeter[it][il][ir] -= fReadTableCal.fCalDelShift[it][il][ir][irun];
      }
    }
  }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::TdcOffsetSubtraction(Level1<armclass> *lvl1, Level0<armclass> *lvl0) {
  double tdc_base = 0.;
  if (lvl0->fTdcFlag[1][0] >= 0) {
    tdc_base = lvl0->fTdc[1][0];
    for (int ich = 0; ich < this->kTdcNch; ++ich)
      for (int ihit = 0; ihit < this->kTdcNhit; ++ihit) {
        if (lvl0->fTdcFlag[ich][ihit] < 0) continue;
        lvl1->fTdc[ich][ihit] -= tdc_base;
      }
  }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ConvertToLevel2(Level2<armclass> *lvl2, Level1<armclass> *lvl1) {
  CopyHeader(lvl2, lvl1);
  CopyData(lvl2, lvl1);
  GainCalibration(lvl2);
  if (this->kArmIndex == Arm1Params::kArmIndex) CrossTalkCorrection(lvl2);  // Arm1
  SetErrors(lvl2);
  // SetOldErrors(lvl2);
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CopyData(Level2<armclass> *lvl2, Level1<armclass> *lvl1) {
  /* Calorimeter */

  // // using high-range if at least 1 channel saturates
  // for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //   Bool_t usehighrange = false;
  //   for (Int_t il = 0; il < this->kCalNlayer; ++il)
  //     if (lvl1->fCalSatFlag[it][il][0])
  // 	usehighrange = true;

  //   for (Int_t il = 0; il < this->kCalNlayer; ++il) {
  //     if (usehighrange) {
  // 	lvl2->fCalorimeter[it][il] = lvl1->fCalorimeter[it][il][1] * fReadTableCal.fCalRangeFactor[it][il];
  //     }
  //     else {
  // 	lvl2->fCalorimeter[it][il] = lvl1->fCalorimeter[it][il][0];
  //     }
  //     lvl2->fCalSatFlag[it][il]  = lvl1->fCalSatFlag[it][il][0];
  //   }
  // }

  // using high-range only for saturated channels
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      if (lvl1->fCalSatFlag[it][il][0]) {
        lvl2->fCalorimeter[it][il] = lvl1->fCalorimeter[it][il][1] * fReadTableCal.fCalRangeFactor[it][il];
      } else {
        lvl2->fCalorimeter[it][il] = lvl1->fCalorimeter[it][il][0];
      }
      lvl2->fCalSatFlag[it][il] = lvl1->fCalSatFlag[it][il][0];
    }
  }

  Utils::CopyVector5D(lvl2->fPosDet, lvl1->fPosDet);
  for (Int_t il = 0; il < this->kFcNlayer; ++il) {
    lvl2->fFrontCounter[il] = lvl1->fFrontCounter[il][0];  // TODO
  }
  Utils::CopyVector1D(lvl2->fLaser, lvl1->fLaser);

  // Zdc values using only wide-range values
  for (Int_t il = 0; il < this->kZdcNchannel; ++il) {
    lvl2->fZdc[il] = lvl1->fZdc[il][1];
  }

  return;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::GainCalibration(Level2<armclass> *lvl2) {
  /* Calorimeter */
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) lvl2->fCalorimeter[it][il] *= fReadTableCal.fCalConvFactor[it][il];

  /* Position Detector */
  // lvl2->fPosDet[0][3][0][7][0] = -1. * lvl2->fPosDet[0][3][0][7][0];
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is)
            lvl2->fPosDet[it][il][iv][ic][is] *= fReadTableCal.fPosConvFactor[it][il][iv][ic][is];

  // !!! TODO !!!
  /* FrontCounter */
  // for (Int_t il = 0; il < this->kFcNlayer; ++il)
  //   lvl2->fFrontCounter[il] *= fFcConvFactor[il];

  // !!! TODO !!!
  /* Zdc */

  return;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SetErrors(Level2<armclass> *lvl2) {
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (int il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            if (this->kArmIndex == Arm1Params::kArmIndex) {
              // == Arm1 ==
              // TO-DO
              // // In the old library, 10% error was temporally assigned to the all channel.
              // const Double_t factor = 0.1;
              // Double_t sig_err = factor * lvl2->fPosDet[it][il][iv][ic][is];
              // Another method, use the statistical fluctuation of # particles
              // Rough estimation of dE for MIP is about 0.0005 GeV / MIP
              Double_t mip_dE = 0.0005;
              Double_t sig_err = TMath::Sqrt(TMath::Abs(lvl2->fPosDet[it][il][iv][ic][is])/mip_dE)*mip_dE;
              Double_t ped_err = fPosPedRms[it][il][iv][ic][is] * fReadTableCal.fPosConvFactor[it][il][iv][ic][is];
              lvl2->fErrPosDet[it][il][iv][ic][is] = TMath::Sqrt(sig_err * sig_err + ped_err * ped_err);
            } else {
              // == Arm2 ==
              const Double_t ion_energy = 3.6E-9;  // 3.6 eV = 3.6 * 10^-9 GeV
              // charge signal [e-]
              Double_t charge = lvl2->fPosDet[it][il][iv][ic][is] / ion_energy;
              // signal stat. fluctuations [GeV]
              Double_t sig_err = TMath::Sqrt(TMath::Max(charge, 100.)) * ion_energy;
              // pedestal fluctuations [GeV]
              Double_t ped_err = fPosPedRms[it][il][iv][ic][is] * fReadTableCal.fPosConvFactor[it][il][iv][ic][is];
              lvl2->fErrPosDet[it][il][iv][ic][is] = TMath::Sqrt(sig_err * sig_err + ped_err * ped_err);
            }
          }
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SetOldErrors(Level2<armclass> *lvl2) {
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (int il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic)
          for (Int_t is = 0; is < this->kPosNsample; ++is) {
            const Double_t ion_energy = 3.6E-9;  // 3.6 eV = 3.6 * 10^-9 GeV
            // signal [ADC]
            Double_t adc_sig = lvl2->fPosDet[it][il][iv][ic][is] / fReadTableCal.fPosConvFactor[it][il][iv][ic][is];
            // signal stat. fluctuations [ADC]
            Double_t sig_err = TMath::Sqrt(TMath::Max(adc_sig, 100.));
            // pedestal fluctuations [ADC]
            Double_t ped_err = fPosPedRms[it][il][iv][ic][is];
            // error [ADC]
            Double_t tot_err = TMath::Sqrt(sig_err * sig_err + ped_err * ped_err);
            // error [GeV]
            lvl2->fErrPosDet[it][il][iv][ic][is] *= TMath::Sqrt(fReadTableCal.fPosConvFactor[it][il][iv][ic][is]);
          }
}

///////////////////////////////////////////////////////
/// Cross Talk Correction for Arm1 GSO bar hodoscopes.
/// The correction factors are given as a 60x60 matrix for each MAPMT.
/// \tparam armclass
/// \tparam armcal
/// \param lvl2
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CrossTalkCorrection(Level2<armclass> *lvl2) {
  if (this->kArmIndex != 0) return;

  Int_t nmap = this->kPosNlayer * this->kPosNview;  // it must be 8
  for (Int_t il = 0; il < this->kPosNlayer; ++il) {
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      Int_t imap = il * 2 + iv;

      // Fill to values into the TVectorD
      Int_t i = 0;
      Double_t tmp[60];
      for (Int_t it = 0; it < this->kPosNtower; ++it) {
        for (Int_t ip = 0; ip < this->kPosNchannel[it]; ++ip) {
          tmp[i++] = lvl2->fPosDet[it][il][iv][ip][0];
        }
      }
      TVectorD V(60, tmp);

      // Apply the correction
      V = fReadTableCal.fPosCrossTalk[imap] * V;

      // Fill back to the Level2
      i = 0;
      for (Int_t it = 0; it < this->kPosNtower; ++it) {
        for (Int_t ip = 0; ip < this->kPosNchannel[it]; ++ip) {
          lvl2->fPosDet[it][il][iv][ip][0] = V(i++);
        }
      }
    }
  }

  return;
}

///////////////////////////////////////////////////////
/// Correction of non-linearity of Sample1 for Arm2 Silicon microstrip.
/// The correction factors are given as one TSpline3 for all channels, views, layers.
/// \tparam armclass(?)
/// \tparam armcal(?)
/// \param lvl2(maybe lvl1)
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::SilCorrCorrection(Level1<armclass> *lvl1) {
  if (this->kArmIndex != 1) return;

  Double_t tmp;
  for (Int_t it = 0; it < this->kPosNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t ip = 0; ip < this->kPosNchannel[it]; ++ip) {
          if (lvl1->fPosDet[it][il][iv][ip][1] > 0) {
            // Fill the temporary variable with Level1 (only Sample1)
            tmp = lvl1->fPosDet[it][il][iv][ip][1];
            // Apply the correction
            tmp = fReadTableCal.fPosSilCorr.Eval(tmp);
            // Fill back to the Level1
            lvl1->fPosDet[it][il][iv][ip][1] = tmp;
          }
        }
      }
    }
  }
  return;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::MakeHistograms() {
  Utils::AllocateVector2D(fHCalMean, this->kCalNtower, this->kCalNrange);
  Utils::AllocateVector2D(fHCalRMS, this->kCalNtower, this->kCalNrange);

  Utils::AllocateVector4D(fHPosDetMean, this->kPosNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);
  Utils::AllocateVector4D(fHPosDetRMS, this->kPosNtower, this->kPosNlayer, this->kPosNview, this->kPosNsample);

  Utils::MakeTH1D_2D(fHCalMean, Form("a%d_cal_ped_mean", this->kArmIndex + 1), ";Layer;Mean [ADC]", this->kCalNlayer,
                     0., this->kCalNlayer);
  Utils::MakeTH1D_2D(fHCalRMS, Form("a%d_cal_ped_rms", this->kArmIndex + 1), ";Layer;RMS [ADC]", this->kCalNlayer, 0.,
                     this->kCalNlayer);

  for (Int_t it = 0; it < this->kPosNtower; ++it)
    Utils::MakeTH1D_3D(fHPosDetMean[it], Form("a%d_pos_ped_mean_%d", this->kArmIndex + 1, it), ";Channel;Mean [ADC]",
                       this->kPosNchannel[it], 0., this->kPosNchannel[it]);
  for (Int_t it = 0; it < this->kPosNtower; ++it)
    Utils::MakeTH1D_3D(fHPosDetRMS[it], Form("a%d_pos_ped_rms_%d", this->kArmIndex + 1, it), ";Channel;RMS [ADC]",
                       this->kPosNchannel[it], 0., this->kPosNchannel[it]);
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::FillHistograms() {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t ir = 0; ir < this->kCalNrange; ++ir)
      for (Int_t il = 0; il < this->kCalNlayer; ++il) {
        fHCalMean[it][ir]->SetBinContent(il + 1, fCalPedMean[it][il][ir]);
        fHCalRMS[it][ir]->SetBinContent(il + 1, fCalPedRms[it][il][ir]);
      }
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t ir = 0; ir < this->kCalNrange; ++ir) fHCalMean[it][ir]->Write();
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t ir = 0; ir < this->kCalNrange; ++ir) fHCalRMS[it][ir]->Write();

  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t is = 0; is < this->kPosNsample; ++is)
          for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
            fHPosDetMean[it][il][iv][is]->SetBinContent(ic + 1, fPosPedMean[it][il][iv][ic][is]);
            fHPosDetRMS[it][il][iv][is]->SetBinContent(ic + 1, fPosPedRms[it][il][iv][ic][is]);
          }
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t is = 0; is < this->kPosNsample; ++is) fHPosDetMean[it][il][iv][is]->Write();
  for (Int_t it = 0; it < this->kPosNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t is = 0; is < this->kPosNsample; ++is) fHPosDetRMS[it][il][iv][is]->Write();
}

/****************** Function for SPS-ADAMO ****************************/
template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::InitialiseAdamo() {
  fAdamoPedeSub.AccumClear();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::CalculatePedestalAdamo() {
  fAdamoPedeSub.CalPedestal();
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ReadPedestalAdamo(SPSAdamo *adamo, InOutCal *io_class) {
  if (!fIsMC) {
    ConvertToAdamo(adamo);
    fAdamoPedeSub.FillPedeEvent(adamo);
  }
}

template <typename armclass, typename armcal>
Int_t EventCal<armclass, armcal>::EventAdamo(SPSAdamo *adamo, InOutCal *io_class) {
  if (!fIsMC) {
    ConvertToAdamo(adamo);
    // Pedestal subtraction
    fAdamoPedeSub.SubtractPedeAverage(adamo);
    fAdamoPedeSub.SubtractCommonNoise(adamo);
  }

  return 0;
}

template <typename armclass, typename armcal>
void EventCal<armclass, armcal>::ConvertToAdamo(SPSAdamo *adamo) {
  // Utils::Printf(Utils::kPrintError,"ConvertToAdamo\n");
  // Utils::Printf(Utils::kPrintError,"nFSIL = %d\n", this->FSIL.nFSIL);
  // Utils::Printf(Utils::kPrintError,"FSIL[0-3]: %x\n", (int)this->FSIL.FSIL[0]);
  fAdamodecoder.Clear();
  char *srbuffer = (char *)(&(this->FSIL.FSIL[0]));
  char *databuffer = (char *)(&(this->FSIL.FSIL[12]));
  int srchk = 0, datachk = 0;
  datachk = fAdamodecoder.Decode(databuffer);
  srchk = fAdamodecoder.SetSR(srbuffer);
  fAdamodecoder.FillToSPSAdamo(adamo);
  adamo->run = this->Number.Run;
  adamo->number = this->Number.Number;
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventCal<Arm1Params, Arm1CalPars>;

template class EventCal<Arm2Params, Arm2CalPars>;
}  // namespace nLHCf
