#include "EventRec.hh"

#include <Math/Factory.h>
#include <Math/Functor.h>
#include <Math/Minimizer.h>
#include <Minuit2/Minuit2Minimizer.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1D.h>
#include <TROOT.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>

#include <fstream>

#include "Arm1CalPars.hh"
#include "Arm1RecPars.hh"
#include "Arm2CalPars.hh"
#include "Arm2RecPars.hh"
#include "CoordinateTransformation.hh"
#include "Utils.hh"

using namespace nLHCf;

#if !defined(__CINT__)
templateClassImp(EventRec);
#endif

typedef CoordinateTransformation CT;

double FcnCumlate(const double &x, const double &y, const double &a, const double &b) {
  return a * (TMath::ATan(x / b) + TMath::ATan(y / b) +
              TMath::ATan(x * y / (b * TMath::Sqrt(TMath::Power(b, 2.) + TMath::Power(x, 2.) + TMath::Power(y, 2.)))));
}

double Cumlate2D(const double &a1, const double &b1, const double &a2, const double &b2, const double &b3) {
  double x = 500.;
  double y = 500.;
  double tmp1 = FcnCumlate(x, y, a1, b1);
  double tmp2 = FcnCumlate(x, y, a2, b2);
  double tmp3 = FcnCumlate(x, y, 1. - a1 - a2, b3);
  return 1. / TMath::TwoPi() * (tmp1 + tmp2 + tmp3) + 0.25;
}

/* DEBUG old fit function */
double singlehitcore(double *x, double *par) {
  const double diffx = TMath::Power(x[0] - par[1], 2.);
  return par[0] * (par[2] / (diffx / par[3] + par[3]) + par[4] / (diffx / par[5] + par[5]) +
                   (1. - par[2] - par[4]) / (diffx / par[6] + par[6]));
}

/* DEBUG old FCN */
vector<Double_t> fitpos;
vector<Double_t> fitmeas;
vector<Double_t> fitmeaserr;
Int_t npfits;

void FcnSinglehitFit(Int_t & /*nPar*/, Double_t * /*grad*/, Double_t &fval, Double_t *par, Int_t /*iflag */) {
  double chi2 = 0.;
  npfits = 0;

  for (unsigned int i = 0; i < fitmeas.size(); i++) {
    double adc = fitmeas[i];
    double err = fitmeaserr[i];
    double pos = fitpos[i];
    double func = singlehitcore(&pos, par);

    if (err > 0.) {
      chi2 += TMath::Power((func - adc) / err, 2.);
      npfits++;
    }
  }

  fval = chi2;
}

/*-------------------*/
/*--- Constructor ---*/
/*-------------------*/
template <typename armclass, typename armcal, typename armrec>
EventRec<armclass, armcal, armrec>::EventRec(Bool_t en_pho, Bool_t en_neu, TString pos_rec)
    : fPhotonEnable(en_pho),
      fNeutronEnable(en_neu),
      fPosRecMode(pos_rec),
      fFastPeakSearch(true),
      fDevelopment(false),
      fFuncPhoton(NULL),
      fFuncNeutron(NULL) {
  fDebug_wait = NULL;
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    fDebug_cal_cv[it] = NULL;
    fDebug_pos_cv[it] = NULL;
    fDebug_hcal[it] = NULL;
    fDebug_lonfi[it] = NULL;
    fDebug_calx0[it] = NULL;
    for (int ip = 0; ip < this->kMaxNparticle; ++ip) fDebug_posx0[it][ip] = NULL;
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fDebug_graph[it][il][iv] = NULL;
        fDebug_tspectrum[it][il][iv] = NULL;
        fDebug_pm[it][il][iv] = NULL;
        fDebug_func[it][il][iv] = NULL;
        fDebug_fminuit[it][il][iv] = NULL;
        for (int ip = 0; ip < this->kMaxNparticle; ++ip) {
          fDebug_mh[it][il][iv][ip] = NULL;
          fDebug_true[it][il][iv][ip] = NULL;
        }
      }
    }
    fDebug_lonprotot[it] = NULL;
    for (int ip = 0; ip < this->kMaxNparticle; ++ip) {
      fDebug_lonpropar[it][ip] = NULL;
      fDebug_lonproint[it][ip] = NULL;
    }
  }
}

/*------------------*/
/*--- Destructor ---*/
/*------------------*/
template <typename armclass, typename armcal, typename armrec>
EventRec<armclass, armcal, armrec>::~EventRec() {
  if (fFuncPhoton) delete fFuncPhoton;
  if (fFuncNeutron) delete fFuncNeutron;

  if (fDebug_wait) delete fDebug_wait;
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    if (fDebug_cal_cv[it]) delete fDebug_cal_cv[it];
    if (fDebug_pos_cv[it]) delete fDebug_pos_cv[it];
    if (fDebug_hcal[it]) delete fDebug_hcal[it];
    if (fDebug_lonfi[it]) delete fDebug_lonfi[it];
    if (fDebug_calx0[it]) delete fDebug_calx0[it];
    for (int ip = 0; ip < this->kMaxNparticle; ++ip)
      if (fDebug_posx0[it][ip]) delete fDebug_posx0[it][ip];
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        if (fDebug_graph[it][il][iv]) delete fDebug_graph[it][il][iv];
        if (fDebug_tspectrum[it][il][iv]) delete fDebug_tspectrum[it][il][iv];
        // if(fDebug_pm[it][il][iv]) delete fDebug_pm[it][il][iv]; //Eugenio: comment it or big crash happens
        if (fDebug_func[it][il][iv]) delete fDebug_func[it][il][iv];
        if (fDebug_fminuit[it][il][iv]) delete fDebug_fminuit[it][il][iv];
        for (int ip = 0; ip < this->kMaxNparticle; ++ip) {
          if (fDebug_mh[it][il][iv][ip]) delete fDebug_mh[it][il][iv][ip];
          if (fDebug_true[it][il][iv][ip]) delete fDebug_true[it][il][iv][ip];
        }
      }
    }
    if (fDebug_lonprotot[it]) fDebug_lonprotot[it] = NULL;
    for (int ip = 0; ip < this->kMaxNparticle; ++ip) {
      if (fDebug_lonpropar[it][ip]) fDebug_lonpropar[it][ip] = NULL;
      if (fDebug_lonproint[it][ip]) fDebug_lonproint[it][ip] = NULL;
    }
  }
}

/*-----------------------------*/
/*--- Allocate/clear memory ---*/
/*-----------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::Initialise() {
  AllocateVectors();
  SetParameters();

#ifdef DEBUG_POS
  fDebug_wait = new TCanvas(Form("debug_wait_%d", this->kArmIndex), Form("WAIT A%d", this->kArmIndex + 1), 200, 200);
  fDebug_wait->Update();
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    string twname = it == 0 ? "ST" : "LT";
    fDebug_cal_cv[it] = new TCanvas(Form("debug_cal_cv_%d_%d", this->kArmIndex, it),
                                    Form("DEBUG CAL A%d %s", this->kArmIndex + 1, twname.c_str()), 1200, 600);
    fDebug_cal_cv[it]->Update();
    fDebug_pos_cv[it] = new TCanvas(Form("debug_pos_cv_%d_%d", this->kArmIndex, it),
                                    Form("DEBUG POS A%d %s", this->kArmIndex + 1, twname.c_str()), 1200, 600);
    fDebug_pos_cv[it]->Divide(4, 2);
    fDebug_pos_cv[it]->Update();
  }
#endif
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::AllocateVectors() {
  Int_t maxCombination = 1;
  for (Int_t ip = 1; ip <= this->kMaxNparticle; ++ip) maxCombination *= ip;

  Utils::AllocateVector3D(fChannelRange, this->kCalNtower, this->kPosNview, 2);

  Utils::AllocateVector3D(fSiSample, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector4D(fPeakX, this->kCalNtower, this->kPosNlayer, this->kPosNview, this->kMaxNparticle);
  Utils::AllocateVector4D(fPeakY, this->kCalNtower, this->kPosNlayer, this->kPosNview, this->kMaxNparticle);
  Utils::AllocateVector3D(fPeakN, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector3D(fMultiPeak, this->kCalNtower, this->kPosNlayer, this->kPosNview);
  Utils::AllocateVector5D(fCluster, this->kCalNtower, this->kMaxNparticle, this->kPosNlayer, this->kPosNview,
                          this->kClusterInformation);
  Utils::AllocateVector4D(fValCom, this->kCalNtower, this->kPosNlayer, this->kMaxNparticle, this->kMaxNparticle);
  Utils::AllocateVector4D(fPeakTrackX, this->kCalNtower, this->kPosNlayer, this->kPosNview, this->kMaxNparticle);
  Utils::AllocateVector4D(fPeakTrackY, this->kCalNtower, this->kPosNlayer, this->kPosNview, this->kMaxNparticle);
  Utils::AllocateVector2D(fmaxPeakPos, this->kCalNtower, this->kPosNlayer);
  Utils::AllocateVector2D(fmaxPeakDep, this->kCalNtower, this->kPosNlayer);
  Utils::AllocateVector2D(fChi2, this->kCalNtower, maxCombination);
  Utils::AllocateVector2D(fNdof, this->kCalNtower, maxCombination);
  Utils::AllocateVector2D(fNpar, this->kCalNtower, maxCombination);
  Utils::AllocateVector1D(fNCluster, this->kCalNtower);
  Utils::AllocateVector1D(fCombination, this->kCalNtower);
  Utils::AllocateVector3D(fMultihitCalorimeter, this->kCalNtower, this->kCalNlayer, this->kMaxNparticle);

  Utils::AllocateVector2D(fPhotonCal, this->kCalNtower, this->kCalNlayer);
  Utils::AllocateVector2D(fNeutronCal, this->kCalNtower, this->kCalNlayer);
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::ClearVectors() {
  Utils::ClearVector3D(fSiSample, -1);
  Utils::ClearVector4D(fPeakX, -1.);
  Utils::ClearVector4D(fPeakY, -1.);
  Utils::ClearVector3D(fPeakN, -1);
  Utils::ClearVector3D(fMultiPeak, false);
  Utils::ClearVector5D(fCluster, -1.);
  Utils::ClearVector4D(fValCom, -1.);
  Utils::ClearVector2D(fmaxPeakPos, -1.);
  Utils::ClearVector2D(fmaxPeakDep, -1.);
  Utils::ClearVector2D(fChi2, 1e9);
  Utils::ClearVector2D(fNdof, 0.);
  Utils::ClearVector2D(fNpar, 0);
  Utils::ClearVector1D(fNCluster, 0);
  Utils::ClearVector1D(fCombination, 0);
  Utils::ClearVector3D(fMultihitCalorimeter, 0.);

  Utils::ClearVector2D(fPhotonCal, -1.);
  Utils::ClearVector2D(fNeutronCal, -1.);
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetTrackCombMap() {
  if (this->kMaxNparticle != 3) {
    Utils::Printf(Utils::kPrintError, "EventRec::SetTrackCombMap: unknown combination map for %d particles\n",
                  this->kMaxNparticle);
    exit(EXIT_FAILURE);
  }

  fMapCombination = vector<vector<vector<vector<Int_t>>>>{{// One Particle Candidate
                                                           {{0, 0}}},
                                                          {// Two Particle Candidate
                                                           {{0, 0}, {1, 1}},
                                                           {{0, 1}, {1, 0}}},
                                                          {// Tree Particle Candidate
                                                           {{0, 0}, {1, 1}, {2, 2}},
                                                           {{0, 0}, {1, 2}, {2, 1}},
                                                           {{0, 2}, {1, 1}, {2, 0}},
                                                           {{0, 1}, {1, 0}, {2, 2}},
                                                           {{0, 2}, {1, 0}, {2, 1}},
                                                           {{0, 1}, {1, 2}, {2, 0}}}};
}

/*----------------------------*/
/*--- Event reconstruction ---*/
/*----------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::EventReconstruction(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  ClearVectors();
  if (fPosRecMode != "none") lvl3->DataClear(fDevelopment ? "all development" : "all");

  Utils::Printf(Utils::kPrintDebugFull, "\theader\n");
  HeaderInformation(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tsoftware trg\n");
  SoftwareTrigger(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tposition\n");
  PositionReconstruction(lvl2, lvl3, fPosRecMode);
  Utils::Printf(Utils::kPrintDebugFull, "\tenergy\n");
  EnergyReconstruction(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tpid\n");
  PIDReconstruction(lvl3);

#ifdef DEBUG_POS
  // if (lvl3->fPhotonMultiHit[0] || lvl3->fPhotonMultiHit[1]) {
  lvl3->Print("photon");
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fDebug_pos_cv[it]->cd(1 + il + this->kPosNlayer * iv);
        gPad->Modified();
        gPad->Update();
      }
    }
  }
  fDebug_wait->cd();
  fDebug_wait->WaitPrimitive();
  //}
#endif
}

/*---------------------------------------------------*/
/*--- Event reconstruction using True Coordinates ---*/
/*---------------------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::EventRecoTrueCoord(McEvent *mc, Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  ClearVectors();
  if (fPosRecMode != "none") lvl3->DataClear(fDevelopment ? "all development" : "all");

  Utils::Printf(Utils::kPrintDebugFull, "\theader\n");
  HeaderInformation(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tsoftware trg\n");
  SoftwareTrigger(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tposition\n");
  TrueCoordToRecoCoord(mc, lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tenergy\n");
  EnergyReconstruction(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tpid\n");
  PIDReconstruction(lvl3);

#ifdef DEBUG_POS
  lvl3->Print("photon");
  gPad->Modified();
  gPad->Update();
  fDebug_wait->cd();
  fDebug_wait->WaitPrimitive();
#endif
}

/*------------------------------------------------------------*/
/*--- Event reconstruction for debug with true information ---*/
/*------------------------------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::EventRecoDebug(McEvent *mc, Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  ClearVectors();
  if (fPosRecMode != "none") lvl3->DataClear(fDevelopment ? "all development" : "all");

  Utils::Printf(Utils::kPrintDebugFull, "\theader\n");
  HeaderInformation(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tsoftware trg\n");
  SoftwareTrigger(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tposition\n");
  PositionReconstruction(lvl2, lvl3, fPosRecMode);
  Utils::Printf(Utils::kPrintDebugFull, "\tenergy\n");
  EnergyReconstruction(lvl2, lvl3);
  Utils::Printf(Utils::kPrintDebugFull, "\tpid\n");
  PIDReconstruction(lvl3);

#ifdef DEBUG_POS
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fDebug_pos_cv[it]->cd(1 + il + this->kPosNlayer * iv);
        for (int ip = 0; ip < this->kMaxNparticle; ++ip) {
          if (fDebug_mh[it][il][iv][ip]) {
            fDebug_mh[it][il][iv][ip]->SetMarkerColor(2 + ip);
            fDebug_mh[it][il][iv][ip]->SetMarkerSize(2);
            fDebug_mh[it][il][iv][ip]->Draw("SAME");
          }
        }
        if (mc != NULL) DrawTrueInformation(mc, it, il, iv);
        gPad->Modified();
        gPad->Update();
      }  // view loop
    }    // layer loop
  }      // tower loop
  lvl3->Print("neutron");
  PrintMc(mc);
  // Check only multihit events
  // TRUE
  if (fDebug_isTrueMultihit[0] || fDebug_isTrueMultihit[1]) {  // Select only multihit events
    // if (!fDebug_isTrueMultihit[0] && !fDebug_isTrueMultihit[1]) {  // Select only singlehit events
    //    	 //RECO
    //    	 if(fHitFlag[0] || fHitFlag[1]) { //Select only multihit events
    //    		 if(!fHitFlag[0] && !fHitFlag[1]) { //Select only singlehit events
    //    			 //MISS
    //    			 if((fDebug_isTrueMultihit[0] && fHitFlag[0]<=1)
    //    					 || (fDebug_isTrueMultihit[1] && fHitFlag[1]<=1)) {
    //    				 //FAKE
    //    				 if((!fDebug_isTrueMultihit[0] && fHitFlag[0]>1)
    //    						 || (!fDebug_isTrueMultihit[1] && fHitFlag[1]>1)) {
    fDebug_wait->cd();
    fDebug_wait->WaitPrimitive();
    fDebug_wait->Update();
  }

#endif
}

/*-------------------------------------*/
/*--- Set reconstruction parameters ---*/
/*-------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetParameters() {
  SetFitFunction();
  SetChannelRange();
  SetTrackCombMap();

  fReadTableCal.ReadTables();
  // Utils::Printf(Utils::kPrintInfo,"\n finish to ReadTableCal\n");
  fReadTableRec.ReadTables();
  // Utils::Printf(Utils::kPrintInfo,"\n finish to ReadTablaRec\n");
}

/* Set channel range for each tower */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetChannelRange() {
  Double_t strip_range[this->kCalNtower][this->kPosNview][2];
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    const Double_t tower_size = this->kTowerSize[it];

    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      fChannelRange[it][iv][0] = -1;
      fChannelRange[it][iv][1] = -1;

      strip_range[it][iv][0] = -1;
      strip_range[it][iv][1] = -1;

      for (Int_t ic = 0; ic < this->kPosNchannel[it]; ++ic) {
        const Double_t pos = ChannelToPosition(it, iv, ic);

        if (this->kArmIndex == 0) { /* Arm1 */

          if (fChannelRange[it][iv][0] < 0 && pos > 0.) fChannelRange[it][iv][0] = ic;
          if (fChannelRange[it][iv][1] < 0 && pos > tower_size) fChannelRange[it][iv][1] = ic - 1;

        } else { /* Arm2 */

          if (iv == 0) {
            /* X */
            if (fChannelRange[it][iv][0] < 0 && pos > 0.) {
              fChannelRange[it][iv][0] = ic - 1;
              strip_range[it][iv][0] = pos - Arm2Params::kPosPitch;
            }
            if (fChannelRange[it][iv][1] < 0 && pos > tower_size) {
              fChannelRange[it][iv][1] = ic;
              strip_range[it][iv][1] = pos;
            }

          } else {
            /* Y */
            if (fChannelRange[it][iv][0] < 0 && pos < tower_size) {
              fChannelRange[it][iv][0] = ic - 1;
              strip_range[it][iv][0] = pos + Arm2Params::kPosPitch;
            }
            if (fChannelRange[it][iv][1] < 0 && pos < 0.) {
              fChannelRange[it][iv][1] = ic;
              strip_range[it][iv][1] = pos;
            }
          }
        }
      }  // channel loop

      // default values (= all range) if none are found above:
      if (fChannelRange[it][iv][0] < 0) fChannelRange[it][iv][0] = 0;
      if (fChannelRange[it][iv][1] < 0) fChannelRange[it][iv][1] = this->kPosNchannel[it] - 1;
    }  // view loop
  }    // tower loop

  /* Print ranges */
  Utils::Printf(Utils::kPrintInfo, "*** Position detectors channel ranges ***\n");
  Utils::Printf(Utils::kPrintInfo, "Arm%d:\n", this->kArmIndex + 1);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    Utils::Printf(Utils::kPrintInfo, "tower %d: ", it);
    fflush(stdout);
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      Utils::Printf(Utils::kPrintInfo, "[%3d, %3d] ", fChannelRange[it][iv][0], fChannelRange[it][iv][1]);
      fflush(stdout);
    }
    if (this->kArmIndex == 1) { /* Arm2 */
      Utils::Printf(Utils::kPrintInfo, " ...in Calorimeter Coordinates: ");
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        Utils::Printf(Utils::kPrintInfo, "[%.3f, %.3f] ", strip_range[it][iv][0], strip_range[it][iv][1]);
        fflush(stdout);
      }
      Utils::Printf(Utils::kPrintInfo, "\n");
    }
  }
  Utils::Printf(Utils::kPrintInfo, "*****************************************\n");
}

/*---------------------------------*/
/*--- Set position fit function ---*/
/*---------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetFitFunction() {
  FuncType ptype;
  switch (fPhotonFitType) {
    case 0:
      ptype = FuncType::kOrgMultiHitFCN;
      break;
    case 1:
      ptype = FuncType::kAreaMultiHitFCN;
      break;
    case 2:
      ptype = FuncType::kHeightMultiHitFCN;
      break;
    case 3:
      ptype = FuncType::kHeightMultiHitDL_FCN;
      break;
    default:
      ptype = FuncType::kOrgMultiHitFCN;
  }
  fFuncPhoton = PosFitBaseFCN::CreateInstance<armrec>(ptype);

  FuncType ntype;
  switch (fNeutronFitType) {
    case 0:
      ntype = FuncType::kOrgMultiHitFCN;
      break;
    case 1:
      ntype = FuncType::kAreaMultiHitFCN;
      break;
    case 2:
      ntype = FuncType::kHeightMultiHitFCN;
      break;
    case 3:
      ptype = FuncType::kHeightMultiHitDL_FCN;
      break;
    default:
      ntype = FuncType::kOrgMultiHitFCN;
  }
  fFuncNeutron = PosFitBaseFCN::CreateInstance<armrec>(ntype);
}

/*--------------------------*/
/*--- Header Information ---*/
/*--------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::HeaderInformation(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  lvl3->fRun = lvl2->fRun;
  lvl3->fEvent = lvl2->fEvent;
  lvl3->fGevent = lvl2->fGevent;
  lvl3->fTime[0] = lvl2->fTime[0];
  lvl3->fTime[1] = lvl2->fTime[1];
  Utils::CopyVector1D(lvl3->fFlag, lvl2->fFlag);
  Utils::CopyVector1D(lvl3->fCounter, lvl2->fCounter);
}

/*------------------------*/
/*--- Software trigger ---*/
/*------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SoftwareTrigger(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  SetDiscriminatorTrigger(lvl2, lvl3);
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetDiscriminatorTrigger(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < (this->kCalNlayer - this->kTrgDscNlayer + 1); ++il) {
      lvl3->fSoftwareTrigger[it] = true;
      for (Int_t jl = il; jl < (il + this->kTrgDscNlayer); ++jl) {
        Double_t threshold = this->kTrgDscThrByLayer[it][jl];
        if (this->kArmIndex == 1) {
          if (this->fOperation == kLHC2022 && jl == 5) {
            if (!lvl3->IsHighEMTrg()) {
              // This is the only case when 5 is considered
              // TODO: Warning! How to handle simulations
              continue;
            }
          }
        }
        lvl3->fSoftwareTrigger[it] = lvl3->fSoftwareTrigger[it] && lvl2->fCalorimeter[it][jl] > threshold;
      }
      if (lvl3->fSoftwareTrigger[it]) break;
    }
  }
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::FirstShowerInteraction(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  const Double_t significance = 2.;           // stdev deviation from mean
  const Double_t firstInteractionThr = 0.25;  // GeV
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    Double_t nev = 1.;
    Double_t sum = lvl2->fCalorimeter[it][0];
    Double_t sqr = TMath::Power(lvl2->fCalorimeter[it][0], 2.);
    Double_t ave = lvl2->fCalorimeter[it][0];
    Double_t rms = 0.;
    for (Int_t il = 1; il < (this->kCalNlayer - this->kTrgDscNlayer + 1); ++il) {
      double signal = lvl2->fCalorimeter[it][il];
      if (signal > firstInteractionThr && signal > ave + significance * rms) {
        lvl3->fFirstInteraction[it] = il;
        break;
      }
      ++nev;
      sum += signal;
      sqr += TMath::Power(signal, 2.);
      ave = sum / nev;
      rms = TMath::Sqrt(sqr / nev - TMath::Power(ave, 2.));
    }
  }
  //	//lvl3->fFirstInteraction[it] = -1.;
  //	for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //		for (Int_t il = 0; il < (this->kCalNlayer - this->kTrgNlayerNeutron + 1); ++il) {
  //
  //			bool trigger = true;
  //			for (Int_t jl = il; jl < (il + this->kTrgNlayerNeutron); ++jl)
  //				trigger = trigger && lvl2->fCalorimeter[it][jl] > firstInteractionThr;
  //
  //			if (trigger) {
  //				lvl3->fFirstInteraction[it] = il;
  //				break;
  //			}
  //		}
  //	}
  //
  //
  //	for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //		Int_t simplefirst = -1;
  //		Int_t uneasyfirst = -1;
  //		for (Int_t il = 0; il < (this->kCalNlayer - this->kTrgNlayerNeutron + 1); ++il) {
  //
  //			bool trigger = true;
  //			for (Int_t jl = il; jl < (il + this->kTrgNlayerNeutron); ++jl)
  //				trigger = trigger && lvl2->fCalorimeter[it][jl] > firstInteractionThr;
  //
  //			if (trigger) {
  //				simplefirst = il;
  //				break;
  //			}
  //		}
  //		double nev=1.;
  //		double sum=lvl2->fCalorimeter[it][0];
  //		double sqr=TMath::Power(lvl2->fCalorimeter[it][0], 2.);
  //		double ave=lvl2->fCalorimeter[it][0];
  //		double rms=0.;
  //		for (Int_t il = 1; il < (this->kCalNlayer - this->kTrgNlayerNeutron + 1); ++il) {
  //			double signal=lvl2->fCalorimeter[it][il];
  //			//printf("Iter %d %d %f %f %f\n", it, il, signal, ave, rms);
  //			if(signal>firstInteractionThr && signal>ave+2*rms) {
  //				uneasyfirst = il;
  //				lvl3->fFirstInteraction[it] = il;
  //				//printf("\t%d!\n", il);
  //				break;
  //			} else {
  //				++nev;
  //				sum+=signal;
  //				ave=sum/nev;
  //				sqr+=TMath::Power(signal, 2.);
  //				rms=TMath::Sqrt(sqr/nev-TMath::Power(ave, 2.));
  //			}
  //		}
  //		//if(simplefirst>=0 || uneasyfirst>=0) {
  //		if(simplefirst>=0 && simplefirst!=uneasyfirst) {
  //			printf("%d - %d %d Difference\n", it, simplefirst, uneasyfirst);
  //			for (Int_t il = 0; il < this->kCalNlayer; ++il)
  //					printf("%d - %d - %f\n", it, il, lvl2->fCalorimeter[it][il]);
  //		}
}

/*-------------------------------*/
/*--- Position reconstruction ---*/
/*-------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PositionReconstruction(Level2<armclass> *lvl2, Level3<armrec> *lvl3,
                                                                TString &mode) {
  if (mode == "none") return;

  lvl3->fPhotonFuncType = fFuncPhoton->GetFtype();
  lvl3->fNeutronFuncType = fFuncNeutron->GetFtype();

  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
  ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(0);
  // gErrorIgnoreLevel = kFatal;

  Utils::Printf(Utils::kPrintDebugFull, "\t\tSetMaxLayer\n");
  SetMaxLayer(lvl2, lvl3);
  // For Elena this mast be ignored if Arm2 and Op2022
  Utils::Printf(Utils::kPrintDebugFull, "\t\tSetSaturationFlag\n");
  SetSaturationFlag(lvl2, lvl3);
  //
  Utils::Printf(Utils::kPrintDebugFull, "\t\tSetSiSample\n");
  SetSiliconSample(lvl3);
  GetSiliconDeposit(lvl2, lvl3);  // For Elena: Add silicon energy deposit call here
  if (fPhotonEnable) {
    if (mode == "full") {
      Utils::Printf(Utils::kPrintDebugFull, "\t\tPhotonSearchPeaks\n");
      PhotonSearchPeaks(lvl2);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tPosRec\n");
      PhotonFullPositionFit(lvl2, lvl3);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tSiliconDeposit\n");
      GetPhotonSiliconArea(lvl2, lvl3);  // For Elena: Add silicon energy deposit call here
    } else if (mode == "fast") {
      if (fFastPeakSearch) PhotonSearchPeaks(lvl2);
      PhotonFastPositionRec(lvl2, lvl3);
    } else {
      Utils::Printf(Utils::kPrintError,
                    "EventRec::PositionReconstruction: wrong position reconstruction mode! Available options are "
                    "\"full\" or \"fast\"\n");
      exit(EXIT_FAILURE);
    }
    if (fDevelopment) {
      // Eugenio
      FirstShowerInteraction(lvl2, lvl3);
      MultihitIdentification(lvl2, lvl3);
      LateralShowerWidth(lvl2, lvl3, false);
      SiliconCharge(lvl2, lvl3, false);
    }
  }
  if (fNeutronEnable) {
    if (mode == "full") {
      Utils::Printf(Utils::kPrintDebugFull, "\t\tNeutronSearchPeaks\n");
      NeutronSearchPeaks(lvl2);  // not needed in "fast" for neutrons
      Utils::Printf(Utils::kPrintDebugFull, "\t\tPosRec\n");
      NeutronFullPositionFit(lvl2, lvl3);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tSiliconDeposit\n");
      GetNeutronSiliconArea(lvl2, lvl3);  // For Elena: Add silicon energy deposit call here
    } else if (mode == "fast") {
      if (fFastPeakSearch) NeutronSearchPeaks(lvl2);  // not needed in "fast" for neutrons
      NeutronFastPositionRec(lvl2, lvl3);
    } else {
      Utils::Printf(Utils::kPrintError,
                    "EventRec::PositionReconstruction: wrong position reconstruction mode! Available options are "
                    "\"full\" or \"fast\"\n");
      exit(EXIT_FAILURE);
    }
    if (fDevelopment) {
      // Eugenio
      FirstShowerInteraction(lvl2, lvl3);
      MultihitIdentification(lvl2, lvl3);
      LateralShowerWidth(lvl2, lvl3, true);
      SiliconCharge(lvl2, lvl3, true);
    }
  }
}

/*----------------------------------------------------*/
/*--- Position extrapolation from true coordinates ---*/
/*----------------------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrueCoordToRecoCoord(McEvent *mc, Level2<armclass> *lvl2,
                                                              Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    const int ntruehit = mc->CheckParticleInTower(this->kArmIndex, it, 1., 0.);
    double TrueColPos[2] = {-1000., -1000.};
    double TrueCalPos[2] = {-1000., -1000.};
    if (ntruehit > 0) {
      // TODO [TrackProjection]: Check this change
      TVector3 lhc_pos = mc->fReference[0]->Position();
      TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(this->kArmIndex, it, lhc_pos);
      TrueCalPos[0] = cal_pos.X();
      TrueCalPos[1] = cal_pos.Y();
      //
    }
    lvl3->fPhotonMultiHit[it] = false;
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      lvl3->fPosMaxLayer[it][iv] = 0;
      lvl3->fPhotonPosition[it][iv] = TrueCalPos[iv];
      lvl3->fPhotonPositionMH[it][iv][0] = TrueCalPos[iv];
      lvl3->fNeutronPosition[it][iv] = TrueCalPos[iv];
    }
  }
}

/*** Conversion from channel to position (relative to calorimeter) WITHOUT correction ***/
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::ChannelToPosition(Int_t tower, Int_t view, Double_t ch) {
  TVector3 pos = CT::ChannelToCalorimeter(this->kArmIndex, tower, -1, -1, ch, ch);
  return view == 0 ? pos.X() : pos.Y();
}

/*** Conversion from channel to position (relative to calorimeter) WITH shift (not tilt) correction ***/
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::ChannelToPosition(Int_t tower, Int_t layer, Int_t view, Double_t ch) {
  TVector3 pos = CT::ChannelToCalorimeter(this->kArmIndex, tower, layer, layer, ch, ch);
  return view == 0 ? pos.X() : pos.Y();
}

/*** Conversion from position to channel (relative to calorimeter) WITHOUT correction ***/
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::PositionToChannel(Int_t tower, Int_t view, Double_t ch) {
  TVector3 pos = CT::CalorimeterToChannel(this->kArmIndex, tower, -1, 1, ch, ch);
  return view == 0 ? pos.X() : pos.Y();
}

/*** Conversion from channel to position (relative to calorimeter) WITH shift (not tilt) correction ***/
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::PositionToChannel(Int_t tower, Int_t layer, Int_t view, Double_t ch) {
  TVector3 pos = CT::CalorimeterToChannel(this->kArmIndex, tower, layer, layer, ch, ch);
  return view == 0 ? pos.X() : pos.Y();
}

/*** Check the position detector X-Y couple with the maximum energy deposit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetMaxLayer(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    // same silicon detector is used for both towers in Arm2
    const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;
    // sample #1 is used for Arm2 (no sample choice for Arm1)
    const Int_t pos_sample = this->kArmIndex == 0 ? 0 : 1;

    Double_t max_sum = -1E30;

    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      Double_t sum = 0.;

      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        const Int_t ch_low = fChannelRange[it][iv][0];
        const Int_t ch_high = fChannelRange[it][iv][1];
        Utils::Printf(Utils::kPrintDebugFull, "\t\t\tread signal\n");
        for (Int_t ic = ch_low; ic <= ch_high; ++ic) {
          sum += lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
        }
        Utils::Printf(Utils::kPrintDebugFull, "\t\t\tdone\n");
      }

      if (sum > max_sum || il == 0) {
        max_sum = sum;
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) lvl3->fPosMaxLayer[it][iv] = il;
      }
    }

    /* Last two layers of Arm2 have X and Y views separated */
    /* -> check also the 3X-4Y couple                       */
    if (this->kArmIndex == 1) {
      Double_t sum = 0.;

      { /* 3X */
        const Int_t il = 2;
        const Int_t iv = 0;

        const Int_t ch_low = fChannelRange[it][iv][0];
        const Int_t ch_high = fChannelRange[it][iv][1];

        for (Int_t ic = ch_low; ic <= ch_high; ++ic) sum += lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
      }
      { /* 4Y */
        const Int_t il = 3;
        const Int_t iv = 1;

        const Int_t ch_low = fChannelRange[it][iv][0];
        const Int_t ch_high = fChannelRange[it][iv][1];

        for (Int_t ic = ch_low; ic <= ch_high; ++ic) sum += lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
      }

      if (sum > max_sum) {
        max_sum = sum;
        lvl3->fPosMaxLayer[it][0] = 2;
        lvl3->fPosMaxLayer[it][1] = 3;
      }
    }
  }  // tower loop
}

/*** Check position detectors saturation ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetSaturationFlag(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    // same silicon detector is used for both towers in Arm2
    const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        for (Int_t is = 0; is < this->kPosNsample; ++is) {
          lvl3->fSilSaturated[it][il][iv][is] = false;
          const Int_t ch_low = fChannelRange[it][iv][0];
          const Int_t ch_high = fChannelRange[it][iv][1];
          for (Int_t ic = ch_low; ic <= ch_high; ++ic) {
            Double_t sat_thr = this->kPosSaturationThr * fReadTableCal.fPosConvFactor[pos_tower][il][iv][ic][is];
            if (lvl2->fPosDet[pos_tower][il][iv][ic][is] > sat_thr) {
              lvl3->fSilSaturated[it][il][iv][is] = true;
              break;
            }
          }
        }  // sample loop
      }    // view loop
    }      // layer loop

  }  // tower loop
}

/*** Set silicon sample used (Arm2 only) ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SetSiliconSample(Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        if (this->kArmIndex == 0) {  // Arm1
          fSiSample[it][il][iv] = 0;
        } else {  // Arm2
          if (lvl3->fSilSaturated[it][il][iv][1])
            fSiSample[it][il][iv] = 0;
          else
            fSiSample[it][il][iv] = 1;
        }
      }  // view loop
    }    // layer loop
  }      // tower loop
}

/*** Search peaks with TSpectrum class ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonSearchPeaks(Level2<armclass> *lvl2) {
  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) PhotonSearchPeaks(lvl2, it, il, iv);
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonSearchPeaks(Level2<armclass> *lvl2, Int_t it, Int_t il, Int_t iv) {
  /* Set TSpectrum parameters */
  TSpectrum::SetAverageWindow(this->kAvgWindowPhoton);
  TSpectrum::SetDeconIterations(this->kDeconIterPhoton);

  // sample #1 is used for Arm2 (no sample choice for Arm1)
  const Int_t pos_sample = fSiSample[it][il][iv];
  // same silicon detector is used for both towers in Arm2
  const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

  const Int_t ch_low = fChannelRange[it][iv][0];
  const Int_t ch_high = fChannelRange[it][iv][1];
  const Int_t n_ch = ch_high - ch_low + 1;

  /* Fill input histogram for TSpectrum */
  const Double_t h_low = Double_t(ch_low) - 0.5;
  const Double_t h_high = Double_t(ch_high) + 0.5;
  // TH1D hsource("hsource", Form("Tower %d, Layer %d, View %d", it, il, iv),
  // 	       n_ch, h_low, h_high);
  // for (Int_t ic = ch_low; ic <= ch_high; ++ic)
  //   hsource.Fill(Double_t(ic), lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample]);
  Double_t source[n_ch];
  Double_t dest[n_ch];
  // Float_t source[n_ch];
  // Float_t dest[n_ch];
  for (Int_t ib = 0; ib < n_ch; ++ib) {
    Int_t ic = ch_low + ib;
    source[ib] = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
  }
  /* Find peaks */
  TSpectrum tspec;
  // const Int_t peaks_found = tspec.Search(&hsource, this->kPeakSigmaPhoton, "nodraw", this->kPeakRatioThrPhoton);
  const Int_t peaks_found =
      tspec.SearchHighRes(source, dest, n_ch, this->kPeakSigmaPhoton, this->kPeakRatioThrPhoton * 100., kTRUE,
                          this->kDeconIterPhoton, kTRUE, this->kAvgWindowPhoton);
  // printf("n = %d\n", peaks_found);

  /* Retrieve results */
  fPeakN[it][il][iv] = 0;
  for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
    fPeakX[it][il][iv][ip] = (ch_low + ch_high) / 2.;
    fPeakY[it][il][iv][ip] = 0.;
  }
  if (peaks_found > 0) {
    // TList *functions = hsource.GetListOfFunctions();
    // TPolyMarker *pm = (TPolyMarker*)functions->FindObject("TPolyMarker");
    // const Int_t     n_peak = pm->GetN();
    // const Double_t *x_peak = pm->GetX();
    // const Double_t *y_peak = pm->GetY();
    const Int_t n_peak = peaks_found;
    Double_t *x_peak = new Double_t[n_peak];
    Double_t *y_peak = new Double_t[n_peak];
    for (Int_t ip = 0; ip < n_peak; ++ip) {
      Double_t xspec = tspec.GetPositionX()[ip];
      Int_t ix = Int_t(xspec + 0.5);
      x_peak[ip] = xspec + ch_low;
      if (ix < n_ch && ix >= 0)
        y_peak[ip] = dest[ix];
      else
        y_peak[ip] = -1.;
      // printf("x = %lf, y = %lf\n", x_peak[ip], y_peak[ip]);
    }

    /* sort from higher to lower peak*/
    Int_t isort[n_peak];
    TMath::Sort<Double_t, Int_t>(n_peak, y_peak, isort);

    /* store highest peaks */
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
      const Double_t xp = ip < n_peak ? x_peak[isort[ip]] : -1.;
      const Double_t yp = ip < n_peak ? y_peak[isort[ip]] : -1.;
      fPeakX[it][il][iv][ip] = xp;
      fPeakY[it][il][iv][ip] = yp;
    }
    if (fPeakY[it][il][iv][0] > this->kPeakAbsThrPhoton) fPeakN[it][il][iv] = 1;

    /* if secondary peak is over threshold, set multi-peak flag */
    for (Int_t ip = 1; ip < n_peak; ++ip)
      if ((fPeakY[it][il][iv][ip] / fPeakY[it][il][iv][0] > this->kPeakRatioThrPhoton) &&
          (fPeakY[it][il][iv][ip] > this->kPeakAbsThrPhoton))
        ++fPeakN[it][il][iv];
      else
        break;

    delete[] x_peak;
    delete[] y_peak;
  }
}

/*** Position full fit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonFullPositionFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        fMultiPeak[it][il][iv] = fPeakN[it][il][iv] > 1;
        if (fPeakN[it][il][iv] > 0) {
          PhotonMultiHitFit(lvl2, lvl3, it, il, iv, fPeakN[it][il][iv]);
          // PhotonOldFit(lvl2, lvl3, it, il, iv);
        }
#ifdef DEBUG_POS
        else
          DrawPositionFit(NULL, lvl2, it, il, iv, NULL, (PosFitBaseFCN *)NULL, NULL);
#endif
      }  // view loop
    }    // layer loop

    /* Get position from layer with max energy deposit */
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      lvl3->fPhotonPosition[it][iv] =
          ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv,
                            fFuncPhoton->GetPos(lvl3->fPhotonPosFitPar[it][lvl3->fPosMaxLayer[it][iv]][iv]));
      /* Multi-hit position reconstruction */
      Int_t layer = lvl3->fPosMaxLayer[it][iv];
      if (!fMultiPeak[it][lvl3->fPosMaxLayer[it][iv]][iv])  // no multi-hit in max layer
        for (Int_t il = 0; il < this->kPosNlayerEM; ++il)   // search it in other layer
          if (fMultiPeak[it][il][iv]) {
            layer = il;
            break;
          }
      for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
        Double_t pos = fFuncPhoton->GetPos(lvl3->fPhotonPosFitPar[it][layer][iv], ip);
        if (pos >= 0.) lvl3->fPhotonPositionMH[it][iv][ip] = ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv, pos);
      }
    }

    /* Set multi-hit flag if there is a multi-peak in at least one layer */
    lvl3->fPhotonMultiHit[it] = false;
    for (Int_t il = 0; il < this->kPosNlayerEM; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        if (fMultiPeak[it][il][iv]) lvl3->fPhotonMultiHit[it] = true;
    if (lvl3->fPhotonMultiHit[it]) {
      for (Int_t il = 0; il < this->kPosNlayerEM; ++il) {
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          for (Int_t ih = 1; ih < this->kMaxNparticle; ++ih) {
            TVector2 pos1(lvl3->fPhotonPositionMH[it][iv][0], lvl3->fPhotonPositionMH[it][iv][0]);
            TVector2 pos2(lvl3->fPhotonPositionMH[it][iv][ih], lvl3->fPhotonPositionMH[it][iv][ih]);
            if (pos2.X() < 0. || pos2.Y() < 0.) continue;
            Double_t dist = (pos1 - pos2).Mod();
            Double_t max_dist = TMath::Sqrt(2.) * this->kTowerSize[it];
            if (dist < this->kMinDistPhoton || dist > max_dist) {
              lvl3->fPhotonMultiHit[it] = false;
              break;
            }
          }
        }
      }
    }
  }  // tower loop
}

/*** Position fast reconstruction ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonFastPositionRec(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    // same silicon detector is used for both towers in Arm2
    const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      // sample #1 is used for Arm2 (no sample choice for Arm1)
      const Int_t pos_sample = fSiSample[it][lvl3->fPosMaxLayer[it][iv]][iv];

      const Int_t ch_low = fChannelRange[it][iv][0];
      const Int_t ch_high = fChannelRange[it][iv][1];
      const Int_t n_ch = ch_high - ch_low + 1;

      /* get maximum position */
      Int_t max_ch;
      Double_t max_value = -1E30;
      for (Int_t ic = ch_low; ic <= ch_high; ++ic)
        if (fReadTableRec.fMaskPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample]) {
          Double_t val = lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample];
          if (val > max_value) {
            max_value = val;
            max_ch = ic;
          }
        }
      /* geometric mean */
      const Int_t width = 2;  // range of the mean
      const Int_t mlow = max_ch - width;
      const Int_t mhigh = max_ch + width;
      if (mlow >= ch_low && mhigh <= ch_high) {
        Double_t sum = 0.;
        Double_t weight_sum = 0.;
        for (Int_t ic = mlow; ic <= mhigh; ++ic)
          if (fReadTableRec.fMaskPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample] &&
              lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample] > 0.) {
            sum += lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample] * Double_t(ic);
            weight_sum += lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample];
          }
        lvl3->fPhotonPosition[it][iv] = ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv, sum / weight_sum);
      } else {
        lvl3->fPhotonPosition[it][iv] = ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv, Double_t(max_ch));
      }
      lvl3->fPhotonPositionMH[it][iv][0] = lvl3->fPhotonPosition[it][iv];
    }  // view loop

    /* Set multi-hit flag if there is a multi-peak in at least one layer */
    lvl3->fPhotonMultiHit[it] = false;
    for (Int_t il = 0; il < this->kPosNlayerEM; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        if (fPeakN[it][il][iv] > 1) lvl3->fPhotonMultiHit[it] = true;
#ifdef DEBUG_POS
        DrawPositionFit(NULL, lvl2, it, il, iv, NULL, (PosFitBaseFCN *)NULL, NULL);
#endif
      }
  }  // tower loop
}

/*** Single-hit fit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonSingleHitFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3, Int_t it,
                                                            Int_t il, Int_t iv) {
  PhotonMultiHitFit(lvl2, lvl3, it, il, iv, 1);
}

/*** Multi-hit fit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonMultiHitFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3, Int_t it,
                                                           Int_t il, Int_t iv, Int_t npeak) {
  // sample #1 is used for Arm2 (no sample choice for Arm1)
  const Int_t pos_sample = fSiSample[it][il][iv];

  // same silicon detector is used for both towers in Arm2
  const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

  const Int_t ch_low = fChannelRange[it][iv][0];
  const Int_t ch_high = fChannelRange[it][iv][1];
  const Int_t n_ch = ch_high - ch_low + 1;

  /* Set function to be minimised */
  fFuncPhoton->SetNhit(npeak);

  // Double_t xp[n_ch];
  // Double_t yp[n_ch];
  // Double_t ey[n_ch];
  vector<Double_t> xp;
  vector<Double_t> yp;
  vector<Double_t> ey;
  for (Int_t ip = 0; ip < n_ch; ++ip) {
    const Int_t ic = ch_low + ip;
    // if single-hit, get only strips within +-50 from estimated peak
    if ((npeak > 1 || (Double_t(ic) > fPeakX[it][il][iv][0] - 50. && Double_t(ic) < fPeakX[it][il][iv][0] + 50.)) &&
        fReadTableRec.fMaskPosDet[pos_tower][il][iv][ic][pos_sample]) {
      // xp[ip] = Double_t(ic);
      // yp[ip] = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
      // ey[ip] = lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample];
      xp.push_back(Double_t(ic));
      yp.push_back(lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample]);
      ey.push_back(lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample]);
    }
  }
  // fFuncPhoton->SetDataVectors(n_ch, xp, yp, ey);
  fFuncPhoton->SetDataVectors(xp.size(), xp.data(), yp.data(), ey.data());
  TGraphErrors pos_graph(xp.size(), xp.data(), yp.data(), NULL, ey.data());

  /* 1st fit iteration: standard ROOT fit */
  TF1 func("func", fFuncPhoton, &PosFitBaseFCN::FitFunc, ch_low, ch_high, fFuncPhoton->GetNparam());
  // set initial parameters of func
  fFuncPhoton->InitializePars(&func, fPeakX[it][il][iv], fPeakY[it][il][iv]);
  // printf("init pars:   "); fflush(stdout);
  // for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
  //   printf(" %lf", func.GetParameter(ipar)); fflush(stdout);
  // }
  // printf("\n");
  // fit
  pos_graph.Fit(&func, "IQN");

  // /////
  // cerr << "PhotonMultihitFit tower " << it << " layer " << il << " view " << iv << endl;
  // cerr << "1st fit results : \n";
  // for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
  //   cerr << setw(6) << setprecision(3) << fixed << func.GetParameter(ipar) << " ";
  // }
  // cerr << endl << "1st fit errors : \n";
  // for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
  //   cerr << setw(6) << setprecision(3) << fixed << func.GetParError(ipar) << " ";
  // }
  // cerr << endl;
  // //////

  /* 2nd fit iteration: Minuit2 minimization */
  Utils::Printf(Utils::kPrintDebugFull, "Make minimiser\n");
  /* create minimizer giving a name and a name (optionally) for the specific
   * algorithm
   * possible choices are:
   *   min_name                algo_name
   * Minuit /Minuit2       Migrad, Simplex, Combined, Scan (default is Migrad)
   * Minuit2               Fumili2
   * Fumili
   * GSLMultiMin           ConjugateFR, ConjugatePR, BFGS,
   *                       BFGS2, SteepestDescent
   * GSLMultiFit
   * GSLSimAn
   * Genetic
   */
  const Char_t *min_name = "Minuit2";
  const Char_t *algo_name = "";
  ROOT::Math::Minimizer *minimum = ROOT::Math::Factory::CreateMinimizer(min_name, algo_name);

  Utils::Printf(Utils::kPrintDebugFull, "Associate FCN to minimiser\n");
  ROOT::Math::Functor functor(fFuncPhoton, &PosFitBaseFCN::DoEval, fFuncPhoton->GetNparam());
  minimum->SetFunction(functor);

  /* set tolerance , etc... */
  Utils::Printf(Utils::kPrintDebugFull, "Set minimiser parameters\n");
  minimum->SetMaxFunctionCalls(500000);  // for Minuit/Minuit2 (default: 1000000)
  minimum->SetMaxIterations(10000);      // for GSL (default: 10000)
  minimum->SetTolerance(1E-10);          // (default: 0.001)
  minimum->SetPrecision(1E-10);          // (default: -1 (= autoset by minimizer))
  minimum->SetPrintLevel(-1);            // suppress all kinds of message
  minimum->SetErrorDef(1.);              // value above the minimum to define error on parameter(s) (default=1.)

  /* Set the free variables to be minimized */
  // for (Int_t ih = 0; ih < fFuncPhoton->GetNhit(); ++ih) {
  //   const Int_t ipar = ih * fFuncPhoton->GetNparamSingle();
  //   minimum->SetVariable(0 + ipar, Form("Position  (peak %d)", ih), func.GetParameter(0 + ipar), 1);
  //   minimum->SetVariable(1 + ipar, Form("1st area  (peak %d)", ih), func.GetParameter(1 + ipar), 1);
  //   minimum->SetVariable(2 + ipar, Form("1st width (peak %d)", ih), func.GetParameter(2 + ipar), 1);
  //   minimum->SetVariable(3 + ipar, Form("2nd area  (peak %d)", ih), func.GetParameter(3 + ipar), 1);
  //   minimum->SetVariable(4 + ipar, Form("2nd width (peak %d)", ih), func.GetParameter(4 + ipar), 1);
  //   minimum->SetVariable(5 + ipar, Form("3rd area  (peak %d)", ih), func.GetParameter(5 + ipar), 1);
  //   minimum->SetVariable(6 + ipar, Form("3rd width (peak %d)", ih), func.GetParameter(6 + ipar), 1);
  // }

  for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
    Bool_t f_fixed = false, f_limited = false;
    Double_t step;

    // Check fixed value (if the parameter was fixed, error from func should be zero)
    // fixed values (defiend in PosFitFNC_XX::InitializePars)
    if (func.GetParError(ipar) < 1.E-9) f_fixed = true;

    // Check if it is a limited parameter if no limit, pmin = pmax = default value
    Double_t pmin, pmax;
    func.GetParLimits(ipar, pmin, pmax);
    if (pmax - pmin > 1.E-9) f_limited = true;

    // Step value
    step = (this->kArmIndex == Arm1Params::kArmIndex) ? 0.001 : 1; // TODO Optimization is needed.
    if (f_fixed) {
      minimum->SetFixedVariable(ipar, Form("par %d (fixed)", ipar), func.GetParameter(ipar));
    } else if (f_limited) {
      minimum->SetLimitedVariable(ipar, Form("par %d (limited)", ipar), func.GetParameter(ipar), step, pmin, pmax);
    } else {
      minimum->SetVariable(ipar, Form("par %d", ipar), func.GetParameter(ipar), step);
    }
  }

  /* Do the minimization */
  Utils::Printf(Utils::kPrintDebugFull, "Minimise\n");
  minimum->Minimize();

  // minimum->FixVariable(0);
  // minimum->ReleaseVariable(1);
  // minimum->Minimize();

  // minimum->ReleaseVariable(0);
  // minimum->Minimize();

  /* Copy result to Level3 */
  const Double_t *min_par = minimum->X();
  // const Double_t *min_err = minimum->Errors();
  // ROOT::Minuit2::Minuit2Minimizer *minuit2 = (ROOT::Minuit2::Minuit2Minimizer *)minimum;
  // cout << minuit2->State().IsValid() << endl;
  // if (minuit2->State().IsValid())
  for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
    Double_t minos_low, minos_high;
    minimum->GetMinosError(ipar, minos_low, minos_high);
    lvl3->fPhotonPosFitPar[it][il][iv][ipar] = min_par[ipar];
    lvl3->fPhotonPosFitErr[it][il][iv][ipar][0] = minos_low;
    lvl3->fPhotonPosFitErr[it][il][iv][ipar][1] = minos_high;
  }
  lvl3->fPhotonPosFitChi2[it][il][iv] = minimum->MinValue();
  lvl3->fPhotonPosFitNDF[it][il][iv] = fFuncPhoton->GetNpoint() - fFuncPhoton->GetNparam();
  lvl3->fPhotonPosFitEDM[it][il][iv] = minimum->Edm();

  // /////
  // cerr << "2nd fit results : \n";
  // for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
  //   cerr << setw(6) << setprecision(3) << fixed << lvl3->fPhotonPosFitPar[it][il][iv][ipar] << " ";
  // }
  // cerr << endl << "2nd fit errors (low): \n";
  // for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
  //   cerr << setw(6) << setprecision(3) << fixed << lvl3->fPhotonPosFitErr[it][il][iv][ipar][0] << " ";
  // }
  // cerr << endl << "2nd fit errors (high): \n";
  // for (Int_t ipar = 0; ipar < fFuncPhoton->GetNparam(); ++ipar) {
  //   cerr << setw(6) << setprecision(3) << fixed << lvl3->fPhotonPosFitErr[it][il][iv][ipar][1] << " ";
  // }
  // cerr << endl;
  // //////

#ifdef DEBUG_POS
  DrawPositionFit(NULL, lvl2, it, il, iv, &func, fFuncPhoton, min_par);
#endif

  delete minimum;
}

/*** Search peaks with TSpectrum class ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronSearchPeaks(Level2<armclass> *lvl2) {
  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) NeutronSearchPeaks(lvl2, it, il, iv);
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronSearchPeaks(Level2<armclass> *lvl2, Int_t it, Int_t il, Int_t iv) {
  // Eugenio: need to solve all TODO here

  /* Set TSpectrum parameters */
  TSpectrum::SetAverageWindow(this->kAvgWindowNeutron);
  TSpectrum::SetDeconIterations(this->kDeconIterNeutron);

  // sample #1 is used for Arm2 (no sample choice for Arm1)
  const Int_t pos_sample = fSiSample[it][il][iv];
  // same silicon detector is used for both towers in Arm2
  const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

  const Int_t ch_low = fChannelRange[it][iv][0];
  const Int_t ch_high = fChannelRange[it][iv][1];
  const Int_t n_ch = ch_high - ch_low + 1;

  /* Fill input histogram for TSpectrum */
  const Double_t h_low = Double_t(ch_low) - 0.5;
  const Double_t h_high = Double_t(ch_high) + 0.5;
  // TH1D hsource("hsource", Form("Tower %d, Layer %d, View %d", it, il, iv),
  // 	       n_ch, h_low, h_high);
  // for (Int_t ic = ch_low; ic <= ch_high; ++ic)
  //   hsource.Fill(Double_t(ic), lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample]);
  Double_t source[n_ch];
  Double_t dest[n_ch];
  // Float_t source[n_ch];
  // Float_t dest[n_ch];
  for (Int_t ib = 0; ib < n_ch; ++ib) {
    Int_t ic = ch_low + ib;
    source[ib] = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
  }
  /* Find peaks */
  TSpectrum tspec;
  // const Int_t peaks_found = tspec.Search(&hsource, this->kPeakSigmaNeutron, "nodraw", this->kPeakRatioThrNeutron);
  const Int_t peaks_found =
      tspec.SearchHighRes(source, dest, n_ch, this->kPeakSigmaNeutron, this->kPeakRatioThrNeutron * 100., kTRUE,
                          this->kDeconIterNeutron, kTRUE, this->kAvgWindowNeutron);
  // printf("n = %d\n", peaks_found);

  /* Retrieve results */
  fPeakN[it][il][iv] = 0;
  for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
    fPeakX[it][il][iv][ip] = (ch_low + ch_high) / 2.;
    fPeakY[it][il][iv][ip] = 0.;
  }

#ifdef DEBUG_POS
  if (fDebug_tspectrum[it][il][iv]) {
    delete fDebug_tspectrum[it][il][iv];
    fDebug_tspectrum[it][il][iv] = NULL;
  }
  Double_t strp[n_ch];
  for (Int_t ib = 0; ib < n_ch; ++ib) strp[ib] = ch_low + ib;
  fDebug_tspectrum[it][il][iv] = new TGraph(n_ch, strp, dest);
#endif

  if (peaks_found > 0) {
    // TList *functions = hsource.GetListOfFunctions();
    // TPolyMarker *pm = (TPolyMarker*)functions->FindObject("TPolyMarker");
    // const Int_t     n_peak = pm->GetN();
    // const Double_t *x_peak = pm->GetX();
    // const Double_t *y_peak = pm->GetY();
    const Int_t n_peak = peaks_found;
    Double_t *x_peak = new Double_t[n_peak];
    Double_t *y_peak = new Double_t[n_peak];
    for (Int_t ip = 0; ip < n_peak; ++ip) {
      Double_t xspec = tspec.GetPositionX()[ip];
      Int_t ix = Int_t(xspec + 0.5);
      x_peak[ip] = xspec + ch_low;
      if (ix < n_ch && ix >= 0)
        y_peak[ip] = dest[ix];
      else
        y_peak[ip] = -1.;
      // printf("x = %lf, y = %lf\n", x_peak[ip], y_peak[ip]);
    }

    /* sort from higher to lower peak*/
    Int_t isort[n_peak];
    TMath::Sort<Double_t, Int_t>(n_peak, y_peak, isort);

    /* store highest peaks */
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
      const Double_t xp = ip < n_peak ? x_peak[isort[ip]] : -1.;
      const Double_t yp = ip < n_peak ? y_peak[isort[ip]] : -1.;
      fPeakX[it][il][iv][ip] = xp;
      fPeakY[it][il][iv][ip] = yp;
    }
    if (fPeakY[it][il][iv][0] > this->kPeakAbsThrNeutron) fPeakN[it][il][iv] = 1;

    /* if secondary peak is over threshold, set multi-peak flag */
    for (Int_t ip = 1; ip < n_peak; ++ip)
      if ((fPeakY[it][il][iv][ip] / fPeakY[it][il][iv][0] > this->kPeakRatioThrNeutron) &&
          (fPeakY[it][il][iv][ip] > this->kPeakAbsThrNeutron))
        ++fPeakN[it][il][iv];
      else
        break;
    // TODO: Need optimization of TSpectrum threshold parameters (They are different from photon case)

    delete[] x_peak;
    delete[] y_peak;
  }
}

/*** Position full fit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronFullPositionFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        NeutronSingleHitFit(lvl2, lvl3, it, il, iv);
      }  // view loop
    }    // layer loop
    /* Get position from layer with max energy deposit */
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      lvl3->fNeutronPosition[it][iv] =
          ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv,
                            fFuncNeutron->GetPos(lvl3->fNeutronPosFitPar[it][lvl3->fPosMaxLayer[it][iv]][iv]));
    /* No multi-hit rejection */
    lvl3->fNeutronMultiHit[it] = false;
  }  // tower loop
}

/*** Position fast reconstruction ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronFastPositionRec(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    // same silicon detector is used for both towers in Arm2
    const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      // sample #1 is used for Arm2 (no sample choice for Arm1)
      const Int_t pos_sample = fSiSample[it][lvl3->fPosMaxLayer[it][iv]][iv];

      const Int_t ch_low = fChannelRange[it][iv][0];
      const Int_t ch_high = fChannelRange[it][iv][1];
      const Int_t n_ch = ch_high - ch_low + 1;

      /* get maximum position */
      Int_t max_ch;
      Double_t max_value = -1E30;
      for (Int_t ic = ch_low; ic <= ch_high; ++ic)
        if (fReadTableRec.fMaskPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample]) {
          Double_t val = lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample];
          if (val > max_value) {
            max_value = val;
            max_ch = ic;
          }
        }
      /* geometric mean */
      const Int_t width = 2;  // range of the mean
      const Int_t mlow = max_ch - width;
      const Int_t mhigh = max_ch + width;
      if (mlow >= ch_low && mhigh <= ch_high) {
        Double_t sum = 0.;
        Double_t weight_sum = 0.;
        for (Int_t ic = mlow; ic <= mhigh; ++ic)
          if (fReadTableRec.fMaskPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample] &&
              lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample] > 0.) {
            sum += lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample] * Double_t(ic);
            weight_sum += lvl2->fPosDet[pos_tower][lvl3->fPosMaxLayer[it][iv]][iv][ic][pos_sample];
          }
        lvl3->fNeutronPosition[it][iv] = ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv, sum / weight_sum);
      } else {
        lvl3->fNeutronPosition[it][iv] = ChannelToPosition(it, lvl3->fPosMaxLayer[it][iv], iv, Double_t(max_ch));
      }
    }  // view loop

    /* No multi-hit rejection (at least not here) */
    lvl3->fNeutronMultiHit[it] = false;

#ifdef DEBUG_POS
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        DrawPositionFit(NULL, lvl2, it, il, iv, NULL, (PosFitBaseFCN *)NULL, NULL);
#endif
  }  // tower loop
}

/*** Multi-hit fit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronMultiHitFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3, Int_t it,
                                                            Int_t il, Int_t iv, Int_t npeak) {
  // Eugenio: need to solve all TODO here

  // sample #1 is used for Arm2 (no sample choice for Arm1)
  const Int_t pos_sample = fSiSample[it][il][iv];

  // same silicon detector is used for both towers in Arm2
  const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

  const Int_t ch_low = fChannelRange[it][iv][0];
  const Int_t ch_high = fChannelRange[it][iv][1];
  const Int_t n_ch = ch_high - ch_low + 1;

  /* Set function to be minimised */
  PosFitMultiHitFCN<armrec> hfun(npeak);
  // Double_t xp[n_ch];
  // Double_t yp[n_ch];
  // Double_t ey[n_ch];
  vector<Double_t> xp;
  vector<Double_t> yp;
  vector<Double_t> ey;
  for (Int_t ip = 0; ip < n_ch; ++ip) {
    const Int_t ic = ch_low + ip;

    Double_t thisPeakX = -1.;
    Double_t thisPeakY = -1.;
    if (npeak == 1) {
      for (Int_t ih = 0; ih < this->kMaxNparticle; ++ih) {
        if (fCluster[it][ih][il][iv][0] >= 0.) {
          thisPeakX = fCluster[it][ih][il][iv][1];
          thisPeakY = fCluster[it][ih][il][iv][2];
        }
      }
    }

    if (fReadTableRec.fMaskPosDet[pos_tower][il][iv][ic][pos_sample]) {
      // TODO: This condition is copied from the photon case, but it is clearly meaningless for Arm1
      // if single-hit, get only strips within +-50 from estimated peak
      if (npeak == 1)
        if (Double_t(ic) <= thisPeakX - 50. || Double_t(ic) >= thisPeakX + 50.) continue;

      // xp[ip] = Double_t(ic);
      // yp[ip] = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
      // ey[ip] = lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample];
      xp.push_back(Double_t(ic));
      yp.push_back(lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample]);
      ey.push_back(lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample]);
    }
  }
  // fFuncNeutron->SetDataVectors(n_ch, xp, yp, ey);
  fFuncNeutron->SetDataVectors(xp.size(), xp.data(), yp.data(), ey.data());
  TGraphErrors pos_graph(xp.size(), xp.data(), yp.data(), NULL, ey.data());

  /* 1st fit iteration: standard ROOT fit */
  // TGraphErrors pos_graph(n_ch);
  // for (Int_t ip = 0; ip < n_ch; ++ip) {
  //   const Int_t ic = ch_low + ip;
  //   const Double_t xx = Double_t(ic);
  //   const Double_t yy = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
  //   const Double_t ee = lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample];
  //   pos_graph.SetPoint(ip, xx, yy);
  //   pos_graph.SetPointError(ip, 0., ee);
  // }

  TF1 func("func", &hfun, &PosFitMultiHitFCN<armrec>::FitFunc, ch_low, ch_high, fFuncNeutron->GetNparam());

  // Delicate matching: Particle ID are not necessarily progressive
  Int_t mhits = 0;
  for (Int_t ih = 0; ih < this->kMaxNparticle; ++ih) {
    if (fCluster[it][ih][il][iv][0] < 0.)  // If this Track is empty move to next one
      continue;
    Int_t ip = mhits;
    if (ip >= fFuncNeutron->GetNparamSingle())
      Utils::Printf(Utils::kPrintError, "\tIn NeutronMultiHitFit: Trying to read Track %d of %d in [%d][%d][%d]\n", it,
                    il, iv, ip, fFuncNeutron->GetNparamSingle());
    if (fCluster[it][ih][il][iv][0] >= 0.) {
      Int_t ph = ih;
      Int_t pp = ip * fFuncNeutron->GetNparamSingle();
      func.SetParameter(0 + pp, 5 * fCluster[it][ph][il][iv][2]);        // Area
      func.SetParLimits(0 + pp, 0., 10. * fCluster[it][ph][il][iv][2]);  // TODO: Check the drawbacks of limitation
      func.SetParameter(1 + pp, fCluster[it][ph][il][iv][1]);            // Position
      func.SetParameter(2 + pp, 0.8);                                    // 1st fraction
      func.SetParLimits(2 + pp, 0.5, 1.0);
      func.SetParameter(3 + pp, 2. * this->kPeakSigmaNeutron);  // 1st width
      func.SetParameter(4 + pp, 0.3);                           // 2nd fraction
      func.SetParLimits(4 + pp, 0.0, 0.5);
      func.SetParameter(5 + pp, this->kPeakSigmaNeutron);        // 2nd width
      func.SetParameter(6 + pp, 10. * this->kPeakSigmaNeutron);  // 3rd width
    }
    ++mhits;
  }

  // fit
  pos_graph.Fit(&func, "QN0");

  /* 2nd fit iteration: Minuit2 minimization */
  // Utils::Printf(Utils::kPrintDebugFull, "Make minimiser\n");
  /* create minimizer giving a name and a name (optionally) for the specific
   * algorithm
   * possible choices are:
   *   min_name                algo_name
   * Minuit /Minuit2       Migrad, Simplex, Combined, Scan (default is Migrad)
   * Minuit2               Fumili2
   * Fumili
   * GSLMultiMin           ConjugateFR, ConjugatePR, BFGS,
   *                       BFGS2, SteepestDescent
   * GSLMultiFit
   * GSLSimAn
   * Genetic
   */
  const Char_t *min_name = "Minuit2";
  const Char_t *algo_name = "";
  ROOT::Math::Minimizer *minimum = ROOT::Math::Factory::CreateMinimizer(min_name, algo_name);

  // Utils::Printf(Utils::kPrintDebugFull, "Associate FCN to minimiser\n");
  ROOT::Math::Functor functor(&hfun, &PosFitMultiHitFCN<armrec>::DoEval, fFuncNeutron->GetNparam());
  minimum->SetFunction(functor);

  /* set tolerance , etc... */
  // Utils::Printf(Utils::kPrintDebugFull, "Set minimiser parameters\n");
  minimum->SetMaxFunctionCalls(500000);  // for Minuit/Minuit2 (default: 1000000)
  minimum->SetMaxIterations(10000);      // for GSL (default: 10000)
  minimum->SetTolerance(1E-10);          // (default: 0.001)
  minimum->SetPrecision(1E-10);          // (default: -1 (= autoset by minimizer))
  minimum->SetPrintLevel(-1);            // suppress all kinds of message
  minimum->SetErrorDef(1.);              // value above the minimum to define error on parameter(s) (default=1.)

  /* Set the free variables to be minimized */
  for (Int_t ih = 0; ih < fFuncNeutron->GetNhit(); ++ih) {
    const Int_t ipar = ih * fFuncNeutron->GetNparamSingle();
    minimum->SetVariable(0 + ipar, Form("Area         (peak %d)", ih), func.GetParameter(0 + ipar), 0.01);
    minimum->SetVariable(1 + ipar, Form("Position     (peak %d)", ih), func.GetParameter(1 + ipar), 0.01);
    minimum->SetVariable(2 + ipar, Form("1st fraction (peak %d)", ih), func.GetParameter(2 + ipar), 0.01);
    minimum->SetVariable(3 + ipar, Form("1st width    (peak %d)", ih), func.GetParameter(3 + ipar), 0.01);
    minimum->SetVariable(4 + ipar, Form("2nd fraction (peak %d)", ih), func.GetParameter(4 + ipar), 0.01);
    minimum->SetVariable(5 + ipar, Form("2nd width    (peak %d)", ih), func.GetParameter(5 + ipar), 0.01);
    minimum->SetVariable(6 + ipar, Form("3rd width    (peak %d)", ih), func.GetParameter(6 + ipar), 0.01);
  }

  /* Do the minimization */
  // Utils::Printf(Utils::kPrintDebugFull, "Minimise\n");
  minimum->Minimize();

  // minimum->FixVariable(0);
  // minimum->ReleaseVariable(1);
  // minimum->Minimize();

  // minimum->ReleaseVariable(0);
  // minimum->Minimize();

  /* Copy result to Level3 */
  const Double_t *min_par = minimum->X();
  // const Double_t *min_err = minimum->Errors();
  // ROOT::Minuit2::Minuit2Minimizer *minuit2 = (ROOT::Minuit2::Minuit2Minimizer *)minimum;
  // cout << minuit2->State().IsValid() << endl;
  // if (minuit2->State().IsValid())

  // Delicate matching: Particle ID are not necessarily progressive
  Int_t nhits = 0;
  for (Int_t ih = 0; ih < this->kMaxNparticle; ++ih) {
    if (fCluster[it][ih][il][iv][0] < 0.)  // If this Track is empty move to next one
      continue;
    Int_t ip = nhits;
    if (ip >= fFuncNeutron->GetNparamSingle())
      Utils::Printf(Utils::kPrintError, "\tIn NeutronMultiHitFit: Trying to read Track %d of %d in [%d][%d][%d]\n", it,
                    il, iv, ip, fFuncNeutron->GetNparamSingle());
    Double_t minos_low, minos_high;
    for (Int_t ipar = 0; ipar < fFuncNeutron->GetNparamSingle(); ++ipar) {
      Int_t ph = ipar + ih * fFuncNeutron->GetNparamSingle();
      Int_t pp = ipar + ip * fFuncNeutron->GetNparamSingle();
      minimum->GetMinosError(pp, minos_low, minos_high);
      lvl3->fNeutronPosFitPar[it][il][iv][ph] = min_par[pp];
      lvl3->fNeutronPosFitErr[it][il][iv][ph][0] = minos_low;
      lvl3->fNeutronPosFitErr[it][il][iv][ph][1] = minos_high;
      func.SetParameter(pp, min_par[pp]);  // This is necessary for the following integration
    }
    // TODO: For the moment overwrite amplitude with function integral inside the tower
    // TODO: Warning: The error is no more consistently estimated in this way!
    // TODO: Better to move this section to somewhere else?
    Double_t amplitud = lvl3->fNeutronPosFitPar[it][il][iv][ih * fFuncNeutron->GetNparamSingle()];
    Double_t integral = func.Integral(fChannelRange[it][iv][0], fChannelRange[it][iv][1]);
    lvl3->fNeutronPosFitPar[it][il][iv][ih * fFuncNeutron->GetNparamSingle()] = integral;
    lvl3->fNeutronPosFitErr[it][il][iv][ih * fFuncNeutron->GetNparamSingle()][0] *= integral / amplitud;
    lvl3->fNeutronPosFitErr[it][il][iv][ih * fFuncNeutron->GetNparamSingle()][1] *= integral / amplitud;
    //
    ++nhits;
  }
  lvl3->fNeutronPosFitChi2[it][il][iv] = minimum->MinValue();
  lvl3->fNeutronPosFitNDF[it][il][iv] = fFuncNeutron->GetNpoint() - fFuncNeutron->GetNparam();
  lvl3->fNeutronPosFitEDM[it][il][iv] = minimum->Edm();

#ifdef DEBUG_POS
  DrawPositionFit(NULL, lvl2, it, il, iv, &func, &hfun, min_par);
#endif

  delete minimum;
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronSingleHitFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3, Int_t it,
                                                             Int_t il, Int_t iv) {
  // sample #1 is used for Arm2 (no sample choice for Arm1)
  const Int_t pos_sample = fSiSample[it][il][iv];

  // same silicon detector is used for both towers in Arm2
  const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;

  const Int_t ch_low = fChannelRange[it][iv][0];
  const Int_t ch_high = fChannelRange[it][iv][1];
  const Int_t n_ch = ch_high - ch_low + 1;

  /* Set function to be minimised */
  fFuncNeutron->SetNhit(1);

  // Double_t xp[n_ch];
  // Double_t yp[n_ch];
  // Double_t ey[n_ch];
  vector<Double_t> xp;
  vector<Double_t> yp;
  vector<Double_t> ey;
  for (Int_t ip = 0; ip < n_ch; ++ip) {
    const Int_t ic = ch_low + ip;
    // get only strips within +-50 from estimated peak
    if (Double_t(ic) > fPeakX[it][il][iv][0] - 50. && Double_t(ic) < fPeakX[it][il][iv][0] + 50. &&
        fReadTableRec.fMaskPosDet[pos_tower][il][iv][ic][pos_sample]) {
      // xp[ip] = Double_t(ic);
      // yp[ip] = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
      // ey[ip] = lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample];
      xp.push_back(Double_t(ic));
      yp.push_back(lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample]);
      ey.push_back(lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample]);
    }
  }
  // fFuncNeutron->SetDataVectors(n_ch, xp, yp, ey);
  fFuncNeutron->SetDataVectors(xp.size(), xp.data(), yp.data(), ey.data());
  TGraphErrors pos_graph(xp.size(), xp.data(), yp.data(), NULL, ey.data());

  /* 1st fit iteration: standard ROOT fit */
  // TGraphErrors pos_graph(n_ch);
  // for (Int_t ip = 0; ip < n_ch; ++ip) {
  //   const Int_t ic = ch_low + ip;
  //   const Double_t xx = Double_t(ic);
  //   const Double_t yy = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
  //   const Double_t ee = lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample];
  //   pos_graph.SetPoint(ip, xx, yy);
  //   pos_graph.SetPointError(ip, 0., ee);
  // }
  TF1 func("func", fFuncNeutron, &PosFitBaseFCN::FitFunc, ch_low, ch_high, fFuncNeutron->GetNparam());

  // set initial parameters
  func.SetParameter(0, fPeakY[it][il][iv][0] * 5.);  // Area
  func.SetParameter(1, fPeakX[it][il][iv][0]);       // Position
  func.SetParameter(2, 0.8);                         // 1st fraction
  func.SetParLimits(2, 0.5, 1.0);
  func.SetParameter(3, 2. * this->kPeakSigmaNeutron);  // 1st width
  func.SetParameter(4, 0.3);                           // 2nd fraction
  func.SetParLimits(4, 0.0, 0.5);
  func.SetParameter(5, this->kPeakSigmaNeutron);        // 2nd width
  func.SetParameter(6, 10. * this->kPeakSigmaNeutron);  // 3rd width
  // fit
  pos_graph.Fit(&func, "QN0");

  /* 2nd fit iteration: Minuit2 minimization */
  Utils::Printf(Utils::kPrintDebugFull, "Make minimiser\n");
  /* create minimizer giving a name and a name (optionally) for the specific
   * algorithm
   * possible choices are:
   *   min_name                algo_name
   * Minuit /Minuit2       Migrad, Simplex, Combined, Scan (default is Migrad)
   * Minuit2               Fumili2
   * Fumili
   * GSLMultiMin           ConjugateFR, ConjugatePR, BFGS,
   *                       BFGS2, SteepestDescent
   * GSLMultiFit
   * GSLSimAn
   * Genetic
   */
  const Char_t *min_name = "Minuit2";
  const Char_t *algo_name = "";
  ROOT::Math::Minimizer *minimum = ROOT::Math::Factory::CreateMinimizer(min_name, algo_name);

  Utils::Printf(Utils::kPrintDebugFull, "Associate FCN to minimiser\n");
  ROOT::Math::Functor functor(fFuncNeutron, &PosFitBaseFCN::DoEval, fFuncNeutron->GetNparam());
  minimum->SetFunction(functor);

  /* set tolerance , etc... */
  Utils::Printf(Utils::kPrintDebugFull, "Set minimiser parameters\n");
  minimum->SetMaxFunctionCalls(500000);  // for Minuit/Minuit2 (default: 1000000)
  minimum->SetMaxIterations(10000);      // for GSL (default: 10000)
  minimum->SetTolerance(1E-10);          // (default: 0.001)
  minimum->SetPrecision(1E-10);          // (default: -1 (= autoset by minimizer))
  minimum->SetPrintLevel(-1);            // suppress all kinds of message
  minimum->SetErrorDef(1.);              // value above the minimum to define error on parameter(s) (default=1.)

  /* Set the free variables to be minimized */
  minimum->SetVariable(0, "Area", func.GetParameter(0), 0.01);
  minimum->SetVariable(1, "Position", func.GetParameter(1), 0.01);
  minimum->SetVariable(2, "1st fraction", func.GetParameter(2), 0.01);
  minimum->SetVariable(3, "1st width", func.GetParameter(3), 0.01);
  minimum->SetVariable(4, "2nd fraction", func.GetParameter(4), 0.01);
  minimum->SetVariable(5, "2nd width", func.GetParameter(5), 0.01);
  minimum->SetVariable(6, "3rd width", func.GetParameter(6), 0.01);

  /* Do the minimization */
  Utils::Printf(Utils::kPrintDebugFull, "Minimise\n");
  minimum->Minimize();

  // minimum->FixVariable(0);
  // minimum->ReleaseVariable(1);
  // minimum->Minimize();

  // minimum->ReleaseVariable(0);
  // minimum->Minimize();

  /* Copy result to Level3 */
  const Double_t *min_par = minimum->X();
  // const Double_t *min_err = minimum->Errors();
  for (Int_t ipar = 0; ipar < fFuncNeutron->GetNparam(); ++ipar) {
    Double_t minos_low, minos_high;
    minimum->GetMinosError(ipar, minos_low, minos_high);
    lvl3->fNeutronPosFitPar[it][il][iv][ipar] = min_par[ipar];
    lvl3->fNeutronPosFitErr[it][il][iv][ipar][0] = minos_low;
    lvl3->fNeutronPosFitErr[it][il][iv][ipar][1] = minos_high;
  }
  lvl3->fNeutronPosFitChi2[it][il][iv] = minimum->MinValue();
  lvl3->fNeutronPosFitNDF[it][il][iv] = fFuncNeutron->GetNpoint() - fFuncNeutron->GetNparam();
  lvl3->fNeutronPosFitEDM[it][il][iv] = minimum->Edm();

  // fMultiPeak[it][il][iv] = false;

#ifdef DEBUG_POS
  DrawPositionFit(NULL, lvl2, it, il, iv, &func, fFuncNeutron, min_par);
#endif

  delete minimum;
}

/*** Cluster TSpectrum output according to relative distance on different layers ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackClustering() {
  // Eugenio: need to solve all TODO here

  // Copy to temporary variables
  Utils::CopyVector4D(fPeakTrackX, fPeakX);
  Utils::CopyVector4D(fPeakTrackY, fPeakY);
  // Remove Peaks not passing preselection
  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ip = fPeakN[it][il][iv]; ip < this->kMaxNparticle; ++ip) {
          fPeakTrackX[it][il][iv][ip] = -1.;
          fPeakTrackY[it][il][iv][ip] = -1.;
        }

  /* Iterate over all Possible combination of 3 Particles to cluster along X (Y) Tracks */
  for (Int_t ik = 0; ik < this->kMaxNparticle; ++ik) {
    Utils::ClearVector2D(fmaxPeakPos, -1.);
    Utils::ClearVector2D(fmaxPeakDep, -1.);
    for (Int_t it = 0; it < this->kCalNtower; it++) {
      /* Find the highest (remnant) Peak for each view */
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {        // Loop over all Peak found the TOWER-LAYER-VIEW
            if (fPeakTrackY[it][il][iv][ip] > fmaxPeakDep[it][iv]) {  // Find the maximum among not-associated Peaks
              fmaxPeakPos[it][iv] = fPeakTrackX[it][il][iv][ip];
              fmaxPeakDep[it][iv] = fPeakTrackY[it][il][iv][ip];
            }
          }
      if (fmaxPeakPos[it][0] < 0 && fmaxPeakPos[it][1] < 0) continue;

      Utils::Printf(Utils::kPrintDebug, "\ttower %d \n", it);
      Utils::Printf(Utils::kPrintDebug, "\t\tMAXIMUM RELEASE X: %f at %f\n", fmaxPeakDep[it][0], fmaxPeakPos[it][0]);
      Utils::Printf(Utils::kPrintDebug, "\t\tMAXIMUM RELEASE Y: %f at %f\n", fmaxPeakDep[it][1], fmaxPeakPos[it][1]);

      /* Associate each Peak to a candidate Track */
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          Int_t ID = -1;
          Double_t xMinDist = 1e9;
          vector<Double_t> candidate = {-1., -1., -1.};
          for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {  // Loop over all Peak found the TOWER-LAYER-VIEW
            if (fPeakTrackY[it][il][iv][ip] > 0.) {             // Consider each element of the not-associated Peaks
              const Double_t distance = TMath::Abs(fmaxPeakPos[it][iv] - fPeakTrackX[it][il][iv][ip]) * this->kPosPitch;
              if (distance <= this->kMinDistNeutron && distance <= xMinDist) {
                ID = ip;
                xMinDist = distance;
                if (ik > fNCluster[it] - 1) fNCluster[it] = ik + 1;
                candidate[0] = ik;
                candidate[1] = fPeakTrackX[it][il][iv][ip];
                candidate[2] = fPeakTrackY[it][il][iv][ip];
                Utils::Printf(Utils::kPrintDebug, "\t\tASSOCIATING: %d %d %d %d %f %f (%f - %f = %f)\n", it, il, iv, ip,
                              fPeakTrackX[it][il][iv][ip], fPeakTrackY[it][il][iv][ip], fmaxPeakPos[it][iv],
                              fPeakTrackX[it][il][iv][ip], distance);
              }
            }
          }
          // No good candidate found: Move to next layer/view
          if (ID < 0) continue;
          Utils::Printf(Utils::kPrintDebug, "\t\tCANDIDATE: %d %d %d %.0f %.1lf %.6lf\n", it, il, iv, candidate[0],
                        candidate[1], candidate[2]);
          // Good candidate found! Remove from temporary arrays and fill to cluster vector
          fPeakTrackX[it][il][iv][ID] = -1.;
          fPeakTrackY[it][il][iv][ID] = -1.;
          fCluster[it][ik][il][iv] = candidate;
        }

    }  // tower loop
  }    // iteration loop

  // TODO: Treat Peaks that were not associated to any Track
  // TODO: Treat Tracks which have only X or Y coordinates
  // TODO: Eventually split aligned track if two particles have the same true position
  //		This probably require shower width to identify EM in first and HAD in last layers

  Utils::Printf(Utils::kPrintDebug, "\tTRACK CLUSTERING\n");
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
          if (fCluster[it][ip][il][iv][0] >= 0.) {
            Utils::Printf(Utils::kPrintDebug,
                          "\t\ttower %d layer %d%s\n"
                          "\t\t\tcluster[%d]      = %.0f %.1lf %.6lf\n",
                          it, il, iv == 0 ? "X" : "Y", ip, fCluster[it][ip][il][iv][0], fCluster[it][ip][il][iv][1],
                          fCluster[it][ip][il][iv][2]);
          }
        }
  }
}

/*** Substitute TSpectrum amplitude with small Radius energy deposit ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackRefinement(Level2<armclass> *lvl2) {
  // Eugenio: need to solve all TODO here

  const Int_t smallRadius = 0.5 * this->kMinDistNeutron / this->kPosPitch;  // Half the minimum multihit distance

  const Double_t eStripThreshold = 0.001;  // TODO: this must be chosen accordingly...
  const Double_t eLayerThreshold = 0.050;  // TODO: this must be chosen accordingly...
  const Double_t eRatioThreshold = 0.010;  // TODO: this must be chosen accordingly...

  // Compute the energy deposit in Small Radius for each layer over each track
  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        Double_t imaxdE = -1000;
        Double_t xmaxdE = -1000;
        Double_t ymaxdE = -1000;
        // First Step : Compute the Small Radius energy deposit among all found Peaks
        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          if (fCluster[it][ip][il][iv][0] >= 0.) {  // Peak found by TSpectrum
            const Int_t iTower = this->kArmIndex == 0 ? it : 0;
            const Int_t iSample = fSiSample[it][il][iv];
            const Int_t iPosition = fCluster[it][ip][il][iv][1];
            Double_t sumdE = 0.;
            for (Int_t ic = iPosition - smallRadius; ic <= iPosition + smallRadius; ++ic) {
              if (ic < fChannelRange[it][iv][0] || ic > fChannelRange[it][iv][1]) continue;
              const Double_t signal = lvl2->fPosDet[iTower][il][iv][ic][iSample];
              if (signal > eStripThreshold) sumdE += signal;
            }
            // Update TSpectrum amplitude with the Small Radius energy deposit
            fCluster[it][ip][il][iv][2] = sumdE;
            // Consider the amplitude and position of the maximum Peak along the Track
            if (sumdE > ymaxdE) {
              imaxdE = fCluster[it][ip][il][iv][0];
              xmaxdE = fCluster[it][ip][il][iv][1];
              ymaxdE = fCluster[it][ip][il][iv][2];
            }
          }
        }
        if (xmaxdE < 0. || ymaxdE < 0.) continue;
        // Second Step : Compute the small Radius energy deposit among all remaining layers
        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          if (fCluster[it][ip][il][iv][0] < 0.) {  // Peak NOT found by TSpectrum
            const Int_t iTower = this->kArmIndex == 0 ? it : 0;
            const Int_t iSample = 1.;        // fSiSample[it][il][iv]; //TODO: need to implement the realistic case
            const Int_t iPosition = xmaxdE;  // Use maximum amplitude Peak as position reference
            Double_t release = 0.;
            Double_t average = 0.;
            for (Int_t ic = iPosition - smallRadius; ic <= iPosition + smallRadius; ++ic) {
              if (ic < fChannelRange[it][iv][0] || ic > fChannelRange[it][iv][1]) continue;
              const Double_t signal = lvl2->fPosDet[iTower][il][iv][ic][iSample];
              if (signal > eStripThreshold) {
                average += signal * Double_t(ic);
                release += signal;
              }
            }
            // Update TSpectrum amplitude with the small Radius energy deposit
            if (release > eLayerThreshold && release > eRatioThreshold * ymaxdE) {
              fCluster[it][ip][il][iv][0] = imaxdE;
              fCluster[it][ip][il][iv][1] = average / release;  // TODO: is it better to use the reference position
                                                                // here?
              fCluster[it][ip][il][iv][2] = release;
            }
          }
        }
      }

  Utils::Printf(Utils::kPrintDebug, "\tTRACK REFINEMENT\n");
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
          if (fCluster[it][ip][il][iv][0] >= 0.) {
            Utils::Printf(Utils::kPrintDebug,
                          "\t\ttower %d layer %d%s\n"
                          "\t\t\tcluster[%d]      = %.0f %.1lf %.6lf\n",
                          it, il, iv == 0 ? "X" : "Y", ip, fCluster[it][ip][il][iv][0], fCluster[it][ip][il][iv][1],
                          fCluster[it][ip][il][iv][2]);
          }
        }
  }
}

/*** Perform Lorentzian fit and update position and amplitude ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackLorentzFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  // Eugenio: need to solve all TODO here

  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        Int_t nPeak = 0;
        for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip)
          if (fCluster[it][ip][il][iv][0] >= 0.) ++nPeak;

        // Single/Multiple Particle Lorentzian Fit
        if (nPeak > 0) NeutronMultiHitFit(lvl2, lvl3, it, il, iv, nPeak);
#ifdef DEBUG_POS
        else
          DrawPositionFit(NULL, lvl2, it, il, iv, NULL, (PosFitBaseFCN *)NULL, NULL);
#endif
        // TODO: This is just copied from the photon case but here it may be necessary to:
        //			1. Check if singlehit fit is necessary in case of multihit fit failure
        //			2. Convert from strip to position (probably not here, for compatibility)

        // Update small Radius energy deposit with fit output parameters
        for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
          if ((fCluster[it][ip][il][iv][1] < 0. &&
               lvl3->fNeutronPosFitPar[it][il][iv][1 + ip * this->kPosNpars] > 0.) ||
              (fCluster[it][ip][il][iv][1] > 0. && lvl3->fNeutronPosFitPar[it][il][iv][1 + ip * this->kPosNpars] < 0.))
            Utils::Printf(
                Utils::kPrintError,
                "\tIn TrackLorentzFit: Inconsistent raw/fit Peak %d in [%d][%d][%d] :(%.3f, %.3f) vs (%.3f, %.3f)\n",
                ip, it, il, iv, fCluster[it][ip][il][iv][1], fCluster[it][ip][il][iv][2],
                lvl3->fNeutronPosFitPar[it][il][iv][1 + ip * this->kPosNpars],
                lvl3->fNeutronPosFitPar[it][il][iv][0 + ip * this->kPosNpars]);
          if (fCluster[it][ip][il][iv][0] >= 0.) {
            fCluster[it][ip][il][iv][1] = lvl3->fNeutronPosFitPar[it][il][iv][1 + ip * this->kPosNpars];
            fCluster[it][ip][il][iv][2] = lvl3->fNeutronPosFitPar[it][il][iv][0 + ip * this->kPosNpars];
            ////NB: This is the correct integral approach, but we are interested in the area inside the towers
            //            Double_t amplitude = TMath::Pi()*lvl3->fNeutronPosFitPar[it][il][iv][0 + ip *
            //            this->kPosNpars]; Double_t fraction1 = lvl3->fNeutronPosFitPar[it][il][iv][2 + ip *
            //            this->kPosNpars]; Double_t fraction2 = lvl3->fNeutronPosFitPar[it][il][iv][4 + ip *
            //            this->kPosNpars]; Double_t fraction3 = 1-fraction1-fraction2;
            //            if(lvl3->fNeutronPosFitPar[it][il][iv][3 + ip * this->kPosNpars]<0)
            //            	fraction1 *= -1;
            //            if(lvl3->fNeutronPosFitPar[it][il][iv][5 + ip * this->kPosNpars]<0)
            //            	fraction2 *= -1;
            //            if(lvl3->fNeutronPosFitPar[it][il][iv][6 + ip * this->kPosNpars]<0)
            //            	fraction3 *= -1;
            //            amplitude *= fraction1+fraction2+fraction3;
            //            fCluster[it][ip][il][iv][2] = amplitude;
          }
        }
      }

  Utils::Printf(Utils::kPrintDebug, "\tTRACK LORENTZ FIT\n");
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
          if (fCluster[it][ip][il][iv][0] >= 0.) {
            Utils::Printf(Utils::kPrintDebug,
                          "\t\ttower %d layer %d%s\n"
                          "\t\t\tcluster[%d]      = %.0f %.1lf %.6lf\n",
                          it, il, iv == 0 ? "X" : "Y", ip, fCluster[it][ip][il][iv][0], fCluster[it][ip][il][iv][1],
                          fCluster[it][ip][il][iv][2]);
          }
        }
  }
}

/*** Associate each X track to a given Y track for each particle ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackExtraction() {
  // Eugenio: need to solve all TODO here

  /* Compute Chi2 associated to each single xy couple for each xy combination */
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    if (fNCluster[it] < 2) continue;
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t ip = 0; ip < fNCluster[it]; ++ip)
        for (Int_t jp = 0; jp < fNCluster[it]; ++jp) {
          Int_t xLayer = il;
          Int_t yLayer = il;
          Int_t xView = 0;
          Int_t yView = 1;

          Int_t xID = (Int_t)fCluster[it][ip][xLayer][xView][0];
          Int_t yID = (Int_t)fCluster[it][jp][yLayer][yView][0];
          if (xID < 0 || yID < 0) continue;

          Double_t xdE = fCluster[it][ip][xLayer][xView][2];
          Double_t ydE = fCluster[it][jp][yLayer][yView][2];
          // TODO: Is it a reasonable definition? Is it correct to use 1e9?
          fValCom[it][il][xID][yID] = (xdE > 0 && ydE > 0) ? TMath::Power(xdE - ydE, 2.) / TMath::Sqrt(xdE + ydE) : 1e9;
          Utils::Printf(Utils::kPrintDebug, "\tCHI2 %d %d (%d, %d) [%d, %d] %.6lf %.6lf %f\n", it, il, ip, jp, xID, yID,
                        xdE, ydE, fValCom[it][il][xID][yID]);
        }
  }
  // TODO: Consider a more proper reweight for chi2 in Layer 3 and 4
  // TODO: Exclude the cases where we combine two layers with a gap

  /* Compute Chi2 association using all combinations in all layers */
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    if (fNCluster[it] < 2) continue;
    for (Int_t ic = 0; ic < fMapCombination[fNCluster[it] - 1].size(); ++ic) {
      Double_t chi2 = 0.;
      Double_t ndof = 0.;
      Int_t npar = 0;
      for (Int_t ip = 0; ip < fNCluster[it]; ++ip) {
        Int_t xID = fMapCombination[fNCluster[it] - 1][ic][ip][0];
        Int_t yID = fMapCombination[fNCluster[it] - 1][ic][ip][1];
        Bool_t isFound = false;
        for (Int_t il = 0; il < this->kPosNlayer; ++il) {
          Double_t val = fValCom[it][il][xID][yID];
          if (val > 0.) {
            if (!isFound) {
              isFound = true;
              ++npar;
            }
            chi2 += val;
            ++ndof;
          }
        }
      }
      if (ndof > 0.) {
        fChi2[it][ic] = chi2;
        fNdof[it][ic] = ndof;
        fNpar[it][ic] = npar;
        Utils::Printf(Utils::kPrintDebug, "\tCHI2/NDOF %d %d %0f %.6lf %.6lf\n", it, ic, ndof, chi2, chi2 / ndof);
      }
    }
  }
  // TODO: Split the algorithm separately for Layer 1-2 and 3-4

  for (Int_t it = 0; it < this->kCalNtower; it++) {
    if (fNCluster[it] < 2) continue;
    Double_t maxNumdof = 0;
    // Compute the maximum number of freedom among all combinations
    for (Int_t ic = 0; ic < fMapCombination[fNCluster[it] - 1].size(); ++ic) {
      Double_t ndof = fNdof[it][ic];
      if (ndof > maxNumdof) maxNumdof = ndof;
    }
    Double_t minRedChi = 1e9;
    // Compute the minimum chi2/ndof among good combinations
    for (Int_t ic = 0; ic < fMapCombination[fNCluster[it] - 1].size(); ++ic) {
      if (fNdof[it][ic] < maxNumdof - 1)  // Exclude combination with a lot of unassociated peaks
        continue;
      if (fNpar[it][ic] < fNCluster[it] - 1)  // Exclude combination with more than one unmatched tracks
        continue;
      Double_t quality = fChi2[it][ic] / fNdof[it][ic];
      if (quality < minRedChi) {
        minRedChi = quality;
        fCombination[it] = ic;
      }
    }
    Utils::Printf(Utils::kPrintDebug, "\tCOMBINATION %d %d\n", it, fCombination[it]);
  }
}

/*** Reconstruct the track for each single identified particle ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackCorrection(Level3<armrec> *lvl3) {
  // Eugenio: need to solve all TODO here

  // TODO: This procedure is enough for a single swap, but not for multiple ones
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          Int_t ID = (Int_t)fCluster[it][ip][il][iv][0];
          if (ID < 0) continue;
          for (Int_t jp = 0; jp < fNCluster[it]; ++jp) {
            if (fMapCombination[fNCluster[it] - 1][fCombination[it]][jp][iv] == ID) {
              lvl3->fMultiParticleSiPos[it][jp][il][iv] = fCluster[it][ip][il][iv][1];  // Copy Peak position
              lvl3->fMultiParticleSiDep[it][jp][il][iv] = fCluster[it][ip][il][iv][2];  // Copy Peak amplitude
            }
          }
        }

    Utils::Printf(Utils::kPrintDebug, "\tPARTICLE RECONSTRUCTION\n");
    for (Int_t it = 0; it < this->kCalNtower; it++)
      for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
        Utils::Printf(Utils::kPrintDebug,
                      "\t\ttower %d\n"
                      "\t\t\tparticle %d\n",
                      it, ip);
        for (Int_t il = 0; il < this->kPosNlayer; ++il)
          for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
            if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0)
              Utils::Printf(Utils::kPrintDebug, "\t\t\t\tlayer %d%s      = %.1lf %.6lf\n", il, iv == 0 ? "X" : "Y",
                            lvl3->fMultiParticleSiPos[it][ip][il][iv], lvl3->fMultiParticleSiDep[it][ip][il][iv]);
          }
      }
    // TODO: This is the position in strip must be converted in mm (in which sdr?)
    // TODO: Add some multihit variable to distinguish between hh, hg, gg multihits
    // TODO: Add energy deposit reordering to consider unavoidable swap in swapping
  }
}

/*** Finally associate to each track a Photon-like or Hadron-like tag ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackSeparation(Level3<armrec> *lvl3) {
  // Eugenio: need to solve all TODO here

  // Associate identity simply using maximum position and shower extinction in silicon
  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
      Int_t maxLayer = -1;
      Double_t maxSignal = 0.;
      Int_t endLayer = -1;
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          Double_t signal = lvl3->fMultiParticleSiDep[it][ip][il][iv];
          if (signal < 0.) continue;

          endLayer = il;

          if (signal > maxSignal) {
            maxSignal = signal;
            maxLayer = il;
          }
        }
      if (endLayer >= 0 && maxLayer >= 0) {
        if (endLayer < 3 && maxLayer < 2)  // Photon-like
          lvl3->fMultiParticleSiID[it][ip] = 1;
        else  // Hadron-like
          lvl3->fMultiParticleSiID[it][ip] = 2;
      }
    }
  // TODO: Implement a method to separate Photon and Hadron-like exploiting:
  //		(Silicon Hit Layers?->)Silicon Transition curve(Better), silicon shower width

  // TODO: There is no energy reordering here: if not previously done, it must be done here
  Utils::Printf(Utils::kPrintDebug, "\tPARTICLE IDENTIFICATION\n");
  for (Int_t it = 0; it < this->kCalNtower; it++)
    Utils::Printf(Utils::kPrintDebug, "\t\ttower %d : %d %d %d\n", it, lvl3->fMultiParticleSiID[it][0],
                  lvl3->fMultiParticleSiID[it][1], lvl3->fMultiParticleSiID[it][2]);

#ifdef DEBUG_POS
  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip)
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          if (fDebug_mh[it][il][iv][ip]) {
            delete fDebug_mh[it][il][iv][ip];
            fDebug_mh[it][il][iv][ip] = NULL;
          }
          if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0) {
            //            fDebug_mh[it][il][iv][ip] = //Eugenio
            //                new TMarker(lvl3->fMultiParticleSiPos[it][ip][il][iv],
            //                lvl3->fMultiParticleSiDep[it][ip][il][iv],
            //                            (lvl3->fMultiParticleSiID[it][ip] == 1) ? kOpenCircle : kOpenSquare);
            fDebug_mh[it][il][iv][ip] =
                new TMarker(lvl3->fMultiParticleSiPos[it][ip][il][iv], 0.,
                            (lvl3->fMultiParticleSiID[it][ip] == 1) ? kOpenCircle : kOpenSquare);
          }
        }
#endif
}

/*** Finally associate a calorimeter layer by layer energy deposit to each particle ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::TrackGamma2DFit(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  // Eugenio: need to solve all TODO here

  // TODO: Move these constants elsewhere
  const Double_t calThickness = 0.1000;  // cm
  const Double_t calDensity = 6.71;      // g/cm3
  const Double_t calMIP = 8.926;         // MeV/cm
  const Double_t silThickness = 0.0285;  // cm
  const Double_t silDensity = 2.33;      // g/cm3
  const Double_t silMIP = 3.876;         // MeV/cm

  const Int_t nPars = 4;
  for (Int_t it = 0; it < this->kCalNtower; it++) {
    // Longitudinal fit procedure is applied only to multihit
    Int_t nPeak = 0;
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip)
      if (lvl3->fMultiParticleSiID[it][ip] > 0) ++nPeak;
    if (nPeak < 2) continue;

    /***************
     * Calorimeter *
     ***************/
    vector<Double_t> xpcal;
    vector<Double_t> ypcal;
    vector<Double_t> lpcal;
    vector<Double_t> hpcal;
    for (Int_t il = 1; il < lvl2->kCalNlayer; ++il) {
      // Skip first layer due to large halo background in data
      // TODO: Confirm later if chi^2 is seriously affected
      Double_t depth = this->kCalLayerX0[it][il];
      // Double_t release = lvl2->fCalorimeter[it][il] / (calThickness * calDensity);
      Double_t release = lvl2->fCalorimeter[it][il] / (calThickness * calMIP);
      // TODO: Understand the best uncertainty associated to energy deposit!
      Double_t resolution = 0.15 * lvl2->fCalorimeter[it][il];
      Double_t inferr = release - resolution > 0. ? resolution : 0.;
      Double_t superr = release + resolution > 0. ? resolution : 0.;
      xpcal.push_back(depth);
      ypcal.push_back(release);
      lpcal.push_back(inferr);
      hpcal.push_back(superr);
    }
#ifdef DEBUG_LON
    // TODO: Remove DrawCalX0 and DrawPosX0 methods
    if (fDebug_calx0[it]) {
      delete fDebug_calx0[it];
      fDebug_calx0[it] = NULL;
    }
    fDebug_calx0[it] =
        new TGraphAsymmErrors(xpcal.size(), xpcal.data(), ypcal.data(), NULL, NULL, lpcal.data(), hpcal.data());
    fDebug_calx0[it]->SetMarkerSize(1.5);
    fDebug_calx0[it]->SetMarkerStyle(kFullDoubleDiamond);
    fDebug_calx0[it]->SetMarkerColor(kBlack);
    fDebug_calx0[it]->SetLineColor(kBlack);
#endif

    /***********
     * Silicon *
     ***********/
    TGraph *ginterpol = new TGraph(xpcal.size(), xpcal.data(), ypcal.data());
    vector<vector<Double_t>> xpsil(nPeak, vector<Double_t>(0));
    vector<vector<Double_t>> ypsil(nPeak, vector<Double_t>(0));
    vector<vector<Double_t>> lpsil(nPeak, vector<Double_t>(0));
    vector<vector<Double_t>> hpsil(nPeak, vector<Double_t>(0));
    for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
      // Look for the first and last silicon layers with signal
      Double_t infDepth = -1;
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = this->kPosNview - 1; iv >= 0; --iv)
          if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0.) {
            if (infDepth < 0.) {
              infDepth = this->kPosLayerX0[it][il][iv];
              break;
            }
          }
      Double_t supDepth = -1;
      for (Int_t il = this->kPosNlayer - 1; il >= 0; --il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0.) {
            if (supDepth < 0.) {
              supDepth = this->kPosLayerX0[it][il][iv];
              break;
            }
          }
      if (infDepth < 0 || supDepth < 0.) continue;

      // Loop over silicon layers
      for (Int_t il = 0; il < this->kPosNlayer; ++il) {  // 0, 1, 2, 3
        Bool_t isDoubleLayer = false;
        // TODO: Decide what to do with double layers
        if (il < 2)
          if (lvl3->fMultiParticleSiDep[it][ip][il][0] > 0. && lvl3->fMultiParticleSiDep[it][ip][il][1] > 0.) {
            isDoubleLayer = true;
          }                                                    //
        for (Int_t iv = this->kPosNview - 1; iv >= 0; --iv) {  // y, x
          // TODO: Decide what to do with double layers
          if (isDoubleLayer)
            if (iv == 1) {
              continue;
            }  //
          Double_t depth = this->kPosLayerX0[it][il][iv];
          Double_t release = 0.;
          // TODO: It must be corrected using leakage and efficiency information!
          if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0.) release = lvl3->fMultiParticleSiDep[it][ip][il][iv];
          // TODO: Decide what to do with double layers
          if (isDoubleLayer) {  // For the first two layers, combine x and y using harmonic mean
            depth = 0.5 * (this->kPosLayerX0[it][il][0] + this->kPosLayerX0[it][il][1]);
            release =
                2. / (1. / lvl3->fMultiParticleSiDep[it][ip][il][0] + 1. / lvl3->fMultiParticleSiDep[it][ip][il][1]);
          }  //
          // release /= (silThickness * silDensity);
          release /= (silThickness * silMIP);
          // Apply rescaling to renormalize silicon deposit to scintillator deposit
          Double_t calrelease = ginterpol->Eval(depth);
          Double_t silrelease = 0.;
          for (Int_t jp = 0; jp < this->kMaxNparticle; ++jp)
            if (lvl3->fMultiParticleSiDep[it][jp][il][iv] > 0.) {
              silrelease += lvl3->fMultiParticleSiDep[it][jp][il][iv] / (silThickness * silMIP);
            }
          if (release > 0.) release *= calrelease / silrelease;
          // TODO: Optimize the silicon layer sumdE to avoid double computation!
          // TODO: Check if rescaling is terrible when position fit miss a Peak!
          // TODO: Understand the best uncertainty associated to energy deposit!
          Double_t resolution = 0.15 * release;
          Double_t inferr = release - resolution > 0. ? resolution : 0.;
          Double_t superr = release + resolution > 0. ? resolution : 0.;
          xpsil[ip].push_back(depth);
          ypsil[ip].push_back(release);
          lpsil[ip].push_back(inferr);
          hpsil[ip].push_back(superr);
        }  // View Loop
      }    // Layer Loop
#ifdef DEBUG_LON
      // TODO: Remove DrawCalX0 and DrawPosX0 methods
      if (fDebug_posx0[it][ip]) {
        delete fDebug_posx0[it][ip];
        fDebug_posx0[it][ip] = NULL;
      }
      fDebug_posx0[it][ip] = new TGraphAsymmErrors(xpsil[ip].size(), xpsil[ip].data(), ypsil[ip].data(), NULL, NULL,
                                                   lpsil[ip].data(), hpsil[ip].data());
      fDebug_posx0[it][ip]->SetMarkerSize(1.25);
      fDebug_posx0[it][ip]->SetMarkerStyle(kFullCircle);
      fDebug_posx0[it][ip]->SetMarkerColor(2 + ip);
      fDebug_posx0[it][ip]->SetLineColor(2 + ip);
#endif
    }  // Particle Loop
    delete ginterpol;

    /* Set function to be minimised */
    LonFitMultiHitFCN hfun(nPars * nPeak, nPeak);
    hfun.SetDataVectors(xpcal, ypcal, lpcal, hpcal, xpsil, ypsil, lpsil, hpsil);

    // TODO: Understand if a first level minimization is necessary
    //    /* 1st fit iteration: Simple minimization */
    //    Utils::Printf(Utils::kPrintDebugFull, "Simple fit\n");
    //    TGraphErrors pos_graph(xp.size(), xp.data(), yp.data(), NULL, ey.data());
    //    TF1 func("func", &hfun, &PosFitMultiHitFCN::FitFunc, ch_low, ch_high, hfun.GetNparam());
    //    // set initial parameters
    //    for (Int_t ih = 0; ih < hfun.GetNhit(); ++ih) {
    //    	const Int_t ipar = ih * hfun.GetNparamSingle();
    //    	func.SetParameter(0 + ipar, fPeakY[it][il][iv][ih] * 5.);  // Area
    //    	func.SetParameter(1 + ipar, fPeakX[it][il][iv][ih]);       // Position
    //    	func.SetParameter(2 + ipar, 0.8);                          // 1st fraction
    //    	func.SetParLimits(2 + ipar, 0.5, 1.0);
    //    	func.SetParameter(3 + ipar, 2. * this->kPeakSigmaPhoton);  // 1st width
    //    	func.SetParameter(4 + ipar, 0.3);                          // 2nd fraction
    //    	func.SetParLimits(4 + ipar, 0.0, 0.5);
    //    	func.SetParameter(5 + ipar, this->kPeakSigmaPhoton);        // 2nd width
    //    	func.SetParameter(6 + ipar, 10. * this->kPeakSigmaPhoton);  // 3rd width
    //    }
    //    // fit
    //    pos_graph.Fit(&func, "QN0");

    /* 2nd fit iteration: Minuit2 minimization */
    Utils::Printf(Utils::kPrintDebugFull, "Make minimiser\n");
    /* create minimizer giving a name and a name (optionally) for the specific
     * algorithm
     * possible choices are:
     *     min_name                alg_name
     * Minuit /Minuit2       Migrad, Simplex, Combined, Scan (default is Migrad)
     * Minuit2               Fumili2
     * Fumili
     * GSLMultiMin           ConjugateFR, ConjugatePR, BFGS,
     *                       BFGS2, SteepestDescent
     * GSLMultiFit
     * GSLSimAn
     * Genetic
     */
    const Char_t *min_name = "Minuit2";
    const Char_t *alg_name = "";
    ROOT::Math::Minimizer *minimum = ROOT::Math::Factory::CreateMinimizer(min_name, alg_name);

    Utils::Printf(Utils::kPrintDebugFull, "Associate FCN to minimiser\n");
    ROOT::Math::Functor functor(&hfun, &LonFitMultiHitFCN::DoEval, hfun.GetNparam());
    minimum->SetFunction(functor);

    /* set tolerance , etc... */
    Utils::Printf(Utils::kPrintDebugFull, "Set minimiser parameters\n");
    minimum->SetMaxFunctionCalls(500000);  // for Minuit/Minuit2 (default: 1000000)
    minimum->SetMaxIterations(10000);      // for GSL (default: 10000)
    minimum->SetTolerance(1E-10);          // (default: 0.001)
    minimum->SetPrecision(1E-10);          // (default: -1 (= autoset by minimizer))
    // minimum->SetPrintLevel(-1);            // suppress all kinds of message
    minimum->SetPrintLevel(3);  // suppress all kinds of message
    minimum->SetErrorDef(1.);   // value above the minimum to define error on parameter(s) (default=1.)

    /*
     * Define initial parameter values
     */
    // TODO: Optimize the computation of initial values!
    // sumdE
    Double_t calsumdE = 0.;
    for (Int_t il = 0; il < ypcal.size(); ++il) {
      calsumdE += ypcal[il];
    }
    Double_t silsumdE[this->kMaxNparticle];
    for (Int_t i = 0; i < this->kMaxNparticle; ++i) silsumdE[i] = 0.;
    for (Int_t ih = 0; ih < hfun.GetNhit(); ++ih)
      for (Int_t il = 0; il < ypsil[ih].size(); ++il) {
        silsumdE[ih] += ypsil[ih][il];
      }
    Double_t siltotal = 0.;
    for (Int_t ih = 0; ih < hfun.GetNhit(); ++ih) {
      siltotal += silsumdE[ih];
    }
    // silicon energy
    Double_t sumde_val[this->kMaxNparticle];
    Double_t sumde_inf[this->kMaxNparticle];
    Double_t sumde_sup[this->kMaxNparticle];
    for (Int_t i = 0; i < this->kMaxNparticle; ++i) {
      sumde_val[i] = 0.;
      sumde_inf[i] = 0.;
      sumde_sup[i] = 0.;
    }
    for (Int_t ih = 0; ih < hfun.GetNhit(); ++ih) {
      // Factor 2/5 accounts for sumdE/E ratio
      // TODO: Optimize this factor for future
      if (siltotal > 0.) {
        if (lvl3->fMultiParticleSiID[it][ih] == 1) {  // photon-like
          sumde_val[ih] = 2.00 * calsumdE * silsumdE[ih] / siltotal;
          sumde_inf[ih] = 0.95 * sumde_val[ih];
          sumde_sup[ih] = 1.10 * sumde_val[ih];
        } else {  // hadron-like
          sumde_val[ih] = 5.00 * calsumdE * silsumdE[ih] / siltotal;
          sumde_inf[ih] = 0.50 * sumde_val[ih];
          sumde_sup[ih] = 2.00 * sumde_val[ih];
        }
      }
    }
    // starting point
    Double_t start_val[this->kMaxNparticle];
    Double_t start_inf[this->kMaxNparticle];
    Double_t start_sup[this->kMaxNparticle];
    for (Int_t i = 0; i < this->kMaxNparticle; ++i) {
      start_val[i] = -2.;
      start_inf[i] = -2.;
      start_sup[i] = -2.;
    }
    for (Int_t ih = 0; ih < hfun.GetNhit(); ++ih) {
      for (Int_t il = 0; il < ypsil[ih].size(); ++il)
        if (ypsil[ih][il] > 0.) {
          if (lvl3->fMultiParticleSiID[it][ih] == 1) {  // photon-like
            start_inf[ih] = -2.0;
            start_sup[ih] = +2.0;
            start_val[ih] = +4.0;  // strict values for photon-like
          } else {                 // hadron-like
            start_inf[ih] = il > 0. ? xpsil[ih][il - 1] : -2.;
            start_sup[ih] = xpsil[ih][il];
            start_val[ih] = 0.5 * (start_inf[ih] + start_sup[ih]);
          }
          break;
        }
    }
    // TODO: Understand why large gap in range force initial value to something larger than average

    /* Set the free variables to be minimized */
    for (Int_t ih = 0; ih < hfun.GetNhit(); ++ih) {
      // TODO: Understand the best initial parameters (and plausible limits!)

      // Alpha developing parameter
      Double_t alpha_val = lvl3->fMultiParticleSiID[it][ih] == 1 ? 2.5 : 2.5;
      Double_t alpha_inf = lvl3->fMultiParticleSiID[it][ih] == 1 ? 0.5 : 0.5;
      Double_t alpha_sup = lvl3->fMultiParticleSiID[it][ih] == 1 ? 7.5 : 7.5;

      // Theta attenuation parameter
      Double_t theta_val = lvl3->fMultiParticleSiID[it][ih] == 1 ? 2.5 : 2.5;
      Double_t theta_inf = lvl3->fMultiParticleSiID[it][ih] == 1 ? 0.5 : 0.5;
      Double_t theta_sup = lvl3->fMultiParticleSiID[it][ih] == 1 ? 7.5 : 7.5;

      Utils::Printf(Utils::kPrintDebugFull, "Initial Parameters:\n");
      Utils::Printf(Utils::kPrintDebugFull, "\t sumdE: %f in [%f, %f]\n", sumde_val[ih], sumde_inf[ih], sumde_sup[ih]);
      Utils::Printf(Utils::kPrintDebugFull, "\t Alpha: %f in [%f, %f]\n", alpha_val, alpha_inf, alpha_sup);
      Utils::Printf(Utils::kPrintDebugFull, "\t Theta: %f in [%f, %f]\n", theta_val, theta_inf, theta_sup);
      Utils::Printf(Utils::kPrintDebugFull, "\t Start: %f in [%f, %f]\n", start_val[ih], start_inf[ih], start_sup[ih]);

      const Int_t ipar = ih * hfun.GetNparamSingle();
      minimum->SetLimitedVariable(0 + ipar, Form("sumdE (peak %d)", ih), sumde_val[ih], 1.00, sumde_inf[ih],
                                  sumde_sup[ih]);
      minimum->SetLimitedVariable(1 + ipar, Form("Alpha (peak %d)", ih), alpha_val, 0.10, alpha_inf, alpha_sup);
      minimum->SetLimitedVariable(2 + ipar, Form("Theta (peak %d)", ih), theta_val, 0.10, theta_inf, theta_sup);
      minimum->SetLimitedVariable(3 + ipar, Form("Start (peak %d)", ih), start_val[ih], 0.10, start_inf[ih],
                                  start_sup[ih]);
      // TODO: Understand which is the reasonable upper value for alpha, theta: 10 is too much, but sometimes necessary
    }

    /* Do the minimization */
    Utils::Printf(Utils::kPrintDebugFull, "Minimize!\n");
    minimum->Minimize();

    // minimum->FixVariable(0);
    // minimum->ReleaseVariable(1);
    // minimum->Minimize();

    // minimum->ReleaseVariable(0);
    // minimum->Minimize();

    /* Copy result to Level3 */
    const Double_t *min_par = minimum->X();
    // const Double_t *min_err = minimum->Errors();
    // ROOT::Minuit2::Minuit2Minimizer *minuit2 = (ROOT::Minuit2::Minuit2Minimizer *)minimum;
    // cout << minuit2->State().IsValid() << endl;
    // if (minuit2->State().IsValid())
    for (Int_t ipar = 0; ipar < hfun.GetNparam(); ++ipar) {
      Double_t minos_low, minos_high;
      minimum->GetMinosError(ipar, minos_low, minos_high);
      // lvl3->fPhotonPosFitPar[it][il][iv][ipar] = min_par[ipar]; //TODO: Create lvl3 field and fill them
      // lvl3->fPhotonPosFitErr[it][il][iv][ipar][0] = minos_low;
      // lvl3->fPhotonPosFitErr[it][il][iv][ipar][1] = minos_high;
    }
    // lvl3->fPhotonPosFitChi2[it][il][iv] = minimum->MinValue();
    // lvl3->fPhotonPosFitNDF[it][il][iv] = hfun.GetNpoint() - hfun.GetNparam();
    // lvl3->fPhotonPosFitEDM[it][il][iv] = minimum->Edm();

    /* Associate the layer-by layer energy deposit to each single particle */
    vector<TF1 *> fLonProFit;
    fLonProFit.resize(nPeak);
    for (Int_t ih = 0; ih < nPeak; ++ih) {
      fLonProFit[ih] =
          new TF1(Form("LonProFit_Tower%d_Particle%d", it, ih), "[0]*ROOT::Math::gamma_pdf(x,[1],[2],[3])", 0., 44.);
      for (Int_t ipar = 0; ipar < nPars; ++ipar) fLonProFit[ih]->SetParameter(ipar, min_par[ipar + ih * nPars]);
    }
    for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) {
      Double_t depth = this->kCalLayerX0[it][il];
      Double_t calor = lvl2->fCalorimeter[it][il];
      Double_t silic = 0.;
      for (Int_t ih = 0; ih < nPeak; ++ih) {
        Double_t value = fLonProFit[ih]->Eval(depth);
        // TODO: Understand what to to with negative function
        if (value > 0.) silic += value;
      }
      for (Int_t ih = 0; ih < nPeak; ++ih) {
        Double_t fract = 0.;
        Double_t value = fLonProFit[ih]->Eval(depth);
        // TODO: Understand what to to with negative function
        if (value > 0. && silic > 0.) fract = value / silic;
        fMultihitCalorimeter[it][il][ih] = calor * fract;
        printf("FRACTION %d %d %f %f %f %f\n", il, ih, value, fract, lvl2->fCalorimeter[it][il],
               fMultihitCalorimeter[it][il][ih]);
      }
    }
    for (Int_t ih = 0; ih < nPeak; ++ih) {
      delete fLonProFit[ih];
      fLonProFit[ih] = NULL;
    }

#ifdef DEBUG_LON
    string name;

    if (fDebug_lonprotot[it]) {
      delete fDebug_lonprotot[it];
      fDebug_lonprotot[it] = NULL;
    }

    name = Form("debug_fitlontot_%d", it);
    fDebug_lonprotot[it] = new TF1(
        name.c_str(), "[0]*ROOT::Math::gamma_pdf(x,[1],[2],[3])+[4]*ROOT::Math::gamma_pdf(x,[5],[6],[7])", 0., 44.);
    for (Int_t ipar = 0; ipar < nPars * nPeak; ++ipar) fDebug_lonprotot[it]->SetParameter(ipar, min_par[ipar]);
    fDebug_lonprotot[it]->SetLineColor(kViolet);
    fDebug_lonprotot[it]->SetLineStyle(kDashed);
    fDebug_lonprotot[it]->SetRange(0, 44.);
    fDebug_lonprotot[it]->SetNpx(1000);

    for (Int_t ih = 0; ih < nPeak; ++ih) {
      if (fDebug_lonpropar[it][ih]) {
        delete fDebug_lonpropar[it][ih];
        fDebug_lonpropar[it][ih] = NULL;
      }

      // LonFitSingleHitFCN hsin(nPars);
      name = Form("debug_fitlonpar_%d_%d", it, ih);
      // fDebug_lonpropar[it][ih] = new TF1(name.c_str(), &hsin, &LonFitSingleHitFCN::FitFunc, 0., 44.,
      // hfun.GetNparamSingle());
      fDebug_lonpropar[it][ih] = new TF1(name.c_str(), "[0]*ROOT::Math::gamma_pdf(x,[1],[2],[3])", 0., 44.);
      for (Int_t ipar = 0; ipar < nPars; ++ipar)
        fDebug_lonpropar[it][ih]->SetParameter(ipar, min_par[ipar + ih * nPars]);
      fDebug_lonpropar[it][ih]->SetLineColor(2 + ih);
      fDebug_lonpropar[it][ih]->SetLineStyle(kDashed);
      fDebug_lonpropar[it][ih]->SetRange(0, 44.);
      fDebug_lonpropar[it][ih]->SetNpx(1000);

      if (fDebug_lonproint[it][ih]) {
        delete fDebug_lonproint[it][ih];
        fDebug_lonproint[it][ih] = NULL;
      }
      // LonFitSingleHitFCN hsin(nPars);
      fDebug_lonproint[it][ih] = new TGraph();
      fDebug_lonproint[it][ih]->SetName(Form("debug_fitlonint_%d_%d", it, ih));
      for (Int_t il = 0; il < lvl2->kCalNlayer; ++il)
        fDebug_lonproint[it][ih]->SetPoint(il, this->kCalLayerX0[it][il],
                                           fMultihitCalorimeter[it][il][ih] / (calThickness * calMIP));
      fDebug_lonproint[it][ih]->SetMarkerSize(2);
      fDebug_lonproint[it][ih]->SetMarkerColor(2 + ih);
      fDebug_lonproint[it][ih]->SetMarkerStyle(kOpenDiamond);
    }

    DrawLongitudinalFit(it, nPeak);
#endif

    delete minimum;

  }  // Tower Loop
}

/*** Identify multihit event in all combinations (gg, gh, hh) ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::MultihitIdentification(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  // Eugenio: need to solve all TODO here
  TrackClustering();
  TrackRefinement(lvl2);
  TrackLorentzFit(lvl2, lvl3);
  TrackExtraction();
  TrackCorrection(lvl3);
  // PID
  TrackSeparation(lvl3);
  // Energy
  TrackGamma2DFit(lvl2, lvl3);
  // TODO: Skip all this useless stuff if the event is a singlehit?
}

/*** Get lateral shower width around track projection ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::LateralShowerWidth(Level2<armclass> *lvl2, Level3<armrec> *lvl3, bool neupho) {
  // Eugenio: need to solve all TODO here

  for (Int_t it = 0; it < this->kCalNtower; it++) {
    // same silicon detector is used for both towers in Arm2
    const Int_t thisSiTower = this->kArmIndex == 0 ? it : 0;

    Double_t glsquared = 0.;
    Double_t gldeposit = 0.;
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        const int trackStrip =
            PositionToChannel(it, il, iv, neupho ? lvl3->fNeutronPosition[it][iv] : lvl3->fPhotonPosition[it][iv]);

        const Int_t ch_inf = fChannelRange[it][iv][0];
        const Int_t ch_sup = fChannelRange[it][iv][1];

        const Int_t thisSiSample = 1.;  // fSiSample[it][il][iv]; //TODO: need to implement the realistic case

        /* In Old Silicon:
         * - G > 2000 ADC/GeV  (sample 0)
         *     > 20000 ADC/GeV (sample 1)
         * - N ~ 2 ADC
         * - S > 10 N = 20 ADC = 0.01 GeV  (sample 0)
         * 	  				   = 0.001 GeV (sample 1)
         * ... and considering 5 strip (including core) with at least this S:
         * - T > 0.05 GeV  (sample 0)
         * 	   > 0.005 GeV (sample 1)
         */
        const Int_t nStripCore = 10;
        const Double_t nStripThreshold = 0.001;
        const Double_t nLayerThreshold = 0.005;
        // TODO: need to optimize these thresholds

        Double_t sum = 0.;
        for (Int_t ic = trackStrip - nStripCore; ic <= trackStrip + nStripCore; ++ic) {
          if (ic < ch_inf || ic > ch_sup) continue;
          const Double_t signal = lvl2->fPosDet[thisSiTower][il][iv][ic][thisSiSample];
          if (signal > nStripThreshold) sum += signal;
        }

        if (sum < nLayerThreshold) continue;

        Double_t sisquared = 0.;
        Double_t sideposit = 0.;
        for (Int_t ic = ch_inf; ic <= ch_sup; ++ic) {
          const Double_t signal = lvl2->fPosDet[thisSiTower][il][iv][ic][thisSiSample];
          if (signal > nStripThreshold) {
            sisquared += signal * TMath::Power(ic - trackStrip, 2.);
            sideposit += signal;
            glsquared += signal * TMath::Power(ic - trackStrip, 2.);
            gldeposit += signal;
          }
        }
        if (sideposit > 0.) lvl3->fShowerWidth[it][il][iv] = TMath::Sqrt(sisquared / sideposit);
        Utils::Printf(Utils::kPrintDebug, "\t Shower Width[%d][%d][%d] = %f\n", it, il, iv,
                      lvl3->fShowerWidth[it][il][iv]);
      }
    if (gldeposit > 0.) lvl3->fShowerWidthTotal[it] = TMath::Sqrt(glsquared / gldeposit);
    Utils::Printf(Utils::kPrintDebug, "\t\t Total Shower Width [%d] = %f\n", it, lvl3->fShowerWidthTotal[it]);
  }
}

/*** Get energy deposit in silicon in the whole tower ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::GetSiliconDeposit(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  // For Elena: TODO
  // Note: It can be mostly copied by SetMaxLayer function
}

/*** Get energy deposit in silicon in the given function ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::GetPhotonSiliconArea(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  // For Elena: TODO
  // It is necessary to handle singlehit/multihit case using lvl3->fPhotonMultiHit[it]
  // You can use GetPeakArea from CheckRunIIIData as base (remove silicon saturation condition)
  // It is better to add a function like PhotonAreaMH in Level3 and, in turn, like GetArea in PosFitFCN
  // Is maximum deposit on x view automatically associated to maximum deposit on y view?
}

/*** Get energy deposit in silicon in the given function ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::GetNeutronSiliconArea(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  // TODO
}

/*** Get energy deposit in silicon around track projection ***/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::SiliconCharge(Level2<armclass> *lvl2, Level3<armrec> *lvl3, bool neupho) {
  vector<vector<int>> stripTrack;
  Utils::AllocateVector2D(stripTrack, this->kCalNtower, this->kPosNview);
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      stripTrack[it][iv] = PositionToChannel(it, lvl3->fPosMaxLayer[it][iv], iv,
                                             neupho ? lvl3->fNeutronPosition[it][iv] : lvl3->fPhotonPosition[it][iv]);
    }

  for (Int_t it = 0; it < this->kCalNtower; it++)
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        const Int_t ch_low = fChannelRange[it][iv][0];
        const Int_t ch_high = fChannelRange[it][iv][1];
        const Int_t n_ch = ch_high - ch_low + 1;

        Int_t max_ch;
        Double_t max_value = -1E30;

        // same silicon detector is used for both towers in Arm2
        const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;
        // sample #1 is used for Arm2 (no sample choice for Arm1)
        const Int_t pos_sample = fSiSample[it][il][iv];

        Utils::Printf(Utils::kPrintDebugFull, "---------- %d %d %d : %d\n", it, il, iv, stripTrack[it][iv]);

        // for (Int_t ic = ch_low; ic <= ch_high; ++ic) {
        for (int istrip = -10; istrip <= 10; ++istrip) {
          int thisstrip = 0;
          double signal = 0;

          thisstrip = stripTrack[it][iv] + istrip;
          if (istrip < 0)
            Utils::Printf(Utils::kPrintDebugFull, "- Inferior Strip %d\n", thisstrip);
          else if (istrip > 0)
            Utils::Printf(Utils::kPrintDebugFull, "- Superior Strip %d\n", thisstrip);
          else
            Utils::Printf(Utils::kPrintDebugFull, "- Central Strip %d\n", thisstrip);
          if (thisstrip < ch_low) {
            Utils::Printf(Utils::kPrintDebugFull, "- %d < %d -> %d\n", thisstrip, ch_low, stripTrack[it][iv] - istrip);
            thisstrip = stripTrack[it][iv] - istrip;
          }
          if (thisstrip > ch_high) {
            Utils::Printf(Utils::kPrintDebugFull, "- %d > %d -> %d\n", thisstrip, ch_high, stripTrack[it][iv] - istrip);
            thisstrip = stripTrack[it][iv] - istrip;
          }
          if (thisstrip < ch_low || thisstrip > ch_high) {
            Utils::Printf(Utils::kPrintDebugFull, "- %d outside [%d, %d]-Incompatible!\n", thisstrip, ch_low, ch_high);
            continue;
          }

          if (fReadTableRec.fMaskPosDet[pos_tower][il][iv][thisstrip][pos_sample]) {
            signal = lvl2->fPosDet[pos_tower][il][iv][thisstrip][pos_sample];
            Utils::Printf(Utils::kPrintDebugFull, "- Strip %d Signal %.6f\n", thisstrip, signal);
            if (TMath::Abs(istrip) <= 3) {
              if (signal > lvl3->fSiliconCharge[it][il][iv]) {
                lvl3->fSiliconCharge[it][il][iv] = signal;
                Utils::Printf(Utils::kPrintDebugFull, "- Updating 3 Strip Maximum = %.6f\n",
                              lvl3->fSiliconCharge[it][il][iv]);
              }
            }
          } else {
            Utils::Printf(Utils::kPrintDebugFull, "- %d is masked \n", thisstrip);
          }
        }
      }
}

/*-----------------------------*/
/*--- Energy reconstruction ---*/
/*-----------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::EnergyReconstruction(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  /* Single-hit reconstruction */
  if (fPhotonEnable) {
    // only leakage-out
    GetPhotonLevel2(lvl2);
    PhotonLeakEffCorrection(lvl3);
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      const Double_t sumde = GetPhotonSumdE(it);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tsumde = %0.1lf\n", sumde);
      lvl3->fPhotonEnergy[it] = GetPhotonEnergy(sumde, it);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tE = %0.1lf\n", lvl3->fPhotonEnergy[it]);
    }

    // leakage-out and leakage-in correction (1st iter)
    PhotonLeakInCorrection(lvl2, lvl3);
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      const Double_t sumde = GetPhotonSumdE(it);
      lvl3->fPhotonEnergyLin[it] = GetPhotonEnergy(sumde, it);
    }
    // leakage-out and leakage-in correction (2nd iter)
    PhotonLeakInCorrection(lvl2, lvl3);
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      const Double_t sumde = GetPhotonSumdE(it);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tsumde-in = %0.1lf\n", sumde);
      lvl3->fPhotonEnergyLin[it] = GetPhotonEnergy(sumde, it);
      Utils::Printf(Utils::kPrintDebugFull, "\t\tE-in = %0.1lf\n", lvl3->fPhotonEnergyLin[it]);
    }
  }

  if (fNeutronEnable) {
    // only leakage-out
    GetNeutronLevel2(lvl2);
    NeutronLeakCorrection(lvl3);
    NeutronEffCorrection(lvl3);
    for (Int_t it = 0; it < this->kCalNtower; ++it) {
      const Double_t sumde = GetNeutronSumdE(it);
      lvl3->fNeutronEnergy[it] = GetNeutronEnergy(sumde, it);
    }
  }

  Utils::Printf(Utils::kPrintDebug, "Test_Debug\n");
  Utils::Printf(Utils::kPrintDebugFull, "Test_Full\n");
  /* Multi-hit reconstruction */
  if (fPhotonEnable) {
    switch (fPhotonErecType) {
      case 1:
        EnergyReconstructionMultiHitVersion1(lvl2, lvl3);
        break;
      case 2:
        EnergyReconstructionMultiHitVersion2(lvl2, lvl3);
        break;
      default:
        EnergyReconstructionMultiHitVersion1(lvl2, lvl3);
        break;
    }
  }

#ifdef DEBUG_POS
  // for (Int_t it = 0; it < this->kCalNtower; ++it) DrawCalorimeter(lvl2, it); //Eugenio
  // for (Int_t it = 0; it < this->kCalNtower; ++it) {
  //   DrawCalX0(lvl2, it);
  //   DrawPosX0(lvl3, it);
  // }
#endif
}

/*--------------------------------------------------------*/
/*--- Energy reconstruction for multi-hit events ---------*/
/*--- version 1 : algorithm used in Mitsuka-san's library */
/*--------------------------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::EnergyReconstructionMultiHitVersion1(Level2<armclass> *lvl2,
                                                                              Level3<armrec> *lvl3) {
  // only leakage-out
  GetPhotonLevel2(lvl2);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    if (fPosRecMode == "none")  // re-evaluate fMultiPeak
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          fMultiPeak[it][il][iv] = lvl3->PhotonPositionMH(it, il, iv, 1, fFuncPhoton->GetFtype()) >= 0;

    // check if there is a multi-hit in each position layer
    // Bool_t good_t2 = true;
    // for (Int_t il = 0; il < this->kPosNlayerEM; ++il)
    // 	for (Int_t iv = 0; iv < this->kPosNview; ++iv)
    // 	  if (!fMultiPeak[it][il][iv]) good_t2 = false;
    // Bool_t good_t2 = false;
    // for (Int_t il = 0; il < this->kPosNlayerEM; ++il)
    // 	if (fMultiPeak[it][il][0] && fMultiPeak[it][il][1])
    // 	  good_t2 = true;

    // find layer for position
    Bool_t good_xy[] = {false, false};
    for (Int_t iv = 0; iv < this->kPosNview; ++iv)
      if (fMultiPeak[it][lvl3->fPosMaxLayer[it][iv]][iv]) {
        good_xy[iv] = true;
      } else {
        for (Int_t il = 0; il < this->kPosNlayerEM; ++il)
          if (fMultiPeak[it][il][iv]) {
            good_xy[iv] = true;
            break;
          }
      }

    // find if multi-hit in each layer (X or Y)
    Bool_t good_area = true;
    for (Int_t il = 0; il < this->kPosNlayerEM; ++il)
      if (!(fMultiPeak[it][il][0] || fMultiPeak[it][il][1])) good_area = false;

    Utils::Printf(Utils::kPrintDebugFull, "\t\tgood_xy = (%d, %d), good_area = %d\n", good_xy[0], good_xy[1],
                  good_area);

    if (good_xy[0] && good_xy[1] && good_area) {
      //                 1st particle                       2nd particle
      Double_t posx[] = {lvl3->fPhotonPositionMH[it][0][0], lvl3->fPhotonPositionMH[it][0][1]};
      Double_t posy[] = {lvl3->fPhotonPositionMH[it][1][0], lvl3->fPhotonPositionMH[it][1][1]};

      // reconstruct energy
      for (Int_t ip = 0; ip < 2; ++ip) {
        Double_t sumde = 0.;
        for (Int_t il = this->kPhotonFirstLayer; il <= this->kPhotonLastLayer; ++il) {
          const Int_t bound = 5;           // this value gives the best performance (Mitsuka)
          Int_t ilp = il < bound ? 0 : 1;  // position layer used
          Double_t weight = il < this->kDoubleStepLayer ? 1. : 2.;
          Double_t area[2] = {1., 0.};
          if (fMultiPeak[it][ilp][0] && fMultiPeak[it][ilp][1]) {  // use both X and Y views
            area[0] = (lvl3->PhotonAreaMH(it, ilp, 0, 0, fFuncPhoton->GetFtype()) +
                       lvl3->PhotonAreaMH(it, ilp, 1, 0, fFuncPhoton->GetFtype())) /
                      2.;  // 1st particle
            area[1] = (lvl3->PhotonAreaMH(it, ilp, 0, 1, fFuncPhoton->GetFtype()) +
                       lvl3->PhotonAreaMH(it, ilp, 1, 1, fFuncPhoton->GetFtype())) /
                      2.;                                                          // 2nd particle
          } else if (fMultiPeak[it][ilp][0]) {                                     // use only X view
            area[0] = lvl3->PhotonAreaMH(it, ilp, 0, 0, fFuncPhoton->GetFtype());  // 1st particle
            area[1] = lvl3->PhotonAreaMH(it, ilp, 0, 1, fFuncPhoton->GetFtype());  // 2nd particle
          } else if (fMultiPeak[it][ilp][1]) {                                     // use only Y view
            area[0] = lvl3->PhotonAreaMH(it, ilp, 1, 0, fFuncPhoton->GetFtype());  // 1st particle
            area[1] = lvl3->PhotonAreaMH(it, ilp, 1, 1, fFuncPhoton->GetFtype());  // 2nd particle
          }
          // Double_t cumlate[2][this->kPosNview];
          // for (Int_t ip = 0; ip < 2; ++ip) {
          //   for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
          // 	cumlate[ip][iv] = Cumlate2D(lvl3->fPhotonPosFitPar[it][ilp][iv][ip*this->kPosNpars+2],
          // 				    lvl3->fPhotonPosFitPar[it][ilp][iv][ip*this->kPosNpars+3],
          // 				    lvl3->fPhotonPosFitPar[it][ilp][iv][ip*this->kPosNpars+4],
          // 				    lvl3->fPhotonPosFitPar[it][ilp][iv][ip*this->kPosNpars+5],
          // 				    lvl3->fPhotonPosFitPar[it][ilp][iv][ip*this->kPosNpars+6]);
          //   }
          // }
          // for (Int_t ip = 0; ip < 2; ++ip) {
          //   if (fMultiPeak[it][ilp][0] && fMultiPeak[it][ilp][1]) { // use both X and Y views
          // 	area[ip] = (cumlate[ip][0] + cumlate[ip][1]) / 2.;
          //   } else if (fMultiPeak[it][ilp][0]) { // use only X view
          // 	area[ip] = cumlate[ip][0];
          //   } else if (fMultiPeak[it][ilp][1]) { // use only Y view
          // 	area[ip] = cumlate[ip][1];
          //   }
          // }
          Double_t leak[] = {GetPhotonLeakEffFactor(it, il, posx[0], posy[0]),   // 1st particle
                             GetPhotonLeakEffFactor(it, il, posx[1], posy[1])};  // 2nd particle
          sumde += weight * fPhotonCal[it][il] * area[ip] / (area[0] * leak[0] + area[1] * leak[1]);
          Utils::Printf(Utils::kPrintDebugFull, "\t\tarea = %lf, %lf\n", area[0], area[1]);
        }  // layer loop
        Utils::Printf(Utils::kPrintDebugFull, "\t\tsumde[%d] = %lf\n", ip, sumde);
        Utils::Printf(Utils::kPrintDebugFull, "\t\tpos[%d] = (%0.1lf, %0.1lf)\n", ip, posx[ip], posy[ip]);
        lvl3->fPhotonEnergyMH[it][ip] = GetPhotonEnergy(sumde, it);
      }  // particle loop

      // printf("E1 = %lf, E2 = %lf\n", lvl3->fPhotonEnergyMH[it][0], lvl3->fPhotonEnergyMH[it][1]);
      // getchar();
    } else {
      lvl3->fPhotonEnergyMH[it][0] = lvl3->fPhotonEnergy[it];
    }
  }  // tower loop
}

/*----------------------------------------------------*/
/*----- Calculate the GSO bar attenuation factor -----*/
/*----------------------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetGSObarAttenuationFactor(Int_t it, Int_t il, Int_t iv, Double_t pos) {
  // Arm2
  if (this->kArmIndex == Arm2Params::kArmIndex) return 1.;

  //
  const double_t attenuation_length = armcal::kPosAttFactor;  // defined in Arm1CalPars
  const Double_t tower_size = this->kTowerSize[it];
  if (pos < 0 || pos > tower_size)
    return 1.;
  else
    return TMath::Exp(-1. * (tower_size - pos) / attenuation_length);
}

/*--------------------------------------------------------*/
/*--- Energy reconstruction for multi-hit events ---------*/
/*--- version 2 : algorithm used in Menjo's simple analysis */
/*--------------------------------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::EnergyReconstructionMultiHitVersion2(Level2<armclass> *lvl2,
                                                                              Level3<armrec> *lvl3) {
  // This algorithm works only for two photon events

  // only leakage-out
  GetPhotonLevel2(lvl2);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    if (fPosRecMode == "none")  // re-evaluate fMultiPeak
      for (Int_t il = 0; il < this->kPosNlayer; ++il)
        for (Int_t iv = 0; iv < this->kPosNview; ++iv)
          fMultiPeak[it][il][iv] = lvl3->PhotonPositionMH(it, il, iv, 1, fFuncPhoton->GetFtype()) >= 0;

    Bool_t good_xy[] = {false, false};
    Double_t pos[2][2];     // [view][hit]
    Double_t height[2][2];  // [view][hit]
    Double_t energy_share[2] = {0.};

    Utils::Printf(Utils::kPrintDebugFull, "\t\t== MH Energy Reconstruction Tower = %d == \n", it);

    // Multi hit (2 hit) check done only in the 2nd layer
    for (int iv = 0; iv < this->kPosNview; ++iv) {
      if (fMultiPeak[it][1][iv]) {
        // sort by the pulse height
        Double_t tmp_pos[2];
        Double_t tmp_height[2];
        tmp_pos[0] = lvl3->PhotonPositionMH(it, 1, iv, 0, fFuncPhoton->GetFtype());
        tmp_pos[1] = lvl3->PhotonPositionMH(it, 1, iv, 1, fFuncPhoton->GetFtype());
        tmp_height[0] = lvl3->PhotonHeightMH(it, 1, iv, 0, -1., fFuncPhoton->GetFtype()) /
                        this->GetGSObarAttenuationFactor(it, 1, iv, tmp_pos[0]);  // -1. = automatic set to peak pos
        tmp_height[1] = lvl3->PhotonHeightMH(it, 1, iv, 1, -1., fFuncPhoton->GetFtype()) /
                        this->GetGSObarAttenuationFactor(it, 1, iv, tmp_pos[1]);

        Utils::Printf(Utils::kPrintDebugFull, "\t\tGSO bar att %lf\n",
                      this->GetGSObarAttenuationFactor(it, 1, iv, tmp_pos[0]));
        Utils::Printf(Utils::kPrintDebugFull, "\t\tGSO bar att %lf\n",
                      this->GetGSObarAttenuationFactor(it, 1, iv, tmp_pos[1]));

        if (tmp_height[0] > tmp_height[1]) {
          height[iv][0] = tmp_height[0];
          height[iv][1] = tmp_height[1];
          pos[iv][0] = tmp_pos[0];
          pos[iv][1] = tmp_pos[1];
        } else {
          height[iv][0] = tmp_height[1];
          height[iv][1] = tmp_height[0];
          pos[iv][0] = tmp_pos[1];
          pos[iv][1] = tmp_pos[0];
        }

        // Check the hit in the calorimeter acceptance.
        if (pos[iv][0] > 0. && pos[iv][0] < this->kTowerSize[it] && pos[iv][1] > 0. &&
            pos[iv][1] < this->kTowerSize[it]) {
          good_xy[iv] = true;
        }
      } else {
        pos[iv][0] = lvl3->PhotonPositionMH(it, 1, iv, 0, fFuncPhoton->GetFtype());
        pos[iv][1] = pos[iv][0];
      }

      //   if (it == 1) {
      //     cerr << " view :  " << iv << endl;
      //     cerr << "values" << endl;
      //     for (int j = 0; j < 40; j++) {
      //       cerr << setw(6) << setprecision(3) << fixed << lvl2->fPosDet[it][1][iv][j][0] << " ";
      //       if (j % 10 == 9) cerr << endl;
      //     }
      //     cerr << endl;
      //     cerr << "errors" << endl;
      //     for (int j = 0; j < 40; j++) {
      //       cerr << setw(6) << setprecision(3) << fixed << lvl2->fErrPosDet[it][1][iv][j][0] << " ";
      //       if (j % 10 == 9) cerr << endl;
      //     }
      //     cerr << endl;
      //     cerr << "fit 1st hit" << endl;
      //     for (int j = 0; j < 40; j++) {
      //       cerr << setw(6) << setprecision(3) << fixed
      //            << lvl3->PhotonHeightMH(it, 1, iv, 0, 0.5 + j, fFuncPhoton->GetFtype()) << " ";
      //       if (j % 10 == 9) cerr << endl;
      //     }
      //     cerr << endl;
      //     cerr << "fit 2nd hit" << endl;
      //     for (int j = 0; j < 40; j++) {
      //       cerr << setw(6) << setprecision(3) << fixed
      //            << lvl3->PhotonHeightMH(it, 1, iv, 1, 0.5 + j, fFuncPhoton->GetFtype()) << " ";
      //       if (j % 10 == 9) cerr << endl;
      //     }
      //     cerr << endl;
      //     cerr << "fit result" << endl;
      //     for (int j = 0; j < 21; j++) {
      //       cerr << setw(8) << setprecision(3) << lvl3->fPhotonPosFitPar[it][1][iv][j] << " ";
      //       if (j % 7 == 6) cerr << endl;
      //     }
      //     cerr << endl;
      //   }
    }

    // Case definition
    // 1 : two hits in both x and y
    // 2 : two hits only in one of xy

    Utils::Printf(Utils::kPrintDebugFull, "\t\tgood_xy = (%d, %d)\n", good_xy[0], good_xy[1]);
    Utils::Printf(Utils::kPrintDebugFull, "\t\theight_x = (%lf, %lf)\n", height[0][0], height[0][1]);
    Utils::Printf(Utils::kPrintDebugFull, "\t\theight_y = (%lf, %lf)\n", height[1][0], height[1][1]);
    // Estimation of Energy Sharing Factors

    double tmp1, tmp2;
    // Case of two hits in both x and y
    if (good_xy[0] && good_xy[1]) {
      // Calculate the average of peak height ratio
      for (int iv = 0; iv < this->kPosNview; ++iv) {
        energy_share[0] += height[iv][0] / (height[iv][0] + height[iv][1]) / 2.;
        energy_share[1] += height[iv][1] / (height[iv][0] + height[iv][1]) / 2.;
      }
    }
    // Case of two hits only in one of the layer
    else if (good_xy[0] || good_xy[1]) {
      Int_t xy_mh = (good_xy[0] ? 0 : 1);
      energy_share[0] += height[xy_mh][0] / (height[xy_mh][0] + height[xy_mh][1]);
      energy_share[1] += height[xy_mh][1] / (height[xy_mh][0] + height[xy_mh][1]);
    } else {
      energy_share[0] = 1.;
      energy_share[1] = 0.;
    }

    // reconstruct energy
    if (good_xy[0] || good_xy[1]) {
      for (Int_t ip = 0; ip < 2; ++ip) {
        Double_t sumde = 0.;
        for (Int_t il = this->kPhotonFirstLayer; il <= this->kPhotonLastLayer; ++il) {
          Double_t weight = il < this->kDoubleStepLayer ? 1. : 2.;
          Double_t leak[] = {GetPhotonLeakEffFactor(it, il, pos[0][0], pos[1][0]),   // 1st particle
                             GetPhotonLeakEffFactor(it, il, pos[0][1], pos[1][1])};  // 2nd particle
          Double_t c = leak[0] * energy_share[0] + leak[1] * energy_share[1];
          sumde += weight * fPhotonCal[it][il] * energy_share[ip] / c;
        }  // layer loop
        lvl3->fPhotonEnergyMH[it][ip] = GetPhotonEnergy(sumde, it);
      }  // particle loop

    } else {
      lvl3->fPhotonEnergyMH[it][0] = lvl3->fPhotonEnergy[it];
    }

    Utils::Printf(Utils::kPrintDebugFull, "\t\tgood_xy = (%d, %d)\n", good_xy[0], good_xy[1]);
    Utils::Printf(Utils::kPrintDebugFull, "\t\tpos = (%lf, %lf), (%lf, %lf)\n", pos[0][0], pos[1][0], pos[0][1],
                  pos[1][1]);
    Utils::Printf(Utils::kPrintDebugFull, "\t\tenergy_share = (%lf, %lf)\n", energy_share[0], energy_share[1]);
    Utils::Printf(Utils::kPrintDebugFull, "\t\tE1 = %lf, E2 = %lf\n", lvl3->fPhotonEnergyMH[it][0],
                  lvl3->fPhotonEnergyMH[it][1]);
  }  // tower loop
}

/* Copy lvl2 to fPhotonCal */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::GetPhotonLevel2(Level2<armclass> *lvl2) {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) fPhotonCal[it][il] = lvl2->fCalorimeter[it][il];
}

/* Copy lvl2 to fNeutronCal */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::GetNeutronLevel2(Level2<armclass> *lvl2) {
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    for (Int_t il = 0; il < this->kCalNlayer; ++il) fNeutronCal[it][il] = lvl2->fCalorimeter[it][il];
}

/* Correct both leakage and efficiency effects */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonLeakEffCorrection(Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      const Double_t factor =
          GetPhotonLeakEffFactor(it, il, lvl3->fPhotonPosition[it][0], lvl3->fPhotonPosition[it][1]);
      /* apply correction */
      fPhotonCal[it][il] /= factor;
    }  // layer loop
  }    // tower loop
}

/* Get leakage and efficiency correction factor */
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetPhotonLeakEffFactor(Int_t it, Int_t il, Double_t x, Double_t y) {
  /* bound position between map histogram */
  Double_t position[this->kPosNview];
  for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
    TAxis *ax = iv == 0 ? fReadTableRec.fPhotonLeakEffMap[it][il]->GetXaxis()
                        : fReadTableRec.fPhotonLeakEffMap[it][il]->GetYaxis();
    const Double_t lower_edge = ax->GetXmin();
    const Double_t upper_edge = ax->GetXmax();
    const Double_t tol = 0.000001;
    position[iv] = iv == 0 ? x : y;
    if (position[iv] < lower_edge)
      position[iv] = lower_edge;
    else if (position[iv] >= upper_edge)
      position[iv] = upper_edge - tol;
  }
  /* interpolate to get leakage factor */
  return fReadTableRec.fPhotonLeakEffMap[it][il]->Interpolate(position[0], position[1]);
}

/* Correct leakage-in effect */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonLeakInCorrection(Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  Double_t ene[this->kCalNtower];
  // at the first iteration, use energy with only leak-out correction
  for (Int_t it = 0; it < this->kCalNtower; ++it)
    ene[it] = lvl3->fPhotonEnergyLin[it] > 0. ? lvl3->fPhotonEnergyLin[it] : lvl3->fPhotonEnergy[it];

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    const Int_t ot = it == 0 ? 1 : 0;  // other tower
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      const Double_t Lout[] = {
          // this tower
          GetPhotonLeakEffFactor(it, il, lvl3->fPhotonPosition[it][0], lvl3->fPhotonPosition[it][1]),
          // other tower
          GetPhotonLeakEffFactor(ot, il, lvl3->fPhotonPosition[ot][0], lvl3->fPhotonPosition[ot][1])};
      const Double_t Lin[] = {
          GetPhotonLeakInFactor(it, il, lvl3->fPhotonPosition[it][0], lvl3->fPhotonPosition[it][1],
                                ene[it]),  // this tower
          GetPhotonLeakInFactor(ot, il, lvl3->fPhotonPosition[ot][0], lvl3->fPhotonPosition[ot][1],
                                ene[ot])  // other tower
      };
      const Double_t norm = Lout[0] * Lout[1] - Lin[0] * Lin[1];

      /* apply correction */
      fPhotonCal[it][il] = (Lout[1] * lvl2->fCalorimeter[it][il] - Lin[1] * lvl2->fCalorimeter[ot][il]) / norm;

    }  // layer loop
  }    // tower loop
}

/* Get leakage and efficiency correction factor */
template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetPhotonLeakInFactor(Int_t it, Int_t il, Double_t x, Double_t y,
                                                                   Double_t energy) {
  if (this->kArmIndex == Arm2Params::kArmIndex)
    x = this->kTowerSize[it] - x;  // !!! TEMP using old table where x-axis was mirrored !!!

  const Double_t step = this->kTowerSize[it] / (Double_t)this->kLeakBins[it];
  Int_t ix = (Int_t)(x / step + 0.5);  // round-off
  Int_t iy = (Int_t)(y / step + 0.5);  // round-off
  if (ix < 0 || ix >= this->kLeakBins[it] || iy < 0 || iy >= this->kLeakBins[it]) return 0.;

  const Double_t a = fReadTableRec.fPhotonLeakInMap[it][ix][iy][0];
  const Double_t b = fReadTableRec.fPhotonLeakInMap[it][ix][iy][1];
  const Double_t c = fReadTableRec.fPhotonLeakInMap[it][ix][iy][2];

  return a * TMath::Exp(b * energy) + c;
}

/* Correct leakage effect */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronLeakCorrection(Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /* bound position between histogram edges */
    Double_t position[this->kPosNview];
    for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
      TAxis *ax =
          iv == 0 ? fReadTableRec.fNeutronLeakMap[it]->GetXaxis() : fReadTableRec.fNeutronLeakMap[it]->GetYaxis();
      const Double_t lower_edge = ax->GetXmin();
      const Double_t upper_edge = ax->GetXmax();
      const Double_t tol = 0.000001;  // [mm]
      position[iv] = lvl3->fNeutronPosition[it][iv];
      if (position[iv] < lower_edge)
        position[iv] = lower_edge;
      else if (position[iv] >= upper_edge)
        position[iv] = upper_edge - tol;
    }
    /* interpolate to get leakage factor */
    const Double_t factor = fReadTableRec.fNeutronLeakMap[it]->Interpolate(position[0], position[1]);
    /* apply correction */
    for (Int_t il = 0; il < this->kCalNlayer; ++il) fNeutronCal[it][il] /= factor;
  }  // tower loop
}

/* Correct efficiency effect */
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronEffCorrection(Level3<armrec> *lvl3) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      /* bound position between map histogram */
      Double_t position[this->kPosNview];
      for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
        TAxis *ax = iv == 0 ? fReadTableRec.fNeutronEffMap[it][il]->GetXaxis()
                            : fReadTableRec.fNeutronEffMap[it][il]->GetYaxis();
        const Double_t lower_edge = ax->GetXmin();
        const Double_t upper_edge = ax->GetXmax();
        const Double_t tol = 0.000001;  // [mm]
        position[iv] = lvl3->fNeutronPosition[it][iv];
        if (position[iv] < lower_edge)
          position[iv] = lower_edge;
        else if (position[iv] >= upper_edge)
          position[iv] = upper_edge - tol;
      }
      /* interpolate to get efficiency factor */
      const Double_t factor = fReadTableRec.fNeutronEffMap[it][il]->Interpolate(position[0], position[1]);
      /* apply correction */
      fNeutronCal[it][il] /= factor;
    }  // layer loop
  }    // tower loop
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetPhotonSumdE(Int_t it) {
  Double_t sumde = 0.;
  for (Int_t il = this->kPhotonFirstLayer; il <= this->kPhotonLastLayer; ++il) {
    if (il < this->kDoubleStepLayer)
      sumde += fPhotonCal[it][il];
    else
      sumde += 2 * fPhotonCal[it][il];
  }
  return sumde;
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetNeutronSumdE(Int_t it) {
  Double_t sumde = 0.;
  for (Int_t il = this->kNeutronFirstLayer; il <= this->kNeutronLastLayer; ++il) {
    if (il < this->kDoubleStepLayer)
      sumde += fNeutronCal[it][il];
    else
      sumde += 2 * fNeutronCal[it][il];
  }
  return sumde;
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetPhotonEnergy(Double_t sumde, Int_t it) {
  if (this->kArmIndex == Arm1Params::kArmIndex) {
    // For Arm1 ----------------------------
    // E = a*SumdE^2 + b*sumdE + c
    const Double_t a = fReadTableRec.fPhotonEconvPars[it][0];
    const Double_t b = fReadTableRec.fPhotonEconvPars[it][1];
    const Double_t c = fReadTableRec.fPhotonEconvPars[it][2];
    // Utils::Printf(Utils::kPrintInfo,"a,b,c = %e, %lf, %lf", a, b, c);
    return a * sumde * sumde + b * sumde + c;
  } else {
    // For Arm2 ----------------------------
    // SumdE = a*E^2 + b*E + c
    const Double_t a = fReadTableRec.fPhotonEconvPars[it][0];
    const Double_t b = fReadTableRec.fPhotonEconvPars[it][1];
    const Double_t c = fReadTableRec.fPhotonEconvPars[it][2] - sumde;

    if (a == 0.) {
      return -c / b;
    } else if (b == 0. && c == 0.) {
      return 0.;
    } else {
      Double_t d = b * b - 4 * a * c;
      if (d >= 0.) {
        // Double_t x1 = (- b - TMath::Sqrt(d)) / (2*a);
        // Double_t x2 = (- b + TMath::Sqrt(d)) / (2*a);
        // return TMath::Max(x1, x2);
        Double_t x1 = (TMath::Abs(b) + TMath::Sqrt(d)) / (2. * a);
        if (b > 0.0) x1 = -x1;
        Double_t x2 = c / (a * x1);
        Utils::Printf(Utils::kPrintDebugFull, "\t\tx1=%g x2=%g -c/b=%g d=%g\n", x1, x2, -c / b, d);
        return x2;
      } else {
        return 0.;
      }
    }
  }
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetNeutronEnergy(Double_t sumde, Int_t it) {
  if (this->kArmIndex == Arm1Params::kArmIndex) {
    // For Arm1 ----------------------------
    // E = a*SumdE^2 + b*sumdE + c

    // const Double_t a = fReadTableRec.fPhotonEconvPars[it][0];
    // const Double_t b = fReadTableRec.fPhotonEconvPars[it][1];
    // const Double_t c = fReadTableRec.fPhotonEconvPars[it][2];

    // Utils::Printf(Utils::kPrintInfo,"a,b,c = %e, %lf, %lf", a, b, c);
    // return a*sumde*sumde + b*sumde + c;

    // 02/11/2022 edited by Kondo based on Ueno's master thesis
    // E = a*SumdE + b
    const Double_t a = fReadTableRec.fNeutronEconvPars[it][0];
    const Double_t b = fReadTableRec.fNeutronEconvPars[it][1];
    // cout << "a=" << a << endl;
    // cout << "b=" << b << endl;
    // cout << "sumde=" << sumde << endl;
    // cout << "Neutron Energy = " << a*sumde + b << endl;
    return a * sumde + b;
  } else {
    // For Arm2 ----------------------------
    // SumdE = a*E^2 + b*E + c
    const Double_t a = fReadTableRec.fNeutronEconvPars[it][0];
    const Double_t b = fReadTableRec.fNeutronEconvPars[it][1];
    const Double_t c = fReadTableRec.fNeutronEconvPars[it][2] - sumde;
    if (a == 0.) {
      return -c / b;
    } else if (b == 0. && c == 0.) {
      return 0.;
    } else {
      Double_t d = b * b - 4 * a * c;
      if (d >= 0.) {
        Double_t x1 = (-b - TMath::Sqrt(d)) / (2 * a);
        Double_t x2 = (-b + TMath::Sqrt(d)) / (2 * a);
        return TMath::Max(x1, x2);
      } else {
        return 0.;
      }
    }
  }
}

/*-------------------------------*/
/*--- Particle identification ---*/
/*-------------------------------*/
template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PIDReconstruction(Level3<armrec> *lvl3) {
  if (fPhotonEnable) PhotonPIDRec(lvl3);
  if (fNeutronEnable) NeutronPIDRec(lvl3);
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PhotonPIDRec(Level3<armrec> *lvl3) {
  CalculateL20L90(fPhotonCal, lvl3->fPhotonL20, lvl3->fPhotonL90);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    if (lvl3->fPhotonL90[it] < L90Boundary(it, lvl3->fPhotonEnergy[it]))
      lvl3->fIsPhoton[it] = true;
    else
      lvl3->fIsPhoton[it] = false;
  }
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::NeutronPIDRec(Level3<armrec> *lvl3) {
  CalculateL20L90(fNeutronCal, lvl3->fNeutronL20, lvl3->fNeutronL90);
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    Double_t l2d = GetL2D(lvl3->fNeutronL20[it], lvl3->fNeutronL90[it]);
    if (l2d > L2DBoundary(it))
      lvl3->fIsNeutron[it] = true;
    else
      lvl3->fIsNeutron[it] = false;
  }
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::CalculateL20L90(vector<vector<Double_t>> &cal, vector<Double_t> &l20,
                                                         vector<Double_t> &l90) {
  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    /* calculate SumdE */
    Double_t sumde = 0.;
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      if (il < this->kDoubleStepLayer)
        sumde += cal[it][il];
      else
        sumde += 2 * cal[it][il];
    }

    /* Get L20% and L90% */
    Double_t tmp_sum = 0.;
    Double_t old_sum = 0.;
    Double_t tmp_x0 = 0.;
    Double_t old_x0 = 0.;
    Bool_t got_l20 = false;
    Bool_t got_l90 = false;
    for (Int_t il = 0; il < this->kCalNlayer; ++il) {
      old_sum = tmp_sum;
      old_x0 = tmp_x0;
      tmp_x0 = GetLayerDepth(il);

      if (il < this->kDoubleStepLayer)
        tmp_sum += cal[it][il] / sumde;
      else
        tmp_sum += 2 * cal[it][il] / sumde;

      // get L20%
      if (!got_l20 && tmp_sum > this->kL20Thr) {
        if (il > 0)
          l20[it] = Utils::LinearXinterpolation(old_x0, tmp_x0, old_sum, tmp_sum, this->kL20Thr);
        else
          l20[it] = tmp_x0;
        got_l20 = true;
      }

      // get L90%
      if (!got_l90 && tmp_sum > this->kL90Thr) {
        if (il > 0)
          l90[it] = Utils::LinearXinterpolation(old_x0, tmp_x0, old_sum, tmp_sum, this->kL90Thr);
        else
          l90[it] = tmp_x0;
        got_l90 = true;
      }

      if (got_l90 && got_l20) break;
    }  // layer loop
  }    // tower loop
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetLayerDepth(Int_t il) {
  /* tower dependences kept in case of a more accurate implementation... */
  if (il < this->kDoubleStepLayer)
    return Double_t(il + 1) * this->kSampleStep;
  else
    return Double_t(this->kDoubleStepLayer) * this->kSampleStep +
           Double_t(il + 1 - this->kDoubleStepLayer) * 2 * this->kSampleStep;
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::GetL2D(Double_t l20, Double_t l90) {
  return l90 - 0.25 * l20;
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::L90Boundary(Int_t tower, Double_t energy, int eff_thr) {
  Double_t l90boundary;
  if (eff_thr == 85) {
    /*--- 85% ---*/
    const double p0[] = {2.42448, 1.57536};
    const double p1[] = {0.812548, 99.6024};
    const double p2[] = {1020.80, 36364.5};
    l90boundary = p0[tower] * log(p1[tower] * energy + p2[tower]);
  } else if (eff_thr == 95) {
    /*--- 95% ---*/
    const double p0[] = {2.90891, 1.76900};
    const double p1[] = {0.265344, 42.7327};
    const double p2[] = {582.268, 38810.9};
    l90boundary = p0[tower] * log(p1[tower] * energy + p2[tower]);
  } else {
    /*--- 90% ---*/
    const double p0[] = {2.15520, 1.65621};
    const double p1[] = {3.00547, 69.2783};
    const double p2[] = {3046.61, 35019.9};
    l90boundary = p0[tower] * log(p1[tower] * energy + p2[tower]);
  }
  return l90boundary;
}

template <typename armclass, typename armcal, typename armrec>
Double_t EventRec<armclass, armcal, armrec>::L2DBoundary(Int_t tower) {
  return 20.;
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::DrawTrueInformation(McEvent *mc, Int_t it, Int_t il, Int_t iv) {
  const Int_t ntruehit = mc->CheckParticleInTower(this->kArmIndex, it, 1., 0.);
  if (ntruehit == 0) return;

  Int_t style[3] = {1, 9, 2};

  for (Int_t ip = 0; ip < ntruehit; ++ip) {
    double TrueCalPos[2] = {-1000., -1000.};
    // TODO [TrackProjection]: Check this change
    TVector3 lhc_pos = mc->fReference[ip]->Position();
    TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(this->kArmIndex, it, lhc_pos);
    TrueCalPos[0] = cal_pos.X();
    TrueCalPos[1] = cal_pos.Y();
    double silchn = PositionToChannel(it, iv, TrueCalPos[iv]);

    gPad->Modified();
    gPad->Update();

    fDebug_true[it][il][iv][ip] = new TLine(silchn, 0, silchn, gPad->GetUymax());
    fDebug_true[it][il][iv][ip]->SetLineWidth(2);
    fDebug_true[it][il][iv][ip]->SetLineStyle(mc->fReference[ip]->Energy() > 50 ? style[ip] : 10);
    fDebug_true[it][il][iv][ip]->SetLineColor(mc->fReference[ip]->PdgCode() == 22 ? kOrange + 7 : kGray + 2);
    fDebug_true[it][il][iv][ip]->Draw();

    gPad->Modified();
    gPad->Update();
  }
}

template <typename armclass, typename armcal, typename armrec>
template <typename fitfunc>
void EventRec<armclass, armcal, armrec>::DrawPositionFit(McEvent *mc, Level2<armclass> *lvl2, Int_t it, Int_t il,
                                                         Int_t iv, TF1 *func, fitfunc *hfun, const Double_t *min_par) {
  /* !!! DEBUG !!! */

  // clear event
  delete fDebug_graph[it][il][iv];
  fDebug_graph[it][il][iv] = NULL;
  delete fDebug_pm[it][il][iv];
  fDebug_pm[it][il][iv] = NULL;
  delete fDebug_func[it][il][iv];
  fDebug_func[it][il][iv] = NULL;
  delete fDebug_fminuit[it][il][iv];
  fDebug_fminuit[it][il][iv] = NULL;
  for (int ip = 0; ip < this->kMaxNparticle; ++ip)
    if (fDebug_true[it][il][iv][ip]) {
      delete fDebug_true[it][il][iv][ip];
      fDebug_true[it][il][iv][ip] = NULL;
    }

  fDebug_pos_cv[it]->cd(1 + il + this->kPosNlayer * iv);

  // sample #1 is used for Arm2 (no sample choice for Arm1)
  const Int_t pos_sample = fSiSample[it][il][iv];
  // same silicon detector is used for both towers in Arm2
  const Int_t pos_tower = this->kArmIndex == 0 ? it : 0;
  const Int_t ch_low = fChannelRange[it][iv][0];
  const Int_t ch_high = fChannelRange[it][iv][1];
  const Int_t n_ch = ch_high - ch_low + 1;

  //  Utils::Printf(Utils::kPrintDebugFull, "Using Sample [%d][%d][%d] = %d\n", it, il, iv, pos_sample);

  fDebug_graph[it][il][iv] = new TGraphErrors(n_ch);
  for (Int_t ip = 0; ip < n_ch; ++ip) {
    const Int_t ic = ch_low + ip;
    const Double_t xx = Double_t(ic);
    const Double_t yy = lvl2->fPosDet[pos_tower][il][iv][ic][pos_sample];
    const Double_t ee = lvl2->fErrPosDet[pos_tower][il][iv][ic][pos_sample];
    fDebug_graph[it][il][iv]->SetPoint(ip, xx, yy);
    // fDebug_graph[it][il][iv]->SetPointError(ip, 0., ee);
  }
  //  for (Int_t ip = 0; ip < fPeakN[it][il][iv]; ++ip) {
  //    Utils::Printf(Utils::kPrintDebugFull, "TSpectrum Peak [%d][%d][%d][%d] = %lf at %lf\n", it, il, iv, ip,
  //                  fPeakY[it][il][iv][ip], fPeakX[it][il][iv][ip]);
  //  }

  string twname = it == 0 ? "Small" : "Large";
  string viewname = iv == 0 ? "X" : "Y";
  fDebug_graph[it][il][iv]->SetName(Form("pos_graph_%d_%d_%d_%d", this->kArmIndex, it, il, iv));
  fDebug_graph[it][il][iv]->SetTitle(Form("Arm%d, %s tower, Layer %d%s;Channel;Signal [ADC]", this->kArmIndex + 1,
                                          twname.c_str(), il + 1, viewname.c_str()));
  fDebug_graph[it][il][iv]->SetMarkerStyle(kFullDotMedium);
  fDebug_graph[it][il][iv]->Draw("AP");

  fDebug_pm[it][il][iv] = new TPolyMarker(fPeakN[it][il][iv]);
  for (Int_t ip = 0; ip < fPeakN[it][il][iv]; ++ip)
    fDebug_pm[it][il][iv]->SetPoint(ip, fPeakX[it][il][iv][ip], fPeakY[it][il][iv][ip]);
  fDebug_pm[it][il][iv]->SetMarkerStyle(kFullStar);
  fDebug_pm[it][il][iv]->SetMarkerColor(kMagenta);
  fDebug_pm[it][il][iv]->SetMarkerSize(1.5);
  fDebug_pm[it][il][iv]->Draw();

  if (fDebug_tspectrum[it][il][iv]) {
    fDebug_tspectrum[it][il][iv]->SetLineColor(kAzure + 10);
    fDebug_tspectrum[it][il][iv]->SetLineWidth(2);
    fDebug_tspectrum[it][il][iv]->Draw("LSAME");
  }

  for (int ip = 0; ip < this->kMaxNparticle; ++ip) {
    if (fDebug_mh[it][il][iv][ip]) {
      fDebug_mh[it][il][iv][ip]->SetMarkerColor(2 + ip);
      fDebug_mh[it][il][iv][ip]->SetMarkerSize(2);
      fDebug_mh[it][il][iv][ip]->Draw("SAME");
    }
  }

  if (mc != NULL) DrawTrueInformation(mc, it, il, iv);

  /* Draw fit function */
  if (func != NULL && hfun != NULL && min_par != NULL) {
    if (func->GetNpar() > this->kPosNpars)
      func->SetLineColor(kMagenta);
    else
      func->SetLineColor(kGreen);
    func->SetRange(ch_low, ch_high);
    func->SetNpx(1000);
    //    Utils::Printf(Utils::kPrintDebugFull, "Fit Peak [%d][%d][%d] = %lf at %lf\n", it, il, iv,
    //    func->GetParameter(0),
    //                  func->GetParameter(1));
    fDebug_func[it][il][iv] = (TF1 *)func->Clone(Form("func_%d_%d_%d_%d", this->kArmIndex, it, il, iv));
    fDebug_func[it][il][iv]->Draw("same");

    fDebug_fminuit[it][il][iv] = (TF1 *)func->Clone(Form("fdebug_%d_%d_%d_%d", this->kArmIndex, it, il, iv));
    for (Int_t ipar = 0; ipar < hfun->GetNparam(); ++ipar)
      fDebug_fminuit[it][il][iv]->SetParameter(ipar, min_par[ipar]);
    if (hfun->GetNparam() > this->kPosNpars)
      fDebug_fminuit[it][il][iv]->SetLineColor(kViolet);
    else
      fDebug_fminuit[it][il][iv]->SetLineColor(kGreen + 2);
    fDebug_fminuit[it][il][iv]->SetLineStyle(kDashed);
    fDebug_fminuit[it][il][iv]->SetRange(ch_low, ch_high);
    fDebug_fminuit[it][il][iv]->SetNpx(1000);
    fDebug_fminuit[it][il][iv]->Draw("same");

    // printf("func pars:   "); fflush(stdout);
    // for (Int_t ipar = 0; ipar < hfun->GetNparam(); ++ipar) {
    //   printf(" %lf", fDebug_func[it][il][iv]->GetParameter(ipar)); fflush(stdout);
    // }
    // printf("\n");
    // printf("minuit pars: "); fflush(stdout);
    // for (Int_t ipar = 0; ipar < hfun->GetNparam(); ++ipar) {
    //   printf(" %lf", min_par[ipar]); fflush(stdout);
    // }
    // printf("\n");
  }

  // gPad->Modified();
  // gPad->Update();
  // fDebug_wait->cd();
  // fDebug_wait->WaitPrimitive();
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::DrawLongitudinalFit(Int_t it, Int_t nhit) {
  if (fDebug_lonfi[it]) {
    delete fDebug_lonfi[it];
    fDebug_lonfi[it] = NULL;
  }

  // string title = Form("Arm%d, %s tower; Depth [X_{0}]; #frac{dE}{#rho dx} [#frac{GeV}{g cm^{-2}}]", this->kArmIndex +
  // 1, it == 0 ? "Small" : "Large");
  string title = Form("Arm%d, %s tower; Depth [X_{0}]; dE [MIP]", this->kArmIndex + 1, it == 0 ? "Small" : "Large");
  fDebug_lonfi[it] = new TMultiGraph();
  fDebug_lonfi[it]->SetTitle(title.c_str());
  fDebug_lonfi[it]->Add(fDebug_calx0[it]);
  for (Int_t ih = 0; ih < nhit; ++ih) fDebug_lonfi[it]->Add(fDebug_posx0[it][ih]);

  fDebug_cal_cv[it]->cd();
  gPad->Modified();
  gPad->Update();
  fDebug_lonfi[it]->Draw("AP");
  gPad->Modified();
  gPad->Update();
  fDebug_lonfi[it]->GetXaxis()->SetLimits(0, 44.);
  gPad->Modified();
  gPad->Update();
  if (fDebug_lonprotot[it]) fDebug_lonprotot[it]->Draw("LSAME");
  gPad->Modified();
  gPad->Update();
  for (Int_t ih = 0; ih < nhit; ++ih)
    if (fDebug_lonpropar[it][ih]) fDebug_lonpropar[it][ih]->Draw("LSAME");
  gPad->Modified();
  gPad->Update();
  for (Int_t ih = 0; ih < nhit; ++ih)
    if (fDebug_lonproint[it][ih]) fDebug_lonproint[it][ih]->Draw("PSAME");
  gPad->Modified();
  gPad->Update();
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::DrawCalorimeter(Level2<armclass> *lvl2, Int_t it) {
  delete fDebug_hcal[it];

  fDebug_cal_cv[it]->cd();

  string twname = it == 0 ? "Small" : "Large";
  fDebug_hcal[it] = new TH1D(Form("hcal_%d_%d", this->kArmIndex, it),
                             Form("Arm%d, %s tower;Layer;dE [GeV]", this->kArmIndex + 1, twname.c_str()),
                             this->kCalNlayer, -0.5, (Double_t)this->kCalNlayer - 0.5);
  for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) fDebug_hcal[it]->SetBinContent(il + 1, lvl2->fCalorimeter[it][il]);

  fDebug_hcal[it]->Draw("HIST");

  gPad->Modified();
  gPad->Update();
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::DrawCalX0(Level2<armclass> *lvl2, Int_t it) {
  if (fDebug_calx0[it]) {
    delete fDebug_calx0[it];
    fDebug_calx0[it] = NULL;
  }

  fDebug_cal_cv[it]->cd();
  fDebug_calx0[it] = new TGraphAsymmErrors(0);
  fDebug_calx0[it]->SetTitle(
      Form("Arm%d, %s tower; Depth [X_{0}]; dE [GeV]", this->kArmIndex + 1, it == 0 ? "Small" : "Large"));
  fDebug_calx0[it]->SetMarkerSize(1.5);
  fDebug_calx0[it]->SetMarkerStyle(kFullDoubleDiamond);
  fDebug_calx0[it]->SetMarkerColor(kBlack);
  fDebug_calx0[it]->SetLineColor(kBlack);
  for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) {
    Double_t depth = il < this->kDoubleStepLayer ? 2. : 4.;
    fDebug_calx0[it]->SetPoint(il, GetLayerDepth(il) - 0.5 * depth, lvl2->fCalorimeter[it][il]);
    fDebug_calx0[it]->SetPointError(il, 0.5 * depth, 0.5 * depth, 0., 0.);
  }
  fDebug_calx0[it]->Draw("AP");

  // gPad->Modified();
  // gPad->Update();
  fDebug_calx0[it]->SetMinimum(-0.000000000000001);
  fDebug_calx0[it]->GetXaxis()->SetLimits(0., 42.);
  // gPad->Modified();
  // gPad->Update();
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::DrawPosX0(Level3<armrec> *lvl3, Int_t it) {
  fDebug_cal_cv[it]->cd();

  for (Int_t ip = 0; ip < this->kMaxNparticle; ++ip) {
    if (fDebug_posx0[it][ip]) {
      delete fDebug_posx0[it][ip];
      fDebug_posx0[it][ip] = NULL;
    }

    // Look for the first and last silicon layer with a peak associated to the particle
    Double_t infDepth = -1;
    for (Int_t il = 0; il < this->kPosNlayer; ++il)
      for (Int_t iv = this->kPosNview - 1; iv >= 0; --iv)
        if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0.) {
          if (infDepth < 0.) {
            infDepth = this->kPosLayerX0[it][il][iv];
            break;
          }
        }
    Double_t supDepth = -1;
    for (Int_t il = this->kPosNlayer - 1; il >= 0; --il)
      for (Int_t iv = 0; iv < this->kPosNview; ++iv)
        if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0.) {
          if (supDepth < 0.) {
            supDepth = this->kPosLayerX0[it][il][iv];
            break;
          }
        }
    if (infDepth < 0 || supDepth < 0.) continue;

    if (fDebug_posx0[it][ip] == NULL) {
      fDebug_posx0[it][ip] = new TGraphAsymmErrors(0);
      fDebug_posx0[it][ip]->SetMarkerStyle(lvl3->fMultiParticleSiID[it][ip] == 1 ? kFullCircle : kFullSquare);
      fDebug_posx0[it][ip]->SetMarkerColor(2 + ip);
      fDebug_posx0[it][ip]->SetMarkerSize(1.25);
    }

    fDebug_posx0[it][ip]->SetPoint(fDebug_posx0[it][ip]->GetN(), 0., 0.);  // Fill zero value for L = 0 X0
    for (Int_t il = 0; il < this->kPosNlayer; ++il) {
      //      Bool_t isDoubleLayer =
      //          (il < 2 && lvl3->fMultiParticleSiDep[it][ip][il][0] > 0 && lvl3->fMultiParticleSiDep[it][ip][il][1] >
      //          0)
      //              ? true
      //              : false;
      for (Int_t iv = this->kPosNview - 1; iv >= 0; --iv) {
        if (lvl3->fMultiParticleSiDep[it][ip][il][iv] > 0.) {  // Fill longitudinal profile with amplitude values
          //          if (isDoubleLayer) {  // For the first two layers, combine x and y amplitude using harmonic mean
          //            Double_t harmean = 2. * lvl3->fMultiParticleSiDep[it][ip][il][0] *
          //            lvl3->fMultiParticleSiDep[it][ip][il][1]; harmean /= lvl3->fMultiParticleSiDep[it][ip][il][0] +
          //            lvl3->fMultiParticleSiDep[it][ip][il][1];
          //            fDebug_posx0[it][ip]->SetPoint(fDebug_posx0[it][ip]->GetN(), this->kPosLayerX0[it][il][iv], 10.
          //            * harmean); continue;
          //          } else  // For the other layers simply fill the single layer amplitude
          fDebug_posx0[it][ip]->SetPoint(fDebug_posx0[it][ip]->GetN(), this->kPosLayerX0[it][il][iv],
                                         10. * lvl3->fMultiParticleSiDep[it][ip][il][iv]);
        } else {  // Fill longitudinal profile with zero values
          if (this->kPosLayerX0[it][il][iv] < infDepth || this->kPosLayerX0[it][il][iv] > supDepth)
            fDebug_posx0[it][ip]->SetPoint(fDebug_posx0[it][ip]->GetN(), this->kPosLayerX0[it][il][iv], 0.);
        }
      }
    }
    fDebug_posx0[it][ip]->SetPoint(
        fDebug_posx0[it][ip]->GetN(), 44.,
        fDebug_posx0[it][ip]->GetY()[fDebug_posx0[it][ip]->GetN() - 1]);  // Fill last value for L = 44 X0

    if (fDebug_posx0[it][ip]) fDebug_posx0[it][ip]->Draw("PSAME");
  }

  // gPad->Modified();
  // gPad->Update();
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::Print(Int_t ev, Level2<armclass> *lvl2, Level3<armrec> *lvl3) {
  Utils::Printf(Utils::kPrintInfo, "\n\n--- Arm%d event %d ---\n", this->kArmIndex + 1, ev);
  // lvl2->Print("photon");
  lvl3->Print("photon");

  // for (Int_t it = 0; it < lvl2->kCalNtower; ++it)
  //   for (Int_t il = 0; il < 2; ++il)
  //     for (Int_t iv = 0; iv < this->kPosNview; ++iv) {
  // 	DrawPositionFit(NULL, lvl2, it, il, iv, NULL, (PosFitBaseFCN *)NULL, NULL);
  //     }
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PrintLevel2(Level2<armclass> *lvl2) {
  Utils::Printf(Utils::kPrintInfo, "\tCALORIMETER\n");
  for (Int_t it = 0; it < lvl2->kCalNtower; ++it) {
    Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
    Utils::Printf(Utils::kPrintInfo, "\t\tcal =");
    for (Int_t il = 0; il < lvl2->kCalNlayer; ++il) {
      Utils::Printf(Utils::kPrintInfo, " %.1lf", lvl2->fCalorimeter[it][il]);
    }
    Utils::Printf(Utils::kPrintInfo, "\n");
  }
  Utils::Printf(Utils::kPrintInfo, "\n");

  // Utils::Printf(Utils::kPrintInfo, "\tSILICON\n");
  // for (Int_t it = 0; it < lvl2->kPosNtower; ++it) {
  //   Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
  //   for (int il = 0; il < lvl2->kPosNlayer; ++il) {
  //     Utils::Printf(Utils::kPrintInfo, "\t\t%d", il + 1);
  //     for (Int_t iv = 0; iv < lvl2->kPosNview; ++iv) {
  // 	if (iv == 0)
  // 	  Utils::Printf(Utils::kPrintInfo, "X =");
  // 	else
  // 	  Utils::Printf(Utils::kPrintInfo, "Y =");
  // 	for (Int_t ic = 0; ic < lvl2->kPosNchannel[it]; ++ic) {
  // 	  const Int_t is = 1;
  // 	  Utils::Printf(Utils::kPrintInfo, " %.1lf", lvl2->fPosDet[it][il][iv][ic][is]);
  // 	}
  // 	Utils::Printf(Utils::kPrintInfo, "\n");
  //     }
  //   }
  // }
  // Utils::Printf(Utils::kPrintInfo, "\n");
}

template <typename armclass, typename armcal, typename armrec>
void EventRec<armclass, armcal, armrec>::PrintMc(McEvent *mc) {
  Utils::Printf(Utils::kPrintInfo, "\tTRUE INFORMATION\n");

  for (Int_t it = 0; it < this->kCalNtower; ++it) {
    const Int_t ntruehit = mc->CheckParticleInTower(this->kArmIndex, it, 1., 0.);
    fDebug_isTrueMultihit[it] = (ntruehit >= 2) ? true : false;

    if (ntruehit == 0) continue;

    for (Int_t ip = 0; ip < ntruehit; ++ip) {
      double TrueColPos[2] = {-1000., -1000.};
      double TrueCalPos[2] = {-1000., -1000.};
      // TODO [TrackProjection]: Check this change
      TVector3 lhc_pos = mc->fReference[ip]->Position();
      //
      TVector3 col_pos = CT::GetTrueCollisionCoordinates(this->kArmIndex, lhc_pos);
      TrueColPos[0] = col_pos.X();
      TrueColPos[1] = col_pos.Y();
      TVector3 cal_pos = CT::GetTrueCalorimeterCoordinates(this->kArmIndex, it, lhc_pos);
      TrueCalPos[0] = cal_pos.X();
      TrueCalPos[1] = cal_pos.Y();
      //
      Utils::Printf(Utils::kPrintInfo, "\ttower %d\n", it);
      Utils::Printf(Utils::kPrintInfo, "\t\tPdg    = %.1d\n", mc->fReference[ip]->PdgCode());
      Utils::Printf(Utils::kPrintInfo, "\t\tE      = %.1lf\n", mc->fReference[ip]->Energy());
      Utils::Printf(Utils::kPrintInfo, "\t\t(x,y)  = [ LHC ] %.1lf, %.1lf\n", TrueColPos[0], TrueColPos[1]);
      Utils::Printf(Utils::kPrintInfo, "\t\t(x,y)  = [TOWER] %.1lf, %.1lf\n", TrueCalPos[0], TrueCalPos[1]);
      Utils::Printf(Utils::kPrintInfo, "\t\t(x,y)  = [STRIP] %.1lf, %.1lf\n",  //
                    PositionToChannel(it, 0, TrueCalPos[0]), PositionToChannel(it, 1, TrueCalPos[1]));
    }  // particle loop
  }    // tower loop
}

/* Explicitly instantiate all needed templates (to avoid problems when linking) */
namespace nLHCf {
template class EventRec<Arm1Params, Arm1CalPars, Arm1RecPars>;

template class EventRec<Arm2Params, Arm2CalPars, Arm2RecPars>;
}  // namespace nLHCf
