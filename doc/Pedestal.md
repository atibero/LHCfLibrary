
# PedestalComputation

This folder contains three executable to generate anche check pedestal tables needed for simulation smearing.

## Usage of CreatePedestalTree 

CreatePedestalTree generate an intermediate ROOT file containing histo and tree, later used to extract pedestal tables

```
./bin/CreatePedestalTree -i <input file> -o <output file>  
```

Options)
```
	-i <input>	Input ROOT file
	-o <output>	Output ROOT file (default = "calibrated.root")
	-f <event>	First event to be processed (default = 0)
	-l <event>	Last event to be processed (default = all)
	--disable-arm1	Disable Arm1 calibration (default = enabled)
	--disable-arm2	Disable Arm2 calibration (default = enabled)
	--realign-arm2	Realign silicon to calorimeter events (default = disabled)
	--histogram	Save pedestal histograms for each channel (default = disabled)
	--average-pedestal	Pedestal tree without delayed gate subtraction (default = disabled)
	-v <verbose>	Set verbose level (default = 1)
				0 -> only errors
				1 -> some info
				2 -> debug
				3 -> all debug
	-h		Show usage
```

Notes)
```
Depending on how pedestal are subtracted in data you may or may not specify --average-pedestal:
- if NO, pedestal tree for simulations smearing contains signal - delayed_gate in pedestal events
- if YES, pedestal tree for simulations smearing contains signal in pedestal events
```

Example)
```
./bin/CreatePedestalTree -i runXXXXX.root -o ped_runXXXXX.root 
```

## Usage of ExtractPedTableAndFile

ExtractPedTableAndFile use the previosly generated ROOT file to extract .dat table and .root file for simulation smearing

```
./bin/ExtractPedTableAndFile -i <input folder> -o <output folder> -s <first run> -e <last run>  
```

Options)
```
-i			input folder
-o			output folder
-s			inferior run
-e			superior run
-a			is arm2 instead of arm1
-c			consider only calorimeter
-p			consider only posdetector
-f			estimate calorimeter shift using fit
-t			save all relevant and non-relevant tables
-d			define calorimeter as "ped" instead of "ped-del" for .dat
-b			subtract the average for calorimeter and posdetector pedestals  for .root
-z			generate posdetector root file considering tower sum as well as single channel
-h			help
```

Notes)
```
Option -d affects how pedestal is considering for .dat table:
- if NO, pedestal tree for simulations smearing contains signal - delayed_gate in pedestal events
- is YES, pedestal tree for simulations smearing contains signal in pedestal events

Note that this option does not directly affects .root file (which completely depends on --average-pedestal in CreatePedestalTree).

If --average-pedestal in CreatePedestalTree was specified, you can here think to specify or not -b options depending on:
- if you are interested in pure pedestal (DAQ level pedestals), do not specify -b (this is the case of LHC2022)
- if you are interested in pedestal-average (pedestal fluctuations only), specify -b (this may be necessary for SPS calibration)
```

Output)
```
The output of the software is contains:
- %s_cal_ped_%d_%d.root containing event-by-event calorimeter pedestals
- %s_cal_ped_v1/2_%d_%d.dat containing calorimeter pedestal mean and rms
- %s_cal_del_shift_v1/2_%d_%d.dat containing calorimeter pedestal shift mean and rms
- %s_pos_ped_%d_%d.dat containing silicon pedestal mean and rms
- %s_pos_ped_%d_%d.root containing event-by-event silicon pedestals

Legend:
v1 means that pedestal is defined event by event as "signal-delayed_gate"
v2 means that pedestal is defined event by event as "signal" (default for silicon)

Important note:
Pedestal shift is defined as signal-delayed_gate difference between pedestal and no-shower events.
This is necessary to correct this shift for Arm2 in principle on an run-by-run basis (here assumed run-independent)
```

Example)
```
./bin/ExtractPedTableAndFile -i infolder -o outfolder -s XXXXX -e YYYYY 
```

## Usage of CheckPedHistoAndTree

CheckPedHistoAndTree can be used to check the .root and .dat file previously generated but it is not mandatory.

```
./bin/CheckPedHistoAndTree -i <input .root file> -d <input .dat file> -t <tree name> -o <output file>  
```

Options)
```
-x			input text file name
-i			input root file name
-t			input root tree name
-o			output file name if you want to save histograms
-s			check variable in silicon instead of in calorimeter
-a			check variable for arm2 instead of for arm1
-d			define calorimeter pedestal is "ped" instead of "ped-del"
-h			help
```

Notes)
```
Option -d affects how pedestal is considering for .dat table:
- if NO, pedestal tree for simulations smearing contains signal - delayed_gate in pedestal events
- is YES, pedestal tree for simulations smearing contains signal in pedestal events
```

Example)
```
./bin/CheckPedHistoAndTree -i a2_cal_ped_80246_80246.root -d a2_cal_ped_v2_80246_80246.dat -t ped_tree -o output.root
```
