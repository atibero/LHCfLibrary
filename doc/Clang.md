
# Clang format

To avoid confusion originated by different syntax formatting among git commits, it is important to configure clang-format before commiting to git

## Installation

At first you need to install clang-format (clang-format version 11 is OK, other versions were not tested).

For example, on Debian GNU/Linux 11 (bullseye) these packages are enough:
- clang-format/stable,now 1:11.0-51+nmu5 amd64
- clang-format-11/stable,now 1:11.0.1-2 amd64
- libclang-cpp11/stable,now 1:11.0.1-2 amd64
- libclang-cpp9/stable,now 1:9.0.1-16.1 amd64

## Configuration

clang-format configuration requires the following three files:

- .clang-format: This file is automatically downloaded in the main directory and you don't need to touch it (actually, you don't have to touch it)
- pre-commit and apply-format: These files are in the Clang folder and you need to perform some actions whenever you download LHCfLibrary to a new path

From the main directory:

```
cp Clang/pre-commit Clang/apply-format .git/hooks/
cd .git/hooks/
chmod +x pre-commit apply-format
```

## Usage

No action is required until you use try to commit something (or if you follow the default format).

At this point, if you did not follow the default format, the pre-processing unit ask you something like:
```
The staged content is not formatted correctly.
The fix shown above can be applied automatically to the commit.

You can:
 [a]: Apply the fix
 [f]: Force and commit anyway (not recommended!)
 [c]: Cancel the commit
 [?]: Show help

What would you like to do? [a/f/c/?]
```

Please select and go on with "a" and go on with your commit

