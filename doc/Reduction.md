# Reduction 

## Usage 
```
Usage: ./bin/Reduction [options]

Available options:
	-i <input>		Input directory (mandatory)
	-o <output>		Output ROOT file (default = "reduction.root")
	-f <first>		First run to be analysed (default = -1)
	-l <last>		Last run to be analysed (default = -1)
				*** If either <first>=-1 or <last>=-1, all ROOT  ***
				*** files from input directory are loaded.       ***
				*** Otherwise, <input> must contains files named ***
				*** "rec_runXXXXX.root" (XXXXX=<first>...<last>) ***
	-F <fill>		Automatically set parameters for specified fill (mandatory)
	-S <subfill>		Automatically set parameters for specified subfill (mandatory/optional)
				*** <fill> and <subfill> are mandatory arguments ***
				*** They are ignored if operation is = SPS2015,2021,2022 ***
				*** Otherwise you must specify both of them accordingly ***
				*** Or set <fill>=0 to select default dummy configuration ***
	-n <number>		Limitation of #event (default = -1)
	-T <filter>		Selection by Trigger Flag (default = 0xFFFF(no selection))
	--disable-arm1		Disable Arm1 analysis (default = enabled)
	--disable-arm2		Disable Arm2 analysis (default = enabled)
	--disable-photon	Disable photon analysis (default = enabled)
	--disable-neutron	Disable neutron analysis (default = enabled)
	--disable-pion		Disable pion analysis (default = enabled)
	--enable-true		Enable MC truth (default = disabled)
	--enable-lvl2		Enable lvl2 (default = disabled)
	--enable-lvl2pos		Enable lvl2 for position detector information (default = disabled)
	--disable-modification		Energy Rescaling (default = enabled)
	--disable-bptx-cut		Disable BPTX cut, must be done for MC!!! (default = enabled)
	-E <E_scale_factor>	Scale factor for photon energy (default = 1)
	-X <beam_centre_offset_x>	Shift beam centre along x (default = 0)
	-Y <beam_centre_offset_x>	Shift beam centre along y (default = 0)
	-P <pid_efficiency>	Set Pi0 PID selection efficiency (default = 90, available: 85, 90, 95)
	-c <text>		comments are save into output file.
	-v <verbose>		Set verbose level (default = 1)
					0 -> only errors
					1 -> some info
					2 -> debug
					3 -> all debug
	-h			Show usage

Bitmap for Trigger:
	Shower Trigger		0x001
	Pi0 Trigger		0x002
	HighEM Trigger		0x004
	Hadron Trigger		0x008
	ZDC Trigger		0x010
	FC Trigger		0x020
	L1T Trigger		0x040
	Laser Trigger		0x080
	Pedestal Trigger	0x100
```

example:  For only Arm2 reduction, 1000 events, with filtering for Pi0 trigger.
```
./bin/Reduction -i ../../data/rec_run80262.root -o test.root -F 3855 -S 1 --disable-arm1 --enable-lvl2 -n 1000 -T 0x2 
```

Note:
- Fill and sub fill numbers must be speficied. The definition of subfill numbers should be found in ?? where ??.  
For MC, both the numbers should be negative like -F -3855 -S -1 
- For MC, add the flags: --disable-bptx-cut and --enable-true


## Data format of the output Tree 

| name | size | description |
| ---- | ---- | ----- |
| arm | int | detector ID 1=Arm1, 2=Arm2|
| run | int | run number |
| gnumber | int | detector-common event id |
| number | int | event-id for the detector|
| time | uint | CPU time in sec |
| time_usec| uint | time in micro-sec|
| tirgger |uint| trigger flag (bitmap)|
|beam|uint| beam presence bitmap (0x1=beam1, 0x2=beam2)|
|bcid|uint| bunch ID|
|quality_flag|unit| data quality flag (not available yet, always=1)|
|atlas_trigger_accept|bool| ATLAS L1A (trigger accept)|
|atlas_ecr|uint|Count of ATLAS ECR in LHCf DAQ|
|atlas_l1id|uint|ATLAS Level1 ID|
|-|||
|photon_nhit|1D v.(uint,[tower=2])|photon number of hits in the tower (for future extension, currently always =1)|
|photon_flag|2D v.(uint,[nhit][tower=2])|photon analysis flag (see below)|
|photon_energy|2D v.(double,[nhit][tower=2])|photon reconstructed energy [GeV]|
|photon_pid|2D v.(bool,[nhit][tower=2])|photon PID result (true=photon)|
|photon_poscal|3D v.(double,[nhit][tower=2],[view=2])|photon position on the calorimeter coordinate [mm].|
|photon_pos|3D v.(double,[nhit][tower=2],[view=2])|photon position on beam center coordinate [mm].|
|photon_l20|1D v.(double,[tower=2])|photon PID parameter (L_20%) [r.l.]|
|photon_l90|1D v.(double,[tower=2])|photon PID parameter (L_90%) [r.l.]|
|-|||
|neutron_flag|1D v.(uint,[tower=2])|neutron analysis flag (see below)|
|neutron_energy|1D v.(double,[tower=2])|neutron reconstructed energy [GeV]|
|neutron_pid|1D v.(bool,[tower=2])|neutron PID result (true=neutron)|
|neutron_poscal|2D v.(double,[tower=2],[view=2])|neutron position on the calorimeter coordinate [mm].|
|neutron_pos|2D v.(double,[tower=2],[view=2])|neutron position on beam center coordinate [mm].|
|neutron_l20|1D v.(double,[tower=2])|neutron PID parameter (L_20%) [r.l.]|
|neutron_l90|1D v.(double,[tower=2])|neutron PID parameter (L_90%) [r.l.]|
|-|||
|pi0_flag|1D v.(uint,[type=3])|pi0 analysis flag|
|pi0_type|1D v.(uint,[type=3])|Pi0 type 0:Type1, 1:Type2-TS, 2:Type2-TL|
|pi0_energy|1D v.(double,[type=3])|pi0 reconstructed energy [GeV]|
|pi0_mass|1D v.(double,[type=3])|pi0 reconstructed mass [MeV]|
|pi0_momentum|3D v.(double,[type=3],[xyz=3])|pi0 reconstructed momentum vector [GeV/c]|
|pi0_photon_tower|2D v.(int,[type=3][photon=2])|pi0 each photon hit tower|
|pi0_photon_energy|2D v.(double,[type=3][photon=2])|pi0 each photon energy [GeV]|
|pi0_photon_pos|3D v.(double,[type=3][photon=2][view=2])|pi0 each photon pos. in calorimeter coordinate [mm]|
|pi0_photon_r|1D v.(double,[type=3])|distance between hit pos. of photons. [mm]|
|-|||
|dE|2D v.(double,[tower=2],[layer=16])|energy deposit in each scintillator plate [GeV]|
|Fc|1D v.(int,[channel=4])|energy deposit in Fc (ADC counts)|
|Zdc|1D v.(double,[channel=3])|energy deposit in Zdc (ADC counts)|
|-|||
|mc_nhit|1D v.(int [tower+1=3])|number of this in MC ture (0=TS, 1=TL, 2=others)|
|mc_pdgcode|2D v.(int [tower+1=3][nhit])|PDG code for each hit|
|mc_usercode|2D v.(int [tower+1=3][nhit])|user code in E2E for each hit|
|mc_statuscode|2D v.(int [tower+1=3][nhit])|not used in this moment|
|mc_parent|3D v.(int [tower+1=3][2][nhit])|ID of parent decoded from usercode (only for >Op2022 MC)|
|mc_energy|2D v.(double [tower+1=3][nhit])|Energy [GeV]|
|mc_energy|3D v.(double [tower+1=3][x,y,z=3][nhit])|Momentum [GeV/c]|
|mc_pos|3D v.(double [tower+1=3][view=2][nhit])|Position with the collision coordinate [mm]|
|mc_poscal|3D v.(double [tower+1=3][view=2][nhit])|Position with the calorimeter coordinate [mm]|
|pi0_true_flag|||
|pi0_true_type|||

#### bitmap for trigger
```
	Shower Trigger		0x001
	Pi0 Trigger		0x002
	HighEM Trigger		0x004
	Hadron Trigger		0x008
	ZDC Trigger		0x010
	FC Trigger		0x020
	L1T Trigger		0x040
	Laser Trigger		0x080
	Pedestal Trigger	0x100
```

#### bitmap for photon/neutron analysis flag
- 0x001 overall selection
- 0x010 colliding bunch slection   
- 0x020 software trigger
- 0x040 PID selection
- 0x080 energy cut
- 0x100 hit position selection (2mm edge cut)
- 0x200 single hit selection (multi-hit cut) (not used for neutron analysis)

For a simple analysis, require only the first bit "ON" like ```if (photon_trigger[0] & 0x1)```

```c
      fPhotonFlag[i]   = 0;
      if (cut->BPTXCut(lvl3)) fPhotonFlag[i] += (1 << 4); // selection of colliding bunch events
      if (lvl3->fPhotonTrigger[i]) fPhotonFlag[i] += (1 << 5);   // software trigger
      if (cut->PhotonPIDCut(lvl3, i)) fPhotonFlag[i] += (1 << 6);    // PID selection
      if (cut->PhotonEnergyCut(lvl3, i)) fPhotonFlag[i] += (1 << 7); // energy cut
      if (cut->PhotonPositionCut(lvl3, i)) fPhotonFlag[i] += (1 << 8); // Position cut (2mm edge cut)
      if (cut->PhotonMultiHitCut(lvl3, i)) fPhotonFlag[i] += (1 << 9); // Multi-hit cut
      if (cut->CutPhotonEventTower(lvl3, i)) fPhotonFlag[i] += 1;  // overall selection
```

#### bitmap for pi0 analysis 
- 0x001 overall selection (except the mass selection)
- 0x002 mass selection  
- others are not implemented yet. 

For energy spectrum analyses, require 1st and 2nd bit "ON" like ```if (photon_trigger[0] & 0x3)```  
For check of mass distribution, require only 1st bit "ON" like ```if (photon_trigger[0] & 0x1)```  

## Log 
- 29 Aug. 2024: Introduced the MC ture branches
