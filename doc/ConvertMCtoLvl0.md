# ConvertMCtoLvl0

## Usage
```
./bin/ConvertMCtoLvl0 [options]
```

Options)
```
-i <input>		Input directory
-n <filename		Format of input filename (default= end2end.073896.out)
-f <first>		First run to be analysed (default = 1)
-l <last>		Last run to be analysed (default = 1)
-o <output>		Output ROOT file (default = "converted.root")
-F <fill>		Automatically set parameters for specified fill (default = 3855)
--arm1			Arm1 input file (either "--arm1" or "--arm2" required)
--arm2			Arm2 input file (either "--arm1" or "--arm2" required)
--disable-smearing	Disable smearing (default = enabled)
-t <tables_dir>		Set tables directory (default = "../Tables")
-m <multihit-list>		List used to combine n-events in one event (default = disabled)
-v <verbose>		Set verbose level (default = 1)
				0 -> only errors
				1 -> some info
				2 -> debug
				3 -> all debug
-h			Show usage
```

Example)
```
./bin/ConvertMCtoLvl0 --arm1 -o mc.root  -i ../../data -f 10001 -l 10001 -n qgs_13tevend2end.arm1.%06d.out -F 3855 -t ../Tables/
```

## Main function and classes

- Tools/MCtoLevel0/main.cpp
- nLHCf::MCtoLevel0

### Core functions

- nLHCf::MCtoLevel0::Convert
  - Open the E2E text file and read the file (loop)
  - Conversion to Level2 -> Level1 -> Level 0
  - Call the following functions
    - nLHCf::MCtoLevel0::ReadEvent
    Read one event from the file and fill to Level2
    - nLHCf::MCtoLevel0::ConvertToLevel1
    Conversion to Level1
    - nLHCf::MCtoLevel0::ConvertToLevel0
    Conversion to Level2
  - Add pedestal value (how ?)
  - Event selection by using non-zero dE in calorimeters
    - If zero dE, lvl2 is not filled to TTree.

- nLHCf::MCtoLevel0::ConvertSumArtificial
  - Similar as Convert but considering event pileup


