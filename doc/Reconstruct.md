# Reconstruct 

## Usage
```
Usage: ./bin/Reconstruct [options]

Available options:
-i <input>			Input ROOT file (mandatory)
-o <output>			Output ROOT file (default = "reconstructed.root")
-f <event>			First event to be reconstructed (default = 0)
-l <event>			Last event to be reconstructed (default = all)
-p <mode>			Position reconstruction mode: "fast" or "full" (default = "full")
-t <tables_dir>			Set tables directory (default = "../Tables")
-F <fill>		Automatically set parameters for specified fill (mandatory)
-S <subfill>		Automatically set parameters for specified subfill (mandatory/optional)
*** <fill> (and <subfill>) are mandatory arguments ***
*** They are ignored if operation is = SPS2015,2021,2022 ***
*** Otherwise you must specify both of them accordingly ***
*** Or set <fill>=0 to select default dummy configuration ***
--disable-arm1			Disable Arm1 reconstruction (default = enabled)
--disable-arm2			Disable Arm2 reconstruction (default = enabled)
--disable-photon		Disable photon reconstruction (default = enabled)
--disable-neutron		Disable neutron reconstruction (default = enabled)
--disable-fastpeaksearch	Disable peak search in fast position reconstruction (default = enabled)
--photon-fit-type		Photon position fit mode (default = 3 for Arm1, 0 for Arm2)
--neutron-fit-type		Neutron position fit mode (default = 3 for Arm1, 0 for Arm2)
--photon-erec-type		Photon position fit mode (default = 2 for Arm1, 1 for Arm2)
--level0			Save Level0 to output file (default = disabled)
--level1			Save Level1 to output file (default = disabled)
--level2			Save Level2 to output file (default = disabled)
--level0cal			Save Level0 (no silicon/GSO bars) to output file (default = disabled)
--level1cal			Save Level1 (no silicon/GSO bars) to output file (default = disabled)
--level2cal			Save Level2 (no silicon/GSO bars) to output file (default = disabled)
--true-coor			Use true instead of reconstructed coordinates
--development			Compute development variables
-X <fill>			Specify non zero MC Beam X offset (default = 0)
-Y <fill>			Specify non zero MC Beam Y offset (default = 0)
-O <old_file>			Compare with old tree in <old_file> (default = off)
-v <verbose>			Set verbose level (default = 1)
0 -> only errors
1 -> some info
2 -> debug
3 -> all debug
-h				Show usage
```
### Options
- ``-F`` Fill and ``-S`` Sub-fill numbers
  - This specify the data set by the LHC Fill number and the subset number, which is required to tune the dataset-dependent parameters like the beam center position.
  - The sub-set number is defined using the range of LHCf run numbers. Please refer the data set pages ([Op2015](./Op2015.md), [Op2022](./Op2022.md)).
  - In case of SPS beam tests, Sub-fill number should be always 1 for the moment.
  - (These values can be specified by negative value; 8178 = -8178 )
- ``-p`` position reconstruction mode
  - ``full`` Use a full position reconstruction based on TSpectrum (peak search) and fitting (default)
  - ``fast`` Use a modified barycenter method (No multi-hit detection)
  - ``none`` No position reconstruction is performed and copy the Level3 result already performed. The lvl3 must be in the same LHCfEvents, which should be the output of Reconstruction with `--level2` or `--level2cal` option. 
- ``--photon-fit-type`` Selection of position fitting function in the photon reconstruction. All the functions are based on the triple or double Lorenz function, but use different parameterization. See [PosFitFCN.hh](../Dictionary/include/PosFitFCN.hh)
  - ``0`` Original function used in the old library (default, but not recommended)
  - ``1`` Triple Lorenz function with parameterization focused on 'area' (p1, p3, p5 are area of peaks (/pi))  
  - ``2`` Triple Lorenz function with parameterization focused on 'height' (p1, p3, p5 are height of peaks)
- ``--neutron-fit-type`` Selection of position fitting function in the neutron reconstruction. same as 'photon-fit-type'
- ``--photon-erec-type`` Selection of energy reconstruction method for MH photon events 
  - ``1`` Original method. Use area of 1st and 2nd layers for energy share parameter  
  - ``2`` Old algorithm used in Arm1 Op2015 preliminary analysis. Use height of only 2nd layer for energy share parameter 

## Main functions and classes
- EventRec::PhotonMultiHitFit (Position fitting for multi-hits in each layer)
- EventRec::EnergyReconstruction (Energy reconstruction)
- 
## Tables 
- **a\*_pos_dead_channels.dat**   Dead channel list of Position sensitive layers
