<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/3.2.2/es5/tex-mml-chtml.min.js">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
 tex2jax: {
 inlineMath: [['$', '$'] ],
 displayMath: [ ['$$','$$'], ["\\[","\\]"] ]
 }
 });
</script>


# Calibrate

This application performs pedestal subtraction and gain calibration as well as convert the format from MIDAS to Level2 (with intermediate step of Level0 and Level1 ).

## Processes 
- Pedestal computation 
  - Pedestal values for all scintillator layers and position sensitive layers are automatically calculated before the main process of event loop.
  - Mainly two pedestal computation type are available 
    - Event-by-event pedestal value : In each event, ADC values of main ADCs obtained with roughly 10 us delayed gate are available together with the values with the normal gate. These values are used as the pedestal values for the event. However, sometimes small offsets are observed between the normal values and delayed gate values in pedestal-triggered events. So in this process, the offset values $<ADC_{normal} - ADC_{delayed}>_{pede. trg}$
    - Average pedestal values : Simply the average of normal ADC values for each channel in the pedestal triggered events $<ADC_{normal}>_{pede. trg}$ is computed in the process. 
  - Skip this processes in case of MC data (Data or MC is automatically identified using input file format (MIDAS-> Data, LHCfEvents-> MC))
- Format conversion from MIDAS to Level0
  - The input file must be MIDAS "ROOT" output, called conv files. The MIDAS text format files, which are used before LHC 2016 as DAQ output format, must be converted to the MIDAS "ROOT" format using MIDAS Analyzer.
  - The values are re-arranged from the device-originated output order to the physical order (like layer number.) 
  - Skip it in case of MC data because the input data is already with Level0 format. 
- Pedestal subtraction 
  - Event-by-event mode (default): $ADC_{normal} - ADC_{delayed} - <ADC_{normal} - ADC_{delayed}>_{pede. trg}$
  - Average-mode: $ADC_{normal} - <ADC_{normal}>_{pede. trg}$. Better performance in case of noisy data like Op2022.
- Format conversion from Level0 (raw data) to Level1 (pedestal subtracted data) 
- Format conversion from Level1 to Level2 (gain calibrated data)
- Gain calibration
- Cross-talk correction of GSO bar hodoscopes (only in Arm1)

## Usage 

```
./bin/Calibrate -i <input file> -o <output file> -F <Fill_number> -S <subfill> -t <path to Tables/>  
```

```
	-i <input>	Input ROOT file (mandatory)
	-o <output>	Output ROOT file (default = "calibrated.root")
	-f <event>	First event to be processed (default = 0)
	-l <event>	Last event to be processed (default = all)
	-t <tables_dir>	Set tables directory (default = "../Tables")
	-F <fill>		Automatically set parameters for specified fill (mandatory)
	-S <subfill>		Automatically set parameters for specified subfill (mandatory/optional)
				*** <fill> (and <subfill>) are mandatory arguments ***
				*** They are ignored if operation is = SPS2015,2021,2022 ***
				*** Otherwise you must specify both of them accordingly ***
				*** Or set <fill>=0 to select default dummy configuration ***
	--disable-arm1	Disable Arm1 calibration (default = enabled)
	--disable-arm2	Disable Arm2 calibration (default = enabled)
	--ped		Process and save pedestal events to output file (default = disabled)
	--level0	Save Level0 to output file (default = disabled)
	--level1	Save Level1 to output file (default = disabled)
	--level0cal	Save Level0 (no silicon/GSO bars) to output file (default = disabled)
	--level1cal	Save Level1 (no silicon/GSO bars) to output file (default = disabled)
	--level2cal	Save Level2 (no silicon/GSO bars) to output file (default = disabled)
	--hist		Save pedestal mean/RMS histograms to output file (default = disabled)
	--realign-arm2	Realign silicon to calorimeter events (default = disabled)
	--average-pedestal	Subtract pedestal average instead of delayed gate (default = disabled)
	--iterate-pedestal	Employ iterative procedure in pedestal computation (default = disabled)
	-v <verbose>	Set verbose level (default = 1)
				0 -> only errors
				1 -> some info
				2 -> debug
				3 -> all debug
	-h		Show usage
```

Example)
```
./bin/Calibrate -i run2798.root -o run2798_ -F 3855 -t ../Tables/ 
```

### Options
- ``-F`` Fill and ``-S`` Sub-fill numbers
  - This specify the data set by the LHC Fill number and the subset number, which is required to tune the time-dependent calibration parameters (for the moment, used only small correction of pedestal values.)
  - The sub-set number is defined using the range of LHCf run number. Please refer the data set pages ([Op2015](./Op2015.md), [Op2022](./Op2022.md)). 
  - In case of SPS beam tests, Sub-fill number should be always 1 for the moment.
  - In case of MC, you must specify the same numbers used in ConvertMCtoLvl0. 
  - (These values can be specified by negative value; 8178 = -8178 )
- ``--average-pedestal``
  - Use average mode for the pedestal subtraction.
  - For Op2016, Op2022 analysis, it must be ON.
- ``--pede``
  - Save the pedestal events to the output TTree also.
  - This option must be ON for LHCf-ATLAS joint analysis to avoid mis-matching between two data sets. 
  - In Op2022, the pedestal-trigger means "zero-bias" trigger and these pedestal triggered events may be useful to check the performance like trigger efficiency.
- save options of intermediate files: ``--level0, --level1, --level0cal, --level1cal, --level2cal``
  - mainly for debug 
  -  "level*cal" option means that only calorimeter (scintillator) layers are saved, and position detectors are not to save the file size.  


## Main function and classes 
- calibrate.cpp      *main function*
- nLHCf::LHCfCalib   *Event loop*
- nLHCf::InOutCal    *Input output control*
- nLHCf::EventCal    *Calibration for each event*

### Core member functions in nLHCf::EventCal　

- nLHCf::EventCal::EventCalibration
  - Core function of the EventCal.
  - Event selection by 
    - Cut Pedestal event
    - Cut Non-collding bunch events 
    - Select only shower (L2TA) and Pi0 (L2TSpecial) events
- nLHCf::EventCal::ConvertToLevel0
  - Format conversion from MIDAS format to Level0
  - Validation check of silicon data by CRC test 
- nLHCf::EventCal::ConvertToLevel1
  - nLHCf::EventCal::PedestalSubtraction 
  - nLHCf::EventCal::TdcOffsetSubtraction
  - if MC, nLHCf::EventCal::PedestalCorrection
- nLHCf::EventCal::ConvertToLevel2
  - nLHCf::EventCal::GainCalibration
  - if Arm1, nLHCf::EventCal::CrossTalkCorrection
  - nLHCf::EventCal::SetErrors
  
## Tables 
- **a\*_cal_hv.dat**  HV values of each PMT
- **a1_pos_hv.dat**   HV value of MAPMT in Arm1
- **a\*_pos_conv_factor.dat** Conversion factor of Pos. layers (ADC/dE [/GeV])
- **a1_pos_gain_factor.dat** Relative gain factor of Arm1 Pos. channels (HV at 650, 600, 550, 480 to 950)


