# How To use LHCfLibrary in python

**[ Please use ROOT of newer version than 6.22 ]**

Load the LHCfLibrary as 
```python
import ROOT
ROOT.gSystem.Load('../../build/lib/libDictionary.so');
```
Please replace the path to fit your environment.
You can also load other libraries like *libReconstruction.so* if your want to use it.

The classes in LHCfLibrary can be addressed as
```python
# ROOT.nLHCf.[ClassName].[Function]
ROOT.nLHCf.Utils.Printf(1, "Test \n") 

# For template classes like Level0, 1, 2
# C++ code nLHCf::Level2<nLHCf::Arm1Params>() ->
ROOT.nLHCf.Level2("nLHCf::Arm1Params")()
```

Sample codes are available in Samples/PyROOT/


