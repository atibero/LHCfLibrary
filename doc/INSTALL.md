# Installation memo for several environments

## For MAC (with ROOT installed via anaconda)

This is a memo for the library installation by Menjo on his Macbook

- Environment 
  - macOS 10.15.7 Catalina 
  - Homebrew  
    - cmake 3.19.0
- ROOT6 installed via anaconda  
see [https://iscinumpy.gitlab.io/post/root-conda/](https://iscinumpy.gitlab.io/post/root-conda/)
  - firstly install anaconda or miniconda in your system
  - create a configuration with root  
    ```conda create -n my_root_env root -c conda-forge```  
    then activate the environment  
    ```conda activate my_root_env```
  
- For Compiling the library **(old)**
  - cmake without the conda-root environment  
    (It is not understood why it needed. but without this, cmake had errors)  
    ```cmake (path-to-source)```
  - cmake again with the conda-root environment.
    For this, two parameters must be specified.  
    ```cmake (path-to-source) -DCMAKE_INSTALL_PREFIX=$CONDA_PREFIX   -DCMAKE_OSX_SYSROOT=$CONDA_BUILD_SYSROOT```
  - make
- For Compiling the library **(new)**
  - The error given in the previous method was investigated and finally solved by downgrading the CommandLine tool in Xcode to 11.5. 
  

## IDE for development 

Some examples of IDE installation and setup for LHCf Library

### CLion 
Menjo uses CLion for debugging and development. An advantage of CLion is to make a necessary configuration like compiler, library path etc vi CMakeLists.txt. The LHCf Library is fully configured by CMakeLists.txt, so you do not need to set CLion up to compile the library in your system. Just load the folder !!  

- Install CLion (C++ IDE of JetBrains IDE series)
  - CLion is not a free software but students and teachers can use it without free of charge. Firstly your need to register JetBrains from https://www.jetbrains.com/shop/eform/students . Anuual verification is needed to keep the Educational free subscription.  
  - Donwload CLion https://www.jetbrains.com/clion
  - Install it to your system
  
- Specific on Menjo's Mac
  - Because ROOT6 environment is provided by anaconda, the configuration must be activated on CLion. The easiest way to do it is to launch Clion from a terminal after configuring anaconda.  
    ```
    (base) menjo@MenjoMac-3 ~ % conda activate my_root_env
    (my_root_env) menjo@x86_64-apple-darwin13 ~ % open /Applications/CLion.app
    ```
  
- Open the LHCfLibrary directory on CLion
  - CLion automatically load CMakeLists.txt and configure it. 
  Note) Do not overwrite CMakeLists.txt
  - It takes a bit time to load huge ROOT library.
  - Compile the library 
    - select Build All
    - click the hammer button on the top menu.  　
  ![](fig/clion.png)
      

